<?php
ini_set('display_errors', 1);

$client = new SoapClient('http://127.0.0.1/patrick_mage_sublogin/api/soap/?wsdl'); // v1
$session = $client->login('sublogin', 'sublogin123'); // login(username, apiKey)

// how to create new entry
$data = array(
	'name'			=>		"Allow Order View",
	'identifier'	=>		"allow_orderview",
);
$newId = $client->call($session, 'mageb2b_sublogin_acl.create', array($data));
echo "########## CREATE ##########<br>";
echo "<pre>";
print_r($newId);
echo "</pre>";

// how to get list
$params = array('filter' => array(
    'acl_id' => array('eq' => $newId),
));
$result = $client->call($session, 'mageb2b_sublogin_acl.list', $params);

echo "########## LIST ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to get single row: info
$result = $client->call($session, 'mageb2b_sublogin_acl.info', array($newId));
echo "########## INFO ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to update existing entry
$data = array(
	'name'			=>		"Allow Order View2",
	'identifier'	=>		"allow_orderview2",
);
$result = $client->call($session, 'mageb2b_sublogin_acl.update', array(
	$newId,
	$data
));
echo "########## UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// getting info after update: info
$result = $client->call($session, 'mageb2b_sublogin_acl.info', array($newId));
echo "########## INFO AFTER UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to delete
$result = $client->call($session, 'mageb2b_sublogin_acl.delete', array($newId));
echo "########## DELETE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";
$client->endSession($session);
