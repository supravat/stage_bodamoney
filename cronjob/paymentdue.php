<?php 
require_once '../app/Mage.php';
Mage::app();
Varien_Profiler::enable();
Mage::setIsDeveloperMode(false);
$_currenttime = date("d.m.Y");
$_intcurrenttime = strtotime($_currenttime);
$transactions=Mage::getModel('customercredit/transaction')
              ->getCollection()
            ->addFieldToFilter('due_time',$_intcurrenttime)
            ->addFieldToFilter('status','pending');
foreach($transactions  as $transaction){

    $receiver_email =$transaction->getData("customer_email_receive");
    $amount_credit  =abs($transaction->getData("amount_credit"));
    $message_detail =$transaction->getData("comment");
    $trans_id       =$transaction->getId();
    $sender_id      =$transaction->getData("customer_id");
    $sender_parent_id =$transaction->getData("parent_id");
    try{
        if(!$sender_parent_id){
            $sender=Mage::getModel('customer/customer')->load($sender_id);
        }else{
            $sender=Mage::getModel('sublogin/sublogin')->load($sender_id);
        }
        $is_sub = false;
        $friend_account_id = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email',$receiver_email)->getFirstItem()->getId();
        if (!$friend_account_id) {     
            $account= Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email',$receiver_email)->getFirstItem();
            $friend_account_id=$account->getId();
            if($friend_account_id){
                $is_sub = true;
                $parent_recevie_id=$account->getData("entity_id");
            }
        }else{
            $account=Mage::getModel('customer/customer')->load($friend_account_id);
        }
        /////////////////////////////
         if($sender->getCreditValue()<$amount_credit)
        {               
                $sender->setData("credit_value",($sender->getCreditValue()+$amount_credit))->save(); 
                Mage::getModel('customercredit/customercredit')->failConfirmToFriendByEmail($amount_credit,$sender->getEmail(),$message_detail);  
                continue;
        }
        /////////////////////////////
        if($account)
        {   echo "Sender:";
            echo $sender->getEmail();
            echo "<Br>";       
            echo "Receive :".$account->getCreditValue();
            $amount=$account->getCreditValue();           
            echo $amount;
            $account->setData("credit_value",($amount+$amount_credit))->save(); 
            echo "Receive :".$account->getCreditValue();
            var_dump($receiver_email);
            var_Dump($amount_credit);
             Mage::getModel('customercredit/customercredit')->receiveConfirmToFriendByEmail($amount_credit,$receiver_email,$message_detail);          
        }
        else
            Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount_credit,$receiver_email,$message_detail);
          if($friend_account_id){
              if(!$is_sub){
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $receiver_email . " received " . $amount_credit ." credit from " . $sender->getName(), "", $amount_credit, "","",$account->getName(),0, $message_detail, $sender->getEmail());
              }else{
                    Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $receiver_email . " received " . $amount_credit ." credit from " . $sender->getFirstName()." ".$sender->getLastName(), "", $amount_credit, "", "", $account->getName(),$parent_recevie_id, $message_detail,$sender->getEmail());
            }
           
        }
         Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status","Completed")->save();
         Mage::getModel('customercredit/customercredit')->sendConfirmToFriendByEmail($amount_credit,$sender->getEmail(),$message_detail); 
            
    }catch(exception $ex){
        $transactions=Mage::getModel('customercredit/transaction')->load($trans_id);
        $transactions->setData("type_transaction_id",Magestore_Customercredit_Model_TransactionType::TYPE_CANCEL_SHARE_CREDIT)->save();
        $transactions->setData("status",Magestore_Customercredit_Model_Status::STATUS_CANCELLED)->save();
    }
}            
?>