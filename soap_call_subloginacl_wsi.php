<?php
ini_set('display_errors', 1);
## MAIN DIFFERENCE IN WSI IS THAT PARAMETERS ARE GOING TO BE PASSED AS AN ARRAY WHILE IN WSDL DIRECT PARAMETER CAN BE PASSED
## SO IN WSDL: $client->subloginAclList($session, $filters);
## WHILE SAME CAN USE USED IN THIS IN WSI COMPLIANT MODE: 
	#$client->subloginAclList(array(
	#	'sessionId'=>$sessionId, 
	#	'filters'=>$filters
	#));
## AND SESSION ID NEEDS BE GET IN THIS WAY: $sessionId = $session->result;

$client = new SoapClient('http://127.0.0.1/patrick_mage_sublogin/api/v2_soap?wsdl', array('trace' => 1, 'connection_timeout' => 120)); // v2
$session = $client->login(array(
	'username' => "sublogin",
	'apiKey' => "sublogin123"
));
$sessionId = $session->result;

// how to create new entry
$data = array(
	'name'			=>		"Allow Order View",
	'identifier'	=>		"allow_orderview",
);
$result = $client->subloginAclCreate(array(
	'sessionId'=>$sessionId, 
	'data'=>$data
));
$newId = $result->result;
echo "########## CREATE ##########<br>";
echo "<pre>";
print_r($newId);
echo "</pre>";

// how to get list
$filters = array(
	'complex_filter' => array(
		array(
			'key' => 'acl_id',
			'value' => array(
					'key' => 'eq',
					'value' => $newId
			),
		),
	),
);

$result = $client->subloginAclList(array(
	"sessionId" => $sessionId, 
	"filters" => $filters
));
echo "########## LIST ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to get single row: info
$result = $client->subloginAclInfo(array(
	'sessionId'=>$sessionId,
	'id'=>$newId, // OR can specify id manually for example: 1
));

echo "########## INFO ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to update existing entry
$data = array(
	'name'			=>		"Allow Order View2",
	'identifier'	=>		"allow_orderview2",
);
$result = $client->subloginAclUpdate(array(
	'sessionId'=>$sessionId, 
	'id'=>$newId, // OR can specify id manually for example: 1
	'data'=>$data
));
echo "########## UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

$result = $client->subloginAclInfo(array(
	'sessionId'=>$sessionId,
	'id'=>$newId, // OR can specify id manually for example: 1
));
echo "########## INFO AFTER UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";


// how to delete
$result = $client->subloginAclDelete(array(
	'sessionId'=>$sessionId, 
	'id'=>$newId, // OR can specify id manually for example: 1
)); 
echo "########## DELETE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

$client->endSession(array(
	'sessionId'=>$sessionId
));
