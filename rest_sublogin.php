<?php
require_once('debug.php');
ini_set('display_errors', 1);
/********************************************************************
File name: rest_sublogin.php
********************************************************************/
$consumerKey = 'cbef2cfa269937b3ec9f758d7ab01296'; // from Admin Panel's &quot;REST - OAuth Consumers page&quot;
$consumerSecret = '7bc51629408ea7ae4812c7dd27e482a1'; // from Admin Panel's &quot;REST - OAuth Consumers page&quot;

$callbackUrl = "http://127.0.0.1/patrick_mage_sublogin/rest_sublogin.php";
// Set the URLs below to match your Magento installation
$temporaryCredentialsRequestUrl = "http://127.0.0.1/patrick_mage_sublogin/oauth/initiate?oauth_callback=" . urlencode($callbackUrl);
$adminAuthorizationUrl = 'http://127.0.0.1/patrick_mage_sublogin/admin/oauth_authorize';
$accessTokenRequestUrl = 'http://127.0.0.1/patrick_mage_sublogin/oauth/token';
$apiUrl = 'http://127.0.0.1/patrick_mage_sublogin/api/rest';
session_start();
// un-comment following lines just to re-init session and reload browser only once, just after that comment it out
 //session_destroy();
 //return;
// session_start();

$sessionContainer = 'mageb2b_sublogin';
if (!isset($_GET['oauth_token']) && isset($_SESSION[$sessionContainer]['state']) && $_SESSION[$sessionContainer]['state'] == 1) {
	$_SESSION[$sessionContainer]['state'] = 0;
}
elseif (!isset($_SESSION[$sessionContainer]['state']))
{
	$_SESSION[$sessionContainer]['state'] = 0;
}

try {	
	$authType = ($_SESSION[$sessionContainer]['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION : OAUTH_AUTH_TYPE_URI;
	$oauthClient = new OAuth($consumerKey, $consumerSecret, OAUTH_SIG_METHOD_HMACSHA1, $authType);
	$oauthClient->enableDebug();
	
	if (!isset($_GET['oauth_token']) && !$_SESSION[$sessionContainer]['state']) {
		$requestToken = $oauthClient->getRequestToken($temporaryCredentialsRequestUrl);
		$_SESSION[$sessionContainer]['secret'] = $requestToken['oauth_token_secret'];
		$_SESSION[$sessionContainer]['state'] = 1;
		header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' . $requestToken['oauth_token']);
		exit;
	} else if ($_SESSION[$sessionContainer]['state'] == 1) {
		$oauthClient->setToken($_GET['oauth_token'], $_SESSION[$sessionContainer]['secret']);
		$accessToken = $oauthClient->getAccessToken($accessTokenRequestUrl);
		$_SESSION[$sessionContainer]['state'] = 2;
		$_SESSION[$sessionContainer]['token'] = $accessToken['oauth_token'];
		$_SESSION[$sessionContainer]['secret'] = $accessToken['oauth_token_secret'];
		header('Location: ' . $callbackUrl);
		exit;
	} else {
		// We have the OAuth client and token. Now, let's make the API call.
		$oauthClient->setToken($_SESSION[$sessionContainer]['token'], $_SESSION[$sessionContainer]['secret']);
		
		// Set the array of params to send with the request		
		$resourceUrl = "$apiUrl/mageb2b/sublogin";
		$headers = array(
			'Accept' => 'application/json',
			'Content-Type' => 'application/json',
		);
		
		// Create new staff
		echo "<h4>Create</h4><br>";
		$data = array(
			'entity_id'			=>		"127", // customer id
			'email'				=>		time()."@mageb2b.com",
			'password'			=>		"123456",
			'firstname'			=>		"Firstname",
			'lastname'			=>		"Lastname",
			'expire_date'		=>		"2015-12-30", // Y-m-d
			'active'			=>		"1",
			'send_backendmails'	=>		"0",
			'create_sublogins'	=>		"0",
			'is_subscribed'		=>		"0",
			'store_id'			=>		"1",
			'address_ids'		=>		"",
			'acl'				=>		"allow_checkout,allow_orderview",
		);
		$oauthClient->fetch($resourceUrl, json_encode($data), OAUTH_HTTP_METHOD_POST, $headers);		
		$response = $oauthClient->getLastResponse();
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		$newId = 5; //$response;
		//die();
		
		// Retrieve collection
		echo "<h4>Retrieve collection</h4><br>";
		$collectionFilters = array();
		$oauthClient->fetch($resourceUrl, $collectionFilters, OAUTH_HTTP_METHOD_GET, $headers);
		$response = json_decode($oauthClient->getLastResponse(), true);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		
		// Retrieve filtered collection
		echo "<h4>Retrieve filtered collection</h4><br>";
		$collectionFilters = array('id'=>$newId);
		$oauthClient->fetch($resourceUrl, $collectionFilters, OAUTH_HTTP_METHOD_GET, $headers);
		$response = json_decode($oauthClient->getLastResponse(), true);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		
		// Update entry
		echo "<h4>Update entry</h4><br>";
		$data = array(
			'entity_id'			=>		"127", // customer id
			'email'				=>		time()."@mageb2b.com",
			'password'			=>		"123456",
			'firstname'			=>		"Firstname updated",
			'lastname'			=>		"Lastname updated",
			'expire_date'		=>		"2016-12-30", // Y-m-d
			'active'			=>		"1",
			'send_backendmails'	=>		"0",
			'create_sublogins'	=>		"1",
			'is_subscribed'		=>		"0",
			'store_id'			=>		"1",
			'address_ids'		=>		"",
			'acl'				=>		"allow_checkout,allow_orderview",
		);
		$oauthClient->fetch($resourceUrl.'/'.$newId, json_encode($data), OAUTH_HTTP_METHOD_PUT, $headers);
		$response = json_decode($oauthClient->getLastResponse(), true);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		
		// Retrieve single entry
		echo "<h4>Retrieve Single Entry</h4><br>";
		$collectionFilters = array();
		$oauthClient->fetch($resourceUrl.'/'.$newId, $collectionFilters, OAUTH_HTTP_METHOD_GET, $headers);
		$response = json_decode($oauthClient->getLastResponse(), true);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
		//die();
		
		// delete entry
		echo "<h4>Delete entry</h4><br>";
		$collectionFilters = array();
		$oauthClient->fetch($resourceUrl.'/'.$newId, $collectionFilters, OAUTH_HTTP_METHOD_DELETE, $headers);
		$response = $oauthClient->getLastResponse();	
		echo "<pre>";
		print_r($response);
		echo "</pre>";		
		
		// Retrieve collection
		echo "<h4>Retrieve collection</h4><br>";
		$collectionFilters = array();
		$oauthClient->fetch($resourceUrl, $collectionFilters, OAUTH_HTTP_METHOD_GET, $headers);
		$response = json_decode($oauthClient->getLastResponse(), true);
		echo "<pre>";
		print_r($response);
		echo "</pre>";
	}
} catch (OAuthException $e) {
	print_r($e->getMessage());
	echo "<br/>";
	print_r($e->lastResponse);
}
