﻿namespace GrabzIt.Enums
{
    public enum PageSize
    {
        A3,
        A4,
        A5,
        B3,
        B4,
        B5,
        Letter
    }
}
