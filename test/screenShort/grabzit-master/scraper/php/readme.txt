GrabzIt Scraper 1.0
===================

This library enables you to process scraped data and integrate it into your own application.

Its usually best to place these package files in their own directory.

To run this example create a scrape here: http://grabz.it/scraper/scrapes.aspx

On the Export Options tab choose "Callback URL" option from the Send Results Via drop down.

Then enter the URL of the handler.php so for instance http://www.example.com/scrape/handler.php

Ensure your PHP application has read and write access to the "results" directory.

More documentation can be found at: http://grabz.it/scraper/api/php/