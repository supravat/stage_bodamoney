<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Customer account controller
 *
 * @category   Mage
 * @package    Mage_Customer
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
{
    /**
     * Action list where need check enabled cookie
     *
     * @var array
     */
    protected $_cookieCheckActions = array('loginPost', 'createpost');

    /**
     * Retrieve customer session model object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Action predispatch
     *
     * Check customer authentication for some actions
     */
    public function preDispatch()
    {
        // a brute-force protection here would be nice

        parent::preDispatch();

        if (!$this->getRequest()->isDispatched()) {
            return;
        }

        $action = $this->getRequest()->getActionName();
        $openActions = array(
            'create',
            'login',
            'logoutsuccess',
            'forgotpassword',
            'forgotpasswordpost',
            'resetpassword',
            'resetpasswordpost',
            'confirm',
            'confirmation',
            'ajaxloginpost'
        );
        $pattern = '/^(' . implode('|', $openActions) . ')/i';

        if (!preg_match($pattern, $action)) {
            if (!$this->_getSession()->authenticate($this)) {
                $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->_getSession()->setNoReferer(true);
        }
    }

    /**
     * Action postdispatch
     *
     * Remove No-referer flag from customer session after each action
     */
    public function postDispatch()
    {
        parent::postDispatch();
        $this->_getSession()->unsNoReferer(false);
    }

    /**
     * Default customer account page
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $this->getLayout()->getBlock('content')->append(
            $this->getLayout()->createBlock('customer/account_dashboard')
        );
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Account'));
        $this->renderLayout();
    }

    /**
     * Customer login form page
     */
    public function loginAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $this->getResponse()->setHeader('Login-Required', 'true');
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    /**
     * Login post action
     */
    public function loginPostAction()
    {
       Mage::log('yess',null,'login.log');
       
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*/');
          return;
       }
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        Mage::log('yess111',null,'login.log');
        
        $session = $this->_getSession();
        if ($this->getRequest()->isPost()) {
        	
        	Mage::log('yess3333',null,'login.log');
        	
            $login = $this->getRequest()->getPost('login');
           
           if(!empty($login['phone_number'])&&$login['phone_number']!="")
           {
               $login['username']=trim($login['country_code'].$login['phone_number'])."@bodamoney.com";
               $login['username']=trim($login['username']);
           }
          
            if (!empty($login['username']) && !empty($login['password'])) {
                try {  
                  
                    $session->login($login['username'], $login['password']);
                   
                    if ($session->getCustomer()->getIsJustConfirmed()) {
                        $this->_welcomeCustomer($session->getCustomer(), true);
                    }
                } catch (Mage_Core_Exception $e) {
                 
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
                            $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                } catch (Exception $e) {
                	Mage::log('yess444',null,'login.log');
                    Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }
        
        Mage::log('yess55555',null,'login.log');
        
        $this->_loginPostRedirect();
    }
    /*
    public function loginPostAction()
    {

        if (!$this->_validateFormKey()) {            
            return;
        }
        if ($this->_getSession()->isLoggedIn()) {
            return;
        }       
        $result = array('success' => false);
        
        $session = $this->_getSession();
        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
          
           if(!empty($login['phone_number'])&&$login['phone_number']!="")
           {
               $login['username']=trim($login['country_code'].$login['phone_number'])."@bodamoney.com";
               $login['username']=trim($login['username']);
           }
         
            if (!empty($login['username']) && !empty($login['password'])) {
               
                try {                    
                    $session->login($login['username'], $login['password']);
                    $result['success'] = true;
                    if ($session->getCustomer()->getIsJustConfirmed()) {
                        $this->_welcomeCustomer($session->getCustomer(), true);
                    }
                     $codes=$this->_getAffilineCode();
                    $quote = Mage::getModel('checkout/cart')->getQuote();
                    $items = $quote->getAllVisibleItems();
                    $product_name="";
                    if($items)
                    foreach ($items as $item) {
                         $product_name = $item->getProduct()->getName();
                         break;
                    }
                    $script="";
                    if($product_name!="")
                    foreach($codes as $code=>$name){
                        if($code&&$name==$product_name){
                            $script=$code;
                        }
                    }
                    $result['redirect'] = $this->_ajaxloginPostRedirect();
                    $result['code']  =$script;
                    $result['urlpath']=Mage::getUrl("checkout/cart/couponPost/") ;
                } catch (Mage_Core_Exception $e) {
                 
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
                            $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    //$session->addError($message);
                    $result['error'] = $message;
                    $session->setUsername($login['username']);
                } catch (Exception $e) {
                    // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                     $result['error'] = $e->getMessage();
                }
            } else {
                //$session->addError($this->__('Login and password are required.'));
                 $result['error'] = $this->__('Login and password are required.');
            }
        }

        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }*/
    protected function _ajaxloginPostRedirect()
    {
        $session = $this->_getSession();

        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
            // Set default URL to redirect customer to
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                )) {
                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = $this->_getModel('core/url')
                            ->getRebuiltUrl( $this->_getHelper('core')->urlDecode($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
            } else {
                $session->setBeforeAuthUrl( $this->_getHelper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() ==  $this->_getHelper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl( $this->_getHelper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
        }
        return ($session->getBeforeAuthUrl(true));
    }
    /**
     * Define target URL and redirect customer after logging in
     */
    protected function _loginPostRedirect()
    {
        $session = $this->_getSession();

        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
            // Set default URL to redirect customer to
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                )) {
                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = $this->_getModel('core/url')
                            ->getRebuiltUrl( $this->_getHelper('core')->urlDecode($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
            } else {
                $session->setBeforeAuthUrl( $this->_getHelper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() ==  $this->_getHelper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl( $this->_getHelper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
        }
        $this->_redirectUrl($session->getBeforeAuthUrl(true));
    }

    /**
     * Customer logout action
     */
    public function logoutAction()
    {
        $this->_getSession()->logout()
            ->renewSession();
        Mage::getSingleton("core/session")->setSubloginEmail(false);
        $this->_redirect('*/*/logoutSuccess');
    }

    /**
     * Logout success page
     */
    public function logoutSuccessAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Customer register form page
     */
    public function createAction()
    {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Create customer account action
     */
    public function createPostAction() {
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input
        if (!$this->getRequest()->isPost()) {
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return;
        }
        $p = $this->getRequest()->getPost();
        $p['emails'] = array_filter($p['emails'], 'strlen');
        $p['mobiles'] = array_filter($p['mobiles'], 'strlen');
        $this->getRequest()->setPost('emails', $p['emails']);
        $this->getRequest()->setPost('mobiles', $p['mobiles']);
        $isExists = $this->isMobileEmailExists();
        if ($isExists === 'REQUIRED') {
            $session->setCustomerFormData($this->getRequest()->getPost())
                ->addError($this->__('Unable to create account. Atleast an email address or a mobile number is required.'));
        } else if ($isExists) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                ->addError($this->__('Unable to create account. One or more email address or mobile number is already registered with us.'));
        }
        if ($isExists) {
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return true;
        }
        $customer = $this->_getCustomer();
        $post = $this->getRequest()->getPost();
        $output=$post;
        $email = false;
        $isEmail = false;
        $isMobile = false;

        if(count($post['mobiles'])>0)
        {
            foreach($post['mobiles'] as $pos=>$val)
            {
                $post['mobiles'][$pos]=$post['country_code'][$pos].$val;
                $output['mobiles'][$pos]=$post['country_code'][$pos]."-".$val;
            }
        }

        if (isset($post['emails']) && count($post['emails']) > 0) {
            $emails = array_map('trim', $post['emails']);
            $emails = array_unique($emails);
            $emails = implode(',', $emails);
            $helper = Mage::helper('bodacustomer');
            $isEmail = $helper->checkExistFinalEmailMobileSaving($emails, '', 'customer_emails');
            $customer->setData('customer_emails', $emails);
            $email = reset($post['emails']);
        }
        if (isset($post['mobiles']) && count($post['mobiles']) > 0) {
            $mobiles = array_map('trim', $post['mobiles']);
            $mobiles = array_unique($mobiles);
            $mobiles = implode(',', $mobiles);
            $helper = Mage::helper('bodacustomer');
            $isMobile = $helper->checkExistFinalEmailMobileSaving($mobiles, '', 'customer_mobiles');
            $customer->setData('customer_mobiles', $mobiles);
            if (!$email) {
                $email = reset($post['mobiles']) . '@bodamoney.com';
            }
        }
        if($isEmail || $isMobile){
            $session->setCustomerFormData($output)
                ->addException($e, $this->__('Unable to create account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return;
        }
        if ($email) {
            $this->getRequest()->setPost('email', $email);
            $customer->setEmail($email);
        } else {
            $session->setCustomerFormData($output)
                ->addException($e, $this->__('Unable to create account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
        }
        try {
            $errors = $this->_getCustomerErrors($customer);

            if (empty($errors)) {
                $customer->setData("can_create_sublogins",1);
                $customer->setData("max_number_sublogins",5); 
                
                ////
                ////
                $customer->save();
                ///
                $datebirth=$customer->getData("dob");
                $datebirth =date("d-m-Y",strtotime($datebirth));
            
                $datebirth =str_replace("-","",$datebirth);
                $id =$customer->getId();
                if(strlen($id)==1)$id="000$id";
                elseif(strlen($id)==2)$id="00$id";
                elseif(strlen($id)==3)$id="0$id";
                else
                $id="0$id";
                $datebirth.=$id;
              
              
                $customer->setData("kamome_customer",$datebirth)->save();
                ///
                $this->_dispatchRegisterSuccess($customer);
                $this->_successProcessRegistration($customer);
                return;
            } else {
                $this->_addSessionError($errors);
            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                $session->setEscapeMessages(false);
            } else {
                $message = $e->getMessage();
            }
            $session->addError($message);
        } catch (Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                ->addException($e, $this->__('Cannot save the customer.'));
        }
        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
        $this->_redirectError($errUrl);
    }

    protected function isMobileEmailExists($postAction = 'createPost') {
        $post = $this->getRequest()->getPost();
        $required = true;
        if (isset($post['emails']) && count($post['emails']) > 0 ||
            isset($post['mobiles']) && count($post['mobiles']) > 0
        ) {
            $required = false;
        }
        if ($required) {
            return 'REQUIRED';
        }
        $collection = Mage::getResourceModel('customer/customer_collection')
            ->addAttributeToSelect(array('customer_emails', 'customer_mobiles'), true);
        $emails = array();
        $mobiles = array();
        foreach ($collection as $c) {
            $e = $c->getData('customer_emails');
            if ($e) {
                $emails = array_merge($emails, explode(',', $e));
            }
            $m = $c->getData('customer_mobiles');
            if ($m) {
                $mobiles = array_merge($mobiles, explode(',', $m));
            }
        }
        $mobiles = array_unique($mobiles);
        $emails = array_unique($emails);
        if ($postAction === 'editPost') {
            $bodacustomer_saved_emails = array();
            if (isset($post['bodacustomer_saved_emails'])) {
                $bodacustomer_saved_emails = explode(',', $post['bodacustomer_saved_emails']);
                $emails = array_diff($emails, $bodacustomer_saved_emails);
            }
            $bodacustomer_saved_mobiles = array();
            if (isset($post['bodacustomer_saved_mobiles'])) {
                $bodacustomer_saved_mobiles = explode(',', $post['bodacustomer_saved_mobiles']);
                $mobiles = array_diff($mobiles, $bodacustomer_saved_mobiles);
            }
        }
        $exists = false;
        if (isset($post['emails']) && count($post['emails']) > 0) {
            $commonemail = array_intersect($post['emails'], $emails);
            if (count($commonemail) > 0) {
                $exists = true;
            }
        }
        if (!$exists && isset($post['mobiles']) && count($post['mobiles']) > 0) {
            $commonmobile = array_intersect($post['mobiles'], $mobiles);
            if (count($commonmobile) > 0) {
                $exists = true;
            }
        }
        return $exists;
    }


    /**
     * Success Registration
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_AccountController
     */
    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer)
    {
        $session = $this->_getSession();
        if ($customer->isConfirmationRequired()) {
            /** @var $app Mage_Core_Model_App */
            $app = $this->_getApp();
            /** @var $store  Mage_Core_Model_Store*/
            $store = $app->getStore();
            $customer->sendNewAccountEmail(
                'confirmation',
                $session->getBeforeAuthUrl(),
                $store->getId()
            );
            $customerHelper = $this->_getHelper('customer');
            $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
                $customerHelper->getEmailConfirmationUrl($customer->getEmail())));
            $url = $this->_getUrl('*/*/index', array('_secure' => true));
        } else {
            $session->setCustomerAsLoggedIn($customer);
            $url = $this->_welcomeCustomer($customer);
        }
        $this->_redirectSuccess($url);
        return $this;
    }

    /**
     * Get Customer Model
     *
     * @return Mage_Customer_Model_Customer
     */
    protected function _getCustomer()
    {
        $customer = $this->_getFromRegistry('current_customer');
        if (!$customer) {
            $customer = $this->_getModel('customer/customer')->setId(null);
        }
        if ($this->getRequest()->getParam('is_subscribed', false)) {
            $customer->setIsSubscribed(1);
        }
        /**
         * Initialize customer group id
         */
        $customer->getGroupId();

        return $customer;
    }

    /**
     * Add session error method
     *
     * @param string|array $errors
     */
    protected function _addSessionError($errors)
    {
        $session = $this->_getSession();
        $session->setCustomerFormData($this->getRequest()->getPost());
        if (is_array($errors)) {
            foreach ($errors as $errorMessage) {
                $session->addError($errorMessage);
            }
        } else {
            $session->addError($this->__('Invalid customer data'));
        }
    }

    /**
     * Validate customer data and return errors if they are
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return array|string
     */
    protected function _getCustomerErrors($customer)
    {
        $errors = array();
        $request = $this->getRequest();
        if ($request->getPost('create_address')) {
            $errors = $this->_getErrorsOnCustomerAddress($customer);
        }
        $customerForm = $this->_getCustomerForm($customer);
        $customerData = $customerForm->extractData($request);
        $customerErrors = $customerForm->validateData($customerData);
        if ($customerErrors !== true) {
            $errors = array_merge($customerErrors, $errors);
        } else {
            $customerForm->compactData($customerData);
            $customer->setPassword($request->getPost('password'));
            $customer->setConfirmation($request->getPost('confirmation'));
            $customerErrors = $customer->validate();
            if (is_array($customerErrors)) {
                $errors = array_merge($customerErrors, $errors);
            }
        }
        return $errors;
    }

    /**
     * Get Customer Form Initalized Model
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_Model_Form
     */
    protected function _getCustomerForm($customer)
    {
        /* @var $customerForm Mage_Customer_Model_Form */
        $customerForm = $this->_getModel('customer/form');
        $customerForm->setFormCode('customer_account_create');
        $customerForm->setEntity($customer);
        return $customerForm;
    }

    /**
     * Get Helper
     *
     * @param string $path
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper($path)
    {
        return Mage::helper($path);
    }

    /**
     * Get App
     *
     * @return Mage_Core_Model_App
     */
    protected function _getApp()
    {
        return Mage::app();
    }

    /**
     * Dispatch Event
     *
     * @param Mage_Customer_Model_Customer $customer
     */
    protected function _dispatchRegisterSuccess($customer)
    {
        Mage::dispatchEvent('customer_register_success',
            array('account_controller' => $this, 'customer' => $customer)
        );
    }

    /**
     * Gets customer address
     *
     * @param $customer
     * @return array $errors
     */
    protected function _getErrorsOnCustomerAddress($customer)
    {
        $errors = array();
        /* @var $address Mage_Customer_Model_Address */
        $address = $this->_getModel('customer/address');
        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm = $this->_getModel('customer/form');
        $addressForm->setFormCode('customer_register_address')
            ->setEntity($address);

        $addressData = $addressForm->extractData($this->getRequest(), 'address', false);
        $addressErrors = $addressForm->validateData($addressData);
        if (is_array($addressErrors)) {
            $errors = array_merge($errors, $addressErrors);
        }
        $address->setId(null)
            ->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
            ->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
        $addressForm->compactData($addressData);
        $customer->addAddress($address);

        $addressErrors = $address->validate();
        if (is_array($addressErrors)) {
            $errors = array_merge($errors, $addressErrors);
        }
        return $errors;
    }

    /**
     * Get model by path
     *
     * @param string $path
     * @param array|null $arguments
     * @return false|Mage_Core_Model_Abstract
     */
    public function _getModel($path, $arguments = array())
    {
        return Mage::getModel($path, $arguments);
    }

    /**
     * Get model from registry by path
     *
     * @param string $path
     * @return mixed
     */
    protected function _getFromRegistry($path)
    {
        return Mage::registry($path);
    }

    /**
     * Add welcome message and send new account email.
     * Returns success URL
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param bool $isJustConfirmed
     * @return string
     */
    protected function _welcomeCustomer(Mage_Customer_Model_Customer $customer, $isJustConfirmed = false)
    {
        $this->_getSession()->addSuccess(
            $this->__('Thank you for registering with %s.', Mage::app()->getStore()->getFrontendName())
        );
        if ($this->_isVatValidationEnabled()) {
            // Show corresponding VAT message to customer
            $configAddressType =  $this->_getHelper('customer/address')->getTaxCalculationAddressType();
            $userPrompt = '';
            switch ($configAddressType) {
                case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
                    break;
                default:
                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation',
                        $this->_getUrl('customer/address/edit'));
            }
            $this->_getSession()->addSuccess($userPrompt);
        }

        $customer->sendNewAccountEmail(
            $isJustConfirmed ? 'confirmed' : 'registered',
            '',
            Mage::app()->getStore()->getId()
        );

        $successUrl = $this->_getUrl('*/*/index', array('_secure' => true));
        if ($this->_getSession()->getBeforeAuthUrl()) {
            $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
        }
        return $successUrl;
    }

    /**
     * Confirm customer account by id and confirmation key
     */
    public function confirmAction()
    {
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_getSession()->logout()->regenerateSessionId();
        }
        try {
            $id      = $this->getRequest()->getParam('id', false);
            $key     = $this->getRequest()->getParam('key', false);
            $backUrl = $this->getRequest()->getParam('back_url', false);
            if (empty($id) || empty($key)) {
                throw new Exception($this->__('Bad request.'));
            }

            // load customer by id (try/catch in case if it throws exceptions)
            try {
                $customer = $this->_getModel('customer/customer')->load($id);
                if ((!$customer) || (!$customer->getId())) {
                    throw new Exception('Failed to load customer by id.');
                }
            }
            catch (Exception $e) {
                throw new Exception($this->__('Wrong customer account specified.'));
            }

            // check if it is inactive
            if ($customer->getConfirmation()) {
                if ($customer->getConfirmation() !== $key) {
                    throw new Exception($this->__('Wrong confirmation key.'));
                }

                // activate customer
                try {
                    $customer->setConfirmation(null);
                    $customer->save();
                }
                catch (Exception $e) {
                    throw new Exception($this->__('Failed to confirm customer account.'));
                }

                // log in and send greeting email, then die happy
                $session->setCustomerAsLoggedIn($customer);
                $successUrl = $this->_welcomeCustomer($customer, true);
                $this->_redirectSuccess($backUrl ? $backUrl : $successUrl);
                return;
            }

            // die happy
            $this->_redirectSuccess($this->_getUrl('*/*/index', array('_secure' => true)));
            return;
        }
        catch (Exception $e) {
            // die unhappy
            $this->_getSession()->addError($e->getMessage());
            $this->_redirectError($this->_getUrl('*/*/index', array('_secure' => true)));
            return;
        }
    }

    /**
     * Send confirmation link to specified email
     */
    public function confirmationAction()
    {
        $customer = $this->_getModel('customer/customer');
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }

        // try to confirm by email
        $email = $this->getRequest()->getPost('email');
        if ($email) {
            try {
                $customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);
                if (!$customer->getId()) {
                    throw new Exception('');
                }
                if ($customer->getConfirmation()) {
                    $customer->sendNewAccountEmail('confirmation', '', Mage::app()->getStore()->getId());
                    $this->_getSession()->addSuccess($this->__('Please, check your email for confirmation key.'));
                } else {
                    $this->_getSession()->addSuccess($this->__('This email does not require confirmation.'));
                }
                $this->_getSession()->setUsername($email);
                $this->_redirectSuccess($this->_getUrl('*/*/index', array('_secure' => true)));
            } catch (Exception $e) {
                $this->_getSession()->addException($e, $this->__('Wrong email.'));
                $this->_redirectError($this->_getUrl('*/*/*', array('email' => $email, '_secure' => true)));
            }
            return;
        }

        // output form
        $this->loadLayout();

        $this->getLayout()->getBlock('accountConfirmation')
            ->setEmail($this->getRequest()->getParam('email', $email));

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Get Url method
     *
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function _getUrl($url, $params = array())
    {
        return Mage::getUrl($url, $params);
    }

    /**
     * Forgot customer password page
     */
    public function forgotPasswordAction()
    {
        $this->loadLayout();

        $this->getLayout()->getBlock('forgotPassword')->setEmailValue(
            $this->_getSession()->getForgottenEmail()
        );
        $this->_getSession()->unsForgottenEmail();

        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();
    }

    /**
     * Forgot customer password action
     */
    public function forgotPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken =  $this->_getHelper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            $this->_getSession()
                ->addSuccess( $this->_getHelper('customer')
                ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
                    $this->_getHelper('customer')->escapeHtml($email)));
            $this->_redirect('*/*/');
            return;
        } else {
            $this->_getSession()->addError($this->__('Please enter your email.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }

    /**
     * Display reset forgotten password form
     *
     * User is redirected on this action when he clicks on the corresponding link in password reset confirmation email
     *
     */
    public function resetPasswordAction()
    {
        $resetPasswordLinkToken = (string) $this->getRequest()->getQuery('token');
        $customerId = (int) $this->getRequest()->getQuery('id');
        try {
            $this->_validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken);
            $this->loadLayout();
            // Pass received parameters to the reset forgotten password form
            $this->getLayout()->getBlock('resetPassword')
                ->setCustomerId($customerId)
                ->setResetPasswordLinkToken($resetPasswordLinkToken);
            $this->renderLayout();
        } catch (Exception $exception) {
            $this->_getSession()->addError( $this->_getHelper('customer')->__('Your password reset link has expired.'));
            $this->_redirect('*/*/forgotpassword');
        }
    }

    /**
     * Reset forgotten password
     * Used to handle data recieved from reset forgotten password form
     */
    public function resetPasswordPostAction()
    {
        $resetPasswordLinkToken = (string) $this->getRequest()->getQuery('token');
        $customerId = (int) $this->getRequest()->getQuery('id');
        $password = (string) $this->getRequest()->getPost('password');
        $passwordConfirmation = (string) $this->getRequest()->getPost('confirmation');

        try {
            $this->_validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken);
        } catch (Exception $exception) {
            $this->_getSession()->addError( $this->_getHelper('customer')->__('Your password reset link has expired.'));
            $this->_redirect('*/*/');
            return;
        }

        $errorMessages = array();
        if (iconv_strlen($password) <= 0) {
            array_push($errorMessages, $this->_getHelper('customer')->__('New password field cannot be empty.'));
        }
        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $this->_getModel('customer/customer')->load($customerId);

        $customer->setPassword($password);
        $customer->setConfirmation($passwordConfirmation);
        $validationErrorMessages = $customer->validate();
        if (is_array($validationErrorMessages)) {
            $errorMessages = array_merge($errorMessages, $validationErrorMessages);
        }

        if (!empty($errorMessages)) {
            $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
            foreach ($errorMessages as $errorMessage) {
                $this->_getSession()->addError($errorMessage);
            }
            $this->_redirect('*/*/resetpassword', array(
                'id' => $customerId,
                'token' => $resetPasswordLinkToken
            ));
            return;
        }

        try {
            // Empty current reset password token i.e. invalidate it
            $customer->setRpToken(null);
            $customer->setRpTokenCreatedAt(null);
            $customer->setConfirmation(null);
            $customer->save();
            $this->_getSession()->addSuccess( $this->_getHelper('customer')->__('Your password has been updated.'));
            $this->_redirect('*/*/login');
        } catch (Exception $exception) {
            $this->_getSession()->addException($exception, $this->__('Cannot save a new password.'));
            $this->_redirect('*/*/resetpassword', array(
                'id' => $customerId,
                'token' => $resetPasswordLinkToken
            ));
            return;
        }
    }

    /**
     * Check if password reset token is valid
     *
     * @param int $customerId
     * @param string $resetPasswordLinkToken
     * @throws Mage_Core_Exception
     */
    protected function _validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken)
    {
        if (!is_int($customerId)
            || !is_string($resetPasswordLinkToken)
            || empty($resetPasswordLinkToken)
            || empty($customerId)
            || $customerId < 0
        ) {
            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Invalid password reset token.'));
        }

        /** @var $customer Mage_Customer_Model_Customer */
        $customer = $this->_getModel('customer/customer')->load($customerId);
        if (!$customer || !$customer->getId()) {
            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Wrong customer account specified.'));
        }

        $customerToken = $customer->getRpToken();
        if (strcmp($customerToken, $resetPasswordLinkToken) != 0 || $customer->isResetPasswordLinkTokenExpired()) {
            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Your password reset link has expired.'));
        }
    }

    /**
     * Forgot customer account information page
     */
    public function editAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $block = $this->getLayout()->getBlock('customer_edit');
        if ($block) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $data = $this->_getSession()->getCustomerFormData(true);
        $customer = $this->_getSession()->getCustomer();
        if (!empty($data)) {
            $customer->addData($data);
        }
        if ($this->getRequest()->getParam('changepass') == 1) {
            $customer->setChangePassword(1);
        }
        ///////////////////////////////////
        if ($this->_getSession()->isLoggedIn()) {
            $bhelper = Mage::helper('bodacustomer');
            $emails = $bhelper->getCustomerEmails($this->_getSession()->getId());
            $mobiles = $bhelper->getCustomerMobiles($this->_getSession()->getId());
            Mage::register('EM_session_formdata', array(
                    'emails' => $emails,
                    'mobiles' => $mobiles
            ));
        }
        ///////////////////////////////////
        $this->getLayout()->getBlock('head')->setTitle($this->__('Account Information'));
        $this->getLayout()->getBlock('messages')->setEscapeMessageFlag(true);
        $this->renderLayout();
    }

    /**
     * Change customer password action
     */
    public function editPostAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/edit');
        }

        if ($this->getRequest()->isPost()) {
             $session = $this->_getSession();
             if (!$session->isLoggedIn()) {
            return $this->_redirect('*/*/login');
        }
            $p = $this->getRequest()->getPost();
            $p['emails'] = array_filter($p['emails'], 'strlen');
            $p['mobiles'] = array_filter($p['mobiles'], 'strlen');
            if(count($p['country_code'])>0){
                foreach($p['mobiles'] as $k=>$val){
                  
                    $p['mobiles'][$k]=$p['country_code'][$k]."-".$val."|".$p['app'][$k];
                }
            }
         
            $p['country_code']=array_filter($p['mobiles'], 'strlen');
            
            $this->getRequest()->setPost('emails', $p['emails']);
            $this->getRequest()->setPost('mobiles', $p['mobiles']);
            $isExists = $this->isMobileEmailExists('editPost');
            if ($isExists === 'REQUIRED') {
                $session->setCustomerFormData($this->getRequest()->getPost())
                        ->addError($this->__('Unable to update account. Atleast an email address or a mobile number is required.'));
            } else if ($isExists) {
                $session->setCustomerFormData($this->getRequest()->getPost())
                        ->addError($this->__('Unable to update account. One or more email address or mobile number is already registered with us.'));
            }
            if ($isExists) {
                $this->_redirect('*/*/edit');
                return $this;
            }    
            /** @var $customer Mage_Customer_Model_Customer */
        $customer = $this->_getSession()->getCustomer();
        /////////////////////////////////////////////////////////////////////////
        $email = false;
        $emails = array();
        $mobiles = array();
        $post = $this->getRequest()->getPost();
        if (isset($post['emails']) && count($post['emails']) > 0) {
            $emails = array_map('trim', $post['emails']);
            $email = reset($post['emails']);
        }
        if (isset($post['mobiles']) && count($post['mobiles']) > 0) {
            if (!$email) {
                $email = reset($post['mobiles']) . '@bodamoney.com';
                $emails[] = $email;
            }
            $mobiles = array_map('trim', $post['mobiles']);
        }

        if (count($emails) > 0) {
            $emails = array_unique($emails);
            $emails = implode(',', $emails);
            $helper = Mage::helper('bodacustomer');
            $isEmail = $helper->checkExistFinalEmailMobileSaving($emails, $post['bodacustomer_saved_emails'], 'customer_emails');
            $customer->setData('customer_emails', $emails);
        } else {
            $customer->setData('customer_emails', '');
        }
        if (count($mobiles) > 0) {
            $mobiles = array_unique($mobiles);
            $mobiles = implode(',', $mobiles);
            $helper = Mage::helper('bodacustomer');
            $isMobile = $helper->checkExistFinalEmailMobileSaving($mobiles, $post['bodacustomer_saved_mobiles'], 'customer_mobiles');
            $customer->setData('customer_mobiles', $mobiles);
        } else {
            $customer->setData('customer_mobiles', '');
        }
        if ($isMobile || $isEmail) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Unable to save account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/edit', array('_secure' => true));
            $this->_redirectError($errUrl);
            $errors[] = $this->__('Unable to save account. Atleast an email or a mobile number is required.');
        }
        if ($email) {
            $customer->setData('email', $email);
            $this->getRequest()->setPost('email', $email);
        } else {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Unable to create account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/edit', array('_secure' => true));
            $this->_redirectError($errUrl);
            $errors[] = $this->__('Unable to save account. Atleast an email or a mobile number is required.');
        }
        //////////////////////////////////////////////////////
            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = $this->_getModel('customer/form');
            $customerForm->setFormCode('customer_account_edit')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());

            $errors = array();
            $customerErrors = $customerForm->validateData($customerData);
            if ($customerErrors !== true) {
                $errors = array_merge($customerErrors, $errors);
            } else {
                $customerForm->compactData($customerData);
                $errors = array();

                // If password change was requested then add it to common validation scheme
                if ($this->getRequest()->getParam('change_password')) {
                    $currPass   = $this->getRequest()->getPost('current_password');
                    $newPass    = $this->getRequest()->getPost('password');
                    $confPass   = $this->getRequest()->getPost('confirmation');

                    $oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
                    if ( $this->_getHelper('core/string')->strpos($oldPass, ':')) {
                        list($_salt, $salt) = explode(':', $oldPass);
                    } else {
                        $salt = false;
                    }

                    if ($customer->hashPassword($currPass, $salt) == $oldPass) {
                        if (strlen($newPass)) {
                            /**
                             * Set entered password and its confirmation - they
                             * will be validated later to match each other and be of right length
                             */
                            $customer->setPassword($newPass);
                            $customer->setConfirmation($confPass);
                        } else {
                            $errors[] = $this->__('New password field cannot be empty.');
                        }
                    } else {
                        $errors[] = $this->__('Invalid current password');
                    }
                }

                // Validate account and compose list of errors if any
                $customerErrors = $customer->validate();
                if (is_array($customerErrors)) {
                    $errors = array_merge($errors, $customerErrors);
                }
            }

            if (!empty($errors)) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
                foreach ($errors as $message) {
                    $this->_getSession()->addError($message);
                }
                $this->_redirect('*/*/edit');
                return $this;
            }

            try {
                $customer->setConfirmation(null);
                $customer->save();
                $this->_getSession()->setCustomer($customer)
                    ->addSuccess($this->__('The account information has been saved.'));
                ////////////////////////////
                $datebirth=$customer->getData("dob");
                $datebirth =date("d-m-Y",strtotime($datebirth));
            
                $datebirth =str_replace("-","",$datebirth);
                $id =$customer->getId();
                if(strlen($id)==1)$id="000$id";
                elseif(strlen($id)==2)$id="00$id";
                elseif(strlen($id)==3)$id="0$id";
                else
                $id="0$id";
                $datebirth.=$id;              
                $customer->setData("kamome_customer",$datebirth)->save();
                /////////////////////////////
                $this->_redirect('customer/account');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }
        }

        $this->_redirect('*/*/edit');
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('dob'));
        return $data;
    }

    /**
     * Check whether VAT ID validation is enabled
     *
     * @param Mage_Core_Model_Store|string|int $store
     * @return bool
     */
    protected function _isVatValidationEnabled($store = null)
    {
        return  $this->_getHelper('customer/address')->isVatValidationEnabled($store);
    }
    /***
    **get affiline
    **/
    public function _getAffilineCode(){
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();              
        if ($customerId&&Mage::getStoreConfig('affiliateplus/coupon/enable') 
        && Mage::helper('affiliatepluscoupon')->isPluginEnabled()){	
                $data=array();
                $session = Mage::getSingleton('checkout/session');
                 $affilateData = $session->getData('affiliate_coupon_data');
                 if(is_array($affilateData)&&isset($affilateData['program_id']))
                 return null;
                if(!Mage::registry('program_coupon_codes')){  
                    ///////////
            		$account = Mage::helper('affiliateplus/account')->getAccount();                    
            		$accountId = $account->getId();
                   
            		$coupon = Mage::getModel('affiliatepluscoupon/coupon')->setCurrentAccountId($accountId);
            		$helper = Mage::helper('affiliatepluscoupon');
            		
            		$coupon->loadByProgram();
            		if (!$coupon->getId()){
            			try {
            				$coupon->setCouponCode($helper->generateNewCoupon())
            					->setAccountName($account->getName())
            					->setProgramName('Affiliate Program')
            					->save();
            			} catch (Exception $e){}
            		}
            		$account->setCouponCode($coupon->getCouponCode());
            		Mage::register('account_model',$account);
            		
            		if (Mage::helper('affiliatepluscoupon')->isMultiProgram()){   
            			$programs = Mage::getResourceModel('affiliateplusprogram/account_collection')
                			->addFieldToFilter('account_id',$accountId);
            			$pCouponCodes = array();
            			foreach ($programs as $accProgram){
            				$program = Mage::getModel('affiliateplusprogram/program')
            					->setStoreId(Mage::app()->getStore()->getId())
            					->load($accProgram->getProgramId());
            				if (!$program->getUseCoupon() || !floatval($program->getDiscount())) continue;
            				$coupon->setId(null)->loadByProgram($accProgram->getProgramId());
            				if (!$coupon->getId()){
            					try {
            						$coupon->setCouponCode($helper->generateNewCoupon($program->getCouponPattern()))
            							->setAccountName($account->getName())
            							->setProgramName($program->getName())
            							->save();
            					} catch (Exception $e){}
            				}
            				if ($coupon->getCouponCode()) $pCouponCodes[$program->getId()] = $coupon->getCouponCode();
            			}
            			Mage::register('program_coupon_codes',$pCouponCodes);
            		  }
                }
                 $programCoupons = Mage::registry('program_coupon_codes');
                 //$Observer =new Magestore_Affiliatepluscoupon_Model_Observer;
                 //$arr      =get_class_methods($Observer);
                /// print_R($arr); 
                 $program_obj= new Magestore_Affiliatepluscoupon_Block_Affiliatepluscoupon;
                 $listPrograms   =$program_obj->getListProgram();             
                 if (count($listPrograms))
                 {                                
                     foreach ($listPrograms as $program)
                     {                                
                        $coupon_code =$program->getCouponCode();
                        $data[$coupon_code]      =$program->getName();
                    } 
                    return $data;   
                 }
            
        }
        return null;
    }

}
