<?php
class Workiverse_Configuration_Model_Resource_Configuration extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct()
    {
        $this->_init('configuration/configuration', 'configuration_id');
    }
}