<?php
class Workiverse_Configuration_Model_Observer
{
    public function addMassAction(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
      
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'configuration')
        {
           
            $block->setMassactionIdField('ad_configuration_id');
            $block->setFormFieldName('configuration');
            $block->addItem('delete', array(
            'label'=> Mage::helper('core')->__('Delete'),
            'url'  => Mage::getUrl('*/*/massDelete', array('' => '')),
            'confirm' => Mage::helper('core')->__('Are you sure you want to delete the selected listing(s)?')
            ));
        }
    }
    public function adminhtml_block_html_before($event) {

    $block = $event->getBlock();
    if ($block instanceof Mage_Adminhtml_Block_Cms_Block_Grid) {

        $block->setMassactionIdField('ad_configuration_id');
        $block->getMassactionBlock()->setFormFieldName('configuration_ids');
        $block->getMassactionBlock()->setUseSelectAll(false);

        $block->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('core')->__('Delete'),
            'url'  => Mage::getUrl('*/*/massDelete', array('' => '')),
            'confirm' => Mage::helper('core')->__('Are you sure you want to delete the selected listing(s)?')
        ));

        $columnId = 'massaction';

        $massactionColumn = array(
            'index' => $block->getMassactionIdField(),
            'filter_index' => $block->getMassactionIdFilter(),
            'type' => 'massaction',
            'name' => $block->getMassactionBlock()->getFormFieldName(),
            'align' => 'center',
            'is_system' => true
        );

        if ($block->getNoFilterMassactionColumn()) {
            $massactionColumn->setData('filter', false);
        }

        // rearrange the columns;
        $oldColumns = $block->getColumns();
        foreach($oldColumns as $column){
           $block->removeColumn($column->getId());  
        }

        $block->addColumn($columnId, $massactionColumn);
        $block->addColumn('configuration_id', array(
            'header' => Mage::helper('cms')->__('ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'configuration_id',
        ));

        // put back the original columns
        foreach($oldColumns as $column){
            $block->addColumn($column->getId(),$column->getData());
        }

        return $this;
    }
}
}