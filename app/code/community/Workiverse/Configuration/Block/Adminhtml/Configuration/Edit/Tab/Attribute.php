<?php
class Workiverse_Configuration_Block_Adminhtml_Configuration_Edit_Tab_Attribute extends Mage_Adminhtml_Block_Widget_Form
{
    
   protected function _prepareForm()
   {
       $form = new Varien_Data_Form();
     
       $fieldset = $form->addFieldset('configuration_form',
                                       array('legend'=>'Attribute Configuration'));
       $fieldset->addField('attribute_code', 'text', array(
            'name'      => 'attribute_code',
            'label'     => Mage::helper('configuration')->__('Script name'),
            'title'     => Mage::helper('configuration')->__('Script name'),
            'required'  => false,
        ));  
      $fieldset->addField('custom_php_script', 'textarea',
                       array(
                          'label' => 'PHP Script code',
                          'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
                          'wysiwyg'   => true,  
                          'required'  => false,
                          'style'     => 'height:20em,',
                          'name' => 'custom_php_script',
                    ));
            
            
     if ( Mage::registry('configuration_data') )
     {
            $form->setValues(Mage::registry('configuration_data')->getData());
     }
      $this->setForm($form);
      return parent::_prepareForm();
 }
}