<?php
class Workiverse_Configuration_Block_Adminhtml_Configuration_Edit_Tab_Formreminder extends Mage_Adminhtml_Block_Widget_Form
{
   protected function _prepareForm()
   {
       $form = new Varien_Data_Form();
      
       $fieldset = $form->addFieldset('configuration_form',
                                       array('legend'=>'Form Reminder Message'));
        
      $fieldset->addField('reminder_message', 'editor',
                       array(
                          'label' => 'Reminder  Message',
                          'style'     => 'height:15em',
                           'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
                           'wysiwyg'   => true,
                           'required'  => false,
                           'name' => 'reminder_message',
                    ));
            
       if ( Mage::registry('configuration_data') )
       {
            $form->setValues(Mage::registry('configuration_data')->getData());
        }
       //$form->setUseContainer(true);
       $this->setForm($form);  
      return parent::_prepareForm();
 }
}