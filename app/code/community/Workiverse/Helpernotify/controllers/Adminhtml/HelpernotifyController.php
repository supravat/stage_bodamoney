<?php
class Workiverse_Helpernotify_Adminhtml_HelpernotifyController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
   
        $this->loadLayout();
        $this->renderLayout();
    }
 
    public function newAction()
    {
         // We just forward the new action to a blank edit form
        
        $this->_forward('edit');
        
    }
 
    public function editAction()
    {  
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('helpernotify/helpernotify');
     
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This Helpernotify no longer exists.'));
                $this->_redirect('*/*/');
     
                return;
            } 
             $this->_title($model->getName()); 
        }  
     
        $this->_title($this->__('New Helpernotify'));
     
        $data = Mage::getSingleton('adminhtml/session')->getHelpernotifyData(true);
        if (!empty($data)) {
            $model->setData($data);
        }  
        
        Mage::register('helpernotify_data', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Helpernotify') : $this->__('New Helpernotify'), $id ? $this->__('Edit Helpernotify') : $this->__('New Helpernotify'))
            ->_addContent($this->getLayout()->createBlock('workiverse_helpernotify/adminhtml_helpernotify_edit')
            ->setData('action', $this->getUrl('*/*/save')))
            ->_addLeft($this->getLayout()->createBlock('workiverse_helpernotify/adminhtml_helpernotify_edit_tabs')) 
            ->renderLayout();
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('helpernotify/helpernotify');
           
             unset($postData['form_key']);
            if(isset($postData['helpernotify_id'])&&$postData['helpernotify_id']!="")
            {
                $helpernotify_id=$postData['helpernotify_id'];    
                $postData['update_at']=date("Y-m-d"); 
                $model->load($helpernotify_id);
                        
            }else
            {
                $postData['created_at']=date("Y-m-d"); 
                unset($postData['helpernotify_id']);
               
            }
            $model->setData($postData);
            try {
                $model->save();
               
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The helpernotify has been saved.'));
                $this->_redirect('*/*/');
 
                return;
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this baz.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setBazData($postData);
            $this->_redirectReferer();
        }
    }
 
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('helpernotify/helpernotify');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('helpernotify')->__('The Helpernotify has been deleted.'));
                $this->_redirect('*/*/');
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the Helpernotify to delete.'));
        $this->_redirect('*/*/');
    }
    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('cms/helpernotify')
            ->_title($this->__('helpernotify'))->_title($this->__('Helpernotify'))
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Helpernotify'), $this->__('Helpernotify'));
         
        return $this;
    }
     public function messageAction()
    {
        $data = Mage::getModel('helpernotify/helpernotify')->load($this->getRequest()->getParam('id'));
        echo $data->getContent();
    }
    public function massDeleteAction()
    {
        $helpernotify_Ids = $this->getRequest()->getParam('helpernotify_ids');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
        if(!is_array($helpernotify_Ids)) {
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('helpernotify')->__('Please select helpernotify.'));
        } else {
        try {
            $helpernotifyModel = Mage::getModel('helpernotify/helpernotify');
            foreach ($helpernotify_Ids as $_Id) {
                $helpernotifyModel->load($_Id)->delete();
            }
        Mage::getSingleton('adminhtml/session')->addSuccess(
        Mage::helper('helpernotify')->__(
        'Total of %d record(s) were deleted.', count($taxIds)
        )
        );
        } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        }
         
        $this->_redirect('*/*/index');
    }
}
 ?>