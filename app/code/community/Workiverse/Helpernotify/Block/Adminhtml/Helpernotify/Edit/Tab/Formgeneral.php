<?php
class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify_Edit_Tab_Formgeneral extends Mage_Adminhtml_Block_Widget_Form
{
   protected function _prepareForm()
   {
         $form = new Varien_Data_Form();
         $fieldset = $form->addFieldset('helpernotify_fieldset', array(
            'legend'    => Mage::helper('helpernotify')->__('General Information'),
            'class'     => 'fieldset-wide',
        ));
        if(Mage::registry('helpernotify_data')){
            $model=Mage::registry('helpernotify_data');
              $fieldset->addField('helpernotify_id', 'hidden', array(
                'name' => 'helpernotify_id',
            ));      
        }
         
       $fieldset->addField('keyword', 'text', array(
            'name'      => 'keyword',
            'label'     => Mage::helper('helpernotify')->__('Keyword'),
            'title'     => Mage::helper('helpernotify')->__('Keyword'),
            'required'  => true,
        )); 
       
         if ( Mage::registry('helpernotify_data') )
         {
            $form->setValues(Mage::registry('helpernotify_data')->getData());
          }
         //$form->setUseContainer(true);
         $this->setForm($form);
          return parent::_prepareForm();
 }
}