<?php 
class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
    
     //where is the controller
     $this->_controller = 'adminhtml_helpernotify';
     $this->_blockGroup = 'workiverse_helpernotify';
     //text in the admin header
     $this->_headerText = 'Helper Management';
     //value of the add button
     $this->_addButtonLabel = 'Add a Helper';
   
     parent::__construct();
     //$this->_removeButton('add');
    }
}
?>