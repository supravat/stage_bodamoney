<?php
class Workiverse_Sendingmoney_Block_Payment extends Mage_Core_Block_Template
{
      private $_main_email="jpmpindi@nordug.com";
      private $_orderIncrementId;
	 /**
     * Get Form data by using ogone payment api
     *
     * @return array
     */
    public function getPayMentUrl(){
        return Mage::getUrl("gatewaysendingamoney/processing/doCharge");
    }
    public function getCheckCodeUrl(){
        return Mage::getUrl("gatewaysendingamoney/processing/checkPinCode");
    }
    public function getWithdrawUrl(){
        return Mage::getUrl("gatewaysendingamoney/processing/doWithdraw");
    }
    public function getFormData()
    {
        return $this->_getOrder()->getPayment()->getMethodInstance()->getFormFields();
    }
     public function getMainEmail(){
        return Mage::getStoreConfig('payment/sendingmoney/master');
    }
    public function getAffilineId(){
        return $this->_getOrder()->getCustomerId();
    }
    /**
     * Getting gateway url
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->_getOrder()->getPayment()->getMethodInstance()->getUrl();
    }
    
	/**
     * Return checkout session instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }
    public function getBodaFee(){
        $order=$this->_getOrder();
        $boda_fee     =$order->getData("fooman_surcharge_amount");
        $discount     =$order->getData("affiliateplus_discount");     
        return ($boda_fee+$discount);
    }
    public function getAffilineEmail(){
        $affiline_email =$this->_getOrder()->getCustomerEmail();
        $pos = strpos($affiline_email,"bodamoney.com");  
        if($pos==false)
          return $affiline_email;        
        else
        {
            $pos= explode("@",$email);
            return $pos[0];
        }
        return "Uregistered customer";
    }
    public function getAffilineName(){
        return $this->_getOrder()->getCustomerName();
    }
    public function getMessage(){
        return Mage::getSingleton('customer/session')->getOrderCustomerComment();
    }
    public function getReceiveEmail(){
        $payment  =$this->_getOrder()->getPayment();
        $email    =$payment->getData("puser");
        $pos = strpos($email,"@bodamoney.com");
        if($pos ===false) { 
            return $email;
          }else{
             $postion =explode("@",$email);
             return $postion[0];      
          }          
    }
     public function getReceiveName(){
        $payment  =$this->_getOrder()->getPayment();
        $email    =$payment->getData("puser");
        $receive=$this->checkcustomer($email);  
        if($receive)
        {
            return $receive->getName()?$receive->getName():($receive->getFirstname()." ".$receive->getLastname());
        }
        else{
            return "Uregistered customer";
        }
    }
    public function getReceiveGender(){
        $payment  =$this->_getOrder()->getPayment();
        $email    =$payment->getData("puser");
        $receive=$this->checkcustomer($email);  
        if($receive)
        {
            return $this->getCusterGender($receive);
        }
        else{
            return "NA";
        }
    }
    public function setOrder($incrementId){
        $this->_orderIncrementId=$incrementId;
    }
    public function getIncrementId(){
        return $_orderIncrementId;
    }
    /**
     * Return order instance
     *
     * @return Mage_Sales_Model_Order|null
     */
    protected function _getOrder()
    {
        if ($this->getOrder()) {
            return $this->getOrder();
         } elseif ($orderIncrementId = $this->_getCheckout()->getLastRealOrderId()) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        } elseif ($orderIncrementId = $this->getRequest()->getPost("transaction_id")) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        }
         else {
            return null;
        }
	}
    public function getorderdetail(){
        return $this->_getOrder();
    }
    public function getItems(){
        $order=$this->_getOrder();
         $items = $order->getAllVisibleItems();
         return $items;
    }
    public function getCustomer(){
        $order=$this->_getOrder();
        if($order->getCustomerId() === NULL){
            $customer =  $order->getCustomer();
        }
        //else, they're a normal registered user.
        else {
           $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());            
        }
        return $customer;
    }
    public function getCusterGender($customer){
        if(!$customer)
        $customer=$this->getCustomer();
            if($customer->getGender())
                return "Male";
        return "Female";
    }
    public function getAffilineAccount(){
        $email    =$this->_main_email;
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);
        return $customer;
    }
    public function getOrderComment(){
        $order=$this->_getOrder();
        $_history = $order->getAllStatusHistory();
        if (count($_history)){
            foreach ($_history as $_historyItem)
                return $_historyItem->getComment()?$_historyItem->getComment():null;
        }
        return null;
    }
    public function checkcustomer($email){
        if(!$email)
        return false;
        $email=trim($email);       
        $customer = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email', $email)->getFirstItem();
        if(!$customer||!$customer->getData("email")) {     
            $customer = Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email', $email)->getFirstItem();
            if($customer)$customer=Mage::getModel('sublogin/sublogin')->load($customer->getId());
        }else{
            $customer = Mage::getModel('customer/customer')->load($customer->getId());
        }
         if($customer&&$customer->getData("email"))
           return $customer;  
        return false; 
    }
     public function getSendingUrl(){
        return Mage::getUrl("gatewaysendingamoney/processing/doCharge");
    }
}
?>