<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Workiverse
 * @package     Workiverse_Perfectmoney
 * @copyright   Copyright (c) 2012 Workiverse Org (http://www.forummods.org)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Workiverse_Perfectmoney_ProcessingController extends Mage_Core_Controller_Front_Action
{
    private $_main_email="jpmpindi@nordug.com";
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Iframe page which submits the payment data to Perfectmoney.
     */
    public function placeformAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function customerAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function gatewayAction()
    {
       $this->loadLayout();
       $this->renderLayout();
       $this->getLayout()->getBlock('head')->setTitle($this->__('Payment Information'));
    }
     public function infoAction()
    {
       try {
            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
          
            $order->loadByIncrementId($session->getLastRealOrderId());
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('perfectmoney')->__('The customer was redirected to Perfect Money.')
            );
            $order->save();
            $session->getQuote()->setIsActive(false)->save();          
            //$session->clear();
            $this->loadLayout();
		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Info'));
		$this->renderLayout();
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    }
    public function doChargeAction(){
        try{
            if ($this->getRequest()->isPost()) {
                $data=$this->getRequest()->getPost();      
                $customerData = Mage::getSingleton('customer/session')->getCustomer();
                if(!$customerData||count($data)==0)
                    parent::_redirect('checkout/cart');
                $session = $this->_getCheckout();
                $orderIncrementId=$session->getLastRealOrderId();
                if(!$orderIncrementId||$orderIncrementId==null)
                {
                    Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                    parent::_redirect("onepagecheckout/index/failure/");
                    return ;
                }
            
                $order=Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
                $order_id =$order->getId();
                $amount=$order->getData("fooman_surcharge_amount");
                $sub_total=$order->getData("subtotal");
                $grand_total =$order->getGrandTotal();
             
                
                $payment  =$order->getPayment();
                $pcustomer =$payment->getData("puser");
                $message_detail	= Mage::getSingleton('customer/session')->getOrderCustomerComment();
                $blockpayment = new Workiverse_Perfectmoney_Block_Payment;               
                $sub      =Mage::helper('sublogin')->getCurrentSublogin();
             
                if($sub)
                {
                    $affiline_id=$sub->getId();
                    $affiline = Mage::getModel('sublogin/sublogin')->load($affiline_id);
                  
                }else{
                    $affiline = $customerData;
                    $parent_id=0;
                }               
                if(!$affiline)
                {
                     Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                     parent::_redirect("onepagecheckout/index/failure/");
                     return ;
                }
                $customer_credit = $affiline->getData('credit_value');
                $account = Mage::getModel("customer/customer");
              
                $account->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                $account = $account->loadByEmail($this->_main_email); 
                
                if($customer_credit<=0||$grand_total < 0 || $grand_total > $customer_credit)
                {
                    Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                    parent::_redirect("onepagecheckout/index/failure/");
                    return ;
                }
                $affiline->setCreditValue($affiline->getCreditValue()-$grand_total)->save();    
                $account->setCreditValue($account->getCreditValue()+$amount)->save(); 
                   
                ////send money to customer
                $receive =$blockpayment->checkcustomer($pcustomer);
              
                if(!$receive){
                    Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($sub_total,$pcustomer,$message_detail);     
                }else{
                  
                  /// var_dump($sub_total);
                
                   $receive->setCreditValue($receive->getCreditValue()+$sub_total)->save();   
                 
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($receive->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                    "get credit value from '" . $affiline->getName() . "'", "",$sub_total,null,null,null,0, null, null);
                }
             
                ///add trans              
                Mage::getModel('customercredit/transaction')->addTransactionHistory($account->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                "get credit value from '" . $affiline->getName() . "'", "", $amount,null,null,null,null, null, null);
                Mage::getModel('customercredit/transaction')->addTransactionHistory($affiline->getId(),
                Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                " sent " . $grand_total . " credit to " . $account->getEmail(), "", -$grand_total,"",$affiline->getName(),
                0,"",$affiline->getEmail());
               //done
                parent::_redirect('gatewaybodamoney/processing/success/transaction_id/'.$order->getIncrementId());
                    
            }
        }catch (Exception $e){
            Mage::logException($e);
             parent::_redirect("onepagecheckout/index/failure/");
        }
    }
    
    
    /**
     * Action to which the customer will be returned when the payment is made.
     */
    public function successAction()
    {
        $event = Mage::getModel('perfectmoney/event')
                 ->setEventData($this->getRequest()->getParams());
        try {
            $quoteId = $event->successEvent();
            $this->_getCheckout()->setLastSuccessQuoteId($quoteId);
            //////
            $param=$this->getRequest()->getParams();
            $trans_id=$param['transaction_id'];
            $orderOBJ = Mage::getModel('sales/order')->loadByIncrementId($trans_id);
            $orderOBJ->setStatus('complete');
            $orderOBJ->save();
            ////
            parent::_redirect('onepagecheckout/index/success');          
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the customer will be returned if the payment process is
     * cancelled.
     * Cancel order and redirect user to the shopping cart.
     */
    public function cancelAction()
    {
        $event = Mage::getModel('perfectmoney/event')
                 ->setEventData($this->getRequest()->getParams());
        $message = $event->cancelEvent();

        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getPerfectmoneyQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }

        $session->addError($message);
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the transaction details will be posted after the payment
     * process is complete.
     */
    public function statusAction()
    {
        $event = Mage::getModel('perfectmoney/event')
            ->setEventData($this->getRequest()->getParams());
        $message = $event->processStatusEvent();
        $this->getResponse()->setBody($message);
    }

    /**
     * Set redirect into responce. This has to be encapsulated in an JavaScript
     * call to jump out of the iframe.
     *
     * @param string $path
     * @param array $arguments
     */
    protected function _redirect($path, $arguments=array())
    {
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('perfectmoney/redirect')
                ->setRedirectUrl(Mage::getUrl($path, $arguments))
                ->toHtml()
        );
        return $this;
    }
    /**
     * Show orderPlaceRedirect page which contains the Perfectmoney iframe.
     */
    public function paymentAction()
    {
        try {
            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
          
            $order->loadByIncrementId($session->getLastRealOrderId());
        
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('perfectmoney')->__('The customer was redirected to Boda Money GateWay.')
            );
            $order->save();
            $session->getQuote()->setIsActive(false)->save();
           
            //$session->clear();
            $this->loadLayout();
		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Payment'));
		$this->renderLayout();
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    } 
}
