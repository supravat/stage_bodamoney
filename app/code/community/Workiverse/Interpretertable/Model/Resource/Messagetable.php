<?php
class Workiverse_Interpretertable_Model_Resource_Messagetable extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct()
    {
        $this->_init('interpretertable/messagetable', 'interpretertable_id');
    }
}