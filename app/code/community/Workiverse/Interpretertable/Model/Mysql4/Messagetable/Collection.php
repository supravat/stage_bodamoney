<?php
class Workiverse_Interpretertable_Model_Mysql4_Messagetable_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
     public function _construct()
     {
         parent::_construct();
         $this->_init('interpretertable/messagetable');
     }
}