<?php 
 class Workiverse_Interpretertable_Block_Adminhtml_Interpretertable_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
     public function __construct()
     {
          parent::__construct();
          $this->setId('interpretertable_tabs');
          $this->setDestElementId('edit_form');
          $this->setTitle(' Interpretertable  Information');
      }
      protected function _beforeToHtml()
      {
          $this->addTab('form_section', array(
                   'label' => 'Contact Information',
                   'title' => 'Contact Information',
                   'content' => $this->getLayout()
     ->createBlock('interpretertable/adminhtml_interpretertable_edit_tab_form')
     ->toHtml()
         ));
         return parent::_beforeToHtml();
    }
}

?>