<?php
/**
 * Rublon config data model
 *
 * @package   rublon/rublon2Factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Admin model configuration data class
 *
 */
class Rublon_Rublon2Factor_Adminhtml_Model_Config_Data extends Mage_Adminhtml_Model_Config_Data
{
    /**
     * Save config section
     * Handle Rublon module registration action.
     *
     * @return Mage_Adminhtml_Model_Config_Data
     */
    public function save() {
    	$helper = Mage::helper('rublon2factor');
    	$helper->setModule(RublonMagentoModule::ADMIN);
    	if ($this->getSection() == 'rublon2factor_options' AND !$helper->isRublonConfigured()) {
    		$params = Mage::app()->getRequest()->getPost();
			$helper->runRublonRegistration(!empty($params['back']) ? $params['back'] : null);
		} else {
			return parent::save();
		}
	}
	
}
