<?php
/**
 * Rublon Seal logo
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon Seal logo block
 */
class Rublon_Rublon2Factor_Block_Seal extends Mage_Core_Block_Template
{
	/**
	 * Returns the html code of Rublon seal
	 *
	 * @return string
	 */
	protected function _toHtml() {
	
	  $whereIam = Mage::getDesign()->getArea();
	
	  if ($whereIam == 'adminhtml') {
	    $sealCSS = 'position:relative;top:25px;';
    } else {
      $sealCSS = 'margin-top: 30px; float: right;top:25px;';            
    } 
	
		$helper = Mage::helper('rublon2factor');
		return sprintf(<<<'END'
			<div style="%s" id="RublonSeal">
				<a href="https://rublon.com" title="Rublon Two-Factor Authentication" target="_blank"
					style="display:block;width:79px;height:30px;background: url(%s) 0px -336px no-repeat;"></a>
			</div>
			<script type="text/javascript">
			if (document.addEventListener) {
				document.addEventListener("DOMContentLoaded", function() {
					if (document.getElementById("loginForm")) { // admin login
						var seal = document.getElementById("RublonSeal");
						var node = seal.parentNode;
						node.removeChild(seal);
						node.appendChild(seal);
					}
				});
			}
			</script>
END
			, $sealCSS, htmlspecialchars($helper->getSpriteURL()) );
	}
}
 

?>