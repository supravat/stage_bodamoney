<?php

/**
 * Menu of the admin page (backend)
 *
 */
class Rublon_Rublon2Factor_Block_Adminhtml_Menu extends Mage_Adminhtml_Block_Page_Menu {
	
	
	/**
	 * Append Rublon menu item
	 * 
	 * @see Mage_Adminhtml_Block_Page_Menu::getMenuArray()
	 * @return array
	 */
	public function getMenuArray() {
		
		$result = parent::getMenuArray();
		
		$helper = Mage::helper('rublon2factor');
		$helper->setModule(RublonMagentoModule::ADMIN);
		
		if (!$helper->canViewMenuRublon()) {
			return $result;
		}
		
		unset($result[count($result)-1]['last']);
		
		$url = Mage::getModel('adminhtml/url')
			->getUrl('adminhtml/system_account', array(
// 				'section' => 'rublon2factor_options',
				'_cache_secret_key' => true,
			)
		);
		$url .= '#RublonConfig';
		
		$result['rublon'] = array(
			'label' => 'Rublon',
			'sort_order' => '1000',
			'url' => $url,
			'active' => false,
			'level' => 0,
			'last' => 1,
			'children' => array(),
		);
		
		/* if ($helper->isRublonConfigPage()) {
			$result['system']['active'] = false;
			$result['rublon']['active'] = true;
		} */
		
		return $result;
		
	}
	
	
}