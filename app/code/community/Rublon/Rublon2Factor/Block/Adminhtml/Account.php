<?php
/**
 * Rublon edit admin user account form
 *
 * @package   rublon/rublon2Factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Admin account edif form class
 *
 */ 
class Rublon_Rublon2Factor_Block_Adminhtml_Account extends Mage_Adminhtml_Block_System_Account_Edit_Form
{
	 /**
     * Get form HTML
	 * Add Rublon section.
     *
     * @return string
     */
    public function getFormHtml() {
    	
    	$parent = parent::getFormHtml();
    	$content = '';
    	
    	$helper = Mage::helper('rublon2factor');
    	$helper->setModule(RublonMagentoModule::ADMIN);
    	
    	if ($helper->isRublonEnabled() OR $helper->canUserActivateRublon()) {
		    
	    	// Header
	    	$content = '<div id="RublonConfig" class="entry-edit-head"><h4 class="icon-head head-edit-form fieldset-legend">';
	    	$content .= $helper->__("Rublon");
	    	$content .= '</h4></div>';
	    	
	    	// Content
	    	$content .= '<div class="fieldset" style="padding-bottom:2em">';
	    	if ($helper->isRublonEnabled()) {
	   			$content .= $helper->getUserBox();
	    	}
	    	else if (!$helper->isRublonConfigured() AND $helper->canUserActivateRublon()) {
	    		$content .= $helper->getActivationBox();
	    	}
	    	$content .= '</div>';
	    	
    	}
	    	
    	
    	return $parent . $content;
    	
    }
}
