<?php
/**
 * Rublon JS script
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon JS script block
 */
class Rublon_Rublon2Factor_Block_Script extends Mage_Core_Block_Template
{
	/**
	 * Returns the html code of Rublon script
	 *
	 * @return string
	 */
	protected function _toHtml()
	{
		return $this->helper('rublon2factor')->getRublonScript();
	}
}
 

?>