<?php

/**
 * Welcome message for customer
 *
 */
class Rublon_Rublon2Factor_Block_Customer_Welcome extends Mage_Page_Block_Html_Welcome {

	/**
	 * Append something to the welcome message
	 * 
	 * @see Mage_Page_Block_Html_Welcome::_toHtml()
	 * @return string
	 */
	protected function _toHtml() {
		$result = parent::_toHtml();
		if (Mage::isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {
			$helper = Mage::helper('rublon2factor');
			$helper->setModule(RublonMagentoModule::FRONT);
			if ($helper->isRublonSecuredAccount()) {
				$result .= ' '. $helper->getRublonIcon();
			}
		}
		return $result;
	}

}