<?php
/**
 * Customers Rublon settings page block
 *
 * @package   rublon/rublon2Factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Customers Rublon settings page block class
 *
 */
class Rublon_Rublon2Factor_Block_Customer_Settings extends Mage_Core_Block_Template
{
	/**
	 * Returns back url
	 *
	 * @return string
	 */
	public function getBackUrl()
	{
		if ($this->getRefererUrl()) {
			return $this->getRefererUrl();
		}
		return $this->getUrl('customer/account/');
	}
}
 

?>