<?php
/**
 * Rublon helper
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */



/**
 * Magento modules constants wrapper class
 *
 */
class RublonMagentoModule {
	const FRONT = 'front';
	const ADMIN = 'admin';
	const CONNECT = 'connect';
}



/**
 * Main Rublon2Factor for Magento helper class
 */
class Rublon_Rublon2Factor_Helper_Data extends Mage_Core_Helper_Abstract {
	
	/**
	 * Version of the plugin.
	 */
	const PLUGIN_VERSION = '1.0.3';
	
	/**
	 * Rublon API domain
	 * 
	 * @var string
	 */
	const RUBLON_DOMAIN = 'https://code.rublon.com';
	
	/**
	 * Rublon module registration API domain
	 * 
	 * @var string
	 */
	const RUBLON_REGISTER_DOMAIN = 'https://developers.rublon.com';
	
	/**
	 * Technology tag
	 * 
	 * @var string
	 */
	const TECHNOLOGY = 'magento';
	
	/**
	 * Label of the Rublon enable button
	 * 
	 * @var string
	 */
	const TEXT_BUTTON_ENABLE = "Protect your account";
	
	/**
	 * Label of the Rublon disable button
	 * 
	 * @var string
	 */
	const TEXT_BUTTON_DISABLE = "Disable account protection";
	
	/**
	 * ACL path for Rublon configuration permissions
	 * 
	 * @var string
	 */
	const ACL_RUBLON_CONFIG = 'admin/system/config/rublon2factor_options';
	
	/**
	 * URL of the Rublon Seal icon
	 * 
	 * @var string
	 */
	const URL_ICON_SEAL = 'http://rublon.com/img/rublon_seal_79x30.png';
	
	/**
	 * Registration initialize URL
	 * 
	 * @var string
	 */
	const URL_REGISTRATION_INIT = 'rublon/registration/initialize';
	
	/**
	 * Module name to authenticate for
	 *
	 * @var string
	 */
	public $module;
	
	/**
	 * Rublon service instance
	 * 
	 * @var RublonService2Factor
	 */
	private $service = null;
	
	/**
	 * Is Rublon enabled and configured
	 * 
	 * @var bool
	 */
	private $isEnabled;
	
	/**
	 * Magento user ID to authenticate
	 * 
	 * @var int
	 */
	private $authUserId = null;
	
	/**
	 * Instance of the Rublon Issue Notifier
	 * 
	 * @var RublonMagentoIssueNotifier
	 */
	private $issueNotifier;
	
	
	
	/**
	 * Initialize object
	 */
	public function __construct() {
		
		require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'RublonConsumer.php');
		require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'RublonService2Factor.php');
		require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'RublonMagentoIssueNotifier.php');
		
		$this->issueNotifier = new RublonMagentoIssueNotifier($this);
		
		$this->isEnabled = ($this->getSystemToken() AND $this->getSecretKey()
			AND Mage::getStoreConfigFlag('rublon2factor_options/general/rublon_enabled', Mage::app()->getStore()));
		
		$consumer = new RublonConsumer($this->getSystemToken(), $this->getSecretKey());
		$consumer->setDomain(self::RUBLON_DOMAIN);
		$consumer->setLang($this->getLang());
		$this->service = new RublonService2Factor($consumer);
		
	}
	
	/**
	 * Get module's technology tag
	 * 
	 * @return string
	 */
	public function getTechnology() {
		return self::TECHNOLOGY;
	}

	
	
	/**
	 * Check whether the current user can activate Rublon
	 * 
	 * @return boolean
	 */
	public function canUserActivateRublon() {
		return Mage::getSingleton('admin/session')->isAllowed(self::ACL_RUBLON_CONFIG);
	}
	
	
	/**
	 * Check whether the current user can view Rublon item in admin's main menu
	 * 
	 * @return boolean
	 */
	public function canViewMenuRublon() {
		$canViewMyAccount = Mage::getSingleton('admin/session')->isAllowed('admin/system/myaccount');
		return ($canViewMyAccount AND ($this->isRublonConfigured() OR $this->canUserActivateRublon()));
	}
	
	
	/**
	 * Initializes Rublon registration.
	 */
	public function initRegistration() {
		require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'RublonConsumerRegistration' . DS . 'RublonConsumerRegistration.php');
		$this->registration = new RublonConsumerRegistration();
		$this->registration->setDomain(self::RUBLON_REGISTER_DOMAIN);
	}
	
	/**
	 * Start Rublon registration process.
	 * 
	 * @param string $back Back to the My Account or Configuration page
	 */
	public function runRublonRegistration($back = null) {
		if (!empty($back)) {
			Mage::getSingleton('core/session')->setReturnBack($back);
		}
		require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'RublonConsumerRegistration' . DS . 'RublonConsumerRegistration.php');
		$this->consumerRegistrationAction(RublonConsumerRegistration::ACTION_INITIALIZE);
	}
	
	/**
	 * Handle action of the Rublon registration process.
	 * 
	 * @param string $action
	 * @return void
	 */
	public function consumerRegistrationAction($action) {
		$this->initRegistration();
		if (!$this->isRublonEnabled()) {
			$this->registration->action($action);
		} else {
			$this->addError(RublonConsumerRegistration::TEXT_FINAL_ERROR, array(
				'method' => __METHOD__,
				'file' => __FILE__,
			));
			$this->returnToPage($this->getAccountUrl());
		}
	}
	
	/**
	 * Wrap JS into DOMContentLoaded event
	 *
	 * @param string $content
	 * @return string
	 */
	public function getScriptOnload($content) {
		return '<script type="text/javascript">if (document.addEventListener) {
	    		document.addEventListener("DOMContentLoaded", function() {
	    			'. $content .'
	    		});
			}
			</script>';
	}
	
	/**
	 * Checks whether Rublon is enabled and configured.
	 *
	 * @return boolean
	 */
	public function isRublonEnabled() {
		return $this->isEnabled;
	}

	/**
	 * Checks whether Rublon is configured.
	 *
	 * @return boolean
	 */	
	public function isRublonConfigured() {
		return ($this->getSystemToken() AND $this->getSecretKey());
	}


	/**
	 * Get URL called before authentication
	 * 
	 * @return string
	 */
	public function getBeforeAuthUrl() {
		switch ($this->getModule()) {
			case RublonMagentoModule::FRONT:
				$session = Mage::getSingleton('customer/session');
				$helper = Mage::helper('customer');
				break;
			default:
				$session = Mage::getSingleton('admin/session');
				$helper = Mage::helper('admin');
		}
		if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
			return $this->getAfterLoginUrl();
		} else {
			return $session->getBeforeAuthUrl();
		}
	}
	
	
	/**
	 * Get "My account" URL
	 * 
	 * @return string
	 */
	public function getAccountUrl() {
		switch ($this->getModule()) {
			case RublonMagentoModule::ADMIN:
				return Mage::helper("adminhtml")->getUrl('adminhtml/system_account');
			case RublonMagentoModule::CONNECT:
				return $this->getMagentoConnectUrl();
			default:
				return Mage::getUrl('rublon/customer/settings');
		}
	}
	
	
	/**
	 * Get URL that should be called after login
	 * 
	 * @return string
	 */
	public function getAfterLoginUrl() {
		switch ($this->getModule()) {
			case RublonMagentoModule::ADMIN:
				return Mage::helper("adminhtml")->getUrl('adminhtml');
			case RublonMagentoModule::CONNECT:
				return $this->getMagentoConnectUrl();
			default:
				return Mage::getUrl('customer/account');
		}
	}
	
	
	
	/**
	 * Login an user with given id, as a current user.
	 * 
	 * @return void
	 */
	public function loginUser($userId) {
		if ($this->isAdmin()) {
			$this->loginAdmin($userId);
		} else {
			$this->loginCustomer($userId);
		}
	}
	
	/**
	 * Login an user with given id, as a current administrator.
	 * 
	 * @return void
	 */
	private function loginAdmin($userId) {
		$session = $this->getUserSession();
		$model = $this->getUserModel();
		$session->setUser($model->load($userId));
	}

	/**
	 * Login an user with given id, as a current customer.
	 * 
	 * @return void
	 */
	private function loginCustomer($userId) {
		$session = $this->getUserSession();
		$model = $this->getUserModel();
		$session->setCustomer($model->load($userId));
	}

	/**
	 * Perform second factor authentication for an administrator
	 * login action.
	 * 
	 * @return void
	 */
	public function authenticateAdmin($observer) {
		$this->authUserId = $observer->getUser()->getId();
		$this->authenticateSecondFactor();
	}

	/**
	 * Perform second factor authentication for a customer
	 * login action.
	 * 
	 * @return void
	 */	
	public function authenticateCustomer($observer) {
		$this->authUserId = $observer->getCustomer()->getId();
		$this->authenticateSecondFactor();
	}
	
	/**
	 * Start authenticating an user (administrator or customer) using
	 * second factor service.
	 * 
	 * @return void
	 */
	public function authenticateSecondFactor() {
		require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'core' . DS . 'RublonAuthParams.php');

		$customURIParam = http_build_query(array(
			'module' => $this->getModule(),
		));
		
		$authParams = new RublonAuthParams($this->service);
		$authParams->setConsumerParam('module', $this->getModule());
		$authParams->setConsumerParam('auth_user_id', $this->getAuthUserId());
		$authParams->setConsumerParam('action', RublonAuthParams::ACTION_FLAG_LOGIN);
		$authParams->setConsumerParam('customURIParam', $customURIParam);
// 		$authParams->setConsumerParam('returnUrl', $this->getBeforeAuthUrl());

		$this->clearLoggedUser();

		$this->service->initAuthorization($this->getUserProfileId($this->getAuthUserId()), $authParams);
	}


	/**
	 * Return proper session, according to the login process 
	 * side (administrator or customer);
	 * 
	 * @return Mage_Admin_Model_Session/Mage_Customer_Model_Session
	 */	
	public function getUserSession() {
		return ($this->isAdmin()) ? Mage::getSingleton('admin/session') : Mage::getSingleton('customer/session');
	}

	/**
	 * Return proper user model, according to the login process 
	 * side (administrator or customer);
	 * 
	 * @return Mage_Admin_Model_User/Mage_Customer_Model_Customer
	 */
	public function getUserModel() {
		return ($this->isAdmin()) ? Mage::getSingleton('admin/user') : Mage::getSingleton('customer/customer');
	}

	/**
	 * Unset currently logged user after authenticating with
	 * username/password, to allow second factor authentication.
	 */
	private function clearLoggedUser() {
		$session = $this->getUserSession();
		if ($this->isAdmin()) {
			$session->setUser($this->getUserModel());
		} else {
			$session->setCustomer($this->getUserModel());
		}
	}

	/**
	 * Answer whether it is on administrator or customer login action.
	 * 
	 * @return boolean
	 */
	public function isAdmin() {
		return ($this->getModule() != RublonMagentoModule::FRONT);
	}

	
	/**
	 * Get Magento Connect URL
	 * 
	 * @return string
	 */
	public function getMagentoConnectUrl() {
		return Mage::getBaseUrl('web') . 'downloader/';
	}

	
	/**
	 * Return an id of user, which need to be authenticated by Rublon second factor
	 * 
	 * @return void
	 */
	public function getAuthUserId() {
		return $this->authUserId;
	}

	/**
	 * Returns the html code of Rublon script
	 *
	 * @return string
	 */
	public function getRublonScript() {
		if ($this->isRublonEnabled()) {
			require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'HTML' . DS . 'RublonConsumerScript.php');
			return new RublonConsumerScript($this->service);
		}
	}

	/**
	 * Returns the html code of Rublon enable/disable second factor
	 * authentication button.
	 *
	 * @return string
	 */
	public function getRublonButton() {
		if ($this->isRublonEnabled()) {
			require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'HTML' . DS . 'RublonButton.php');
			
			$this->setModule($this->checkIsAdmin() ? RublonMagentoModule::ADMIN : RublonMagentoModule::FRONT);
			if ($this->isRublonSecuredAccount()) {
				$button = $this->service->createButtonDisable($this->__(self::TEXT_BUTTON_DISABLE), $this->getUserProfileId($this->getLoggedUser()->getId()));
			} else {
				$button = $this->service->createButtonEnable($this->__(self::TEXT_BUTTON_ENABLE));
			}
			
			$currentURL = $this->getCurrentURL();
			$customURIParam = http_build_query(array(
				'module' => $this->getModule(),
				'url' => preg_replace('#^http(s)?://[^/]+/#', '/', $currentURL),
			));
			
			$authParams = $button->getAuthParams();
			$authParams->setConsumerParam('module', $this->getModule());
			$authParams->setConsumerParam('customURIParam', $customURIParam);
			$authParams->setOriginUrl($currentURL);
			
			return $button;
			
		}
	}
	
	/**
	 * Get current URL
	 *
	 * @return string
	 */
	public function getCurrentURL() {
		return Mage::helper('core/url')->getCurrentUrl();
	}
	
	
	/**
	 * Convert local relative URL into absolute
	 * 
	 * @param string $url
	 * @return string
	 */
	public function getAbsoluteURL($url) {
		if (!parse_url($url, PHP_URL_HOST)) {
			$request = Mage::app()->getRequest();
			$port = $request->getServer('SERVER_PORT');
			if ($port) {
				$defaultPorts = array(
					Mage_Core_Controller_Request_Http::DEFAULT_HTTP_PORT,
					Mage_Core_Controller_Request_Http::DEFAULT_HTTPS_PORT
				);
				$port = (in_array($port, $defaultPorts)) ? '' : ':' . $port;
			}
			return $request->getScheme() . '://' . $request->getHttpHost() . $port . $url;
		}
	}
	
	
	/**
	 * Check whether current page is a Rublon configuration page
	 * 
	 * @return boolean
	 */
	public function isRublonConfigPage() {
		$current = parse_url($this->getCurrentURL(), PHP_URL_PATH);
		$url = parse_url($this->getRublonConfigPageURL(), PHP_URL_PATH);
		$pos = strpos($current, 'rublon2factor_options');
		return ($pos !== false AND substr($url, $pos) == substr($current, $pos));
	}
	
	
	/**
	 * Get URL to Rublon Configuration page
	 * 
	 * @return string
	 */
	public function getRublonConfigPageURL() {
		return Mage::helper("adminhtml")->
			getUrl('adminhtml/system_config/edit', array(
				'section' => 'rublon2factor_options',
			)
		);
	}
	
	/**
	 * Add error message and notify Rublon team if needed
	 * 
	 * @param mixed $error
	 * @param string $notifierOptions
	 * @return void
	 */
	public function addError($error, $notifierOptions = array()) {
		
		// Prepare error message
		if (is_object($error) AND $error instanceof Exception) {
			$message = $error->getMessage();
		} else {
			$message = $this->__((string)$error);
			if (!strlen($message)) {
				$message = $this->__('An error has occurred.');
			}
		}
		
		// Add flash message
		Mage::getSingleton('core/session')->addError($message);
		
		// Send notify
		if (!empty($notifierOptions)) {
			$this->issueNotifier->notify($error, $notifierOptions);
		}
		
	}


	/**
	 * Return currently logged user.
	 * 
	 * @return object
	 */
	public function getLoggedUser() {
		$session = $this->getUserSession();
		return ($this->isAdmin()) ? $session->getUser() : $session->getCustomer();
	}

	/**
	 * Checks whether currently logged user account is secured by Rublon
	 * second factor.
	 *
	 * @return boolean
	 */
	public function isRublonSecuredAccount() {
		return $this->isRublonSecuredUser();
	}
	
	
	/**
	 * Get current user's ID
	 * 
	 * @return int
	 */
	public function getUserId() {
		$currentUser = $this->getLoggedUser();
		if ($currentUser) {
			return $currentUser->getId();
		}
	}

	/**
	 * Checks whether an user with given id is secured by Rublon
	 * second factor.
	 *
	 * @param int $userId
	 * @return boolean
	 */
	public function isRublonSecuredUser($userId = null) {
		return ($this->isRublonConfigured()
			AND $this->isRublonEnabled()
			AND $this->getUserProfileId($userId));
	}
	
	
	/**
	 * Return rublon_profile_id for an user with given id.
	 * 
	 * @return int
	 */
	public function getUserProfileId($userId = null) {
		if (empty($userId)) {
			$userId = $this->getUserId();
		}
		if ($userId) {
			$model = $this->getRublonUserModel();
			$model->load($userId);
			return $model->getRublonProfileId();
		}
	}
	

	/**
	 * Connect given user with Rublon using profile id.
	 * 
	 * @param mixed $user (can be customer or administrator)
	 * @param int $profileId
	 */
	public function connectSecondFactor($user, $profileId) {
		$model = $this->getRublonUserModel();
		$model->setId($user->getId());
		$model->setRublonProfileId($profileId);
		$model->save();
	}
	
	/**
	 * Disconnect given user from Rublon using profile id.
	 * 
	 * @param mixed $user (can be customer or administrator)
	 * @param int $profileId
	 */
	public function disconnectSecondFactor($user, $profileId){
		$model = $this->getRublonUserModel();
		$model->setId($user->getId());
		$model->setRublonProfileId($profileId);
		$model->delete();
	}

	/**
	 * Return a proper Rublon resource object according to the login action side.
	 * 
	 * @return mixed Rublon_Rublon2Factor_Model_Resource_Customer/Rublon_Rublon2Factor_Model_Resource_User
	 */
	private function getRublonUserModel() {
		return ($this->isAdmin()) ? Mage::getModel('rublon2factor/user') : Mage::getModel('rublon2factor/customer');
	}
	
	/**
	 * Redirect to a page with given url.
	 * 
	 * @param string $returnUrl
	 */
	public function returnToPage($returnUrl) {
		header('location: '. $returnUrl);
		exit;
	}

	/**
	 * Check whether currently logged user is at administrator side.
	 * 
	 * @return boolean
	 */
	public function checkIsAdmin() {
		return Mage::getSingleton('admin/session')->isLoggedIn();
	}
	
	
	
	
	/**
	 * Get system token
	 * 
	 * @return string|NULL
	 */
	public function getSystemToken() {
		return Mage::getStoreConfig('rublon2factor_options/general/rublon_system_token', Mage::app()->getStore());
	}
	
	/**
	 * Get secret key
	 * 
	 * @return string|NULL
	 */
	public function getSecretKey() {
		return Mage::getStoreConfig('rublon2factor_options/general/rublon_secret_key', Mage::app()->getStore());
	}
	
	/**
	 * Get current language code
	 * 
	 * @return string
	 */
	public function getLang() {
		return substr(Mage::app()->getLocale()->getLocaleCode(), 0, 2);
	}
	
	
	/**
	 * Get Magento module name
	 * 
	 * @return string
	 */
	public function getModule() {
		return $this->module;
	}
	
	
	/**
	 * Set Magento module name
	 * 
	 * @param string $module
	 * @return Rublon_Rublon2Factor_Helper_Data
	 */
	public function setModule($module) {
		$this->module = $module;
		return $this;
	}
	
	
	/**
	 * Check whether this is Magento Connect
	 * 
	 * @return boolean
	 */
	public function isMagentoConnect() {
		return ($this->getModule() == RublonMagentoModule::CONNECT);
	}
	
	
	/**
	 * Get user box for Rublon settings
	 *
	 * @return string
	 */
	public function getUserBox() {
		
		$result = $this->getRublonScript();
		
		if ($this->isRublonEnabled()) {
			$result .= '<p>';
			if ($this->isRublonSecuredAccount()) {
				$result .= $this->__("<strong>Your account is protected by Rublon</strong>. It can be accessed from your Trusted Devices only.");
			} else {
				$result .= $this->__("Since your account is protected by a password only, it can be accessed from any device in the world. Rublon protects your account from sign ins from unknown devices, even if your password gets stolen.");
			}
			$result .= ' '. $this->__("Learn more at <a href='http://rublon.com' target='_blank'>www.rublon.com</a>.");
			$result .= '</p>
				<div class="rublon-button-content">';
					$result .= $this->getRublonButton();
					$result .= $this->getAppInfoBox(true);
			$result .= '</div><div style="clear:both"></div>';
			
		}
		
		return $result;
		
	}
	
	
	
	/**
	 * Get Rublon module activation box
	 *
	 * @return string
	 */
	public function getActivationBox() {
		return '<div style="padding:1em 2em;width:500px;">
				<p style="background:#ffffe0;padding:0.5em 1em;border:solid 1px #e6db55;">'. $this->__('Before any of your users will be able to use Rublon, you or another administrator needs to protect his account first.') .'</p>
				<p style="margin:1em 0;">'. $this->__("Since your account is protected by a password only, it can be accessed from any device in the world. Rublon protects your account from sign ins from unknown devices, even if your password gets stolen.") .'
					'. $this->__("Learn more at <a href='http://rublon.com' target='_blank'>www.rublon.com</a>.") .'</p>
				<div class="rublon-button-content">'.
					$this->getActivationButton() .
					$this->getAppInfoBox(false) .
				'</div>
				<div style="clear:both;"></div>
			</div>';
	}
	
	
	/**
	 * Returns registration initialize URL
	 * 
	 * @return string
	 */
	public function getRegistrationInitURL() {
		return Mage::helper("adminhtml")->getUrl(self::URL_REGISTRATION_INIT);
	}
	
	
	/**
	 * Get Rublon module activation button
	 *
	 * @return string
	 */
	public function getActivationButton() {
		$rublonDomain = htmlspecialchars(self::RUBLON_DOMAIN);
		$text = htmlspecialchars($this->__('Protect your account'));
		$button = '<a href="'. htmlspecialchars($this->getRegistrationInitURL()) .'" style="width:auto;height:30px;background: url(' . $rublonDomain
			. '/public/img/buttons/rublon-btn-bg-dark-medium.png) left top repeat-x;font-weight:bold;font-size:13px;font-family:&quot;Helvetica&quot;'
			. ' &quot;Nimbus Sans&quot; &quot;Arial&quot; &quot;sans-serif&quot;;color:#ffffff;text-decoration:none;display:inline-block;position:relative;padding:0 8px;'
			. 'margin:0 8px;"><span style="display:block;width:33px;height:30px;background:url(' . $rublonDomain
			. '/public/img/buttons/rublon-btn-bg-begin-dark-medium.png) left top no-repeat;position:absolute;left:-8px;top:0;padding:0;margin:0;"></span><span'
			. ' style="display:block;width:22px;height:30px;background:url(' . $rublonDomain . '/public/img/buttons/rublon-btn-bg-end-dark-medium.png)'
			. ' left top no-repeat;position:absolute;right:-8px;top:0;padding:0;margin:0;"></span><span style="display:block;margin:0;padding:5px 22px 0 33px;font-weight:bold;'
			. 'line-height:17px;height:17px;font-size:13px;font-family: Helvetica, &quot;Nimbus Sans&quot;, Arial, sans-serif;color:#ffffff;white-space:nowrap;'
			. '">'. $text . '</span></a>';
		return $button;
	}
	
	

	/**
	 * Displays the app info box (to be displayed under the Rublon buttons when a Trusted Device is not present)
	 *
	 * @return string
	 */
	public function getAppInfoBox($hidden = true) {
		$lang = $this->getLang();
		$infoBox = sprintf('<a class="rublon-app-info-boxlink" href="http://rublon.com%s/get" target="_blank">', (($lang != 'en') ? ('/' . $lang) : ''))
		. '<div class="rublon-app-info-box"' . ((!$hidden) ? ' style="display: block;"' : '') . '>';
		$infoBox .= '<p class="rublon-app-info-text"><strong>' . $this->__('Rublon mobile app required:') . '</strong></p>';
		$infoBox .= '<div class="rublon-app-info-icons"></div>';
		$infoBox .= '<p class="rublon-app-info-link"><strong>' . '' . $this->__('Free Download') . '</strong></p>';
		$infoBox .= '</div></a>';
		if ($hidden AND !$this->isRublonSecuredAccount()) {
			$infoBox .= '<script type="text/javascript">//<![CDATA[
				window.RublonAppInfo = function() {
					if (window.RublonConfigure && !window.RublonConfigure.trustedDevices) {
					var elements = document.querySelectorAll(".rublon-app-info-box");
					for (var i = 0; i < elements.length; i++)
						elements[i].style.display = "block";
					}
				}
				
				if (window.RublonConfigure) {
					RublonAppInfo();
				} else {
					document.addListener("RublonJSSDKInit", RublonAppInfo);
				}
			//]]></script>';
		}
		
		return $infoBox . '<style type="text/css">
				.rublon-button-content {
					margin: 1em 0 0;
					padding: 0.5em 0;
					background: none;
					font: normal 12px Helvetica, Arial;
					text-align: center;
					float: left;
				}
		
				.rublon-app-info-boxlink {
					text-decoration: none !important;
				}
		
				.rublon-app-info-box {
					width: 195px;
					height: 101px;
					margin: 5px auto 0;
					background-image: url("'. self::RUBLON_DOMAIN .'/public/img/app_info_box_bg.png");
					background-repeat: no-repeat;
					text-align: center;
					display: none;
				}
		
				.rublon-app-info-box p.rublon-app-info-text {
					padding-top: 20px;
					margin: 0;
					text-align: center;
					color: #000;
				}
		
				.rublon-app-info-box div.rublon-app-info-icons {
					text-align: center;
					width: 91px;
					height: 18px;
					margin: 10px auto 10px;
					padding: 0;
					background-image: url("'. self::RUBLON_DOMAIN .'/public/img/app_info_box_icons.png");
					background-repeat: no-repeat;
				}
		
				.rublon-app-info-box p.rublon-app-info-link {
					padding: 0;
					margin: 0;
					text-align: center;
					text-decoration: underline;
					font-size: 1.1em;
				}
			</style>';
	
	}
	
	
	/**
	 * Get HTML code with Rublon icon for welcome message
	 * 
	 * @return string
	 */
	public function getRublonIcon() {
		return sprintf('<span title="%s"
			style="width:16px;height:16px;background:url(%s) -219px -378px no-repeat;vertical-align:middle;display:inline-block;"
			class="rublon-protected-icon"></span>',
			htmlspecialchars($this->__('Your account is protected by Rublon')),
			$this->getSpriteURL()
		);
	}
	
	
	/**
	 * Returns Rublon API domain
	 * 
	 * @return string
	 */
	public function getRublonDomain() {
		return self::RUBLON_DOMAIN;
	}
	
	
	/**
	 * Get Rublon Seal icon URL
	 * 
	 * @return string
	 */
	public function getSpriteURL() {
		return self::RUBLON_DOMAIN . '/public/img/sprite.png';
	}
	
	
	
}




