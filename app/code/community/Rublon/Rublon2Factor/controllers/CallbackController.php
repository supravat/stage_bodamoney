<?php
/**
 * Rublon callback controller
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon Callback for frontend and backend
 * 
 * Magento Connect has own callback in Observer - this callback will redirect to proper URL if needed.
 */
class Rublon_Rublon2Factor_CallbackController extends Mage_Core_Controller_Front_Action {
	
	
	/**
	 * Rublon Callback instance
	 * 
	 * @var RublonCallback
	 */
	protected $callback;
	

	/**
	 * Pre-dispatch
	 */
	public function preDispatch() {
		
		$helper = Mage::helper('rublon2factor');
		if ($helper->isRublonEnabled()) {
			
			require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'RublonCallback.php');
			
			$rublonCallbackParams = $this -> getRequest() -> getParams();
			$state = isset($rublonCallbackParams['state']) ? $rublonCallbackParams['state'] : '';
			$token = isset($rublonCallbackParams['token']) ? $rublonCallbackParams['token'] : null;
			$windowType = 'window';
			
			// Extract custom params
			$module = 'front';
			$customURIParam = null;
			if (!empty($_GET['custom'])) {
				parse_str($_GET['custom'], $customURIParam);
				if (isset($customURIParam['module'])) {
					$module = $customURIParam['module'];
				}
				if (isset($customURIParam['url'])) {
					$returnURL = $helper->getAbsoluteURL($customURIParam['url']);
					if (!$returnURL) $returnURL = $helper->getAccountUrl();
				}
			}
			
			// If Magento Connect - redirect to different URL and there get credentials (because of session encapsulation)
			if ($module == RublonMagentoModule::CONNECT) {
				$query = compact('token', 'state');
				$query['rublon'] = 'callback';
				if (isset($_GET['custom'])) {
					$query['custom'] = $_GET['custom'];
				}
				$url = $helper->getMagentoConnectUrl() .'?'. http_build_query($query);
				$helper->returnToPage($url);
			}
			
			// Set module and session namespace
			$helper->setModule($module);
			$this->_sessionNamespace = ($helper->isAdmin()) ? 'adminhtml' : 'frontend';
			parent::preDispatch();
			
			// Run callback
			try {
				$this->callback = new RublonCallback($this, $state, $token);
				$this->callback->setCustomURIParam($customURIParam);
				$this->callback->run();
			} catch(RublonException $e) {
				$helper->addError($e, array('method' => __METHOD__, 'file' => __FILE__));
				$helper->returnToPage($helper->getAccountUrl());
			}
			
		}
		
	}

	/**
	 * Redirect after Rublon Callback
	 */
	public function indexAction() {
		$helper = Mage::helper('rublon2factor');
		
		if (!empty($this->callback) AND is_object($this->callback)) {
			$url = $this->callback->getReturnUrl();
		} else {
			$url = $helper->getAccountUrl();
		}
		$helper->returnToPage($url);
	}
	
	
	/**
	 * Set session namespace and call parent preDispatch method
	 * 
	 * Called from Rublon Callback instance to initialize admin or frontend session.
	 * 
	 * @return void
	 */
	public function setSessionNamespace() {
		if (Mage::helper('rublon2factor')->isAdmin()) {
			$this->_sessionNamespace = 'admin';
		} else {
			$this->_sessionNamespace = 'frontend';
		}
		parent::preDispatch();
	}
	
	
	
}
