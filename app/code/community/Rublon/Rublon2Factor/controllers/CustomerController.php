<?php
/**
 * Rublon customer account controller
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon customer controller class
 */
class Rublon_Rublon2Factor_CustomerController extends Mage_Core_Controller_Front_Action{
	
	/**
	 * Check customer authentication
	 */
	public function preDispatch()
	{
		parent::preDispatch();
		$action = $this->getRequest()->getActionName();
		$loginUrl = Mage::helper('customer')->getLoginUrl();
	
		if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
			$this->setFlag('', self::FLAG_NO_DISPATCH, true);
		}
	}
	
	/**
	 * rublon/customer/settings GET controller
	 */
	public function settingsAction(){
		$this->loadLayout();
		$this->_initLayoutMessages('customer/session');
		if ($block = $this->getLayout()->getBlock('rublon2factor_customer_settings')) {
			$block->setRefererUrl($this->_getRefererUrl());
		}
		$headBlock = $this->getLayout()->getBlock('head');
		if ($headBlock) {
			$headBlock->setTitle(Mage::helper('rublon2factor')->__("Rublon"));
		}
		$this->renderLayout();
	}
}




?>