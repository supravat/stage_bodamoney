<?php
/**
 * Rublon customer resource model
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon customer resource model class
 */
class Rublon_Rublon2Factor_Model_Resource_Customer extends Mage_Core_Model_Resource_Db_Abstract
{
	/**
	 * Default constructor
	 *
	 */
	protected function _construct()
	{
		//Set table and primary key
		$this->_init('rublon2factor/rublon_customer', 'customer_id');
		
		//Set primary key as not auto increment - is is also foreign key to customer/entity table
		$this->_isPkAutoIncrement = false;
	}
}
 

?>