<?php
/**
 * Rublon customer model
 * 
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon customer model class
 * 
 * This entity contains all customer scoped rublon data including profile ID.
 */
class Rublon_Rublon2Factor_Model_Customer extends Mage_Core_Model_Abstract
{
	/**
	 * Default constructor
	 */
	protected function _construct()
	{
		$this->_init('rublon2factor/customer');
	}

	/**
	 * Loads by Rublon profile id
	 *
	 * @param int $rublonProfileId
	 * @return Rublon_Rublon2Factor_Model_Customer
	 */
	public function loadByRublonProfileId($rublonProfileId)
	{
		return $this->load($rublonProfileId,'rublon_profile_id');
	}
	
}

?>