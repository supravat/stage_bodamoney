<?php
/**
 * Rublon api settings validator/backend model
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon settings value model class
 */
class Rublon_Rublon2Factor_Model_Configvalue extends Mage_Core_Model_Config_Data
{	
	/**
	 * Handle action after configuration save. 
	 * Checks whether configuration value is empty and sets appropriate warning.
	 */
	public function _afterSave()
	{
		$val = $this->getValue();
		if( empty($val) )
		{
			$warning = Mage::getModel('core/message')->error(Mage::helper('rublon2factor')->__("If you leave &quot;system token&quot; or &quot;secret key&quot; empty this disables Rublon.") );
			Mage::getSingleton('core/session')->addUniqueMessages($warning);
		}
		return parent::_afterSave();
	}
	
}

?>