<?php
/**
 * Rublon2Factor for magento event listener
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

/**
 * Rublon events observer class
 */
class Rublon_Rublon2Factor_Model_Observer extends Mage_Core_Model_Abstract {

	/**
	 * Implements customer_login hook to apply customer second 
	 * factor authentication, if it is enabled.
	 */
	public function customerLogin($observer) {
		if ($customerId = $observer->getCustomer()->getId()) {
			$helper = Mage::helper('rublon2factor');
			$helper->setModule(RublonMagentoModule::FRONT);
			if ($helper->isRublonConfigured() AND $helper->isRublonEnabled() AND $helper->isRublonSecuredUser($customerId)) {
				$helper->authenticateCustomer($observer);
			}
		}
	}
	
	/**
	 * After admin login
	 * 
	 * @param unknown $observer
	 */
	public function afterAdminLogin($observer) {
		if ($observer->getResult() AND $userId = $observer->getUser()->getId()) { // successful login
			$connect = (strpos(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 'downloader') !== false);
			$helper = Mage::helper('rublon2factor');
			$helper->setModule($connect ? RublonMagentoModule::CONNECT : RublonMagentoModule::ADMIN);
			if ($helper->isRublonEnabled()  AND $helper->isRublonSecuredUser($userId)) {
				$helper->authenticateAdmin($observer);
			}
		}
	}
	
	
	/**
	 * Magento Connect custom callback URL
	 * 
	 * Because Magento Connect has own session and cookie restricted to directory "/downloader"
	 * the Rublon callback must be called in this location.
	 * 
	 * @param object $observer
	 */
	public function controllerInit($observer) {
		$helper = Mage::helper('rublon2factor');
		if ($helper->isRublonEnabled() AND isset($_GET['rublon']) AND $_GET['rublon'] == 'callback') {
			
			require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'RublonCallback.php');
			
			$helper->setModule(RublonMagentoModule::CONNECT);
			
			$state = isset($_GET['state']) ? $_GET['state'] : '';
			$token = isset($_GET['token']) ? $_GET['token'] : null;
			
			try {
				$callback = new RublonCallback($this, $state, $token);
				$callback->run();
				$helper->getUserSession()->refreshAcl();
				$url = $callback->getReturnUrl();
				$helper->returnToPage($url);
			} catch (RublonException $e) {
				$helper->addError($e, array('method' => __METHOD__, 'file' => __FILE__));
				$helper->returnToPage($helper->getAccountUrl());
			}
			
		}
	}
	

}
?>