<?php

require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'RublonConsumer.php');
require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'Rublon' . DS . 'RublonService2Factor.php');
require_once (Mage::getBaseDir('code') . DS . 'community' . DS . 'Rublon' . DS . 'Rublon2Factor' . DS . 'lib' . DS . 'RublonConsumerRegistration' . DS .'RublonConsumerRegistrationTemplate.php');

/**
 * Implementation of the Rublon Consumer Registration Template
 *
 */
class RublonConsumerRegistration extends RublonConsumerRegistrationTemplate {
	
	/**
	 * Communication URL
	 * 
	 * @var string
	 */
	const URL_COMMUNICATION = 'rublon/registration/callback';
	
	/**
	 * Message on successful registration
	 * 
	 * @var string
	 */
	const TEXT_FINAL_SUCCESS = "Thank you! Now all of your users can protect their accounts with Rublon.";
	
	/**
	 * Message on error
	 * 
	 * @var string
	 */
	const TEXT_FINAL_ERROR = "Rublon activation failed. Please try again. Should the error occur again, contact us at <a href='mailto:support@rublon.com'>support@rublon.com</a>.";
	
	/**
	 * Message on plugin outdated error.
	 */
	const TEXT_PLUGIN_OUTDATED = "Rublon activation failed: this version of the Rublon extension is already outdated. Please install the latest version.";

	
	/**
	 * Rublon helper instance
	 * 
	 * @var Rublon_Rublon2Factor_Helper_Data
	 */
	protected $helper;
	
	
	/**
	 * Constructor
	 */
	function __construct() {
		$this->helper = Mage::helper('rublon2factor');
	}
	
	
	/**
	 * Send string to the standard output
	 * 
	 * If your system have an ususual way to echo strings, override this method in a subclass.
	 * 
	 * @param string $str
	 * @return void
	 */
	protected function _echo($str) {
		flush();
		echo $str;
		exit;
	}

	/**
	 * Get POST parameter
	 * 
	 * If your system have an ususual way to get POST parameters, override this method in a subclass.
	 * 
	 * @param string $name
	 * @return mixed
	 */
	protected function _post($name) {
		$params = Mage::app()->getRequest()->getPost();
		return (isset($params[$name]) ? $params[$name] : NULL);
	}

	/**
	 * Final method when registration process was successful
	 *
	 * Clean some variables.
	 * Update Rublon using 'systemToken' and 'secretKey' from
	 * successfully registered project and to Rublon setting
	 * page
	 *
	 * @return void
	 */
	protected function finalSuccess() {
		parent::finalSuccess();
		$this->updateRublonSettings();
		$message = $this->helper->__(self::TEXT_FINAL_SUCCESS);
		Mage::getSingleton('core/session')->addSuccess($message);
		$this->returnBack();
	}
	
	
	/**
	 * Return back to the activation page
	 * 
	 * @return void
	 */
	protected function returnBack() {
		$back = Mage::getSingleton('core/session')->getReturnBack();
		if ($back == 'system_config') {
			$url = $this->helper->getRublonConfigPageURL();
		} else {
			$url = $this->helper->getAccountUrl();
		}
		$this->_redirect($url);
	}
	
	/**
	 * Final method when registration process was failed
	 *
	 * Clean some variables.
	 * Set an error message, an redirect to Rublon setting
	 * page
	 *
	 * @param string $msg
	 * @return void
	 */
	protected function finalError($msg = NULL) {

		parent::finalError($msg);
		
		if (strpos($this->_get('error'), 'PLUGIN_OUTDATED') !== false) {
			$error = self::TEXT_PLUGIN_OUTDATED;
			$msg = $this->_get('error_msg');
		} else {
			$error = self::TEXT_FINAL_ERROR;
		}
		
		$this->helper->addError($error, array(
			'method' => __METHOD__,
			'file' => __FILE__,
			'error' => $msg,
			
		));
		$this->returnBack();
		
	}
	
	/**
	 * Check whether user authenticated in current session can 
	 * perform administrative operations such as registering 
	 * the Rublon module.
	 *
	 * @return bool
	 */
	protected function isUserAuthorized() {
		$session = Mage::getSingleton('admin/session');
		return $session->isLoggedIn();
	}

	/**
	 * Returns local-stored system token or NULL if empty.
	 * 
	 * @return string/null
	 */
	public function getSystemToken() {
		return $this->_getConfigData('rublon_system_token');
	}
	
	/**
	 * Save system token to the local storage
	 * 
	 * Returns true/false on success/failure.
	 *
	 * @param string $systemToken
	 * @return bool
	 */
	protected function saveSystemToken($systemToken) {
		return $this->_saveConfigData('rublon_system_token', $systemToken);
	}
	
	/**
	 * Return local-stored secret key or NULL if empty.
	 * 
	 * @return string/null
	 */
	protected function getSecretKey() {
		return $this->_getConfigData('rublon_secret_key');
	}
	
	/**
	 * Save secret key to the local storage
	 *
	 * Returns true/false on success/failure.
	 *
	 * @param string $secretKey
	 * @return bool
	*/
	protected function saveSecretKey($secretKey) {
		return $this->_saveConfigData('rublon_secret_key', $secretKey);
	}
	
	/**
	 * Returns local-stored temporary key or NULL if empty.
	 * Temporary key is used to sign communication with API instead of secret key which is not given.
	 * 
	 * @return string
	 */
	protected function getTempKey() {
		return $this->_getConfigData('rublon_temp_key');
	}

	/**
	 * Save temporary key to the local storage
	 *
	 * Returns true/false on success/failure.
	 *
	 * @param string $tempKey
	 * @return bool
	*/
	protected function saveTempKey($tempKey) {
		return $this->_saveConfigData('rublon_temp_key', $tempKey);
	}
	
	/**
	 * Save given temporary key and process start time into local storage.
	 * 
	 * Returns true/false on success/failure.
	 * 
	 * @param string $tempKey
	 * @param int $startTime
	 * @return bool
	 */	
	protected function saveInitialParameters($tempKey, $startTime) {
		return $this->saveTempKey($tempKey) && $this->saveStartTime($startTime);
	}
	
	/**
	 * Return local-stored start time of the process or NULL if empty.
	 * Start time is used to validate lifetime of the process.
	 * 
	 * @return int/null
	 */
	protected function getStartTime() {
		return (int)$this->_getConfigData('rublon_start_time');
	}
		
	/**
	 * Save temporary start time to the local storage
	 *
	 * Returns true/false on success/failure.
	 *
	 * @param string $tempKey
	 * @return bool
	*/
	protected function saveStartTime($startTime) {
		return $this->_saveConfigData('rublon_start_time', $startTime);
	}

	/**
	 * Get the communication URL of this Rublon module
	 * 
	 * Returns public URL address of the communication script.
	 * API server calls the communication URL to communicate with local system by REST or browser redirections.
	 * The communication URL is supplied to the API during initialization.
	 * 
	 * @return string
	 */
	protected function getCommunicationUrl() {
		$url = Mage::getUrl(self::URL_COMMUNICATION);
		$url = rtrim($url, '/\\');
		return $url;
	}
	
	/**
	 * Get project's public webroot URL address
	 * 
	 * Returns the main project URL address needful for registration consumer in API.
	 * 
	 * @return string
	 */
	protected function getProjectUrl() {
		return Mage::getBaseUrl();
	}
	
	/**
	 * Get the callback URL of this Rublon module
	 * 
	 * Returns public URL address of the Rublon consumer's callback script.
	 * API server calls the callback URL after valid authentication.
	 * The callback URL is needful for registration consumer in API.
	 * 
	 * @return string
	 */
	protected function getCallbackUrl() {
		return $this->getProjectUrl() . 'rublon/callback/index/state/%state%/token/%token%/windowType/%windowType%';
	}
	
	/**
	 * Save a given data in a local-stored configuration.
	 * 
	 * @param array $data
	 * @return bool
	 */
	protected function _saveConfigData($path, $data) {
		$config = Mage::getConfig();
		$config->saveConfig($path, $data);
		Mage::getConfig()->cleanCache();
		$configValue = $this->_getConfigData($path);
		return (isset($configValue));
	}

	protected function _getConfigData($path)
	{
		$data = Mage::getStoreConfig($path);
		return (isset($data) ? $data : NULL);
	}

	/**
	 * Set API domain for testing
	 */
	public function setDomain($domain) {
		$this->apiDomain = $domain;
	}
	
	/**
	 * Update Rublon plugin settings using 'systemToken' and 'secretKey' from
	 * successfully registered project
	 */
	private function updateRublonSettings() {
		$this->_saveConfigData('rublon2factor_options/general/rublon_system_token', $this->getSystemToken());
		$this->_saveConfigData('rublon2factor_options/general/rublon_secret_key', $this->getSecretKey());
		$this->_removeConfig();
	}
	
	
	/**
	 * Enable Rublon for current user
	 * 
	 * Associate current user with given Rublon profile ID
	 * 
	 * @param int $profileId
	 * @return void
	 */
	protected function handleProfileId($profileId) {
		$helper = $this->helper;
		$helper->setModule(RublonMagentoModule::ADMIN);
		$user = Mage::getSingleton('admin/session')->getUser();
		$helper->connectSecondFactor($user, $profileId);
	}
	
	
	/**
	 * Get name of the project
	 *
	 * Returns name of the project that will be set in Rublon Developers Dashboard.
	 *
	 * @return string
	 */
	protected function getProjectName() {
		return Mage::app()->getStore()->getFrontendName();
	}
	
	
	
	/**
	 * Get project's technology
	 *
	 * Returns technology, module or library name to set in project.
	 *
	 * @return string
	*/
	protected function getProjectTechnology() {
		return $this->helper->getTechnology();
	}
	
	
	/**
	 * Remove Rublon configuration values
	 */
	private function _removeConfig() {
		$config = Mage::getConfig();
		$config->deleteConfig('rublon_system_token');
		$config->deleteConfig('rublon_secret_key');
		$config->deleteConfig('rublon_temp_key');
		$config->deleteConfig('rublon_start_time');
	}
	
	
	protected function getProjectData() {
		$result = parent::getProjectData();
		$result['plugin-version'] = Rublon_Rublon2Factor_Helper_Data::PLUGIN_VERSION;
		return $result;
	}
	
	
}