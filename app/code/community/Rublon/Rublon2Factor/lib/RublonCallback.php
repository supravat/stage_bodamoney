<?php

require_once 'Rublon'. DS .'Rublon2FactorCallbackTemplate.php';

/**
 * Implementation of the Rublon Callback Template
 *
 */
class RublonCallback extends Rublon2FactorCallbackTemplate {
	
	
	/**
	 * Flash message text after valid Rublon enabling
	 * 
	 * @var string
	 */
	const TEXT_MSG_ENABLED = "Your account is now protected by Rublon.";
	
	
	/**
	 * Flash message text after valid Rublon disabling
	 * 
	 * @var string
	 */
	const TEXT_MSG_DISABLED = "Rublon protection has been disabled. You are now protected by a password only, which may result in unauthorized access to your account. We strongly encourage you to protect your account with Rublon.";
	

	/**
	 * Caller instance
	 * 
	 * @var object
	 */
	protected $caller;
	
	/**
	 * Rublon helper instance
	 * 
	 * @var Rublon_Rublon2Factor_Helper_Data
	 */
	protected $helper;
	
	/**
	 * Callback state code
	 * 
	 * @var string
	 */
	protected $state;
	
	/**
	 * Access token
	 * 
	 * @var string
	 */
	protected $token;
	
	/**
	 * URL to return after successful auth
	 * 
	 * @var string
	 */
	protected $returnUrl;
	
	
	/**
	 * User's ID
	 * 
	 * @var int
	 */
	protected $userIdToAuthorize;
	
	/**
	 * Custom URI params array passed into callback
	 *
	 * @var array
	 */
	protected $customURIParam;
	
	/**
	 * Has an error occured?
	 * 
	 * @var boolean
	 */
	protected $error = false;
	
	
	/**
	 * Override default constructor
	 * 
	 * @param object $caller
	 * @param string $state
	 * @param string $token
	 */
	public function __construct($caller, $state = null, $token = null) {
		$this->helper = Mage::helper('rublon2factor');
		$this->caller = $caller;
		$this->state = $state;
		$this->token = $token;
		parent::__construct();
	}
	

	/**
	 * Return the (optional) Rublon API domain
	 *
	 * Default domain can be replaced by testing configuration.
	 *
	 * @return string
	 */
	protected function getAPIDomain() {
		return $this->helper->getRublonDomain();
	}
	
	
	/**
	 * Get current language
	 *
	 * @return string
	 */
	protected function getLang() {
		return $this->helper->getLang();
	}
	
	
	/**
	 * Set custom URI params array passed into callback
	 *
	 * @param array $customURIParam
	 * @return RublonCallback
	 */
	public function setCustomURIParam($customURIParam) {
		$this->customURIParam = $customURIParam;
		return $this;
	}
	
	


	/**
	 * Get state from GET parameters or NULL if not present
	 *
	 * @return string|NULL
	 */
	protected function getState() {
		return $this->state;
	}
	
	/**
	 * Get access token from GET parameters or NULL if not present
	 *
	 * @return string|NULL
	 */
	protected function getAccessToken() {
		return $this->token;
	}
	
	
	/**
	 * Handle an error and return back to the previous page
	 *
	 * @param int $errorCode
	 * @param mixed $details (optional)
	 * @return void
	 */
	protected function finalError($errorCode, $details = null) {
		
		$this->error = true;
		
		switch ($errorCode) {
			case self::ERROR_REST_CREDENTIALS:
				$msg = $this->helper->__("Failed to authenticate user. Please try again.");
				break;
			case self::ERROR_UNKNOWN_ACTION_FLAG:
			case self::ERROR_MISSING_ACTION_FLAG:
				$msg = $this->helper->__("Unknown action to perform.");
				break;
			case self::ERROR_USER_NOT_AUTHORIZED:
				$msg = $this->helper->__("User is not authorized by first factor (login and password).");
				break;
			case self::ERROR_DIFFERENT_USER:
				$msg = $this->helper->__("Expected different user.");
				break;
			default:
				$msg = $this->helper->__("Rublon server error. Please try again in a moment.");
		}
		
		$this->helper->addError($msg, array(
			'method' => __METHOD__,
			'file' => __FILE__,
			'error' => $errorCode,
			'profile_id' => $this->userIdToAuthorize,
			'exception' => !empty($details)?print_r($details, true):null,
		));
		
		$this->setReturnUrl();
		
	}
	
	
	/**
	 * Restore session with passed session ID
	 *
	 * @param string $sessionId
	 * @return void
	*/
	protected function sessionStart($sessionId) {
		// not implemented yet
	}
	
	
	/**
	 * Check whether user exists in session and has been authorized by first factor
	 *
	 * @return boolean
	*/
	protected function isUserAuthorizedByFirstFactor() {
		
		$consumerParams = $this->response->getConsumerParams();
		
		// Set magento admin session
		if (isset($consumerParams['module'])) {
			$this->helper->setModule($consumerParams['module']);
		}
		if (method_exists($this->caller, 'setSessionNamespace')) {
			$this->caller->setSessionNamespace();
		}
		
		// Check user ID
		if ($this->getActionFlag() == RublonAuthParams::ACTION_FLAG_LOGIN) {
			if (!empty($consumerParams['auth_user_id'])) {
				$this->userIdToAuthorize = $consumerParams['auth_user_id'];
				return true;
			} else {
				return false;
			}
		} else {
			if ($userId = $this->helper->getLoggedUser()->getId()) {
				if (!empty($consumerParams['auth_user_id'])) {
					$this->userIdToAuthorize = $consumerParams['auth_user_id'];
					return ($consumerParams['auth_user_id'] == $userId);
				} else {
					$this->userIdToAuthorize = $userId;
					return true;
				}
			} else {
				return false;
			}
		}
		
	}
	
	
	
	/**
	 * Get Rublon profile ID of the user in current session
	 *
	 * @return int
	*/
	protected function getRublonProfileId() {
		return $this->helper->getUserProfileId(
				$this->userIdToAuthorize
			);
	}
	
	
	/**
	 * Set Rublon profile ID of the user in current session
	 *
	 * @param int $rublonProfileId New profile ID
	 * @return void
	*/
	protected function setRublonProfileId($rublonProfileId) {
		$user = $this->helper->getLoggedUser();
		if (!empty($rublonProfileId)) {
			$this->helper->connectSecondFactor($user, $rublonProfileId);
		} else {
			$this->helper->disconnectSecondFactor($user, $rublonProfileId);
		}
	}
	
	
	/**
	 * Set second-factor authorization status of the user in current session to SUCCESS
	 *
	 * @return void
	*/
	protected function authorizeUser() {
		$this->helper->loginUser($this->userIdToAuthorize);
	}
	
	
	/**
	 * Cancel authentication process and return back to the previous page
	 *
	 * Note that it may be login authentication where user is not signed-in
	 * or enabling/disabling Rublon when user is signed-in.
	 *
	 * @return void
	*/
	protected function cancel() {
		$this->setReturnUrl();
	}
	
	
	/**
	 * Handle success
	 *
	 * @return void
	*/
	protected function finalSuccess() {
		if (!$this->error) {
			$actionFlag = $this->getActionFlag();
			$consumerParams = $this->response->getConsumerParams();
			if ($actionFlag == RublonAuthParams::ACTION_FLAG_LOGIN) {
				if (isset($consumerParams['returnUrl'])) {
					$this->returnUrl = $consumerParams['returnUrl'];
				} else {
					$this->returnUrl = $this->helper->getBeforeAuthUrl();
				}
			} else {
				if ($actionFlag == RublonAuthParams::ACTION_FLAG_LINK_ACCOUNTS) {
					Mage::getSingleton('core/session')->addSuccess(
						$this->helper->__(self::TEXT_MSG_ENABLED)
					);
				}
				else if ($actionFlag == RublonAuthParams::ACTION_FLAG_UNLINK_ACCOUNTS) {
					Mage::getSingleton('core/session')->addSuccess(
						$this->helper->__(self::TEXT_MSG_DISABLED)
					);
				}
				if (isset($consumerParams['originUrl'])) {
					$this->returnUrl = $consumerParams['originUrl'];
				} else {
					$this->setReturnUrl();
				}
			}
		}
	}
	
	
	/**
	 * Set return URL from custom URI params or set to account URL
	 * 
	 * @return void
	 */
	protected function setReturnUrl() {
		if (!empty($this->customURIParam['url'])) {
			$url = $this->helper->getAbsoluteURL($this->customURIParam['url']);
		}
		if (empty($url)) {
			$url = $this->helper->getAccountUrl();
		}
		$this->returnUrl = $url;
	}
	
	
	/**
	 * Extract action flag from consumer params
	 * 
	 * @return string|NULL
	 */
	private function getActionFlag() {
		$consumerParams = $this->response->getConsumerParams();
		if (isset($consumerParams[self::FIELD_ACTION_FLAG])) {
			return $consumerParams[self::FIELD_ACTION_FLAG];
		} else {
			return NULL;
		}
	}
	
	
	/**
	 * Retrieve consumer's systemToken
	 *
	 * @return string
	*/
	protected function getSystemToken() {
		return $this->helper->getSystemToken();
	}
	
	
	/**
	 * Retrieve consumer's secretKey
	 *
	 * @return string
	*/
	protected function getSecretKey() {
		return $this->helper->getSecretKey();
	}
	
	
	
	
	/**
	 * Get URL of the page to return
	 * 
	 * @return string
	 */
	public function getReturnUrl() {
		return $this->returnUrl;
	}
	
	
}