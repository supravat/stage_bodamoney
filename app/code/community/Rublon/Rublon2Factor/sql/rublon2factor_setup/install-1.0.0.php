<?php
/**
 * Rublon2Factor for Magento SQL schema installation script
 *
 * @package   rublon/rublon2factor
 * @author     Adips Sp. z o.o.
 * @copyright  Adips Sp. z o.o.
 * @license    http://opensource.org/licenses/gpl-license.php GNU Public License, version 2
 */

$installer = $this;


$installer->startSetup();

// Create table for Rublon secure customer

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('rublon2factor/rublon_customer')} (
	customer_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
	rublon_profile_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Rublon Profile Id',
	PRIMARY KEY (customer_id),
	INDEX (rublon_profile_id),
	CONSTRAINT FOREIGN KEY (customer_id)
    REFERENCES {$this->getTable('customer/entity')}(entity_id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'rublon_rublon2factor_customer';

");

// Create table for Rublon secure administrator

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('rublon2factor/rublon_user')} (
	user_id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Admin_User Id',
	rublon_profile_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'Rublon Profile Id',
	PRIMARY KEY (user_id),
	INDEX (rublon_profile_id),
	CONSTRAINT FOREIGN KEY (user_id)
    REFERENCES {$this->getTable('admin/user')}(user_id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
COMMENT = 'rublon_rublon2factor_admin_user';

");

$installer->endSetup();
?>