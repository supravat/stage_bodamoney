<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not sell, sub-license, rent or lease
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_CurrencyRateUpdater
 * @copyright  Copyright (c) 2014 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_CurrencyRateUpdater_Block_Adminhtml_System_Config_Form_Field_Linktooptions
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return '<a href="' . $this->getUrl('*/system_config/edit', array('section' => 'et_currencyrateupdater')) . '">'
            . Mage::helper('et_currencyrateupdater')->__('Go to Currency Rate Updater settings section') .
            '</a>';
    }
}