<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_CurrencyRateUpdater
 * @copyright  Copyright (c) 2014 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_CurrencyRateUpdater_Model_Cbr extends Mage_Directory_Model_Currency_Import_Abstract
{
    protected $_url = 'http://www.cbr.ru/scripts/XML_daily.asp?date_req=';
    protected $_date;
    protected $_messages = array();
    protected $_eurToCurrencyRate;

    /**
     * @param string $currencyFrom
     * @param string $currencyTo
     * @return float|null
     */
    protected function _convert($currencyFrom, $currencyTo)
    {
        /** @var $helper ET_CurrencyRateUpdater_Helper_Data */
        $helper = Mage::helper('et_currencyrateupdater');


        $isExcluded = $helper->isCurrencyExcluded($currencyTo);
        if ($isExcluded) {
            $this->_messages[] = $helper->__(
                'Rate %s not updated. Currency %s is excluded from update. Check Currency Rate Updater settings.',
                $currencyFrom . '/' . $currencyTo,
                $currencyTo);
            return null;
        }

        $this->_date = date('d/m/Y');
        $availableRates = $this->getRatesFromCbr($this->_date);


        if (isset($availableRates[$currencyTo]) && isset($availableRates[$currencyFrom])) {
            $exchangeRate = 1 * $availableRates[$currencyFrom] / $availableRates[$currencyTo];

        } else {
            $exchangeRate = null;
        }

        if (!$exchangeRate) {
            $this->_messages[] = Mage::helper('directory')
                ->__('Rate %s not available by link %s', $currencyFrom . '/' . $currencyTo, $this->_url . $this->_date);
            return $exchangeRate;
        }

        if ($helper->isAdjustmentEnabled()) {
            $exchangeRate = $helper->getCurrencyRateWithCorrections($exchangeRate);
        }

        return round($exchangeRate, 4);

    }

    /**
     * @param $date
     * @return array
     */
    protected function getRatesFromCbr($date)
    {
        //Mage::log('getRatesFromCbr start');
        if (!$this->_eurToCurrencyRate) {
            //Mage::log('getRatesFromCbr loading  file');
            //This is aPHP(5)script example on how eurofxref-daily.xml can be parsed
            //Read eurofxref-daily.xml file in memory
            //For the next command you will need the config option allow_url_fopen=On (default)
            try {
                //echo $this->_url . $date;
                $url = $this->_url . $date;
                $XML = simplexml_load_file($url);

                //the file is updated daily between 2.15 p.m. and 3.00 p.m. CET

                $eurToCurrencyRate = array();
                foreach ($XML->Valute as  $rate) {
                    //Output the value of 1EUR for a currency code
                    $eurToCurrencyRate[(string)$rate->CharCode] = (string)$rate->Value;
                }
                $eurToCurrencyRate['RUB'] = '1.0000';
                $this->_eurToCurrencyRate = $eurToCurrencyRate;
            } catch (Exception $e) {
                $this->_messages[] = Mage::helper('directory')->__('Cannot retrieve rate from %s', $url);
            }
        }

        return $this->_eurToCurrencyRate;
    }
}