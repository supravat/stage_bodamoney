<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_CurrencyRateUpdater
 * @copyright  Copyright (c) 2014 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */
class ET_CurrencyRateUpdater_Model_Google extends Mage_Directory_Model_Currency_Import_Abstract
{
    protected $_url = 'http://www.google.com/finance/converter?a=1&from={{CURRENCY_FROM}}&to={{CURRENCY_TO}}';
    protected $_messages = array();

    /**
     * @param string $currencyFrom
     * @param string $currencyTo
     * @return float|null
     */
    protected function _convert($currencyFrom, $currencyTo)
    {
        return $this->getRateFromGoogle($currencyFrom, $currencyTo);
    }

    /**
     * @param $currencyFrom
     * @param $currencyTo
     * @param int $retry
     * @return float|null
     */
    protected function getRateFromGoogle($currencyFrom, $currencyTo, $retry = 0)
    {
        /** @var $helper ET_CurrencyRateUpdater_Helper_Data */
        $helper = Mage::helper('et_currencyrateupdater');

        $isExcluded = $helper->isCurrencyExcluded($currencyTo);
        if ($isExcluded) {
            $this->_messages[] = $helper->__(
                'Rate %s not updated. Currency %s is excluded from update. Check Currency Rate Updater settings.',
                $currencyFrom . '/' . $currencyTo,
                $currencyTo);
            return null;
        }

        $url = str_replace('{{CURRENCY_FROM}}', $currencyFrom, $this->_url);
        $url = str_replace('{{CURRENCY_TO}}', $currencyTo, $url);

        try {
            sleep(1); //Be nice to Google, they don't have a lot of hi-spec servers

            $ch = curl_init();

            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            // grab URL and pass it to the browser
            $res = curl_exec($ch);
            curl_close($ch);
            $exchangeRate = null;
            if (preg_match("'<span class=bld>([0-9\.]+)\s\w+</span>'", $res, $m)) {
                $exchangeRate = $m[1];
            }

            if (!$exchangeRate) {
                $this->_messages[] = Mage::helper('directory')->__('Cannot retrieve rate from %s', $url);
                return null;
            }

            if ($helper->isAdjustmentEnabled()) {
                $exchangeRate = $helper->getCurrencyRateWithCorrections($exchangeRate);
            }

            return round($exchangeRate, 4);
        } catch (Exception $e) {
            if ($retry == 0) {
                $this->getRateFromGoogle($currencyFrom, $currencyTo, 1);
                return null;
            } else {
                $this->_messages[] = Mage::helper('directory')->__('Cannot retrieve rate from %s', $url);
                return null;
            }
        }

    }
}