<?php

/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_CurrencyRateUpdater
 * @copyright  Copyright (c) 2014 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */
class ET_CurrencyRateUpdater_Model_System_Config_Source_Currency_Allowed
{

    protected $_options;

    /**
     * Array of allowed currencies (for those currencies we will get rates)
     *
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->_options) {
            $currencies = Mage::app()->getLocale()->getOptionCurrencies();

            /** @var $currencyModel Mage_Directory_Model_Currency*/
            $currencyModel = Mage::getSingleton('directory/currency');
            $allowedCurrencyCodes = $currencyModel->getConfigAllowCurrencies();

            $formattedCurrencies = array();
            foreach ($currencies as $currency) {
                $formattedCurrencies[$currency['value']]['label'] = $currency['label'];
            }

            $allowedCurrencies = array();
            foreach ($allowedCurrencyCodes as $currencyCode) {
                $allowedCurrencies[] = array(
                    'label' => $formattedCurrencies[$currencyCode]['label'],
                    'value' => $currencyCode
                );
            }

            $this->_options = $allowedCurrencies;
        }

        return $this->_options;
    }


}