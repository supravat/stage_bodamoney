<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_CurrencyRateUpdater
 * @copyright  Copyright (c) 2014 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */
class ET_CurrencyRateUpdater_Model_Ecb extends Mage_Directory_Model_Currency_Import_Abstract
{
    protected $_url = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
    protected $_messages = array();
    protected $_eurToCurrencyRate;

    /**
     * @param string $currencyFrom
     * @param string $currencyTo
     * @return float|null
     */
    protected function _convert($currencyFrom, $currencyTo)
    {
        /** @var $helper ET_CurrencyRateUpdater_Helper_Data */
        $helper = Mage::helper('et_currencyrateupdater');

        $isExcluded = $helper->isCurrencyExcluded($currencyTo);
        if ($isExcluded) {
            $this->_messages[] = $helper->__(
                'Rate %s not updated. Currency %s is excluded from update. Check Currency Rate Updater settings.',
                $currencyFrom . '/' . $currencyTo,
                $currencyTo);
            return null;
        }

        $availableRates = $this->getRatesFromEcb();

        if (isset($availableRates[$currencyTo]) && isset($availableRates[$currencyFrom])) {
            $exchangeRate = 1 / $availableRates[$currencyFrom] * $availableRates[$currencyTo];

        } else {
            $exchangeRate = null;
        }

        if (!$exchangeRate) {
            $this->_messages[] = Mage::helper('directory')
                ->__('Rate %s not available by link %s', $currencyFrom . '/' . $currencyTo, $this->_url);
            return $exchangeRate;
        }

        if ($helper->isAdjustmentEnabled()) {
            $exchangeRate = $helper->getCurrencyRateWithCorrections($exchangeRate);
        }

        return round($exchangeRate, 4);

    }

    /**
     * @return array
     */
    protected function getRatesFromEcb()
    {

        if (!$this->_eurToCurrencyRate) {
            //This is aPHP(5)script example on how eurofxref-daily.xml can be parsed
            //Read eurofxref-daily.xml file in memory
            //For the next command you will need the config option allow_url_fopen=On (default)
            try {
                $XML = simplexml_load_file($this->_url);
                //the file is updated daily between 2.15 p.m. and 3.00 p.m. CET

                $eurToCurrencyRate = array();
                foreach ($XML->Cube->Cube->Cube as $rate) {
                    //Output the value of 1EUR for a currency code
                    $eurToCurrencyRate[(string)$rate["currency"]] = (string)$rate["rate"];
                }
                $eurToCurrencyRate['EUR'] = '1.0000';
                $this->_eurToCurrencyRate = $eurToCurrencyRate;
            } catch (Exception $e) {
                $this->_messages[] = Mage::helper('directory')->__('Cannot retrieve rate from %s', $this->_url);
            }
        }

        return $this->_eurToCurrencyRate;
    }

}