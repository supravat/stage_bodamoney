<?php

/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_CurrencyRateUpdater
 * @copyright  Copyright (c) 2014 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */
class ET_CurrencyRateUpdater_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Check if rate should be adjusted
     *
     * @param $currencyCode null/string
     * @return bool
     */
    public function isAdjustmentEnabled($currencyCode = NULL)
    {
        $isEnabled = Mage::getStoreConfig('et_currencyrateupdater/general/rate_correction_enabled') == 1;

        // disable adjustment for excluded currencies
        /*If (!$currencyCode) {
            $isEnabled = false;
        }
        */

        return $isEnabled;
    }


    /**
     * Getting Adjustment percent from configuration
     *
     * @return int|double
     */
    public function getAdjustmentPercents()
    {
        $percent = Mage::getStoreConfig('et_currencyrateupdater/general/rate_correction_percents'); //get percent
        $percent = str_replace('%', '', $percent);
        if (is_numeric($percent)) {
            return $percent;
        } else {
            return 3; // default value
        }
    }


    /**
     * This functions returns currency rate with the corrections
     *
     * @param $rate - default rate
     * @return float
     */
    public function getCurrencyRateWithCorrections($rate)
    {
        $rate += $rate * $this->getAdjustmentPercents() / 100;
        return $rate;
    }

    /**
     *
     * @return array
     */
    public function getExcludedCurrencies()
    {
        $excludedCurrencyArray = array();

        $isExcludingEnabled = Mage::getStoreConfig('et_currencyrateupdater/general/exclude_currency_enabled');
        if ($isExcludingEnabled) {
            $string = Mage::getStoreConfig('et_currencyrateupdater/general/exclude_currency');
            $excludedCurrencyArray = explode(',', $string);
        } // else return NULL

        return $excludedCurrencyArray;
    }

    /**
     * @param $code - Currency code
     * @return bool
     */
    public function isCurrencyExcluded($code)
    {
        $excludedArray = $this->getExcludedCurrencies();
        return in_array($code, $excludedArray);
    }


    /**
     * Writes information to log file
     *
     * @param $message - string or array of strings
     * @return bool
     */
    public function log($message)
    {
        if ($this->isLogEnabled()) {
            $file = $this->getLogFileName();
            if (is_array($message)) {
                $forLog = array();
                foreach ($message as $answerKey => $answerValue) {
                    $forLog[] = $answerKey . ": " . $answerValue;
                }
                $forLog[] = '***************************';
                $message = implode("\r\n", $forLog);
            }
            Mage::log($message, Zend_Log::INFO, $file, true);
        }
        return true;
    }

    /**
     * Is log writing enabled or not?
     *
     * @return bool
     */
    public function isLogEnabled()
    {
        return (bool)Mage::getStoreConfig('et_currencyrateupdater/general/log_enabled');
    }

    /**
     * Function returns log file name
     *
     * @return string
     */
    public function getLogFileName()
    {
        return Mage::getStoreConfig('et_currencyrateupdater/general/log_file');
    }


}
