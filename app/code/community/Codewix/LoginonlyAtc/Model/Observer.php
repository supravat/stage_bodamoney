<?php
/**
 * Created by Ravinder (Rav).
 * User    : Ravinder (Rav)
 * Company : Codewix Development
 * Email   : codewix@gmail.com
 * Date    : 17/11/14
 * Time    : 9:32 PM
 */

class Codewix_LoginonlyAtc_Model_Observer {

    public function controller_action_predispatch_checkout_cart_add($observer) {
        if(!Mage::helper('customer')->isLoggedIn()){
            Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            Mage::getSingleton('core/session')->addError('You need to login/create account before adding product to cart');
            $controller = $observer->getControllerAction();
            $controller->getRequest()->setDispatched(true);
            $controller->setFlag(
                '',
                Mage_Core_Controller_Front_Action::FLAG_NO_DISPATCH,
                true
            );
            Mage::app()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
            return;
        }
    }

}