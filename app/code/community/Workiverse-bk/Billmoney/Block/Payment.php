<?php
class Workiverse_Billmoney_Block_Payment extends Mage_Core_Block_Template
{
     private $_main_email="jpmpindi@nordug.com";
	 /**
     * Get Form data by using ogone payment api
     *
     * @return array
     */
    public function getPayMentUrl(){
        return Mage::getUrl("gatewaybillamoney/processing/doCharge");
    }
    public function getCheckCodeUrl(){
        return Mage::getUrl("gatewaybillamoney/processing/checkPinCode");
    }
    public function getWithdrawUrl(){
        return Mage::getUrl("gatewaybillamoney/processing/doWithdraw");
    }
    public function getFormData()
    {
        return $this->_getOrder()->getPayment()->getMethodInstance()->getFormFields();
    }
    public function getMainEmail(){
        return Mage::getStoreConfig('payment/billmoney/master');
    }
    //public function getOrder(){
    //    return $this->_getOrder();
    //}
    /**
     * Getting gateway url
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->_getOrder()->getPayment()->getMethodInstance()->getUrl();
    }
	/**
     * Return checkout session instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }
      public function getBodaFee(){
        $order=$this->_getOrder();
        $boda_fee     =$order->getData("fooman_surcharge_amount");
        $discount     =$order->getData("affiliateplus_discount");     
        return ($boda_fee+$discount);
    }
    /**
     * Return order instance
     *
     * @return Mage_Sales_Model_Order|null
     */
    protected function _getOrder()
    {
        if ($this->getOrder()) {
            return $this->getOrder();
         } elseif ($orderIncrementId = $this->_getCheckout()->getLastRealOrderId()) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        } elseif ($orderIncrementId = $this->getRequest()->getPost("transaction_id")) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        }
         else {
            return null;
        }
	}
    public function getorderdetail(){
        return $this->_getOrder();
    }
    public function getItems(){
        $order=$this->_getOrder();
         $items = $order->getAllVisibleItems();
         return $items;
    }
    public function getCustomer(){
        $order=$this->_getOrder();
        if($order->getCustomerId() === NULL){
            $customer =  $order->getCustomer();
        }
        //else, they're a normal registered user.
        else {
           $model =Mage::getModel('customer/customer');
           $customer = $model->load($order->getCustomerId());            
        }
        return $customer;
    }
    public function getCusterGender($customer){
        if(!$customer)
        $customer=$this->getCustomer();
            if($customer->getGender())
                return "Male";
        return "Female";
    }
    public function getAffilineAccount(){
        $email    =$this->_main_email;
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);
        return $customer;
    }
    public function getOrderComment(){
        $order=$this->_getOrder();
        $_history = $order->getAllStatusHistory();
        if (count($_history)){
            foreach ($_history as $_historyItem)
                return $_historyItem->getComment()?$_historyItem->getComment():null;
        }
        return null;
    }
    public function checkcustomer($email){
        if(!$email)
        return false;
        $email=trim($email);    
        $datamodel =Mage::getModel('customer/customer');   
        $customer = $datamodel->getCollection()->addFieldToFilter('email', $email)->getFirstItem();
        if(!$customer||!$customer->getData("email")) {     
            $customer = Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email', $email)->getFirstItem();
            if($customer)$customer=Mage::getModel('sublogin/sublogin')->load($customer->getId());
        }else{
            $model    = Mage::getModel('customer/customer');
            $customer = $model->load($customer->getId());
        }
         if($customer&&$customer->getData("email"))
           return $customer;  
        return false; 
    }
     public function generateurl(){
        return Mage::getUrl("gatewaybillamoney/processing/generatecode");
    }
    public function _getSellerInfo()
    {
         $order =$this->_getOrder();
   
         if($order){
             $ordered_items = $order->getAllItems();
             Foreach($ordered_items as $item){
                 $produdct_id =$item->getProduct()->getId(); //product id
               
             }         
             $collection = Mage::getModel('marketplace/product')->load($produdct_id,"mageproductid");
             //var_dump($collection->getData());
             if($collection){
             $customer_id=$collection->getData("userid");
             $customer   =Mage::getModel('customer/customer');
             $customer_data = $customer->load($customer_id);  
            
             //$customer_email  =$customer_data->getOrigEmail()?$customer_data->getOrigEmail():$customer_data->getEmail();
             return $customer_data;
             //$data=$this->checkcustomer($customer_email);
             }
         }
         return null;
    }
    public function getSeller(){
        return $this->_getSellerInfo();
    }
    public function getSellerEmail(){
        $seller =$this->_getSellerInfo();
        $email=null;
        if($seller){
             if($seller->getData("orig_email"))
                $email =$seller->getData("orig_email");
             else
                $email =$seller->getEmail();
            
        }
        return $email;
    }
    public function getSellerName(){
        $seller =$this->_getSellerInfo();
        if($seller){
             if($seller->getData("orig_email"))
             {
                return @$seller->getData("orig_firstname")." ".@$seller->getData("orig_middlename")." ".@$seller->getData("orig_lastname");
             }
             else
                return $seller->getName();
        }
    }
     public function getAffilineEmail(){
        $affiline_email =$this->_getOrder()->getCustomerEmail();
        $pos = strpos($affiline_email,"bodamoney.com");  
        if($pos==false)
          return $affiline_email;        
        else
        {
            $pos= explode("@",$affiline_email);
            return $pos[0];
        }
        return "Uregistered customer";
    }
    public function getAffilineName(){
        return $this->_getOrder()->getCustomerName();
    }
}
?>