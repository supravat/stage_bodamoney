<?php 
class Workiverse_Interpretertable_Block_Adminhtml_Interpretertable extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
         //where is the controller
         $this->_controller = 'adminhtml_interpretertable';
         $this->_blockGroup = 'workiverse_interpretertable';
         //text in the admin header
         $this->_headerText = 'Interpreter Arrivals  ';
         //value of the add button
         //$this->_addButtonLabel = 'Add a Interpretertable';
    
        parent::__construct();
        $this->_removeButton('add');
     }
}
?>