<?php 
class Workiverse_Interpretertable_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
     //where is the controller
     $this->_controller = 'adminhtml_interpretertable';
     $this->_blockGroup = 'Interpretertable';
     //text in the admin header
     $this->_headerText = 'Interpretertable Management';
     //value of the add button
     $this->_addButtonLabel = 'Add a Interpretertable';
   
     parent::__construct();
     $this->_removeButton('add');
    }
}
?>