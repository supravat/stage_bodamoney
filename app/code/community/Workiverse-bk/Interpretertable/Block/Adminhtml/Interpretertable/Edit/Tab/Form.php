<?php
class Workiverse_Interpretertable_Block_Adminhtml_Interpretertable_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
   protected function _prepareForm()
   {
       $form = new Varien_Data_Form();
       $this->setForm($form);
       $fieldset = $form->addFieldset('interpretertable_form',
                                       array('legend'=>'ref information'));
        $fieldset->addField('purpose', 'text',
                       array(
                          'label' => 'Purpose',
                          'class' => 'required-entry',
                          'required' => true,
                           'name' => 'nom',
                    ));
        $fieldset->addField('keyword', 'text',
                         array(
                          'label' => 'Keyword',
                          'class' => 'required-entry',
                          'required' => true,
                          'name' => 'keyword',
                      ));
          $fieldset->addField('alias_keyword', 'text',
                    array(
                        'label' => 'Alias keyword',
                        'class' => 'required-entry',
                        'required' => true,
                        'name' => 'alias_keyword',
                 ));
 if ( Mage::registry('interpretertable_data') )
 {
    $form->setValues(Mage::registry('interpretertable_data')->getData());
  }
  return parent::_prepareForm();
 }
}