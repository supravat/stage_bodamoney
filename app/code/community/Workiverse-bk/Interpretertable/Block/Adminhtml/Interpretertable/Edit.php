<?php 
class Workiverse_Interpretertable_Block_Adminhtml_Interpretertable_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
   {
        parent::__construct();
        $this->_objectId = 'interpretertable_id';
        //vwe assign the same blockGroup as the Grid Container
        $this->_blockGroup = 'interpretertable';
        //and the same controller
        $this->_controller = 'adminhtml_interpretertable';
        //define the label for the save and delete button
        $this->_updateButton('save', 'label','save interpretertable');
        $this->_updateButton('delete', 'label', 'delete interpretertable');
    }
       /* Here, we're looking if we have transmitted a form object,
          to update the good text in the header of the page (edit or add) */
    public function getHeaderText()
    {
        if( Mage::registry('interpretertable_data')&&Mage::registry('interpretertable_data')->getId())
         {
              return 'interpretertable  '.$this->htmlEscape(
              Mage::registry('interpretertable_data')->getTitle()).'<br />';
         }
         else
         {
             return 'Add a Interpretertable';
         }
    }
}
?>