<?php 
class Workiverse_Interpretertable_Block_Adminhtml_Interpretertable_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
   public function __construct()
   {
       parent::__construct();
       $this->setId('contactGrid');
       $this->setDefaultSort('interpretertable_id');
       $this->setDefaultDir('DESC');
       $this->setSaveParametersInSession(true);
   }
   protected function _prepareCollection()
   {
      $collection = Mage::getModel('interpretertable/messagetable')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
    }
   protected function _prepareColumns()
   {
       $this->addColumn('interpretertable_id',
             array(
                    'header' => 'ID',
                    'align' =>'right',
                    'width' => '50px',
                    'index' => 'interpretertable_id',
               ));
       $this->addColumn('phone',
               array(
                    'header' => 'Telephone number',
                    'align' =>'left',
                    'index' => 'phone',
                    'type'  =>'text'
              ));
       $this->addColumn('type', array(
                    'header' => 'Agent',
                    'align' =>'left',
                    'index' => 'type',
             ));
        /*
        $this->addColumn('alias_keyword', array(
                     'header' => 'alias_keyword',
                     'align' =>'left',
                     'index' => 'alias_keyword',
        ));
        $this->addColumn('position_1', array(
                     'header' => 'Position 1',
                     'align' =>'left',
                     'index' => 'alias_keyword',
        ));
        $this->addColumn('position_2', array(
                     'header' => 'Position 2',
                     'align' =>'left',
                     'index' => 'position_2',
        ));
        $this->addColumn('position_3', array(
                     'header' => 'Position 3',
                     'align' =>'left',
                     'index' => 'position_3',
        ));
        $this->addColumn('position_4', array(
                     'header' => 'Position 4',
                     'align' =>'left',
                     'index' => 'position_1',
        ));
        
        /*$this->addColumn('reply_message', array(
                     'header' => 'Reply message',
                     'align' =>'left',
                     'index' => 'reply_message',
        ));
        
        $this->addColumn('reminder_message', array(
                     'header' => 'Reminder message',
                     'align' =>'left',
                     'index' => 'reminder_message',
        ));
        
         $this->addColumn('fail_message', array(
                     'header' => 'Fail_message',
                     'align' =>'left',
                     'index' => 'fail_message',
        ));*/
        
         $this->addColumn('penalty_message', array(
                     'header' => 'Message',
                     'align' =>'left',
                     'index' => 'penalty_message',
        ));
        
         /*$this->addColumn('product', array(
                     'header' => 'Product',
                     'align' =>'left',
                     'index' => 'product',
        ));
        */
         $this->addColumn('updated_at', array(
                     'header' => 'Updated at',
                     'align' =>'left',
                     'type' => 'datetime',
                     'index' => 'updated_at',
        ));
         $this->addColumn('created_at', array(
                     'header' => 'Arrival time',
                     'align' =>'left',
                     'type' => 'datetime',
                     'index' => 'created_at',
        ));
        
        
         return parent::_prepareColumns();
    }
    public function getRowUrl($row)
    {
        // return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        return null;
    }
}
?>