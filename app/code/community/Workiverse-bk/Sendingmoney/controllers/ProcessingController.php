<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Workiverse
 * @package     Workiverse_sendingmoney
 * @copyright   Copyright (c) 2012 Workiverse Org (http://www.forummods.org)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Workiverse_Sendingmoney_ProcessingController extends Mage_Core_Controller_Front_Action
{
    private $_main_email="";
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Iframe page which submits the payment data to sendingmoney.
     */
    public function placeformAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function customerAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function gatewayAction()
    {
       $this->loadLayout();
       $this->renderLayout(); 
       $this->getLayout()->getBlock('head')->setTitle($this->__('Payment Information'));
    }
     public function infoAction()
    {
       try {
            if ($this->getRequest()->isPost()) {
                $this->_main_email=Mage::getStoreConfig('payment/sendingmoney/master');
                $order = Mage::getModel('sales/order');
                $data=$this->getRequest()->getPost();
                $transaction_id =$data['transaction_id']?$data['transaction_id']:"";
                $order->loadByIncrementId($transaction_id);
             
                if (!$order->getId()) {
                    Mage::throwException('No order for processing found');
                }
                $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                    Mage::helper('sendingmoney')->__('The customer was redirected to Perfect Money.')
                );
                $order->save();
                $session = $this->_getCheckout();
                $session->getQuote()->setIsActive(false)->save();          
                $session->clear();
                $this->loadLayout();
    		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Info'));
    		  $this->renderLayout();
        }
        } catch (Exception $e){
            Mage::logException($e);
            echo  $e->getMessage();
            //parent::_redirect('checkout/cart');
        }
    }
    
    
    /**
     * Show orderPlaceRedirect page which contains the sendingmoney iframe.
     */
    public function paymentAction()
    {
        try {
                $this->_main_email=Mage::getStoreConfig('payment/sendingmoney/master');
                $session = $this->_getCheckout();
                $order = Mage::getModel('sales/order');        
                $order->loadByIncrementId($session->getLastRealOrderId());
                if (!$order->getId()) {
                    Mage::throwException('No order for processing found');
                }
                $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                    Mage::helper('sendingmoney')->__('The customer was redirected to Perfect Money.')
                );
                $order->save();
                $session->getQuote()->setIsActive(false)->save();               
                //$session->clear();
                $this->renderLayout();
                $this->loadLayout();
    		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday SendingMoney  GateWay'));
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    }
    public function doChargeAction(){
        try{
            if ($this->getRequest()->isPost()) {   
                $data=$this->getRequest()->getPost();
                $this->_main_email=Mage::getStoreConfig('payment/sendingmoney/master');
                $orderIncrementId =$data['transaction_id']?$data['transaction_id']:"";
                if(!$orderIncrementId||$orderIncrementId==null)
                {
                    Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                    parent::_redirect("checkout/onepage/failure/");
                    return ;
                }
               
                $order=Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
                $order_id =$order->getId();
                $blockpayment = new Workiverse_sendingmoney_Block_Payment;       
                $amount=$blockpayment->getBodaFee();
              
                $grand_total =$order->getGrandTotal();
                $sub_total=$order->getData("subtotal");
                $payment  =$order->getPayment();
                $pcustomer =$payment->getData("puser");
              
                /////
                $receive   =$blockpayment->checkcustomer($pcustomer);
               
                $message_detail	= $blockpayment->getMessage();
                $customer_id       =$order->getCustomerId();  
                $customer_email       =$order->getCustomerEmail();  
                //$affiline       =Mage::getModel("customer/customer")->load($customer_id);  
                $parent_affiline_id=0;
                $affiline   =$blockpayment->checkcustomer($customer_email);      
                if(!$affiline)
                {
                     Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                     parent::_redirect("checkout/onepage/failure/");
                     return ;
                }
                if($affiline->getData("id"))
                {
                    $parent_affiline_id=$affiline->getData("entity_id");
                }
                $customer_credit = $affiline->getData('credit_value');
                $account = Mage::getModel("customer/customer");
              
                $account->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                $account = $account->loadByEmail($this->_main_email); 
                
                if($customer_credit<=0||$grand_total < 0 || $grand_total > $customer_credit)
                {
                    Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                    parent::_redirect("checkout/onepage/failure/");
                    return ;
                }
                             
                if($receive){
                    $receive->setData("credit_value",($receive->getCreditValue()+$sub_total))->save();  
                    $parent_id =0;
                    if($receive->getData("id"))
                    $parent_id=$receive->getData("entity_id");
                    if(!$parent_id)
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($receive->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                    "get credit value from " . $blockpayment->getAffilineName() ,$orderIncrementId,$sub_total,$orderIncrementId,date("Y-m-d"),$receive->getName(),$parent_id,$blockpayment->getMessage(),$affiline->getEmail());
                    else
                    Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($receive->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                    "get credit value from " . $blockpayment->getAffilineName() ,$orderIncrementId,$sub_total,$orderIncrementId,date("Y-m-d"),($receive->getFirstName()." ".$receive->getLastName()),$parent_id,$blockpayment->getMessage(),$affiline->getEmail());
                    if(!$parent_affiline_id)
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($affiline->getId(),
                    Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                    " sent " . $sub_total . " credit to " . $receive->getEmail(),$orderIncrementId, -$sub_total,$orderIncrementId,date("Y-m-d "),$affiline->getName(),
                    0,$blockpayment->getMessage(),$receive->getEmail());
                    else
                    Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($affiline->getId(),
                    Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                    " sent " . $sub_total . " credit to " . $receive->getEmail(),$orderIncrementId, -$sub_total,$orderIncrementId,date("Y-m-d"),$affiline->getName(),
                    $parent_affiline_id,$blockpayment->getMessage(),$receive->getEmail());
                    
                    
                    Mage::getModel('customercredit/customercredit')->sendConfirmToFriendByEmail($sub_total,$customer_email,$blockpayment->getMessage(),$affiline->getData("id")); 
                   
                    Mage::getModel('customercredit/customercredit')->receiveConfirmToFriendByEmail($sub_total,$receive->getEmail(),$blockpayment->getMessage(),$receive->getData("id")); 
                 
                   
                }else{
                    Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($sub_total,$pcustomer,$message_detail);     
                }                               
                $affiline->setData("credit_value",($affiline->getCreditValue()-$grand_total))->save();   
                $account->setCreditValue($account->getCreditValue()+$amount)->save();                    
                ///add trans  
                Mage::getModel('customercredit/transaction')->addTransactionHistory($account->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                "get credit value from " . $blockpayment->getAffilineName(),$orderIncrementId, $amount,$orderIncrementId,date("Y-m-d "),$account->getName(),0,null,$affiline->getEmail());
                if(!$parent_affiline_id)
                Mage::getModel('customercredit/transaction')->addTransactionHistory($affiline->getId(),
                Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                " sent " . $amount . " credit to " . $account->getEmail(),$orderIncrementId, -$amount,$orderIncrementId,date("Y-m-d "),$affiline->getName(),
                $parent_affiline_id,"",$account->getEmail());
                else
                Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($affiline->getId(),
                Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                " sent " . $amount . " credit to " . $account->getEmail(),$orderIncrementId, -$amount,$orderIncrementId,date("Y-m-d "),$affiline->getName(),
                $parent_affiline_id,"",$account->getEmail());
               //done
                parent::_redirect('gatewaysendingamoney/processing/success/transaction_id/'.$order->getIncrementId());
                    
            }
        }catch (Exception $e){
            Mage::logException($e);
             parent::_redirect("checkout/onepage/failure/");
        }
    }
   
    /**
     * Action to which the customer will be returned when the payment is made.
     */
    public function successAction()
    {
        $event = Mage::getModel('sendingmoney/event')
                 ->setEventData($this->getRequest()->getParams());
        try {
            $this->_main_email=Mage::getStoreConfig('payment/sendingmoney/master');
            $quoteId = $event->successEvent();
            $this->_getCheckout()->setLastSuccessQuoteId($quoteId);
            //////
            $param=$this->getRequest()->getParams();
            $trans_id=$param['transaction_id'];
            $orderOBJ = Mage::getModel('sales/order')->loadByIncrementId($trans_id);
            $orderOBJ->setStatus('complete');
            $orderOBJ->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
            $history = $orderOBJ->addStatusHistoryComment(' Changed Status to Complete  ', false);
            $history->setIsCustomerNotified(true);
            $orderOBJ->save();
            ////
            parent::_redirect('checkout/onepage/success');          
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the customer will be returned if the payment process is
     * cancelled.
     * Cancel order and redirect user to the shopping cart.
     */
    public function cancelAction()
    {
        $this->_main_email=Mage::getStoreConfig('payment/sendingmoney/master');
        $event = Mage::getModel('sendingmoney/event')
                 ->setEventData($this->getRequest()->getParams());
        $message = $event->cancelEvent();

        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getsendingmoneyQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }

        $session->addError($message);
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the transaction details will be posted after the payment
     * process is complete.
     */
    public function statusAction()
    {
        $event = Mage::getModel('sendingmoney/event')
            ->setEventData($this->getRequest()->getParams());
        $message = $event->processStatusEvent();
        $this->getResponse()->setBody($message);
    }

    /**
     * Set redirect into responce. This has to be encapsulated in an JavaScript
     * call to jump out of the iframe.
     *
     * @param string $path
     * @param array $arguments
     */
    protected function _redirect($path, $arguments=array())
    {
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('sendingmoney/redirect')
                ->setRedirectUrl(Mage::getUrl($path, $arguments))
                ->toHtml()
        );
        return $this;
    }
}
