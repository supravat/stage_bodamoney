<?php
class Workiverse_Withdrawmoney_Block_Payment extends Mage_Core_Block_Template
{
     private $_main_email="jpmpindi@nordug.com";
     private $_orderIncrementId;
	 /**
     * Get Form data by using ogone payment api
     *
     * @return array
     */
    public function getPayMentUrl(){
        return Mage::getUrl("gatewaywithdrawbodamoney/processing/doCharge");
    }
    public function getCheckCodeUrl(){
        return Mage::getUrl("gatewaywithdrawbodamoney/processing/checkPinCode");
    }
    public function getWithdrawUrl(){
        return Mage::getUrl("gatewaywithdrawbodamoney/processing/doWithdraw");
    }
    public function getFormData()
    {
        return $this->_getOrder()->getPayment()->getMethodInstance()->getFormFields();
    }
    
    public function getMainEmail(){
        return Mage::getStoreConfig('payment/withdrawmoney/master');
    }
    /**
     * Getting gateway url
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->_getOrder()->getPayment()->getMethodInstance()->getUrl();
    }
	/**
     * Return checkout session instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }
    public function getBodaFee($order=null){
        if($order==null)
            $order=$this->_getOrder();
        $boda_fee     =$order->getData("fooman_surcharge_amount");
        $discount     =$order->getData("affiliateplus_discount");     
        return ($boda_fee+$discount);
    }
    public function getAffilineId($order=null){
         if($order==null)
            return $this->_getOrder()->getCustomerId();
        else
            return $order->getCustomerId();
    }
    public function getAffilineEmail($order=null){
        $affiline_email =$this->_getOrder()->getCustomerEmail();
        $pos = strpos($affiline_email,"bodamoney.com");  
        if($pos==false)
          return $affiline_email;        
        else
        {
            $pos= explode("@",$email);
            return $pos[0];
        }
        return "Uregistered customer";
    }
    public function getAffilineName($order=null){
         if($order==null)
        return $this->_getOrder()->getCustomerName();
        else
        return $order->getCustomerName();
    }
    public function getMessage(){
        return Mage::getSingleton('customer/session')->getOrderCustomerComment();
    }
    public function getReceiveEmail($order=null){
        if($order==null)
        $payment  =$this->_getOrder()->getPayment();
        else
        $payment  =$order->getPayment();
        $email    =$payment->getData("puser");
        $pos = strpos($email,"@bodamoney.com");
        if($pos ===false) { 
            return $email;
          }else{
             $postion =explode("@",$email);
             return $postion[0];      
          }          
    }
    public function getReceiveName($order=null){
        if($order==null)
        $payment  =$this->_getOrder()->getPayment();
        else
        $payment  =$order->getPayment();
        $email    =$payment->getData("puser");
        $receive=$this->checkcustomer($email);  
        if($receive)
        {
            return $receive->getName()?$receive->getName():($receive->getFirstname()." ".$receive->getLastname());
        }
        else{
            return "Uregistered customer";
        }
    }
    
    public function getReceiveGender($order=null){
        if($order==null)
            $payment  =$this->_getOrder()->getPayment();
        else
        $payment  =$order->getPayment();
        $email    =$payment->getData("puser");
        $receive=$this->checkcustomer($email);  
        if($receive)
        {
            return $this->getCusterGender($receive);
        }
        else{
            return "NA";
        }
    }
    public function getIdNumber($order=null){
        if($order==null)
            $payment  =$this->_getOrder()->getPayment();
        else
            $payment  =$order->getPayment();
        $email    =$payment->getData("puser");
        $receive=$this->checkcustomer($email);  
        if($receive)
        {
            return $receive->getIdNumber();
        }
        else{
            return "NA";
        }
    }
    protected function setOrder($_orderIncrementId){
        $this->_orderIncrementId=$_orderIncrementId;
    }
    protected function getIncrementId(){
        $params=$this->getRequest()->getPost();
        $_orderIncrementId=$params['transaction_id'];
        $this->setOrder($_orderIncrementId);
        return $_orderIncrementId;
    }
    public function getOrderIncreamentId(){
        return $this->_orderIncrementId;
    }
    
    /**
     * Return order instance
     *
     * @return Mage_Sales_Model_Order|null
     */
     protected function _getOrder($orderIncrementId=null)
    {
        if ($this->getOrder()) {
            return $this->getOrder();
         } elseif ($orderIncrementId = $this->_getCheckout()->getLastRealOrderId()) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        } elseif ($orderIncrementId = $this->getRequest()->getPost("transaction_id")) {
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        }elseif($orderIncrementId!=""){
            return Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        }
         else {
            return null;
        }
	}
    public function getorderdetail($orderIncrementId=null){
        return $this->_getOrder($orderIncrementId);
    }
    public function getItems(){
        $order=$this->_getOrder();
         $items = $order->getAllVisibleItems();
         return $items;
    }
    public function getCustomer(){
        $order=$this->_getOrder();
        if($order->getCustomerId() === NULL){
            $customer =  $order->getCustomer();
        }
        //else, they're a normal registered user.
        else {
           $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());            
        }
        return $customer;
    }
    public function getCusterGender($customer){
        if(!$customer)
        $customer=$this->getCustomer();
            if($customer->getGender())
                return "Male";
        return "Female";
    }
    public function getAffilineAccount(){
        $email    =$this->_main_email;
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($email);
        return $customer;
    }
    public function getOrderComment(){
        $order=$this->_getOrder();
        $_history = $order->getAllStatusHistory();
        if (count($_history)){
            foreach ($_history as $_historyItem)
                return $_historyItem->getComment()?$_historyItem->getComment():null;
        }
        return null;
    }
    public function checkcustomer($email){
          try{
       if(!$email)
        return false;
        $email=trim($email);                        
                //$customer = Mage::getModel("customer/customer"); 
                ///$customer->loadByEmail($email); 
      
        $customer = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email',$email)->getFirstItem();     
        if(!$customer||count($customer->getData())==0){             
            $customer = Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email',$email)->getFirstItem();                
            if($customer&&$customer->getEmail()){
            $customer = Mage::getModel('sublogin/sublogin')->load($customer->getId());
            }
        }else{
             $customer=Mage::getModel('customer/customer')->load($customer->getId());
        }
      
        return $customer; 
            
        }catch(exception $ex){
            var_dump($ex->getMessage());
        }
    }
     public function generateurl(){
        return Mage::getUrl("gatewaywithdrawbodamoney/processing/generatecode");
    }
    public function getinputdata()
    {
        return  Mage::getSingleton('core/session')->getWithdrawmoneyData();
        
    }
    public function getOrderInformation(){
        $data=$this->getinputdata();

        return $this->getorderdetail($datainput['order_id']);
    }
}
?>