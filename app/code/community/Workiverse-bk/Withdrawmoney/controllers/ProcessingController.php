<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Workiverse
 * @package     Workiverse_Perfectmoney
 * @copyright   Copyright (c) 2012 Workiverse Org (http://www.forummods.org)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Workiverse_Withdrawmoney_ProcessingController extends Mage_Core_Controller_Front_Action
{
    private $_main_email="";
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Iframe page which submits the payment data to Perfectmoney.
     */
    public function placeformAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function customerAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function gatewayAction()
    {
       $this->loadLayout();
       $this->renderLayout();
       $this->getLayout()->getBlock('head')->setTitle($this->__('Payment Information'));
    }
     public function infoAction()
    {
       try {
            $this->_main_email=Mage::getStoreConfig('payment/withdrawmoney/master');
            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
            $params=$this->getRequest()->getPost();
            if(count($params)==0)
            {
                Mage::getSingleton('core/session')->addError('Session timeout!');
                parent::_redirect("checkout/cart");
                return;
            }
            $transaction_id=$params['transaction_id'];
            $order->loadByIncrementId($transaction_id);
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('withdrawmoney')->__('The customer was redirected to Boda Money.')
            );
            $order->save();
            $session->getQuote()->setIsActive(false)->save();          
            //$session->clear();
            $this->loadLayout();
		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Info'));
		$this->renderLayout();
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    }
    public function checkPinCodeAction(){
        try{
            if ($this->getRequest()->isPost()) {
                $this->_main_email=Mage::getStoreConfig('payment/withdrawmoney/master');
                $data=$this->getRequest()->getPost();
                $password=trim($data['password']);
                $username =trim($data['username']);
                
                if(!$username)
                {
                    $result['error']  = true; 
                    $result['message']="WithDraw Account is not exits";
                }
                $method =new Workiverse_Withdrawmoney_Block_Payment;
                $customerData=$method->checkcustomer($username);
                if(!$customerData)
                {
                    $result['error']  = true; 
                    $result['message']="WithDraw Account is not exits";
                }
                if($customerData->getData("id"))
                $inDatabase=$customerData->getPassword();
                else
                $inDatabase=$customerData->getPasswordHash();
               
                $hashPassword = explode(':', $inDatabase);
             
                $firstPart = $hashPassword[0];
                $oldpass=$firstPart;
                $salt = $hashPassword[1];
                $pos  =explode(":",$hash);              
                $newpass= md5($salt.$password);
                if($oldpass!=$newpass)
                {
                     $result['error']  = true; 
                     $result['message']="Your Pin code is not true";
                    
                }else
                {
                    $block = $this->getLayout()->createBlock('withdrawmoney/payment')->setTemplate('withdrawmoney/step2.phtml');
                    $result['content']  =$block->toHtml(); 
                    $result['success'] = true; 
                    $subtotal          =$data['sub-total'];
                    $currency_grandtotal=Mage::app()->getLocale()->currency("UGX")->toCurrency($subtotal);
                    $this->gereratecode($username,$username,$username,"",$currency_grandtotal);
                    $data['step']=1;
                   
                    Mage::getSingleton('core/session')->setWithdrawmoneyData($data);

                }
                 echo json_encode($result);
                 die;
            }
           $result['error']  = true; 
           $result['message']="Session is TimeOut";
           echo json_encode($result);
           die;
        }catch(exception $ex){
             $result['error']  = true; 
            $result['message']=$ex->getMessage();
        }
    }
    /*****/
    public function gereratecode($email,$buyer,$receiver,$orderid,$amount){
        try{
            $code = rand(100000, 999999);
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $connection->beginTransaction();
            //$date = date('m/d/Y h:i:s a', time());        
            $__fields = array("phone_number"=>$email,"verification_code"=>$code,"created"=>time(),"type"=>1);
            $connection->insert('vertify_code', $__fields);
            $connection->commit();  
            Mage::getModel('customercredit/customercredit')->sendvertifycode($email,$buyer,$receiver,$orderid,$amount,$code);         
        }
        catch(exception $ex){
            
        }
    }
    public function checkvertifycode($email,$code){
        try{
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query = "SELECT * FROM vertify_code WHERE phone_number ='".$email."' and verification_code='".$code."' and verified=0  and type=1 LIMIT 1";       
            $rowArray =$readConnection->fetchRow($query);
           
            if($rowArray)
            {
                $id=intval($rowArray['id']);
                $writeConnection = $resource->getConnection('core_write');
                $query     ="update vertify_code set verified=1 where id=$id";
                $writeConnection->query($query);
                return true;
            }
            return false;
            }catch(exception $ex){
                echo $ex->getMessage();
                return false;
            }        
    }
    public function vertifycodeAction(){
        try{
            $result=array();
            if ($this->getRequest()->isPost()) {
                 $data=$this->getRequest()->getPost('login');                
                 $code=$data['code'];
                 $datainput= Mage::getSingleton('core/session')->getWithdrawmoneyData();       
               
                 $email    =$datainput['username'];
                 $checked=$this->checkvertifycode($email,$code);
                 if($checked)
                 {
                    $block = $this->getLayout()->createBlock('withdrawmoney/payment')->setTemplate('withdrawmoney/step3.phtml');
                    $result['success']=true;    
                    $result['content']=trim($block->toHtml());                                                            
                    $datainput['step']=2;
                    Mage::getSingleton('core/session')->setWithdrawmoneyData($datainput);                                                         
                    // var_dump($result);
                 }
                else
                 {
                    $result['error'] = true;
                    $result['message']="Wrong Confirm Code.";
                //    echo json_encode($result);
                 } 
                  echo json_encode($result);
                 die;
            }
             
        }
        catch(exception $ex){
            $result['error'] = true;
            $result['message']="Wrong Confirm Code.";
             echo json_encode($result);
        }
    }
     public function confirmAction(){
        $result['success'] = true;
        $block = $this->getLayout()->createBlock('withdrawmoney/payment')->setTemplate('withdrawmoney/step4.phtml');
        $datainput= Mage::getSingleton('core/session')->getWithdrawmoneyData();       
        $datainput['step']=3;
        Mage::getSingleton('core/session')->setWithdrawmoneyData($datainput);  
        $result['content']=$block->toHtml(); 
        echo json_encode($result);
     }
    /*****/
    public function doWithdrawAction(){
        try{
            if ($this->getRequest()->isPost()) {
                $this->_main_email=Mage::getStoreConfig('payment/withdrawmoney/master');
                $data=$this->getRequest()->getPost();    
                $blockpayment = new Workiverse_Withdrawmoney_Block_Payment;  
                
                $transaction_id =$data['transaction_id'];
                $order = Mage::getModel('sales/order');
                $order->loadByIncrementId($transaction_id);
                if (!$order->getId()) {
                    Mage::throwException('No order for processing found');
                }
                $payment     =$order->getPayment();
                $send_email       =$payment->getData("puser");
                $sender = $blockpayment->checkcustomer($send_email);
                $order_id =$order->getId();
                $amount=$order->getGrandTotal();
                $sub_total=$order->getData("subtotal");
                $boda_fee=$blockpayment->getBodaFee();
                $affilineEmail=$blockpayment->getAffilineEmail();
                $affilineData = $blockpayment->checkcustomer($affilineEmail);             
                if(!$affilineData)
                {
                     Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                     parent::_redirect("checkout/onepage/failure/");
                     return;
                }          
                $account = Mage::getModel("customer/customer");
                $account->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                $account = $account->loadByEmail($this->_main_email); 
          
                $customer_credit = $sender->getData('credit_value');
                if($customer_credit<=0||$amount < 0 || $amount > $customer_credit)
                {   
                    Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                    parent::_redirect("checkout/onepage/failure/");
                    return;
                }
                $_sender_parent_id=0;
                $_affiline_parent_id=0;
                $customer_id=$sender->getId();
                $sender->setData("credit_value",($sender->getCreditValue()-$amount))->save();
                //$sender->setCreditValue($customer_credit-$amount)->save();
                ///update affiline   
                $affilineData->setData("credit_value",($affilineData->getCreditValue()+$sub_total))->save();             
                if($affilineData->getData("id"))
                    $_affiline_parent_id=$affilineData->getData("entity_id");   
                if($sender->getData("id"))
                    $_sender_parent_id=$sender->getData("entity_id");     
                ///  
                $account->setCreditValue($account->getCreditValue()+$boda_fee)->save();    
                ///add trans 
                if(!$_sender_parent_id)
                {
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($sender->getId(),
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $sender->getEmail() .
                        " sent " . $sub_total . " credit to " . $affilineData->getEmail(), $transaction_id, -$sub_total,$transaction_id,date("Y-m-d"),$sender->getName(),
                        $_sender_parent_id,$blockpayment->getMessage(),$affilineData->getEmail());
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($sender->getId(),
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $sender->getEmail() .
                        " sent " . $boda_fee . " credit to " . $account->getName(), $transaction_id, -$boda_fee,$transaction_id,date("Y-m-d"),$sender->getName(),
                        $_sender_parent_id,$blockpayment->getMessage(),$account->getEmail());
                }
                else
                {   
                    Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($sender->getId(),
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $sender->getEmail() .
                        " sent " . $sub_total . " credit to " . $affilineData->getEmail(), $transaction_id, -$sub_total,$transaction_id,date("Y-m-d"),($sender->getFirstName()." ".$sender->getLastName()),
                        $_sender_parent_id,$blockpayment->getMessage(),$affilineData->getEmail());
                    Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($sender->getId(),
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $sender->getEmail() .
                        " sent " . $boda_fee . " credit to " . $account->getName(), $transaction_id, -$boda_fee,$transaction_id,date("Y-m-d"),($sender->getFirstName()." ".$sender->getLastName()),
                        $_sender_parent_id,$blockpayment->getMessage(),$account->getEmail());
                }
                
                
                
                Mage::getModel('customercredit/transaction')->addTransactionHistory($account->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                    "get credit value from " .$blockpayment->getReceiveName(),$transaction_id,$boda_fee,$transaction_id,date("Y-m-d"),$this->_main_email,0, null,$send_email);
               if(!$_affiline_parent_id)
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($affilineData->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                    "get credit value from " .$blockpayment->getReceiveName(),$transaction_id,$sub_total,$transaction_id,date("Y-m-d"),$affilineData->getName(),$_affiline_parent_id, null,$send_email);              
               else
                    Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($affilineData->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                    "get credit value from " .$blockpayment->getReceiveName(),$transaction_id,$sub_total,$transaction_id,date("Y-m-d"),($affilineData->getFirstName()." ".$affilineData->getLastName()),$_affiline_parent_id, null,$send_email);              
               ///done
               
               Mage::getModel('customercredit/customercredit')->sendConfirmToFriendByEmail($sub_total,$send_email,$blockpayment->getMessage());   
               Mage::getModel('customercredit/customercredit')->receiveConfirmToFriendByEmail($sub_total,$affilineData->getEmail(),$blockpayment->getMessage());
                ///Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($sub_total,$send_email,$blockpayment->getMessage());   
              // Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($boda_fee,$this->_main_email,""); 
               ////               
                parent::_redirect('gatewaywithdrawbodamoney/processing/success/transaction_id/'.$order->getIncrementId());                    
            }
        }catch (Exception $e){
            Mage::logException($e);
            echo $e->getMessage();
            //parent::_redirect("checkout/cart");
        }
    }
    /**
     * Show orderPlaceRedirect page which contains the Perfectmoney iframe.
     */
    public function paymentAction()
    {
        try {
            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
            $this->_main_email=Mage::getStoreConfig('payment/withdrawmoney/master');
            $order->loadByIncrementId($session->getLastRealOrderId());
        
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('perfectmoney')->__('The customer was redirected to WithDraw Money.')
            );
            $order->save();
            $session->getQuote()->setIsActive(false)->save();
           
            //$session->clear();
            $this->loadLayout();
		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Payment'));
		$this->renderLayout();
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    }   
    /**
     * Action to which the customer will be returned when the payment is made.
     */
    public function successAction()
    {
        $event = Mage::getModel('withdrawmoney/event')
                 ->setEventData($this->getRequest()->getParams());
        try {
            $this->_main_email=Mage::getStoreConfig('payment/withdrawmoney/master');
            $quoteId = $event->successEvent();      
            $this->_getCheckout()->setLastSuccessQuoteId($quoteId);
            //////
            $param=$this->getRequest()->getParams();
            $trans_id=$param['transaction_id'];
            $orderOBJ = Mage::getModel('sales/order')->loadByIncrementId($trans_id);
            $orderOBJ->setStatus('complete');
            $orderOBJ->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
            $history = $orderOBJ->addStatusHistoryComment(' Changed Status to Complete  ', false);
            $history->setIsCustomerNotified(true);
            $orderOBJ->save();
            $orderOBJ->getStatus();
            parent::_redirect('checkout/onepage/success');   
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
            $this->_redirect('checkout/cart');
        }
        
    }

    /**
     * Action to which the customer will be returned if the payment process is
     * cancelled.
     * Cancel order and redirect user to the shopping cart.
     */
    public function cancelAction()
    {
         $event = Mage::getModel('withdrawmoney/event')

                 ->setEventData($this->getRequest()->getParams());

        $message = $event->cancelEvent();



        // set quote to active

        $session = $this->_getCheckout();

        if ($quoteId = $session->getBodamoneyQuoteId()) {

            $quote = Mage::getModel('sales/quote')->load($quoteId);

            if ($quote->getId()) {

                $quote->setIsActive(true)->save();

                $session->setQuoteId($quoteId);

            }

        }



        $session->addError($message);

        $this->_redirect('checkout/cart');
        
    }

    /**
     * Action to which the transaction details will be posted after the payment
     * process is complete.
     */
    public function statusAction()
    {
        $event = Mage::getModel('withdrawmoney/event')
            ->setEventData($this->getRequest()->getParams());
        $message = $event->processStatusEvent();
        $this->getResponse()->setBody($message);
    }

    /**
     * Set redirect into responce. This has to be encapsulated in an JavaScript
     * call to jump out of the iframe.
     *
     * @param string $path
     * @param array $arguments
     */
    protected function _redirect($path, $arguments=array())
    {
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('withdrawmoney/redirect')
                ->setRedirectUrl(Mage::getUrl($path, $arguments))
                ->toHtml()
        );
        return $this;
    }
}
