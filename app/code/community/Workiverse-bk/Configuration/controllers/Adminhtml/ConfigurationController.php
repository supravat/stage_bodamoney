<?php
class Workiverse_Configuration_Adminhtml_ConfigurationController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        
        $this->loadLayout();
        $this->renderLayout();
    }
 
    public function newAction()
    {
         // We just forward the new action to a blank edit form
        
        $this->_forward('edit');
        
    }
 
    public function editAction()
    {  
        $this->_initAction();
     
        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('configuration/configuration');
     
        if ($id) {
            // Load record
            $model->load($id);
     
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This Configuration no longer exists.'));
                $this->_redirect('*/*/');
     
                return;
            } 
             $this->_title($model->getName()); 
        }  
     
        $this->_title($this->__('New Configuration'));
     
        $data = Mage::getSingleton('adminhtml/session')->getConfigurationData(true);
        if (!empty($data)) {
            $model->setData($data);
        }  
        
        Mage::register('configuration_data', $model);
     
        $this->_initAction()
            ->_addBreadcrumb($id ? $this->__('Edit Configuration') : $this->__('New Configuration'), $id ? $this->__('Edit Configuration') : $this->__('New Configuration'))
            ->_addContent($this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit')
            ->setData('action', $this->getUrl('*/*/save')))
            ->_addLeft($this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit_tabs')) 
            ->renderLayout();
    }
     
    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('configuration/configuration');
           
             unset($postData['form_key']);
            if(isset($postData['configuration_id'])&&$postData['configuration_id']!="")
            {
                $configuration_id=$postData['configuration_id'];    
                $postData['update_at']=date("Y-m-d"); 
                $model->load($configuration_id);
                        
            }else
            {
                $postData['created_at']=date("Y-m-d"); 
                unset($postData['configuration_id']);
               
            }
            $model->setData($postData);
            try {
                $model->save();
               
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The configuration has been saved.'));
                $this->_redirect('*/*/');
 
                return;
            }  
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this baz.'));
            }
 
            Mage::getSingleton('adminhtml/session')->setBazData($postData);
            $this->_redirectReferer();
        }
    }
 
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('configuration/configuration');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('configuration')->__('The Configuration has been deleted.'));
                $this->_redirect('*/*/');
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Unable to find the Configuration to delete.'));
        $this->_redirect('*/*/');
    }
    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('cms/configuration')
            ->_title($this->__('configuration'))->_title($this->__('Configuration'))
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Configuration'), $this->__('Configuration'));
         
        return $this;
    }
     public function messageAction()
    {
        $data = Mage::getModel('configuration/configuration')->load($this->getRequest()->getParam('id'));
        echo $data->getContent();
    }
    public function massDeleteAction()
    {
        $configuration_Ids = $this->getRequest()->getParam('configuration_ids');      // $this->getMassactionBlock()->setFormFieldName('tax_id'); from Mage_Adminhtml_Block_Tax_Rate_Grid
        if(!is_array($configuration_Ids)) {
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('configuration')->__('Please select configuration.'));
        } else {
        try {
            $configurationModel = Mage::getModel('configuration/configuration');
            foreach ($configuration_Ids as $_Id) {
                $configurationModel->load($_Id)->delete();
            }
        Mage::getSingleton('adminhtml/session')->addSuccess(
        Mage::helper('configuration')->__(
        'Total of %d record(s) were deleted.', count($taxIds)
        )
        );
        } catch (Exception $e) {
        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        }
         
        $this->_redirect('*/*/index');
    }
}
 ?>