<?php 
class Workiverse_Configuration_Block_Adminhtml_Configuration extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    
    
    public function __construct()
    {
         //where is the controller
         $this->_controller = 'adminhtml_configuration';
         $this->_blockGroup = 'workiverse_configuration';
         //text in the admin header
         $this->_headerText = 'Configuration Management';
         //value of the add button
      $this->_addButtonLabel = 'Add a Configuration';       
        parent::__construct();       
     }
}
?>