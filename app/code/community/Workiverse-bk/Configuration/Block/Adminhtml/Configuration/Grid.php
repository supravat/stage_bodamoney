<?php 
class Workiverse_Configuration_Block_Adminhtml_Configuration_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
   public function __construct()
   {
       parent::__construct();
       $this->setId('contactGrid');
       $this->setDefaultSort('configuration_id');
       $this->setDefaultDir('DESC');
       $this->setSaveParametersInSession(true);
   }
   protected function _prepareCollection()
   {
      $collection = Mage::getModel('configuration/configuration')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
    }
   protected function _prepareColumns()
   {
       $this->addColumn('configuration_id',
             array(
                    'header' => 'ID',
                    'align' =>'right',
                    'width' => '50px',
                    'index' => 'configuration_id',
               ));
       $this->addColumn('keyword', array(
                    'header' => 'KeyWord',
                    'align' =>'left',
                    'index' => 'keyword',
             ));
        
        $this->addColumn('alias_keyword', array(
                     'header' => 'Alias Keyword',
                     'align' =>'left',
                     'width' => '200px',
                     'index' => 'alias_keyword',
        ));
        $this->addColumn('position_1', array(
                     'header' => 'Position 1',
                     'align' =>'left',
                     'index' => 'position_1',
        ));
        $this->addColumn('position_2', array(
                     'header' => 'Position 2',
                     'align' =>'left',
                     'index' => 'position_2',
        ));
        $this->addColumn('position_3', array(
                     'header' => 'Position 3',
                     'align' =>'left',
                     'index' => 'position_3',
        ));
        $this->addColumn('position_4', array(
                     'header' => 'Position 4',
                     'align' =>'left',
                     'index' => 'position_4',
        ));
        
         $this->addColumn('update_at', array(
                     'header' => 'Updated at',
                     'align' =>'left',
                     'type' => 'datetime',
                     'index' => 'update_at',
        ));
         $this->addColumn('created_at', array(
                     'header' => 'Date created',
                     'align' =>'left',
                     'type' => 'datetime',
                     'index' => 'created_at',
        ));
        
        
         return parent::_prepareColumns();
    }
    public function getRowUrl($row)
    {
         return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        return null;
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('ad_configuration_id');
        $this->getMassactionBlock()->setFormFieldName('configuration_ids');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> $this->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => $this->__('Are you sure you want to delete the selected listing(s)?')
        ));
        return $this;
    }
}
?>