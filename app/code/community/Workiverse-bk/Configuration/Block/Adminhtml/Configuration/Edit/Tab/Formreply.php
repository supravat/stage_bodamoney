<?php
class Workiverse_Configuration_Block_Adminhtml_Configuration_Edit_Tab_Formreply extends Mage_Adminhtml_Block_Widget_Form
{
   protected function _prepareForm()
   {
       $form = new Varien_Data_Form();
      
       $fieldset = $form->addFieldset('configuration_form',
                                       array('legend'=>'Form Reply Message'));
        $fieldset->addField('reply_message', 'editor',
                       array(
                            'label' => 'Reply Message',
                            'style'     => 'height:15em',
                            'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
                            'wysiwyg'   => true,
                            'required'  => false,
                            'name' => 'reply_message',
                    ));        
        if ( Mage::registry('configuration_data') )
        {
            $form->setValues(Mage::registry('configuration_data')->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form); 
        return parent::_prepareForm();
 }
}