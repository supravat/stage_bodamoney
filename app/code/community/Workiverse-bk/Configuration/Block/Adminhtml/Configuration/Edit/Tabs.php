<?php 
 class Workiverse_Configuration_Block_Adminhtml_Configuration_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
     public function __construct()
     {
          parent::__construct();
          $this->setId('configuration_tabs');
          $this->setDestElementId('edit_form');
          $this->setTitle(' Configuration  Information');
      }
      protected function _beforeToHtml()
      {
          $this->addTab('form_section', array(
                   'label' => 'General Information',
                   'title' => 'General Information',
                   'content' => $this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit_tab_formgeneral')->toHtml())
                   );
                   
         $this->addTab('form_reply', array(
                   'label' => 'Reply Message',
                   'title' => 'Reply Message',
                   'content' => $this->getLayout()
                ->createBlock('workiverse_configuration/adminhtml_configuration_edit_tab_formreply')->toHtml()
                ));
         $this->addTab('form_reminder', array(
                   'label' => 'Reminder Message',
                   'title' => 'Reminder Message',
                   'content' => $this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit_tab_formreminder')
                     ->toHtml()
                     
                     ));
         $this->addTab('form_fail', array(
                   'label' => 'Fail Message',
                   'title' => 'Fail Message',
                   'content' => $this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit_tab_formfail')
                    ->toHtml()
                     
                     ));   
          $this->addTab('form_penaty', array(
                   'label' => 'Penaty Message',
                   'title' => 'Penaty Message',
                  'content' => $this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit_tab_formpenaty')
                     ->toHtml()
                     
                     ));
          $this->addTab('php_script', array(
                   'label' => 'Attribute Configuration',
                   'title' => 'Attribute Configuration',
                  'content' => $this->getLayout()->createBlock('workiverse_configuration/adminhtml_configuration_edit_tab_attribute')
                     ->toHtml()
                     
                     ));
         return parent::_beforeToHtml();
    }
    public function canShowTab()
    {
        return true;
    }
}

?>