<?php
class Workiverse_Configuration_Block_Adminhtml_Configuration_Edit_Tab_Formgeneral extends Mage_Adminhtml_Block_Widget_Form
{
   protected function _prepareForm()
   {
         $form = new Varien_Data_Form();
         $fieldset = $form->addFieldset('configuration_fieldset', array(
            'legend'    => Mage::helper('configuration')->__('General Information'),
            'class'     => 'fieldset-wide',
        ));
        if(Mage::registry('configuration_data')){
            $model=Mage::registry('configuration_data');
              $fieldset->addField('configuration_id', 'hidden', array(
                'name' => 'configuration_id',
            ));      
        }
         
       $fieldset->addField('keyword', 'text', array(
            'name'      => 'keyword',
            'label'     => Mage::helper('configuration')->__('Keyword'),
            'title'     => Mage::helper('configuration')->__('Keyword'),
            'required'  => true,
        )); 
        $fieldset->addField('alias_keyword', 'text', array(
            'name'      => 'alias_keyword',
            'label'     => Mage::helper('configuration')->__('Alias Keyword'),
            'title'     => Mage::helper('configuration')->__('Alias Keyword'),
            'required'  => true,
        )); 
        $fieldset->addField('position_1', 'text', array(
            'name'      => 'position_1',
            'label'     => Mage::helper('configuration')->__('Position 1'),
            'title'     => Mage::helper('configuration')->__('Position 1'),
            'required'  => true,
        )); 
        $fieldset->addField('position_2', 'text', array(
            'name'      => 'position_2',
            'label'     => Mage::helper('configuration')->__('Position 2'),
            'title'     => Mage::helper('configuration')->__('Position 2'),
            'required'  => false,
        )); 
        $fieldset->addField('position_3', 'text', array(
            'name'      => 'position_3',
            'label'     => Mage::helper('configuration')->__('Position 3'),
            'title'     => Mage::helper('configuration')->__('Position 3'),
            'required'  => false,
        )); 
        $fieldset->addField('position_4', 'text', array(
            'name'      => 'position_4',
            'label'     => Mage::helper('configuration')->__('Position 4'),
            'title'     => Mage::helper('configuration')->__('Position 4'),
            'required'  => false,
        ));  
         $fieldset->addField('purpose', 'text', array(
            'name'      => 'purpose',
            'label'     => Mage::helper('configuration')->__('Purpose'),
            'title'     => Mage::helper('configuration')->__('Purpose'),
            'required'  => false,
        )); 
         $fieldset->addField('sku', 'text', array(
            'name'      => 'Product',
            'label'     => Mage::helper('configuration')->__('Product'),
            'title'     => Mage::helper('configuration')->__('Product'),
            'required'  => false,
        )); 
         $fieldset->addField('description', 'textarea', array(
            'name'      => 'description',
            'label'     => Mage::helper('configuration')->__('Description'),
            'title'     => Mage::helper('configuration')->__('Description'),
            'required'  => false,
        ));     
         if ( Mage::registry('configuration_data') )
         {
            $form->setValues(Mage::registry('configuration_data')->getData());
          }
         //$form->setUseContainer(true);
         $this->setForm($form);
          return parent::_prepareForm();
 }
}