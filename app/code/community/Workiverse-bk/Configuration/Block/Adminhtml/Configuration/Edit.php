<?php 
class Workiverse_Configuration_Block_Adminhtml_Configuration_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
   {
        parent::__construct();
        $this->_objectId = 'configuration_id';
        //vwe assign the same blockGroup as the Grid Container
        $this->_blockGroup = 'workiverse_configuration';
        //and the same controller
        $this->_controller = 'adminhtml_configuration';
        //define the label for the save and delete button
        $this->_updateButton('save', 'label','save configuration');
        $this->_updateButton('delete', 'label', 'delete configuration');
    }
       /* Here, we're looking if we have transmitted a form object,
          to update the good text in the header of the page (edit or add) */
    public function getHeaderText()
    {
        if( Mage::registry('configuration_data')&&Mage::registry('configuration_data')->getId())
         {
              return 'configuration  '.$this->htmlEscape(
              Mage::registry('configuration_data')->getTitle()).'<br />';
         }
         else
         {
             return 'Add a Configuration';
         }
    }
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
}
}
?>