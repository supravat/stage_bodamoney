<?php 
 class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
  {
     public function __construct()
     {
          parent::__construct();
          $this->setId('helpernotify_tabs');
          $this->setDestElementId('edit_form');
          $this->setTitle(' Helpernotify  Information');
      }
      protected function _beforeToHtml()
      {
          $this->addTab('form_section', array(
                   'label' => 'General Information',
                   'title' => 'General Information',
                   'content' => $this->getLayout()->createBlock('workiverse_helpernotify/adminhtml_helpernotify_edit_tab_formgeneral')->toHtml())
                   );
                   
         $this->addTab('form_attribute', array(
                   'label' => 'Content Message',
                   'title' => 'Content Message',
                   'content' => $this->getLayout()
                ->createBlock('workiverse_helpernotify/adminhtml_helpernotify_edit_tab_attribute')->toHtml()
                ));
         
         return parent::_beforeToHtml();
    }
    public function canShowTab()
    {
        return true;
    }
}

?>