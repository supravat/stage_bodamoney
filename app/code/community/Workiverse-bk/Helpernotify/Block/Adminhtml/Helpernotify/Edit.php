<?php 
class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
   {
        parent::__construct();
        $this->_objectId = 'helpernotify_id';
        //vwe assign the same blockGroup as the Grid Container
        $this->_blockGroup = 'workiverse_helpernotify';
        //and the same controller
        $this->_controller = 'adminhtml_helpernotify';
        //define the label for the save and delete button
        $this->_updateButton('save', 'label','save helpernotify');
        $this->_updateButton('delete', 'label', 'delete helpernotify');
    }
       /* Here, we're looking if we have transmitted a form object,
          to update the good text in the header of the page (edit or add) */
    public function getHeaderText()
    {
        if( Mage::registry('helpernotify_data')&&Mage::registry('helpernotify_data')->getId())
         {
              return 'helpernotify  '.$this->htmlEscape(
              Mage::registry('helpernotify_data')->getTitle()).'<br />';
         }
         else
         {
             return 'Add a Helpernotify';
         }
    }
    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
}
}
?>