<?php 
class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
   public function __construct()
   {
       
       parent::__construct();
       $this->setId('contactGrid');
       $this->setDefaultSort('helpernotify_id');
       $this->setDefaultDir('DESC');
       $this->setSaveParametersInSession(true);
   }
   protected function _prepareCollection()
   {
      $collection = Mage::getModel('helpernotify/helpernotify')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
    }
   protected function _prepareColumns()
   {
       $this->addColumn('helpernotify_id',
             array(
                    'header' => 'ID',
                    'align' =>'right',
                    'width' => '50px',
                    'index' => 'helpernotify_id',
               ));
       $this->addColumn('keyword', array(
                    'header' => 'KeyWord',
                    'align' =>'left',
                    'index' => 'keyword',
             ));
        
        
        
         $this->addColumn('update_at', array(
                     'header' => 'Updated at',
                     'align' =>'left',
                     'type' => 'datetime',
                     'index' => 'update_at',
        ));
         $this->addColumn('created_at', array(
                     'header' => 'Date created',
                     'align' =>'left',
                     'type' => 'datetime',
                     'index' => 'created_at',
        ));
        
        
         return parent::_prepareColumns();
    }
    public function getRowUrl($row)
    {
         return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        return null;
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('ad_helpernotify_id');
        $this->getMassactionBlock()->setFormFieldName('helpernotify_ids');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> $this->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('' => '')),
            'confirm' => $this->__('Are you sure you want to delete the selected listing(s)?')
        ));
        return $this;
    }
}
?>