<?php
class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify_Edit_Tab_Attribute extends Mage_Adminhtml_Block_Widget_Form
{
    
   protected function _prepareForm()
   {
       $form = new Varien_Data_Form();
     
       $fieldset = $form->addFieldset('helpernotify_form',
                                       array('legend'=>'Content Message'));
       $fieldset->addField('content', 'textarea', array(
            'name'      => 'content',
            'label'     => Mage::helper('helpernotify')->__('Content'),
            'title'     => Mage::helper('helpernotify')->__('Content'),
            'required'  => false,
        ));  
     
            
     if ( Mage::registry('helpernotify_data') )
     {
            $form->setValues(Mage::registry('helpernotify_data')->getData());
     }
      $this->setForm($form);
      return parent::_prepareForm();
 }
}