<?php 
class Workiverse_Helpernotify_Block_Adminhtml_Helpernotify_Edit_Form extends
                          Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Init class
     */
    public function __construct()
    {  
        parent::__construct();
     
        $this->setId('workiverse_helpernotify_helpernotify_form');
        $this->setTitle($this->__('Helpernotify Information'));
    }   
    protected function _prepareForm()
    {
         $model = Mage::registry('helpernotify_data');
         $form = new Varien_Data_Form(
                array(
                  'id' => 'edit_form',
                  'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))
                     ),
                 'enctype' => 'multipart/form-data',    
                 'method' => 'post',
                 )
              );
       
      //if (isset($model)&&$model->getId())    
       // $form->setValues($model->getData());  
      $form->setUseContainer(true);
      $this->setForm($form);
      return parent::_prepareForm();
   }
}