<?php
$installer = $this;
$installer->startSetup();
$installer->run("
 
ALTER TABLE `{$installer->getTable('sales/quote_payment')}` ADD `perfectmoney_type` VARCHAR(10) NOT NULL ;
ALTER TABLE `{$installer->getTable('sales/quote_payment')}` ADD `perfectmoney_user` VARCHAR(255) NOT NULL ;
 
ALTER TABLE `{$installer->getTable('sales/order_payment')}` ADD `perfectmoney_type` VARCHAR(10) NOT NULL ;
ALTER TABLE `{$installer->getTable('sales/order_payment')}` ADD `perfectmoney_user` VARCHAR(255) NOT NULL ;
 
");
$installer->endSetup();
