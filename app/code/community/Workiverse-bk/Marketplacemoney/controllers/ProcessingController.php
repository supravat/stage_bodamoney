<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Workiverse
 * @package     Workiverse_Marketplacemoney
 * @copyright   Copyright (c) 2012 Workiverse Org (http://www.forummods.org)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Workiverse_Marketplacemoney_ProcessingController extends Mage_Core_Controller_Front_Action
{
    private $_main_email="";
  
    /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Iframe page which submits the payment data to Marketplacemoney.
     */
    public function placeformAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function customerAction()
    {
       $this->loadLayout();
       $this->renderLayout();
    }
    public function gatewayAction()
    {
       $this->loadLayout();
       $this->renderLayout();
       $this->getLayout()->getBlock('head')->setTitle($this->__('Payment Information'));
    }
     public function infoAction()
    {
           
          try {
                if ($this->getRequest()->isPost()) {
                    $this->_main_email=Mage::getStoreConfig('payment/marketplacemoney/master');
                    $order = Mage::getModel('sales/order');
                 
                    $data=$this->getRequest()->getPost();
                    $transaction_id =$data['transaction_id']?$data['transaction_id']:"";
                    $order->loadByIncrementId($transaction_id);
                   
                    if (!$order->getId()) {
                        Mage::throwException('No order for processing found');
                    }
                    $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                        Mage::helper('sendingmoney')->__('The customer was redirected to Perfect Money.')
                    );
                    $order->save();
                    $session = $this->_getCheckout();
                    $session->getQuote()->setIsActive(false)->save();          
                    $session->clear();
                    $this->loadLayout();
        		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Info'));
        		  $this->renderLayout();
            }
            } catch (Exception $e){
                Mage::logException($e);
                echo  $e->getMessage();
                //parent::_redirect('checkout/cart');
            }
    }
    public function doChargeAction(){
        try{
            if ($this->getRequest()->isPost()) {   
                $this->_main_email=Mage::getStoreConfig('payment/marketplacemoney/master');
                $data=$this->getRequest()->getPost();
              
                $orderIncrementId =$data['transaction_id']?$data['transaction_id']:"";
               
                if(!$orderIncrementId||$orderIncrementId==null)
                {
                    Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                    parent::_redirect("checkout/onepage/failure/");
                    return ;
                }
               
                $order=Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
                $order_id =$order->getId();
                $blockpayment = new Workiverse_Marketplacemoney_Block_Payment;       
                $amount=$blockpayment->getBodaFee();
                $grand_total =$order->getGrandTotal();
                $sub_total=$order->getData("subtotal");
                $payment  =$order->getPayment();
                $receive   =$blockpayment->getSeller();  
                  
                $message_detail	= "";
                /*if(isset($data['frm_contact'])&&$data['frm_contact']!="")
                  $message_detail.="Contact/Phone:{$data['frm_contact']}|";
                if(isset($data['frm_customer_name'])&&$data['frm_customer_name']!="")
                  $message_detail.="Yours Names:{$data['frm_customer_name']}|";
                if(isset($data['frm_referce'])&&$data['frm_referce']!="")
                  $message_detail.="Customer reference number *:{$data['frm_referce']}|";
                if(isset($data['frm_bill_reference'])&&$data['frm_bill_reference']!="")
                  $message_detail.="BILL REFERENCE:{$data['frm_bill_reference']}|";      
                */          
                $customer_id       =$order->getCustomerId();  
                $email_customer    =$order->getCustomerEmail();
                $affiline       =$blockpayment->checkcustomer($email_customer);
            
                ////$affiline       =Mage::getModel("customer/customer")->load($customer_id);        
                if(!$affiline)
                {
                     Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                     parent::_redirect("checkout/onepage/failure/");
                     return ;
                }
                $parent_affiline_id=0;
                if($affiline->getData("id"))
                {
                    $parent_affiline_id=$affiline->getData("entity_id");
                
                }
                
                $customer_credit = $affiline->getData('credit_value');
            
                $account = Mage::getModel("customer/customer");
                $account->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                $account = $account->loadByEmail($this->_main_email); 
                
                if($customer_credit<=0||$grand_total < 0 || $grand_total > $customer_credit)
                {
                    Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                    parent::_redirect("checkout/onepage/failure/");
                    return ;
                }
                 
                if($receive){
                    
                    $receive->setData("credit_value",($receive->getCreditValue()+$sub_total))->save();  
                    $parent_id =0;
                    if($receive->getData("id"))
                    $parent_id=$receive->getData("entity_id");
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($receive->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                        "get credit value from " . $affiline->getName(),$orderIncrementId,$sub_total,$orderIncrementId,date("Y-m-d "),$blockpayment->getSellerName(),$parent_id,$message_detail,$affiline->getEmail());
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($affiline->getId(),
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                        " sent " . $sub_total . " credit to " .$blockpayment->getSellerName(),$orderIncrementId, -$sub_total,$orderIncrementId,date("Y-m-d "),$affiline->getName(),
                        $parent_affiline_id,"",$blockpayment->getSellerEmail());
                
                }                                
                $affiline->setData("credit_value",($affiline->getCreditValue()-$grand_total))->save();   
                if($amount>0)   
                $account->setData("credit_value",($account->getCreditValue()+$amount))->save();                 
                ////send money to customer
                Mage::getModel('customercredit/customercredit')->sendConfirmToFriendByEmail($sub_total,$affiline->getEmail(),$message_detail);     
                Mage::getModel('customercredit/customercredit')->receiveConfirmToFriendByEmail($sub_total,$blockpayment->getSellerEmail(),$message_detail);
                ///add trans  
                if($amount>0)
                {                    
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($account->getId(), Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                        "get credit value from '" . $affiline->getName() . "'",$orderIncrementId, $amount,$orderIncrementId,date("Y-m-d"),$account->getName(),0,null,$affiline->getEmail());
                    
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($affiline->getId(),
                    Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $affiline->getEmail() .
                        " sent " . $amount . " credit to " . $account->getName(),$orderIncrementId, -$amount,$orderIncrementId,date("Y-m-d "),$affiline->getName(),
                        $parent_affiline_id,"",$this->_main_email);
                }
                
               //done
             
                parent::_redirect('gatewaymarketpaceamoney/processing/success/transaction_id/'.$order->getIncrementId());
                    
            }
        }catch (Exception $e){
            Mage::logException($e);
            echo $e->getMessage();
            // parent::_redirect("onepagecheckout/index/failure/");
        }
    }  
    /**
     * Show orderPlaceRedirect page which contains the Marketplacemoney iframe.
     */
    public function paymentAction()
    {
        try {
            $this->_main_email=Mage::getStoreConfig('payment/marketplacemoney/master');
            $session = $this->_getCheckout();
            $order = Mage::getModel('sales/order');
          
            $order->loadByIncrementId($session->getLastRealOrderId());
        
            if (!$order->getId()) {
                Mage::throwException('No order for processing found');
            }
            $order->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT, Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                Mage::helper('marketplacemoney')->__('The customer was redirected to Perfect Money.')
            );
            $order->save();
            $session->getQuote()->setIsActive(false)->save();
           
            //$session->clear();
            $this->loadLayout();
		    $this->getLayout()->getBlock('head')->setTitle($this->__('Boday GateWay Payment'));
		$this->renderLayout();
        } catch (Exception $e){
            Mage::logException($e);
            parent::_redirect('checkout/cart');
        }
    }      
    /**
     * Action to which the customer will be returned when the payment is made.
     */
    public function successAction()
    {
        $this->_main_email=Mage::getStoreConfig('payment/marketplacemoney/master');
        $event = Mage::getModel('marketplacemoney/event')
                 ->setEventData($this->getRequest()->getParams());
        try {
            $quoteId = $event->successEvent();
            $this->_getCheckout()->setLastSuccessQuoteId($quoteId);
            //////
            $param=$this->getRequest()->getParams();
            $trans_id=$param['transaction_id'];
            $orderOBJ = Mage::getModel('sales/order')->loadByIncrementId($trans_id);
            $orderOBJ->setStatus('complete');
            $orderOBJ->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
            $history = $orderOBJ->addStatusHistoryComment(' Changed Status to Complete  ', false);
            $history->setIsCustomerNotified(true);
            $orderOBJ->save();
            ////
            parent::_redirect('checkout/onepage/success');            
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckout()->addError($e->getMessage());
        } catch(Exception $e) {
            Mage::logException($e);
        }
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the customer will be returned if the payment process is
     * cancelled.
     * Cancel order and redirect user to the shopping cart.
     */
    public function cancelAction()
    {
        $event = Mage::getModel('marketplacemoney/event')
                 ->setEventData($this->getRequest()->getParams());
        $message = $event->cancelEvent();

        // set quote to active
        $session = $this->_getCheckout();
        if ($quoteId = $session->getMarketplacemoneyQuoteId()) {
            $quote = Mage::getModel('sales/quote')->load($quoteId);
            if ($quote->getId()) {
                $quote->setIsActive(true)->save();
                $session->setQuoteId($quoteId);
            }
        }

        $session->addError($message);
        $this->_redirect('checkout/cart');
    }

    /**
     * Action to which the transaction details will be posted after the payment
     * process is complete.
     */
    public function statusAction()
    {
        $event = Mage::getModel('marketplacemoney/event')
            ->setEventData($this->getRequest()->getParams());
        $message = $event->processStatusEvent();
        $this->getResponse()->setBody($message);
    }

    /**
     * Set redirect into responce. This has to be encapsulated in an JavaScript
     * call to jump out of the iframe.
     *
     * @param string $path
     * @param array $arguments
     */
    protected function _redirect($path, $arguments=array())
    {
        $this->getResponse()->setBody(
            $this->getLayout()
                ->createBlock('marketplacemoney/redirect')
                ->setRedirectUrl(Mage::getUrl($path, $arguments))
                ->toHtml()
        );
        return $this;
    }
}
