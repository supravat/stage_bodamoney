<?php
 
class MCorner_Checkout_Model_Observer extends Varien_Object
{
 
    public function applyCoupon(Varien_Event_Observer $observer) {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getId();                    
        if ($customerId&&Mage::getStoreConfig('affiliateplus/coupon/enable') 
        && Mage::helper('affiliatepluscoupon')->isPluginEnabled()){	
                if(!Mage::registry('program_coupon_codes')){  
                    ///////////
            		$account = Mage::helper('affiliateplus/account')->getAccount();                    
            		$accountId = $account->getId();
                   
            		$coupon = Mage::getModel('affiliatepluscoupon/coupon')->setCurrentAccountId($accountId);
            		$helper = Mage::helper('affiliatepluscoupon');
            		
            		$coupon->loadByProgram();
            		if (!$coupon->getId()){
            			try {
            				$coupon->setCouponCode($helper->generateNewCoupon())
            					->setAccountName($account->getName())
            					->setProgramName('Affiliate Program')
            					->save();
            			} catch (Exception $e){}
            		}
            		$account->setCouponCode($coupon->getCouponCode());
            		Mage::register('account_model',$account);
            		
            		if (Mage::helper('affiliatepluscoupon')->isMultiProgram()){   
            			$programs = Mage::getResourceModel('affiliateplusprogram/account_collection')
                			->addFieldToFilter('account_id',$accountId);
            			$pCouponCodes = array();
            			foreach ($programs as $accProgram){
            				$program = Mage::getModel('affiliateplusprogram/program')
            					->setStoreId(Mage::app()->getStore()->getId())
            					->load($accProgram->getProgramId());
            				if (!$program->getUseCoupon() || !floatval($program->getDiscount())) continue;
            				$coupon->setId(null)->loadByProgram($accProgram->getProgramId());
            				if (!$coupon->getId()){
            					try {
            						$coupon->setCouponCode($helper->generateNewCoupon($program->getCouponPattern()))
            							->setAccountName($account->getName())
            							->setProgramName($program->getName())
            							->save();
            					} catch (Exception $e){}
            				}
            				if ($coupon->getCouponCode()) $pCouponCodes[$program->getId()] = $coupon->getCouponCode();
            			}
            			Mage::register('program_coupon_codes',$pCouponCodes);
            		  }
                }
                 $programCoupons = Mage::registry('program_coupon_codes');
                 $Observer =new Magestore_Affiliatepluscoupon_Model_Observer;
                 //$arr      =get_class_methods($Observer);
                /// print_R($arr); 
                 $program_obj= new Magestore_Affiliatepluscoupon_Block_Affiliatepluscoupon;
                 $listPrograms   =$program_obj->getListProgram();
                 $session = Mage::getSingleton('checkout/session');
                 $affilateData = $session->getData('affiliate_coupon_data');
              
                 if (count($listPrograms))
                 {                                
                     foreach ($listPrograms as $program)
                     {                                
                        $coupon_code =$program->getCouponCode();
                        if (!$affilateData && !is_array($affilateData)&&!isset($affilateData['program_id'])){    
                           $Observer->couponNoPostAction($coupon_code);                           
                        }
                    } 
                     
                 }
            
        }
    }
    
}