<?php
require_once ("Mage/Customer/controllers/AccountController.php");
class MageB2B_Sublogin_AccountController extends Mage_Customer_AccountController
{
    public function preDispatch()
    {
        parent::preDispatch();
        $_726da6d3022af6314a6aa35c59981f2654ef1b69 = $this->getRequest()->getActionName();
        if ($_726da6d3022af6314a6aa35c59981f2654ef1b69 == 'refreshsublogin') {
            $this->_getSession()->setNoReferer(true);
            $this->setFlag('', 'no-dispatch', false);
        }

    }

    public function forgotPasswordPostAction()
    {
        /////////////
         $reset_pin1 = (string) $this->getRequest()->getPost('reset_pin1');
         if ($reset_pin1) {
        ////////////
            $helper = Mage::helper('bodacustomer');
            $by = ($reset_pin1 == 'mobile') ? 'Mobile Number' : 'Email Address';
            $reset_pin1_by = (string) $this->getRequest()->getPost("reset_pin1_$reset_pin1");
            if (!$reset_pin1_by) {
                $this->_getSession()->addError($this->__("Please enter your $by."));
                $this->_redirect('*/*/forgotpassword');
                return;
            }
            if ($reset_pin1 == 'email') {
                if (!Zend_Validate::is($reset_pin1_by, 'EmailAddress')) {
//                    $this->_getSession()->setForgottenEmail($reset_pin1_by);
                    $this->_getSession()->addError($this->__('Invalid email address.'));
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            if (!Mage::getStoreConfig('sublogin/email/old_forgot'))
                return parent::forgotPasswordPostAction();
            //$customer_input = $this->getRequest()->getPost('email');
            if ($reset_pin1 == 'email') {
                $customer_input=$reset_pin1_by;
            }elseif($reset_pin1 == 'mobile')
               $customer_input=trim($reset_pin1_by."@bodamoney.com");            
               ///die($customer_input);
                if ($customer_input) {
                    if (!Zend_Validate::is($customer_input,'EmailAddress')) {
                        $this->_getSession()->setForgottenEmail($customer_input);
                         if ($reset_pin1 == 'email') {
                            $this->_getSession()->addError($this->__('Invalid email address.'));
                        }else{
                            $this->_getSession()->addError($this->__('Invalid PhoneNumber.'));  
                        }
                        $this->getResponse()->setRedirect(Mage::getUrl('*/*/forgotpassword'));
                        return;
                    }
                    //die($customer_input);
                    $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($customer_input);
                    if ($customer->getId()) {
                        try {
                            //$_a4c20b3df57a6a1b409d16274aafd91b35447cee = $customer->generatePassword();
                            $newResetPasswordLinkToken = $this->_randompassword();
                          
                            $customer->changePassword($newResetPasswordLinkToken, false);
          
                            $customer->setPassword($newResetPasswordLinkToken);
                            $customer->setStoreId(Mage::app()->getStore()->getId());        
                            if ($reset_pin1 == 'email') {
                                $customer->sendPasswordReminderEmail();
                            }elseif($reset_pin1 == 'mobile'){
                                $mobile=$customer_input;
                                ///$customer->sendPIN1ChangeRequestEmail($customer_input);
                                $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
                                $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
                                $phone=str_replace("@bodamoney.com","",$mobile);
                                $phone=str_replace("+","",$phone);
                                $target=$phone;
                                $message = "NewPassword:$newResetPasswordLinkToken";
                              
                                shell_exec ("php sms/index.php $username  $password  $target  $message");
                            }
                            $this->_getSession()->addSuccess($this->__('A new password has been sent.'));
                            $this->getResponse()->setRedirect(Mage::getUrl('*/*'));
                            return;
                        }
                        catch (exception $_68dc3926fdadefc4ea3e5e09bf1dd6f6e1785aa5) {
                            $this->_getSession()->addError($_68dc3926fdadefc4ea3e5e09bf1dd6f6e1785aa5->
                                getMessage());
                        }
                    } else {
                        if ($reset_pin1 == 'email') 
                            $this->_getSession()->addError($this->__('This email address was not found in our records.'));
                        else
                            $this->_getSession()->addError($this->__('This mobile number was not found in our records.'));
                        $this->_getSession()->setForgottenEmail($customer_input);
                    }
                } else {
                    $this->_getSession()->addError($this->__('Please enter your email.'));
                    $this->getResponse()->setRedirect(Mage::getUrl('*/*/forgotpassword'));
                    return;
                }
            //}
        }
        $this->getResponse()->setRedirect(Mage::getUrl('*/*/forgotpassword'));
    }

    public function resetPasswordAction()
    {
        $_1193a032c60aa21cb0323d5e50afd4c3965510f1 = (string )$this->getRequest()->
            getQuery('token');
        $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d = (int)$this->getRequest()->getQuery('id');

        $customer_input = $this->getRequest()->getQuery('email');

        $this->_getSession()->setData('resetPasswordEmail', $customer_input);
        $_4c08962d83c4a8d0b8e15398c5db95ab58b3cf62 = Mage::getModel('customer/customer')->
            load($_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d);
        $customer = Mage::getModel('customer/customer')->
            setWebsiteId($_4c08962d83c4a8d0b8e15398c5db95ab58b3cf62->getWebsiteId())->
            loadByEmail($customer_input);

        if (!$customer->getId())
            $customer = $_4c08962d83c4a8d0b8e15398c5db95ab58b3cf62;

        try {

            $this->_validateResetPasswordLinkToken($customer,
                $_1193a032c60aa21cb0323d5e50afd4c3965510f1);

            $this->loadLayout();

            $this->getLayout()->getBlock('resetPassword')->setCustomerId($_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d)->
                setResetPasswordLinkToken($_1193a032c60aa21cb0323d5e50afd4c3965510f1);
            $this->renderLayout();
        }
        catch (exception $_096e526fb587435687d8b326c166325c87efa974) {
            $this->_getSession()->addError(Mage::helper('customer')->__('Your password reset link has expired.'));
            $this->_redirect('*/*/');
        }
    }

    public function resetPasswordPostAction()
    {
        $_1193a032c60aa21cb0323d5e50afd4c3965510f1 = (string )$this->getRequest()->
            getQuery('token');
        $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d = (int)$this->getRequest()->getQuery('id');
        $_506da6907f960f50cad09ca45512519f91515237 = (string )$this->getRequest()->
            getPost('password');
        $_a12b972da4e44447d3534c51bf5e9b5c2f596030 = (string )$this->getRequest()->
            getPost('confirmation');

        $customer_input = $this->_getSession()->getData('resetPasswordEmail');
        $_4c08962d83c4a8d0b8e15398c5db95ab58b3cf62 = Mage::getModel('customer/customer')->
            load($_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d);
        $customer = Mage::getModel('customer/customer')->
            setWebsiteId($_4c08962d83c4a8d0b8e15398c5db95ab58b3cf62->getWebsiteId())->
            loadByEmail($customer_input);

        if (!$customer->getId())
            $customer = $_4c08962d83c4a8d0b8e15398c5db95ab58b3cf62;
        try {
            $this->_validateResetPasswordLinkToken($customer,
                $_1193a032c60aa21cb0323d5e50afd4c3965510f1);

        }
        catch (exception $_096e526fb587435687d8b326c166325c87efa974) {
            $this->_getSession()->addError(Mage::helper('customer')->__('Your password reset link has expired.'));
            $this->_redirect('*/*/');
            return;
        }
        $_9dcc3b3207ac666e4c174411da26b17170543b06 = array();
        if (iconv_strlen($_506da6907f960f50cad09ca45512519f91515237) <= 0) {
            array_push($_9dcc3b3207ac666e4c174411da26b17170543b06, Mage::helper('customer')->
                __('New password field cannot be empty.'));
        }


        $customer->setPassword($_506da6907f960f50cad09ca45512519f91515237);
        $customer->setConfirmation($_a12b972da4e44447d3534c51bf5e9b5c2f596030);
        $_b7542ebd2d63afe458fada9dae566393ebbae000 = $customer->
            validate();
        if (is_array($_b7542ebd2d63afe458fada9dae566393ebbae000)) {
            $_9dcc3b3207ac666e4c174411da26b17170543b06 = array_merge($_9dcc3b3207ac666e4c174411da26b17170543b06,
                $_b7542ebd2d63afe458fada9dae566393ebbae000);
        }
        if (!empty($_9dcc3b3207ac666e4c174411da26b17170543b06)) {
            $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
            foreach ($_9dcc3b3207ac666e4c174411da26b17170543b06 as $_d6efd9fa1f666cfead3d8f61ea3ffc81d285726b) {
                $this->_getSession()->addError($_d6efd9fa1f666cfead3d8f61ea3ffc81d285726b);
            }
            $this->_redirect('*/*/resetpassword', array('id' => $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d,
                    'token' => $_1193a032c60aa21cb0323d5e50afd4c3965510f1));
            return;
        }
        try {

            $customer->setRpToken(null);
            $customer->setRpTokenCreatedAt(null);
            $customer->setConfirmation(null);
            $customer->save();
            $this->_getSession()->addSuccess(Mage::helper('customer')->__('Your password has been updated.'));
            $this->_redirect('*/*/login');
        }
        catch (exception $_096e526fb587435687d8b326c166325c87efa974) {
            $this->_getSession()->addException($_096e526fb587435687d8b326c166325c87efa974, $this->
                __('Cannot save a new password.'));
            $this->_redirect('*/*/resetpassword', array('id' => $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d,
                    'token' => $_1193a032c60aa21cb0323d5e50afd4c3965510f1));
            return;
        }
    }


    protected function _validateResetPasswordLinkToken($customer,
        $_1193a032c60aa21cb0323d5e50afd4c3965510f1)
    {
        if (!is_string($_1193a032c60aa21cb0323d5e50afd4c3965510f1) || empty($_1193a032c60aa21cb0323d5e50afd4c3965510f1)) {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid password reset token.'));
        }

        if (!$customer || !$customer->
            getId()) {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Wrong customer account specified.'));
        }
        $_1dabec2283d85c7ca0103a68c05263dcfb258e90 = $customer->
            getRpToken();
        if (strcmp($_1dabec2283d85c7ca0103a68c05263dcfb258e90, $_1193a032c60aa21cb0323d5e50afd4c3965510f1) !=
            0 || $customer->
            isResetPasswordLinkTokenExpired()) {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Your password reset link has expired.'));
        }
    }


    public function refreshsubloginAction()
    {
        $_7661e073459e96b1adbc480bebcb023560a54a58 = Mage::getStoreConfig('sublogin/general/expire_interval');
        $_ce376578098cf70af7021fca72fe4e15a4a98365 = (int)$this->getRequest()->getQuery('id');
        $_1193a032c60aa21cb0323d5e50afd4c3965510f1 = (string )$this->getRequest()->
            getQuery('token');
        $_4e45fa4f3249d9c3cc1314ed89922593008cf117 = Mage::getModel('sublogin/sublogin')->
            load($_ce376578098cf70af7021fca72fe4e15a4a98365);
        if ($_4e45fa4f3249d9c3cc1314ed89922593008cf117->getId()) {
            if ($_4e45fa4f3249d9c3cc1314ed89922593008cf117->getRpToken() == $_1193a032c60aa21cb0323d5e50afd4c3965510f1) {
                $_4e45fa4f3249d9c3cc1314ed89922593008cf117->setExpireDate(strtotime(date("Y-m-d",
                    strtotime(date('Y-m-d'))) . " + " . $_7661e073459e96b1adbc480bebcb023560a54a58 .
                    " days"));
                $_4e45fa4f3249d9c3cc1314ed89922593008cf117->setActive(1);
                $this->_getSession()->addSuccess(Mage::helper('sublogin')->__('Successfully refreshed this account'));
            } else
                $this->_getSession()->addError(Mage::helper('sublogin')->__('Your account refresh link has expired - try to login to get another email.'));
            $_4e45fa4f3249d9c3cc1314ed89922593008cf117->setRpToken(null);
            $_4e45fa4f3249d9c3cc1314ed89922593008cf117->setRpTokenCreatedAt(null);
            $_4e45fa4f3249d9c3cc1314ed89922593008cf117->save();
        } else
            $this->_getSession()->addError(Mage::helper('sublogin')->__('This account doesn\'t exist anymore and can therefore not be refreshed'));

        $this->_redirect('*/*/login');
    }
    public function _randompassword(){
        $characters =array(0,1,2,3,4,5,6,7,8,9);
       $result = '';
       for ($i = 0; $i < 4; $i++){
            $result .= $characters[mt_rand(0,9)];
       }
       if(strlen($result)==4)
        return $result;   
       else
       return $this->_randompassword();
    }
}
