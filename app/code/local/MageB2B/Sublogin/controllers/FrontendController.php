<?php

class MageB2B_Sublogin_FrontendController extends
    Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();
        if (!Mage::getSingleton('customer/session')->authenticate($this)) {
            $this->setFlag('', 'no-dispatch', true);
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('My Sublogins'));
        if ($_d1b5a9e6d88844d06736cc808299e4fce5599202 = $this->getLayout()->getBlock('customer.account.link.back')) {
            $_d1b5a9e6d88844d06736cc808299e4fce5599202->setRefererUrl($this->_getRefererUrl
                ());
        }
        $this->renderLayout();
    }

    public function createAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $expire_interval = Mage::getStoreConfig('sublogin/general/expire_interval');
        $_sublogin_id = $this->getRequest()->getParam('id');
        $_sublogin = Mage::getModel('sublogin/sublogin')->load($_sublogin_id);
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($this->hasAccess($_sublogin) && ($_sublogin->getId() || $_sublogin_id == 0)) {
            $_sublogin->setData('expire_date', $this->unFormatDate($_sublogin->getData('expire_date')));
            if ($this->getRequest()->isPost()) {
                $_6fb02c14a9aa8279c0be3446f87b5153ed38c016 = false;
                $_02000d93f53443c66f188e6620797faaaeba5739 = false;

                $_791b80c93437c3a51c059f29b3d495ff07ca01e2 = array(
                    'firstname',
                    'lastname',
                    'email',
                    'active',
                    'transfer_limit',
                    'is_affiliate',
                    'is_marketing',
                    'group_id',
                    'id_number',
                    'driving_license_number',
                    'carregisternumber',
                    'kamome_customer',
                    'nf_card',
                    'contact_email',
                    'contact_whatapp',
                    'contact_telegram',
                    'contact_confirm_ccount'
                    
                    );
                $_2a145552e00905331cd502938610b699e18379e0 = array(
                    'firstname',
                    'lastname',
                    'email',
                    //'transfer_limit',
                    //'is_affiliate',
                    //'is_marketing'
                    );
                $_da9741d865f8de50c427250ecfa2890ae98ae8d0 = false;
                if ($_sublogin_id == 0) {
                    $_2a145552e00905331cd502938610b699e18379e0[] = 'password';
                    $_02000d93f53443c66f188e6620797faaaeba5739 = true;
                }
                foreach ($_2a145552e00905331cd502938610b699e18379e0 as $item) {
                    if (!$this->getRequest()->getParam($item)) {
                        $_da9741d865f8de50c427250ecfa2890ae98ae8d0 = true;
                        Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('%s is required',$this->__($item)));
                    }
                }

                $email = $this->getRequest()->getParam('email');
                if ($_sublogin->getData('email') != $email) {
                    if (!Mage::helper('sublogin')->validateUniqueEmail($email,$customer->getWebsiteId())) {
                        Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('Email "%s" already exists',
                            $email));
                        $_da9741d865f8de50c427250ecfa2890ae98ae8d0 = true;
                    }
                }
                if ($_da9741d865f8de50c427250ecfa2890ae98ae8d0) {
                    $this->_redirect('sublogin/frontend/edit', array('id' => $_sublogin->getId()));
                    return;
                }

                foreach ($_791b80c93437c3a51c059f29b3d495ff07ca01e2 as $item) {
                    $_sublogin->setData($item,$this->getRequest()->getParam($item));
                }
                ///add group id
                ////
                if ($this->getRequest()->getParam('address_ids')) {
                    $_sublogin->setData('address_ids', implode(',',
                        $this->getRequest()->getParam('address_ids')));
                }
                if ($this->getRequest()->getParam('password')) {
                    $_sublogin->setData('password', Mage::helper('core')->
                        getHash($this->getRequest()->getParam('password'), 2));
                    $_6fb02c14a9aa8279c0be3446f87b5153ed38c016 = true;
                }
                if ($this->getRequest()->getParam('expire_date')) {
                    $_f0c57a04a51d1d900c8d28a111ab8f340a17465b = $this->formatDate($this->
                        getRequest()->getParam('expire_date'));
                    $_sublogin->setData('expire_date', $_f0c57a04a51d1d900c8d28a111ab8f340a17465b);
                } else {

                    $_sublogin->setData('expire_date', date('Y-m-d',
                        time() + 60 * 60 * 24 * $expire_interval));
                }
              
                $_sublogin->setData('entity_id', Mage:: getSingleton('customer/session')->getData('id'));
               
                $_sublogin->save();
                $email=$_sublogin->getEmail();
                $email=str_replace("@bodamoney.com","",$email);
                Mage::getSingleton('core/session')->addSuccess(Mage::helper('sublogin')->__('Successfully saved sublogin %s',$email));

                if ($_6fb02c14a9aa8279c0be3446f87b5153ed38c016 || $_02000d93f53443c66f188e6620797faaaeba5739) {

                    $_sublogin->setPassword($this->getRequest()->getParam('password'));
                    Mage::getModel('core/session')->setData('mpassword',$this->getRequest()->getParam('password'));
               
                    $_sublogin->setStoreId(Mage::app()->getStore()->getId());
                    if ($_02000d93f53443c66f188e6620797faaaeba5739)
                        Mage::helper('sublogin')->sendNewSubloginEmail($_sublogin);
                    else
                        if ($_6fb02c14a9aa8279c0be3446f87b5153ed38c016)
                            Mage::helper('sublogin')->sendNewPasswordEmail($_sublogin);
                }
                $this->_redirect('sublogin/frontend/edit', array('id' => $_sublogin->
                        getId()));
                return;
            }
            Mage::register('subloginModel', $_sublogin);
            $this->loadLayout();
            if (($_sublogin->getData('id')) == 0) {
                $this->getLayout()->getBlock('head')->setTitle($this->__('Create new sublogin'));
            } else {
                $this->getLayout()->getBlock('head')->setTitle($this->__('Edit Sublogin'));
            }
            $this->renderLayout();
        } else {
            Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function deleteAction()
    {
        $_sublogin_id = $this->getRequest()->getParam('id');
        $_sublogin = Mage::getModel('sublogin/sublogin')->
            load($_sublogin_id);
        if (!$_sublogin->getId() || !$this->hasAccess($_sublogin)) {
            Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('Item does not exist'));
            $this->_redirect('*/*/');
        } else {
            $_sublogin->delete();
            Mage::getSingleton('core/session')->addSuccess(Mage::helper('sublogin')->__('Deleted sublogin %s',
                $_sublogin->getEmail()));
            $this->_redirect('sublogin/frontend/index');
        }
    }

    protected function hasAccess($_4e45fa4f3249d9c3cc1314ed89922593008cf117)
    {

        if (!Mage::getSingleton('customer/session')->isLoggedIn())
            return false;

        if (!Mage::helper('sublogin')->getCurrentSublogin())
            return true;
        if ($_4e45fa4f3249d9c3cc1314ed89922593008cf117->getCreateSublogins() || Mage::
            helper('sublogin')->getCurrentSublogin()->getCreateSublogins())
            return true;

        if ($_4e45fa4f3249d9c3cc1314ed89922593008cf117->getId() && $_4e45fa4f3249d9c3cc1314ed89922593008cf117->
            getEntityId() != Mage::getSingleton('customer/session')->getData('id'))
            return false;

        return true;
    }

    public function unFormatDate($_f0c57a04a51d1d900c8d28a111ab8f340a17465b)
    {
        if (!$_f0c57a04a51d1d900c8d28a111ab8f340a17465b || $_f0c57a04a51d1d900c8d28a111ab8f340a17465b ==
            1970 || $_f0c57a04a51d1d900c8d28a111ab8f340a17465b == 0) {
            return null;
        }
        $_1a191a32c0ac6906283b33c6b98088360bf0b9ea = Mage::app()->getLocale()->
            getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        return Mage::app()->getLocale()->date($_f0c57a04a51d1d900c8d28a111ab8f340a17465b,
            Varien_Date::DATE_INTERNAL_FORMAT)->toString($_1a191a32c0ac6906283b33c6b98088360bf0b9ea);
    }

    public function formatDate($_f0c57a04a51d1d900c8d28a111ab8f340a17465b)
    {
        if (empty($_f0c57a04a51d1d900c8d28a111ab8f340a17465b)) {
            return 0;
        }

        if (preg_match('/^[0-9]+$/', $_f0c57a04a51d1d900c8d28a111ab8f340a17465b)) {
            if ($_f0c57a04a51d1d900c8d28a111ab8f340a17465b <= 0)
                return 0;
            $_f0c57a04a51d1d900c8d28a111ab8f340a17465b = new Zend_Date((int)$_f0c57a04a51d1d900c8d28a111ab8f340a17465b);
        } else
            if (preg_match('#^\d{4}-\d{2}-\d{2}( \d{2}:\d{2}:\d{2})?$#', $_f0c57a04a51d1d900c8d28a111ab8f340a17465b)) {
                if ($_f0c57a04a51d1d900c8d28a111ab8f340a17465b <= 0)
                    return 0;
                $_bbb3c91a8584760004706197a44eb6790aba37a9 = new Zend_Date();
                $_f0c57a04a51d1d900c8d28a111ab8f340a17465b = $_bbb3c91a8584760004706197a44eb6790aba37a9->
                    setIso($_f0c57a04a51d1d900c8d28a111ab8f340a17465b);
            } else {
                $_f0c57a04a51d1d900c8d28a111ab8f340a17465b = Mage::app()->getLocale()->date($_f0c57a04a51d1d900c8d28a111ab8f340a17465b,
                    Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::
                    FORMAT_TYPE_SHORT), null, false);
            }

            return $_f0c57a04a51d1d900c8d28a111ab8f340a17465b->toString(Varien_Date::
                DATE_INTERNAL_FORMAT);
    }

    public function approveorderAction()
    {
        $_1fca9ed82493ef87f408fb2a0e244e2ffca359d2 = $this->getRequest()->getParam('order_id');
        if (!$_1fca9ed82493ef87f408fb2a0e244e2ffca359d2) {
            Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('OrderID not found.'));
            $this->_redirectReferer();
            return;
        }
        try {
            $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4 = Mage::getModel('sales/order')->
                load($_1fca9ed82493ef87f408fb2a0e244e2ffca359d2);
            $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4->setState('approved')->setStatus('approved')->
                save();
            $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4->sendNewOrderEmail();
        }
        catch (exception $_68dc3926fdadefc4ea3e5e09bf1dd6f6e1785aa5) {
            Mage::logException($_68dc3926fdadefc4ea3e5e09bf1dd6f6e1785aa5);
        }
        Mage::getSingleton('core/session')->addSuccess(Mage::helper('sublogin')->__('Order is approved.'));
        $this->_redirectReferer();
    }

    public function declineorderAction()
    {
        $_1fca9ed82493ef87f408fb2a0e244e2ffca359d2 = $this->getRequest()->getParam('order_id');
        if (!$_1fca9ed82493ef87f408fb2a0e244e2ffca359d2) {
            Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('OrderID not found.'));
            $this->_redirectReferer();
            return;
        }
        try {
            $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4 = Mage::getModel('sales/order')->
                load($_1fca9ed82493ef87f408fb2a0e244e2ffca359d2);
            $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4->setState('not_approved')->setStatus('not_approved')->
                save();
            $_86062e25478b46d5c43aa3c9fd776ed449827851 = $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4->
                getSubloginId();
            if ($_86062e25478b46d5c43aa3c9fd776ed449827851) {
                $_17fe0f58f521a1d8d613a93097a5a22f2074c04f = Mage::getModel('sublogin/sublogin')->
                    load($_86062e25478b46d5c43aa3c9fd776ed449827851);
                $_17fe0f58f521a1d8d613a93097a5a22f2074c04f->setData('order_id', $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4->
                    getId());
                $_17fe0f58f521a1d8d613a93097a5a22f2074c04f->setData('order_increment_id', $_bd0c62bfdc3abe2c29909f093a67c9c0d7f319a4->
                    getIncrementId());
                Mage::helper('sublogin')->sendOrderDeclinedEmailAlert($_17fe0f58f521a1d8d613a93097a5a22f2074c04f);
            }
        }
        catch (exception $_68dc3926fdadefc4ea3e5e09bf1dd6f6e1785aa5) {
            Mage::logException($_68dc3926fdadefc4ea3e5e09bf1dd6f6e1785aa5);
        }
        Mage::getSingleton('core/session')->addSuccess(Mage::helper('sublogin')->__('Order is declined.'));
        $this->_redirectReferer();
    }
}
