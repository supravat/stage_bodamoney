<?php

class MageB2B_Sublogin_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function customerHasSublogin(Mage_Customer_Model_Customer $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c)
    {
        $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d = $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c ?
            $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getId() : Mage::getSingleton('customer/session')->
            getCustomerId();
        if ($_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d) {
            static $_sublogin = array();
            if (!isset($_sublogin[$_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d])) {
                $_205d95c001c842f933e3b55fa4e902d5d2fdd0af = Mage::getModel('sublogin/sublogin')->
                    getCollection()->addFieldToFilter('entity_id', $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d);
                if ($_205d95c001c842f933e3b55fa4e902d5d2fdd0af->getSize() > 0) {
                    $_sublogin[$_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d] =
                        $_205d95c001c842f933e3b55fa4e902d5d2fdd0af;
                    return $_205d95c001c842f933e3b55fa4e902d5d2fdd0af;
                }
            } else {
                return $_sublogin[$_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d];
            }
        }
        return null;
    }

    public function getCurrentSublogin(Mage_Customer_Model_Customer $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c = null,
        $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607 = '')
    {
        $_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d = $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c ?
            $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getId() : Mage::getSingleton('customer/session')->
            getCustomerId();
        $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607 = $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607 ?
            $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607 : Mage::getSingleton('customer/session')->
            getSubloginEmail();
        if ($_ff0a3525eb1a63ce44e7951a0b522829a4d2f607) {
            static $_sublogin = array();
            if (!isset($_sublogin[$_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d .
                '_' . $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607])) {
                $_205d95c001c842f933e3b55fa4e902d5d2fdd0af = Mage::getModel('sublogin/sublogin')->
                    getCollection()->addFieldToFilter('email', $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607);
                foreach ($_205d95c001c842f933e3b55fa4e902d5d2fdd0af as $_7f11b1962e14e69c8844d0655d561b3e8cd30d5b) {
                    if ($_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d == $_7f11b1962e14e69c8844d0655d561b3e8cd30d5b->
                        getEntityId()) {
                        $_sublogin[$_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d .
                            '_' . $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607] = $_7f11b1962e14e69c8844d0655d561b3e8cd30d5b;
                        return $_7f11b1962e14e69c8844d0655d561b3e8cd30d5b;
                    }
                }
            } else {
                return $_sublogin[$_00bfc7a78eb28c3a15ee3f0a828d5a0f3f8d9b1d .
                    '_' . $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607];
            }
        }
        return null;
    }

    public function validateUniqueEmail($_ff0a3525eb1a63ce44e7951a0b522829a4d2f607,
        $_8c21f819efc249c0efe711aa784aa8a6a6d8a13f, $_e0e6e3a4eeae3cf9643bc7e9a30537fce9e9bb22 = null)
    {
        $_205d95c001c842f933e3b55fa4e902d5d2fdd0af = Mage::getModel('sublogin/sublogin')->
            getCollection()->addFieldToFilter('email', $_ff0a3525eb1a63ce44e7951a0b522829a4d2f607);
        foreach ($_205d95c001c842f933e3b55fa4e902d5d2fdd0af as $_sublogin) {
            $_f9712cf3eb2bf043e1ddede22c150b1170c69142 = Mage::getModel('customer/customer')->
                load($_sublogin->getEntityId());
            if ($_f9712cf3eb2bf043e1ddede22c150b1170c69142->getWebsiteId() == $_8c21f819efc249c0efe711aa784aa8a6a6d8a13f) {
                if ($_f9712cf3eb2bf043e1ddede22c150b1170c69142->getId() == $_e0e6e3a4eeae3cf9643bc7e9a30537fce9e9bb22)
                    continue;
                return false;
            }
        }


        $_aa9e6f37f9aafed0bccd8a8130762979ec1d7b7e = Mage::getSingleton('customer/session')->
            getSubloginEmail();
        $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c = Mage::getModel('customer/customer')->
            setWebsiteId($_8c21f819efc249c0efe711aa784aa8a6a6d8a13f)->loadByEmail($_ff0a3525eb1a63ce44e7951a0b522829a4d2f607,
            Mage::getModel('customer/customer'));
        Mage::getSingleton('customer/session')->setSubloginEmail($_aa9e6f37f9aafed0bccd8a8130762979ec1d7b7e);
        if ($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getId() && $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->
            getId() != $_e0e6e3a4eeae3cf9643bc7e9a30537fce9e9bb22)
            return false;
        return true;
    }

    public function setFrontendLoadAttributes($_sublogin,
        $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c)
    {
        if (!$_sublogin || !$_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c)
            return;
        $_a8c5747baec2d53dc80fd53f721245f3ab24680d = array(
            'prefix',
            'is_subscribed',
            'firstname',
            'lastname',
            'password',
            'email',
            'customer_id',
            'rp_token',
            'rp_token_created_at');
        if (Mage::getStoreConfig('sublogin/projects/project266_active')) {
            $_a8c5747baec2d53dc80fd53f721245f3ab24680d = array(
                'firstname',
                'lastname',
                'password',
                'email',
                'customer_id',
                'rp_token',
                'rp_token_created_at',
                'website',
                'company',
                'klt',
                'kcp',
                'is_subscribed');
        }

        foreach (array(
            'firstname',
            'lastname',
            'email') as $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07) {
            $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->setData('orig_' . $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07,
                $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getData($_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07));
        }
        foreach ($_a8c5747baec2d53dc80fd53f721245f3ab24680d as $_cf80d58e34c98ca4787395f86a0a654da3781807) {
            $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07 = $_cf80d58e34c98ca4787395f86a0a654da3781807;
            if ($_cf80d58e34c98ca4787395f86a0a654da3781807 == 'password') {
                $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07 = 'password_hash';
            }
            $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->setData($_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07,
                $_sublogin->getData($_cf80d58e34c98ca4787395f86a0a654da3781807));
        }
    }

    public function sendAccountExpiredMail($_sublogin)
    {
        if (!$_sublogin->getData('rp_token')) {
            $_sublogin->setData('rp_token', mt_rand(0,
                PHP_INT_MAX));
        }
        $_sublogin->save();
        $_6b0361ca199f403b6f7eb781b83d507641544828 = 'sublogin/email/expire_refresh';
        $this->_sendNewEmail($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin);
    }

    public function sendNewSubloginEmail($_sublogin)
    {
        $_6b0361ca199f403b6f7eb781b83d507641544828 = 'sublogin/email/new';
        $this->_sendNewEmail($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin);
    }

    public function sendNewPasswordEmail($_sublogin)
    {
        $_6b0361ca199f403b6f7eb781b83d507641544828 = 'sublogin/email/reset_password';
        $this->_sendNewEmail($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin);
    }

    public function sendMainLoginOrderAlert($_sublogin)
    {
        $_6b0361ca199f403b6f7eb781b83d507641544828 =
            'sublogin/email/mainlogin_orderalert';
        $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c = $_sublogin->
            getCustomer();
        if ($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c) {

            $_sublogin->setAlterEmail($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->
                getOrigEmail());
            $_sublogin->setCustomerName($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->
                getOrigFirstname() . ' ' . $_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->
                getOrigLastname());
        }
        $this->_sendNewEmail($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin);
    }

    public function sendOrderDeclinedEmailAlert($_sublogin)
    {
        $_6b0361ca199f403b6f7eb781b83d507641544828 = 'sublogin/email/order_declined';
        $this->_sendNewEmail($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin);
    }

    protected function _sendNewEmail($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin)
    {
        $_44f8b790a939f0b9202f65d2e050f16b3cd0fbd6 = Mage::getSingleton('core/translate');
        $_44f8b790a939f0b9202f65d2e050f16b3cd0fbd6->setTranslateInline(false);
        $email_template = Mage::getModel('core/email_template');
        $email_template->setDesignConfig(array('area' =>
                'frontend', 'store' => $_sublogin->getStoreId()));
        $email_template->addBcc($this->_prepareBcc($_sublogin->
            getStoreId()));
        $_80e9f53c89ea8843430714bfcb1a99292d02cef9 = ($_sublogin->getAlterEmail()) ? $_sublogin->getAlterEmail() :$_sublogin->getEmail();
        $email =$_80e9f53c89ea8843430714bfcb1a99292d02cef9;
        if (strpos($email,'bodamoney.com') === false) { 
            $email_template->sendTransactional(Mage::
                getStoreConfig($_6b0361ca199f403b6f7eb781b83d507641544828, $_sublogin->
                getStoreId()), array('name' => Mage::getStoreConfig('sublogin/email/send_from_name',$_sublogin->getStoreId()), 'email' => Mage::
                    getStoreConfig('sublogin/email/send_from_email', $_sublogin->
                    getStoreId())), $email, null, array('sublogin' =>
                    $_sublogin), $_sublogin->getStoreId());
            if (!$email_template->getSentSuccess()) {
                if ($_sublogin->getAlterEmail()) {
                    Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('Problem with sending email to customer %s',
                        $_sublogin->getAlterEmail()));
                } else {
                    Mage::getSingleton('core/session')->addError(Mage::helper('sublogin')->__('Problem with sending sublogin email to %s',
                        $_sublogin->getEmail()));
                }
            }
        }else{
            
            $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
            $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
            $phone=str_replace("@bodamoney.com","",$email);
            $phone=str_replace("+","",$phone);
            $target=$phone;
            $mpas=Mage::getModel('core/session')->getData('mpassword');
            $message = "UserName:$phone,password:".$mpas;
            Mage::getModel('core/session')->setData('mpassword','');
            ///die($message);
            shell_exec ("php sms/index.php $username  $password  $target  $message");
        }
    }

    protected function _prepareBcc($_e79f42cd6fb01fb1483aa880b524a8d65c7b6081)
    {
        $_11ba77016ab1985b3211d707f2f2c144269d7091 = trim(Mage::getStoreConfig('sublogin/email/send_bcc',
            $_e79f42cd6fb01fb1483aa880b524a8d65c7b6081));
        $_bc509a4207fe4d326675bc926dc569be241779c2 = explode(";", $_11ba77016ab1985b3211d707f2f2c144269d7091);
        $_85b73c34e466d0c344b7dbb8bdcebf348b76724b = array();
        if (!empty($_bc509a4207fe4d326675bc926dc569be241779c2)) {

            foreach ($_bc509a4207fe4d326675bc926dc569be241779c2 as $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07 =>
                $_101966cc6117772941ef274d5dfb132cb8452d0a) {
                if (trim($_101966cc6117772941ef274d5dfb132cb8452d0a)) {
                    $_85b73c34e466d0c344b7dbb8bdcebf348b76724b[$_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07] =
                        trim($_101966cc6117772941ef274d5dfb132cb8452d0a);
                }
            }
            return $_85b73c34e466d0c344b7dbb8bdcebf348b76724b;
        }
        return null;
    }

    public function getGridFields($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c, $_168f685b62f9bec4ef848eff88e14ca431705084 =
        '')
    {
        $_c9cb8b3327e7c441d3547b479c17979eb046dc77 = Mage::getStoreConfig('customer_id/customer_id/auto_increment') ? true : false;
        $_91a2a43f77d1dc504cd0664864c1dfd4834a8f53 = Mage::getModel('core/website')->
            load($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getWebsiteId());
        $_6ab22cf5b83adf50f6320e3b010f11ee77b3b669 = array();
        foreach ($_91a2a43f77d1dc504cd0664864c1dfd4834a8f53->getStoreCollection() as $_7ff01efcb6976550e400d525bb2f8f2090febc06) {
            $_6ab22cf5b83adf50f6320e3b010f11ee77b3b669[$_7ff01efcb6976550e400d525bb2f8f2090febc06->
                getId()] = $_7ff01efcb6976550e400d525bb2f8f2090febc06->getCode();
        }
        $_81e77768f3068185edd8fda5ab1f65a052f81082 = array();
        foreach ($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getAddresses() as $_17d2af4b1493648e1cc1cb8dffce9a7658374484) {
            $_e654d1a18146651e3bfe6fd079c03211dddd7e9f = '';
            $_1b144addba58891e49d3fed7e7095afd8ce1c0e5 = $_17d2af4b1493648e1cc1cb8dffce9a7658374484->
                getStreet();
            foreach ($_1b144addba58891e49d3fed7e7095afd8ce1c0e5 as $_317d4a7f0913fca2b26a7e355e8f6ae219b839ca) {
                $_e654d1a18146651e3bfe6fd079c03211dddd7e9f .= isset($_317d4a7f0913fca2b26a7e355e8f6ae219b839ca) ?
                    $_317d4a7f0913fca2b26a7e355e8f6ae219b839ca : '';
                $_e654d1a18146651e3bfe6fd079c03211dddd7e9f .= ' ';
            }
            $_e60edee9bbcec9af2084c09a7e3e3bf5a0643e9f = $_17d2af4b1493648e1cc1cb8dffce9a7658374484->
                getCompany() . ' ' . $_e654d1a18146651e3bfe6fd079c03211dddd7e9f . ' ' . $_17d2af4b1493648e1cc1cb8dffce9a7658374484->
                getPostcode() . ' ' . $_17d2af4b1493648e1cc1cb8dffce9a7658374484->getCity();
            $_81e77768f3068185edd8fda5ab1f65a052f81082[$_17d2af4b1493648e1cc1cb8dffce9a7658374484->
                getId()] = $_e60edee9bbcec9af2084c09a7e3e3bf5a0643e9f;
        }
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9 = array();
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'store_id',
            'label' => Mage::helper('sublogin')->__('Store'),
            'required' => false,
            'type' => 'select',
            'style' => 'width:100px',
            'cssclass' => '',
            'options' => $_6ab22cf5b83adf50f6320e3b010f11ee77b3b669,
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'address_ids',
            'label' => Mage::helper('sublogin')->__('Addresses'),
            'required' => false,
            'type' => 'multiselect',
            'style' => 'width:350px',
            'cssclass' => '',
            'options' => $_81e77768f3068185edd8fda5ab1f65a052f81082,
            );
        if ($_b803c43bc8b3ef64b7d19b78f670cdbd11dacf8c->getCustomerId()) {
            $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
                'name' => 'customer_id',
                'label' => Mage::helper('sublogin')->__('Customer Id'),
                'required' => false,
                'type' => 'text',
                'style' => 'width:100px',
                'cssclass' => '',
                'readonly' => $_c9cb8b3327e7c441d3547b479c17979eb046dc77,
                );
        }
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'email',
            'label' => Mage::helper('sublogin')->__('Email'),
            'required' => true,
            'type' => 'text',
            'style' => 'width:150px',
            'cssclass' => 'validate-email',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'send_backendmails',
            'label' => Mage::helper('sublogin')->__('Send Mail'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'create_sublogins',
            'label' => Mage::helper('sublogin')->__('Can create sublogins'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );

        if (Mage::helper('customer')->getNamePrefixOptions()) {
            $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
                'name' => 'prefix',
                'label' => Mage::helper('sublogin')->__('Prefix'),
                'required' => false,
                'type' => 'select',
                'style' => 'width:50px',
                'cssclass' => '',
                'options' => Mage::helper('customer')->getNamePrefixOptions(),
                );
        } else {
            $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
                'name' => 'prefix',
                'label' => Mage::helper('sublogin')->__('Prefix'),
                'required' => false,
                'type' => 'text',
                'style' => 'width:50px',
                'cssclass' => '',
                );
        }
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'firstname',
            'label' => Mage::helper('sublogin')->__('Firstname'),
            'required' => true,
            'type' => 'text',
            'style' => 'width:100px',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'lastname',
            'label' => Mage::helper('sublogin')->__('Lastname'),
            'required' => true,
            'type' => 'text',
            'style' => 'width:100px',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'password',
            'label' => Mage::helper('sublogin')->__('Password'),
            'required' => true,
            'type' => 'text',
            'style' => 'width:80px',
            'cssclass' => 'validate-password',
            'onlyNewRequired' => true,
            'onlyNewValue' => true,
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'days_to_expire',
            'label' => Mage::helper('sublogin')->__('Days to Expire'),
            'required' => false,
            'type' => 'text',
            'style' => '',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'expire_date',
            'label' => Mage::helper('sublogin')->__('Date to Expire'),
            'type' => 'html',
            'html' => $_168f685b62f9bec4ef848eff88e14ca431705084,
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'acl',
            'label' => Mage::helper('sublogin')->__('ACL'),
            'required' => false,
            'type' => 'multiselect',
            'style' => 'width:250px',
            'cssclass' => '',
            'options' => Mage::getModel('sublogin/acl')->getCollection()->keyValuePair(),
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'is_subscribed',
            'label' => Mage::helper('sublogin')->__('Is subscribed'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'order_needs_approval',
            'label' => Mage::helper('sublogin')->__('Order needs approval'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'active',
            'label' => Mage::helper('sublogin')->__('Active'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'credit_value',
            'label' => Mage::helper('sublogin')->__('Boda Money'),
            'required' => true,
            'type' => 'text',
            'style' => 'width:100px',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'transfer_limit',
            'label' => Mage::helper('sublogin')->__('Transfer limit'),
            'required' => true,
            'type' => 'text',
            'style' => 'width:100px',
            'cssclass' => '',
            );    
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'is_affiliate',
            'label' => Mage::helper('sublogin')->__('Affiline'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );
        $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'is_marketing',
            'label' => Mage::helper('sublogin')->__('Marketing'),
            'required' => false,
            'type' => 'checkbox',
            'style' => '',
            'cssclass' => '',
            );
         $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'id_number',
            'label' => Mage::helper('sublogin')->__('Identity card Number'),
            'required' => false,
            'type' => 'text',
            'style' => 'width:100px',
            'cssclass' => '',
            );    
          $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'driving_license_number',
            'label' => Mage::helper('sublogin')->__('Driving License Number'),
            'required' => false,
            'type' => 'multiselect',
            'style' => 'width:250px',
            ); 
            //////////////////
             $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'carregisternumber',
            'label' => Mage::helper('sublogin')->__('Car Register Number'),
            'required' => false,
            'type' => 'text',
            'style' => 'width:100px',
            'cssclass' => '',
            );
             $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'motocycleregisternumber',
            'label' => Mage::helper('sublogin')->__('Motocycle registration numbers '),
            'required' => false,
            'type' => 'multiselect',
            'style' => 'width:250px',
            );        
          $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'kamome_customer',
            'label' => Mage::helper('sublogin')->__('Kamome Customer'),
            'required' => false,
             'type' => 'text',
            'style' => 'width:100px',
            );  
             $_d2389373c4d226ef389a44a6ea431427c26dd7a9[] = array(
            'name' => 'nf_card',
            'label' => Mage::helper('sublogin')->__('NFC card UID'),
            'required' => false,
             'type' => 'text',
            'style' => 'width:100px',
            );            
            ////////////////////
            
                     
        foreach ($_d2389373c4d226ef389a44a6ea431427c26dd7a9 as $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07 =>
            $_54323be853e934686a9fca46e2f91c8ead26624e) {
            if (!isset($_54323be853e934686a9fca46e2f91c8ead26624e['onlyNewRequired']))
                $_54323be853e934686a9fca46e2f91c8ead26624e['onlyNewRequired'] = false;
            if (!isset($_54323be853e934686a9fca46e2f91c8ead26624e['onlyNewValue']))
                $_54323be853e934686a9fca46e2f91c8ead26624e['onlyNewValue'] = false;
            if (!isset($_54323be853e934686a9fca46e2f91c8ead26624e['readonly']))
                $_54323be853e934686a9fca46e2f91c8ead26624e['readonly'] = false;
            $_d2389373c4d226ef389a44a6ea431427c26dd7a9[$_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07] =
                $_54323be853e934686a9fca46e2f91c8ead26624e;
        }
        return $_d2389373c4d226ef389a44a6ea431427c26dd7a9;
    }

    public function json_encode($_f9fbb812b678a976ed736c49d6880ed425aed2f7)
    {
        switch (gettype($_f9fbb812b678a976ed736c49d6880ed425aed2f7)) {
            case 'object':
                $_f9fbb812b678a976ed736c49d6880ed425aed2f7 = get_object_vars($_f9fbb812b678a976ed736c49d6880ed425aed2f7);
                return json_encode($_f9fbb812b678a976ed736c49d6880ed425aed2f7);
                break;
            case 'array':
                $_a46bb2d71d162bd66a7176d0a9c0f944d40d9737 = false;
                $_174bd2bdf2bc930d6a4fc5d75f2c76d8da76a997 = count($_f9fbb812b678a976ed736c49d6880ed425aed2f7);
                for ($_e5063eb1568e4b8089f8c18b623e0363b782b74c = 0; $_e5063eb1568e4b8089f8c18b623e0363b782b74c <
                    $_174bd2bdf2bc930d6a4fc5d75f2c76d8da76a997; $_e5063eb1568e4b8089f8c18b623e0363b782b74c++)
                    if (!array_key_exists($_e5063eb1568e4b8089f8c18b623e0363b782b74c, $_f9fbb812b678a976ed736c49d6880ed425aed2f7)) {
                        $_a46bb2d71d162bd66a7176d0a9c0f944d40d9737 = true;
                        break;
                    }
                if ($_a46bb2d71d162bd66a7176d0a9c0f944d40d9737) {
                    $_d7073612911bf0c7dc87033618838552aa2497df = array();
                    foreach ($_f9fbb812b678a976ed736c49d6880ed425aed2f7 as $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07 =>
                        $_080c67f9b4e0fa2795ac6258b05f7280be9b7d96)
                        $_d7073612911bf0c7dc87033618838552aa2497df[] = '"' . $_3f8de01169eabbb582c7c5ffb762d5f7d1f1bf07 .
                            '":' . $this->json_encode($_080c67f9b4e0fa2795ac6258b05f7280be9b7d96);
                    return '{' . implode(',', $_d7073612911bf0c7dc87033618838552aa2497df) . '}';
                } else {
                    $_d7073612911bf0c7dc87033618838552aa2497df = array();
                    for ($_86ff0a1d51cba890d386dd0a7964e7d3158966de = 0; $_86ff0a1d51cba890d386dd0a7964e7d3158966de <
                        $_174bd2bdf2bc930d6a4fc5d75f2c76d8da76a997; $_86ff0a1d51cba890d386dd0a7964e7d3158966de++)
                        $_d7073612911bf0c7dc87033618838552aa2497df[] = $this->json_encode($_f9fbb812b678a976ed736c49d6880ed425aed2f7[$_86ff0a1d51cba890d386dd0a7964e7d3158966de]);
                    return '[' . implode(',', $_d7073612911bf0c7dc87033618838552aa2497df) . ']';
                }
                break;
            case 'NULL':
                return 'null';
                break;
            case 'boolean':
                return ($_f9fbb812b678a976ed736c49d6880ed425aed2f7) ? 'true' : 'false';
                break;
            case 'integer':
            case 'double':
                return $_f9fbb812b678a976ed736c49d6880ed425aed2f7;
                break;
            case 'string':
            default:
                $_f9fbb812b678a976ed736c49d6880ed425aed2f7 = str_replace(array(
                    '\\',
                    '/',
                    '"',
                    "\n",
                    "\r"), array(
                    '\\\\',
                    '\/',
                    '\"',
                    "\\\n",
                    ""), $_f9fbb812b678a976ed736c49d6880ed425aed2f7);
                return '"' . $_f9fbb812b678a976ed736c49d6880ed425aed2f7 . '"';
                break;
                return true;
        }
        return false;
    }
}
