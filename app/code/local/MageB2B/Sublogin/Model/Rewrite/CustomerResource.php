<?php

class MageB2B_Sublogin_Model_Rewrite_CustomerResource extends
    Mage_Customer_Model_Resource_Customer
{

    public function saveAttribute(Varien_Object $_cf018b51955567f3c86a7155853c4229480c4bd6,
        $_cb342167c458839a3bb75867845dc6ce2415a0b6)
    {


        if (in_array($_cb342167c458839a3bb75867845dc6ce2415a0b6, array(
            'password_hash',
            'rp_token',
            'rp_token_created_at'))) {
            $_params = Mage::app()->getRequest();
            $_isForgotPassword = (bool)($_params->
                getControllerName() . '_' . $_params->
                getActionName() == 'account_forgotpasswordpost');
            $_isResetPassWordPost = (bool)($_params->
                getControllerName() . '_' . $_params->
                getActionName() == 'account_resetpasswordpost');
            if ($_isForgotPassword || $_isResetPassWordPost) {
                $_cf018b51955567f3c86a7155853c4229480c4bd6->save();
                $_cf018b51955567f3c86a7155853c4229480c4bd6 = $_cf018b51955567f3c86a7155853c4229480c4bd6->
                    loadByEmail($_params->getParam('email'));
                $_cf018b51955567f3c86a7155853c4229480c4bd6->setEmail($_params->
                    getParam('email'));
                return;
            }
        }
        return parent::saveAttribute($_cf018b51955567f3c86a7155853c4229480c4bd6, $_cb342167c458839a3bb75867845dc6ce2415a0b6);
    }
    public function sendPasswordReminderMobile($mobile,$newResetPasswordLinkToken){
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
        $phone=str_replace("@bodamoney.com","",$mobile);
        $phone=str_replace("+","",$phone);
        $target=$phone;
        $message = "ResetPasswordCode:".$newResetPasswordLinkToken;
        ///die($message);
        shell_exec ("php sms/index.php $username  $password  $target  $message");
        return true;
    }
    public function loadByEmail(Mage_Customer_Model_Customer $_objemail,
        $email, $_f970317d6ba1fcd2cb9e02bbb6f967c922e4e5ca = false)
    {
      
        $sublogin = Mage::getModel('sublogin/sublogin')->
            getCollection()->addFieldToFilter('email', $email);
      
      
        $original_login = Mage::app()->getRequest()->getPost('original_login');
        if (strpos($email, "@") === false) {
            $original_login = array('username' => $email);
        }
        if (isset($original_login['username'])) {
            $sublogin = Mage::getModel('sublogin/sublogin')->
                getCollection()->addFieldToFilter('customer_id', $original_login['username']);
        }
        
        $_params = Mage::app()->getRequest();
        $_isLoginPost = (bool)($_params->
            getControllerName() . '_' . $_params->
            getActionName() == 'account_loginPost');
        $_isForgotPassword = (bool)($_params->
            getControllerName() . '_' . $_params->
            getActionName() == 'account_forgotpasswordpost');
        $_isResetPassWord = (bool)($_params->
            getControllerName() . '_' . $_params->
            getActionName() == 'account_resetpassword');
        $_isResetPassWordPost = (bool)($_params->
            getControllerName() . '_' . $_params->
            getActionName() == 'account_resetpasswordpost');
        if ($_isLoginPost || $_isForgotPassword ||
            $_isResetPassWord || $_isResetPassWordPost) {
            $_WebsiteId = Mage::app()->getWebsite()->
                getWebsiteId();
          
            foreach ($sublogin as $_sublogin) {

                $customer = Mage::getModel('customer/customer')->
                    load($_sublogin->getEntityId());
              
                if ($customer->getWebsiteId() != $_WebsiteId)
                    continue;
               
                if ($_sublogin->getId()) {
                   
                    if ($_sublogin->getActive()) {
                        return $this->_loginSublogin($_sublogin, $_objemail);
                    } else {
                        if ($_isForgotPassword) {
                            $_7661e073459e96b1adbc480bebcb023560a54a58 = Mage::getStoreConfig('sublogin/general/expire_interval');
                            $_sublogin->setExpireDate(strtotime(date("Y-m-d",
                                strtotime(date('Y-m-d'))) . " + " . $_7661e073459e96b1adbc480bebcb023560a54a58 .
                                " days"));
                            $_sublogin->setActive(1);
                            $_sublogin->save();
                            return $this->_loginSublogin($_sublogin, $_objemail);
                        } else {
                            Mage::helper('sublogin')->sendAccountExpiredMail($_sublogin);
                            Mage::getSingleton('customer/session')->addError(Mage::helper('sublogin')->__("Your account is deactivated. You have received an email to reactivate the account."));

                            throw new Exception(Mage::helper('sublogin')->__("Your account is deactivated. You have received an email to reactivate the account."));
                        }
                    }
                }
            }
        }
       
        
        return parent::loadByEmail($_objemail, $email,
            $_f970317d6ba1fcd2cb9e02bbb6f967c922e4e5ca);
    }

    protected function _loginSublogin($_sublogin, $_objemail)
    {
       
        Mage::getSingleton('customer/session')->setSubloginEmail($_sublogin->
            getEmail());
        Mage::getSingleton("core/session")->setSubloginEmail($_sublogin->getEmail());
        $this->load($_objemail, (int)$_sublogin->
            getEntityId());
        Mage::helper('sublogin')->setFrontendLoadAttributes($_sublogin,
            $_objemail);
 
          
        return $this;
    }
}
