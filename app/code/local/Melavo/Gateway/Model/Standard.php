<?php
class Melavo_Gateway_Model_Standard extends Mage_Payment_Model_Method_Abstract {
	protected $_code = 'bodamoney';
	
	protected $_isGateway              = true;
    protected $_canAuthorize           = true;
    protected $_canCapture             = true;
    protected $_canCapturePartial      = false;
    protected $_canRefund              = false;
    protected $_canVoid                = false;
    protected $_canUseInternal         = false;
    protected $_canUseCheckout         = true;
    protected $_canUseForMultishipping = false;
    protected $_order; 
	
	public function getOrderPlaceRedirectUrl() {
		return Mage::getUrl('gateway/payment/redirect', array('_secure' => true));
	}
}
?>