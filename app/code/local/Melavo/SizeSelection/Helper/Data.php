<?php
class Melavo_SizeSelection_Helper_Data extends Mage_Core_Helper_Abstract{

    protected function _getSimpleProduct($configurableProduct, $attributeValue, $attributeCode='size'){
        if($configurableProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE){
            $associatedProducts = $configurableProduct->getTypeInstance()->getUsedProducts();
            
            foreach($associatedProducts as $associatedProduct)
                if($associatedProduct->getIsInStock() && $associatedProduct->getAttributeText($attributeCode)==$attributeValue)
                    return Mage::getModel('catalog/product')->load($associatedProduct->getId());
        }
            
        return false;
    }
    
    protected function _getCartItemByParentId($parentItemId){
        $cart = Mage::getModel('checkout/cart')->getQuote();
        foreach ($cart->getAllItems() as $item) {
            if($item->getItemId() == $parentItemId){
                return $item;
            }
        }
        return false;
    }
    
    protected function _getCurrentCartItemQty($associatedProduct){
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $currentCartQty = 0;
        foreach ($cart->getAllItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);  
            
            //if($product->getId() == $associatedProduct->getId() && $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE){
            if($product->getId() == $associatedProduct->getId() && $item->getParentItemId()){
                $currentItem = $this->_getCartItemByParentId($item->getParentItemId());
                if($currentItem){
                    $currentCartQty = (int)$currentItem->getQty(); 
                }
            }
        }
        return $currentCartQty;
    }

    
    public function getSimpleInventoryStockQty($associatedProduct){
        $product = Mage::getModel('catalog/product')->load($associatedProduct->getId());
        
        if($product->getId()){
            return (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
        }
            
        return 0;
    }
    
    
    public function getCurrentCartItemQty($associatedProduct){
        return $this->_getCurrentCartItemQty($associatedProduct);
    }
    
    
    public function getSimpleProductByAttribute($configurableProduct, $attributeValue, $attributeCode='size'){
        return $this->_getSimpleProduct($configurableProduct, $attributeValue, $attributeCode);
    }
    
    
    public function isCartItemValidQty($associatedProduct, $productQty){
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $isValid = true;
        foreach ($cart->getAllItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            
            //if($product->getId() == $associatedProduct->getId() && $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE){
            if($product->getId() == $associatedProduct->getId() && $item->getParentItemId()){
                $cartItemQty = $this->_getCurrentCartItemQty($associatedProduct);
                $stockItemQty = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
                $currentCartQty = (int)$productQty + $cartItemQty;
                if($currentCartQty > $stockItemQty){
                    $isValid = false;
                }
            }
        }
        return $isValid;
    }
     public function isCartItemValidQtySingleProduct($associatedProduct, $productQty){
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $isValid = true;
        foreach ($cart->getAllItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            
            //if($product->getId() == $associatedProduct->getId() && $product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE){
            if($product->getId() == $associatedProduct->getId() && $item->getParentItemId()){
                $cartItemQty = $this->_getCurrentCartItemQty($associatedProduct);
                $stockItemQty = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
                $currentCartQty = (int)$productQty + $cartItemQty;
                if($currentCartQty > $stockItemQty){
                    $isValid = false;
                }
            }
        }
        return $isValid;
    }  

    public function destroyAllCartItems(){
        $cart = Mage::getSingleton('checkout/cart');
        
        foreach( Mage::getSingleton('checkout/session')->getQuote()->getItemsCollection() as $item ){
            $cart->removeItem($item->getId());
        }
        
        $cart->save();
    }
}