<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

# Controllers are not autoloaded so we will have to do it manually: 
require_once 'Mage/Checkout/controllers/CartController.php';

/**
 * Shopping cart controller
 */
class Melavo_SizeSelection_CartController extends Mage_Checkout_CartController{
    /**
     * Test cart action
     */
    public function testAction(){
        echo 'I successfully Override Cart Controller'; 
    }

    /**
     * Add product to shopping cart action
     */
    public function bulkcartAction(){ 
        
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');
            $_group_name=Mage::helper('customer')->getCustomerGroupName();
            
            /**
             * Check product availability
             */
            if (!$product) {
                $this->_goBack();
                return;
            }
            
            #-------------------------------------------------------------------
            #-------------------------------------------------------------------
            # Melavo_SizeSelection
            $sizeAttribute = $product->getResource()->getAttribute("size");
            $sizeAttributeId = (int)$sizeAttribute->getId();
            
            if($product->getTypeId()!="giftvoucher"){
                    if(isset($params['qty']) && !empty($params['qty'])){
                    $qtyParams = $params['qty'];
                    foreach ($qtyParams as $optionId => $qty){
                        if((int)$qty > 0){
                            $newParams = $params;
                            
                            unset($newParams['uenc']); 
                            unset($newParams['related_product']);
                            
                            $newParams['qty'] = (int)$qty;
                            $newParams['super_attribute'] = array($sizeAttributeId => (int)$optionId);
    
                            $product = Mage::getModel('catalog/product')->load((int)$params['product']);                        
                            
                            if($product->getId() && $product->getIsInStock()){
                                $sizeAttributeValue = $sizeAttribute->getSource()->getOptionText($optionId);
                                $associatedProduct = Mage::helper('sizeselection')->getSimpleProductByAttribute($product, $sizeAttributeValue);
                        
                                $simpleProductQty = Mage::helper('sizeselection')->getSimpleInventoryStockQty($associatedProduct);
    
                                $currentCartItemQty = Mage::helper('sizeselection')->getCurrentCartItemQty($associatedProduct);
                                $isValidProductQty = Mage::helper('sizeselection')->isCartItemValidQty($associatedProduct, $qty);                            
                                if(! $isValidProductQty && $associatedProduct){
                                    // Set new quantity
                                    $updateItemQty = $simpleProductQty - $currentCartItemQty;
                                    $newParams['qty'] = $updateItemQty;
                                            
                                    // Output notice from a controller
                                    if($associatedProduct != false && $updateItemQty != 0){
                                        Mage::getSingleton('core/session')->addNotice("The requested quantity for '{$associatedProduct->getName()}' is not available.");
                                    }
                                }
                                $cart->addProduct($product, $newParams); 
                            }
                        }
                    }
                }
            }
            else{
                        
                        $params['qty']=1;
                        $newParams=$params;
                        unset($newParams['uenc']); 
                        unset($newParams['related_product']);
                        $newParams['qty'] = 1;
                      
                        $product = Mage::getModel('catalog/product')->load((int)$params['product']);                             
                        $filter = new Zend_Filter_LocalizedToNormalized(
                             array('locale' => Mage::app()->getLocale()->getLocaleCode())
                         );
                         $newParams['qty'] = $filter->filter($newParams['qty']);                       
                        if($product->getId() && $product->getIsInStock()){
                            
                            $simpleProductQty = Mage::helper('sizeselection')->getSimpleInventoryStockQty($product);                          
                            $currentCartItemQty = Mage::helper('sizeselection')->getCurrentCartItemQty($product);
                            ///die("TEst");
                            
                            $isValidProductQty = Mage::helper('sizeselection')->isCartItemValidQtySingleProduct($product,$qty);                
                          
                            if(!$isValidProductQty ){
                                    // Set new quantity
                                    $updateItemQty = $simpleProductQty - $currentCartItemQty;
                                    $newParams['qty'] = $updateItemQty;
                                            
                                    // Output notice from a controller
                                    if($associatedProduct != false && $updateItemQty != 0){
                                        Mage::getSingleton('core/session')->addNotice("The requested quantity for '{$product->getName()}' is not available.");
                                    }
                            }
                            
                             $cart->addProduct($product, $newParams); 
                            
                        }                                 
            }
            $cart->save();   
            //die;
            # Melavo_SizeSelection
            #-------------------------------------------------------------------
            #-------------------------------------------------------------------
            
            
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }
            $cart->save();
            
            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                   // $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                   // $this->_getSession()->addSuccess($message);
                }
                if (!preg_match('/wholesale/',$_group_name))
                {
                    //die("test");
                    //$this->_goBack();
                    header("location : http://{$_SERVER['SERVER_NAME']}/checkout/cart/?___SID=U ");
                    //echo "<script type='text/javascript'>window.location.href='http://{$_SERVER['SERVER_NAME']}/checkout/cart/?___SID=U';</script>";
                    
                }
                else{
                
                    $ids = $product->getCategoryIds();
                    $category= Mage::getModel('catalog/category');
                    $category->load(intval($ids[0]));
                    $url=$category->getUrl();  
                    $url=$url."?___SID=U";
                    //die($url);      
                     header("location : $url");
                               
                    //$this->_redirectUrl($url);
                }
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    } 
}
?>
