<?php 
class Melavo_Ecofit_Model_Observer {

    public function unsetCouponCode(Varien_Event_Observer $observer) {
        $quote = $observer->getQuoteItem()->getQuote();
    
        if (!$quote->hasItems()) {
            Mage::getSingleton('core/session')->setCouponCode('');
            $quote->getShippingAddress()->setCollectShippingRates(true);
            $quote->setCouponCode('')->collectTotals()->save();
        }
    }
}
?>