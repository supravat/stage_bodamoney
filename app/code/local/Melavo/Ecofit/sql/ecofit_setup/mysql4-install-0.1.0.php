<?php
$installer = $this;
 
$installer->startSetup();
$installer->run("
 
-- DROP TABLE IF EXISTS {$this->getTable('melavo_ecofit')};
CREATE TABLE {$this->getTable('melavo_ecofit')} (
  `id` int(11) UNSIGNED NOT NULL auto_increment,
  `email` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
	");
$installer->endSetup();