<?php

class Melavo_Ecofit_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
     */
     public function indexAction() {
         if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $from   =$this->getRequest()->getParam('from');
        $to     =$this->getRequest()->getParam('to');
        $account     =$this->getRequest()->getParam('account');
        $account_id=$this->getRequest()->getParam('account_id');
        $is_new     =$this->getRequest()->getParam('is_new');
        $type     =$this->getRequest()->getParam('type');
        /////
       if($account=="main"){
                $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
                $customer    = Mage::getSingleton('customer/session')->getCustomer();
        }else{
                $parent_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
                $customer_id=$account_id;
                $customer =Mage::getModel('sublogin/sublogin')->load($customer_id);
        }
        if($type==0){          
            $query ="select month(transaction_time) as month,sum(case when amount_credit<0 then -amount_credit else 0 end) as spend,";
            $query.= "sum(case when amount_credit>0 then amount_credit else 0 end ) as receive ";
            $query.=" from credit_transaction ";
            $query.="  where type_transaction_id<>".Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE;
            $query.="  and  customer_id=$customer_id ";
             if($account=="main"){
                 $query.=" and  parent_id=0";     
                  if($from!=""&&$to!="")
                  $query.=" and transaction_time >'$from' and <='$to'";     
                  else
                  $query.=" and transaction_time >'$from' ";         
             }
             else{
                 $query.=" and  parent_id<>0";
                 if($from!=""&&$to!="")
                  $query.=" and transaction_time >'$from' and <='$to'";     
                  else
                  $query.=" and transaction_time >'$from' ";
             }
            $query.=" group by (Month(transaction_time))";
            $query.= "order by transaction_time asc";
            $resource = Mage::getSingleton('core/resource');  
            $readConnection = $resource->getConnection('core_read');  
            $transactions = $readConnection->fetchAll($query);                         
          
            //////////
            Mage::register('transactions',$transactions); 
            Mage::register('customer',$customer);  
            Mage::register('customer_id',$customer_id); 
            Mage::register('parent_id',$parent_id);         
            $this->loadLayout();
    	    $this->getLayout()->getBlock('root')->setTemplate('melavo/ecofit.phtml');
    	    $this->_initLayoutMessages('core/session');
    	    $this->renderLayout();
        ////
        }elseif($type==2)
        {
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=Transaction_statement_sys.'.date("dmY").'.csv');
                if($account=="main"){
                if($from!=""&&$to!="")
                    $collection =  Mage::getModel('customercredit/transaction')
                                ->getCollection()
                                ->addFieldToFilter('customer_id',$customer_id)
                                ->addFieldToFilter('parent_id',0)
                                ->addFieldToFilter('type_transaction_id',
                                    array(
                                    array('neq' => Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE)))
                               ->addFieldToFilter('transaction_time', array(
                                'from'     => $from,
                                'to'       => $to,
                                'datetime' => true
                                ));
                else
                    $collection =  Mage::getModel('customercredit/transaction')->getCollection()   
                              ->addFieldToFilter('customer_id',$customer_id)
                              ->addFieldToFilter('parent_id',0)
                               ->addFieldToFilter('type_transaction_id',
                                array(
                                 array('neq' => Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE)))
                              ->addFieldToFilter('transaction_time', array(
                                'from'     => $from,
                                'datetime' => true
                                 ));
                ///////////////////
             }else
             {
                
               
                if($from!=""&&$to!="")
                $collection =  Mage::getModel('customercredit/transaction')->getCollection()
                                  ->addFieldToFilter('customer_id',$customer_id)
                                  ->addFieldToFilter('parent_id',
                                                    array(
                                                     array('neq' => 0)))
                                  ->addFieldToFilter('transaction_time', array(
                                                    'from'     => $from,
                                                    'to'       => $to,
                                                    'datetime' => true
                                                     ));
                else
                $collection =  Mage::getModel('customercredit/transaction')->getCollection()                       
                                    ->addFieldToFilter('customer_id',$customer_id)
                                    ->addFieldToFilter('parent_id',
                                                    array(
                                                     array('neq' => 0)))
                                     ->addFieldToFilter('type_transaction_id',
                                                array(
                                            array('neq' => Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE)))
                                     ->addFieldToFilter('transaction_time', array(
                                'from'     => $from,
                                //'to'       => $to,
                                'datetime' => true
                                 ));
             } 
                // create a file pointer connected to the output stream
                $output = fopen('php://output', 'w');
                
                // output the column headings
                fputcsv($output, array('Account number', 'Date', 'Name of payer or payee','transaction type','message','Transaction number','Reference','Amount'));
                
                // fetch the data
               $i=1; 
               $total =0;
               foreach($transactions as $transaction)
               {
                  $account="";
                  if($parent_id==0){
                     $customer=Mage::getModel("customer/customer")->load($customer_id);
                     $customer_email = $customer->getEmail();
                    if (strpos($customer_email,'@bodamoney.com') !== false) {
                        $part  =explode("@",$customer_email);
                        $account= @$part[0];
                      }else{
                        $account =$customer_email;
                      }
                  }else{
                    
                  }
                    $currentTimestamp=$transaction->getTransactionTime();
                    $currentTimestamp=Mage::getModel('core/date')->timestamp($currentTimestamp);
                    $time=   date('d.m.Y', $currentTimestamp); 
                    $type=$transaction->getTypeTransactionId();    
                    $credit = Mage::getModel('customercredit/customercredit')->getConvertedFromBaseCustomerCredit($transaction->getAmountCredit());
                    //$credit =Mage::getModel('customercredit/customercredit')->getLabel($credit);
                    $row=array();
                    $row=array($account,$time,$transaction->getCustomerName(),$this->__(Mage::getModel('customercredit/typetransaction')->load($type)->getTransactionName()),$transaction->getMessage(),$i,$transaction->getReference(),$credit);              
                    fputcsv($output, $row);
                    //$total+=abs($transaction->getAmountCredit());
                    $i++;              
               }
                // $total = Mage::getModel('customercredit/customercredit')->getConvertedFromBaseCustomerCredit($total);
                 //$total =Mage::getModel('customercredit/customercredit')->getLabel($total); 
                // fputcsv($output, array('Total', '', '','',$total));
                
           
                // loop over the rows, outputting them
              
        }
    }	
}