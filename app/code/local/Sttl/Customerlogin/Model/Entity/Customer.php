<?php
/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Customerlogin
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
class Sttl_Customerlogin_Model_Entity_Customer extends Mage_Customer_Model_Entity_Customer
{
    public function loadByMobilenumber(Mage_Customer_Model_Customer $customer, $username, $testOnly = false)
    {
        $select = $this->_getReadAdapter()->select()
            ->from($this->getEntityTable(), array($this->getEntityIdField()))
            //->where('email=?', $email);
			->joinNatural('customer_entity_varchar')
			->joinNatural('eav_attribute')
			->where('eav_attribute.attribute_code=\'mobilenumber\' AND customer_entity_varchar.value=?',$username);
            //->where('email=:customer_email');
		
        if ($customer->getSharingConfig()->isWebsiteScope()) {
            if (!$customer->hasData('website_id')) {
                Mage::throwException(Mage::helper('customer')->__('Customer website ID must be specified when using the website scope.'));
            }
            $select->where('website_id=?', (int)$customer->getWebsiteId());
        }
		if ($id = $this->_getReadAdapter()->fetchOne($select, 'entity_id'))
		{
			$this->load($customer, $id);
		}		
		
        else
		{
            $customer->setData(array());
        }
		
		
        return $this;
    }
	protected function _beforeSave(Varien_Object $customer)
    {
        Mage_Eav_Model_Entity_Abstract::_beforeSave($customer);


        $select = $this->_getWriteAdapter()->select()
            ->from($this->getEntityTable(), array($this->getEntityIdField()))
            ->where('email=?', $customer->getEmail());
        if ($customer->getSharingConfig()->isWebsiteScope()) {
            $select->where('website_id=?', (int) $customer->getWebsiteId());
        }
        if ($customer->getId()) {
            $select->where('entity_id !=?', $customer->getId());
        }

        
	}
	
}

