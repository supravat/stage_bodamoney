<?php
/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Customerlogin
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
      class Sttl_Customerlogin_Model_Mysql4_Customerlogin_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
      {
          public function _construct()
          {
              //parent::__construct();
              $this->_init('customerlogin/customer');
          }
		  public function getDetails() {
 $read = $this->_getReadAdapter();
		 $select = $read->select()
				  ->from($this->_caroptionsdetailTable)
				  ->where("sku  = ?", $sku)
				  ->where("product_id  = ?", $product_id);
	
		$result = $read->fetchAll($select);

		if(is_array($result) && count($result) > 0 ){
				return 1;
		}else{
				return 0;
		}		  }
      }