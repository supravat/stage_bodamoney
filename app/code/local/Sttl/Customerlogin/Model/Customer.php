<?php
/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Customerlogin
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
class Sttl_Customerlogin_Model_Customer extends Mage_Customer_Model_Customer
{
	CONST EXCEPTION_EMAIL_EXISTS = 0;
    
    /*public function authenticate($login, $password)
    {
        
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');		
		$select = $connection->select()->from('customer_entity', array('email'))->where('email=?',$login);		
		$rowsArray = $connection->fetchAll($select); 		
		$count	=	 count($rowsArray);	
		
		if ($count == 1)
		{
			//echo "Email Id Exists In DB";
			$this->loadByEmail($login);
		}
		else
		{
			//echo "Email Id Not Exists In DB Or Try To Login By Mobile";
			$this->loadByMobilenumber($login);
		}
        if ($this->getConfirmation() && $this->isConfirmationRequired()) 
		{
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('This account is not confirmed.'),
                self::EXCEPTION_EMAIL_NOT_CONFIRMED
            );
        }
        if (!$this->validatePassword($password)) 
		{
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid login or password.'),
                self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
            );
        }
        Mage::dispatchEvent('customer_customer_authenticated', array(
           'model'    => $this,
           'password' => $password,
        ));
        return true;
    }*/
	
    // Load customer by username
  
    public function loadByMobilenumber($customerUsername)
    {
	//echo get_class($this);exit;
		Mage::log("Username1:".$customerUsername);
		//$obj = new Sttl_Customerlogin_Model_Entity_Customer();
		Mage::getModel('customerlogin/entity_customer')->loadByMobilenumber($this, $customerUsername);
        //$this->_getResource()->loadByMobilenumber($this, $customerUsername);
        return $this;
		
    }
	public function validate()
    {
       
        $errors = array();
        $customerHelper = Mage::helper('customer');
        if (!Zend_Validate::is( trim($this->getFirstname()) , 'NotEmpty')) {
            $errors[] = $customerHelper->__('The first name cannot be empty.');
        }

        if (!Zend_Validate::is( trim($this->getLastname()) , 'NotEmpty')) {
            $errors[] = $customerHelper->__('The last name cannot be empty.');
        }
		$entityType = Mage::getSingleton('eav/config')->getEntityType('customer');
        //$attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'mobilenumber');
		if($this->getMobilenumber()) {
			$min = Mage::getStoreConfig('sms_section/sms_group/min');
			$max = Mage::getStoreConfig('sms_section/sms_group/max');
			$mode = Mage::getStoreConfig('sms_section/sms_group/mode');
			if($mode == 'numeric') {
				if (!Zend_Validate::is(trim($this->getMobilenumber()) , 'Digits')) {
					$errors[] = $customerHelper->__('Please enter Digits only.');
				}
			}
			if($mode == 'alphanumeric') {
				if (!ctype_alnum($this->getMobilenumber())) {
					$errors[] = $customerHelper->__('Please enter Alphanumeric value only.');
				}
			}
			
			if($min) {
				if(strlen(trim($this->getMobilenumber())) < $min) {
					$errors[] = $customerHelper->__('Please enter morethan %s character.',$min);
				}
			}
			if($max) {
				if(strlen(trim($this->getMobilenumber())) > $max) {
					$errors[] = $customerHelper->__('Please enter lessthan %s character.',$max);
				}
			}
		}
        if (!Zend_Validate::is($this->getEmail(), 'EmailAddress')) {
            $errors[] = $customerHelper->__('Invalid email address "%s".', $this->getEmail());
        }

        $password = $this->getPassword();
        if (!$this->getId() && !Zend_Validate::is($password , 'NotEmpty')) {
            $errors[] = $customerHelper->__('The password cannot be empty.');
        }
        if (strlen($password) && !Zend_Validate::is($password, 'StringLength', array(6))) {
            $errors[] = $customerHelper->__('The minimum password length is %s', 6);
        }
        $confirmation = $this->getConfirmation();
        if ($password != $confirmation) {
            $errors[] = $customerHelper->__('Please make sure your passwords match.');
        }

        $entityType = Mage::getSingleton('eav/config')->getEntityType('customer');
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'dob');
        if ($attribute->getIsRequired() && '' == trim($this->getDob())) {
            $errors[] = $customerHelper->__('The Date of Birth is required.');
        }
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'taxvat');
        if ($attribute->getIsRequired() && '' == trim($this->getTaxvat())) {
            $errors[] = $customerHelper->__('The TAX/VAT number is required.');
        }
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'gender');
        if ($attribute->getIsRequired() && '' == trim($this->getGender())) {
            $errors[] = $customerHelper->__('Gender is required.');
        }

        if (empty($errors)) {
            return true;
        }
		//echo "<pre>";
		//print_r($errors);
        return $errors;
    }

}
