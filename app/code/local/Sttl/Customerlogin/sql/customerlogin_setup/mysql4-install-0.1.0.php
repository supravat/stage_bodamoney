<?php
/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Customerlogin
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
 
     $installer = $this;
     $installer->startSetup();
     $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
	 $setup->addAttribute('customer', 'mobilenumber', array(
		'label'		=> 'Mobile number',
		'type'		=> 'varchar',
		'input'		=> 'text',		
		'visible'	=> true,
		'required'	=> false,
		'position'	=> 1,
	 ));
	  
	 



	 $eavConfig = Mage::getSingleton('eav/config');
	 $attribute = $eavConfig->getAttribute('customer', 'mobilenumber');
	/* $attribute->setData('used_in_forms',   array('customer_account_edit',
                                             'customer_account_create',
                                             'adminhtml_customer'));*/
	 $attribute->save();	
	 
	 
	 	 //find out the attribute_id of the newly created attribute
		$sql = "SELECT attribute_id FROM ".$this->getTable('eav_attribute')." WHERE attribute_code='mobilenumber'";
		$row = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchRow($sql);
		
		//insert the supporting values into customer_form_attribute
		$installer->run("
		INSERT INTO customer_form_attribute (form_code, attribute_id)
		VALUES (
		'adminhtml_customer',".$row['attribute_id']."
		);
		");
		
		$installer->run("
		INSERT INTO customer_form_attribute (form_code, attribute_id)
		VALUES (
		'checkout_register', ".$row['attribute_id']."
		);
		");
		
		$installer->run("
		INSERT INTO customer_form_attribute (form_code, attribute_id)
		VALUES (
		'customer_account_create', ".$row['attribute_id']."
		);
		");
		
		$installer->run("
		INSERT INTO customer_form_attribute (form_code, attribute_id)
		VALUES (
		'customer_account_edit', ".$row['attribute_id']."
		);
		");
	 
	 $setup = new Mage_Sales_Model_Mysql4_Setup('sales_setup');
	 $setup->getConnection()->addColumn(
     $setup->getTable('sales_flat_quote'),
        "customer_mobilenumber",
        "text NULL DEFAULT NULL"
    );
	$setup->addAttribute('quote', "customer_mobilenumber", array('type' => 'static', 'visible' => false));
	 
   $installer->endSetup();
	 
