<?php
require_once 'Mage/Customer/controllers/AccountController.php';
class Sttl_BodaCustomer_AccountController extends Mage_Customer_AccountController {

    public function createPostAction() {
        $session = $this->_getSession();
        if ($session->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session->setEscapeMessages(true); // prevent XSS injection in user input
        if (!$this->getRequest()->isPost()) {
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return;
        }
        $p = $this->getRequest()->getPost();
        $p['emails'] = array_filter($p['emails'], 'strlen');
        $p['mobiles'] = array_filter($p['mobiles'], 'strlen');
        $this->getRequest()->setPost('emails', $p['emails']);
        $this->getRequest()->setPost('mobiles', $p['mobiles']);
        $isExists = $this->isMobileEmailExists();
        if ($isExists === 'REQUIRED') {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($this->__('Unable to create account. Atleast an email address or a mobile number is required.'));
        } else if ($isExists) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($this->__('Unable to create account. One or more email address or mobile number is already registered with us.'));
        }
        if ($isExists) {
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return true;
        }
        $customer = $this->_getCustomer();
        $post = $this->getRequest()->getPost();
        $email = false;
        $isEmail = false;
        $isMobile = false;
        print_r($post);
        die;
        if (isset($post['emails']) && count($post['emails']) > 0) {
            $emails = array_map('trim', $post['emails']);
            $emails = array_unique($emails);
            $emails = implode(',', $emails);
            $helper = Mage::helper('bodacustomer');
            $isEmail = $helper->checkExistFinalEmailMobileSaving($emails, '', 'customer_emails');            
            $customer->setData('customer_emails', $emails);
            $email = reset($post['emails']);
        }
        if (isset($post['mobiles']) && count($post['mobiles']) > 0) {
            $mobiles = array_map('trim', $post['mobiles']);
            $mobiles = array_unique($mobiles);
            $mobiles = implode(',', $mobiles);
            $helper = Mage::helper('bodacustomer');
            $isMobile = $helper->checkExistFinalEmailMobileSaving($mobiles, '', 'customer_mobiles');                        
            $customer->setData('customer_mobiles', $mobiles);
            if (!$email) {
                $email = reset($post['mobiles']) . '@bodamoney.com';
            }
        }
        if($isEmail || $isMobile){
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Unable to create account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
            return;
        }
        if ($email) {
            $this->getRequest()->setPost('email', $email);
            $customer->setEmail($email);
        } else {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Unable to create account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
            $this->_redirectError($errUrl);
        }
        try {
            $errors = $this->_getCustomerErrors($customer);

            if (empty($errors)) {
                $customer->save();
                $this->_dispatchRegisterSuccess($customer);
                $this->_successProcessRegistration($customer);
                return;
            } else {
                $this->_addSessionError($errors);
            }
        } catch (Mage_Core_Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost());
            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
                $url = $this->_getUrl('customer/account/forgotpassword');
                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
                $session->setEscapeMessages(false);
            } else {
                $message = $e->getMessage();
            }
            $session->addError($message);
        } catch (Exception $e) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
        }
        $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
        $this->_redirectError($errUrl);
    }

    protected function isMobileEmailExists($postAction = 'createPost') {
        $post = $this->getRequest()->getPost();
        $required = true;
        if (isset($post['emails']) && count($post['emails']) > 0 ||
                isset($post['mobiles']) && count($post['mobiles']) > 0
        ) {
            $required = false;
        }
        if ($required) {
            return 'REQUIRED';
        }
        $collection = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect(array('customer_emails', 'customer_mobiles'), true);
        $emails = array();
        $mobiles = array();
        foreach ($collection as $c) {
            $e = $c->getData('customer_emails');
            if ($e) {
                $emails = array_merge($emails, explode(',', $e));
            }
            $m = $c->getData('customer_mobiles');
            if ($m) {
                $mobiles = array_merge($mobiles, explode(',', $m));
            }
        }
        $mobiles = array_unique($mobiles);
        $emails = array_unique($emails);
        if ($postAction === 'editPost') {
            $bodacustomer_saved_emails = array();
            if (isset($post['bodacustomer_saved_emails'])) {
                $bodacustomer_saved_emails = explode(',', $post['bodacustomer_saved_emails']);
                $emails = array_diff($emails, $bodacustomer_saved_emails);
            }
            $bodacustomer_saved_mobiles = array();
            if (isset($post['bodacustomer_saved_mobiles'])) {
                $bodacustomer_saved_mobiles = explode(',', $post['bodacustomer_saved_mobiles']);
                $mobiles = array_diff($mobiles, $bodacustomer_saved_mobiles);
            }
        }
        $exists = false;
        if (isset($post['emails']) && count($post['emails']) > 0) {
            $commonemail = array_intersect($post['emails'], $emails);
            if (count($commonemail) > 0) {
                $exists = true;
            }
        }
        if (!$exists && isset($post['mobiles']) && count($post['mobiles']) > 0) {
            $commonmobile = array_intersect($post['mobiles'], $mobiles);
            if (count($commonmobile) > 0) {
                $exists = true;
            }
        }
        return $exists;
    }

    public function editAction() {
        if ($this->_getSession()->isLoggedIn()) {
            $bhelper = Mage::helper('bodacustomer');
            $emails = $bhelper->getCustomerEmails($this->_getSession()->getId());
            $mobiles = $bhelper->getCustomerMobiles($this->_getSession()->getId());
            Mage::register('EM_session_formdata', array(
                'emails' => $emails,
                'mobiles' => $mobiles
            ));
        }
        parent::editAction();
    }

    public function editPostAction() {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/edit');
        }
        $session = $this->_getSession();
        if (!$session->isLoggedIn()) {
            return $this->_redirect('*/*/login');
        }
        $p = $this->getRequest()->getPost();
        $p['emails'] = array_filter($p['emails'], 'strlen');
        $p['mobiles'] = array_filter($p['mobiles'], 'strlen');
        $this->getRequest()->setPost('emails', $p['emails']);
        $this->getRequest()->setPost('mobiles', $p['mobiles']);
        $isExists = $this->isMobileEmailExists('editPost');
        if ($isExists === 'REQUIRED') {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($this->__('Unable to update account. Atleast an email address or a mobile number is required.'));
        } else if ($isExists) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($this->__('Unable to update account. One or more email address or mobile number is already registered with us.'));
        }
        if ($isExists) {
            $this->_redirect('*/*/edit');
            return $this;
        }
        $customer = $session->getCustomer();
        $post = $this->getRequest()->getPost();
        $email = false;
        $emails = array();
        $mobiles = array();
        if (isset($post['emails']) && count($post['emails']) > 0) {
            $emails = array_map('trim', $post['emails']);
            $email = reset($post['emails']);
        }
        if (isset($post['mobiles']) && count($post['mobiles']) > 0) {
            if (!$email) {
                $email = reset($post['mobiles']) . '@bodamoney.com';
                $emails[] = $email;
            }
            $mobiles = array_map('trim', $post['mobiles']);
        }

        if (count($emails) > 0) {
            $emails = array_unique($emails);
            $emails = implode(',', $emails);
            $helper = Mage::helper('bodacustomer');
            $isEmail = $helper->checkExistFinalEmailMobileSaving($emails, $post['bodacustomer_saved_emails'], 'customer_emails');
            $customer->setData('customer_emails', $emails);
        } else {
            $customer->setData('customer_emails', '');
        }
        if (count($mobiles) > 0) {
            $mobiles = array_unique($mobiles);
            $mobiles = implode(',', $mobiles);
            $helper = Mage::helper('bodacustomer');
            $isMobile = $helper->checkExistFinalEmailMobileSaving($mobiles, $post['bodacustomer_saved_mobiles'], 'customer_mobiles');
            $customer->setData('customer_mobiles', $mobiles);
        } else {
            $customer->setData('customer_mobiles', '');
        }
        if ($isMobile || $isEmail) {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Unable to save account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/edit', array('_secure' => true));
            $this->_redirectError($errUrl);
        }
        if ($email) {
            $customer->setData('email', $email);
            $this->getRequest()->setPost('email', $email);
        } else {
            $session->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Unable to create account. Atleast an email or a mobile number is required.'));
            $errUrl = $this->_getUrl('*/*/edit', array('_secure' => true));
            $this->_redirectError($errUrl);
        }
        parent::editPostAction();
    }

    public function forgotPasswordPostAction() {
       
        $reset_pin1 = (string) $this->getRequest()->getPost('reset_pin1');
        //die($reset_pin1);
        if ($reset_pin1) {
            $helper = Mage::helper('bodacustomer');
            $by = ($reset_pin1 == 'mobile') ? 'Mobile Number' : 'Email Address';
            $reset_pin1_by = (string) $this->getRequest()->getPost("reset_pin1_$reset_pin1");
            if (!$reset_pin1_by) {
                $this->_getSession()->addError($this->__("Please enter your $by."));
                $this->_redirect('*/*/forgotpassword');
                return;
            }
            if ($reset_pin1 == 'email') {
                if (!Zend_Validate::is($reset_pin1_by, 'EmailAddress')) {
//                    $this->_getSession()->setForgottenEmail($reset_pin1_by);
                    $this->_getSession()->addError($this->__('Invalid email address.'));
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            if ($reset_pin1 == 'email') {
                /** @var $customer Mage_Customer_Model_Customer */
                $customer = $this->_getModel('customer/customer')
                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                        ->loadByEmail($reset_pin1_by);

                if ($customer->getId()) {
                    try {
                        $customer->setEmail($reset_pin1_by);
                        $newResetPasswordLinkToken = $this->_getHelper('customer')->generateResetPasswordLinkToken();
                        $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                        $customer->sendPasswordResetConfirmationEmail();
                    } catch (Exception $exception) {
                        $this->_getSession()->addError($exception->getMessage());
                        $this->_redirect('*/*/forgotpassword');
                        return;
                    }
                }
                $this->_getSession()
                        ->addSuccess($this->_getHelper('customer')
                                ->__('If there is an account associated with %s you will receive an email with a link to reset your password.', $this->_getHelper('customer')->escapeHtml($reset_pin1_by)));
                $this->_redirect('*/*/');
                return;
            } else {
                $customer = Mage::getModel('customer/customer');
                $isSend = $customer->sendPIN1ChangeRequestEmail($reset_pin1_by);
                if ($isSend) {
                    $this->_getSession()
                            ->addSuccess($this->_getHelper('bodacustomer')
                                    ->__('Your Pin1 change request is received successfuly. New Pin1 for mobile %s will be sent via SMS if an account is associated with this mobile number.', $this->_getHelper('bodacustomer')->escapeHtml($reset_pin1_by)));
                    $this->_redirect('*/*/');
                    return;
                } else {
                    $this->_getSession()->addError($this->__('Unable to send Pin1 change request.Please try again later.'));
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
        } else {
            $this->_getSession()->addError($this->__('PIN1 reset method not specified.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }

}
