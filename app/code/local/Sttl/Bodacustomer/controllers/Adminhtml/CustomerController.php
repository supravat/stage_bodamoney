<?php

include_once("Mage/Adminhtml/controllers/CustomerController.php");

class Sttl_Bodacustomer_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController {

    protected function isMobileEmailExists() {
        $post = $this->getRequest()->getPost();
        $accountData = $post['account'];
        $required = true;
        if (isset($post['emails']) && count($post['emails']) > 0 ||
                isset($post['mobiles']) && count($post['mobiles']) > 0
        ) {
            $required = false;
        }
        if ($required) {
            return 'REQUIRED';
        }
        if (count($post['emails']) > count(array_unique($post['emails'])) ||
                count($post['mobiles']) > count(array_unique($post['mobiles'])
                )) {
            return 'DUPLICATE';
        }
        $collection = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect(array('customer_emails', 'customer_mobiles'), true);
        $emails = array();
        $mobiles = array();
        foreach ($collection as $c) {
            $e = $c->getData('customer_emails');
            if ($e) {
                $emails = array_merge($emails, explode(',', $e));
            }
            $m = $c->getData('customer_mobiles');
            if ($m) {
                $mobiles = array_merge($mobiles, explode(',', $m));
            }
        }
        $mobiles = array_unique($mobiles);
        $emails = array_unique($emails);
        if (isset($accountData['emails_hidden'])) {
            $bodacustomer_saved_emails = explode(',', $accountData['emails_hidden']);
            $emails = array_diff($emails, $bodacustomer_saved_emails);
        }
        if (isset($accountData['mobiles_hidden'])) {
            $bodacustomer_saved_mobiles = explode(',', $accountData['mobiles_hidden']);
            $mobiles = array_diff($mobiles, $bodacustomer_saved_mobiles);
        }
        $exists = false;
        if (isset($post['emails'])) {
            $post['emails'] = array_map('trim', $post['emails']);
            $commonemail = array_intersect($post['emails'], $emails);
            if (count($commonemail) > 0) {
                $exists = true;
            }
        }

        if (isset($post['mobiles'])) {
            $post['mobiles'] = array_map('trim', $post['mobiles']);
            if (!$exists && isset($post['mobiles'])) {
                $commonmobile = array_intersect($post['mobiles'], $mobiles);
                if (count($commonmobile) > 0) {
                    $exists = true;
                }
            }
        }
        return $exists;
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();        
        if ($data) {
            $redirectBack = $this->getRequest()->getParam('back', false);
            $this->_initCustomer('customer_id');
            $customer = Mage::registry('current_customer');
            $isExists = $this->isMobileEmailExists();
            if ($isExists === 'REQUIRED') {
                $this->_getSession()->addError('Unable to save account. Atleast an email address or a mobile number is required.');
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            } else if ($isExists) {
                $this->_getSession()->addError('Unable to save account. One or more email address or mobile number is already registered with us.');
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            }
            /** @var $customer Mage_Customer_Model_Customer */
            $post = $this->getRequest()->getPost();
            $email = false;
            $emails = array();
            $mobiles = array();
            if (isset($post['emails']) && count($post['emails']) > 0) {
                $emails = array_map('trim', $post['emails']);
                $email = reset($post['emails']);
            }
            if (isset($post['mobiles']) && count($post['mobiles']) > 0) {
                $mobiles = array_map('trim', $post['mobiles']);
                if (!$email) {
                    $email = reset($post['mobiles']) . '@bodamoney.com';
                    $emails[] = $email;
                }
            }
            if (count($emails) > 0) {
                $emails = array_unique($emails);
                $emails = implode(',', $emails);
            } else {
                $emails = '';
            }
            if (count($mobiles) > 0) {
                $mobiles = array_unique($mobiles);
                $mobiles = implode(',', $mobiles);
            } else {
                $mobiles = '';
            }
            if ($email) {
                $helper = Mage::helper('bodacustomer');
                $isEmail = $helper->checkExistFinalEmailMobileSaving($emails, $data['account']['emails_hidden'], 'customer_emails');
                $isMobile = $helper->checkExistFinalEmailMobileSaving($mobiles, $data['account']['mobiles_hidden'], 'customer_mobiles');
                if ($isEmail || $isMobile) {
                    $this->_getSession()->addError('Unable to save account. One or more email address or mobile number is already registered with us.');
                    $this->_getSession()->setCustomerData($data);
                    $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                    return;
                }
                $customer->setData('customer_emails', $emails);
                $customer->setData('customer_mobiles', $mobiles);
                $customer->setData('email', $email);
                
                $account = $this->getRequest()->getPost('account');
                $account['email'] = $email;
                $account['customer_mobiles'] = $mobiles;
                $account['customer_emails'] = $emails;
                $this->getRequest()->setPost('account', $account);
            } else {
                $this->_getSession()->addError('Unable to save account. Atleast an email or a mobile number is required.');
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
            }
            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = Mage::getModel('customer/form');
            $customerForm->setEntity($customer)
                    ->setFormCode('adminhtml_customer')
                    ->ignoreInvisible(false)
            ;

            $formData = $customerForm->extractData($this->getRequest(), 'account');

            // Handle 'disable auto_group_change' attribute
            if (isset($formData['disable_auto_group_change'])) {
                $formData['disable_auto_group_change'] = empty($formData['disable_auto_group_change']) ? '0' : '1';
            }

            $errors = $customerForm->validateData($formData);
            if ($errors !== true) {
                foreach ($errors as $error) {
                    $this->_getSession()->addError($error);
                }
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            }

            $customerForm->compactData($formData);

            // Unset template data
            if (isset($data['address']['_template_'])) {
                unset($data['address']['_template_']);
            }

            $modifiedAddresses = array();
            if (!empty($data['address'])) {
                /** @var $addressForm Mage_Customer_Model_Form */
                $addressForm = Mage::getModel('customer/form');
                $addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);

                foreach (array_keys($data['address']) as $index) {
                    $address = $customer->getAddressItemById($index);
                    if (!$address) {
                        $address = Mage::getModel('customer/address');
                    }

                    $requestScope = sprintf('address/%s', $index);
                    $formData = $addressForm->setEntity($address)
                            ->extractData($this->getRequest(), $requestScope);

                    // Set default billing and shipping flags to address
                    $isDefaultBilling = isset($data['account']['default_billing']) && $data['account']['default_billing'] == $index;
                    $address->setIsDefaultBilling($isDefaultBilling);
                    $isDefaultShipping = isset($data['account']['default_shipping']) && $data['account']['default_shipping'] == $index;
                    $address->setIsDefaultShipping($isDefaultShipping);

                    $errors = $addressForm->validateData($formData);
                    if ($errors !== true) {
                        foreach ($errors as $error) {
                            $this->_getSession()->addError($error);
                        }
                        $this->_getSession()->setCustomerData($data);
                        $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array(
                                    'id' => $customer->getId())
                        ));
                        return;
                    }

                    $addressForm->compactData($formData);

                    // Set post_index for detect default billing and shipping addresses
                    $address->setPostIndex($index);

                    if ($address->getId()) {
                        $modifiedAddresses[] = $address->getId();
                    } else {
                        $customer->addAddress($address);
                    }
                }
            }

            // Default billing and shipping
            if (isset($data['account']['default_billing'])) {
                $customer->setData('default_billing', $data['account']['default_billing']);
            }
            if (isset($data['account']['default_shipping'])) {
                $customer->setData('default_shipping', $data['account']['default_shipping']);
            }
            if (isset($data['account']['confirmation'])) {
                $customer->setData('confirmation', $data['account']['confirmation']);
            }
            if(isset($data['sublogin']['can_create_sublogins'])){
                $customer->setData('can_create_sublogins',$data['sublogin']['can_create_sublogins']);
            }              
             if(isset($data['sublogin']['max_number_sublogins'])){
                 $customer->setData('can_create_sublogins',$data['sublogin']['max_number_sublogins']);
             }    
            // Mark not modified customer addresses for delete
            foreach ($customer->getAddressesCollection() as $customerAddress) {
                if ($customerAddress->getId() && !in_array($customerAddress->getId(), $modifiedAddresses)) {
                    $customerAddress->setData('_deleted', true);
                }
            }

            if (Mage::getSingleton('admin/session')->isAllowed('customer/newsletter') && !$customer->getConfirmation()
            ) {
                $customer->setIsSubscribed(isset($data['subscription']));
            }

            if (isset($data['account']['sendemail_store_id'])) {
                $customer->setSendemailStoreId($data['account']['sendemail_store_id']);
            }

            $isNewCustomer = $customer->isObjectNew();
            try {
                $sendPassToEmail = false;
                // Force new customer confirmation
                if ($isNewCustomer) {
                    $customer->setPassword($data['account']['password']);
                    $customer->setForceConfirmed(true);
                    if ($customer->getPassword() == 'auto') {
                        $sendPassToEmail = true;
                        $customer->setPassword($customer->generatePassword());
                    }
                }

                Mage::dispatchEvent('adminhtml_customer_prepare_save', array(
                    'customer' => $customer,
                    'request' => $this->getRequest()
                ));

                $customer->save();

                // Send welcome email
                if ($customer->getWebsiteId() && (isset($data['account']['sendemail']) || $sendPassToEmail)) {
                    $storeId = $customer->getSendemailStoreId();
                    if ($isNewCustomer) {
                        $customer->sendNewAccountEmail('registered', '', $storeId);
                    } elseif ((!$customer->getConfirmation())) {
                        // Confirm not confirmed customer
                        $customer->sendNewAccountEmail('confirmed', '', $storeId);
                    }
                }

                if (!empty($data['account']['new_password'])) {
                    $newPassword = $data['account']['new_password'];
                    if ($newPassword == 'auto') {
                        $newPassword = $customer->generatePassword();
                    }
                    $customer->changePassword($newPassword);
                    $customer->sendPasswordReminderEmail();
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__('The customer has been saved.')
                );
                Mage::dispatchEvent('adminhtml_customer_save_after', array(
                    'customer' => $customer,
                    'request' => $this->getRequest()
                ));

                if ($redirectBack) {
                    $this->_redirect('*/*/edit', array(
                        'id' => $customer->getId(),
                        '_current' => true
                    ));
                    return;
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
            } catch (Exception $e) {
                $this->_getSession()->addException($e, Mage::helper('adminhtml')->__('An error occurred while saving the customer.'));
                $this->_getSession()->setCustomerData($data);
                $this->getResponse()->setRedirect($this->getUrl('*/customer/edit', array('id' => $customer->getId())));
                return;
            }
        }
        $this->getResponse()->setRedirect($this->getUrl('*/customer'));
    }

    public function validateAction() {
        $response = new Varien_Object();
        $response->setError(0);
        $p = $this->getRequest()->getPost();
        $websiteId = Mage::app()->getStore()->getWebsiteId();
        $accountData = $this->getRequest()->getPost('account');
        $customer = Mage::getModel('customer/customer');
        $customerId = $this->getRequest()->getParam('id');
        if ($customerId) {
            $customer->load($customerId);
            $websiteId = $customer->getWebsiteId();
        } else if (isset($accountData['website_id'])) {
            $websiteId = $accountData['website_id'];
        }
        $isExists = $this->isMobileEmailExists();
        if ($isExists === 'REQUIRED') {
            $this->_getSession()->addError(
                    Mage::helper('adminhtml')->__('Unable to save account. Atleast an email address or a mobile number is required.'));
            $this->getResponse()->setBody($response->toJson());
        } else if ($isExists == 'DUPLICATE') {
            $this->_getSession()->addError(
                    Mage::helper('adminhtml')->__('Unable to save account. You may have entered already used email id / mobile. '));
        } else if ($isExists) {
            $this->_getSession()->addError(
                    Mage::helper('adminhtml')->__('Unable to save account. One or more email address or mobile number is already registered with us.'));
        }
        if ($isExists) {
            $response->setError(1);
//            $this->_initLayoutMessages('adminhtml/session');
//            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
//            $this->getResponse()->setBody($response->toJson());
//            return;
        }
        /* @var $customerForm Mage_Customer_Model_Form */

        $customerForm = Mage::getModel('customer/form');
        $customerForm->setEntity($customer)
                ->setFormCode('adminhtml_customer')
                ->setIsAjaxRequest(true)
                ->ignoreInvisible(false)
        ;

        $data = $customerForm->extractData($this->getRequest(), 'account');
        $errors = $customerForm->validateData($data);
        if ($errors !== true) {
            foreach ($errors as $error) {
                $this->_getSession()->addError($error);
            }
            $response->setError(1);
        }

        # additional validate email
//        if (!$response->getError()) {
//            # Trying to load customer with the same email and return error message
//            # if customer with the same email address exisits
//            $checkCustomer = Mage::getModel('customer/customer')
//                    ->setWebsiteId($websiteId);
//            $checkCustomer->loadByEmail($accountData['email']);
//            if ($checkCustomer->getId() && ($checkCustomer->getId() != $customer->getId())) {
//                $response->setError(1);
//                $this->_getSession()->addError(
//                        Mage::helper('adminhtml')->__('Customer with the same email already exists.')
//                );
//            }
//        }
        $addressesData = $this->getRequest()->getParam('address');
        if (is_array($addressesData)) {
            /* @var $addressForm Mage_Customer_Model_Form */
            $addressForm = Mage::getModel('customer/form');
            $addressForm->setFormCode('adminhtml_customer_address')->ignoreInvisible(false);
            foreach (array_keys($addressesData) as $index) {
                if ($index == '_template_') {
                    continue;
                }
                $address = $customer->getAddressItemById($index);
                if (!$address) {
                    $address = Mage::getModel('customer/address');
                }

                $requestScope = sprintf('address/%s', $index);
                $formData = $addressForm->setEntity($address)
                        ->extractData($this->getRequest(), $requestScope);

                $errors = $addressForm->validateData($formData);
                if ($errors !== true) {
                    foreach ($errors as $error) {
                        $this->_getSession()->addError($error);
                    }
                    $response->setError(1);
                }
            }
        }
        if ($response->getError()) {
            $this->_initLayoutMessages('adminhtml/session');
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }
        $this->getResponse()->setBody($response->toJson());
    }

}
