<?php

class Sttl_Bodacustomer_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    protected function _prepareLayout() {
        if (!Mage::registry('current_customer')->isReadonly()) {
            $this->_addButton('save_and_continue', array(
                'label' => Mage::helper('customer')->__('Save and Continue Edit'),
                'onclick' => 'if(!validateBodaCustomer()){return false;} saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
                'class' => 'save'
                    ), 10);
        }

        return parent::_prepareLayout();
    }

}
