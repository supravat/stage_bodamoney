
<?php

class Sttl_Bodacustomer_Model_Adminhtml_System_Config_Source_Pwdtype {

    public function toOptionArray(){
        return array(
            array(
                'value' => 0,
                'label' => Mage::helper('bodacustomer')->__('NO')
            ),
            array(
                'value' => 1,
                'label' => Mage::helper('bodacustomer')->__('YES, Picture Selection Only')
            ),
            array(
                'value' => 2,
                'label' => Mage::helper('bodacustomer')->__('YES, Picture Selection And Type Both')
            ),
        );
    }
}
