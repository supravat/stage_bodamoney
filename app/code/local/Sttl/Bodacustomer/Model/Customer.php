<?php

/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Bodacustomer
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
class Sttl_Bodacustomer_Model_Customer extends Mage_Customer_Model_Customer {

    const XML_PATH_PIN1_CHANGE_EMAIL_IDENTITY = 'customer/bodacustomer/pin1sender_email_identity';
    const XML_PATH_PIN1_CHANGE_EMAIL_TEMPLATE = 'customer/bodacustomer/pin1change_requestemail';
    const XML_PATH_PIN1_CHANGE_RECIPIENT_EMAIL = 'customer/bodacustomer/pin1_recipient_email';

    /*public function loadByEmail($customerEmail) {
        $helper = Mage::helper('bodacustomer');
        if (!$helper->isEnable()) {
            $this->_getResource()->loadByEmail($this, $customerEmail);
            return $this;
        } else {
            if (strpos($customerEmail, '@')) {
                $load_type = 'customer_emails';
            } else {
                $load_type = 'customer_mobiles';
            }
            $collection = $this->getCollection()
                    ->addAttributeToFilter($load_type, array('like' => "%$customerEmail%"));
            if ($collection->count() > 0) {
                $customer = $collection->getFirstItem();
                $loginemail = $customer->getEmail();
                $this->_getResource()->loadByEmail($this, $loginemail);
            } else {
                $this->setData(array());
            }
            return $this;
        }
    }*/

    /**
     * Validate customer attribute values.
     * For existing customer password + confirmation will be validated only when password is set (i.e. its change is requested)
     *
     * @return bool
     */
    public function validate() {
        
        
        $errors = array();
        if (!Zend_Validate::is(trim($this->getFirstname()), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('The first name cannot be empty.');
        }

        if (!Zend_Validate::is(trim($this->getLastname()), 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('The last name cannot be empty.');
        }

        if (!Zend_Validate::is($this->getEmail(), 'EmailAddress')) {
            $errors[] = Mage::helper('customer')->__('Invalid email address "%s".', $this->getEmail());
        }

        $password = $this->getPassword();
      
        if (!$this->getId() && !Zend_Validate::is($password, 'NotEmpty')) {
            $errors[] = Mage::helper('customer')->__('The password cannot be empty.');
        }
        $plength = Mage::helper('bodacustomer/pin')->getPINPasswordLength();
        if (strlen($password) && !Zend_Validate::is($password, 'StringLength', array($plength))) {
            $errors[] = Mage::helper('customer')->__('The minimum password length is %s', $plength);
        }
        $confirmation = $this->getConfirmation();
        if ($password != $confirmation) {
            $errors[] = Mage::helper('customer')->__('Please make sure your passwords match.');
        }

        $entityType = Mage::getSingleton('eav/config')->getEntityType('customer');
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'dob');
        if ($attribute->getIsRequired() && '' == trim($this->getDob())) {
            $errors[] = Mage::helper('customer')->__('The Date of Birth is required.');
        }
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'taxvat');
        if ($attribute->getIsRequired() && '' == trim($this->getTaxvat())) {
            $errors[] = Mage::helper('customer')->__('The TAX/VAT number is required.');
        }
        $attribute = Mage::getModel('customer/attribute')->loadByCode($entityType, 'gender');
        if ($attribute->getIsRequired() && '' == trim($this->getGender())) {
            $errors[] = Mage::helper('customer')->__('Gender is required.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }
    
    public function sendPIN1ChangeRequestEmail($mobilenumber) {
        if ($mobilenumber) {
            $storeId = $this->getStoreId();
            if (!$storeId) {
                $storeId = $this->_getWebsiteStoreId();
            }
            die($mobilenumber);
            $object = new Varien_Object(array('mobile_number' => $mobilenumber));
            /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
            $mailer = Mage::getModel('core/email_template_mailer');
            /** @var $mailer Mage_Core_Model_Email_Info */
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo(Mage::getStoreConfig(self::XML_PATH_PIN1_CHANGE_RECIPIENT_EMAIL));
            $mailer->addEmailInfo($emailInfo);

            // Set all required params and send emails
            $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_PIN1_CHANGE_EMAIL_IDENTITY, $storeId));
            $mailer->setStoreId($storeId);
            $mailer->setTemplateId(Mage::getStoreConfig(self::XML_PATH_PIN1_CHANGE_EMAIL_TEMPLATE, $storeId));
            $mailer->setTemplateParams(array('mobile' => $object));
            try {
                $mailer->send();
                return true;
            } catch (Exception $e) {
                Mage::logException($e);
                return false;
            }
        }
        return false;
    }

    /**
     * Retrieve random password
     *
     * @param   int $length
     * @return  string
     */
    public function generatePassword($length = 8) {
        $length = Mage::helper('bodacustomer/pin')->getPINPasswordLength();
        $chars = Mage_Core_Helper_Data::CHARS_DIGITS;
        $pinGN = Mage::helper('bodacustomer')->getPINGuessingNumbers();
        $newPIN = Mage::helper('core')->getRandomString($length, $chars);
        while (in_array($newPIN, $pinGN)) {
            $newPIN = Mage::helper('core')->getRandomString($length, $chars);
        }
        return $newPIN;
    }
    /*public function authenticate($login, $password)
    {
        $this->loadByEmail($login);
        if ($this->getConfirmation() && $this->isConfirmationRequired()) {
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('This account is not confirmed.'),
                self::EXCEPTION_EMAIL_NOT_CONFIRMED
            );
        }
       
        /*if (!$this->validatePassword($password)) {
            
            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid login or password.'),
                self::EXCEPTION_INVALID_EMAIL_OR_PASSWORD
            );
        }*/
        /*Mage::dispatchEvent('customer_customer_authenticated', array(
           'model'    => $this,
           'password' => $password,
        ));

        return true;
    }*/
}
