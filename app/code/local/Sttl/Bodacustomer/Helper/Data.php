<?php

/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Bodacustomer
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
class Sttl_Bodacustomer_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_PATH_PIN_COMMON_GUESSING_NUMBER = 'customer/bodacustomer/pin_gussing_number';
    const XML_PATH_EMAIL_PIN_CHANGE = 'customer/bodacustomer/email';

    public function isEnable() {
        $isActive = true;
        if ($this->isModuleEnabled() && $isActive) {
            return true;
        }
        return false;
    }

    public function getPINGuessingNumbers($json = false) {
        $common_pins = Mage::getStoreConfig(self::XML_PATH_PIN_COMMON_GUESSING_NUMBER);
        $common_pins = trim($common_pins);
        $result = array();
        if ($common_pins && $common_pins != '') {
            $result = explode(',', $common_pins);
        }
        $result = array_map('trim', $result);
        if ($json) {
            $result = json_encode($result);
        }
        return $result;
    }

    public function getCustomerEmails($custId) {
        if (!$custId) {
            return array();
        }
        $customer = Mage::getModel('customer/customer')->load($custId);
        if (!$customer->getId()) {
            return array();
        }
        $emails = $customer->getData('customer_emails');
        $emails = explode(',', $emails);
        return $emails;
    }

    public function getCustomerMobiles($custId) {
        if (!$custId) {
            return array();
        }
        $customer = Mage::getModel('customer/customer')->load($custId);
        if (!$customer->getId()) {
            return array();
        }
        $mobiles = $customer->getData('customer_mobiles');
        $mobiles = explode(',', $mobiles);
        return $mobiles;
    }

    public function validateMobileNumber($mobile) {
        if ($mobile) {
            if (is_numeric($mobile) && strlen($mobile) == 10) {
                return true;
            }
        }
        return false;
    }

    public function checkExistFinalEmailMobileSaving($data, $hidden, $type = 'customer_emails') {
        $data = explode(',', $data);
        $hidden = explode(',', $hidden);
        $collection = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect(array($type), true);
        $datas = array();
        foreach ($collection as $c) {
            $d = $c->getData($type);
            if ($d) {
                $datas = array_merge($datas, explode(',', $d));
            }
        }
        $datas = array_diff($datas, $hidden);
        $common = array_intersect($datas, $data);
        if ($common && is_array($common) && count($common) > 0 && isset($common[0])) {
            return true;
        }
        return false;
    }

}
