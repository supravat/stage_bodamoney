<?php

class Magesoul_Bodacustomer_Model_Customer_Attribute_Backend_Password extends Mage_Customer_Model_Customer_Attribute_Backend_Password {

    /**
     * Special processing before attribute save:
     * a) check some rules for password
     * b) transform temporary attribute 'password' into real attribute 'password_hash'
     */
    public function beforeSave($object) {
        $password = trim($object->getPassword());
        $len = Mage::helper('core/string')->strlen($password);
        if ($len) {
            $requiredLength = Mage::helper('bodacustomer/pin')->getPINPasswordLength();
            if ($len < $requiredLength) {
                Mage::throwException(Mage::helper('customer')->__("The password must have at least $requiredLength characters. Leading or trailing spaces will be ignored."));
            }
            $object->setPasswordHash($object->hashPassword($password));
        }
    }

}
