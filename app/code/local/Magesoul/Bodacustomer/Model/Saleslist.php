<?php

class Magesoul_Bodacustomer_Model_Saleslist extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bodacustomer/saleslist');
    }

    public function getProductSalesCalculation($order) {


        $percent = Mage::getStoreConfig('marketplace/marketplace_options/percent');
        $lastOrderId = $order->getId();

        foreach ($order->getAllItems() as $item) {
            
            $item_data = $item->getData();
            $attrselection = unserialize($item_data['product_options']);
            $bundle_selection_attributes = unserialize($attrselection['bundle_selection_attributes']);
            if (!$bundle_selection_attributes['option_id']) {
                $temp = $item->getProductOptions();
                if (array_key_exists('seller_id', $temp['info_buyRequest'])) {
                    $seller_id = $temp['info_buyRequest']['seller_id'];
                } else {
                    $seller_id = '';
                }
                $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
                $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
                $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
                $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
                $price = $item->getPrice() / $rates[$currentCurrencyCode];
                if ($seller_id == '') {
                    $collection_product = Mage::getModel('marketplace/product')->getCollection();
                    $collection_product->addFieldToFilter('mageproductid', array('eq' => $item->getProductId()));
                    foreach ($collection_product as $selid) {
                        $seller_id = $selid->getuserid();
                    }
                }
                if ($seller_id == '') {
                    $seller_id = 0;
                }
                $collection1 = Mage::getModel('marketplace/saleperpartner')->getCollection();
                $collection1->addFieldToFilter('mageuserid', array('eq' => $seller_id));
                $taxamount = $item_data['tax_amount'];
                $qty = $item->getQtyOrdered();
                $totalamount = $qty * $price;

                if (count($collection1) != 0) {
                    foreach ($collection1 as $rowdatasale) {
                        $commision = ($totalamount * $rowdatasale->getcommision()) / 100;
                    }
                } else {
                    $commision = ($totalamount * $percent) / 100;
                }

                $wholedata['id'] = $item->getProductId();
                Mage::dispatchEvent('mp_advance_commission', $wholedata);
                $advancecommission = Mage::getSingleton('core/session')->getData('commission');
                if ($advancecommission != '') {
                    $percent = $advancecommission;
                    $commType = Mage::getStoreConfig('mpadvancecommission/mpadvancecommission_options/commissiontype');
                    if ($commType == 'fixed') {
                        $commision = $percent;
                    } else {
                        $commision = ($totalamount * $advancecommission) / 100;
                    }
                    if ($commision > $totalamount) {
                        $commission = $totalamount * (Mage::getStoreConfig('marketplace/marketplace_options/percent')) / 100;
                    }
                }

                $actparterprocost = $totalamount - $commision;
                $collectionsave = Mage::getModel('bodacustomer/saleslist');
                $collectionsave->setsku($item->getSku());
                $collectionsave->setmageproid($item->getProductId());
                $collectionsave->setbarcode(Mage::getSingleton('customer/session')->getCustomer()->getKamomeCustomer());
                $collectionsave->setmageorderid($lastOrderId);
                $collectionsave->setmagerealorderid($order->getIncrementId());
                $collectionsave->setmagequantity($qty);
                $collectionsave->setmageproownerid($seller_id);
                $collectionsave->setcpprostatus(0);

                $collectionsave->setmagebuyerid(Mage::getSingleton('customer/session')->getCustomer()->getId());
                $collectionsave->setmageproprice($price);
                $collectionsave->setmageproname($item->getName());
                if ($totalamount != 0) {
                    $collectionsave->settotalamount($totalamount);
                } else {
                    $collectionsave->settotalamount($price);
                }
                $collectionsave->setTotaltax($taxamount);
                if (Mage::getStoreConfig('marketplace/marketplace_options/taxmanage')) {
                    $actparterprocost = $actparterprocost + $taxamount;
                }
                $collectionsave->settotalcommision($commision);
                $collectionsave->setactualparterprocost($actparterprocost);
                $collectionsave->setcleared_at(date('Y-m-d H:i:s'));
                $collectionsave->save();
                $qty = '';
            }
        }
    }

}
