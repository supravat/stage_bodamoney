<?php

/**
 * Silver Touch Technologies Limited.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 *
 * @category   Sttl
 * @package    Sttl_Bodacustomer
 * @copyright  Copyright (c) 2011 Silver Touch Technologies Limited. (http://www.silvertouch.com/MagentoExtensions)
 * @license    http://www.silvertouch.com/MagentoExtensions/LICENSE.txt
 */
class Magesoul_Bodacustomer_Model_Observer extends Mage_Core_Model_Abstract {

    public function _construct() {
        
    }

    public function addBodacustomerTabObserver($observer) {
        $_block = $observer->getBlock();
        $_type = $_block->getType();
        $moduleName = Mage::app()->getRequest()->getModuleName();
        $controllerName = Mage::app()->getRequest()->getControllerName();
        $actionName = Mage::app()->getRequest()->getActionName();
        if ($moduleName . '/' . $controllerName . '/' . $actionName == 'admin/customer/edit') {
            if ($_type == 'adminhtml/customer_edit_tabs') {
                $_block->addTab('emails_mobiles', array(
                    'label' => Mage::helper('bodacustomer')->__('Multiple Emails / Mobiles'),
                    'content' => Mage::app()->getLayout()->createBlock('bodacustomer/adminhtml_customer_edit_tab_emailmobile')->initFormData()->toHtml(),
                    'after' => 'account'
                ));
            }
        }
        return $this;
    }

    public function salesruleSaveBefore($observer) {
        $ruleObj = $observer->getEvent()->getData('rule');
        $ruleId = $ruleObj->getId();
        $surchargeRuleId = (int) Mage::getStoreConfig('shipping/sttl_surcharge/rule_id');
        if ($ruleId != $surchargeRuleId) { // If ruleId is not surcharge rule id then we have to work
            $short_order = $ruleObj->getData('sort_order');
            if ($short_order == 0) {
                $ruleObj->setData('sort_order', 1);
            }
        }
    }

    public function afterPlaceOrder($observer) {

        $lastOrderId = $observer->getOrder()->getId();
        Mage::log($lastOrderId);
        $order = Mage::getModel('sales/order')->load($lastOrderId);

        Mage::getModel('bodacustomer/saleslist')->getProductSalesCalculation($order);
    }

    public function exportOrder($observer) {

        try {
            $orderIds = $observer->getData('order_ids');
            $payment_status = 'UNPAID';
            foreach ($orderIds as $_orderId) {
                $order = Mage::getModel('sales/order')->load($_orderId);

                if ($order->getBaseTotalDue()) {
                    $payment_status = 'PAID';
                }

                $salesList = Mage::getModel('bodacustomer/saleslist')->load($order->getIncrementId(), 'magerealorderid');

                $salesList->setPayment($payment_status);

                $salesList->save();

                Mage::log($order->getId());
            }
        } catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
    }

}
