<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
        ->newTable($installer->getTable('bodacustomer/saleslist'))
        ->addColumn('autoid', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('mageproid', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Magento product Id')
        ->addColumn('sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'sku')
        ->addColumn('payment', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'payment')
        ->addColumn('barcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Customer Barcode')
        ->addColumn('mageorderid', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Magento Order Id')
        ->addColumn('magerealorderid', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Magento real Order Id')
        ->addColumn('magequantity', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Magento Quantity')
        ->addColumn('mageproownerid', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'Magento product owner id')
        ->addColumn('cpprostatus', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'nullable' => false,
            'default' => '0'
                ), 'cpprostatus')
        ->addColumn('magebuyerid', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
                ), 'magebuyerid')
        ->addColumn('mageproprice', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'mageproprice')
        ->addColumn('mageproname', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
                ), 'Magento Product name')
        ->addColumn('totalamount', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'Order Total Amount')
        ->addColumn('totalamount', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'Order Total Amount')
        ->addColumn('totalcommision', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'Total Commision amount')
        ->addColumn('cleared_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
                ), 'Cleared At')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT
                ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Updated At');

$installer->getConnection()->createTable($table);

$table = $installer->getConnection()
        ->newTable($installer->getTable('bodacustomer/barcode'))
        ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'Id')
        ->addColumn('seller_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 100, array(
                ), 'scanned by Seller')
        ->addColumn('seller_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
                ), 'Seller name')
        ->addColumn('buyer_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'buyer_name')
        ->addColumn('barcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'scanned customer barcode')
        ->addColumn('barcode_count', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'nullable' => false,
                ), 'scanned customer barcode counter')
        ->addColumn('barcode', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'scanned customer barcode')
        ->addColumn('status', Varien_Db_Ddl_Table::TYPE_TINYINT, 2, array(
            'default' => '0'
                ), 'status')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT
                ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Updated At');


$installer->getConnection()->createTable($table);

$installer->endSetup();
