<?php

class Magesoul_Bodacustomer_CartController extends Mage_Core_Controller_Front_Action {

	public function indexAction() {
		if (!Mage::getSingleton('customer/session')->isLoggedIn()):
			$this->_redirect('customer/account/login');
			return;
		endif;

		$this->loadLayout();
		$this->renderLayout();
	}
	
	public function addtocartAction() {
		
		$cartHelper = Mage::helper('checkout/cart');
		$items = $cartHelper->getCart()->getItems();
		foreach($items as $item) {
			$itemId = $item->getItemId();
			$cartHelper->getCart()->removeItem($itemId)->save();
		}
		
		try {
			
			if($product_id = $this->getRequest()->getPost('product_id')) {
				
				
				$product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())->load($product_id);

				$cart = Mage::helper('checkout/cart')->getCart();
				$cart->addProduct($product, array('qty'=> 1));
				$cart->save();
				Mage::log("added to your shopping cart", null, 'developer.log');
				Mage::getSingleton('core/session')->addSuccess('Product added');
                
				
				$this->_redirect('checkout/cart');
			}
			//$this->_goBack();
			
		} catch (Exception $e) {
			$this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
			Mage::log($e);
		}
		
	}

}
