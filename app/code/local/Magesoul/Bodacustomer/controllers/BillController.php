<?php

class Magesoul_Bodacustomer_BillController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {


        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('customer/account/login');
            return;
        endif;

        try {
            if ($this->getRequest()->isPost()) {
                $barcode = $this->getRequest()->getPost('scanned_barcode_id');


                $collection = Mage::getResourceModel('customer/customer_collection')
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('kamome_customer', $barcode)
                        ->getFirstItem();

                $collection = Mage::getResourceModel('customer/customer_collection')
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('kamome_customer', $barcode)
                        ->getFirstItem();


                if ($collection->getKamomeCustomer()) {


                    $cId = (int) Mage::getSingleton('customer/session')->getCustomer()->getId();

                    Mage::register('current_barcode', $barcode);
                    Mage::register('current_customer_id', $collection->getId());
                    $model = Mage::getModel('bodacustomer/barcode');
                    $model->setSellerId($cId);
                    $model->setSellerName(Mage::getSingleton('customer/session')->getCustomer()->getName());
                    $model->setBuyerName($collection->getName());
                    $model->setBarcode($barcode);
                    $model->setStatus(1);
                    $model->save();
                    Mage::getSingleton('core/session')->addSuccess(Mage::helper('bodacustomer')->__('%s has been saved', $barcode));
                } else {
                    Mage::getSingleton('core/session')->addError(Mage::helper('bodacustomer')->__('Invalid Barcode scanned'));
                }
            }
        } catch (Exception $ex) {
            Mage::throwException(Mage::helper('bodacustomer')->__('Error: ' . $ex->getMessage()));
        }
        $this->loadLayout();
        $this->renderLayout();
    }

    public function storeBarcodeAction() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('customer/account/login');
            return;
        endif;
        if ($this->getRequest()->isPost()) {
            var_dump($barcode);
            die;
            $barcode = $this->getRequest()->getPost('scanned_barcode_id');
            $collection = Mage::getResourceModel('customer/customer_collection')
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('kamome_customer', $barcode)
                    ->getFirstItem();

            Mage::register('current_barcode', $barcode);
            if ($collection->getKamomeCustomer()) {
                $barcodeModel = Mage::getModel('bodacustomer/barcode')
                        ->getCollection()
                        ->addFieldToFilter('barcode', array('eq' => $collection->getKamomeCustomer()))
                        ->getFirstItem();
//Mage::register('barcode', $barcode);
//Mage::register('customerid', $collection->getId());
//if ($barcodeModel->getBarcode() != $barcode) {
                $barcodeModel->setBarcode($barcode);
                $barcodeModel->setBarcodeCount(1);
                $barcodeModel->setStatus(1);
                $barcodeModel->save();
                Mage::getSingleton('core/session')->addSuccess('Your Scanned barcode has been saved');
                $this->_redirect('bodacustomer/bill');
//}
            }
            Mage::getSingleton('core/session')->addError("Your Scanned barcode can't saved");
            $this->_redirect('bodacustomer/bill');
        }
        $this->loadLayout();
        $this->renderLayout();
    }

    public function searchAction() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('customer/account/login');
            return;
        endif;


        if ($barcode = $this->getRequest()->getParam('barcode')) {
            $collection = Mage::getResourceModel('customer/customer_collection')
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('kamome_customer', $barcode)
                    ->getFirstItem();

            if ($collection->getKamomeCustomer() == null) {
                Mage::getSingleton('core/session')->addError('Barcode not exist');
                $this->_redirect('bodacustomer/bill');
            }
            Mage::register('current_barcode', $barcode);
            Mage::register('current_customer_id', $collection->getId());
        }

        $this->loadLayout();
        $this->renderLayout();
    }

}
