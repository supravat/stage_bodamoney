<?php

class Magesoul_Bodacustomer_Helper_Pin extends Mage_Core_Helper_Abstract {

    const DEFAULT_PIN_LENGTH = 4;
    const XML_PATH_PIN_IMAGE = 'customer/bodacustomer/pin_img';
    const XML_PATH_PIN_PASSWORD_TYPE = 'customer/bodacustomer/pin_pwd_type';
    const XML_PATH_PIN_PASSWORD_LENGTH = 'customer/bodacustomer/pin_pwd_length';

    public function getPINImages() {
        $images = array();
        for ($i = 0; $i <= 9; $i++) {
            $images[$i] = Mage::getBaseUrl('media') . 'bodacustomer/pin/' . trim((string) Mage::getStoreConfig(self::XML_PATH_PIN_IMAGE . $i));
        }
        return $images;
    }

    public function getPINPasswordEnable() {
        return (int) Mage::getStoreConfig(self::XML_PATH_PIN_PASSWORD_TYPE);
    }

    public function getPINPasswordLength() {
        $len = (int) Mage::getStoreConfig(self::XML_PATH_PIN_PASSWORD_LENGTH);
        if (!$len) {
            $len = self::DEFAULT_PIN_LENGTH;
        }
        return $len;
    }

}
