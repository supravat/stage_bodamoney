<?php

class Magesoul_Bodacustomer_Helper_Data extends Mage_Core_Helper_Abstract {

    const XML_PATH_PIN_COMMON_GUESSING_NUMBER = 'customer/bodacustomer/pin_gussing_number';
    const XML_PATH_EMAIL_PIN_CHANGE = 'customer/bodacustomer/email';

    public function isEnable() {
        $isActive = true;
        if ($this->isModuleEnabled() && $isActive) {
            return true;
        }
        return false;
    }

    public function getPINGuessingNumbers($json = false) {
        $common_pins = Mage::getStoreConfig(self::XML_PATH_PIN_COMMON_GUESSING_NUMBER);
        $common_pins = trim($common_pins);
        $result = array();
        if ($common_pins && $common_pins != '') {
            $result = explode(',', $common_pins);
        }
        $result = array_map('trim', $result);
        if ($json) {
            $result = json_encode($result);
        }
        return $result;
    }

    public function getCustomerEmails($custId) {
        if (!$custId) {
            return array();
        }
        $customer = Mage::getModel('customer/customer')->load($custId);
        if (!$customer->getId()) {
            return array();
        }
        $emails = $customer->getData('customer_emails');
        $emails = explode(',', $emails);
        return $emails;
    }

    public function getCustomerMobiles($custId) {
        if (!$custId) {
            return array();
        }
        $customer = Mage::getModel('customer/customer')->load($custId);
        if (!$customer->getId()) {
            return array();
        }
        $mobiles = $customer->getData('customer_mobiles');
        $mobiles = explode(',', $mobiles);
        return $mobiles;
    }

    public function validateMobileNumber($mobile) {
        if ($mobile) {
            if (is_numeric($mobile) && strlen($mobile) == 10) {
                return true;
            }
        }
        return false;
    }

    public function checkExistFinalEmailMobileSaving($data, $hidden, $type = 'customer_emails') {
        $data = explode(',', $data);
        $hidden = explode(',', $hidden);
        $collection = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect(array($type), true);
        $datas = array();
        foreach ($collection as $c) {
            $d = $c->getData($type);
            if ($d) {
                $datas = array_merge($datas, explode(',', $d));
            }
        }
        $datas = array_diff($datas, $hidden);
        $common = array_intersect($datas, $data);
        if ($common && is_array($common) && count($common) > 0 && isset($common[0])) {
            return true;
        }
        return false;
    }

    public function getCustomerPicture($barcode, $dob) {

        $date_of_birth = date('Y-m-d', strtotime($dob));


        $path = Mage::getBaseDir('media') . DS . 'IdImages';
        $file1 = $barcode . '.jpg';
        $file2 = $barcode . '.png';
        $file3 = $barcode . '.jpeg';
        $file4 = $barcode . '.gif';

        $idPath = $path . DS . $date_of_birth . DS . 'id' . DS . $barcode . '.png';
        $imgPath_1 = $path . DS . $date_of_birth . DS . $file1;
        $imgPath_2 = $path . DS . $date_of_birth . DS . $file2;
        $imgPath_3 = $path . DS . $date_of_birth . DS . $file3;
        $imgPath_4 = $path . DS . $date_of_birth . DS . $file4;

        $idUrl = Mage::getBaseUrl('media') . 'IdImages/' . $date_of_birth . '/id/' . $barcode . '.png';
        $imgUrl_1 = Mage::getBaseUrl('media') . 'IdImages/' . $date_of_birth . '/' . $file1;
        $imgUrl_2 = Mage::getBaseUrl('media') . 'IdImages/' . $date_of_birth . '/' . $file2;
        $imgUrl_3 = Mage::getBaseUrl('media') . 'IdImages/' . $date_of_birth . '/' . $file3;
        $imgUrl_4 = Mage::getBaseUrl('media') . 'IdImages/' . $date_of_birth . '/' . $file4;

        $noImgPath = $path . DS . '2015-10-17' . DS . '171020150000.png';
        $noImgurl = Mage::getBaseUrl('media') . 'IdImages/2015-10-17/171020150000.png';

        $imgUrl = '';
        if (file_exists($imgPath_1)) {
            $imgUrl = $imgUrl_1;
        } elseif (file_exists($imgPath_2)) {
            $imgUrl = $imgUrl_2;
        } elseif (file_exists($imgPath_3)) {
            $imgUrl = $imgUrl_3;
        } elseif (file_exists($imgPath_4)) {
            $imgUrl = $imgUrl_4;
        } else {
            $imgUrl = $noImgurl;
        }

        return $imgUrl;
    }

    public function group_by_date($array, $key) {
        $return = array();
        foreach ($array as $val) {
            $return[$val[$key]][] = $val;
        }
        return $return;
    }

    public function addProductMyCartUrl() {
        return Mage::getUrl('bodacustomer/cart/addtocart');
    }

}
