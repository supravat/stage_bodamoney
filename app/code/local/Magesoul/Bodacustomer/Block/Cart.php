<?php

class Magesoul_Bodacustomer_Block_Cart extends Mage_Core_Block_Template {

    public function getCart() {

        if (Mage::helper('checkout/cart')->getItemsCount() > 0) {
            $this->_emptyCart();
        }
        $this->_addProductToCart();

        $cart = Mage::getSingleton('checkout/session')->getQuote();
        return $cart->getAllItems();
    }

    public function _emptyCart() {
        $cartHelper = Mage::helper('checkout/cart');
        $items = $cartHelper->getCart()->getItems();
        foreach ($items as $item) {
            $itemId = $item->getItemId();
            $cartHelper->getCart()->removeItem($itemId)->save();
        }
        return Mage::helper('checkout/cart')->getItemsCount();
    }

    public function _addProductToCart() {

        try {
            $id = '24'; // Replace id with your product id
            $qty = '1'; // Replace qty with your qty
            
            $params = array('qty' => $qty, 'form_key' => Mage::getSingleton('core/session')->getFormKey() );
            $_product = Mage::getModel('catalog/product')
                    ->setStoreId(
                            Mage::app()
                            ->getStore()
                            ->getId()
                    )
                    ->load($id);
            
            $cart = Mage::helper('checkout/cart')->getCart();
            $cart->addProduct($_product, $params);
            $cart->save();
            Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
            Mage::getSingleton('core/session')->addSuccess('Add item');
            return;
        } catch (Exception $ex) {
            return Mage::getSingleton('core/session')->addError("Can not added item");
        }
    }
    
    public function proccessOrderUrl() {
        
        return Mage::getUrl('bodacustomer/bill/pay');
    }

}
