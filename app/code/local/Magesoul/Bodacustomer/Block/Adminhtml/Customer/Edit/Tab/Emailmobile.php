<?php

class Magesoul_Bodacustomer_Block_Adminhtml_Customer_Edit_Tab_Emailmobile extends Mage_Adminhtml_Block_Widget_Form {

    public function __construct() {
        parent::__construct();
        $this->setTemplate('sttl/bodacustomer/edit/tab/email_mobile.phtml');
    }

    public function initFormData() {
        $customer = Mage::registry('current_customer');
        $emails = Mage::helper('bodacustomer')->getCustomerEmails($customer->getId());
        $mobiles = Mage::helper('bodacustomer')->getCustomerMobiles($customer->getId());
        $data = array(
            'emails' => $emails,
            'mobiles' => $mobiles,
        );
        Mage::register('EM_CUSTOMER_DATA', $data);
        return $this;
    }

    public function getEmailsHtml() {
        return $this->getLayout()->createBlock('core/template')->setTemplate('magesoul/bodacustomer/edit/tab/email_mobile/emails.phtml')->toHtml();
    }

    public function getMobilesHtml() {
        return $this->getLayout()->createBlock('core/template')->setTemplate('magesoul/bodacustomer/edit/tab/email_mobile/mobiles.phtml')->toHtml();
    }

}
