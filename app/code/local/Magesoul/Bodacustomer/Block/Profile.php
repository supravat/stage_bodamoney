<?php

class Magesoul_Bodacustomer_Block_Profile extends Mage_Core_Block_Template {

    public function getCustomer() {
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        if ($customer->getId()) {
            return $customer;
        }
        return null;
    }
}
