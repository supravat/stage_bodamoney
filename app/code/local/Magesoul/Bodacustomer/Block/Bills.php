<?php

class Magesoul_Bodacustomer_Block_Bills extends Mage_Core_Block_Template {

    public function getCustomer() {
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        if ($customer->getId()) {
            return $customer;
        }
        return null;
    }

    public function getBodaCustomer() {

        return Mage::getModel('customer/customer')->load(Mage::registry('current_customer_id'));
    }

    public function getBarcodeList() {
        $barcodeCollections = Mage::getModel('bodacustomer/barcode')
                ->getCollection()
                ->addFieldToFilter('created_at', array('gt' => date("Y-m-d H:i:s", strtotime('-2 day'))))
                //->groupBy('barcode')
                ->setOrder('id', 'DESC');

        $barcodeCollections->getSelect()
                ->columns('COUNT(*) AS scanned_count')
                ->group('barcode');
        //echo $barcodeCollections->getSelect()->__toString(); 
        //var_dump($barcodeCollections);
        $args = array();
        $i = 0;
        foreach ($barcodeCollections as $code) {

            $args[$i]['barcode'] = $code->getBarcode();
            $args[$i]['scanned_count'] = $code->getScannedCount();
            $args[$i]['date'] = date('Y/m/d', strtotime($code->getCreatedAt()));
            $args[$i]['time'] = date("h:i A", strtotime($sTime));
            $i++;
        }
        //var_dump($args);
        $tmp = array();
        foreach ($args as $arg) {
            $tmp[$arg['date']][] = $arg['barcode'];
        }
        //var_dump($tmp);
        $output = array();

        foreach ($tmp as $type => $labels) {
            $output[] = array(
                'date' => $type,
                'barcode' => $labels
            );
        }

        //var_dump($output);
        $barcodesItems = Mage::helper('bodacustomer')->group_by_date($args, 'date');

        //echo '<pre>'; 
        //print_r($barcodesItems);

        return $barcodesItems;
    }

}
