<?php

class Magesoul_Bodacustomer_Block_History extends Mage_Core_Block_Template
{
    /**
     * prepare block's layout
     *
     * @return Magestore_Customercredit_Block_Customercredit
     */
    public function _construct()
    {
        parent::_construct();
        $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
        $sub = Mage::helper('sublogin')->getCurrentSublogin();
        if ($sub) {
            $sub_email = ($sub->getEmail());
            $default_account_type = "sub";
            $default_account_id = $sub->getId();
        } else {
            $customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
            $default_account_type = "main";
            $default_account_id = $customer_id;
        }
        $account = $this->getRequest()->getParam('account_type', $default_account_type);
        $account_id = $this->getRequest()->getParam('account', $default_account_id);

        if (!$sub_email || $sub_email == "") {

            $collection = Mage::getModel('customercredit/transaction')->getCollection()->
                addFieldToFilter('type_transaction_id', array(array('neq' =>
                        Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE)));
            if ($account == "main") {
                $collection->addFieldToFilter('customer_id', $customer_id)->addFieldToFilter('parent_id',
                    0);
            } else {
                $customer_id = $account_id;
                $collection->addFieldToFilter('customer_id', $customer_id)->addFieldToFilter('parent_id',
                    array(array('neq' => 0)));
            }
        } else {

            $collection = Mage::getModel('customercredit/transaction')
                        ->getCollection()
                        ->addFieldToFilter('customer_id',$default_account_id)
                        ->addFieldToFilter('parent_id',
                                    array(array('neq' => 0)))->addFieldToFilter('status', array(array('neq' =>
                                    'pending')))
                        ->addFieldToFilter('type_transaction_id', array(array('neq' =>
                                    Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE)));

        }
        $from = $this->getRequest()->getParam('from');
        $to = $this->getRequest()->getParam('to');

        if ($from != "") {
            $from = strtotime($from);
            $from = date("Y-m-d", $from);

            if ($to != "") {
                $to = strtotime($to);
                $to = date("Y-m-d", $to);
                $collection->addFieldToFilter('transaction_time', array(
                    'from' => $from,
                    'to' => $to,
                    'date' => true,
                    ));

            } else
                $collection->addFieldToFilter('transaction_time', array(
                    'from' => $from,
                    //'to' => $last,
                    'date' => true,
                    ));
        }
        $collection->addFieldToFilter('status',
                                array(
                                    array('neq' =>'pending')
        ));
        $collection->setOrder('transaction_time', 'DESC');
        $this->setCollection($collection);
    }

    public function _prepareLayout()
    {
        $pager = $this->getLayout()->createBlock('page/html_pager',
            'customercredit.history.pager')->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getTransactionType($trans_type_id)
    {

        return Mage::getModel('customercredit/typetransaction')->load($trans_type_id)->getTransactionName();
    }
    public function getCurrencyLabel($credit)
    {
        $credit = Mage::getModel('customercredit/customercredit')->getConvertedFromBaseCustomerCredit($credit);
        return Mage::getModel('customercredit/customercredit')->getLabel($credit);
    }
    public function getSubAccountBalanceLabel($blance)
    {
        return Mage::getModel('customercredit/customercredit')->getSubAccountCreditLabel($blance);
    }
}
