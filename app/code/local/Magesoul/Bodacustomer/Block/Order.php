<?php

class Magesoul_Bodacustomer_Block_Order extends Mage_Core_Block_Template {

    public function getScan() {

        $barcode = $this->getRequest()->getParam('barcode');
        $sellerId = Mage::getSingleton('customer/session')->getCustomer()->getId();

        $barcodeScaned = Mage::getModel('bodacustomer/barcode')->getCollection();
        $barcodeScaned->addFieldToFilter('barcode', $barcode);
        $barcodeScaned->addFieldToFilter('seller_id', $sellerId);
        $barcodeScaned->addFieldToFilter('created_at', array('gt' => date("Y-m-d H:i:s", strtotime('-11 day'))));
        $barcodeScaned->setOrder('id', 'DESC');

        return $barcodeScaned;
    }

    public function getScanSummery() {

        $barcodeCollections = Mage::getModel('bodacustomer/barcode')
                ->getCollection()
                ->addFieldToFilter('created_at', array('gt' => date("Y-m-d H:i:s", strtotime('-11 day'))))
                ->setOrder('id', 'DESC');

        $barcodeCollections->getSelect()
                ->columns('COUNT(*) AS scanned_count')
                ->group('barcode');

        return $barcodeCollections;
    }

    public function getScanProduct() {

        $barcode = $this->getRequest()->getParam('barcode');

        return $barcode;
    }

    public function getProductFilter() {
        
        $barcode = $this->getRequest()->getParam('barcode');
        $sellerId = Mage::getSingleton('customer/session')->getCustomer()->getId();
        
        $pro = Mage::getModel('bodacustomer/saleslist')->getCollection(); 
        $pro->addFieldToFilter( 'magebuyerid' , $sellerId);
        //Zend_Debug::dump($pro->getSelect()->__toString()); die; 
        return $pro;
    }
}
