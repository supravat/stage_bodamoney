<?php

class Magesoul_Bodacart_Model_Observer extends Mage_Core_Model_Abstract {

	public function _construct() {
		
    }
	
	
	/**
	* @param Varien_Event_Observer $observer
	*/
	public function changePrice($observer) {

        $data = Mage::app()->getFrontController()->getRequest()->getParams();

        $event = $observer->getEvent();
        
        $product = $event->getProduct();
        
        $quote_item = $event->getQuoteItem();
        
        $new_price = $data['price'];
        
        $quote_item->setOriginalCustomPrice($data['price']);
        
        $quote_item->getQuote()->save();
		Mage::log('Update cart item', null, 'developer.log');
        return;
	}

}
