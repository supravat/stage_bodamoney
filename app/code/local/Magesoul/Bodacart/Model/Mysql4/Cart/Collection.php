<?php
class Magesoul_Bodacart_Model_Mysql4_Cart_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('bodacart/cart');
    }
    
    public function getSize() {
	
		if (is_null($this->_totalRecords)) {
			$sql = $this->getSelectCountSql();
			$this->_totalRecords = $this->getConnection()->fetchOne($sql, $this->_bindParams);
		}
		return intval($this->_totalRecords);
	}
}
