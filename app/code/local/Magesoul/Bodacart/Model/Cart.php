<?php

class Magesoul_Bodacart_Model_Cart extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bodacart/cart');
    }
}
