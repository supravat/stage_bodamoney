<?php

class Magesoul_Bodacart_Block_Cart extends Mage_Core_Block_Template {

    public function getBuyer() {

        $code = $this->getRequest()->getParam('barcode');

        $collection = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('kamome_customer', $code)
                ->getFirstItem();
        Mage::register('buyer', $collection);
        return Mage::registry('buyer');
    }

    public function getBodacart() {

        $barcode = $this->getRequest()->getParam('barcode');

        $customer = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('kamome_customer', $barcode)
                ->getFirstItem();

        $buyerId = $customer->getId();

        $sellerId = (int) Mage::getSingleton('customer/session')->getCustomer()->getId();

        $cart = Mage::getModel('bodacart/cart')->getCollection();

        $cart->addFieldToFilter('seller_id', $sellerId);
        $cart->addFieldToFilter('buyer_id', $buyerId);

        //var_dump($cart->getSelect()->__toString()); die;

        if ($cart->getSize()) {

            return $cart;
        } else {
            $product = Mage::getModel('catalog/product')->load(1);


            $newCart = Mage::getModel('bodacart/cart');
            $newCart->setProductName("BodaCash");
            $newCart->setProductSku($product->getSku());
            $newCart->setProductId($product->getId());
            $newCart->setProductQty(0);
            $newCart->setProductPrice(0);
            $newCart->setSellerId($sellerId);
            $newCart->setBuyerId($buyerId);
            $newCart->setItemTotal(0);
            $newCart->setGrandTotal(0);
            $newCart->save();


            $cart = Mage::getModel('bodacart/cart')->getCollection();

            $cart->addFieldToFilter('seller_id', $sellerId);
            $cart->addFieldToFilter('buyer_id', $buyerId);



            return $cart;
        }
    }

    public function getGrandTotal() {
        $barcode = $this->getRequest()->getParam('barcode');

        $customer = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('kamome_customer', $barcode)
                ->getFirstItem();

        $buyerId = $customer->getId();

        $buyerId = $customer->getId();

        $sellerId = (int) Mage::getSingleton('customer/session')->getCustomer()->getId();

        $cart = Mage::getModel('bodacart/cart')->getCollection();

        $cart->addFieldToFilter('seller_id', $sellerId);
        $cart->addFieldToFilter('buyer_id', $buyerId);


        $total = 0;


        foreach ($cart as $item) {
            $total += $item->getProductQty() * $item->getProductPrice();
        }
        return number_format($total, 2);
    }

}
