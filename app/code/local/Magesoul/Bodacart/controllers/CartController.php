<?php

require_once 'Mage/Checkout/controllers/CartController.php';

class Magesoul_Bodacart_CartController extends Mage_Checkout_CartController {

    public function indexAction() {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('customer/account/login');
        }

        if (!$barcode = $this->getRequest()->getParam('barcode')) {
            Mage::getSingleton('core/session')->addError("Some error message");
            $this->_redirect('bodacustomer/bill');
        }
        $customer = Mage::getResourceModel('customer/customer_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('kamome_customer', $barcode)
                ->getFirstItem();

        if ($customer->getKamomeCustomer()) {
            
        }

        $this->loadLayout();
        $this->renderLayout();

        /*
         * http://boostalpha.com/blog/magento-custom-pricing-in-cart/
         * http://magentocoder.jigneshpatel.co.in/add-to-cart-with-custom-attributes-values/
         * http://www.tech.theplayhub.com/programmatically_add_product_to_cart_with_price_change/
         */
    }

    public function addAction() {

	

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('bodacart/cart');
        }
        

        $data = $this->getRequest()->getParams();
        
        
        
        $preCart = Mage::getModel('bodacart/cart')->getCollection();
        
        $preCart->addFieldToFilter('buyer_id', $data['buyer_id']);
        $preCart->addFieldToFilter('seller_id', $data['seller_id']);
        
        
        
        $cart = Mage::getModel('checkout/cart');
        try {
			foreach($preCart as $prod) {
				
				
				$params = array(
					'product' => $prod->getProductId(),
					'qty' => $prod->getProductQty(),
				);
				
				
				$product = Mage::getModel('catalog/product')->load($prod->getProductId());

				$product->setSpecialPrice($prod->getProductPrice());
				$product->save();
				$cart->init();
				$cart->addProduct($product, $params);
				$cart->save();	
			}
            
        } catch (Mage_Core_Exception $e) {
            if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                Mage::getSingleton('checkout/session')->addNotice($product->getName() . ': ' . $e->getMessage());
            } else {
                Mage::getSingleton('checkout/session')->addError($product->getName() . ': ' . $e->getMessage());
            }
        } catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
            $this->_redirectReferer();
        }

        $this->_redirect('checkout/cart');

        //$currentUrl = Mage::helper('core/url')->getCurrentUrl();
        //$newUrl = str_replace('', 'checkout/cart/', $currentUrl);
        //Mage::app()->getFrontController()->getResponse()->setRedirect($newUrl);
        //var_dump($productId); die;
    }
    
    
    public function rowUpdateAction(){
		
		 if ($this->getRequest()->isPost()) {
			 
			 $data = $this->getRequest()->getParams(); 
			 
			$cartItem = Mage::getModel('bodacart/cart')->load($data['cartId']);
				
			$cartItem->setProductQty($data['qty']);
			$cartItem->setProductPrice($data['price']);
			$cartItem->setSellerId((int) Mage::getSingleton('customer/session')->getCustomer()->getId());	
			$cartItem->setBuyerId($data['buyerid']);
			$cartItem->setItemTotal($data['qty']*$data['price']);
			$cartItem->setGrandTotal();		
			$cartItem->save();
			
			$this->_redirectReferer();
			return;
		 }
	}
	
	

}
