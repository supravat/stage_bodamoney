<?php

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
        ->newTable($installer->getTable('bodacart/cart'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary' => true,
                ), 'entity id')
                
        ->addColumn('product_sku', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'product_sku')
                
        ->addColumn('product_name', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
            'nullable' => false,
                ), 'product_name')
                
        ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
            'nullable' => false,
                ), 'product_id') 

          ->addColumn('product_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
            'nullable' => false,
                ), 'product_qty')
                
          ->addColumn('product_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'product_price')
                            
          ->addColumn('item_total', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'item_total')

          ->addColumn('grand_total', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
                ), 'item_total')
                                
         ->addColumn('seller_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
            'nullable' => false,
                ), 'seller_id') 
          
         ->addColumn('buyer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 255, array(
            'nullable' => false,
                ), 'buyer_id') 
                      
             
        ->addColumn('cleared_at', Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
                ), 'Cleared At')
        ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
            'default' => Varien_Db_Ddl_Table::TIMESTAMP_INIT
                ), 'Created At')
        ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        ), 'Updated At');

$installer->getConnection()->createTable($table);
$installer->endSetup();
