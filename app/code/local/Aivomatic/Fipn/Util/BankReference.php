<?php
/**
 * Aivomatic_Fipn_Util_BankReference
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id$
 * @file        $URL:$
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Exception class
 */
require_once dirname(dirname(__FILE__)).'/Exception.php';

/**
 * Aivomatic Fipn Util BankReference base class
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Util_BankReference
{
    /**
     * Constructor
     * @param void
     * @return void
     */
    public function __construct() {}
}