<?php
/**
 * Finnish bank reference
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id$
 * @file        $URL:$
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Util_BankReference class
 */
require_once dirname(__FILE__).'.php';

/**
 * Finnish bank reference
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Util_BankReference_Finnish extends Aivomatic_Fipn_Util_BankReference
{
    /**
     * Constructor
     * @param void
     * @return void
     */
    public function __construct()    {
        parent::__construct();
    }

    /**
     * Calculates check sum for finnish bank reference number
     *
     * Check sum is calculated according to Finnish Banker's Assosiation
     * standard.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 3-19 characters.
     * @throws Aivomatic_Fipn_Exception If base is not valid.
     * @return string Single digit check sum
     */
    public static function getCheckSum($base)
    {
        //$base = Aivomatic_Fipn_Validator::validateFbaBankRefBase($base);
        $baseNums = array_reverse(str_split($base));
        $weight = 0;
        $sum = 0;
        foreach ($baseNums as $num) {
            switch ($weight) {
            case 0:
                $sum += 7 * $num;
                $weight = 1;
                break;
            case 1:
                $sum += 3 * $num;
                $weight = 2;
                break;
            case 2:
                $sum += 1 * $num;
                $weight = 0;
                break;
            }
        }
        $rounded = round($sum, -1);

        //Tulot lasketaan yhteen ja summa vähennetään *seuraavasta* täydestä
        // kymmenestä.
        if ($rounded < $sum) {
            $rounded += 10;
        }
        return (string)($rounded-$sum);
    }

    /**
     * Formats finnish bank reference number
     *
     * Formating is performed according to Finnish Banker's Assosiation
     * standard. The reference number is split into chunks of five characters
     * beginning from end. Zero padding is removed from the start.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 4-20 characters.
     * @throws Aivomatic_Fipn_Exception If $ref is not valid.
     * @return string Formated reference number
     */
    public static function formatBankRef($ref)
    {
        //$ref = Aivomatic_Fipn_Validator::validateUnformattedFbaBankRef($ref);
        return trim(strrev(chunk_split(strrev(ltrim($ref, '0')), 5, ' ')));
    }

    /**
     * Returns formated finnish bank reference number including its check sum
     *
     * Check sum and formating according to Finnish Banker's Assosiation
     * standard.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 3-19 characters.
     * @throws Aivomatic_Fipn_Exception If base is not valid.
     * @return string Formated standard compliant bank reference number.
     */
    public static function getBankRef($base)
    {
        return self::formatBankRef(
            $base.self::getCheckSum($base));
    }
}