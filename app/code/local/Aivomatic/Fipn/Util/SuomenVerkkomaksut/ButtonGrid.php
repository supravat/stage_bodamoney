<?php
/**
 * Utility methods for payment button grid picture of Suomen Verkkomaksut
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: ButtonGrid.php 1142 2010-02-17 16:16:18Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Util/SuomenVerkkomaksut/ButtonGrid.php $
 * @copyright   2009 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

$dir = dirname(dirname(dirname(__FILE__)));

/**
 * Require Aivomatic_Fipn_Exception class
 */
require_once $dir.'/Exception.php';

/**
 * Require validator class
 */
require_once $dir.'/Validator/SuomenVerkkomaksut.php';

/**
 * Class to handle payment button grid picture of Suomen Verkkomaksut
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @since       0.6.0+
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Util_SuomenVerkkomaksut_ButtonGrid
{
    const FIPN_SVM_GRID_TYPE_VERTICAL    = 'vertical';
    const FIPN_SVM_GRID_TYPE_HORIZONTAL  = 'horizontal';
    const FIPN_SVM_GRID_COLS_MIN         = 2;
    const FIPN_SVM_GRID_COLS_MAX         = 20;
    const FIPN_SVM_GRID_URL = 'https://img.verkkomaksut.fi/index.svm';

    /**
     * Merchant's secret ID
     *
     * @var string
     */
    protected $secretMerchantId;

    /**
     * Merchant's public ID
     *
     * @var string
     */
    protected $publicMerchantId;

    /**
     * Type of button grid - vertical or horizontal
     *
     * @var string
     */
    protected $type;

    /**
     * Number of grid columns
     *
     * Min. 2, max. 20.
     * @var int
     */
    protected $cols;

    /**
     * Show text 'Kaupan luotettavuuden takaa verkkomaksut.fi':
     * true = yes = 1, false = no = 0
     *
     * @var string
     */
    protected $text;

    /**
     * Constructor
     * @param string $secretMerchantd
     * @param string $publicMerchantd
     * @param string $type 'vertical' (default) or 'horizontal'
     * @param int|string $cols Num of columns. 2-20. Default 2.
     * @throws Aivomatic_Fipn_Exception
     * @return void
     */
    public function __construct($secretMerchantId,
                                $publicMerchantId,
                                $type = self::FIPN_SVM_GRID_TYPE_VERTICAL,
                                $cols = 2,
                                $text = true)
    {
        $this->secretMerchantId =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateSecretMerchantId($secretMerchantId);

        $this->publicMerchantId =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validatePublicMerchantId($publicMerchantId);

        $this->type = self::validateType($type);

        $this->cols = self::validateCols($cols);

        $this->text = self::validateText($text);
    }

    /**
     * Returns auth code
     * @param void
     * @return string Auth code, 16 chars alpha numeric
     */
    public function getAuth()
    {
        $str = $this->publicMerchantId.$this->secretMerchantId;
        return substr(md5($str), 0, 16);
    }

    /**
     * Returns URL of button grid image at Suomen Verkkomaksut
     * @param void
     * @return string URL
     */
    public function getUrl()
    {
        return self::FIPN_SVM_GRID_URL.'?'.
               'id='.$this->publicMerchantId.'&'.
               'type='.$this->type.'&'.
               'cols='.$this->cols.'&'.
               'auth='.$this->getAuth().'&'.
               'text='.$this->text;
    }

    /**
     * Validates type of button grid
     * @param   string $type 'vertical' or 'horizontal'
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $type
     */
    public static function validateType($type)
    {
        if (Aivomatic_Fipn_Validator::isEmptyButNotZero($type)) {
            throw new Aivomatic_Fipn_Exception('$type must not be empty');
        }
        if (strval($type) != self::FIPN_SVM_GRID_TYPE_VERTICAL AND
            strval($type) != self::FIPN_SVM_GRID_TYPE_HORIZONTAL) {
            throw new Aivomatic_Fipn_Exception(
                '$type ('.$type.') must be '
                .self::FIPN_SVM_GRID_TYPE_VERTICAL.' or '
                .self::FIPN_SVM_GRID_TYPE_HORIZONTAL);
        }
        return $type;
    }

    /**
     * Validates number of columns of button grid
     * @param   string|int $cols Integer, min. 2, max. 20
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($cols)
     */
    public static function validateCols($cols)
    {
        if (!Aivomatic_Fipn_Validator::isIntOrString($cols)) {
            throw new Aivomatic_Fipn_Exception(
                '$cols must be an integer or a string (is '.
                gettype($cols).')');
        }
        if (Aivomatic_Fipn_Validator::isEmptyButNotZero($cols)) {
            throw new Aivomatic_Fipn_Exception('$cols must not be empty');
        }
        if (!ctype_digit(strval($cols))) {
            throw new Aivomatic_Fipn_Exception(
                '$cols ('.$cols.') must be numeric');
        }
        if ((intval($cols) < self::FIPN_SVM_GRID_COLS_MIN) OR
            (intval($cols) > self::FIPN_SVM_GRID_COLS_MAX)) {
            throw new Aivomatic_Fipn_Exception(
                '$cols must not be smaller than '.self::FIPN_SVM_GRID_COLS_MIN.
                ' or bigger than '.self::FIPN_SVM_GRID_COLS_MAX.
                ' (is '.strval($cols).')');
        }
        return strval($cols);
    }

    /**
     * Validates boolean 'text'
     * @param   boolean $text true = yes, false = no
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string true = 1, false = 0
     */
    public static function validateText($text)
    {
        if (!is_bool($text)) {
            throw new Aivomatic_Fipn_Exception(
                '$text must be a boolean (is '.gettype($text).')');
        }
        if ($text) {
            return '1';
        } else {
            return '0';
        }
    }
}