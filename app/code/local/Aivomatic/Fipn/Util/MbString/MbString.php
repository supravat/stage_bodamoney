<?php
/**
 * Aivomatic_Fipn_Util_MbString_MbString
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id$
 * @file        $URL:$
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Util_MbString class
 */
require_once dirname(__FILE__).'.php';

/**
 * Aivomatic Fipn Util MbString class for mb_string multibyte string functions
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Util_MbString_MbString extends Aivomatic_Fipn_Util_MbString
{
    /**
     * Constructor
     * @param void
     * @throws Aivomatic_Fipn_Exception
     * @return void
     */
    public function __construct()
    {
        if (!extension_loaded('mbstring')) {
            throw new Aivomatic_Fipn_Exception (
                'mbstring extension is not loaded.');
        }
        parent::__construct();
    }

    /**
     * Returns the number of characters in given string
     * @param string $string
     * @return int Number of characters in $string
     */
    public static function strlenMb($string)
    {
        return mb_strlen($string, 'UTF-8');
    }

    /**
     * Returns internal encoding config value
     * @return string Name of character set
     */
    public static function getInternalEncoding()
    {
        return mb_internal_encoding();
    }

    /**
     * Checks that given string is valid input characterset
     * @param string $charset
     * @return bool True if valid, false if invalid
     */
    public static function isValidInputCharset($charset)
    {
        if (in_array(strtoupper($charset), mb_list_encodings())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns name of the current multibyte extension
     * @param void
     * @return string
     */
    public static function getExtensionName()
    {
        return 'mbstring';
    }

    /**
     * Converts string from input charset to output charset
     * @param string Input charset
     * @param string Output charset
     * @param string String
     * @throws Aivomatic_Fipn_Exception
     * @return string Corverted string
     */
    public static function convertString($input, $output, $string)
    {
        if (!mb_convert_variables($output, $input, $string)) {
            throw new Aivomatic_Fipn_Exception (
                'Error converting string ('.$string.') from '.
                $input.' to '.$output.'.');
        }
        return $string;
    }
}