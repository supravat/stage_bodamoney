<?php
/**
 * Aivomatic_Fipn_Util_MbString_Iconv
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id$
 * @file        $URL:$
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Util_MbString class
 */
require_once dirname(__FILE__).'.php';

/**
 * Aivomatic Fipn Util MbString class for iconv multibyte string functions
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Util_MbString_Iconv extends Aivomatic_Fipn_Util_MbString
{
    /**
     * Constructor
     * @param void
     * @return void
     */
    public function __construct()    {
        if (!extension_loaded('iconv')) {
            throw new Aivomatic_Fipn_Exception (
                'iconv extension is not loaded.');
        }
        parent::__construct();
    }

    /**
     * Returns the number of characters in given string
     * @param string $string
     * @return int Number of characters in $string
     */
    public static function strlenMb($string)
    {
        return iconv_strlen($string, 'UTF-8');
    }

    /**
     * Returns internal encoding config value
     * @return string Name of character set
     */
    public static function getInternalEncoding()
    {
        return iconv_get_encoding('internal_encoding');
    }

    /**
     * Checks that given string is valid input characterset
     * @param string $charset
     * @return bool True if valid, false if invalid
     */
    public static function isValidInputCharset($charset)
    {
        if (!@iconv($charset, iconv_get_encoding('output_encoding'), 'fubar')) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns name of the current multibyte extension
     * @param void
     * @return string
     */
    public static function getExtensionName()
    {
        return 'iconv';
    }

    /**
     * Converts string from input charset to output charset
     * @param string Input charset
     * @param string Output charset
     * @param string String
     * @throws Aivomatic_Fipn_Exception
     * @return string Corverted string
     */
    public static function convertString($input, $output, $string)
    {
        return iconv($input, $output, $string);
    }
}