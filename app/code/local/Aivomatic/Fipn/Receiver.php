<?php
/**
 * Aivomatic_Fipn_Receiver
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Receiver.php 1534 2011-08-23 10:29:35Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Receiver.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Fipn base class (../Fipn.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Fipn Receiver base class
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Receiver extends Aivomatic_Fipn
{

    /**
     * Payment status according to the response from payment gateway
     *
     * 'success' or 'cancel' (or 'error' or 'dealy' depending on payment gateway).
     * Set by successUrl or cancelUrl (or errorUrl or delayUrl).
     *
     * @see Aivomatic_Fipn_Sender::$successUrl
     * @see Aivomatic_Fipn_Sender::$cancelUrl
     * @see Aivomatic_Fipn_Sender::$errorUrl
     * @see Aivomatic_Fipn_Sender::$delayUrl
     * @see Aivomatic_Fipn_Sender::$declineUrl
     * @see Aivomatic_Fipn_Validator::validateResponseStatus()
     * @var string
     */
    protected $responseStatus = '';

    /**
     * Responce MAC (Message Authentication Code) from the payment gateway
     *
     * If the MAC calculated by Fipn matches with this, we know that the
     * response originates from the payment gateway and that the return values
     * are not tampered.
     *
     * @see Aivomatic_Fipn_Validator::validateResponseMac()
     * @var string
     */
    protected $responseMac = 'notset';

    /**
     * Transaction Id set by the payment gateway
     *
     * Some payment gateways define a transaction Id (code or number) separately
     * of order Id.
     * @var string
     */
    protected $transactionId = '';

    /**
     * Is the response authentic
     *
     * Did the MAC and responce MAC match?
     * @see $mac, $responseMac
     * @var bool True if authentic, false if not
     */
    protected $isAuthenticResponse = false;

    /**
     * Was the payment accepted and successful
     * @var bool True if everything is OK, false if not
     */
    protected $isSuccess = false;

    /**
     * Was the payment canceled by the customer at the payment gateway
     * @var bool True if payment was canceled
     */
    protected $isCancel = false;

    /**
     * Did some error (technical or authentication) happen during payment
     * @var bool True if something is wrong
     */
    protected $isError = true;

    /**
     * Is the payment delayed, ie. its status is not know now but later?
     * @var bool True if delayed
     */
    protected $isDelay = false;

    /**
     * Is the payment declined, ie. because of bad credit or an error?
     * @var bool True if declined
     */
    protected $isDecline = false;

    /**
     * HTTP GET-parameters of the response request
     * @see Aivomatic_Fipn_Validator::validateResponseGetParams()
     * @var array
     */
    protected $responseGetParams = array();

    /**
     * @var object Aivomatic_Fipn_Validator -subclass
     */
    protected $Validator;

    /**
     * Constructor
     * @param   void
     * @return  void
     */
    protected function __construct()
    {
        parent::__construct();
    }

    /**
     * Returns value of a GET-parameter by key name
     *
     * @uses $responseGetParams
     * @uses responseGetParamExists()
     * @see array_key_exists()
     * @param string $keyName Name of the GET-parameter key.
     * @throws  Aivomatic_Fipn_Exception If key doesn't exist.
     * @return string Value of the GET-parameter.
     */
    protected function getResponseGetParam($keyName)
    {
        if(!self::responseGetParamExists($keyName)){
            throw new Aivomatic_Fipn_Exception(
                'Response request parameter "'.$keyName.'" does not exist');
        }
        return $this->responseGetParams[$keyName];
    }

    /**
     * Returns TRUE if GET-param exists, FALSE if it doens't
     *
     * @uses $responseGetParams
     * @see array_key_exists()
     * @param string $keyName Name of the GET-parameter key.
     * @return bool
     */
    protected function responseGetParamExists($keyName)
    {
        return array_key_exists($keyName, $this->responseGetParams);
    }

    /**
     * Set response status according to $_GET['fipnstatus'] or child class's
     * third constructor argumet ($fipnStatus)
     * @uses $responseGetParams
     * @uses $responseStatus
     * @param string $argStatus Subclass constructor's third argument
     * @throws Aivomatic_Fipn_Exception If not valid.
     * @return string Valid return status
     */
    protected function figureResponseStatus($responseGetParams, $argStatus)
    {
        if (!empty($argStatus)) {
            $argStatus = $this->Validator->validateResponseStatus($argStatus);
        }
        if (array_key_exists('fipnstatus', $responseGetParams)) {
            $getStatus = $this->Validator->validateResponseStatus(
                $responseGetParams['fipnstatus']);
        } else {
            $getStatus = '';
        }
        if (empty($argStatus) AND empty($getStatus)) {
            throw new Aivomatic_Fipn_Exception('fipnstatus parameter not set');
        } elseif (!empty($argStatus) AND !empty($getStatus)) {
            if (strtolower($argStatus) != strtolower($getStatus)) {
                throw new Aivomatic_Fipn_Exception(
                    'Mismatching two status parameters (GET and arg.).');
            } else {
                return $argStatus;
            }
        } elseif (!empty($argStatus)) {
            return $argStatus;
        } else {
            return $getStatus;
        }
    }

    /**
     * Sets isSuccess, isCancel and isError status properties
     *
     * @param void
     * @return void
     */
    protected function setOutcome()
    {
        if (!$this->getIsAuthenticResponse()) {
            $this->isSuccess = false;
            $this->isCancel = false;
            $this->isError = true;
            $this->isDelay = false;
            $this->isDecline = false;
        } else {
            switch ($this->responseStatus) {
            case ('success'):
                $this->isSuccess = true;
                $this->isCancel  = false;
                $this->isError   = false;
                $this->isDelay = false;
                $this->isDecline = false;
                break;
            case ('notify'):
                $this->isSuccess = true;
                $this->isCancel  = false;
                $this->isError   = false;
                $this->isDelay = false;
                $this->isDecline = false;
                break;
            case ('cancel'):
                $this->isSuccess = false;
                $this->isCancel  = true;
                $this->isError   = false;
                $this->isDelay = false;
                $this->isDecline = false;
                break;
            case ('error'):
                $this->isSuccess = false;
                $this->isCancel  = false;
                $this->isError   = true;
                $this->isDelay = false;
                $this->isDecline = false;
                break;
            case ('delay'):
                $this->isSuccess = false;
                $this->isCancel  = false;
                $this->isError   = false;
                $this->isDelay = true;
                $this->isDecline = false;
                break;
            case ('decline'):
                $this->isSuccess = false;
                $this->isCancel  = false;
                $this->isError   = false;
                $this->isDelay   = false;
                $this->isDecline = true;
                break;
            default:
                throw new Aivomatic_Fipn_Exception(
                    'Unknown response status. Can\'t set outcome');
            }
        }
    }

    /**
     * Authenticates response
     *
     * Sets isAuthentic if response request's MAC is OK
     * @param void
     * @return void
     */
    protected function authenticateResponse()
    {
        $this->setMac();
        $this->isAuthenticResponse = $this->compareMacs();
    }

    /**
     * Get response status
     * @uses responseStatus
     * @param void
     * @return string
     */
    public function getResponseStatus()
    {
        return $this->responseStatus;
    }

    /**
     * Get response MAC
     * @param void
     * @return string
     * @uses $responseMac
     */
    public function getResponseMac()
    {
        return $this->responseMac;
    }

    /**
     * Get transactionId
     * @param void
     * @return string
     * @uses $transactionId
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Get bankReference
     * @param void
     * @return string
     * @uses $bankReference
     */
    public function getBankReference()
    {
        return $this->bankReference;
    }

    /**
     * Compare MAC calculated localy to the MAC of response request
     * @uses $mac, $responseMac
     * @param void
     * @return bool
     */
    protected function compareMacs()
    {
        return $this->getMac() == $this->getResponseMac();
    }

    /**
     * Returns authentication status
     * @uses $isAuthenticResponse
     * @param void
     * @return bool True if authentic response
     */
    public function getIsAuthenticResponse()
    {
        return $this->isAuthenticResponse;
    }

    /**
     * Returns success status
     * @uses $isSuccess
     * @param void
     * @return bool True if success
     */
    public function getIsSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * Returns cancel status
     * @uses $isCancel
     * @param void
     * @return bool True if cancel
     */
    public function getIsCancel()
    {
        return $this->isCancel;
    }

    /**
     * Returns error status
     * @uses $isError
     * @param void
     * @return bool True if error
     */
    public function getIsError()
    {
        return $this->isError;
    }

    /**
     * Returns delayed status
     * @uses $isDelayed
     * @param void
     * @return bool True if delayed
     */
    public function getIsDelay()
    {
        return $this->isDelay;
    }

    /**
     * Returns decline status
     * @uses $isDecline
     * @param void
     * @return bool True if declined
     */
    public function getIsDecline()
    {
        return $this->isDecline;
    }

    /**
     * Returns status code
     * @param void
     * @return string
     */
    public function getStatusCode()
    {
        return '';
    }

    /**
     * Returns status code description
     * @param void
     * @return string
     */
    public function getStatusCodeDesc($statusCode = false)
    {
        return '';
    }

    /**
     * Textual presentation of object's properties
     *
     * Gathers and formats class properties to a string to be used by eg. echo
     * and print PHP-functions. Nice for debuging etc.
     * This method adds Receiver-specific properties to the end of the string.
     * @param   void
     * @see     $responseStatus, $responseMac, $transactionId, $bankReference,
     *          $myMac, @isAuthenticResponse, $isSuccess, $isCancel, $isError,
     *          $isDelay, $isDecline
     * @see     print_r
     * @uses    parent::__toString()
     * @return  string Class properties as formated string
     */
    public function __toString()
    {
        return
        parent::__toString().
        __CLASS__."::\n".
        'responseStatus:        '.$this->getResponseStatus()."\n".
        'responseMac:           '.$this->getResponseMac()."\n".
        'transactionId:         '.$this->getTransactionId()."\n".
        'bankReference:         '.$this->getBankReference()."\n".
        'isAuthenticResponse:   '.print_r($this->getIsAuthenticResponse(), true)."\n".
        'isSuccess:             '.print_r($this->getIsSuccess(), true)."\n".
        'isCancel:              '.print_r($this->getIsCancel(), true)."\n".
        'isError:               '.print_r($this->getIsError(), true)."\n".
        'isDelay:               '.print_r($this->getIsDelay(), true)."\n".
        'isDecline:             '.print_r($this->getIsDecline(), true)."\n";
    }
}
?>