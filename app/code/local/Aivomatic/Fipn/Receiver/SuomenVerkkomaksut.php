<?php
/**
 * Receiver_SuomenVerkkomaksut
 *
 * @category    Aivomatic
 * @package     Fipn_SuomenVerkkomaksut
 * @version     ver. 1.10.2 - $Id: SuomenVerkkomaksut.php 1726 2013-01-30 05:46:21Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Receiver/SuomenVerkkomaksut.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require receiver base class (../Receiver.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Require validator class
 */
require_once dirname(dirname(__FILE__)).'/Validator/SuomenVerkkomaksut.php';

/**
 * Aivomatic Fipn Sender base class
 *
 * @category    Aivomatic
 * @package     Fipn_SuomenVerkkomaksut
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Receiver_SuomenVerkkomaksut extends Aivomatic_Fipn_Receiver
{
    /**
     * Payment method id from Suomen Verkkomaksut
     * @var string
     */
    protected $methodId = '0';

    /**
     * Get method id
     * @param void
     * @return string
     */
    public function getMethodId()
    {
        return $this->methodId;
    }

    /**
     * Timestamp, an Unix timestamp returned by Suomen Verkkomaksut
     * @var string
     */
    protected $timestamp = 0;

    /**
     * Constructor
     *
     * @param   string $secretMerchantId
     * @param   string $responseGetParams
     * @param   string $fipnStatus Valid values are 'success' or 'cancel'
     * @return  void
     */
    public function __construct($secretMerchantId,
                                $responseGetParams,
                                $fipnStatus = null)
    {

        parent::__construct();

        $this->Validator = new Aivomatic_Fipn_Validator_SuomenVerkkomaksut();

        $this->responseStatus =
            $this->figureResponseStatus($responseGetParams, $fipnStatus);

        $this->responseGetParams =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateResponseGetParams($this->responseStatus,
                                      $responseGetParams);

        $this->secretMerchantId =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateSecretMerchantId($secretMerchantId);

        //Suomen Verkkomaksuilla ei ole erillistä error-URLia, ja ne käyttää
        //cancel-URLia siihen merkitykseen speksien vastaisesti ilman mitään
        //niiden omia parametreja
        if ($this->responseStatus == 'cancel' AND
            !isset($responseGetParams['ORDER_NUMBER']) AND
            !isset($responseGetParams['TIMESTAMP']) AND
            !isset($responseGetParams['RETURN_AUTHCODE']) AND
            !isset($responseGetParams['PAID'])) {
            $this->responseStatus = 'error';
        }

        switch ($this->responseStatus) {
        case 'success':
        case 'notify':
        case 'delay':
            $this->orderId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateOrderId($this->getResponseGetParam('ORDER_NUMBER'));

            $this->timestamp =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateTimestamp($this->getResponseGetParam('TIMESTAMP'));

            $this->responseMac =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateResponseMac($this->getResponseGetParam('RETURN_AUTHCODE'));
            $this->transactionId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateTransactionId($this->getResponseGetParam('PAID'));
            $this->methodId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateMethodId($this->getResponseGetParam('METHOD'));
            $this->authenticateResponse();
            break;
        /*case ('notify'):
            $this->orderId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateOrderId($this->getResponseGetParam('ORDER_NUMBER'));

            $this->timestamp =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateTimestamp($this->getResponseGetParam('TIMESTAMP'));

            $this->responseMac =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateResponseMac($this->getResponseGetParam('RETURN_AUTHCODE'));
            $this->transactionId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateTransactionId($this->getResponseGetParam('PAID'));
            $this->methodId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateMethodId($this->getResponseGetParam('METHOD'));
            $this->authenticateResponse();
            break;*/
        case ('cancel'):
            $this->orderId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateOrderId($this->getResponseGetParam('ORDER_NUMBER'));

            $this->timestamp =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateTimestamp($this->getResponseGetParam('TIMESTAMP'));

            $this->responseMac =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateResponseMac($this->getResponseGetParam('RETURN_AUTHCODE'));
            $this->transactionId = '';
            $this->authenticateResponse();
            break;
        case ('error'):
            $this->orderId =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateOrderId($this->getResponseGetParam('fipnorderid'));
            $this->isAuthenticResponse = true;
            break;
        default:
            throw new Aivomatic_Fipn_Exception(
                'Unknown response status. Can\'t set MAC');
        }
        $this->setOutcome();
    }

    /**
     * @param void
     * @return string Unix timestamp
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param void
     * @throws Aivomatic_Fipn_Exception If status isn't 'success' or 'cancel'
     * @return void
     */
    protected function setMac()
    {
        switch ($this->getResponseStatus()) {
        case 'success':
        case 'notify':
        case 'delay':
            $myMacString = $this->getOrderId().'|'.
                           $this->getTimestamp().'|'.
                           $this->getTransactionId().'|'.
                           $this->getMethodId().'|'.
                           $this->getSecretMerchantId();
            break;
        /*case ('notify'):
            $myMacString = $this->getOrderId().'|'.
                           $this->getTimestamp().'|'.
                           $this->getTransactionId().'|'.
                           $this->getMethodId().'|'.
                           $this->getSecretMerchantId();
            break;*/
        case ('cancel'):
            $myMacString = $this->getOrderId().'|'.
                           $this->getTimestamp().'|'.
                           $this->getSecretMerchantId();
            break;
        default:
            throw new Aivomatic_Fipn_Exception(
                'Unknown response status. Can\'t set MAC');
        }
        $this->mac = parent::calculateMac($myMacString);
    }

    /**
     * Sets isSuccess, isCancel and isError status properties
     *
     * @param void
     * @return void
     */
    /*protected function setOutcome()
    {
        if (!$this->getIsAuthenticResponse()) {
            $this->isSuccess = false;
            $this->isCancel = false;
            $this->isError = true;
            $this->isDelay = false;
        } else {
            switch ($this->responseStatus) {
            case ('success'):
                $this->isSuccess = true;
                $this->isCancel  = false;
                $this->isError   = false;
                $this->isDelay = false;
                break;
            case ('notify'):
                $this->isSuccess = true;
                $this->isCancel  = false;
                $this->isError   = false;
                $this->isDelay = false;
                break;
            case ('cancel'):
                $this->isSuccess = false;
                $this->isCancel  = true;
                $this->isError   = false;
                $this->isDelay = false;
                break;
            case ('error'):
                $this->isSuccess = false;
                $this->isCancel  = false;
                $this->isError   = true;
                $this->isDelay = false;
                break;
            default:
                throw new Aivomatic_Fipn_Exception(
                    'Unknown response status. Can\'t set outcome');
            }
        }
    }*/

    /**
     * Get payment method name
     * @param void
     * @return string Name
     */
    public function getMethodName()
    {
        $codes = array(
            '0' => 'not set',
            '1' => 'Nordea Pankki',
            '2' => 'Osuuspankki',
            '3' => 'Sampo Pankki',
            '4' => 'Tapiola',
            '5' => 'Ålandsbanken',
            '6' => 'Handelsbanken',
            '7' => 'Aktia/Noaa/Sp/Pop',
            '8' => 'Luottokunta',
            '9' => 'PayPal',
            '10' => 'S-Pankki',
            '11' => 'Klarna, laskulla',
            '12' => 'Klarna, osamaksulla',
            '18' => 'Joustoraha',
            '19' => 'Collector');
        if (isset($codes[$this->getMethodId()])) {
            return $codes[$this->getMethodId()];
        } else {
            return 'n/a';
        }
    }

    /**
     * Returns method Id as the status code
     * @param void
     * @return string
     */
    public function getStatusCode()
    {
        return $this->getMethodId();
    }

    /**
     * Returns method name as the status code description
     * @param void
     * @return string
     */
    public function getStatusCodeDesc($statusCode = false)
    {
        return $this->getMethodName();
    }

    /**
     * Textual presentation of object's properties
     * @param   void
     * @return  string
     */
    public function __toString()
    {
        return
        parent::__toString().
        __CLASS__."::\n".
        'timestamp:             '.$this->timestamp.
        ' ('.date(DATE_ISO8601, $this->timestamp).")\n".
        'statusCode:            '.$this->getStatusCode()."\n".
        'statusCodeDesc:        '.$this->getStatusCodeDesc()."\n";
    }
}
?>