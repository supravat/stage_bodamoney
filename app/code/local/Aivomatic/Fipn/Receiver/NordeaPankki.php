<?php
/**
 * Receiver_NordeaPankki
 *
 * @category    Aivomatic
 * @package     Fipn_NordeaPankki
 * @version     ver. 1.8.1 - $Id: NordeaPankki.php 574 2009-03-03 11:24:23Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Receiver/NordeaPankki.php $
 * @copyright   2009 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require receiver base class (../Receiver.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Require validator class
 */
require_once dirname(dirname(__FILE__)).'/Validator/NordeaPankki.php';

/**
 * Aivomatic Fipn Sender base class
 *
 * @category    Aivomatic
 * @package     Fipn_NordeaPankki
 * @since       0.2.0+
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Receiver_NordeaPankki extends Aivomatic_Fipn_Receiver
{
    /**
     * Constructor
     *
     * @param   string $secretMerchantId
     * @param   string $responseGetParams
     * @param   string $fipnStatus Valid values are 'success', 'cancel'
     *                             or 'error'
     * @return  void
     */
    public function __construct($secretMerchantId,
                                $responseGetParams,
                                $fipnStatus = null)
    {

        parent::__construct();

        $this->Validator = new Aivomatic_Fipn_Validator_NordeaPankki();

        $this->responseStatus =
            $this->figureResponseStatus($responseGetParams, $fipnStatus);

        $this->responseGetParams =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateResponseGetParams($this->responseStatus,
                                      $responseGetParams);

        $this->secretMerchantId =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateSecretMerchantId($secretMerchantId);

        $this->returnWapiVersion = Aivomatic_Fipn_Validator_NordeaPankki::
            validateWapiVersion($this->getResponseGetParam('RETURN_VERSION'));

        $this->orderId =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateOrderId($this->getResponseGetParam('RETURN_STAMP'));

        $this->bankReference =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateUnformattedFbaBankRef(
                $this->getResponseGetParam('RETURN_REF'));

        $this->responseMac =
                Aivomatic_Fipn_Validator_NordeaPankki::
                validateResponseMac($this->getResponseGetParam('RETURN_MAC'));

        switch ($this->responseStatus) {
        case ('success'):
            $this->transactionId =
                Aivomatic_Fipn_Validator_NordeaPankki::
                validateTransactionId(
                    $this->getResponseGetParam('RETURN_PAID'));
            break;
        case ('cancel'):
            //NordeaPankki doesn't have special GET-params for cancel response
            break;
        case ('error'):
            //NordeaPankki doesn't have special GET-params for error response
            break;
        default:
            throw new Aivomatic_Fipn_Exception(
                'Unknown response status.');
        }
        $this->authenticateResponse();
        $this->setOutcome();
    }

    /**
     * @param void
     * @throws Aivomatic_Fipn_Exception
     * @return void
     */
    protected function setMac()
    {
        $myMacString =  $this->getReturnWapiVersion().'&'.
                        $this->getOrderId().'&'.
                        $this->getBankReference().'&'.
                        $this->getTransactionId().'&'.
                        $this->getSecretMerchantId().'&';
        $this->mac = parent::calculateMac($myMacString);
    }

    /**
     * Return Web API version number of the return request
     * @uses Aivomatic_Fipn_Validator_NordeaPankki::FIPN_NORDEAP_WAPI_VERSION
     * @param void
     * @return string Web API version
     */
    public function getReturnWapiVersion()
    {
        return Aivomatic_Fipn_Validator_NordeaPankki::FIPN_NORDEAP_WAPI_VERSION;
    }

    /**
     * Textual presentation of object's properties
     * @param   void
     * @return  string
     */
    public function __toString()
    {
        return
        parent::__toString().
        __CLASS__."::\n".
        'returnWapiVersion:     '.$this->getReturnWapiVersion()."\n";
    }
}
?>