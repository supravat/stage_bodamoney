<?php
/**
 * Aivomatic_Fipn_Factory
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Factory.php 1276 2010-06-14 08:50:19Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Factory.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Exception class
 */
require_once dirname(__FILE__).'/Exception.php';

/**
 * FIPN Factory methods and related utility methods
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @since       0.0.0+
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Factory
{

    /**
     * Produces instance of Aivomatic_Fipn_Receiver -subclass
     *
     * Implemets Factory pattern.
     * @param string    $secretMerchantId   Secret ID of the merchant for this
     *                                      payment gateway.
     * @param array     $responseGetParams  Response request's GET-parameters as
     *                                      an array.
     * @param string    $fipnpgw            Name of the Payment Gateway.
     *                                      Case insensitive. Can be null if
     *                                      GET-params include ['fipnpgw'].
     * @param string    $fipnstatus         Response status. Must be 'success',
     *                                      'cancel' or 'error'. Can be null if
     *                                      GET-params include ['fipnpstatus'].
     * @throws  Aivomatic_Fipn_Exception On failure.
     * @return object Instance of Aivomatic_Fipn_Receiver-child class.
     */
    public static function getReceiver($secretMerchantId,
                                $responseGetParams,
                                $fipnPwg = null,
                                $fipnStatus = null)
    {
        if ($fipnPwg == null) {
            if (empty($responseGetParams['fipnpgw'])) {
                throw new Aivomatic_Fipn_Exception (
                    'Payment Gateway name ($fipnPwg or $_GET[\'fipnpgw\'])'.
                    ' missing');
            } else {
                $nameOfPaymentGateway = $responseGetParams['fipnpgw'];
            }
        } else {
            $nameOfPaymentGateway = $fipnPwg;
        }
        $pgwName = self::getValidPaymentGatewayName($nameOfPaymentGateway);
        require_once dirname(__FILE__).'/Receiver/'.$pgwName.'.php';
        $className = 'Aivomatic_Fipn_Receiver_'.$pgwName;
        try {
            $Receiver = new $className($secretMerchantId,
                                       $responseGetParams,
                                       $fipnStatus);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw $e;
        }
        return $Receiver;
    }

    /**
     * Produce instance of Aivomatic_Fipn_Sender -subclass
     *
     * Implemets Factory pattern.
     * @param string $nameOfPaymentGateway Name of the Payment Gateway.
     *               Case insensitive.
     * @throws Aivomatic_Fipn_Exception On failure.
     * @return object Instance of Aivomatic_Fipn_Sender subclass.
     */
    public static function getSender($nameOfPaymentGateway)
    {
        $pgwName = self::getValidPaymentGatewayName($nameOfPaymentGateway);
        require_once dirname(__FILE__).'/Sender/'.$pgwName.'.php';
        $className = 'Aivomatic_Fipn_Sender_'.$pgwName;
        try {
            $Sender = new $className;
        } catch (Aivomatic_Fipn_Exception $e) {
            throw $e;
        }
        return $Sender;
    }

    /**
     * @param string $nameOfPaymentGateway Name of the Payment Gateway.
     *               Case insensitive.
     * @throws Aivomatic_Fipn_Exception On failure.
     * @return string   Valid payment gateway name (as it is in the file name)
     */
    protected static function getValidPaymentGatewayName($nameOfPaymentGateway)
    {
        $paymentGateways = self::getPaymentGateways();
        $lcNameOfPaymentGateway = strtolower($nameOfPaymentGateway);
        if (!array_key_exists($lcNameOfPaymentGateway, $paymentGateways)) {
            throw new Aivomatic_Fipn_Exception (
                'Payment Gateway "'.$nameOfPaymentGateway.'" doesn\'t exist');
        }
        return $paymentGateways[$lcNameOfPaymentGateway];
    }

    /**
     * Get list of available payment gateway methods
     * @param   void
     * @return  array lowerCaseName => fileName
     */
    protected static function getPaymentGateways()
    {
        $senderArray = array();
        $senderIterator = new directoryIterator(dirname(__FILE__).'/Sender');
        foreach ($senderIterator as $sender) {
            if ($sender->isFile()) {
                $senderArray[] = $sender->getFileName();
            }
        }
        $validatorArray = array();
        $validatorIterator = new directoryIterator(dirname(__FILE__).'/Validator');
        foreach ($validatorIterator as $validator) {
            if ($validator->isFile()) {
                $validatorArray[] = $validator->getFileName();
            }
        }
        $pgwArray = array();
        $receiverIterator = new directoryIterator(dirname(__FILE__).'/Receiver');
        foreach ($receiverIterator as $receiver) {
            if (in_array($receiver->getFileName(), $senderArray) AND
                in_array($receiver->getFileName(), $validatorArray)){
                $name = str_replace('.php', '', $receiver->getFileName());
                $pgwArray[strtolower($name)] = $name;
            }
        }
        return $pgwArray;
    }

    /**
     * Returns name of payment gateway based on $_GET-param and file naming
     * @param array $responseGetParams Response request's GET-parameters.
     * @throws Aivomatic_Fipn_Exception On failure.
     * @return string Name of the payment gateway as it is in file names and if
     *                both sender- and receiver- files exist.
     */
    public static function getPaymentGatewayName($responseGetParams)
    {
        if (empty($responseGetParams['fipnpgw'])) {
                throw new Aivomatic_Fipn_Exception (
                    'Payment Gateway name ($_GET[\'fipnpgw\']) missing');
        }
        return self::getValidPaymentGatewayName($responseGetParams['fipnpgw']);
    }

    /**
     * Returns an instance of a multibyte string extension class
     * @param string Name of particular extension. Default false.
     * @throws Aivomatic_Fipn_Exception On failure.
     * @return object Instance of subclass of Aivomatic_Fipn_Util_MbString
     */
    public static function getMbString($extension = false)
    {
        if ($extension == 'mbstring' OR
            ($extension === false AND extension_loaded('mbstring'))) {
            require_once dirname(__FILE__).'/Util/MbString/MbString.php';
            return new Aivomatic_Fipn_Util_MbString_MbString();
        } elseif ($extension == 'iconv' OR
            ($extension === false AND extension_loaded('iconv'))) {
            require_once dirname(__FILE__).'/Util/MbString/IConv.php';
            return new Aivomatic_Fipn_Util_MbString_IConv();
        } else {
            throw new Aivomatic_Fipn_Exception (
                'Server does not support multibyte functions. '.
                'Fipn needs mbstring or iconv PHP extension.');
        }
    }

    /**
     * Returns an instance of a bank barcode class
     * @param int|string Version number. Version 2 is supporter. Versions 4 or 5
     * will be supported in the future.
     * @throws Aivomatic_Fipn_Exception On failure.
     * @return object Instance of subclass of Aivomatic_Fipn_Util_BankBarcode
     */
    public static function getBankBarcode($version = '2')
    {
        $ver = strval($version);
        if ($ver !== '2') {
            throw new Aivomatic_Fipn_Exception (
                'Bank barcode version must be "2" (is "'.$ver.'")');
        }
        require_once dirname(__FILE__).'/Util/BankBarcode/v2.php';
        return new Aivomatic_Fipn_Util_BankBarcode_v2();
    }
}
?>