<?php
/**
 * Aivomatic_Fipn_Validator
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Validator.php 893 2009-09-16 08:10:54Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Validator.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Factory class
 */
require_once 'Factory.php';

/**
 * Aivomatic Fipn Validator base class
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Validator2
{
// - - - - - - - - Secret Merchant Id Max Len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of merchant's secret Id
     * @var int
     */
    private $secretMerchantIdMaxLen = 0;

    /**
     * Set maximum length of merchant's secret Id
     * @param int
     * @return void
     */
    protected function setSecretMerchantIdMaxLen($int)
    {
        $this->secretMerchantIdMaxLen = $int;
    }

    /**
     * Get maximum length of merchant's secret Id
     * @param void
     * @return int
     */
    public function getSecretMerchantIdMaxLen()
    {
        return $this->secretMerchantIdMaxLen;
    }

// - - - - - - - - Secret Merchant Id Min Len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of merchant's secret Id
     * @var int
     */
    private $secretMerchantIdMinLen = 0;

    /**
     * Set minimum length of merchant's secret Id
     * @param int
     * @return void
     */
    protected function setSecretMerchantIdMinLen($int)
    {
        $this->secretMerchantIdMinLen = $int;
    }

    /**
     * Get minimum length of merchant's secret Id
     * @param void
     * @return int
     */
    public function getSecretMerchantIdMinLen()
    {
        return $this->secretMerchantIdMinLen;
    }

// - - - - - - - - Public Merchant Id Max Len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of merchant's public Id
     * @var int
     */
    private $publicMerchantIdMaxLen = 0;

    /**
     * Set maximum length of merchant's public Id
     * @param int
     * @return void
     */
    protected function setPublicMerchantIdMaxLen($int)
    {
        $this->publicMerchantIdMaxLen = $int;
    }

    /**
     * Get maximum length of merchant's public Id
     * @param void
     * @return int
     */
    public function getPublicMerchantIdMaxLen()
    {
        return $this->publicMerchantIdMaxLen;
    }

// - - - - - - - - Public Merchant Id Min Len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of merchant's public Id
     * @var int
     */
    private $publicMerchantIdMinLen = 0;

    /**
     * Set minimum length of merchant's public Id
     * @param int
     * @return void
     */
    protected function setPublicMerchantIdMinLen($int)
    {
        $this->publicMerchantIdMinLen = $int;
    }

    /**
     * Get minimum length of merchant's public Id
     * @param void
     * @return int
     */
    public function getPublicMerchantIdMinLen()
    {
        return $this->publicMerchantIdMinLen;
    }

// - - - - - - - - Order Id Max Len  - - - - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of order Id
     * @var int
     */
    private $orderIdMaxLen = 0;

    /**
     * Set maximum length of order Id
     * @param int
     * @return void
     */
    protected function setOrderIdMaxLen($int)
    {
        $this->orderIdMaxLen = $int;
    }

    /**
     * Get maximum length of order Id
     * @param void
     * @return int
     */
    public function getOrderIdMaxLen()
    {
        return $this->orderIdMaxLen;
    }

// - - - - - - - - Order Total Max Len - - - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of order total
     * @var int
     */
    private $orderTotalMaxLen = 0;

    /**
     * Set maximum length of order total
     * @param int
     * @return void
     */
    protected function setOrderTotalMaxLen($int)
    {
        $this->orderTotalMaxLen = $int;
    }

    /**
     * Get maximum length of order total
     * @param void
     * @return int
     */
    public function getOrderTotalMaxLen()
    {
        return $this->orderTotalMaxLen;
    }

// - - - - - - - - Order Total Max Val - - - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum value of order total
     * @var float
     */
    private $orderTotalMaxVal = 0.0;

    /**
     * Set maximum value of order total
     * @param float
     * @return void
     */
    protected function setOrderTotalMaxVal($float)
    {
        $this->orderTotalMaxVal = $float;
    }

    /**
     * Get maximum value of order total
     * @param void
     * @return float
     */
    public function getOrderTotalMaxVal()
    {
        return $this->orderTotalMaxVal;
    }

// - - - - - - - - Order Total Min Val - - - - - - - - - - - - - - - - - - - - -

    /**
     * Minimum value of order total
     * @var float
     */
    private $orderTotalMinVal = 0.0;

    /**
     * Set minimum value of order total
     * @param float
     * @return void
     */
    protected function setOrderTotalMinVal($float)
    {
        $this->orderTotalMinVal = $float;
    }

    /**
     * Get minimum value of order total
     * @param void
     * @return float
     */
    public function getOrderTotalMinVal()
    {
        return $this->orderTotalMinVal;
    }

// - - - - - - - - Days To Delivery Max Value - - - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum value of days to delivery
     * @var int
     */
    private $daysToDeliveryMaxVal = 99;

    /**
     * Set maximum value of days to delivery
     * @param int
     * @return void
     */
    protected function setDaysToDeliveryMaxVal($int)
    {
        $this->daysToDeliveryMaxVal = $int;
    }

    /**
     * Get maximum value of days to delivery
     * @param void
     * @return int
     */
    public function getDaysToDeliveryMaxVal()
    {
        return $this->daysToDeliveryMaxVal;
    }

// - - - - - - - - Days To Delivery Min Value - - - - - - - - - - - - - - - - - - - - -

    /**
     * Minimum value of days to delivery
     * @var int
     */
    private $daysToDeliveryMinVal = 0;

    /**
     * Set minimum value of days to delivery
     * @param int
     * @return void
     */
    protected function setDaysToDeliveryMinVal($int)
    {
        $this->daysToDeliveryMinVal = $int;
    }

    /**
     * Get minimum value of days to delivery
     * @param void
     * @return int
     */
    public function getDaysToDeliveryMinVal()
    {
        return $this->daysToDeliveryMinVal;
    }

// - - - - - - - - Return URL Max Len - - - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of return URL
     * @var int
     */
    private $urlMaxLen = 0;

    /**
     * Set maximum length of return URL
     * @param int
     * @return void
     */
    protected function setUrlMaxLen($int)
    {
        $this->urlMaxLen = $int;
    }

    /**
     * Get maximum length of return URL
     * @param void
     * @return int
     */
    public function getUrlMaxLen()
    {
        return $this->urlMaxLen;
    }

// - - - - - - - - Valid Locales - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * Valid locales
     * Array key is code for the PGW, array value is just a comment.
     * @var array
     */
    private $validLocales = array();

    /**
     * Set valid locales
     * @param array
     * @return void
     */
    protected function setValidLocales($array)
    {
        $this->validLocales = $array;
    }

    /**
     * Get valid locales
     * @param void
     * @return array
     */
    public function getValidLocales()
    {
        return $this->validLocales;
    }

// - - - - - - - - Order desc max len - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum combined lenght of order description
     * @var int
     */
    private $descMaxLen = 0;

    /**
     * Set maximum combined length of order descripiton
     * @param int
     * @return void
     */
    protected function setDescMaxLen($int)
    {
        $this->descMaxLen = $int;
    }

    /**
     * Get maximum combined length of order descripiton
     * @param void
     * @return int
     */
    public function getDescMaxLen()
    {
        return $this->descMaxLen;
    }

// - - - - - - - - Order desc max line len - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of order description line
     * @var int
     */
    private $descLineMaxLen = 0;

    /**
     * Set maximum length of order descripiton line
     * @param int
     * @return void
     */
    protected function setDescLineMaxLen($int)
    {
        $this->descLineMaxLen = $int;
    }

    /**
     * Get maximum length of order descripiton line
     * @param void
     * @return int
     */
    public function getDescLineMaxLen()
    {
        return $this->descLineMaxLen;
    }

// - - - - - - - - Order desc max line num - - - - - - - - - - - - - - - - - - -

    /**
     * Maximum number of order description lines
     * @var int
     */
    private $descLineMaxNum = 0;

    /**
     * Set maximum number of order descripiton lines
     * @param int
     * @return void
     */
    protected function setDescLineMaxNum($int)
    {
        $this->descLineMaxNum = $int;
    }

    /**
     * Get maximum number of order descripiton lines
     * @param void
     * @return int
     */
    public function getDescLineMaxNum()
    {
        return $this->descLineMaxNum;
    }

// - - - - - - - - Valid Statues - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * Valid statues
     * @var array
     */
    private $validStatues = array();

    /**
     * Set valid statues
     * @param array
     * @return void
     */
    protected function setValidStatues($array)
    {
        $this->validStatues = $array;
    }

    /**
     * Get valid statues
     * @param void
     * @return array
     */
    public function getValidStatues()
    {
        return $this->validStatues;
    }

// - - - - - - - - Obligatory Responce GET parameters  - - - - - - - - - - - - -

    /**
     * Obligatory responce GET parameters
     * @var array
     */
    private $obligatoryResponseGetParams = array();

    /**
     * Set obligatory Response GET parameters
     * @param array
     * @return void
     */
    protected function setObligatoryResponseGetParams($array)
    {
        $this->obligatoryResponseGetParams = $array;
    }

    /**
     * Get obligatory Response GET parameters
     * @param void
     * @return array
     */
    public function getObligatoryResponseGetParams()
    {
        return $this->obligatoryResponseGetParams;
    }

// - - - - - - - - Key version exact len - - - - - - - - - - - - - - - - - - -

    /**
     * Exact lenght of key version
     * @var int
     */
    private $keyVersionExactLen = 0;

    /**
     * Set length of key version
     * @param int
     * @return void
     */
    protected function setKeyVersionExactLen($int)
    {
        $this->keyVersionExactLen = $int;
    }

    /**
     * Get length of key version
     * @param void
     * @return int
     */
    public function getKeyVersionExactLen()
    {
        return $this->keyVersionExactLen;
    }

// - - - - - - - - Bank Account Max Len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of bank account
     * @var int
     */
    private $bankAccountMaxLen = 0;

    /**
     * Set maximum length of bank account
     * @param int
     * @return void
     */
    protected function setBankAccountMaxLen($int)
    {
        $this->bankAccountMaxLen = $int;
    }

    /**
     * Get maximum length of bank account
     * @param void
     * @return int
     */
    public function getBankAccountMaxLen()
    {
        return $this->bankAccountMaxLen;
    }

// - - - - - - - - Bank Account Min Len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of bank account
     * @var int
     */
    private $bankAccountMinLen = 0;

    /**
     * Set minimum length of bank account
     * @param int
     * @return void
     */
    protected function setBankAccountMinLen($int)
    {
        $this->bankAccountMinLen = $int;
    }

    /**
     * Get minimum length of bank account
     * @param void
     * @return int
     */
    public function getBankAccountMinLen()
    {
        return $this->bankAccountMinLen;
    }

// - - - - - - - - Merchant name max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of merchant name
     * @var int
     */
    private $merchantNameMaxLen = 0;

    /**
     * Set maximum length of merchant name
     * @param int
     * @return void
     */
    protected function setMerchantNameMaxLen($int)
    {
        $this->merchantNameMaxLen = $int;
    }

    /**
     * Get maximum length of merchant name
     * @param void
     * @return int
     */
    public function getMerchantNameMaxLen()
    {
        return $this->merchantNameMaxLen;
    }

// - - - - - - - - Merchant name min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of merchant name
     * @var int
     */
    private $merchantNameMinLen = 0;

    /**
     * Set minimum length of merchant name
     * @param int
     * @return void
     */
    protected function setMerchantNameMinLen($int)
    {
        $this->merchantNameMinLen = $int;
    }

    /**
     * Get minimum length of merchant name
     * @param void
     * @return int
     */
    public function getMerchantNameMinLen()
    {
        return $this->merchantNameMinLen;
    }

// - - - - - - - - Web API version  - - - - - - - - - - - - - - - - -

    /**
     * WAPI version
     * @var string
     */
    private $wapiVersion = '';

    /**
     * Set WAPI version
     * @param string
     * @return void
     */
    protected function setWapiVersion($string)
    {
        $this->wapiVersion = $string;
    }

    /**
     * Get WAPI version
     * @param void
     * @return string
     */
    public function getWapiVersion()
    {
        return $this->wapiVersion;
    }

// - - - - - - - - Receiver Web API version  - - - - - - - - - - - - - - - - -

    /**
     * Receiver WAPI version
     * @var string
     */
    private $receiverWapiVersion = '';

    /**
     * Set Receiver WAPI version
     * @param string
     * @return void
     */
    protected function setReceiverWapiVersion($string)
    {
        $this->receiverWapiVersion = $string;
    }

    /**
     * Get receiver WAPI version
     * @param void
     * @return string
     */
    public function getReceiverWapiVersion()
    {
        return $this->receiverWapiVersion;
    }

// - - - - - - - - Transaction Id max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of transaction Id
     * @var int
     */
    private $transactionIdMaxLen = 0;

    /**
     * Set maximum length of transaction Id
     * @param int
     * @return void
     */
    protected function setTransactionIdMaxLen($int)
    {
        $this->transactionIdMaxLen = $int;
    }

    /**
     * Get maximum length of transaction Id
     * @param void
     * @return int
     */
    public function getTransactionIdMaxLen()
    {
        return $this->transactionIdMaxLen;
    }

// - - - - - - - - Transaction Id min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of transaction Id
     * @var int
     */
    private $transactionIdMinLen = 0;

    /**
     * Set minimum length of transaction Id
     * @param int
     * @return void
     */
    protected function setTransactionIdMinLen($int)
    {
        $this->transactionIdMinLen = $int;
    }

    /**
     * Get minimum length of transaction Id
     * @param void
     * @return int
     */
    public function getTransactionIdMinLen()
    {
        return $this->transactionIdMinLen;
    }

// - - - - - - - - Bank reference max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of bank reference number
     * @var int
     */
    private $bankRefMaxLen = 0;

    /**
     * Set maximum length of bank reference number
     * @param int
     * @return void
     */
    protected function setBankRefMaxLen($int)
    {
        $this->bankRefMaxLen = $int;
    }

    /**
     * Get maximum length of bank reference number
     * @param void
     * @return int
     */
    public function getBankRefMaxLen()
    {
        return $this->bankRefMaxLen;
    }

// - - - - - - - - Bank reference min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of bank reference number
     * @var int
     */
    private $bankRefMinLen = 0;

    /**
     * Set minimum length of bank reference number
     * @param int
     * @return void
     */
    protected function setBankRefMinLen($int)
    {
        $this->bankRefMinLen = $int;
    }

    /**
     * Get minimum length of bank reference number
     * @param void
     * @return int
     */
    public function getBankRefMinLen()
    {
        return $this->bankRefMinLen;
    }

// - - - - - - - - Billing name max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of billing name
     * @var int
     */
    private $billingNameMaxLen = 0;

    /**
     * Set maximum length of billing name
     * @param int
     * @return void
     */
    protected function setBillingNameMaxLen($int)
    {
        $this->billingNameMaxLen = $int;
    }

    /**
     * Get maximum length of billing name
     * @param void
     * @return int
     */
    public function getBillingNameMaxLen()
    {
        return $this->billingNameMaxLen;
    }

// - - - - - - - - Billing name min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of billing name
     * @var int
     */
    private $billingNameMinLen = 0;

    /**
     * Set minimum length of billing name
     * @param int
     * @return void
     */
    protected function setBillingNameMinLen($int)
    {
        $this->billingNameMinLen = $int;
    }

    /**
     * Get minmum length of billing name
     * @param void
     * @return int
     */
    public function getBillingNameMinLen()
    {
        return $this->billingNameMinLen;
    }

// - - - - - - - - Billing address max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of billing address
     * @var int
     */
    private $billingAddressMaxLen = 0;

    /**
     * Set maximum length of billing address
     * @param int
     * @return void
     */
    protected function setBillingAddressMaxLen($int)
    {
        $this->billingAddressMaxLen = $int;
    }

    /**
     * Get maximum length of billing address
     * @param void
     * @return int
     */
    public function getBillingAddressMaxLen()
    {
        return $this->billingAddressMaxLen;
    }

// - - - - - - - - Billing address min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of billing address
     * @var int
     */
    private $billingAddressMinLen = 0;

    /**
     * Set minimum length of billing address
     * @param int
     * @return void
     */
    protected function setBillingAddressMinLen($int)
    {
        $this->billingAddressMinLen = $int;
    }

    /**
     * Get minimum length of billing address
     * @param void
     * @return int
     */
    public function getBillingAddressMinLen()
    {
        return $this->billingAddressMinLen;
    }

// - - - - - - - - Billing zip max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of billing zip
     * @var int
     */
    private $billingZipMaxLen = 0;

    /**
     * Set maximum length of billing zip
     * @param int
     * @return void
     */
    protected function setBillingZipMaxLen($int)
    {
        $this->billingZipMaxLen = $int;
    }

    /**
     * Get maximum length of billing zip
     * @param void
     * @return int
     */
    public function getBillingZipMaxLen()
    {
        return $this->billingZipMaxLen;
    }

// - - - - - - - - Billing zip min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of billing zip
     * @var int
     */
    private $billingZipMinLen = 0;

    /**
     * Set minimum length of billing zip
     * @param int
     * @return void
     */
    protected function setBillingZipMinLen($int)
    {
        $this->billingZipMinLen = $int;
    }

    /**
     * Get minimum length of billing zip
     * @param void
     * @return int
     */
    public function getBillingZipMinLen()
    {
        return $this->billingZipMinLen;
    }

// - - - - - - - - Billing phone max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of billing phone
     * @var int
     */
    private $billingPhoneMaxLen = 0;

    /**
     * Set maximum length of billing phone
     * @param int
     * @return void
     */
    protected function setBillingPhoneMaxLen($int)
    {
        $this->billingPhoneMaxLen = $int;
    }

    /**
     * Get maximum length of billing phone
     * @param void
     * @return int
     */
    public function getBillingPhoneMaxLen()
    {
        return $this->billingPhoneMaxLen;
    }

// - - - - - - - - Billing phone min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of billing phone
     * @var int
     */
    private $billingPhoneMinLen = 0;

    /**
     * Set minimum length of billing phone
     * @param int
     * @return void
     */
    protected function setBillingPhoneMinLen($int)
    {
        $this->billingPhoneMinLen = $int;
    }

    /**
     * Get minimum length of billing phone
     * @param void
     * @return int
     */
    public function getBillingPhoneMinLen()
    {
        return $this->billingPhoneMinLen;
    }

// - - - - - - - - Billing phone max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of billing email
     * @var int
     */
    private $billingEmailMaxLen = 0;

    /**
     * Set maximum length of billing email
     * @param int
     * @return void
     */
    protected function setBillingEmailMaxLen($int)
    {
        $this->billingEmailMaxLen = $int;
    }

    /**
     * Get maximum length of billing email
     * @param void
     * @return int
     */
    public function getBillingEmailMaxLen()
    {
        return $this->billingEmailMaxLen;
    }

// - - - - - - - - Shipping name max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of shipping name
     * @var int
     */
    private $shippingNameMaxLen = 0;

    /**
     * Set maximum length of shipping name
     * @param int
     * @return void
     */
    protected function setShippingNameMaxLen($int)
    {
        $this->shippingNameMaxLen = $int;
    }

    /**
     * Get maximum length of shipping name
     * @param void
     * @return int
     */
    public function getShippingNameMaxLen()
    {
        return $this->shippingNameMaxLen;
    }

// - - - - - - - - Shipping name min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of shipping name
     * @var int
     */
    private $shippingNameMinLen = 0;

    /**
     * Set minimum length of shipping name
     * @param int
     * @return void
     */
    protected function setShippingNameMinLen($int)
    {
        $this->shippingNameMinLen = $int;
    }

    /**
     * Get minmum length of shipping name
     * @param void
     * @return int
     */
    public function getShippingNameMinLen()
    {
        return $this->shippingNameMinLen;
    }

// - - - - - - - - Shipping address max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of shipping address
     * @var int
     */
    private $shippingAddressMaxLen = 0;

    /**
     * Set maximum length of shipping address
     * @param int
     * @return void
     */
    protected function setShippingAddressMaxLen($int)
    {
        $this->shippingAddressMaxLen = $int;
    }

    /**
     * Get maximum length of shipping address
     * @param void
     * @return int
     */
    public function getShippingAddressMaxLen()
    {
        return $this->shippingAddressMaxLen;
    }

// - - - - - - - - Shipping address min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of shipping address
     * @var int
     */
    private $shippingAddressMinLen = 0;

    /**
     * Set minimum length of shipping address
     * @param int
     * @return void
     */
    protected function setShippingAddressMinLen($int)
    {
        $this->shippingAddressMinLen = $int;
    }

    /**
     * Get minimum length of shipping address
     * @param void
     * @return int
     */
    public function getShippingAddressMinLen()
    {
        return $this->shippingAddressMinLen;
    }

// - - - - - - - - Shipping zip max len  - - - - - - - - - - - - - - - - -

    /**
     * Maximum lenght of shipping zip
     * @var int
     */
    private $shippingZipMaxLen = 0;

    /**
     * Set maximum length of shipping zip
     * @param int
     * @return void
     */
    protected function setShippingZipMaxLen($int)
    {
        $this->shippingZipMaxLen = $int;
    }

    /**
     * Get maximum length of shipping zip
     * @param void
     * @return int
     */
    public function getShippingZipMaxLen()
    {
        return $this->shippingZipMaxLen;
    }

// - - - - - - - - Shipping zip min len  - - - - - - - - - - - - - - - - -

    /**
     * Minimum lenght of shipping zip
     * @var int
     */
    private $shippingZipMinLen = 0;

    /**
     * Set minimum length of shipping zip
     * @param int
     * @return void
     */
    protected function setShippingZipMinLen($int)
    {
        $this->shippingZipMinLen = $int;
    }

    /**
     * Get minimum length of shipping zip
     * @param void
     * @return int
     */
    public function getShippingZipMinLen()
    {
        return $this->shippingZipMinLen;
    }


// - - - - - - - - Multibyte string functions class  - - - - - - - - - - - - - - - - -

    /**
     * Multibyte string functions class
     */
    protected $MbStr;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    /**
     * Constructor
     * @param void
     * @return void
     */
    public function __construct()
    {
        $this->MbStr = Aivomatic_Fipn_Factory::getMbString();
    }

    /**
     * Validates locale
     * @param   string|int
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($locale)
     */
    public function validateLocale($locale)
    {
        if (self::isEmptyButNotZero($locale)) {
            throw new Aivomatic_Fipn_Exception('$locale must not be empty');
        }
        if (!array_key_exists($locale, self::getValidLocales())) {
            $msg = '$locale ('.$locale.') must be one of these:';
            foreach (self::getValidLocales() as $key => $value) {
                $msg .= strval($key).' ('.$value.'), ';
            }
            throw new Aivomatic_Fipn_Exception($msg);
        }
        return strval($locale);
    }

    /**
     * Validate charset
     * @param string Charset
     * @param array Valid charsets
     * @throws Aivomatic_Fipn_Exception If not valid.
     * @return string Charset
     */
    public static function validateCharset($charset, $validCharsets)
    {
        if (!in_array(strtoupper($charset), $validCharsets)) {
            throw new Aivomatic_Fipn_Exception(
                'Charset ('.$charset.') is not valid');
        }
        return $charset;
    }

    /**
     * Validate input charset
     * @param string Input charset
     * @throws Aivomatic_Fipn_Exception If not valid.
     * @return string Charset
     */
    public static function validateInputCharset($charset)
    {
        if (!Aivomatic_Fipn_Factory::getMbString()->isValidInputCharset($charset)) {
            throw new Aivomatic_Fipn_Exception(
                'Charset ('.$charset.') is not a valid input charset');
        }
        return $charset;
    }

    /**
     * Validate output charset
     * @param string Output charset
     * @throws Aivomatic_Fipn_Exception If not valid.
     * @return string Charset
     */
    public static function validateOutputCharset($charset)
    {
        $validCharsets = array('ISO-8859-1', 'ISO-8859-15', 'UTF-8');
        return self::validateCharset($charset, $validCharsets);
    }

    /**
     * Basic validation of success, cancel or error URL
     * Validates return URL (just the length, nothing more fancy).
     * @param   string  $url
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $url
     */
    public function validateReturnUrl($url)
    {
        if (empty($url)) {
            throw new Aivomatic_Fipn_Exception('$url must not be empty');
        }
        try {
            self::validateString($url,
                                 self::getUrlMaxLen(),
                                 10);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $url failed:'.$e->getRawMessage());
        }
        if (strstr($url, '#')) {
            throw new Aivomatic_Fipn_Exception(
                'URL ('.$url.') must not contain "#" character (ie. no anchor)');
        }
        $lastChar = substr($url, -1);
        if ($lastChar != '?' AND $lastChar != '&') {
            throw new Aivomatic_Fipn_Exception(
                'Last character of URL ('.$url.') must be "?" or "&"');
        }
        require_once dirname(__FILE__).'/Pear/Validate.php';
        $options = array('allowed_schemes' => array('http', 'https'));
        $pearValidate = new Fipn_Pear_Validate();
        if (!$pearValidate->uri(substr($url, 0, -1), $options)) {
            throw new Aivomatic_Fipn_Exception(
                'URL ('.$url.') must be a valid url (ignoring the last character)');
        }
        return $url;
    }

    /**
     * Validates an email address
     * @param   string  $email
     * @param   int     $maxLen Maximum legnth
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $email
     */
    public static function validateEmailAddress($email, $maxLen = 0)
    {
        if (empty($email)) {
            throw new Aivomatic_Fipn_Exception('$email must not be empty');
        }
        if (strlen($email) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                '$email ('.$email.') must not be longer than '.$maxLen.
                'characters (is '.strlen($email).')');
        }
        require_once dirname(__FILE__).'/Pear/Validate.php';
        $pearValidate = new Fipn_Pear_Validate();
        if (!$pearValidate->email($email, array('check_domain' => false,
                                                'use_rfc822' => true))) {
            throw new Aivomatic_Fipn_Exception(
                '$email ('.$email.') must be a valid email address');
        }
        return $email;
    }

    /**
     * Validates 'lined' order description
     * 'Lined' means length and number of lines is limited.
     * @param   string|int $orderId
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderDesc)
     */
    public function validateLinedOrderDesc($orderDesc)
    {
        if (!self::isIntOrString($orderDesc)) {
            throw new Aivomatic_Fipn_Exception(
                '$orderDescLines must be an integer or a string (is '.
                gettype($orderDesc).')');
        }
        $orderDescRows = explode(chr(10), $orderDesc);
        if (count($orderDescRows) > self::getDescLineMaxNum()) {
            throw new Aivomatic_Fipn_Exception('$orderDescLines must not contain '.
                'more than '.self::getDescLineMaxNum().' lines '.
                '(contains '.count($orderDescRows).')');
        }
        $charsTotal = 0;
        foreach ($orderDescRows as $line => $string) {
            $chars = self::numOfChars(rtrim($string, chr(10)));
            if ($chars > self::getDescLineMaxLen()) {
                throw new Aivomatic_Fipn_Exception('Line '.$line.
                    ' ('.$string.') of $orderDescLines is longer than '.
                    self::getDescLineMaxLen().' (is '.$chars.
                    ') characters');
            }
            $charsTotal = $charsTotal+$chars;
        }
        if ($charsTotal > self::getDescMaxLen()) {
            throw new Aivomatic_Fipn_Exception('$orderDescLines must not contain '.
                'more than '.self::getDescMaxLen().' characters combined '.
                '(contains '.$charsTotal.')');
        }
        return strval($orderDesc);
    }

    /**
     * Validates 'monolithic' order description
     * 'Monolithic' means line length and number is unlimited
     * @param   string|int $orderDesc
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderDesc)
     */
    protected function validateMonolithicOrderDesc($orderDesc)
    {
        if (!self::isIntOrString($orderDesc)) {
            throw new Aivomatic_Fipn_Exception(
                '$orderDescMono must be an integer or a string (is '.
                gettype($orderDesc).')');
        }
        if (!self::isEmptyButNotZero($orderDesc)) {
            if (!self::orderDescBelowMaxLen($orderDesc)) {
                throw new Aivomatic_Fipn_Exception(
                    '$orderDescMono ('.$orderDesc.') must be shorter than '.
                    self::getDescMaxLen().
                    ' (is '.self::numOfChars($orderDesc).') characters');
            }
        }
        return strval($orderDesc);
    }

    /**
     * Checks that order description is below maximun length
     * @param   string $orderDesc   Order description
     * @param   int    $maxLen      Maximum leght
     * @return  bool True if below, false if above
     */
    protected function orderDescBelowMaxLen($orderDesc)
    {
        if (self::numOfChars($orderDesc) > self::getDescMaxLen()) {
            return false;
        }
        return true;
    }

    /**
     * Basic validation of currency code
     * @param   string $currency 'eur' or 'euro' - case insensitive
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $currency
     */
    public static function validateCurrency($currency)
    {
        if ((strtoupper($currency) != 'EUR') AND
            (strtoupper($currency) != 'EURO')) {
            throw new Aivomatic_Fipn_Exception(
                '$currency ('.$currency.') must be EUR or EURO'
                .' (case insensitive)');
        }
        return $currency;
    }

    /**
     * Basic validation of total amount
     * @param   float $orderTotal Strictly float!
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string Aivomatic_Fipn::formatOrderTotal($orderTotal)
     */
    public function validateOrderTotal($orderTotal)
    {
        if (self::isEmptyButNotZero($orderTotal)) {
            throw new Aivomatic_Fipn_Exception('$orderTotal must not be empty');
        }
        if (!is_float($orderTotal)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $orderTotal ('.$orderTotal
                .') must be a float (is '.gettype($orderTotal).')');
        }
        if ($orderTotal > self::getOrderTotalMaxVal() OR
            $orderTotal < self::getOrderTotalMinVal()) {
            throw new Aivomatic_Fipn_Exception(
                '$orderTotal ('.$orderTotal.') must be smaller than '.
                self::getOrderTotalMaxVal().' and bigger than '.
                self::getOrderTotalMinVal());
        }
        $str = Aivomatic_Fipn::formatOrderTotal($orderTotal);
        if (strlen($str) > self::getOrderTotalMaxLen()) {
            throw new Aivomatic_Fipn_Exception(
                'String presentation of $orderTotal ('.$str.') must be shorter'.
                ' than '.self::getOrderTotalMaxLen().
                ' (is '.strlen($str).') characters');
        }
        return $str;
    }

    /**
     * Basic validation of days to delivery
     * @param   int $daysToDelivery
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($daysToDelivery)
     */
    public function validateDaysToDelivery($daysToDelivery)
    {
        if (self::isEmptyButNotZero($daysToDelivery)) {
            throw new Aivomatic_Fipn_Exception('$daysToDelivery must not be empty');
        }
        if (!is_int($daysToDelivery)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $daysToDelivery ('.$daysToDelivery
                .') must be an integer (is '.gettype($daysToDelivery).')');
        }
        if ($daysToDelivery < 0) {
            throw new Aivomatic_Fipn_Exception(
                '$daysToDelivery ('.$daysToDelivery
                .') must be zero or positive');
        }
        if ($daysToDelivery < self::getDaysToDeliveryMinVal()) {
            throw new Aivomatic_Fipn_Exception(
                '$daysToDelivery ('.$daysToDelivery
                .') must be greater than '.self::getDaysToDeliveryMinVal());
        }
        if ($daysToDelivery > self::getDaysToDeliveryMaxVal()) {
            throw new Aivomatic_Fipn_Exception(
                '$daysToDelivery ('.$daysToDelivery
                .') must be smaller than '.self::getDaysToDeliveryMaxVal());
        }
        return strval($daysToDelivery);
    }

    /**
     * Basic validation of percentage
     * @param   float|int $percentage
     * @param   float $maxValue
     * @param   float $minValue
     * @param   int   $maxLen Maximum string length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($percentage) Dot as decimal, precision two digits
     */
    public static function validatePercentage($percentage,
                                              $maxValue = 100.00,
                                              $minValue = 0.00,
                                              $maxLen = 6)
    {
        if (self::isEmptyButNotZero($percentage)) {
            throw new Aivomatic_Fipn_Exception('$percentage must not be empty');
        }
        if (!is_float($percentage) AND !is_int($percentage)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $percentage ('.$percentage
                .') must be a float or integer');
        }
        if ($percentage > $maxValue) {
            throw new Aivomatic_Fipn_Exception(
                '$percentage ('.$percentage.') must be smaller than '.$maxValue);
        }
        if ($percentage < $minValue) {
            throw new Aivomatic_Fipn_Exception(
                '$percentage ('.$percentage.') must be bigger than '.$minValue);
        }
        $str = Aivomatic_Fipn::formatOrderTotal($percentage);
        if (strlen($str) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'String presentation of $percentage ('.$str.') must be shorter'
                .' than '.$maxLen.' (is '.strlen($str).') characters');
        }
        return $str;
    }

    /**
     * Basic validation of order Id
     * @param   string|int $orderId
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderId)
     */
    public function validateOrderId($orderId)
    {
        try {
            self::validateFbaBankRefBase($orderId);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                '$orderId can not be used as base for bank reference number: '.
                $e->getRawMessage());
        }
        $orderId = strval($orderId);
        try {
            self::validateString(
                $orderId,
                self::getOrderIdMaxLen(),
                1);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $orderId failed: '.$e->getRawMessage());
        }
        return $orderId;
    }

    /**
     * Basic validation of customer Id
     * @param   string|int $customerId Alphanumeric
     * @param   int        $maxLen Maximum length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($customerId)
     */
    protected static function validateCustomerId($customerId, $maxLen = 0)
    {
        if (!self::isIntOrString($customerId)) {
            throw new Aivomatic_Fipn_Exception(
                'customerId must be an integer or a string (is '.
                gettype($customerId).')');
        }
        $customerId = strval($customerId);
        try {
            self::validateString($customerId, $maxLen, 1);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $customerId failed:'.$e->getRawMessage());
        }
        if (!ctype_alnum($customerId)) {
            throw new Aivomatic_Fipn_Exception(
                '$customerId ('.$customerId.') must be alphanumeric (a-z, A-Z, 0-9)');
        }
        return $customerId;
    }

    /**
     * Basic validation of merchant's public Id
     * @param   string|int $publicMerchantId Not empty.
     * @throws  Aivomatic_Fipn_Exception If $publicMerchantId isn't valid
     * @return  string     strval($publicMerchantId)
     */
    public function validatePublicMerchantId($publicMerchantId)
    {
        if (!self::isIntOrString($publicMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'publicMerchantId must be an integer or a string (is '.
                gettype($publicMerchantId).')');
        }
        $publicMerchantId = strval($publicMerchantId);
        try {
            self::validateString($publicMerchantId,
                                 self::getPublicMerchantIdMaxLen(),
                                 self::getPublicMerchantIdMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $publicMerchantId failed:'.$e->getRawMessage());
        }
        if (!ctype_alnum($publicMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'publicMerchantId ('.$publicMerchantId.') '.
                'must be alphanumeric (a-z, A-Z, 0-9)');
        }
        return $publicMerchantId;
    }

    /**
     * Basic validation of merchant's secret Id
     * @param   string|int $secretMerchantId Merchant's secret Id code.
     * @throws  Aivomatic_Fipn_Exception If $secretMerchantId isn't valid
     * @return  string     strval($secretMerchantId)
     */
    public function validateSecretMerchantId($secretMerchantId)
    {
        if (!self::isIntOrString($secretMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'secretMerchantId must be an integer or a string (is '.
                gettype($secretMerchantId).')');
        }
        $secretMerchantId = strval($secretMerchantId);
        try {
            self::validateString($secretMerchantId,
                                 self::getSecretMerchantIdMaxLen(),
                                 self::getSecretMerchantIdMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $secretMerchantId failed:'.$e->getRawMessage());
        }
        if (!ctype_alnum($secretMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'secretMerchantId ('.$secretMerchantId.') '.
                'must be alphanumeric (a-z, A-Z, 0-9)');
        }
        return $secretMerchantId;
    }

    /**
     * Basic validation of a string
     * @param   string $string
     * @param   int    $maxLen Maximum length
     * @param   int    $minLen Minimum length
     * @throws  Aivomatic_Fipn_Exception If not valid
     * @return  string $string
     */
    protected static function validateString(
        $string,
        $maxLen = 0,
        $minLen = 0)
    {
        if (!is_int($maxLen)) {
            throw new Aivomatic_Fipn_Exception(
                '$maxLen ('.$maxLen.') must be an integer '.
                '(is '.gettype($maxLen).')');
        }
        if (!is_int($minLen)) {
            throw new Aivomatic_Fipn_Exception(
                '$minLen ('.$minLen.') must be an integer '.
                '(is '.gettype($minLen).')');
        }
        if (!is_string($string)) {
            throw new Aivomatic_Fipn_Exception(
                '$string ('.$string.') must be a string '.
                '(is '.gettype($string).')');
        }
        if ($minLen != 0) {
            if (self::isEmptyButNotZero($string)) {
                throw new Aivomatic_Fipn_Exception(
                    '$string must not be empty');
            }
        }
        $strLen = self::numOfChars($string);
        if ($strLen > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                '$string ('.$string.') must not be longer than '.$maxLen
                .' (is '.$strLen.') characters');
        }
        if ($strLen < $minLen) {
            throw new Aivomatic_Fipn_Exception(
                '$string ('.$string.') must not be shorter than '.$minLen
                .' (is '.$strLen.') characters');
        }
        return $string;
    }

    /**
     * Basic validation of response request's GET-parameters array
     * @param   array $responseGetParams Requests GET-parameters as an array.
     * @param   array $obligatoryResponseGetParams
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  array $responseGetParams
     */
    public function validateResponseGetParams($type, $responseGetParams)
    {
        self::validateResponseStatus($type);
        if (!is_array($responseGetParams)) {
            throw new Aivomatic_Fipn_Exception(
                '$responseGetParams must be an array');
        }
        $params = self::getObligatoryResponseGetParams();
        if (!empty($params[$type]) AND
             empty($responseGetParams)) {
            throw new Aivomatic_Fipn_Exception(
                '$responseGetParams must not be empty');
        }
        foreach ($params[$type] as $oblParam) {
            if(!array_key_exists($oblParam, $responseGetParams)) {
                throw new Aivomatic_Fipn_Exception(
                    'Response request GET-parameter '.$oblParam.' does not exist');
            }
        }
        return $responseGetParams;
    }

    /**
     * Validate status parameter
     * Valid statues are 'success' and 'cancel'. Some payment gateways may have
     * 'error' and 'notify' statues. These are directly relater to so called
     * successUrl, cancelUrl, errorUrl and notifyUrl.
     * @param string $status  Status parameter.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $status in all lower case
     */
    public function validateResponseStatus($status)
    {

        if (!in_array(strtolower($status), self::getValidStatues())) {
            throw new Aivomatic_Fipn_Exception(
                'Status ('.$status.') is not valid');
        }
        return strtolower($status);
    }

    /**
     * Basic validation of responseMac
     * @param   string|int $responseMac Alphanumeric Uppercase
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $responseMac
     */
    public static function validateResponseMac($responseMac)
    {
        if (!preg_match('/^[A-F0-9]{32}$/', $responseMac)) {
            throw new Aivomatic_Fipn_Exception(
                'responseMac ('.$responseMac.') must be an uppercase md5 hash');
        }
        return $responseMac;
    }

    /**
     * Validates that parameter is an integer or a string
     *
     * Note that very big integers might be unexpectedly casted to type double.
     * The limit depends on your system.
     * @param   mixed $param
     * @return  bool true if int or string, false if some other type
     */
    public static function isIntOrString($param)
    {
        if (is_int($param) OR is_string($param)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if parameter is empty but not zero (0 or '0')
     *
     * PHP's buildin function empty() returns true even  if param is (int)0 or
     * (string)'0'. This method returns true if param is '' (an empty string)
     * NULL, FALSE or array() (an empty array). It returns false if param is 0
     * (integer 0), '0' (string 0) or 0.0 (float 0).
     * @param   mixed $param
     * @return  bool true if really empty, false if not empty including 0 and '0'
     */
    public static function isEmptyButNotZero($param)
    {
        if ((empty($param) OR $param === true) AND
             $param !== 0 AND
             $param !== '0' AND
             $param !== 0.0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if a string contains tags
     * @param string $string
     * @return bool True if string contains tags, else false
     */
    public static function containsTags($string)
    {
        if ($string != strip_tags($string)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Basic validation of finnish bank reference number's base part
     * @param   string|int $base Base part for finnish bank reference number
     * All digits. Length according to each PWG (max and min bank ref len-1) ->
     * standard 3-19.
     * @throws  Aivomatic_Fipn_Exception If $base isn't valid
     * @return  string     strval($base)
     */
    public function validateFbaBankRefBase($base)
    {
        if (!self::isIntOrString($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base must be an integer or a string (is '.
                gettype($base).')');
        }
        if (self::isEmptyButNotZero($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base must not be empty');
        }
        $base = strval($base);
        if (!ctype_digit($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be all digits (0-9)');
        }
        $maxLen = self::getBankRefMaxLen()-1;
        if (strlen($base) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be '.$maxLen.' characters or shorter'
                .' (is '.strlen($base).')');
        }
        $minLen = self::getBankRefMinLen()-1;
        if (strlen($base) < $minLen) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be '.$minLen.' characters or longer'
                .' (is '.strlen($base).')');
        }
        return $base;
    }

    /**
     * Basic validation of unformatted finnish bank reference number
     * @param   string|int $ref Unformatted finnish bank reference number
     * All digits. Length according to each PWG, standard 4-20.
     * @throws  Aivomatic_Fipn_Exception If $ref isn't valid
     * @return  string     strval($ref)
     */
    public function validateUnformattedFbaBankRef($ref)
    {
        if (!self::isIntOrString($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must be an integer or a string (is '.
                gettype($ref).')');
        }
        if (self::isEmptyButNotZero($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must not be empty');
        }
        $ref = strval($ref);
        if (!ctype_digit($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be all digits (0-9)');
        }
        if (strlen($ref) > self::getBankRefMaxLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be '.self::getBankRefMaxLen().
                ' characters or shorter (is '.strlen($ref).')');
        }
        if (strlen($ref) < self::getBankRefMinLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be '.self::getBankRefMinLen().
                ' characters or longer (is '.strlen($ref).')');
        }
        if (strlen(ltrim($ref, '0')) < self::getBankRefMinLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be '.self::getBankRefMinLen().
                ' characters or longer even when '.
                'padding with zeros is removed from the beginning ('.
                ltrim($ref, '0').')(length '.strlen(ltrim($ref, '0')).')');
        }

        /**
         * Require Fipn base class (../Fipn.php)
         */
        require_once dirname(__FILE__).'.php';
        $base = substr($ref, 0, -1);
        $checkSum = substr($ref, -1);
        if ($checkSum != Aivomatic_Fipn::calculateFbaBankRefCheckSum($base)) {
            throw new Aivomatic_Fipn_Exception(
                'Checksum ('.$checkSum.') of the bank reference number ('.$ref
                .') is not valid');
        }
        return $ref;
    }

    /**
     * Basic validation of unformatted finnish bank reference number
     * @param   string|int $ref Unformatted finnish bank reference number
     * All digits. Length according to each PWG, standard 4-20.
     * @throws  Aivomatic_Fipn_Exception If $ref isn't valid
     * @return  string     strval($ref)
     */
    public function validatePaddedFbaBankRef($ref)
    {
        if (!self::isIntOrString($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must be an integer or a string (is '.
                gettype($ref).')');
        }
        if (self::isEmptyButNotZero($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must not be empty');
        }
        $ref = strval($ref);
        if (!ctype_digit($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be all digits (0-9)');
        }
        if (strlen($ref) > self::getBankRefMaxLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be '.self::getBankRefMaxLen().
                ' characters or shorter (is '.strlen($ref).')');
        }
        if (strlen($ref) < self::getBankRefMinLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be '.self::getBankRefMinLen().
                ' characters or longer (is '.strlen($ref).')');
        }

        /**
         * Require Fipn base class (../Fipn.php)
         */
        require_once dirname(__FILE__).'.php';
        $base = substr($ref, 0, -1);
        $checkSum = substr($ref, -1);
        if ($checkSum != Aivomatic_Fipn::calculateFbaBankRefCheckSum($base)) {
            throw new Aivomatic_Fipn_Exception(
                'Checksum ('.$checkSum.') of the bank reference number ('.$ref
                .') is not valid');
        }
        return $ref;
    }

    /**
     * Validate transaction Id
     * @param string|int $transactionId
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($transactionId)
     */
    public function validateTransactionId($transactionId)
    {
        if (!self::isIntOrString($transactionId)) {
            throw new Aivomatic_Fipn_Exception(
                '$transactionId must be an integer or a string (is '.
                gettype($transactionId).')');
        }
        if (self::isEmptyButNotZero($transactionId)) {
            throw new Aivomatic_Fipn_Exception(
                '$transactionId must not be empty');
        }
        $transactionId = strval($transactionId);
        if (!ctype_alnum($transactionId)) {
            throw new Aivomatic_Fipn_Exception(
                'transactionId ('.$transactionId.')'
                .' must be alphanumeric (a-z, A-Z, 0-9)');
        }
        try {
            self::validateString($transactionId,
                                self::getTransactionIdMaxLen(),
                                self::getTransactionIdMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $transactionId failed:'.$e->getRawMessage());
        }
        return $transactionId;
    }

    /**
     * Compares two floats and checks they match within precision
     * @param float $sum1
     * @param float $sum2
     * @param float $precision Maximum diffrence between sum1 and sum2
     * @return bool True if match within precision
     */
     public static function compareSums($sum1, $sum2, $precision = 0)
     {
        return abs($sum1-$sum2) <= $precision;
     }

    /**
     * Validates ISO 3361-1 alpha-2 two letter country code
     *
     * Case insensitive.
     * @link http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
     * @param   string $code Case insensitive two letter country code.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $code
     */
    public static function validateISO3166_1Alpha2($code)
    {
        $codes = array(
            'af', 'ax', 'al', 'dz', 'as', 'ad', 'ao', 'ai', 'aq', 'ag', 'ar',
            'am', 'aw', 'au', 'at', 'az', 'bs', 'bh', 'bd', 'bb', 'by', 'be',
            'bz', 'bj', 'bm', 'bt', 'bo', 'ba', 'bw', 'bv', 'br', 'io', 'bn',
            'bg', 'bf', 'bi', 'kh', 'cm', 'ca', 'cv', 'ky', 'cf', 'td', 'cl',
            'cn', 'cx', 'cc', 'co', 'km', 'cg', 'cd', 'ck', 'cr', 'ci', 'hr',
            'cu', 'cy', 'cz', 'dk', 'dj', 'dm', 'do', 'ec', 'eg', 'sv', 'gq',
            'er', 'ee', 'et', 'fk', 'fo', 'fj', 'fi', 'fr', 'gf', 'pf', 'tf',
            'ga', 'gm', 'ge', 'de', 'gh', 'gi', 'gr', 'gl', 'gd', 'gp', 'gu',
            'gt', 'gg', 'gn', 'gw', 'gy', 'ht', 'hm', 'va', 'hn', 'hk', 'hu',
            'is', 'in', 'id', 'ir', 'iq', 'ie', 'im', 'il', 'it', 'jm', 'jp',
            'je', 'jo', 'kz', 'ke', 'ki', 'kp', 'kr', 'kw', 'kg', 'la', 'lv',
            'lb', 'ls', 'lr', 'ly', 'li', 'lt', 'lu', 'mo', 'mk', 'mg', 'mw',
            'my', 'mv', 'ml', 'mt', 'mh', 'mq', 'mr', 'mu', 'yt', 'mx', 'fm',
            'md', 'mc', 'mn', 'me', 'ms', 'ma', 'mz', 'mm', 'na', 'nr', 'np',
            'nl', 'an', 'nc', 'nz', 'ni', 'ne', 'ng', 'nu', 'nf', 'mp', 'no',
            'om', 'pk', 'pw', 'ps', 'pa', 'pg', 'py', 'pe', 'ph', 'pn', 'pl',
            'pt', 'pr', 'qa', 're', 'ro', 'ru', 'rw', 'bl', 'sh', 'kn', 'lc',
            'mf', 'pm', 'vc', 'ws', 'sm', 'st', 'sa', 'sn', 'rs', 'sc', 'sl',
            'sg', 'sk', 'si', 'sb', 'so', 'za', 'gs', 'es', 'lk', 'sd', 'sr',
            'sj', 'sz', 'se', 'ch', 'sy', 'tw', 'tj', 'tz', 'th', 'tl', 'tg',
            'tk', 'to', 'tt', 'tn', 'tr', 'tm', 'tc', 'tv', 'ug', 'ua', 'ae',
            'gb', 'us', 'um', 'uy', 'uz', 'vu', 've', 'vn', 'vg', 'vi', 'wf',
            'eh', 'ye', 'zm', 'zw');
        if (!in_array(strtolower(strval($code)), $codes)) {
            throw new Aivomatic_Fipn_Exception(
                '$code ('.$code.') is not a valid ISO 3361-1 alpha-2 two '.
                'letter country code');
        }
        return $code;
    }

    /**
     * Validates ISO 3361-1 alpha-3 three letter country code
     *
     * Case insensitive.
     * @link http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
     * @param   string $code Case insensitive three letter country code.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $code
     */
    public static function validateISO3166_1Alpha3($code)
    {
        $codes = array(
            'afg', 'alb', 'dza', 'asm', 'and', 'ago', 'aia', 'ata', 'atg',
            'arg', 'arm', 'abw', 'aus', 'aut', 'aze', 'bhs', 'bhr', 'bgd',
            'brb', 'blr', 'bel', 'blz', 'ben', 'bmu', 'btn', 'bol', 'bih',
            'bwa', 'bvt', 'bra', 'iot', 'vgb', 'brn', 'bgr', 'bfa', 'bdi',
            'khm', 'cmr', 'can', 'cpv', 'cym', 'caf', 'tcd', 'chl', 'chn',
            'cxr', 'cck', 'col', 'com', 'cod', 'cog', 'cok', 'cri', 'civ',
            'cub', 'cyp', 'cze', 'dnk', 'dji', 'dma', 'dom', 'ecu', 'egy',
            'slv', 'gnq', 'eri', 'est', 'eth', 'fro', 'flk', 'fji', 'fin',
            'fra', 'guf', 'pyf', 'atf', 'gab', 'gmb', 'geo', 'deu', 'gha',
            'gib', 'grc', 'grl', 'grd', 'glp', 'gum', 'gtm', 'gin', 'gnb',
            'guy', 'hti', 'hmd', 'vat', 'hnd', 'hkg', 'hrv', 'hun', 'isl',
            'ind', 'idn', 'irn', 'irq', 'irl', 'isr', 'ita', 'jam', 'jpn',
            'jor', 'kaz', 'ken', 'kir', 'prk', 'kor', 'kwt', 'kgz', 'lao',
            'lva', 'lbn', 'lso', 'lbr', 'lby', 'lie', 'ltu', 'lux', 'mac',
            'mkd', 'mdg', 'mwi', 'mys', 'mdv', 'mli', 'mlt', 'mhl', 'mtq',
            'mrt', 'mus', 'myt', 'mex', 'fsm', 'mda', 'mco', 'mng', 'msr',
            'mar', 'moz', 'mmr', 'nam', 'nru', 'npl', 'ant', 'nld', 'ncl',
            'nzl', 'nic', 'ner', 'nga', 'niu', 'nfk', 'mnp', 'nor', 'omn',
            'pak', 'plw', 'pse', 'pan', 'png', 'pry', 'per', 'phl', 'pcn',
            'pol', 'prt', 'pri', 'qat', 'reu', 'rou', 'rus', 'rwa', 'shn',
            'kna', 'lca', 'spm', 'vct', 'wsm', 'smr', 'stp', 'sau', 'sen',
            'scg', 'syc', 'sle', 'sgp', 'svk', 'svn', 'slb', 'som', 'zaf',
            'sgs', 'esp', 'lka', 'sdn', 'sur', 'sjm', 'swz', 'swe', 'che',
            'syr', 'twn', 'tjk', 'tza', 'tha', 'tls', 'tgo', 'tkl', 'ton',
            'tto', 'tun', 'tur', 'tkm', 'tca', 'tuv', 'vir', 'uga', 'ukr',
            'are', 'gbr', 'umi', 'usa', 'ury', 'uzb', 'vut', 'ven', 'vnm',
            'wlf', 'esh', 'yem', 'zmb', 'zwe');
        if (!in_array(strtolower(strval($code)), $codes)) {
            throw new Aivomatic_Fipn_Exception(
                '$code ('.$code.') is not a valid ISO 3361-1 alpha-3 three '.
                'letter country code');
        }
        return $code;
    }

    /**
     * Validates unix timestamp
     *
     * Max: 2147483647 = 2038-01-19 03:14:07 +0000
     * Min in Fipn: 0 = 1970-01-01 00:00:00 +0000
     * Theoretical min:-2147483648 = 1901-12-13 20:45:52 +0000
     *
     * @param string|int $time 0 - 2147483647
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return string strval($time)
     */
    public static function validateUnixTimestamp($time) {
        $strtime = strval($time);
        if (!ctype_digit($strtime)) {
            throw new Aivomatic_Fipn_Exception(
                '$time ('.$strtime.')'
                .' must be all numbers (0-9)');
        }
        if (strlen($strtime) > 10) {
        throw new Aivomatic_Fipn_Exception(
            '$time ('.$strtime.') must be shorter than 10'
            .' (is '.strlen($strtime).') characters');
        }
        $inttime = intval($time);
        if ($inttime > 2147483647) {
            throw new Aivomatic_Fipn_Exception(
            '$time ('.$strtime.') must be smaller than 2147483647');
        }
        if ($inttime < 0) {
            throw new Aivomatic_Fipn_Exception(
            '$time ('.$strtime.') must be bigger than zero');
        }
        return $strtime;
    }

    /**
     * Validates boolean (true or false)
     *
     * @param bool $bool
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return bool $bool
     */
    public static function validateBoolean($bool) {
        if (!is_bool($bool)) {
            throw new Aivomatic_Fipn_Exception(
                '$bool ('.strval($bool).')'
                .' must be boolean (is '.gettype($bool).')');
        }
        return $bool;
    }

    /**
     * Validates MAC key version
     * @param   string $keyVersion
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $keyVersion
     */
    public function validateKeyVersion($keyVersion)
    {
        if (self::isEmptyButNotZero($keyVersion)) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion must not be empty');
        }
        if (!is_string($keyVersion)) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion must be a string (is '.
                gettype($keyVersion).')');
        }
        if (!ctype_digit(strval($keyVersion))) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion ('.$keyVersion.') must be all digits (0-9)');
        }
        if (strlen($keyVersion) != self::getKeyVersionExactLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion ('.$keyVersion.') must be exactly'
                .self::getKeyVersionExactLen().' '.
                '(is '.strlen($keyVersion).' characters');
        }
        return $keyVersion;
    }

    /**
     * Basic validation of bank account number
     * @param   string $bankAccount
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $bankAccount
     */
    public function validateBankAccount($bankAccount)
    {
        if (self::isEmptyButNotZero($bankAccount)) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount must not be empty');
        }
        if (!is_string($bankAccount)) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount must be a string (is '.
                gettype($bankAccount).')');
        }
        if (self::numOfChars($bankAccount) > self::getBankAccountMaxLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount ('.$bankAccount.') must be shorter than'
                .self::getBankAccountMaxLen().' '.
                '(is '.self::numOfChars($bankAccount).' characters');
        }
        if (self::numOfChars($bankAccount) < self::getBankAccountMinLen()) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount ('.$bankAccount.') must be longer than'
                .self::getBankAccountMinLen().' '.
                '(is '.self::numOfChars($bankAccount).' characters');
        }
        return $bankAccount;
    }

    /**
     * Validates merchant name
     * @param   string $merchantName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $merchantName
     */
    public function validateMerchantName($merchantName)
    {
        if (self::isEmptyButNotZero($merchantName)) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName must not be empty');
        }
        if (!is_string($merchantName)) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName must be a string (is '.
                gettype($merchantName).')');
        }
        try {
            self::validateString($merchantName,
                                 self::getMerchantNameMaxLen(),
                                 self::getMerchantNameMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $merchantName failed:'.$e->getRawMessage());
        }
        if (self::containsTags($merchantName)) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName ('.$merchantName.') must not contain tags');
        }
        return $merchantName;
    }

    /**
     * Validates billing name
     * @param   string $billingName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $billingName
     */
    public function validateBillingName($billingName)
    {
        if (self::isEmptyButNotZero(trim($billingName))) {
            throw new Aivomatic_Fipn_Exception(
                '$billingName must not be empty');
        }
        if (!is_string($billingName)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingName must be a string (is '.
                gettype($billingName).')');
        }
        try {
            self::validateString($billingName,
                                 self::getBillingNameMaxLen(),
                                 self::getBillingNameMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $billingName failed:'.$e->getRawMessage());
        }
        if (self::containsTags($billingName)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingName ('.$billingName.') must not contain tags');
        }
        return $billingName;
    }

    /**
     * Validates billing address
     * @param   string $billingAddress
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $billingAddress
     */
    public function validateBillingAddress($billingAddress)
    {
        if (self::isEmptyButNotZero(trim($billingAddress))) {
            throw new Aivomatic_Fipn_Exception(
                '$billingAddress must not be empty');
        }
        if (!is_string($billingAddress)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingAddress must be a string (is '.
                gettype($billingAddress).')');
        }
        try {
            self::validateString($billingAddress,
                                 self::getBillingAddressMaxLen(),
                                 self::getBillingAddressMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $billingAddress failed:'.$e->getRawMessage());
        }
        if (self::containsTags($billingAddress)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingAddress ('.$billingAddress.') must not contain tags');
        }
        return $billingAddress;
    }

    /**
     * Validates billing zip
     * @param   string|int $billingZip
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $billingZip
     */
    public function validateBillingZip($billingZip)
    {
        if (self::isEmptyButNotZero($billingZip)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingZip must not be empty');
        }
        try {
            return self::validateZip($billingZip,
                                 self::getBillingZipMaxLen(),
                                 self::getBillingZipMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $billingZip failed:'.$e->getRawMessage());
        }
    }

    /**
     * Simple validation of a Finnish ZIP code (three to five digits)
     * @param   string|int $zip
     * @param   int     $maxLen Maximum legnth
     * @param   int     $minLen Minimum legnth
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($zip)
     */
    public static function validateZip($zip, $maxLen = 5, $minLen = 3)
    {
        if (!self::isIntOrString($zip)) {
            throw new Aivomatic_Fipn_Exception(
                '$zip ('.$zip.') must be an integer or a string (is '.
                gettype($zip).')');
        }
        $zip = strval($zip);
        try {
            self::validateString($zip, $maxLen, $minLen);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $zip failed!:'.$e->getRawMessage());
        }
        if (!ctype_digit($zip)) {
            throw new Aivomatic_Fipn_Exception(
                '$zip ('.$zip.') must be all digits (0-9)');
        }
        return $zip;
    }

    /**
     * Validates billing phone
     * @param   string|int $billingPhone
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string stval($billingPhone)
     */
    public function validateBillingPhone($billingPhone)
    {
        if (self::isEmptyButNotZero($billingPhone)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingPhone must not be empty');
        }
        if (!self::isIntOrString($billingPhone)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingPhone ('.$billingPhone.') must be an integer or a '.
                'string (is '.gettype($billingPhone).')');
        }
        $billingPhone = strval($billingPhone);
        try {
            self::validateString($billingPhone,
                                 self::getBillingPhoneMaxLen(),
                                 self::getBillingPhoneMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $billingPhone failed:'.$e->getRawMessage());
        }
        if (self::containsTags($billingPhone)) {
            throw new Aivomatic_Fipn_Exception(
                '$billingPhone ('.$billingPhone.') must not contain tags');
        }
        return $billingPhone;
    }

    /**
     * Validates billing email
     * @param   string $billingEmail
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $billingEmail
     */
    public function validateBillingEmail($billingEmail)
    {
        try {
            self::validateEmailAddress($billingEmail,
                                 self::getBillingEmailMaxLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $billingEmail failed:'.$e->getRawMessage());
        }
        return $billingEmail;
    }

    /**
     * Validates shipping name
     * @param   string $shippingName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $shippingName
     */
    public function validateShippingName($shippingName)
    {
        if (self::isEmptyButNotZero(trim($shippingName))) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingName must not be empty');
        }
        if (!is_string($shippingName)) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingName must be a string (is '.
                gettype($shippingName).')');
        }
        try {
            self::validateString($shippingName,
                                 self::getShippingNameMaxLen(),
                                 self::getShippingNameMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $shippingName failed:'.$e->getRawMessage());
        }
        if (self::containsTags($shippingName)) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingName ('.$shippingName.') must not contain tags');
        }
        return $shippingName;
    }

    /**
     * Validates shipping address
     * @param   string $shippingAddress
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $shippingAddress
     */
    public function validateShippingAddress($shippingAddress)
    {
        if (self::isEmptyButNotZero(trim($shippingAddress))) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingAddress must not be empty');
        }
        if (!is_string($shippingAddress)) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingAddress must be a string (is '.
                gettype($shippingAddress).')');
        }
        try {
            self::validateString($shippingAddress,
                                 self::getShippingAddressMaxLen(),
                                 self::getShippingAddressMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $shippingAddress failed:'.$e->getRawMessage());
        }
        if (self::containsTags($shippingAddress)) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingAddress ('.$shippingAddress.') must not contain tags');
        }
        return $shippingAddress;
    }

    /**
     * Validates shipping zip
     * @param   string|int $shippingZip
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $shippingZip
     */
    public function validateShippingZip($shippingZip)
    {
        if (self::isEmptyButNotZero($shippingZip)) {
            throw new Aivomatic_Fipn_Exception(
                '$shippingZip must not be empty');
        }
        try {
            return self::validateZip($shippingZip,
                                 self::getShippingZipMaxLen(),
                                 self::getShippingZipMinLen());
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $shippingZip failed:'.$e->getRawMessage());
        }
    }

    /**
     * Validates Web API version identifier (number)
     * @param   string  $wapi
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string  $wapi
     */
    public function validateWapiVersion($wapi)
    {
        if ($wapi !== self::getWapiVersion()) {
            throw new Aivomatic_Fipn_Exception(
                '$wapi ('.$wapi.') should be equal to "'.
                self::getWapiVersion().'"');
        }
        return $wapi;
    }

    /**
     * Validates return Web API version identifier (number) sent by gateway
     * @param   string  $wapi
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string  $wapi
     */
    public function validateReturnWapiVersion($returnWapi)
    {
        if ($returnWapi !== self::getReceiverWapiVersion()) {
            throw new Aivomatic_Fipn_Exception(
                '$returnWapi ('.$returnWapi.') should be equal to "'.
                self::getReceiverWapiVersion().'"');
        }
        return $returnWapi;
    }

    /**
     * Returns length of the string (number of characters)
     *
     * Tries to tackle multibyte strings correctly even on servers where
     * PHP multibyte functions are not installed.
     *
     * @param string $str
     * @return int Number of characters in string
     */
    public static function numOfChars($str) {
        return Aivomatic_Fipn_Factory::getMbString()->strlenMb($str);
    }

    /**
     * Validates order description
     * @param   string|int $orderDesc
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderDesc)
     */
    public function validateOrderDesc($orderDesc)
    {
        throw new Exception(__CLASS__.'::'.__METHOD__.' should be overriden.');
    }

    /**
     * Textual presentation of object's properties
     * @param   void
     * @return  string
     */
    public function __toString()
    {
        $str =
        'secretMerchantIdMaxLen '.strval($this->getSecretMerchantIdMaxLen())."\n".
        'secretMerchantIdMinLen '.strval($this->getSecretMerchantIdMinLen())."\n".
        'publicMerchantIdMaxLen '.strval($this->getPublicMerchantIdMaxLen())."\n".
        'publicMerchantIdMinLen '.strval($this->getPublicMerchantIdMinLen())."\n".
        'orderIdMaxLen          '.strval($this->getOrderIdMaxLen())."\n".
        'orderTotalMaxLen       '.strval($this->getOrderTotalMaxLen())."\n".
        'orderTotalMaxVal       '.strval($this->getOrderTotalMaxVal())."\n".
        'orderTotalMinVal       '.strval($this->getOrderTotalMinVal())."\n".
        'daysToDeliveryMaxVal   '.strval($this->getDaysToDeliveryMaxVal())."\n".
        'daysToDeliveryMinVal   '.strval($this->getDaysToDeliveryMinVal())."\n".
        'urlMaxLen              '.strval($this->getUrlMaxLen())."\n".
        'descMaxLen             '.strval($this->getDescMaxLen())."\n".
        'descLineMaxLen         '.strval($this->getDescLineMaxLen())."\n".
        'descLineMaxNum         '.strval($this->getDescLineMaxNum())."\n".
        'keyVersionExactLen     '.strval($this->getKeyVersionExactLen())."\n".
        'bankAccountMaxLen      '.strval($this->getBankAccountMaxLen())."\n".
        'bankAccountMinLen      '.strval($this->getBankAccountMinLen())."\n".
        'merchantNameMaxLen     '.strval($this->getMerchantNameMaxLen())."\n".
        'merchantNameMinLen     '.strval($this->getMerchantNameMinLen())."\n".
        'transactionMaxLen      '.strval($this->getTransactionIdMaxLen())."\n".
        'transactionMinLen      '.strval($this->getTransactionIdMinLen())."\n".
        'bankRefMaxLen          '.strval($this->getBankRefMaxLen())."\n".
        'bankRefMinLen          '.strval($this->getBankRefMinLen())."\n".
        'billingNameMaxLen      '.strval($this->getBillingNameMaxLen())."\n".
        'billingNameMinLen      '.strval($this->getBillingNameMinLen())."\n".
        'billingAddressMaxLen   '.strval($this->getBillingAddressMaxLen())."\n".
        'billingAddressMinLen   '.strval($this->getBillingAddressMinLen())."\n".
        'billingZipMaxLen       '.strval($this->getBillingZipMaxLen())."\n".
        'billingZipMinLen       '.strval($this->getBillingZipMinLen())."\n".
        'billingPhoneMaxLen     '.strval($this->getBillingPhoneMaxLen())."\n".
        'billingPhoneMinLen     '.strval($this->getBillingPhoneMinLen())."\n".
        'billingEmailMaxLen     '.strval($this->getBillingEmailMaxLen())."\n".
        'shippingNameMaxLen     '.strval($this->getShippingNameMaxLen())."\n".
        'shippingNameMinLen     '.strval($this->getShippingNameMinLen())."\n".
        'shippingAddressMaxLen  '.strval($this->getShippingAddressMaxLen())."\n".
        'shippingAddressMinLen  '.strval($this->getShippingAddressMinLen())."\n".
        'shippingZipMaxLen      '.strval($this->getShippingZipMaxLen())."\n".
        'shippingZipMinLen      '.strval($this->getShippingZipMinLen())."\n".
        'wapiVersion            '.$this->getWapiVersion()."\n".
        'receiverWapiVersion    '.$this->getReceiverWapiVersion()."\n".
        'mb string extension    '.$this->MbStr->getExtensionName()."\n";

        $validLocales = self::getValidLocales();
        if (!empty($validLocales)) {
            $str .= 'validLocales          ';
            foreach (self::getValidLocales() as $key => $value) {
                $str .= ' '.$key.' ('.$value.')';
            }
        }
        $str .= "\n";

        $validStatues = self::getValidStatues();
        if (!empty($validStatues)) {
            $str .= 'validStatues          ';
            foreach (self::getValidStatues() as $status) {
                $str .= ' '.$status;
            }
        }
        $str .= "\n";

        $params = self::getObligatoryResponseGetParams();
        if (!empty($params)) {
            $str .= 'obligatoryResponseGetParams:'."\n";
            foreach (self::getObligatoryResponseGetParams() as $status => $params) {
                $str .= '    '.$status."\n";
                foreach ($params as $param) {
                    $str .= '        '.$param."\n";
                }
            }
        }
        return $str;
    }
}
?>