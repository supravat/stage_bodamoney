<?php
/**
 * Aivomatic_Fipn_Sender
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Sender.php 1534 2011-08-23 10:29:35Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Sender.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Fipn base class (../Fipn.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Aivomatic Fipn Sender base class
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Sender extends Aivomatic_Fipn
{
    /**
     * Name of the payment gateway
     * @var string
     */
    private $pgwName = '';

    /**
     * Set the name of the payment gateway
     * @param string
     * @return void
     */
    protected function setPgwName($string)
    {
        $this->pgwName = $string;
    }

    /**
     * Return the name of the payment gateway
     * @param void
     * @return string
     */
    public function getPgwName()
    {
        return $this->pgwName;
    }

    /**
     * URL for HTML-form POST-method
     * Tämän voisi muuttaa privateksi.
     * @var string
     */
    protected $postUrl = '';

    /**
     * Set target URL for HTML-form POST-method
     * @param void
     * @return string URL
     */
    protected function setPostUrl($string)
    {
        $this->postUrl = $string;
    }

    /**
     * Return target URL for HTML-form POST-method
     * @param void
     * @return string URL
     */
    public function getPostUrl()
    {
        return $this->postUrl;
    }

    /**
     * Character set accepted by the payment gateway
     * @var string
     */
    protected $outputCharset = '';

    /**
     * Set payment gateway character set
     * @param string Charset
     * @return void
     */
    protected function setOutputCharset($string)
    {
        $this->outputCharset = $string;
    }

    /**
     * Returns payment gateway character set
     * @param void
     * @return string Charset
     */
    public function getOutputCharset()
    {
        return $this->outputCharset;
    }

    /**
     * Character set of the server
     * @var string
     */
    protected $inputCharset = '';

    /**
     * Set server character set
     * @param string Charset
     * @return void
     */
    protected function setInputCharset($string)
    {
        $this->inputCharset = $string;
    }

    /**
     * Returns server character set
     * @param void
     * @return string Charset
     */
    public function getInputCharset()
    {
        return $this->inputCharset;
    }

    /**
     * Locale (or language code)
     *
     * Some payment gateways offer swedish and/or english in addition to
     * finnish as their UI language.
     *
     * @see Aivomatic_Fipn_Validator::validateLocale()
     * @var string
     */
    protected $locale = '';

    /**
     * Success return URL
     *
     * Customer is directed from payment gateway back to this URL if payment was
     * accepted and successful.
     *
     * @see getUrlGetParams()
     * @see Aivomatic_Fipn_Validator::validateReturnUrl()
     * @var string
     */
    protected $successUrl = '';

    /**
     * Cancel return URL
     *
     * Customer is directed from payment gateway back to this URL if she/he
     * cancels the payment.
     *
     * @see getUrlGetParams()
     * @see Aivomatic_Fipn_Validator::validateReturnUrl()
     * @var string
     */
    protected $cancelUrl = '';

    /**
     * Error return URL
     *
     * Customer is directed from payment gateway back to this URL if an error
     * occures during the payment.
     *
     * @see getUrlGetParams()
     * @see Aivomatic_Fipn_Validator::validateReturnUrl()
     * @var string
     */
    protected $errorUrl = '';

    /**
     * Delay URL
     *
     * Customer returns to this URL if the payment process has been started but
     * if its outcome has to be waited, ie. we don't know the final result yet.
     * Netposti in the case of Suomen Verkkomaksut.
     *
     * @see getUrlGetParams()
     * @see Aivomatic_Fipn_Validator::validateReturnUrl()
     * @var string
     */
    protected $delayUrl = '';

    /**
     * Decline URL
     *
     * Customer returns to this URL if the payment processor declines the
     * payment because of bad credit etc. or if an error occured
     *
     * @see getUrlGetParams()
     * @see Aivomatic_Fipn_Validator::validateReturnUrl()
     * @var string
     */
    protected $declineUrl = '';

    /**
     * Bank account number of merchant
     *
     * @var string
     */
    protected $bankAccount = '';

    /**
     * Name of merchant
     *
     * @var string
     */
    protected $merchantName = '';

    /**
     * Days to delivery
     * @var integer
     */
    protected $daysToDelivery = 0;

    /**
     * Returns days to delivery
     * @param void
     * @return integer
     */
    public function getDaysToDelivery()
    {
        return $this->daysToDelivery;
    }

    /**
     * Billing name
     * @var string
     */
    protected $billingName = '';

    /**
     * Returns billing name
     * @param void
     * @return string
     */
    public function getBillingName()
    {
        return $this->billingName;
    }

    /**
     * Billing street address
     * @var string
     */
    protected $billingAddress = '';

    /**
     * Returns billing street address
     * @param void
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Billing Zip (postal code)
     * @var string
     */
    protected $billingZip = '';

    /**
     * Returns billing Zip (postal code)
     * @param void
     * @return string
     */
    public function getBillingZip()
    {
        return $this->billingZip;
    }

    /**
     * Billing city
     * @var string
     */
    protected $billingCity = '';

    /**
     * Returns billing city
     * @param void
     * @return string
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * Billing country
     * @var string
     */
    protected $billingCountry = '';

    /**
     * Returns billing country
     * @param void
     * @return string
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * Billing phone number
     * @var string
     */
    protected $billingPhone = '';

    /**
     * Returns billing phone
     * @param void
     * @return string
     */
    public function getBillingPhone()
    {
        return $this->billingPhone;
    }

    /**
     * Billing email
     * @var string
     */
    protected $billingEmail = '';

    /**
     * Returns billing email
     * @param void
     * @return string
     */
    public function getBillingEmail()
    {
        return $this->billingEmail;
    }

    /**
     * Order description
     *
     * Some payment gateways offer an order description field. Its usage varies.
     * Some gateways show it only to the merchant, some show it also for the
     * customer. Format varies too.
     * @var string|bool
     */
    protected $orderDesc = '';

    /**
     * String for which the MAC is calculated for
     *
     * @var string
     */
    protected $macBaseString = '';

    /**
     * Constructor
     * @param   void
     * @return  void
     */
    protected function __construct()
    {
        parent::__construct();
    }

    /**
     * Get success return URL
     * @uses $successUrl
     * @param  void
     * @return string successUrl
     */
    public function getSuccessUrl()
    {
        return $this->successUrl;
    }

    /**
     * Get cancel return URL
     * @uses $cancelUrl
     * @param  void
     * @return string cancelUrl
     */
    public function getCancelUrl()
    {
        return $this->cancelUrl;
    }

    /**
     * Get error return URL
     * @uses $errorUrl
     * @param  void
     * @return string errorUrl
     */
    public function getErrorUrl()
    {
        return $this->errorUrl;
    }

    /**
     * Get Delay URL
     * @uses $delayedUrl
     * @param  void
     * @return string delayedUrl
     */
    public function getDelayUrl()
    {
        return $this->delayUrl;
    }

    /**
     * Get Decline URL
     * @uses $declineUrl
     * @param  void
     * @return string declineUrl
     */
    public function getDeclineUrl()
    {
        return $this->declineUrl;
    }

    /**
     * Returns bank account number
     *
     * @param   void
     * @return  string $bankAccount
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * Returns merchant name
     *
     * @param   void
     * @return  string $merchantName
     */
    public function getMerchantName()
    {
        return $this->merchantName;
    }

    /**
     * Get locale
     * @uses $locale
     * @param   void
     * @return  string locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Returns the string for which the MAC is calculated for
     * @param void
     * @return string $this->macBaseString
     */
    public function getMacBaseString()
    {
        return $this->macBaseString;
    }

    /**
     * Add string to the end of order description
     *
     * Set the order description ie. line by line.
     *
     * @uses $orderDesc
     * @see strlen()
     * @see chr()
     * @param string $string The string to be added to the end of orderDesc
     * @param bool   $newLine If true, a 'new line character' chr(10) is added
     *                        after the added string.
     * @param int    $maxLen Maximum total length of the order description
     * @return bool True if added (description is below max.leght), false if not
     */
    protected function addToOrderDesc($string, $newLine = true, $maxLen = 0)
    {
        if ($newLine) {
            $string = chr(10).$string;
        }
        if (strlen($this->orderDesc)+strlen($string) > $maxLen) {
            return false;
        }
        $this->orderDesc = trim($this->orderDesc.$string);
        return true;
    }

    /**
     * Add string to the end of monolithic order description
     *
     * Set the order description ie. line by line.
     *
     * @param string $string The string to be added to the end of orderDesc
     * @param bool   $newLine If true, a 'new line character' chr(10) is added
     *                        after the added string.
     * @throws Aivomatic_Fipn_Exception If not valid.
     * @return bool True if added
     */
    protected function addToMonolithicOrderDesc($string, $newLine = true)
    {
        if ($newLine) {
            $string = chr(10).$string;
        }
        $orderDesc = trim($this->getOrderDesc().$string);
        try {
            $this->Validator->validateOrderDesc($orderDesc);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Can not add to order description: '.$e->getRawMessage());
        }
        $this->setOrderDesc($orderDesc);
        return true;
    }

    /**
     * Add string to the end of lined order description
     *
     * @param   string  $string String to be added
     * @param   bool    $newLine If true (default) a new line character is added
     *                           to the BEGINNING of string
     * @return  bool    True on success
     */
    public function addToLinedOrderDesc($string, $newLine = true)
    {
        $orderDesc = trim($this->getOrderDesc());
        if ($newLine AND !empty($orderDesc)) {
            $orderDesc = $orderDesc.chr(10);
        }
        $rows = explode(chr(10), $string);
        foreach ($rows as $row) {
            $splitedRows =
                str_split($row, $this->Validator->getDescLineMaxLen());
            foreach ($splitedRows as $splitRow) {
                $orderDesc = $orderDesc.$splitRow.chr(10);
            }
        }
        $orderDesc = trim($orderDesc);
        try {
            $this->Validator->validateOrderDesc($orderDesc);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Can not add to order description: '.$e->getRawMessage());
        }
        $this->setOrderDesc($orderDesc);
        return true;
    }

    /**
     * Formats amount
     * No thousands separator, comma as decimal point, two decimals
     * @param $amount string
     * @return string Formated $amount
     */
    public static function formatAmount_commaTwoDesimals($amount)
    {
        return number_format(floatval($amount), 2, ',', '');
    }

    /**
     * Converts string from server charset to payment gateway charset
     * @param string In server charset
     * @return string In payment gateway charset
     */
    protected function toOutputCharset($str)
    {
        return $this->MbStr->convertString(
            $this->getInputCharset(),
            $this->getOutputCharset(),
            $str);
    }

    /**
     * Get order description
     * @uses $orderDesc
     * @param   void
     * @return  string
     */
    public function getOrderDesc()
    {
        return $this->orderDesc;
    }

    /**
     * Sets order desciption
     * @param string $orderDesc
     * @return void
     */
    protected function setOrderDesc($orderDesc)
    {
        $this->orderDesc = $orderDesc;
    }

    /**
     * Makes and sets bank reference number
     * Formed according to Finnish bankers' association's standard exept no
     * splitting to five digit groups.
     * @param void
     * @return void
     */
    protected function makeBankReference()
    {
        $orderId = ltrim($this->getOrderId(), '0');
        $this->bankReference = $orderId.
            self::calculateFbaBankRefCheckSum($orderId);
    }

    /**
     * Returns hidded input fields for a HTML form
     * @param array $array key=fields name, value=fields value
     * @return string <INPUT TYPE="hidden" NAME="arrayKey" VALUE="arrayValue">\n
     */
    public static function getHtmlInputFields($array)
    {
        $str = '';
        foreach ($array as $name => $value) {
            $str .= '<input type="hidden" ';
            $str .= 'id="'.$name.'" name="'.$name.'" value="'.$value.'"/>'."\n";
        }
        return $str;
    }

    /**
     * Get GET-parameters for success, cancel or error URLs
     *
     * E.g. 'fipnstatus=success&fipnpgw=pgwName&fipnorderid=12345'
     * @param string $type success, cancel or error
     * @return string GET-parameters
     */
    protected function getUrlGetParams($type)
    {
        return 'fipnstatus='.$type.
               '&fipnpgw='.$this->getPgwName().
               '&fipnorderid='.$this->getOrderId();
    }

    /**
     * Merges and returns standard and PGW specific form fields
     *
     * @param array Payment gateway specific form fields
     * @return array Standard and special form fields merged to one array
     */
    public function mergeStandardFormFields($specialFields)
    {
        $standardFields = array(
            'fipnPgwName'       => $this->getPgwName(),
            'inputCharset'      => $this->getInputCharset(),
            'outputCharset'     => $this->getOutputCharset());
        return array_merge($standardFields, $specialFields);
    }

    /**
     * Textual presentation of object's properties
     *
     * Gathers and formats class properties to a string to be used by eg. echo
     * and print PHP-functions. Nice for debuging etc.
     * This method adds Sender-specific properties to the end of the string.
     * @param   void
     * @see     $locale, $successUrl, $cancelUrl, $errorUrl, $orderDesc
     * @uses    parent::__toString()
     * @return  string Class properties as formated string
     */
    public function __toString()
    {
        return
        parent::__toString().
        __METHOD__."::\n".
        'pgwName:               '.$this->getPgwName()."\n".
        'postUrl:               '.$this->getPostUrl()."\n".
        'locale:                '.$this->locale."\n".
        'successUrl:            '.$this->successUrl."\n".
        'cancelUrl:             '.$this->cancelUrl."\n".
        'errorUrl:              '.$this->errorUrl."\n".
        'delayUrl:              '.$this->delayUrl."\n".
        'declineUrl:            '.$this->declineUrl."\n".
        'charsetOutput:         '.$this->getOutputCharset()."\n".
        'charsetInput:          '.$this->getInputCharset()."\n".
        'bankAccount:           '.$this->getBankAccount()."\n".
        'merchantName:          '.$this->getMerchantName()."\n".
        'daysToDelivery:        '.$this->getDaysToDelivery()."\n".
        'orderDesc:'."\n".
        '-----------------------------------'."\n".
        $this->orderDesc."\n".
        '-----------------------------------'."\n";
    }
}
?>