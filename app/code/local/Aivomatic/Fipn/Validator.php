<?php
/**
 * Aivomatic_Fipn_Validator
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Validator.php 1276 2010-06-14 08:50:19Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Validator.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Factory class
 */
require_once 'Factory.php';

/**
 * Aivomatic Fipn Validator base class
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Validator
{

    protected static $allValidResponseStatues =
        array('success', 'cancel', 'error', 'delay', 'notify');

    /**
     * Basic validation of locale
     * @param   string|int $locale Not empty
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($locale)
     */
    protected static function validateLocale($locale)
    {
        if (self::isEmptyButNotZero($locale)) {
            throw new Aivomatic_Fipn_Exception('locale must not be empty');
        }
        return strval($locale);
    }

    /**
     * Basic validation of success, cancel or error URL
     * Validates return URL (just the length, nothing more fancy).
     * @param   string  $url
     * @param   int     $maxLen Maximum legnth
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $url
     */
    protected static function validateReturnUrl($url, $maxLen = 0)
    {
        if (empty($url)) {
            throw new Aivomatic_Fipn_Exception('URL must not be empty');
        }
        if (strlen($url) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'URL must be shorter than '.$maxLen.' (is '.strlen($url).')');
        }
        if (strstr($url, '#')) {
            throw new Aivomatic_Fipn_Exception(
                'URL ('.$url.') must not contain "#" character (ie. no anchor)');
        }
        $lastChar = substr($url, -1);
        if ($lastChar != '?' AND $lastChar != '&') {
            throw new Aivomatic_Fipn_Exception(
                'Last character of URL ('.$url.') must be "?" or "&"');
        }
        require_once dirname(__FILE__).'/Pear/Validate.php';
        $options = array('allowed_schemes' => array('http', 'https'));
        $pearValidate = new Fipn_Pear_Validate();
        if (!$pearValidate->uri(substr($url, 0, -1), $options)) {
            throw new Aivomatic_Fipn_Exception(
                'URL ('.$url.') must be a valid url (ignoring the last character)');
        }
        return $url;
    }

    /**
     * Validates an email address
     * @param   string  $email
     * @param   int     $maxLen Maximum legnth
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $email
     */
    public static function validateEmailAddress($email, $maxLen = 0)
    {
        if (empty($email)) {
            throw new Aivomatic_Fipn_Exception('$email must not be empty');
        }
        if (strlen($email) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                '$email ('.$email.') must not be longer than '.$maxLen.
                'characters (is '.strlen($email).')');
        }
        require_once dirname(__FILE__).'/Pear/Validate.php';
        $pearValidate = new Fipn_Pear_Validate();
        if (!$pearValidate->email($email, array('check_domain' => false,
                                                'use_rfc822' => true))) {
            throw new Aivomatic_Fipn_Exception(
                '$email ('.$email.') must be a valid email address');
        }
        return $email;
    }

    /**
     * Validates order description according to specs of Suomen Verkkomaksut
     * @param   string|int $orderDesc Max. len. 65000 chars.
     * @param   int    $maxLen Maximum length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderDesc)
     */
    protected static function validateOrderDesc($orderDesc, $maxLen = 0)
    {
        if (!self::isEmptyButNotZero($orderDesc)) {
            if (!self::orderDescBelowMaxLen($orderDesc, $maxLen)) {
                throw new Aivomatic_Fipn_Exception(
                    'orderDescFoo ('.$orderDesc.') must be shorter than '.$maxLen
                    .' (is '.self::numOfChars($orderDesc).') characters');
            }
        }
        return strval($orderDesc);
    }

    /**
     * Checks that order description is below maximun length
     * @param   string $orderDesc   Order description
     * @param   int    $maxLen      Maximum leght
     * @return  bool True if below, false if above
     */
    protected static function orderDescBelowMaxLen($orderDesc, $maxLen = 0)
    {
        if (self::numOfChars($orderDesc) > $maxLen ) {
            return false;
        }
        return true;
    }

    /**
     * Basic validation of currency code
     * @param   string $currency 'eur' or 'euro' - case insensitive
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $currency
     */
    protected static function validateCurrency($currency)
    {
        if ((strtoupper($currency) != 'EUR') AND
            (strtoupper($currency) != 'EURO'AND (strtoupper($currency) != 'UGX'))) {
            throw new Aivomatic_Fipn_Exception(
                '$currency ('.$currency.') must be EUR or EURO'
                .' (case insensitive)');
        }
        return $currency;
    }

    /**
     * Basic validation of total amount
     * @param   float|int $orderTotal
     * @param   int   $maxLen Maximum length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderTotal)
     */
    protected static function validateOrderTotal($orderTotal, $maxLen = 0)
    {
        if (self::isEmptyButNotZero($orderTotal)) {
            throw new Aivomatic_Fipn_Exception('$orderTotal must not be empty');
        }
        if (!is_float($orderTotal) AND !is_int($orderTotal)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $orderTotal ('.$orderTotal
                .') must be a float or integer');
        }
        $str = Aivomatic_Fipn::formatOrderTotal($orderTotal);
        if (strlen($str) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'String presentation of $orderTotal ('.$str.') must be shorter'
                .' than '.$maxLen.' (is '.strlen($str).') characters');
        }
        return $str;
    }

    /**
     * Basic validation of total amount
     * @param   float|int $number
     * @param   int   $maxLen Maximum length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($number)
     */
    protected static function validateNumber($number, $maxLen=0)
    {
        if (self::isEmptyButNotZero($number)) {
            throw new Aivomatic_Fipn_Exception('$number must not be empty');
        }
        if (!is_float($number) AND
            !is_int($number)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $number ('.$number.
                ') must be a float or integer (is '.gettype($number).')');
        }
        /*$str = Aivomatic_Fipn::formatOrderTotal($number);
        if (strlen($str) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'String presentation of $orderTotal ('.$str.') must be shorter'
                .' than '.$maxLen.' (is '.strlen($str).') characters');
        }*/
        if (strlen($number) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'String presentation of $orderTotal ('.$number.') must be shorter'
                .' than '.$maxLen.' (is '.strlen($number).') characters');
        }
        return strval($number);
    }

    /**
     * Basic validation of percentage
     * @param   float|int $percentage
     * @param   float $maxValue
     * @param   float $minValue
     * @param   int   $maxLen Maximum string length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($percentage) Dot as decimal, precision two digits
     */
    public static function validatePercentage($percentage,
                                                 $maxValue = 100.00,
                                                 $minValue = 0.00,
                                                 $maxLen = 6)
    {
        if (self::isEmptyButNotZero($percentage)) {
            throw new Aivomatic_Fipn_Exception('$percentage must not be empty');
        }
        if (!is_float($percentage) AND !is_int($percentage)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $percentage ('.$percentage
                .') must be a float or integer');
        }
        if ($percentage > $maxValue) {
            throw new Aivomatic_Fipn_Exception(
                '$percentage ('.$percentage.') must be smaller than '.$maxValue);
        }
        if ($percentage < $minValue) {
            throw new Aivomatic_Fipn_Exception(
                '$percentage ('.$percentage.') must be bigger than '.$minValue);
        }
        $str = Aivomatic_Fipn::formatOrderTotal($percentage);
        if (strlen($str) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'String presentation of $percentage ('.$str.') must be shorter'
                .' than '.$maxLen.' (is '.strlen($str).') characters');
        }
        return $str;
    }

    /**
     * Basic validation of order Id
     * @param   string|int $orderId
     * @param   int        $maxLen Maximum length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($orderId)
     */
    protected static function validateOrderId($orderId, $maxLen = 0)
    {
        if (self::isEmptyButNotZero($orderId)) {
            throw new Aivomatic_Fipn_Exception('$orderId must not be empty');
        }
        $strLen = self::numOfChars($orderId);
        if ($strLen > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                '$orderId ('.$orderId.') must be shorter than '
                .$maxLen.' (is '.$strLen.') characters');
        }
        return strval($orderId);
    }

    /**
     * Basic validation of customer Id
     * @param   string|int $customerId Alphanumeric
     * @param   int        $maxLen Maximum length
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($customerId)
     */
    protected static function validateCustomerId($customerId, $maxLen = 0)
    {
        if (!self::isIntOrString($customerId)) {
            throw new Aivomatic_Fipn_Exception(
                'customerId must be an integer or a string (is '.
                gettype($customerId).')');
        }
        $customerId = strval($customerId);
        try {
            self::validateString($customerId, $maxLen, 1);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $customerId failed:'.$e->getRawMessage());
        }
        if (!ctype_alnum($customerId)) {
            throw new Aivomatic_Fipn_Exception(
                '$customerId ('.$customerId.') must be alphanumeric (a-z, A-Z, 0-9)');
        }
        return $customerId;
    }

    /**
     * Basic validation of merchant's public Id
     * @param   string|int $publicMerchantId Not empty.
     * @param   int        $maxLen Maximum length
     * @param   int        $minLen Minimum length
     * @throws  Aivomatic_Fipn_Exception If $publicMerchantId isn't valid
     * @return  string     strval($publicMerchantId)
     */
    protected static function validatePublicMerchantId(
        $publicMerchantId,
        $maxLen = 0,
        $minLen = 0)
    {
        if (!self::isIntOrString($publicMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'publicMerchantId must be an integer or a string (is '.
                gettype($publicMerchantId).')');
        }
        $publicMerchantId = strval($publicMerchantId);
        try {
            self::validateString($publicMerchantId, $maxLen, $minLen);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $publicMerchantId failed:'.$e->getRawMessage());
        }
        return $publicMerchantId;
    }

    /**
     * Basic validation of merchant's secret Id
     * @param   string|int $secretMerchantId Merchant's secret Id code.
     * @param   int        $maxLen Maximum length
     * @param   int        $minLen Minimum length
     * @throws  Aivomatic_Fipn_Exception If $secretMerchantId isn't valid
     * @return  string     strval($secretMerchantId)
     */
    protected static function validateSecretMerchantId(
        $secretMerchantId,
        $maxLen = 0,
        $minLen = 0)
    {
        if (!self::isIntOrString($secretMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'secretMerchantId must be an integer or a string (is '.
                gettype($secretMerchantId).')');
        }
        $secretMerchantId = strval($secretMerchantId);
        try {
            self::validateString($secretMerchantId, $maxLen, $minLen);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $secretMerchantId failed:'.$e->getRawMessage());
        }
        if (!ctype_alnum($secretMerchantId)) {
            throw new Aivomatic_Fipn_Exception(
                'secretMerchantId ('.$secretMerchantId.') '.
                'must be alphanumeric (a-z, A-Z, 0-9)');
        }
        return $secretMerchantId;
    }

    /**
     * Basic validation of a string
     * @param   string $string
     * @param   int    $maxLen Maximum length
     * @param   int    $minLen Minimum length
     * @throws  Aivomatic_Fipn_Exception If not valid
     * @return  string $string
     */
    protected static function validateString(
        $string,
        $maxLen = 0,
        $minLen = 0)
    {
        if (!is_int($maxLen)) {
            throw new Aivomatic_Fipn_Exception(
                '$maxLen ('.$maxLen.') must be an integer '.
                '(is '.gettype($maxLen).')');
        }
        if (!is_int($minLen)) {
            throw new Aivomatic_Fipn_Exception(
                '$minLen ('.$minLen.') must be an integer '.
                '(is '.gettype($minLen).')');
        }
        if (!is_string($string)) {
            throw new Aivomatic_Fipn_Exception(
                '$string ('.$string.') must be a string '.
                '(is '.gettype($string).')');
        }
        if ($minLen != 0) {
            if (self::isEmptyButNotZero($string)) {
                throw new Aivomatic_Fipn_Exception(
                    '$string must not be empty');
            }
        }
        $strLen = self::numOfChars($string);
        if ($strLen > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                '$string ('.$string.') must not be longer than '.$maxLen
                .' (is '.$strLen.') characters');
        }
        if ($strLen < $minLen) {
            throw new Aivomatic_Fipn_Exception(
                '$string ('.$string.') must not be shorter than '.$minLen
                .' (is '.$strLen.') characters');
        }
        return $string;
    }

    /**
     * Basic validation of response request's GET-parameters array
     * @param   array $responseGetParams Requests GET-parameters as an array.
     * @param   array $obligatoryResponseGetParams
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  array $responseGetParams
     */
    protected static function validateResponseGetParams(
        $type,
        $responseGetParams,
        $obligatoryResponseGetParams = array())
    {
        self::validateResponseStatus($type, self::$allValidResponseStatues);
        if (!is_array($responseGetParams)) {
            throw new Aivomatic_Fipn_Exception(
                '$responseGetParams must be an array');
        }
        if (!empty($obligatoryResponseGetParams[$type]) AND
             empty($responseGetParams)) {
            throw new Aivomatic_Fipn_Exception(
                '$responseGetParams must not be empty');
        }
        foreach ($obligatoryResponseGetParams[$type] as $oblParam) {
            if(!array_key_exists($oblParam, $responseGetParams)) {
                throw new Aivomatic_Fipn_Exception(
                    'Response request parameter ('.$oblParam.') does not exist');
            }
        }
        return $responseGetParams;
    }

    /**
     * Basic validation of getUrlGetParams-methods parameter array
     * @param   array $paramArray
     * @param   array $obligatoryParams
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  array $paramArray
     */
    protected static function validateGetUrlGetParams(
        $type,
        $paramArray,
        $obligatoryParams = array())
    {
        if (!is_array($paramArray)) {
            throw new Aivomatic_Fipn_Exception(
                '$paramArray must be an array');
        }
        if (empty($paramArray)) {
            throw new Aivomatic_Fipn_Exception(
                '$paramArray must not be empty');
        }
        foreach ($obligatoryParams[$type] as $oblParam) {
            if (!array_key_exists($oblParam, $paramArray)) {
                throw new Aivomatic_Fipn_Exception(
                    'getUrlGetParams parameter ('.$oblParam.') does not exist');
            }
            if (self::isEmptyButNotZero($paramArray[$oblParam])) {
                throw new Aivomatic_Fipn_Exception(
                    'getUrlGetParams parameter ('.$oblParam.') must not be empty');
            }
        }
        return $paramArray;
    }

    /**
     * Validate status parameter
     * Valid statues are 'success' and 'cancel'. Some payment gateways may have
     * third valid status 'error'. These are directly relater to so called
     * successUrl, cancelUrl and errorUrl.
     * @param string $status  Status parameter.
     * @param array  $statues Valid statues for this payment gateway.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $status in all lower case
     */
    protected static function validateResponseStatus(
        $status,
        $validStatues = array())
    {

        if (!in_array(strtolower($status), $validStatues)) {
            throw new Aivomatic_Fipn_Exception(
                'Status ('.$status.') is not valid');
        }
        return strtolower($status);
    }

    /**
     * Basic validation of responseMac
     * @param   string|int $responseMac Alphanumeric Uppercase
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $responseMac
     */
    protected static function validateResponseMac($responseMac)
    {
        if (!preg_match('/^[A-F0-9]{32}$/', $responseMac)) {
            throw new Aivomatic_Fipn_Exception(
                'responseMac ('.$responseMac.') must be an uppercase md5 hash');
        }
        return $responseMac;
    }

    /**
     * Validates that parameter is an integer or a string
     *
     * Note that very big integers might be unexpectedly casted to type double.
     * The limit depends on your system.
     * @param   mixed $param
     * @return  bool true if int or string, false if some other type
     */
    public static function isIntOrString($param)
    {
        if (is_int($param) OR is_string($param)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if parameter is empty but not zero (0 or '0')
     *
     * PHP's buildin function empty() returns true even  if param is (int)0 or
     * (string)'0'. This method returns true if param is '' (an empty string)
     * NULL, FALSE or array() (an empty array). It returns false if param is 0
     * (integer 0), '0' (string 0) or 0.0 (float 0).
     * @param   mixed $param
     * @return  bool true if really empty, false if not empty including 0 and '0'
     */
    public static function isEmptyButNotZero($param)
    {
        if ((empty($param) OR $param === true) AND
             $param !== 0 AND
             $param !== '0' AND
             $param !== 0.0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if a string contains tags
     * @param string $string
     * @return bool True if string contains tags, else false
     */
    public static function containsTags($string)
    {
        if ($string != strip_tags($string)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Basic validation of finnish bank reference number's base part
     * @param   string|int $base Base part for finnish bank reference number
                           All digits. Length 3-19.
     * @throws  Aivomatic_Fipn_Exception If $base isn't valid
     * @return  string     strval($base)
     */
    public static function validateFbaBankRefBase($base)
    {
        if (!self::isIntOrString($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base must be an integer or a string (is '.
                gettype($base).')');
        }
        if (self::isEmptyButNotZero($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base must not be empty');
        }
        $base = strval($base);
        if (!ctype_digit($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be all digits (0-9)');
        }
        if (strlen($base) > 19) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be 20 characters or shorter'
                .' (is '.strlen($base).')');
        }
        if (strlen($base) < 1) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be three characters or longer'
                .' (is '.strlen($base).')');
        }
        return $base;
    }

    /**
     * Basic validation of unformatted finnish bank reference number
     * @param   string|int $ref Unformatted finnish bank reference number
                           All digits. Length 4-20.
     * @throws  Aivomatic_Fipn_Exception If $ref isn't valid
     * @return  string     strval($ref)
     */
    public static function validateUnformattedFbaBankRef($ref)
    {
        if (!self::isIntOrString($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must be an integer or a string (is '.
                gettype($ref).')');
        }
        if (self::isEmptyButNotZero($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must not be empty');
        }
        $ref = strval($ref);
        if (!ctype_digit($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be all digits (0-9)');
        }
        if (strlen($ref) > 20) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be 20 characters or shorter'
                .' (is '.strlen($ref).')');
        }
        if (strlen($ref) < 2) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be four characters or longer'
                .' (is '.strlen($ref).')');
        }
        if (strlen(ltrim($ref, '0')) < 2) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be four characters or longer even when '
                .'padding with zeros is removed from the beginning ('
                .ltrim($ref, '0').')(length '.strlen(ltrim($ref, '0')).')');
        }

        /**
         * Require Fipn base class (../Fipn.php)
         */
        require_once dirname(__FILE__).'.php';
        $base = substr($ref, 0, -1);
        $checkSum = substr($ref, -1);
        if ($checkSum != Aivomatic_Fipn::calculateFbaBankRefCheckSum($base)) {
            throw new Aivomatic_Fipn_Exception(
                'Checksum ('.$checkSum.') of the bank reference number ('.$ref
                .') is not valid');
        }
        return $ref;
    }

    /**
     * Validate transaction Id
     * @param string $transactionId
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $transactionId
     */
    public static function validateTransactionId($transactionId, $maxLen = 0)
    {
        if (!empty($transactionId)) {
            if (!ctype_alnum($transactionId)) {
                throw new Aivomatic_Fipn_Exception(
                    'transactionId ('.$transactionId.')'
                    .' must be alphanumeric (a-z, A-Z, 0-9)');
            }
            if (strlen($transactionId) > $maxLen) {
            throw new Aivomatic_Fipn_Exception(
                'transactionId ('.$transactionId.') must be shorter than '
                .$maxLen.' (is '.strlen($transactionId).') characters');
            }
        }
        return $transactionId;
    }

    /**
     * Compares two floats and checks they match within precision
     * @param float $sum1
     * @param float $sum2
     * @param float $precision Maximum diffrence between sum1 and sum2
     * @return bool True if match within precision
     */
     public static function compareSums($sum1, $sum2, $precision = 0)
     {
        return abs($sum1-$sum2) <= $precision;
     }

    /**
     * Validates ISO 3361-1 alpha-2 two letter country code
     *
     * Case insensitive.
     * @link http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
     * @param   string $code Case insensitive two letter country code.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $code
     */
    public static function validateISO3166_1Alpha2($code)
    {
        $codes = array(
            'af', 'ax', 'al', 'dz', 'as', 'ad', 'ao', 'ai', 'aq', 'ag', 'ar',
            'am', 'aw', 'au', 'at', 'az', 'bs', 'bh', 'bd', 'bb', 'by', 'be',
            'bz', 'bj', 'bm', 'bt', 'bo', 'ba', 'bw', 'bv', 'br', 'io', 'bn',
            'bg', 'bf', 'bi', 'kh', 'cm', 'ca', 'cv', 'ky', 'cf', 'td', 'cl',
            'cn', 'cx', 'cc', 'co', 'km', 'cg', 'cd', 'ck', 'cr', 'ci', 'hr',
            'cu', 'cy', 'cz', 'dk', 'dj', 'dm', 'do', 'ec', 'eg', 'sv', 'gq',
            'er', 'ee', 'et', 'fk', 'fo', 'fj', 'fi', 'fr', 'gf', 'pf', 'tf',
            'ga', 'gm', 'ge', 'de', 'gh', 'gi', 'gr', 'gl', 'gd', 'gp', 'gu',
            'gt', 'gg', 'gn', 'gw', 'gy', 'ht', 'hm', 'va', 'hn', 'hk', 'hu',
            'is', 'in', 'id', 'ir', 'iq', 'ie', 'im', 'il', 'it', 'jm', 'jp',
            'je', 'jo', 'kz', 'ke', 'ki', 'kp', 'kr', 'kw', 'kg', 'la', 'lv',
            'lb', 'ls', 'lr', 'ly', 'li', 'lt', 'lu', 'mo', 'mk', 'mg', 'mw',
            'my', 'mv', 'ml', 'mt', 'mh', 'mq', 'mr', 'mu', 'yt', 'mx', 'fm',
            'md', 'mc', 'mn', 'me', 'ms', 'ma', 'mz', 'mm', 'na', 'nr', 'np',
            'nl', 'an', 'nc', 'nz', 'ni', 'ne', 'ng', 'nu', 'nf', 'mp', 'no',
            'om', 'pk', 'pw', 'ps', 'pa', 'pg', 'py', 'pe', 'ph', 'pn', 'pl',
            'pt', 'pr', 'qa', 're', 'ro', 'ru', 'rw', 'bl', 'sh', 'kn', 'lc',
            'mf', 'pm', 'vc', 'ws', 'sm', 'st', 'sa', 'sn', 'rs', 'sc', 'sl',
            'sg', 'sk', 'si', 'sb', 'so', 'za', 'gs', 'es', 'lk', 'sd', 'sr',
            'sj', 'sz', 'se', 'ch', 'sy', 'tw', 'tj', 'tz', 'th', 'tl', 'tg',
            'tk', 'to', 'tt', 'tn', 'tr', 'tm', 'tc', 'tv', 'ug', 'ua', 'ae',
            'gb', 'us', 'um', 'uy', 'uz', 'vu', 've', 'vn', 'vg', 'vi', 'wf',
            'eh', 'ye', 'zm', 'zw');
        if (!in_array(strtolower(strval($code)), $codes)) {
            throw new Aivomatic_Fipn_Exception(
                '$code ('.$code.') is not a valid ISO 3361-1 alpha-2 two '.
                'letter country code');
        }
        return $code;
    }

    /**
     * Validates ISO 3361-1 alpha-3 three letter country code
     *
     * Case insensitive.
     * @link http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
     * @param   string $code Case insensitive three letter country code.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $code
     */
    public static function validateISO3166_1Alpha3($code)
    {
        $codes = array(
            'afg', 'alb', 'dza', 'asm', 'and', 'ago', 'aia', 'ata', 'atg',
            'arg', 'arm', 'abw', 'aus', 'aut', 'aze', 'bhs', 'bhr', 'bgd',
            'brb', 'blr', 'bel', 'blz', 'ben', 'bmu', 'btn', 'bol', 'bih',
            'bwa', 'bvt', 'bra', 'iot', 'vgb', 'brn', 'bgr', 'bfa', 'bdi',
            'khm', 'cmr', 'can', 'cpv', 'cym', 'caf', 'tcd', 'chl', 'chn',
            'cxr', 'cck', 'col', 'com', 'cod', 'cog', 'cok', 'cri', 'civ',
            'cub', 'cyp', 'cze', 'dnk', 'dji', 'dma', 'dom', 'ecu', 'egy',
            'slv', 'gnq', 'eri', 'est', 'eth', 'fro', 'flk', 'fji', 'fin',
            'fra', 'guf', 'pyf', 'atf', 'gab', 'gmb', 'geo', 'deu', 'gha',
            'gib', 'grc', 'grl', 'grd', 'glp', 'gum', 'gtm', 'gin', 'gnb',
            'guy', 'hti', 'hmd', 'vat', 'hnd', 'hkg', 'hrv', 'hun', 'isl',
            'ind', 'idn', 'irn', 'irq', 'irl', 'isr', 'ita', 'jam', 'jpn',
            'jor', 'kaz', 'ken', 'kir', 'prk', 'kor', 'kwt', 'kgz', 'lao',
            'lva', 'lbn', 'lso', 'lbr', 'lby', 'lie', 'ltu', 'lux', 'mac',
            'mkd', 'mdg', 'mwi', 'mys', 'mdv', 'mli', 'mlt', 'mhl', 'mtq',
            'mrt', 'mus', 'myt', 'mex', 'fsm', 'mda', 'mco', 'mng', 'msr',
            'mar', 'moz', 'mmr', 'nam', 'nru', 'npl', 'ant', 'nld', 'ncl',
            'nzl', 'nic', 'ner', 'nga', 'niu', 'nfk', 'mnp', 'nor', 'omn',
            'pak', 'plw', 'pse', 'pan', 'png', 'pry', 'per', 'phl', 'pcn',
            'pol', 'prt', 'pri', 'qat', 'reu', 'rou', 'rus', 'rwa', 'shn',
            'kna', 'lca', 'spm', 'vct', 'wsm', 'smr', 'stp', 'sau', 'sen',
            'scg', 'syc', 'sle', 'sgp', 'svk', 'svn', 'slb', 'som', 'zaf',
            'sgs', 'esp', 'lka', 'sdn', 'sur', 'sjm', 'swz', 'swe', 'che',
            'syr', 'twn', 'tjk', 'tza', 'tha', 'tls', 'tgo', 'tkl', 'ton',
            'tto', 'tun', 'tur', 'tkm', 'tca', 'tuv', 'vir', 'uga', 'ukr',
            'are', 'gbr', 'umi', 'usa', 'ury', 'uzb', 'vut', 'ven', 'vnm',
            'wlf', 'esh', 'yem', 'zmb', 'zwe');
        if (!in_array(strtolower(strval($code)), $codes)) {
            throw new Aivomatic_Fipn_Exception(
                '$code ('.$code.') is not a valid ISO 3361-1 alpha-3 three '.
                'letter country code');
        }
        return $code;
    }

    /**
     * Returns length of the string (number of characters)
     *
     * Tries to tackle multibyte strings correctly even on servers where
     * PHP multibyte functions are not installed.
     *
     * @param string $str
     * @return int Number of characters in string
     */
    public static function numOfChars($str) {
        return Aivomatic_Fipn_Factory::getMbString()->strlenMb($str);
    }

    /**
     * Validates unix timestamp
     *
     * Max: 2147483647 = 2038-01-19 03:14:07 +0000
     * Min in Fipn: 0 = 1970-01-01 00:00:00 +0000
     * Theoretical min:-2147483648 = 1901-12-13 20:45:52 +0000
     *
     * @param string|int $time 0 - 2147483647
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return string strval($time)
     */
    public static function validateUnixTimestamp($time) {
        $strtime = strval($time);
        if (!ctype_digit($strtime)) {
            throw new Aivomatic_Fipn_Exception(
                '$time ('.$strtime.')'
                .' must be all numbers (0-9)');
        }
        if (strlen($strtime) > 10) {
        throw new Aivomatic_Fipn_Exception(
            '$time ('.$strtime.') must be shorter than 10'
            .' (is '.strlen($strtime).') characters');
        }
        $inttime = intval($time);
        if ($inttime > 2147483647) {
            throw new Aivomatic_Fipn_Exception(
            '$time ('.$strtime.') must be smaller than 2147483647');
        }
        if ($inttime < 0) {
            throw new Aivomatic_Fipn_Exception(
            '$time ('.$strtime.') must be bigger than zero');
        }
        return $strtime;
    }

    /**
     * Validates boolean (true or false)
     *
     * @param bool $bool
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return bool $bool
     */
    public static function validateBoolean($bool) {
        if (!is_bool($bool)) {
            throw new Aivomatic_Fipn_Exception(
                '$bool ('.strval($bool).')'
                .' must be boolean (is '.gettype($bool).')');
        }
        return $bool;
    }
}
?>