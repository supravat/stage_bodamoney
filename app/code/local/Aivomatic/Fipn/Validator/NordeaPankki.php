<?php
/**
 * Aivomatic_Fipn_Validator_NordeaPankki
 *
 * @category    Aivomatic
 * @package     Fipn_NordeaPankki
 * @version     ver. 1.8.1 - $Id: NordeaPankki.php 1133 2010-01-08 04:28:45Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Validator/NordeaPankki.php $
 * @copyright   2009 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require validator base class (../Validator.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Aivomatic Fipn Validator for Nordea Pankki payment gateway
 *
 * @category    Aivomatic
 * @package     Fipn_NordeaPankki
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Validator_NordeaPankki extends Aivomatic_Fipn_Validator
{

    const FIPN_NORDEAP_LOCALE_FI = '1';
    const FIPN_NORDEAP_LOCALE_SE = '2';
    const FIPN_NORDEAP_LOCALE_EN = '3';

    const FIPN_NORDEAP_WAPI_VERSION = '0003';

    const FIPN_NORDEAP_DESC_MAX_LEN                  = 210;
    const FIPN_NORDEAP_DESC_MAX_LINES                = 6;
    const FIPN_NORDEAP_DESC_MAX_LINE_LEN             = 35;
    const FIPN_NORDEAP_URL_MAX_LEN                   = 120;
    const FIPN_NORDEAP_ORDER_TOTAL_MAX_LEN           = 19;
    const FIPN_NORDEAP_ORDER_ID_MAX_LEN              = 20;
    const FIPN_NORDEAP_PUBLIC_MERCHANT_ID_MAX_LEN    = 15;
    const FIPN_NORDEAP_PUBLIC_MERCHANT_ID_MIN_LEN    = 8;
    const FIPN_NORDEAP_SECRET_MERCHANT_ID_MAX_LEN    = 64;
    const FIPN_NORDEAP_SECRET_MERCHANT_ID_MIN_LEN    = 5;
    const FIPN_NORDEAP_KEY_VERSION_LEN               = 4;
    const FIPN_NORDEAP_BANK_ACCOUNT_MAX_LEN          = 15;
    const FIPN_NORDEAP_BANK_ACCOUNT_MIN_LEN          = 8;
    const FIPN_NORDEAP_MERCHANT_NAME_MAX_LEN         = 30;
    const FIPN_NORDEAP_MERCHANT_NAME_MIN_LEN         = 1;
    const FIPN_NORDEAP_TRANSACTION_ID_MAX_LEN        = 20;

    protected static $obligatoryResponseGetParams =
        array('success' => array('RETURN_VERSION', 'RETURN_STAMP',
                                 'RETURN_REF', 'RETURN_PAID', 'RETURN_MAC'),
              'cancel'  => array('RETURN_VERSION', 'RETURN_STAMP',
                                 'RETURN_REF', 'RETURN_MAC'),
              'error'   => array('RETURN_VERSION', 'RETURN_STAMP',
                                 'RETURN_REF', 'RETURN_MAC'));

    protected static $validStatues = array('success', 'cancel', 'error');

    /**
     * Validates merchant's secret Id according to specs of NordeaPankki
     * @param   string $secretMerchantId Alphanumeric
     * @param mixed  $ignored Uses self::FIPN_NORDEAP_SECRET_MERCHANT_ID_MAX_LEN
     * @param mixed  $ignored Uses self::FIPN_NORDEAP_SECRET_MERCHANT_ID_MIN_LEN
     * @throws  Aivomatic_Fipn_Exception If $secretMerchantId isn't valid
     * @return  string $secretMerchantId
     */
    public static function validateSecretMerchantId(
        $secretMerchantId,
        $ignored1 = false,
        $ignored2 = false)
    {
        return parent::validateSecretMerchantId(
                    $secretMerchantId,
                    self::FIPN_NORDEAP_SECRET_MERCHANT_ID_MAX_LEN,
                    self::FIPN_NORDEAP_SECRET_MERCHANT_ID_MIN_LEN);
    }

    /**
     * Validates merchant's public Id according to specs of NordeaPankki
     * @param string|int $publicMerchantId All digits.
     * @param mixed  $maxIgnored Uses self::FIPN_NORDEAP_PUBLIC_MERCHANT_ID_MAX_LEN
     * @param mixed  $minIgnored Uses self::FIPN_NORDEAP_PUBLIC_MERCHANT_ID_MIN_LEN
     * @throws  Aivomatic_Fipn_Exception If $publicMerchantId isn't valid
     * @return  string strval($publicMerchantId)
     */
    public static function validatePublicMerchantId(
        $publicMerchantId,
        $maxIgnored = false,
        $minIgnored = false)
    {
        parent::validatePublicMerchantId(
            $publicMerchantId,
            self::FIPN_NORDEAP_PUBLIC_MERCHANT_ID_MAX_LEN,
            self::FIPN_NORDEAP_PUBLIC_MERCHANT_ID_MIN_LEN);
        /*if (!ctype_digit(strval($publicMerchantId))) {
            throw new Aivomatic_Fipn_Exception(
                '$publicMerchantId ('.$publicMerchantId.') must be all digits');
        }*/
        return strval($publicMerchantId);
    }

    /**
     * Validates order Id according to specs of NordeaPankki
     * @param   string|int $orderId Numeric, max. len. 20.
     * @param mixed  $ignored Uses self::FIPN_NORDEAP_ORDER_ID_MAX_LEN
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $orderId
     */
    public static function validateOrderId($orderId, $ignored = false)
    {
        if (!ctype_digit(strval($orderId))) {
            throw new Aivomatic_Fipn_Exception(
                '$orderId ('.$orderId.') must be all digits (0-9)');
        }
        return parent::validateOrderId($orderId,
                                       self::FIPN_NORDEAP_ORDER_ID_MAX_LEN);
    }

    /**
     * Validates total amount according to specs of Nordea Pankki
     * @param   float $orderTotal Strictly float! Max. len 19
     * @param mixed  $ignored Uses self::FIPN_NORDEAP_ORDER_TOTAL_MAX_LEN
     * @return  string strval($orderTotal)
     */
    public static function validateOrderTotal($orderTotal, $ignored = false)
    {
        if (!is_float($orderTotal)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $orderTotal ('.$orderTotal
                .') must be a float');
        }
        return parent::validateOrderTotal(
                    $orderTotal,
                    self::FIPN_NORDEAP_ORDER_TOTAL_MAX_LEN);
    }

    /**
     * Validates return URL
     * @param   string  $url
     * @param mixed  $ignored Uses self::FIPN_NORDEAP_URL_MAX_LEN
     * @return  string $url
     */
    public static function validateReturnUrl($url, $ignored = false)
    {
        return parent::validateReturnUrl($url, self::FIPN_NORDEAP_URL_MAX_LEN);
    }

    /**
     * Validates locale according to specs of NordeaPankki
     * @param   string|int $locale '1' (fi), '2' (se) or '3' (en)
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $locale
     */
    public static function validateLocale($locale)
    {
        $locale = parent::validateLocale($locale);
        if ($locale != self::FIPN_NORDEAP_LOCALE_FI AND
            $locale != self::FIPN_NORDEAP_LOCALE_EN AND
            $locale != self::FIPN_NORDEAP_LOCALE_SE) {
            throw new Aivomatic_Fipn_Exception(
                '$locale ('.$locale.') must be '
                .self::FIPN_NORDEAP_LOCALE_FI.' or '
                .self::FIPN_NORDEAP_LOCALE_EN.' or '
                .self::FIPN_NORDEAP_LOCALE_SE);
        }
        return $locale;
    }

    /**
     * Validates order description according to specs of NordeaPankki
     * @param   string|int $orderId Alphanumeric, max. len. 999.
     * @param mixed  $ignored API compatibility
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $orderDesc
     */
    public static function validateOrderDesc($orderDesc, $ignored = false)
    {
        $orderDescRows = explode(chr(10), $orderDesc);
        if (count($orderDescRows) > self::FIPN_NORDEAP_DESC_MAX_LINES) {
            throw new Aivomatic_Fipn_Exception('$orderDesc must not contain '.
                'more than '.self::FIPN_NORDEAP_DESC_MAX_LINES.' lines '.
                '(contains '.count($orderDescRows).')');
        }
        $charsTotal = 0;
        foreach ($orderDescRows as $line => $string) {
            $chars = self::numOfChars(rtrim($string, chr(10)));
            if ($chars > self::FIPN_NORDEAP_DESC_MAX_LINE_LEN) {
                throw new Aivomatic_Fipn_Exception('Line '.$line.
                    ' ('.$string.') of $orderDesc is longer than '.
                    self::FIPN_NORDEAP_DESC_MAX_LINE_LEN.' (is '.$chars.
                    ') characters');
            }
            $charsTotal = $charsTotal+$chars;
        }
        if ($charsTotal > self::FIPN_NORDEAP_DESC_MAX_LEN) {
            throw new Aivomatic_Fipn_Exception('$orderDesc must not contain '.
                'more than '.self::FIPN_NORDEAP_DESC_MAX_LEN.' characters '.
                '(contains '.$charsTotal.')');
        }
        return strval($orderDesc);
    }

    /**
     * Basic validation of response request's GET-parameters array
     * @param string $type 'success', 'cancel' or 'error'
     * @param array $responseGetParams Requests GET-parameters as an array.
     * @param mixed  $ignored Uses self::$obligatorySuccessResponseGetParams
     * @return  array $responseGetParams
     */
    public static function validateResponseGetParams(
        $type,
        $responseGetParams,
        $ignored = false)
    {
        return parent::validateResponseGetParams(
                    self::validateResponseStatus($type),
                    $responseGetParams,
                    self::$obligatoryResponseGetParams);
    }

    /**
     * Validate status parameter
     * Valid statues for NordeaPankki are 'success', 'cancel' and 'error'
     * @param string $status  Status parameter.
     * @param mixed  $ignored Uses self::$validStatues
     * @return  string $status
     */
    public static function validateResponseStatus($status, $ignored = false)
    {
        return parent::validateResponseStatus($status, self::$validStatues);
    }

    /**
     * Basic validation of responseMac
     * @param   string|int $responseMac Alphanumeric Uppercase
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $responseMac
     */
    public static function validateResponseMac($responseMac)
    {
        return parent::validateResponseMac($responseMac);
    }

    /**
     * Basic validation of currency code
     * @param   string $currency 'eur' or 'euro' - case insensitive
     * @return  void
     */
    public static function validateCurrency($currency)
    {
        return parent::validateCurrency($currency);
    }

    /**
     * Validates MAC key version
     * @param   string $keyVersion Numeric, len. 4.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $keyVersion
     */
    public static function validateKeyVersion($keyVersion)
    {
        if (parent::isEmptyButNotZero($keyVersion)) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion must not be empty');
        }
        if (!is_string($keyVersion)) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion must be a string (is '.
                gettype($keyVersion).')');
        }
        if (!ctype_digit(strval($keyVersion))) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion ('.$keyVersion.') must be all digits (0-9)');
        }
        if (strlen($keyVersion) != self::FIPN_NORDEAP_KEY_VERSION_LEN) {
            throw new Aivomatic_Fipn_Exception(
                '$keyVersion ('.$keyVersion.') must be exactly'
                .self::FIPN_NORDEAP_KEY_VERSION_LEN.' '.
                '(is '.strlen($keyVersion).' characters');
        }
        return $keyVersion;
    }

    /**
     * Validates bank account number
     * @param   string|int $bankAccount AlphaNumeric, len. 8-15. Note that long
     *                      integer might be casted to be a double.
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $bankAccount
     */
    public static function validateBankAccount($bankAccount)
    {
        if (parent::isEmptyButNotZero($bankAccount)) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount must not be empty');
        }
        if (!parent::isIntOrString($bankAccount)) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount must be an integer or a string (is '.
                gettype($bankAccount).')');
        }
        // "Asiakastunnus sopimuslomakkeesta; syötetään ilman väliviivaa"
        if (strpos($bankAccount, '-')) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount ('.$bankAccount.') must not contain a dash (-) '.
                'character');
        }
        if (!ctype_alnum(strval($bankAccount))) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount ('.$bankAccount.') must be alphanumeric '.
                '(a-z, A-Z, 0-9)');
        }
        if (self::numOfChars($bankAccount) > self::FIPN_NORDEAP_BANK_ACCOUNT_MAX_LEN) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount ('.$bankAccount.') must be shorter than'
                .self::FIPN_NORDEAP_BANK_ACCOUNT_MAX_LEN.' '.
                '(is '.self::numOfChars($bankAccount).' characters');
        }
        if (self::numOfChars($bankAccount) < self::FIPN_NORDEAP_BANK_ACCOUNT_MIN_LEN) {
            throw new Aivomatic_Fipn_Exception(
                '$bankAccount ('.$bankAccount.') must be longer than'
                .self::FIPN_NORDEAP_BANK_ACCOUNT_MIN_LEN.' '.
                '(is '.self::numOfChars($bankAccount).' characters');
        }
        return strval($bankAccount);
    }

    /**
     * Validates merchant name
     * @param   string $merchantName AlphaNumeric, len. 1-30
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $merchantName
     */
    public static function validateMerchantName($merchantName)
    {
        if (parent::isEmptyButNotZero($merchantName)) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName must not be empty');
        }
        if (!is_string($merchantName)) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName must be a string (is '.
                gettype($merchantName).')');
        }
        if (self::numOfChars($merchantName) >
            self::FIPN_NORDEAP_MERCHANT_NAME_MAX_LEN) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName ('.$merchantName.') must be shorter than'
                .self::FIPN_NORDEAP_MERCHANT_NAME_MAX_LEN.' '.
                '(is '.self::numOfChars($merchantName).') characters');
        }
        if (self::numOfChars($merchantName) <
            self::FIPN_NORDEAP_MERCHANT_NAME_MIN_LEN) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName ('.$merchantName.') must be longer than'
                .self::FIPN_NORDEAP_MERCHANT_NAME_MIN_LEN.' '.
                '(is '.self::numOfChars($merchantName).') characters');
        }
        if (parent::containsTags($merchantName)) {
            throw new Aivomatic_Fipn_Exception(
                '$merchantName ('.$merchantName.') must not contain tags');
        }
        return $merchantName;
    }

    /**
     * Validates Web API version number
     * @param   string  $wapi   Must be '0003'
     * @uses    Aivomatic_Fipn_Validator_NordeaPankki::FIPN_NORDEAP_WAPI_VERSION
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string  $wapi
     */
    public static function validateWapiVersion($wapi)
    {
        if ($wapi != Aivomatic_Fipn_Validator_NordeaPankki::
            FIPN_NORDEAP_WAPI_VERSION) {
            throw new Aivomatic_Fipn_Exception(
                '$wapi ('.$wapi.') should be equal to "'.
                Aivomatic_Fipn_Validator_NordeaPankki::
                FIPN_NORDEAP_WAPI_VERSION.'"');
        }
    }

    /**
     * Basic validation of bank reference number's base part
     * Officially bank reference number must be at least 4 characters long
     * (including the chech sum) but Nordea accepts 2 characters long reference
     * numbers.
     * @param   string|int $base Base part for finnish bank reference number
                           All digits. Length 1-19.
     * @throws  Aivomatic_Fipn_Exception If $base isn't valid
     * @return  string     strval($base)
     */
    public static function validateFbaBankRefBase($base)
    {
        if (!self::isIntOrString($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base must be an integer or a string (is '.
                gettype($base).')');
        }
        if (self::isEmptyButNotZero($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base must not be empty');
        }
        $base = strval($base);
        if (!ctype_digit($base)) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be all digits (0-9)');
        }
        if (strlen($base) > 19) {
            throw new Aivomatic_Fipn_Exception(
                '$base ('.$base.') must be 20 characters or shorter'
                .' (is '.strlen($base).')');
        }
        return $base;
    }

    /**
     * Basic validation of unformatted bank reference number
     * @param   string|int $ref Unformatted bank reference number.
     *                     Note! Nordea allows short (2 chars) ref numbers.
     *                     All digits. Length 2-20.
     * @throws  Aivomatic_Fipn_Exception If $ref isn't valid
     * @return  string     strval($ref)
     */
    public static function validateUnformattedFbaBankRef($ref)
    {
        if (!self::isIntOrString($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must be an integer or a string (is '.
                gettype($ref).')');
        }
        if (self::isEmptyButNotZero($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref must not be empty');
        }
        $ref = strval($ref);
        if (!ctype_digit($ref)) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be all digits (0-9)');
        }
        if (strlen($ref) > 20) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be 20 characters or shorter'
                .' (is '.strlen($ref).')');
        }
        if (strlen($ref) < 2) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be two characters or longer'
                .' (is '.strlen($ref).')');
        }
        if (strlen(ltrim($ref, '0')) < 2) {
            throw new Aivomatic_Fipn_Exception(
                '$ref ('.$ref.') must be two characters or longer even when '
                .'padding with zeros is removed from the beginning ('
                .ltrim($ref, '0').')(length '.strlen(ltrim($ref, '0')).')');
        }
        /**
         * Require Fipn base class (../Fipn.php)
         */
        require_once dirname(dirname(__FILE__)).'/Sender/NordeaPankki.php';
        $base = substr($ref, 0, -1);
        $checkSum = substr($ref, -1);
        if ($checkSum != Aivomatic_Fipn_Sender_NordeaPankki::
            calculateFbaBankRefCheckSum($base)) {
            throw new Aivomatic_Fipn_Exception(
                'Checksum ('.$checkSum.') of the bank reference number ('.$ref
                .') is not valid');
        }
        return $ref;
    }

    /**
     * Validate transaction Id
     * @param string $transactionId
     * @param mixed  $ignored Uses self::FIPN_NORDEAP_TRANSACTION_ID_MAX_LEN
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $transactionId
     */
    public static function validateTransactionId($transactionId, $ignored = false)
    {
        return parent::validateTransactionId($transactionId,
            self::FIPN_NORDEAP_TRANSACTION_ID_MAX_LEN);
    }
}
?>