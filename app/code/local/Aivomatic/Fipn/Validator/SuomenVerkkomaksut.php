<?php
/**
 * Aivomatic_Fipn_Validator_SuomenVerkkomaksut
 *
 * @category    Aivomatic
 * @package     Fipn_SuomenVerkkomaksut
 * @version     ver. 1.10.2 - $Id: SuomenVerkkomaksut.php 1726 2013-01-30 05:46:21Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Validator/SuomenVerkkomaksut.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require validator base class (../Validator.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Aivomatic Fipn Validator for Suomen Verkkomaksut eFactoring payment gateway
 *
 * @category    Aivomatic
 * @package     Fipn_SuomenVerkkomaksut
 * @since       0.2.0+
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Validator_SuomenVerkkomaksut extends Aivomatic_Fipn_Validator
{

    const FIPN_SVM_LOCALE_FI = 'fi_FI';
    const FIPN_SVM_LOCALE_EN = 'en_US';
    const FIPN_SVM_LOCALE_SV = 'sv_SE';
    //const FIPN_SVM_LOCALE_RU = 'ru_RU';

    const FIPN_SVM_ITEM_TYPE_PRODUCT  = '1';
    const FIPN_SVM_ITEM_TYPE_SHIPPING = '2';
    const FIPN_SVM_ITEM_TYPE_HANDLING = '3';

    const FIPN_SVM_DESC_MAX_LEN                  = 65000;
    const FIPN_SVM_URL_MAX_LEN                   = 255;
    const FIPN_SVM_ORDER_TOTAL_MAX_LEN           = 10;
    const FIPN_SVM_ORDER_ID_MAX_LEN              = 50;
    const FIPN_SVM_PUBLIC_MERCHANT_ID_MAX_LEN    = 11;
    const FIPN_SVM_PUBLIC_MERCHANT_ID_MIN_LEN    = 5;       //assumption
    const FIPN_SVM_SECRET_MERCHANT_ID_MAX_LEN    = 32;
    const FIPN_SVM_SECRET_MERCHANT_ID_MIN_LEN    = 30;      //assumption
    const FIPN_SVM_TIMESTAMP_LEN                 = 10;
    const FIPN_SVM_TRANSACTION_ID_MAX_LEN        = 10;

    const FIPN_SVM_FIRST_NAME_MAX_LEN            = 64;
    const FIPN_SVM_FIRST_NAME_MIN_LEN            = 1;

    const FIPN_SVM_LAST_NAME_MAX_LEN             = 64;
    const FIPN_SVM_LAST_NAME_MIN_LEN             = 1;

    const FIPN_SVM_COMPANY_NAME_MAX_LEN          = 128;
    const FIPN_SVM_COMPANY_NAME_MIN_LEN          = 0;

    const FIPN_SVM_ADDRESS_MAX_LEN               = 128;
    const FIPN_SVM_ADDRESS_MIN_LEN               = 1;

    const FIPN_SVM_ZIP_MAX_LEN                   = 16;
    const FIPN_SVM_ZIP_MIN_LEN                   = 1;

    const FIPN_SVM_CITY_MAX_LEN                  = 64;
    const FIPN_SVM_CITY_MIN_LEN                  = 1;

    const FIPN_SVM_EMAIL_MAX_LEN                 = 255;

    const FIPN_SVM_CELLPHONE_MAX_LEN             = 64;
    const FIPN_SVM_CELLPHONE_MIN_LEN             = 1;

    const FIPN_SVM_WIREPHONE_MAX_LEN             = 64;
    const FIPN_SVM_WIREPHONE_MIN_LEN             = 0;

    const FIPN_SVM_ITEM_NAME_MAX_LEN             = 255;
    const FIPN_SVM_ITEM_NAME_MIN_LEN             = 1;

    const FIPN_SVM_ITEM_SKU_MAX_LEN              = 255;
    const FIPN_SVM_ITEM_SKU_MIN_LEN              = 0;

    const FIPN_SVM_ITEM_QTY_MAX_LEN              = 10;

    const FIPN_SVM_ITEM_PRICE_MAX_LEN            = 10;

    const FIPN_SVM_ITEM_VAT_CLASS_MAX_VAL        = 100.00;
    const FIPN_SVM_ITEM_VAT_CLASS_MIN_VAL        = 0.00;
    const FIPN_SVM_ITEM_VAT_CLASS_MAX_LEN        = 10;

    const FIPN_SVM_ITEM_DISCOUNT_MAX_VAL         = 100.00;
    const FIPN_SVM_ITEM_DISCOUNT_MIN_VAL         = 0.00;
    const FIPN_SVM_ITEM_DISCOUNT_MAX_LEN         = 10;

    const FIPN_SVM_TOTAL_SUM_PRECISION           = 0.02;

    const FIPN_SVM_METHOD_ID_MAX_LEN             = 2;
    const FIPN_SVM_METHOD_ID_MIN_LEN             = 1;

    const WAPI_TYPE = '4';

    protected static $obligatoryResponseGetParams =
        array('success' => array('ORDER_NUMBER', 'TIMESTAMP', 'RETURN_AUTHCODE', 'PAID', 'METHOD'),
              'notify' => array('ORDER_NUMBER', 'TIMESTAMP', 'RETURN_AUTHCODE', 'PAID', 'METHOD'),
              //'delay' => array('ORDER_NUMBER', 'TIMESTAMP', 'RETURN_AUTHCODE', 'PAID', 'METHOD'),
              'cancel'  => array());

    //protected static $validStatues = array('success', 'cancel', 'notify', 'delay');
    protected static $validStatues = array('success', 'cancel', 'notify');

    /**
     * Validates merchant's secret Id according to specs of Suomen Verkkomaksut
     * @param   string $secretMerchantId Alphanumeric
     * @param mixed  $maxIgnored Uses self::FIPN_SVM_SECRET_MERCHANT_ID_MAX_LEN
     * @param mixed  $minIgnored Uses self::FIPN_SVM_SECRET_MERCHANT_ID_MIN_LEN
     * @throws  Aivomatic_Fipn_Exception If $secretMerchantId isn't valid
     * @return  string $secretMerchantId
     */
    public static function validateSecretMerchantId(
        $secretMerchantId,
        $maxIgnored = false,
        $minIgnored = false)
    {
        return parent::validateSecretMerchantId(
                    $secretMerchantId,
                    self::FIPN_SVM_SECRET_MERCHANT_ID_MAX_LEN,
                    self::FIPN_SVM_SECRET_MERCHANT_ID_MIN_LEN);
    }

    /**
     * Validates merchant's public Id according to specs of Suomen Verkkomaksut
     * @param   string|int $publicMerchantId All digits.
     * @param mixed  $ignored Uses self::FIPN_SVM_PUBLIC_MERCHANT_ID_MAX_LEN
     * @throws  Aivomatic_Fipn_Exception If $publicMerchantId isn't valid
     * @return  string strval($publicMerchantId)
     */
    public static function validatePublicMerchantId(
        $publicMerchantId,
        $maxIgnored = false,
        $minIgnored = false)
    {
        parent::validatePublicMerchantId(
            $publicMerchantId,
            self::FIPN_SVM_PUBLIC_MERCHANT_ID_MAX_LEN,
            self::FIPN_SVM_PUBLIC_MERCHANT_ID_MIN_LEN);
        if (!ctype_digit(strval($publicMerchantId))) {
            throw new Aivomatic_Fipn_Exception(
                '$publicMerchantId ('.$publicMerchantId.') must be all digits');
        }
        return strval($publicMerchantId);
    }

    /**
     * Validates method Id
     * @param   string|int $methodId All digits.
     * @throws  Aivomatic_Fipn_Exception If $methodId isn't valid
     * @return  string strval($methodId)
     */
    public static function validateMethodId($methodId)
    {
        if(!parent::isIntOrString($methodId)) {
            throw new Aivomatic_Fipn_Exception(
                '$methodId must be int or string (is '.gettype($methodId).')');
        }
        $methodId = strval($methodId);
        parent::validateString(
            $methodId,
            self::FIPN_SVM_METHOD_ID_MAX_LEN,
            self::FIPN_SVM_METHOD_ID_MIN_LEN);
        if (!ctype_digit($methodId)) {
            throw new Aivomatic_Fipn_Exception(
                '$methodId ('.$methodId.') must be all digits');
        }
        return $methodId;
    }

    /**
     * Validates order Id according to specs of Suomen Verkkomaksut
     * @param   string|int $orderId Alphanumeric, max. len. 50.
     * @param mixed  $ignored Uses self::FIPN_SVM_ORDER_ID_MAX_LEN
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $orderId
     */
    public static function validateOrderId($orderId, $ignored = false)
    {
        if (!ctype_alnum(strval($orderId))) {
            throw new Aivomatic_Fipn_Exception(
                '$orderId ('.$orderId.') must be alphanumeric (a-z, A-Z, 0-9)');
        }
        return parent::validateOrderId($orderId,
                                       self::FIPN_SVM_ORDER_ID_MAX_LEN);
    }

    /**
     * Validates total amount according to specs of Suomen Verkkomaksut
     * @param   float $orderTotal Strictly float! Max. len 10
     * @param mixed  $ignored Uses self::FIPN_SVM_ORDER_TOTAL_MAX_LEN
     * @return  string strval($orderTotal)
     */
    public static function validateOrderTotal($orderTotal, $ignored = false)
    {
        if (!is_float($orderTotal)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $orderTotal ('.$orderTotal
                .') must be a float');
        }
        return parent::validateOrderTotal(
                    $orderTotal,
                    self::FIPN_SVM_ORDER_TOTAL_MAX_LEN);
    }

    /**
     * Validates return URL
     * @param   string  $url
     * @param mixed  $ignored Uses self::FIPN_SVM_URL_MAX_LEN
     * @return  string $url
     */
    public static function validateReturnUrl($url, $ignored = false)
    {
        try {
            self::validateNoPipeChar($url);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of return url failed:'.$e->getRawMessage());
        }
        return parent::validateReturnUrl($url, self::FIPN_SVM_URL_MAX_LEN);
    }

    /**
     * Validates locale according to specs of Suomen Verkkomaksut
     * @param   string $locale 'fi_FI', 'en_US', 'sv_SE' and 'ru_RU'
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $locale
     */
    public static function validateLocale($locale)
    {
        $locale = parent::validateLocale($locale);
        if ($locale != self::FIPN_SVM_LOCALE_FI AND
            $locale != self::FIPN_SVM_LOCALE_EN AND
            $locale != self::FIPN_SVM_LOCALE_SV) {
            throw new Aivomatic_Fipn_Exception(
                '$locale ('.$locale.') must be '
                .self::FIPN_SVM_LOCALE_FI.' or '
                .self::FIPN_SVM_LOCALE_EN.' or '
                .self::FIPN_SVM_LOCALE_SV);
        }
        return $locale;
    }

    /**
     * Validates order description according to specs of Suomen Verkkomaksut
     * @param   string|int $orderId Alphanumeric, max. len. 65000.
     * @param mixed  $ignored Uses self::FIPN_SVM_DESC_MAX_LEN
     * @return  string $orderDesc
     */
    public static function validateOrderDesc($orderDesc, $ignored = false)
    {
        try {
            self::validateNoPipeChar($orderDesc);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of order description failed:'.$e->getRawMessage());
        }
        return parent::validateOrderDesc($orderDesc,
                                         self::FIPN_SVM_DESC_MAX_LEN);
    }

    /**
     * Basic validation of currency code
     * @param   string $currency 'eur' or 'euro' - case insensitive
     * @return  void
     */
    public static function validateCurrency($currency)
    {
        return parent::validateCurrency($currency);
    }

    /**
     * Basic validation of response request's GET-parameters array
     * @param string $type 'success', 'cancel'
     * @param   array $reqGetParamArray Requests GET-parameters as an array.
     * @param mixed  $ignored Uses self::$obligatoryResponseGetParams
     * @return  array $responseGetParams
     */
    public static function validateResponseGetParams(
        $type,
        $responseGetParams,
        $ignored = false)
    {
        return parent::validateResponseGetParams(
                    self::validateResponseStatus($type),
                    $responseGetParams,
                    self::$obligatoryResponseGetParams);
    }

    /**
     * Validate status parameter
     * Valid statues for Suomen Verkkomaksut are 'success' and 'cancel'.
     * @param string $status  Status parameter.
     * @param mixed  $ignored Uses self::$validStatues
     * @return string $status
     */
    public static function validateResponseStatus($status, $ignored = false)
    {
        return parent::validateResponseStatus($status, self::$validStatues);
    }

    /**
     * Validate timestamp
     * @param string $timestamp Unix timestamp
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $timestamp
     */
    public static function validateTimestamp($timestamp)
    {
        if (empty($timestamp)) {
            throw new Aivomatic_Fipn_Exception('$timestamp must not be empty');
        }
        if (!ctype_digit($timestamp)) {
            throw new Aivomatic_Fipn_Exception(
                '$timestamp ('.$timestamp.') must be all digits');
        }
        if ($timestamp != strval(intval($timestamp))) {
            throw new Aivomatic_Fipn_Exception(
                '$timestamp ('.$timestamp.') must be like an integer');
        }
        if (strlen($timestamp) != self::FIPN_SVM_TIMESTAMP_LEN) {
                throw new Aivomatic_Fipn_Exception(
                    '$timestamp must be exactly '.
                    self::FIPN_SVM_TIMESTAMP_LEN
                    .' (is '.strlen($timestamp).') characters long');
        }
        return strval($timestamp);
    }

    /**
     * Validate transaction Id
     * @param string $transactionId
     * @param mixed  $ignored Uses self::FIPN_SVM_TRANSACTION_ID_MAX_LEN
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $transactionId
     */
    public static function validateTransactionId($transactionId, $ignored = false)
    {
        return parent::validateTransactionId($transactionId,
            self::FIPN_SVM_TRANSACTION_ID_MAX_LEN);
    }

    /**
     * Basic validation of responseMac
     * @param   string|int $responseMac Alphanumeric Uppercase
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $responseMac
     */
    public static function validateResponseMac($responseMac)
    {
        return parent::validateResponseMac($responseMac);
    }

    /**
     * Basic validation of customer's first name
     * @param   string $firstName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $firstName
     */
    public static function validateFirstName($firstName)
    {
        try {
            self::validateName($firstName,
                               self::FIPN_SVM_FIRST_NAME_MAX_LEN,
                               self::FIPN_SVM_FIRST_NAME_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $firstName failed!: '.$e->getRawMessage());
        }
        return $firstName;
    }

    /**
     * Basic validation of customer's last name
     * @param   string $lastName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $lastName
     */
    public static function validateLastName($lastName)
    {
        try {
            self::validateName($lastName,
                               self::FIPN_SVM_LAST_NAME_MAX_LEN,
                               self::FIPN_SVM_LAST_NAME_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $lastName failed!: '.$e->getRawMessage());
        }
        return $lastName;
    }

    /**
     * Basic validation of company name
     * @param   string $companyName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $companyName
     */
    public static function validateCompanyName($companyName)
    {
        try {
            self::validateName($companyName,
                               self::FIPN_SVM_COMPANY_NAME_MAX_LEN,
                               self::FIPN_SVM_COMPANY_NAME_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $companyName failed!: '.$e->getRawMessage());
        }
        return $companyName;
    }

    /**
     * Basic validation of name (fisrtName or lastName)
     * @param  string $name
     * @param  int $maxLen Maximum length
     * @param  int $minLen Minimum length
     * @throws Aivomatic_Fipn_Exception If not valid.
     * @return string $name
     */
    public static function validateName($name, $maxLen, $minLen)
    {
        try {
            self::validateNoPipeChar($name);
            parent::validateString(
                $name,
                $maxLen,
                $minLen);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $name failed!:'.$e->getRawMessage());
        }
        return $name;
    }

    /**
     * Basic validation of address
     * @param   string $address
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $address
     */
    public static function validateAddress($address)
    {
        try {
            self::validateNoPipeChar($address);
            parent::validateString(
                $address,
                self::FIPN_SVM_ADDRESS_MAX_LEN,
                self::FIPN_SVM_ADDRESS_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $address failed!:'.$e->getRawMessage());
        }
        return $address;
    }

    /**
     * Basic validation of ZIP code
     * @param   string|int $zip
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string strval($zip)
     */
    public static function validateZip($zip)
    {
        if($zip=="") $zip="11111";
        if (!parent::isIntOrString($zip)) {
            throw new Aivomatic_Fipn_Exception(
                '$zip ('.$zip.') must be an integer or a string (is '.
                gettype($zip).')');
        }
        $zip = strval($zip);
        try {
            self::validateNoPipeChar($zip);
            parent::validateString(
                $zip,
                self::FIPN_SVM_ZIP_MAX_LEN,
                self::FIPN_SVM_ZIP_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $zip failed!:'.$e->getRawMessage());
        }
        /*if (!ctype_digit($zip)) {
            throw new Aivomatic_Fipn_Exception(
                '$zip ('.$zip.') must be all digits (0-9)');
        }*/
        return $zip;
    }

    /**
     * Basic validation of city name
     * @param   string $city
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $city
     */
    public static function validateCity($city)
    {
        try {
            self::validateNoPipeChar($city);
            parent::validateString(
                $city,
                self::FIPN_SVM_CITY_MAX_LEN,
                self::FIPN_SVM_CITY_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $city failed!:'.$e->getRawMessage());
        }
        return $city;
    }

    /**
     * Basic validation of country code
     * @param   string $country Case insensitive ISO-3166-1 alpha-2
     *                          two letter country code
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $country
     */
    public static function validateCountry($country)
    {
        return parent::validateISO3166_1Alpha2($country);
    }

    /**
     * Basic validation of customer's email address
     * @param   string $email
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $email
     */
    public static function validateEmail($email)
    {
        return parent::validateEmailAddress($email,
                                            self::FIPN_SVM_EMAIL_MAX_LEN);
    }

    /**
     * Basic validation of cell phone number
     * @param   string $cellPhone
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $cellPhone
     */
    public static function validateCellPhone($cellPhone)
    {
        try {
            self::validateNoPipeChar($cellPhone);
            parent::validateString(
                $cellPhone,
                self::FIPN_SVM_CELLPHONE_MAX_LEN,
                self::FIPN_SVM_CELLPHONE_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $cellPhone failed!:'.$e->getRawMessage());
        }
        return $cellPhone;
    }

    /**
     * Basic validation of wire (land line) phone number
     * @param   string $wirePhone
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $wirePhone
     */
    public static function validateWirePhone($wirePhone)
    {
        try {
            self::validateNoPipeChar($wirePhone);
            parent::validateString(
                $wirePhone,
                self::FIPN_SVM_WIREPHONE_MAX_LEN,
                self::FIPN_SVM_WIREPHONE_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $wirePhone failed!:'.$e->getRawMessage());
        }
        return $wirePhone;
    }

    /**
     * Basic validation of item name
     * @param   string $itemName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $itemName
     */
    public static function validateItemName($itemName)
    {
        try {
            self::validateNoPipeChar($itemName);
            parent::validateString(
                $itemName,
                self::FIPN_SVM_ITEM_NAME_MAX_LEN,
                self::FIPN_SVM_ITEM_NAME_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $itemName failed!:'.$e->getRawMessage());
        }
        return trim($itemName);
    }

    /**
     * Basic validation of item quantity
     * @param   float|int $itemQty
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  float|int $itemQty
     */
    public static function validateItemQty($itemQty)
    {
        try {
            parent::validateNumber($itemQty, self::FIPN_SVM_ITEM_QTY_MAX_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $itemQty failed!:'.$e->getRawMessage());
        }
        if ($itemQty == 0) {
            throw new Aivomatic_Fipn_Exception('$itemQty must not be zero');
        }
        if ($itemQty < 0) {
            throw new Aivomatic_Fipn_Exception('$itemQty ('.$itemQty.')'.
                'must not be negative');
        }
        return $itemQty;
    }

    /**
     * Basic validation of item price
     * @param   float $itemPrice
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  float $itemPrice
     */
    public static function validateItemUnitPrice($itemPrice)
    {
        if (!is_float($itemPrice)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $itemPrice ('.$itemPrice
                .') must be a float');
        }
        return parent::validateOrderTotal(
                    $itemPrice,
                    self::FIPN_SVM_ITEM_PRICE_MAX_LEN);
        return $itemPrice;
    }

    /**
     * Basic validation of item VAT class
     * @param   float $itemVatClass
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  float $itemVatClass
     */
    public static function validateItemVatClass($itemVatClass)
    {
        if (!is_float($itemVatClass)) {
            throw new Aivomatic_Fipn_Exception(
                'Strict type validation!'.' $itemVatClass ('.$itemVatClass
                .') must be a float');
        }
        try {
            return parent::validatePercentage(
                        $itemVatClass,
                        self::FIPN_SVM_ITEM_VAT_CLASS_MAX_VAL,
                        self::FIPN_SVM_ITEM_VAT_CLASS_MIN_VAL,
                        self::FIPN_SVM_ITEM_VAT_CLASS_MAX_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $itemVatClass failed!:'.$e->getRawMessage());
        }
    }

    /**
     * Basic validation of item SKU (stock keeping unit - ID code)
     * @param   string $itemSku
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $itemSku
     */
    public static function validateItemSku($itemSku)
    {
        if (is_bool($itemSku)) {
            throw new Aivomatic_Fipn_Exception(
                '$itemSku must not be a boolean');
        }
        if (!is_scalar($itemSku)) {
            throw new Aivomatic_Fipn_Exception(
                '$itemSku ('.$itemSku.') must be a scalar');
        }
        $itemSku = strval($itemSku);
        try {
            self::validateNoPipeChar($itemSku);
            parent::validateString(
                $itemSku,
                self::FIPN_SVM_ITEM_SKU_MAX_LEN,
                self::FIPN_SVM_ITEM_SKU_MIN_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $itemSku failed!:'.$e->getRawMessage());
        }
        return $itemSku;
    }

    /**
     * Basic validation of item type
     * @param   string|int $itemType
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $itemType
     */
    public static function validateItemType($itemType)
    {
        if (!parent::isIntOrString($itemType)) {
            throw new Aivomatic_Fipn_Exception(
                '$itemType ('.$itemType.') must be an integer or a string (is '.
                gettype($itemType).')');
        }
        $itemType = strval($itemType);
        if ($itemType != self::FIPN_SVM_ITEM_TYPE_PRODUCT AND
            $itemType != self::FIPN_SVM_ITEM_TYPE_SHIPPING AND
            $itemType != self::FIPN_SVM_ITEM_TYPE_HANDLING)
            throw new Aivomatic_Fipn_Exception(
                '$itemType ('.$itemType.') must be '
                .self::FIPN_SVM_ITEM_TYPE_PRODUCT.' or '
                .self::FIPN_SVM_ITEM_TYPE_SHIPPING.' or '
                .self::FIPN_SVM_ITEM_TYPE_HANDLING);
        return $itemType;
    }

    /**
     * Basic validation of preselected payment method
     *
     * 1 = Nordea
     * 2 = Osuuspankki
     * 3 = Sampo Pankki
     * 4 = Tapiola
     * 5 = Ålandsbanken
     * 6 = Handelsbanken
     * 7 = Säästöpankit, paikallisosuuspankit, Aktia, Nooa
     * 8 = Luottokunta
     * 9 = Paypal
     * 10 = S-Pankki
     * 11 = Klarna, Laskulla
     * 12 = Klarna, Osamaksulla
     *
     * @param int|string $method
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $method
     */
    public static function validatePreselectedMethod($method)
    {
        if (!parent::isIntOrString($method)) {
            throw new Aivomatic_Fipn_Exception(
                '$method ('.$method.') must be an integer or a string (is '.
                gettype($method).')');
        }
        $method = intval($method);
        if ($method < 1 OR $method > 30) {
            throw new Aivomatic_Fipn_Exception(
                '$method ('.$method.') must match PRESELECTED_METHOD at '.
                ' SVM WAPI documentation');
        }
        return strval($method);
    }

    /**
     * Basic validation of payment gateway mode
     *
     * 1 = Normal service
     * 2 = Bypassing Suomen Verkkomaksut payment method selection
     *
     * @param int|string $mode
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $mode
     */
    public static function validateGatewayMode($mode)
    {
        if (!parent::isIntOrString($mode)) {
            throw new Aivomatic_Fipn_Exception(
                '$mode ('.$mode.') must be an integer or a string (is '.
                gettype($mode).')');
        }
        $mode = intval($mode);
        if ($mode !== 1 AND $mode !== 2) {
            throw new Aivomatic_Fipn_Exception(
                '$mode ('.$mode.') must match MODE at '.
                ' SVM WAPI documentation');
        }
        return strval($mode);
    }

    /**
     * Basic validation of item discount
     * @param   float $itemDiscount
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  float $itemDiscount
     */
    public static function validateItemDiscount($itemDiscount)
    {
        try {
            return parent::validatePercentage(
                        $itemDiscount,
                        self::FIPN_SVM_ITEM_DISCOUNT_MAX_VAL,
                        self::FIPN_SVM_ITEM_DISCOUNT_MIN_VAL,
                        self::FIPN_SVM_ITEM_DISCOUNT_MAX_LEN);
        } catch (Aivomatic_Fipn_Exception $e) {
            throw new Aivomatic_Fipn_Exception(
                'Validation of $itemDiscount failed!:'.$e->getRawMessage());
        }
    }

    /**
     * Validates that a string doesn't contain a pipe (|) character
     * @param   mixed $string
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  string $string
     */
    public static function validateNoPipeChar($string)
    {
        $string = strval($string);
        if (strpos($string, '|') !== false) {
            throw new Aivomatic_Fipn_Exception(
                'String ('.$string.') must not contain "|" character ("pipe")');
        } else {
            return $string;
        }
    }

    /**
     * Compares order total and item row prices
     * @param  float $total Order total
     * @param  float $rows  Sum of item row prices including VATs and discounts
     * @throws Aivomatic_Fipn_Exception If sums do not match.
     * @return bool True if match within precision
     */
     public static function compareTotalToRows($total, $rows)
     {
        if (!Aivomatic_Fipn_Validator::compareSums(
            $total, $rows, self::FIPN_SVM_TOTAL_SUM_PRECISION)) {
            throw new Aivomatic_Fipn_Exception(
                'Order total ('.$total.') and sum of item'.
                ' rows ('.$rows.') do not match within precision '.
                '('.self::FIPN_SVM_TOTAL_SUM_PRECISION.')');
        }
        return true;
     }
}
?>