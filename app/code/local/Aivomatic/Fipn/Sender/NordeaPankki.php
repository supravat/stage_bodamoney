<?php
/**
 * Aivomatic_Fipn_Sender_NordeaPankki
 *
 * @category    Aivomatic
 * @package     Fipn_NordeaPankki
 * @version     ver. 1.8.1 - $Id: NordeaPankki.php 968 2009-09-30 07:44:09Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Sender/NordeaPankki.php $
 * @copyright   2009 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require sender base class (../Sender.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Require validator class
 */
require_once dirname(dirname(__FILE__)).'/Validator/NordeaPankki.php';

/**
 * Aivomatic Fipn Sender for Nordea Pankki
 *
 * @category    Aivomatic
 * @package     Fipn_NordeaPankki
 * @since       0.2.0+
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Sender_NordeaPankki extends Aivomatic_Fipn_Sender
{
    /**
     * URL for HTML-form POST-method
     * @var string
     */
    protected $postUrl = 'https://solo3.nordea.fi/cgi-bin/SOLOPM01';

    /**
     * Constructor
     *
     * @param   void
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Validates and sets majority of data
     *
     * @param   string $secretMerchantId
     * @param   string $publicMerchantId    RCV_ID
     * @param   string $orderId             STAMP
     * @param   float  $orderTotal          AMOUNT
     * @param   string $currency            CUR
     * @param   string $urlSuccess          RETURN
     * @param   string $urlCancel           CANCEL
     * @param   string $urlError            REJECT
     * @param   string $keyVersion          KEYVERS
     * @param   string $locale              LANGUAGE
     * @param   string $orderDesc           MSG
     * @param   string $bankAccount         RCV_ACCOUNT
     * @param   string $merchantName        RCV_NAME
     * @return  void
     */
    public function setData($secretMerchantId,
                            $publicMerchantId,
                            $orderId,
                            $orderTotal,
                            $currency,
                            $urlSuccess,
                            $urlCancel,
                            $urlError,
                            $keyVersion,
                            $locale = '',
                            $orderDesc = '',
                            $bankAccount = '',
                            $merchantName = '')
    {

        $this->secretMerchantId =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateSecretMerchantId($secretMerchantId);

        $this->publicMerchantId =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validatePublicMerchantId($publicMerchantId);

        $this->orderId =
            Aivomatic_Fipn_Validator_NordeaPankki::validateOrderId($orderId);

        $this->orderTotal =
            self::formatAmount(Aivomatic_Fipn_Validator_NordeaPankki::
                               validateOrderTotal($orderTotal));

        $this->currency =
            Aivomatic_Fipn_Validator_NordeaPankki::validateCurrency($currency);

        $this->successUrl =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateReturnUrl($urlSuccess).
            $this->getUrlGetParams('success');

        $this->cancelUrl =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateReturnUrl($urlCancel).
            $this->getUrlGetParams('cancel');

        $this->errorUrl =
            Aivomatic_Fipn_Validator_NordeaPankki::validateReturnUrl($urlError).
            $this->getUrlGetParams('error');

        $this->keyVersion =
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateKeyVersion($keyVersion);

        if (!empty($locale)) {
            $this->locale =
                Aivomatic_Fipn_Validator_NordeaPankki::validateLocale($locale);
        } else {
            $this->locale = false;
        }

        if (!empty($orderDesc)) {
            $this->orderDesc =
                Aivomatic_Fipn_Validator_NordeaPankki::
                validateOrderDesc($orderDesc);
        } else {
            $this->orderDesc = false;
        }

        if (!empty($bankAccount)) {
            $this->bankAccount =
                Aivomatic_Fipn_Validator_NordeaPankki::
                validateBankAccount($bankAccount);
        } else {
            $this->bankAccount = false;
        }

        if (!empty($merchantName)) {
            $this->merchantName =
                Aivomatic_Fipn_Validator_NordeaPankki::
                validateMerchantName($merchantName);
        } else {
            $this->merchantName = false;
        }

        $this->makeBankReference();
    }

    /**
     * Makes and sets bank reference number
     * Formed according to Finnish bankers' association's standard exept no
     * splitting to five digit groups.
     * @param void
     * @return void
     */
    protected function makeBankReference()
    {
        $orderId = ltrim($this->getOrderId(), '0');
        $this->bankReference = $orderId.
            self::calculateFbaBankRefCheckSum($orderId);
    }

    /**
     * Formats amount (order total)
     * Formats decimal number for Nordea Pankki. No thousands separator, comma
     * as decimal point, two decimals
     * @param $amount string
     * @return string Formated amount
     */
    public static function formatAmount($amount)
    {
        return number_format(floatval($amount), 2, ',', '');
    }

    /**
     * Add string to the end of order description
     *
     * @param   string  $string String to be added
     * @param   bool    $newLine If true (default) a new line character is added
     *                           to the BEGINNING of string
     * @param   mixed   $ignored Not needed (strict interface compatibility)
     * @return  bool    True on success, false on failure
     */
    public function addToOrderDesc($string, $newLine = true, $ignored = false)
    {
        $orderDesc = trim($this->orderDesc);
        if ($newLine) {
            $orderDesc = $orderDesc.chr(10);
        }
        $newRows = str_split(
            $string,
            Aivomatic_Fipn_Validator_NordeaPankki::
            FIPN_NORDEAP_DESC_MAX_LINE_LEN);
        foreach ($newRows as $row) {
            $orderDesc = $orderDesc.$row.chr(10);
        }
        $orderDesc = trim($orderDesc);
        try {
            Aivomatic_Fipn_Validator_NordeaPankki::
            validateOrderDesc($orderDesc);
        } catch (Aivomatic_Fipn_Exception $e) {
            return false;
        }
        $this->orderDesc = $orderDesc;
        return true;
    }

    /**
     * Return form fields
     *
     * @param void
     * @return array Key = field name, Value = field value
     */
    public function getFormFields()
    {
        $fields = array(
            'RCV_ID'    => $this->publicMerchantId,
            'AMOUNT'    => $this->orderTotal,
            'STAMP'     => $this->orderId,
            'RETURN'    => $this->successUrl,
            'CANCEL'    => $this->cancelUrl,
            'REJECT'    => $this->errorUrl,
            'CUR'       => 'EUR',
            'MAC'       => $this->getMac(),
            'CONFIRM'   => 'YES',
            'VERSION'   => Aivomatic_Fipn_Validator_NordeaPankki::
                           FIPN_NORDEAP_WAPI_VERSION,
            'DATE'      => 'EXPRESS',
            'KEYVERS'   => $this->keyVersion,
            'REF'       => $this->getBankReference()
            );
        if ($this->locale) {
            $fields['LANGUAGE'] = $this->getLocale();
        }
        if ($this->orderDesc) {
            $fields['MSG'] = $this->getOrderDesc();
        }
        if ($this->getBankAccount()) {
            $fields['RCV_ACCOUNT'] = $this->getBankAccount();
        }
        if ($this->getMerchantName()) {
            $fields['RCV_NAME'] = $this->getMerchantName();
        }
        return $fields;
    }

    /**
     * Returns MAC (message authentication code)
     *
     * @param void
     * @return string MAC
     */
    public function getMac()
    {
        $this->mac = $this->calculateMac(
            Aivomatic_Fipn_Validator_NordeaPankki::FIPN_NORDEAP_WAPI_VERSION.'&'.
            $this->getOrderId().'&'.
            $this->getPublicMerchantId().'&'.
            $this->getOrderTotal().'&'.
            $this->getBankReference().'&'.
            'EXPRESS&'.
            'EUR&'.
            $this->getSecretMerchantId().'&'
            );
        return $this->mac;
    }

    /**
     * Get GET-parameters for success, cancel or error URLs
     *
     * 'fipnstatus=success&fipnorderid=12345&fipnpgw=nameofpaymentgateway
     * @param string $type success, cancel or error
     * @return string GET-parameters
     */
    protected function getUrlGetParams($type)
    {
        return 'fipnstatus='.$type.'&fipnpgw=nordeapankki';
    }

    /**
     * Calculates check sum for bank reference number
     *
     * Check sum is calculated according to Finnish Banker's Assosiation
     * standard, but lenght of its base can be shorter. Official shortest lenght
     * for bank reference number is 4, but Nordea accepts 2.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 1-19 characters.
     * @throws Aivomatic_Fipn_Exception If base is not valid.
     * @return string Single digit check sum
     */
    public static function calculateFbaBankRefCheckSum($base)
    {
        $base = Aivomatic_Fipn_Validator_NordeaPankki::validateFbaBankRefBase($base);
        $baseNums = array_reverse(str_split($base));
        $weight = 0;
        $sum = 0;
        foreach ($baseNums as $num) {
            switch ($weight) {
            case 0:
                $sum += 7 * $num;
                $weight = 1;
                break;
            case 1:
                $sum += 3 * $num;
                $weight = 2;
                break;
            case 2:
                $sum += 1 * $num;
                $weight = 0;
                break;
            }
        }
        $rounded = round($sum, -1);

        //Tulot lasketaan yhteen ja summa vähennetään *seuraavasta* täydestä
        // kymmenestä.
        if ($rounded < $sum) {
            $rounded += 10;
        }
        return (string)($rounded-$sum);
    }

    /**
     * Textual presentation of object's properties
     * @param   void
     * @return  string
     */
    public function __toString()
    {
        return
        parent::__toString().
        __CLASS__."::\n".
        'wapiType:              '.Aivomatic_Fipn_Validator_NordeaPankki::
                                  FIPN_NORDEAP_WAPI_VERSION."\n".
        'keyVersion:            '.$this->getKeyVersion()."\n";
    }
}
?>