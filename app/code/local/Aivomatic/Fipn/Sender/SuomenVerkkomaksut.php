<?php
/**
 * Aivomatic_Fipn_Sender_SuomenVerkkomaksut
 *
 * @category    Aivomatic
 * @package     Fipn_SuomenVerkkomaksut
 * @version     ver. 1.10.2 - $Id: SuomenVerkkomaksut.php 1726 2013-01-30 05:46:21Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Sender/SuomenVerkkomaksut.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require sender base class (../Sender.php)
 */
require_once dirname(__FILE__).'.php';

/**
 * Require validator class
 */
require_once dirname(dirname(__FILE__)).'/Validator/SuomenVerkkomaksut.php';

/**
 * Aivomatic Fipn Sender for Suomen Verkkomaksut eFactoring payment gateway
 *
 * @category    Aivomatic
 * @package     Fipn_SuomenVerkkomaksut
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Sender_SuomenVerkkomaksut extends Aivomatic_Fipn_Sender
{
    /**
     * URL for HTML-form POST-method
     * @var string
     */
    //protected $postUrl = 'https://ssl.verkkomaksut.fi/payment.svm';
    protected $postUrl = 'https://payment.verkkomaksut.fi/';

    /**
     * Notify URL
     *
     * URL to be called when the payment has been marked as paid by Suomen
     * Verkkomaksut. Ie. SVM 'notifies' merchant by calling this URL.
     *
     * @see getUrlGetParams()
     * @see Aivomatic_Fipn_Validator::validateReturnUrl()
     * @var string
     */
    protected $notifyUrl = '';

    /**
     * First name of customer
     *
     * @var string
     */
    protected $firstName = '';

    /**
     * Last name of customer
     *
     * @var string
     */
    protected $lastName = '';

    /**
     * Address of customer
     *
     * @var string
     */
    protected $address = '';

    /**
     * ZIP code of customer
     *
     * @var string
     */
    protected $zip = '';

    /**
     * Name of customer's city
     *
     * @var string
     */
    protected $city = '';

    /**
     * ISO-3166-1 country code of customer's country
     *
     * @var string
     */
    protected $country = '';

    /**
     * Customer's email address
     *
     * @var string
     */
    protected $email = '';

    /**
     * Customer's cell phone number
     *
     * @var string
     */
    protected $cellPhone = '';

    /**
     * Customer's wire phone number (land line)
     *
     * @var string
     */
    protected $wirePhone = '';

    /**
     * Name of customer's company
     *
     * @var string
     */
    protected $companyName = '';

    /**
     * Array of order items
     *
     * @var array
     */
    protected $items = array();

    /**
     * Does the item row price include VAT?
     * False = price doesn't include VAT, True = VAT is included
     *
     * @var bool
     */
    protected $priceIncludesVat = false;

    /**
     * Status of order data
     *
     * True if order data is set and ready for WAPI v. S1 or E1
     * @var boolean
     */
    protected $orderDataOk = false;

    /**
     * Status of customer data
     *
     * True if customer data is set and ready for WAPI v. E1
     * @var boolean
     */
    protected $customerDataOk = false;

    /**
     * Status of items data
     *
     * True if at least one item has been set and thus ready for WAPI v. E1
     * @var boolean
     */
    protected $itemsDataOk = false;

    /**
     * Preselected payment method
     *
     * @var string|null
     */
    protected $preselectedMethod = '';

    /**
     * Payment gateway mode
     *
     * @var string
     */
    protected $gatewayMode = '1';


    /**
     * Constructor
     *
     * @param   void
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Validates and sets order data
     *
     * @param   string $secretMerchantId
     * @param   string $publicMerchantId
     * @param   string $orderId
     * @param   float  $orderTotal Including VAT.
     * @param   string $currency
     * @param   string $urlSuccess
     * @param   string $urlCancel
     * @param   string $urlNotify
     * @param   string $urlDelay
     * @param   string $locale
     * @param   string $orderDesc
     * @return  void
     */
    public function setOrderData($secretMerchantId,
                                 $publicMerchantId,
                                 $orderId,
                                 $orderTotal,
                                 $currency,
                                 $urlSuccess,
                                 $urlCancel,
                                 $urlNotify,
                                 $urlDelay,
                                 $locale =
                                     Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                                     FIPN_SVM_LOCALE_FI,
                                 $orderDesc = '',
                                 $priceIncludesVat = false,
                                 $preselectedMethod = '',
                                 $gatewayMode = '1')
    {

        $this->secretMerchantId =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateSecretMerchantId($secretMerchantId);

        $this->publicMerchantId =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validatePublicMerchantId($publicMerchantId);

        $this->orderId =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateOrderId($orderId);

        $this->orderTotal =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateOrderTotal($orderTotal);

        Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateCurrency($currency);
            $this->currency = 'EUR';

        $this->successUrl =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateReturnUrl($urlSuccess).
            $this->getUrlGetParams('success');

        $this->cancelUrl =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateReturnUrl($urlCancel).
            $this->getUrlGetParams('cancel');

        $this->notifyUrl =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateReturnUrl($urlNotify).
            $this->getUrlGetParams('notify');

        $this->delayUrl = '';
            /*Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateReturnUrl($urlDelay).
            $this->getUrlGetParams('delay');*/

        $this->locale =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateLocale($locale);

        if (!empty($orderDesc)) {
            $this->setOrderDesc(
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateOrderDesc(self::removePipe($orderDesc)));
        } else {
            $this->orderDesc = false;
        }

        if (empty($preselectedMethod)) {
            $this->preselectedMethod = '';
        } else {
            $this->preselectedMethod =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validatePreselectedMethod($preselectedMethod);
        }

        if (empty($gatewayMode)) {
            $this->gatewayMode = '1';
        } else {
            $this->gatewayMode =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateGatewayMode($gatewayMode);
        }

        if (empty($priceIncludesVat)) {
            $this->priceIncludesVat = false;
        } else {
            $this->priceIncludesVat = true;
        }

        $this->orderDataOk = true;
    }

    /**
     * Validates and sets customer data
     *
     * @param   string $firstName
     * @param   string $lastName
     * @param   string $address
     * @param   float  $zip
     * @param   string $city
     * @param   string $country
     * @param   string $email
     * @param   string $cellPhone
     * @param   string|bool $wirePhone
     * @param   string|bool $companyName
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  void
     */
    public function setCustomerData($firstName,
                                    $lastName,
                                    $address,
                                    $zip,
                                    $city,
                                    $country,
                                    $email,
                                    $cellPhone,
                                    $wirePhone = false,
                                    $companyName = false)
    {

        $this->firstName =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateFirstName(self::removePipe($firstName));

        $this->lastName =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateLastName(self::removePipe($lastName));

        $this->address =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateAddress(self::removePipe($address));

        $this->zip =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateZip($zip);

        $this->city =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateCity(self::removePipe($city));

        $this->country =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateCountry($country);

        $this->email =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateEmail($email);

        $this->cellPhone =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateCellPhone(self::removePipe($cellPhone));

        if ($wirePhone != false) {
            $this->wirePhone =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateWirePhone(self::removePipe($wirePhone));
        }

        if ($companyName != false) {
            $this->companyName =
                Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
                validateCompanyName(self::removePipe($companyName));
        }

        $this->customerDataOk = true;
    }

    /**
     * Adds and validates an order item
     *
     * @param   string     $itemName      Name of product
     * @param   int        $itemQty       Number of products
     * @param   float|int  $itemUnitPrice Positive or negative.
     * @param   float|int  $itemVatClass  VAT percentage
     * @param   string     $itemSku       Product code, ie. Stock Keeping Unit
     * @param   string|int $itemType      1=product, 2=shipping, 3=handling
     * @param   int|float  $itemDiscount  Discount percentage
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return  void
     */
    public function addItem($itemName,
                            $itemQty,
                            $itemUnitPrice,
                            $itemVatClass,
                            $itemSku = false,
                            $itemType = 1,
                            $itemDiscount = false)
    {
        $itemName =
        Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
        validateItemName(self::formatItemName($itemName));

        $itemQty =
        Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
        validateItemQty($itemQty);

        $itemUnitPrice =
        Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
        validateItemUnitPrice($itemUnitPrice);

        $itemVatClass =
        Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
        validateItemVatClass($itemVatClass);

        if (!Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
             isEmptyButNotZero($itemSku)) {
            $itemSku =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateItemSku(self::removePipe($itemSku));
        } else {
            $itemSku = false;
        }

        if ($itemDiscount !== 1) {
            $itemType =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateItemType($itemType);
        }

        if ($itemDiscount) {
            $itemDiscount =
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            validateItemDiscount($itemDiscount);
        } else {
            $itemDiscount = '0';
        }

        $this->items[] = array('name'      => $itemName,
                               'qty'       => $itemQty,
                               'unitPrice' => $itemUnitPrice,
                               'vatClass'  => $itemVatClass,
                               'sku'       => $itemSku,
                               'type'      => $itemType,
                               'discount'  => $itemDiscount);

        $this->itemsDataOk = true;
    }

    /**
     * Returns number of item rows of order
     * @param void
     * @return int
     */
    public function getNumOfItemRows()
    {
        return count($this->items);
    }

    /**
     * Returns item rows as ascii table
     * @param void
     * @return string
     */
    public function getItemsAsAsciiTable()
    {
        if ($this->getNumOfItemRows() === 0) {
            return '  No item rows';
        }
        $str = '';
        foreach ($this->items as $key => $item) {
            $str .=
            '  item['.$key.']'."\n".
            '    name:      '.$item['name']."\n".
            '    qty:       '.$item['qty']."\n".
            '    unitPrice: '.$item['unitPrice']."\n".
            '    vatClass:  '.$item['vatClass']."\n".
            '    sku:       '.$item['sku']."\n".
            '    type:      '.$item['type']."\n".
            '    discount:  '.$item['discount']."\n";
        }
        return $str;
    }

    /**
     * Returns items as an array
     * @param void
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add string to the end of order description
     *
     * @param   string  $string String to be added
     * @param   bool    $newLine If true (default) a new line character is added
     *                           to the end of string
     * @param   mixed   $ignored Not needed (strict interface compatibility)
     * @see Aivomatic_Fipn_Validator_SuomenVerkkomaksut::FIPN_SVM_DESC_MAX_LEN
     * @return  bool True on success, false on failure
     */
    public function addToOrderDesc($string, $newLine = true, $ignored = false)
    {
        if (!parent::addToOrderDesc(
            $string,
            $newLine,
            Aivomatic_Fipn_Validator_SuomenVerkkomaksut::FIPN_SVM_DESC_MAX_LEN)) {
            return false;
        }
        //$this->orderDesc = str_replace("\n", '', nl2br($this->orderDesc));
        $this->setOrderDesc($this->orderDesc);
        return true;
    }

    /**
     * Sets order desciption
     * New line characters are replaced with <br />'s and stripped.
     * @param string $orderDesc
     * @return void
     */
    protected function setOrderDesc($orderDesc)
    {
        $this->orderDesc = str_replace(array("\r\n", "\r", "\n"), '', nl2br($orderDesc));
    }

    /**
     * Determines and returns WAPI version number
     * Currently S1 or E1.
     * @param void
     * @throws  Aivomatic_Fipn_Exception If not valid.
     * @return string WAPI version number, S1 (simple) or E1 (extended).
     */
    public function getWapiVersion()
    {
        if ($this->orderDataOk AND
            $this->customerDataOk AND
            $this->itemsDataOk) {
            return 'E1';
        } elseif ($this->orderDataOk) {
            return 'S1';
        } else {
            throw new Aivomatic_Fipn_Exception(
                'Order, customer or items data not set');
        }
    }

    /**
     * Returns form fields
     *
     * @param void
     * @return array Key = field name, Value = field value
     */
    public function getFormFields()
    {
        if ($this->getWapiVersion() == 'S1') {
            return $this->getFormFieldsWapiS1();
        } else {
            return $this->getFormFieldsWapiE1();
        }
    }

    /**
     * Return form fields for WAPI version S1
     *
     * @param void
     * @return array Key = field name, Value = field value
     */
    public function getFormFieldsWapiS1()
    {
        $fields = array(
            'MERCHANT_ID'       => $this->publicMerchantId,
            'AMOUNT'            => $this->orderTotal,
            'ORDER_NUMBER'      => $this->orderId,
            'RETURN_ADDRESS'    => $this->successUrl,
            'CANCEL_ADDRESS'    => $this->cancelUrl,
            'NOTIFY_ADDRESS'    => $this->notifyUrl,
            //'PENDING_ADDRESS'   => $this->delayUrl,
            'CURRENCY'          => $this->currency,
            'TYPE'              => 'S1',
            'MODE'              => $this->gatewayMode,
            'AUTHCODE'          => $this->getMacWapiS1(),
            'CULTURE'           => $this->locale
            );
        if ($this->orderDesc) {
            $fields['ORDER_DESCRIPTION'] = $this->orderDesc;
        }
        if (!empty($this->preselectedMethod)) {
            $fields['PRESELECTED_METHOD'] = $this->preselectedMethod;
        }
        return $fields;
    }

    /**
     * Return form fields for WAPI version E1
     *
     * @param void
     * @return array Key = field name, Value = field value
     */
    public function getFormFieldsWapiE1()
    {
        $fields = array(
            'MERCHANT_ID'       => $this->publicMerchantId,
            'ORDER_NUMBER'      => $this->orderId,
            'RETURN_ADDRESS'    => $this->successUrl,
            'CANCEL_ADDRESS'    => $this->cancelUrl,
            'NOTIFY_ADDRESS'    => $this->notifyUrl,
            //'PENDING_ADDRESS'   => $this->delayUrl,
            'CURRENCY'          => $this->currency,
            'TYPE'              => 'E1',
            'MODE'              => $this->gatewayMode,
            'INCLUDE_VAT'       => $this->getPriceIncludesVatAsString(),
            'AUTHCODE'          => $this->getMacWapiE1(),
            'CULTURE'           => $this->locale
            );
        if ($this->orderDesc) {
            $fields['ORDER_DESCRIPTION'] = $this->orderDesc;
        }
        if (!empty($this->preselectedMethod)) {
            $fields['PRESELECTED_METHOD'] = $this->preselectedMethod;
        }
        $fields['CONTACT_CELLNO'] = $this->cellPhone;
        $fields['CONTACT_EMAIL'] = $this->email;
        $fields['CONTACT_FIRSTNAME'] = $this->firstName;
        $fields['CONTACT_LASTNAME'] = $this->lastName;
        $fields['CONTACT_ADDR_STREET'] = $this->address;
        $fields['CONTACT_ADDR_ZIP'] = $this->zip;
        $fields['CONTACT_ADDR_CITY'] = $this->city;
        $fields['CONTACT_ADDR_COUNTRY'] = $this->country;
        $fields['ITEMS'] = strval($this->getNumOfItemRows());
        if ($this->wirePhone) {
            $fields['CONTACT_TELNO'] = $this->wirePhone;
        }
        if ($this->companyName) {
            $fields['CONTACT_COMPANY'] = $this->companyName;
        }

        //$rowsSum = 0.0;
        foreach ($this->items as $key => $item) {
            $fields['ITEM_TITLE['.$key.']'] = $item['name'];
            $fields['ITEM_AMOUNT['.$key.']'] = strval($item['qty']);
            $fields['ITEM_PRICE['.$key.']'] = $item['unitPrice'];
            $fields['ITEM_TAX['.$key.']'] = $item['vatClass'];
            $fields['ITEM_DISCOUNT['.$key.']'] = $item['discount'];
            $fields['ITEM_TYPE['.$key.']'] = $item['type'];
            if ($item['sku']) {
                $fields['ITEM_NO['.$key.']'] = $item['sku'];
            }
            /*$rowsSum =
                $rowsSum + self::calculateRowPrice($item['qty'],
                                                   $item['unitPrice'],
                                                   $item['vatClass'],
                                                   $item['discount']);*/
        }
        //$rowsSum = round($rowsSum, 2);
        /*Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            compareTotalToRows($this->orderTotal, $rowsSum);*/
        Aivomatic_Fipn_Validator_SuomenVerkkomaksut::
            compareTotalToRows($this->getOrderTotal(), $this->getRowSum());
        return $fields;
    }

    /**
     * Return total sum of item rows
     * @param void
     * @return float Total sum of item rows including VAT and discount
     */
    public function getRowSum()
    {
        $rowSum = 0.0;
        foreach ($this->items as $key => $item) {
            $rowSum = $rowSum + self::calculateRowPrice(
                $item['qty'],
                $item['unitPrice'],
                $item['vatClass'],
                $item['discount']
            );
        }
        return $rowSum;
    }

    /**
     * Returns rounding diffrence between order total and total of row sums
     * @param void
     * @return float
     */
    public function getRoundingDiff()
    {
        return $this->getOrderTotal()-$this->getRowSum();
    }

    /**
     * Returns total sum of an item row
     * @param int   $qty      Quantity - number of items
     * @param float $price    Price of single item without VAT
     * @param float $vat      VAT percentage
     * @param float $discount Discount percentage
     * @return float Total sum including VAT and discount
     */
    public function calculateRowPrice($qty, $price, $vat, $discount)
    {
        if ($this->priceIncludesVat) {
            return $qty*$price*((100-$discount)/100);
        } else {
            return $qty*($price*((100+$vat)/100))*((100-$discount)/100);
        }
    }

    /**
     * Returns priceIncludesVat
     * @param void
     * @return bool
     */
    public function getPriceIncludesVat() {
        return $this->priceIncludesVat;
    }

    /**
     * Returns priceIncludesVat as a string according to specs of Suomen Verkkomaksut
     * If VAT is included, returns '1', if VAT is not included, returns '0'
     * @param void
     * @return string True = '1', false = '0'
     */
    public function getPriceIncludesVatAsString() {
        if ($this->getPriceIncludesVat()) {
            return '1';
        } else {
            return '0';
        }
    }

    /**
     * Returns a string for which the MAC can be calculated for in WAPI v. S1
     * See SVM WAPI-specification for details.
     * @param void
     * @return string
     */
    protected function getMacWapiS1()
    {
        $string =
        $this->getSecretMerchantId().'|'.
        $this->getPublicMerchantId().'|'.
        $this->getOrderTotal().'|'.
        $this->getOrderId().'|'.
        '|';                                //Bank ref. num. not implemented

        if ($this->orderDesc) {
            $string .= $this->orderDesc;
        }

        $string .= '|'.
        $this->currency.'|'.
        $this->successUrl.'|'.
        $this->cancelUrl.'|'.
        $this->delayUrl.'|'.
        $this->notifyUrl.'|'.
        'S1|'.
        $this->locale.'|'.
        $this->preselectedMethod.'|'.
        $this->gatewayMode.'|'.
        '|';                                //VISIBLE_METHODS and GROUP not implemented

        $this->macBaseString = $string;
        $this->mac = $this->calculateMac($this->macBaseString);
        return $this->mac;
    }

    /**
     * Returns a string for which the MAC can be calculated for in WAPI v. E1
     * See SVM WAPI-specification for details.
     * @param void
     * @return string
     */
    protected function getMacWapiE1()
    {
        //Almost Same as in WAPI S1
        $string =
        $this->getSecretMerchantId().'|'.
        $this->getPublicMerchantId().'|'.   //NOTE orderId is not included in WAPI 5
        $this->getOrderId().'|'.
        '|';                                //Bank ref. num. not implemented

        if ($this->orderDesc) {
            $string .= $this->orderDesc;
        }

        $string .= '|'.
        $this->currency.'|'.
        $this->successUrl.'|'.
        $this->cancelUrl.'|'.
        $this->delayUrl.'|'.
        $this->notifyUrl.'|'.
        'E1|'.
        $this->locale.'|'.
        $this->preselectedMethod.'|'.
        $this->gatewayMode.'|'.
        '||';                                //VISIBLE_METHODS and GROUP not implemented

        //WAPI E1 specifics
        if ($this->wirePhone) {
            $string .= $this->wirePhone;
        }
        $string .= '|'.
        $this->cellPhone.'|'.
        $this->email.'|'.
        $this->firstName.'|'.
        $this->lastName.'|';

        if ($this->companyName) {
            $string .= $this->companyName;
        }
        $string .= '|'.
        $this->address.'|'.
        $this->zip.'|'.
        $this->city.'|'.
        $this->country.'|'.
        $this->getPriceIncludesVatAsString().'|'.//'0&'.
        $this->getNumOfItemRows();

        foreach ($this->items as $key => $item) {
            $string .= '|'.$item['name'].'|';
            if ($item['sku']) {
                $string .= $item['sku'];
            }
            $string .= '|'.
            $item['qty'].'|'.
            $item['unitPrice'].'|'.
            $item['vatClass'].'|'.
            $item['discount'].'|'.
            $item['type'];
        }

        $this->macBaseString = $string;
        $this->mac = $this->calculateMac($this->macBaseString);
        return $this->mac;
    }

    /**
     * Get GET-parameters for success, cancel or error URLs
     *
     * 'fipnstatus=success&fipnorderid=12345&fipnpgw=nameofpaymentgateway
     * @param string $type success, cancel or error
     * @return string GET-parameters
     */
    protected function getUrlGetParams($type)
    {
        return 'fipnstatus='.$type.
               '&fipnpgw=suomenverkkomaksut'.
               '&fipnorderid='.$this->getOrderId();
    }

    /**
     * Returns formated item name
     *
     * Double quotation mark in item name fraks MAC-calculation. Thus it is
     * changed to two single quotation marks.
     * @param string $name Item name
     * @return string
     */
    public static function formatItemName($name)
    {
        return self::removePipe(str_replace('"', "''", $name));
    }

    /**
     * Removes pipe character (|) from string
     *
     * @param string $string
     * @return string
     */
    public static function removePipe($string)
    {
        return str_replace('|', "", $string);
    }

    /**
     * Get notify URL
     * @uses $notifyUrl
     * @param  void
     * @return string notifyUrl
     */
    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }

    /**
     * Textual presentation of object's properties
     * @param   void
     * @return  string
     */
    public function __toString()
    {
        $wapi = '';
        try {
            $wapi = $this->getWapiVersion();
        } catch (Aivomatic_Fipn_Exception $e) { }

        $macBase = substr_replace($this->getMacBaseString(),
                                str_repeat('x', 30),
                                0,
                                30);
        $string =
        parent::__toString().
        __CLASS__."::\n".
        'notifyUrl:             '.$this->getNotifyUrl()."\n".
        'WAPI version:          '.$wapi."\n".
        'macBase:               '.$macBase."\n".
        'firstName:             '.$this->firstName."\n".
        'lastName:              '.$this->lastName."\n".
        'address:               '.$this->address."\n".
        'zip:                   '.$this->zip."\n".
        'city:                  '.$this->city."\n".
        'country:               '.$this->country."\n".
        'email:                 '.$this->email."\n".
        'cellPhone:             '.$this->cellPhone."\n".
        'wirePhone:             '.$this->wirePhone."\n".
        'companyName:           '.$this->companyName."\n".
        'numOfItemRows:         '.$this->getNumOfItemRows()."\n".
        'rowSum:                '.$this->getRowSum()."\n".
        'roundingDiff:          '.$this->getRoundingDiff()."\n".
        'priceIncludesVat:      '.$this->getPriceIncludesVatAsString()."\n".
        'preselectedMethod:     '.$this->preselectedMethod."\n".
        'gatewayMode:           '.$this->gatewayMode."\n".
        'Items:'."\n".
        $this->getItemsAsAsciiTable();

        return $string;
    }
}
?>