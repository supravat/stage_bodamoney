<?php
/**
 * Aivomatic_Fipn_Exception
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Exception.php 1107 2009-11-26 14:14:38Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn/Exception.php $
 * @copyright   2009 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Factory class
 */
require_once 'Factory.php';

/**
 * Aivomatic_Fipn_Exception
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @since       0.2.0+
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn_Exception extends Exception
{

    /**
     * Unsanitized 'raw' error message
     *
     * May contain direct user input.
     * @var string
     */
    protected $rawMessage;

    /**
     * Constructor
     *
     * Extends standard Exception, but message string is obligatory. Separate
     * getRawMessage method is added.
     * @uses setRawMessage()
     * @see getRawMessage()
     * @param mixed $message
     * @param int $code
     * @return void
     */
    public function __construct($message, $code = 1)
    {
        parent::__construct(self::clean($message), $code);
        $this->setRawMessage($message);
    }

    /**
     * Set raw error message
     *
     * @uses $rawMessage
     * @param mixed
     * @return void
     */
    protected function setRawMessage($message)
    {
        $this->rawMessage = strval($message);
    }

    /**
     * Get unsanitized 'raw' error message
     *
     * Raw error message may contain direct user input, which may be dangerous
     * to output eg. to a browser or a database. Use with caution.
     * @uses $rawMessage
     * @param void
     * @return string Unsanitized 'raw' error message
     */
    public function getRawMessage()
    {
        return $this->rawMessage;
    }

    /**
     * Remove potentially dangerous strings from error message
     *
     * Not very sophisticated (might produce badly mangled strings depending on
     * output media) but safe.
     * @see addslashes()
     * @see htmlentities()
     * @see function_exists()
     * @param mixed $mixed
     * @return string
     */
    protected static function clean($mixed)
    {
        $encoding = Aivomatic_Fipn_Factory::getMbString()->getInternalEncoding();
        return htmlentities(addslashes(strval($mixed)), ENT_QUOTES, $encoding);
    }
}
?>