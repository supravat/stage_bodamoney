<?php
/**
 * @category   Aivomatic
 * @package    AmcMgDdPp
 * @version    ver. 0.2.1 - $Id: Amcmgddpp.php 1456 2010-11-12 12:10:29Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/amcmgddpp/trunk/base/app/code/local/Aivomatic/Amcmgddpp/Model/Amcmgddpp.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic AmcMgDdPp
 *
 * @category   Aivomatic
 * @package    AmcMgDdPp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Amcmgddpp_Model_Amcmgddpp extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'amcmgddpp';
    protected $_formBlockType = 'amcmgddpp/form';
    protected $_infoBlockType = 'amcmgddpp/info';

    protected $_isGateway               = false;
    protected $_canAuthorize            = false;
    protected $_canCapture              = false;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = true;

    /**
     * Return config value 'account_holder'
     *
     * @param void
     * @return  string
     */
    public function getAccountHolder()
    {
        return $this->getConfigData('account_holder');
    }

    /**
     * Return config value 'account_number'
     *
     * @param void
     * @return  string
     */
    public function getAccountNumber()
    {
        return $this->getConfigData('account_number');
    }

    /**
     * Return config value 'bank_name'
     *
     * @param void
     * @return  string
     */
    public function getBankName()
    {
        return $this->getConfigData('bank_name');
    }

    /**
     * Return config value 'iban'
     *
     * @param void
     * @return  string
     */
    public function getIban()
    {
        return $this->getConfigData('iban');
    }

    /**
     * Return config value 'bic'
     *
     * @param void
     * @return  string
     */
    public function getBic()
    {
        return $this->getConfigData('bic');
    }

    /**
     * Are IBAN and BIc OK, ie. can we show international payment info
     * @param void
     * @return bool
     */
    public function showInternationalInfo() {
        if ($this->getIban() AND
            $this->getBic()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return config value 'pay_within_days'
     *
     * @param void
     * @return  string
     */
    public function getPayWithinDays()
    {
        return $this->getConfigData('pay_within_days');
    }

    /**
     * Return config value 'custom_info_text'
     *
     * @param void
     * @return  string
     */
    public function getCustomInfoText()
    {
        return nl2br($this->getConfigData('custom_info_text'));
    }

    /**
     * Return config value 'custom_form_text'
     *
     * @param void
     * @return  string
     */
    public function getCustomFormText()
    {
        return nl2br($this->getConfigData('custom_form_text'));
    }

    /**
     * Return config value 'order_status_new'
     *
     * @param void
     * @return  string
     */
    public function getOrderStatusNew()
    {
        return $this->getConfigData('order_status_new');
    }

    /**
     * Return config value 'debug_flag'
     *
     * @param void
     * @return  boolean
     */
    public function getDebug()
    {
        return $this->getConfigData('debug_flag');
    }

    /**
     * Return current checkout session object
     * @param void
     * @return object Mage_Checkout_Model_Session
     */
    protected function getSession() {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Returns current quote/order info instance
     * @param void
     * @return object
     */
    protected function getInfo() {
        $info = $this->getInfoInstance();
        return $info;
    }

    /**
     * Returns unformated quote/order creation date
     * @param void
     * @return string Unformated creation date (yyyy-mm-dd hh:mm:ss)
     */
    public function getDate() {
        $info = $this->getInfo();
        if ($info instanceof Mage_Sales_Model_Order_Payment) {
            $date = $info->getOrder()->getCreatedAt();
        } else {
            $date = $info->getQuote()->getCreatedAt();
        }
        return $date;
    }

    /**
     * Returns formated due date
     * @param mixed Unix time stamp
     * @return string Due date
     */
    public function getDueDate($unixtime = false) {
        if ($unixtime) {
            $date = $unixtime;
        } else {
            $date = strtotime($this->getDate());
        }
        return
            Mage::helper('core')->formatDate(
                date('Y-m-d H:i:s',
                    self::calculateDueDate(
                        $date,
                        intval($this->getPayWithinDays())
                    )
                ),
                'medium',
                false
            );
    }

    /**
     * Calculates the due date based on order date and days to expiry
     * If due day would be saturday or sunday, it is adjusted to the following
     * monday.
     * @param int $orderDate Unix timestamp
     * @param int $daysToExpiry Number of days to expiry
     * @return int Unix timestamp
     */
    static public function calculateDueDate($orderDate, $daysToExpiry) {
        $orderDate = $orderDate+($daysToExpiry*86400);
        $dueDate = mktime(date('G', $orderDate),
                          date('i', $orderDate),
                          date('s', $orderDate),
                          date('m', $orderDate),
                          date('j', $orderDate),
                          date('Y', $orderDate));
        $weekDay = date('N', $dueDate);
        if ($weekDay == 6) {
            //Saturday - add two days to adjust due date to monday
            $dueDate = $dueDate+172800;
        } elseif ($weekDay == 7) {
            //Sunday - add one day to adjust due date to monday
            $dueDate = $dueDate+86400;
        }
        return $dueDate;
    }

    /**
     * Returns bank reference number
     * @param void
     * @return mixed string|bool Bank reference number or false
     */
    public function getBankReference() {
        $info = $this->getInfo();

        if ($info->getPoNumber()) {
            return $info->getPoNumber();
        }
        if ($info->getData('order_placed') == 'y') {
            $orderId = $info->getOrder()->getRealOrderId();
            $pos = strpos($orderId, '-');
            if ($pos !== false) {
                $orderId = substr($orderId, 0, $pos);
            }
            $bankRefNum = self::makeFbaBankRef($orderId);
            $info->setPoNumber($bankRefNum);
            $info->save();
            return $bankRefNum;
        } else {
            return false;
        }
    }

    /**
     * Payment action Authorize, ran automatically after order is placed
     *
     * @param   Varien_Object $orderPayment
     * @return  Aivomatic_Amcmgddpp_Model_Amcmgddpp
     */
    public function authorize(Varien_Object $payment, $amount) {
        $this->assignData(array('order_placed' => 'y'));
        $info = $this->getInfoInstance();
        $order = $info->getOrder();
        $msg  = $this->getTitle().': ';
        $msg .= Mage::helper('amcmgddpp')->__('Pay within').' ';
        $msg .= $this->getPayWithinDays();
        $msg .= Mage::helper('amcmgddpp')->__('days').'. ';
        $msg .= Mage::helper('amcmgddpp')->__('Due date').' ';
        $msg .= $this->getDueDate(time()).'.';
        $order->addStatusToHistory($order->getStatus(), $msg, true);
        return $this;
    }

    /**
     * Calculates check sum for finnish bank reference number
     *
     * Check sum is calculated according to Finnish Banker's Assosiation
     * standard.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 1-19 characters.
     * @link http://www.pankkiyhdistys.fi/asp/ida/download.asp?prm1=wwwuser_fkl&docid=20176&sec=&ext=.pdf
     * @return string Single digit check sum
     */
    public static function calculateFbaBankRefCheckSum($base)
    {
        $base = self::validateFbaBankRefBase($base);
        $baseNums = array_reverse(str_split($base));
        $weight = 0;
        $sum = 0;
        foreach ($baseNums as $num) {
            switch ($weight) {
            case 0:
                $sum += 7 * $num;
                $weight = 1;
                break;
            case 1:
                $sum += 3 * $num;
                $weight = 2;
                break;
            case 2:
                $sum += 1 * $num;
                $weight = 0;
                break;
            }
        }
        $rounded = round($sum, -1);

        if ($rounded < $sum) {
            $rounded += 10;
        }
        return (string)($rounded-$sum);
    }

    /**
     * Formats finnish bank reference number
     *
     * Formating is performed according to Finnish Banker's Assosiation
     * standard. The reference number is split into chunks of five characters
     * beginning from end. Zero padding is removed from the start.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 4-20 characters.
     * @link http://www.pankkiyhdistys.fi/asp/ida/download.asp?prm1=wwwuser_fkl&docid=20176&sec=&ext=.pdf
     * @return string Formated reference number
     */
    public static function formatFbaBankRef($ref)
    {
        return trim(strrev(chunk_split(strrev(ltrim($ref, '0')), 5, ' ')));
    }

    /**
     * Returns formated finnish bank reference number including its check sum
     *
     * Check sum and formating according to Finnish Banker's Assosiation
     * standard.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 1-19 characters.
     * @link http://www.pankkiyhdistys.fi/asp/ida/download.asp?prm1=wwwuser_fkl&docid=20176&sec=&ext=.pdf
     * @return string Formated standard compliant bank reference number.
     */
    public static function makeFbaBankRef($base)
    {
        return self::formatFbaBankRef(
            $base.self::calculateFbaBankRefCheckSum($base));
    }

    /**
     * Basic validation of finnish bank reference number's base part
     * @param   string|int $base Base part for finnish bank reference number
                           All digits. Length 3-19.
     * @throws  Exception If $base isn't valid
     * @return  string     strval($base)
     */
    public static function validateFbaBankRefBase($base)
    {
        if (!self::isIntOrString($base)) {
            throw new Exception(
                '$base must be an integer or a string (is '.
                gettype($base).')');
        }
        if (self::isEmptyButNotZero($base)) {
            throw new Exception(
                '$base must not be empty');
        }
        $base = strval($base);
        if (!ctype_digit($base)) {
            throw new Exception(
                '$base must be all digits (0-9)');
        }
        if (strlen($base) > 19) {
            throw new Exception(
                '$base must be 20 characters or shorter'
                .' (is '.strlen($base).')');
        }
        if (strlen($base) < 3) {
            throw new Exception(
                '$base must be three characters or longer'
                .' (is '.strlen($base).')');
        }
        return $base;
    }

    /**
     * Validates that parameter is an integer or a string
     *
     * Note that very big integers might be unexpectedly casted to type double.
     * The limit depends on your system.
     * @param   mixed $param
     * @return  bool true if int or string, false if some other type
     */
    public static function isIntOrString($param)
    {
        if (is_int($param) OR is_string($param)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if parameter is empty but not zero (0 or '0')
     *
     * PHP's buildin function empty() returns true even  if param is (int)0 or
     * (string)'0'. This method returns true if param is '' (an empty string)
     * NULL, FALSE or array() (an empty array). It returns false if param is 0
     * (integer 0) or '0' (string 0).
     * @param   mixed $param
     * @return  bool true if really empty, false if not empty including 0 and '0'
     */
    public static function isEmptyButNotZero($param)
    {
        if ($param == '' AND $param !== 0 AND $param !== '0') {
            return true;
        } else {
            return false;
        }
    }
}