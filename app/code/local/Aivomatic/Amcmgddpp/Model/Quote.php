<?php
/**
 * @category   Aivomatic
 * @package    AmcMgDdPp
 * @version    ver. 0.2.1 - $Id: Quote.php 789 2009-05-19 06:06:23Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/amcmgddpp/trunk/base/app/code/local/Aivomatic/Amcmgddpp/Model/Quote.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic AmcMgDdPp
 *
 * @category   Aivomatic
 * @package    AmcMgDdPp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Amcmgddpp_Model_Quote extends Mage_Sales_Model_Quote
{

}