<?php
/**
 * @category   Aivomatic
 * @package    AmcMgDdPp
 * @version    ver. 0.2.1 - $Id: Data.php 405 2008-09-17 09:30:03Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/amcmgddpp/trunk/base/app/code/local/Aivomatic/Amcmgddpp/Helper/Data.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic AmcMgDdPp
 *
 * @category   Aivomatic
 * @package    AmcMgDdPp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Amcmgddpp_Helper_Data extends Mage_Core_Helper_Abstract
{

}
