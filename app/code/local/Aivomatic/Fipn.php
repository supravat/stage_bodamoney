<?php
/**
 * FIPN - Finnish Instant Payment Notification
 *
 * E-commerce software independent framework for finnish payment gateway
 * adapter applications.
 *
 * Requirements:
 *      - PHP version 5.0.3 or higher
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * @version     ver. 1.10.2 - $Id: Fipn.php 1505 2011-05-09 05:36:38Z teemu $
 * @file        $URL: file:///home/teemu/svn/aivomatic/fipn/trunk/Aivomatic/Fipn.php $
 * @copyright   2013 Aivomatic Ltd
 * @link        http://www.aivomatic.com/ Aivomatic Ltd
 * @license     Copyright-(C)-2010-Aivomatic-Ltd
 * @license     All-rights-reserved
 */

/**
 * Require Aivomatic_Fipn_Exception class
 */
require_once dirname(__FILE__).'/Fipn/Exception.php';

/**
 * Require Aivomatic_Fipn_Validator class
 */
require_once dirname(__FILE__).'/Fipn/Validator.php';

/**
 * Require Aivomatic_Fipn_Factory class
 */
require_once dirname(__FILE__).'/Fipn/Factory.php';

/**
 * Finnish Instant Payment Notification base class
 *
 * @category    Aivomatic
 * @package     Fipn_Framework
 * The base class holds common properties and methods.
 * It is extended by child classes: one related to sending customer to payment
 * gateway (Sender) and one to receiving customer back from the payment gateway
 * (Receiver). In addition there is a base class for validating input (Validator).
 * The Sender, Receiver and Validator classes classes are extented by payment
 * gateway specific subclasses.
 *
 * @author      Teemu Mäntynen, Aivomatic Ltd
 */
class Aivomatic_Fipn
{

    /**
     * Fipn version number
     *
     * Set by {@link http://phing.info/ Phing} during build-process.
     * @var string
     */
    const   AIVOMATIC_FIPN_VERSION = '1.10.2';

    /**
     * Instance of Aivomatic_Fipn_Validator -child class
     * @var object
     */
    protected $Validator;

    /**
     * Sets Validator -object
     * @param object
     * @return void
     * @uses $Validator
     */
    protected function setValidator($Validator)
    {
        $this->Validator = $Validator;
    }

    /**
     * Returns Validator-object
     * @param   void
     * @return  ovject
     * @uses $Validator
     */
    public function getValidator()
    {
        return $this->Validator;
    }

    /**
     * Merchant's secret ID
     *
     * Secret authentication ID is issued by the payment gateway. It is never
     * sent from shop to payment gateway or vise versa as it is, but as a part
     * of the MAC (message authentity code).
     * @see Aivomatic_Fipn_Validator::validateSecretMerchantId()
     * @var string
     */
    protected $secretMerchantId;

    /**
     * MAC key (merchant's secret ID) version
     *
     * @var string
     */
    protected $keyVersion;

    /**
     * Merchant's public ID
     *
     * Public (as in 'non-secret') authentication ID is issued by the payment
     * @see Aivomatic_Fipn_Validator::validatePublicMerchantId()
     * @var string
     */
    protected $publicMerchantId;

    /**
     * Order Id
     *
     * @see Aivomatic_Fipn_Validator::validateOrderId()
     * @var string
     */
    protected $orderId;

    /**
     * Order Total
     *
     * Ie. total sum of the order in Euros. Dot as desimal sign, precision
     * two digits. Some classes might over ride this (at least Luottokunta).
     * @see Aivomatic_Fipn_Validator::validateOrderTotal()
     * @see formatOrderTotal()
     * @var string
     */
    protected $orderTotal;

    /**
     * Currency code
     *
     * Varies by payment gateway. In most cases "EUR", but might be a numeric
     * code in some cases.
     * @see Aivomatic_Fipn_Validator::validateCurrency()
     * @var string
     */
    protected $currency;

    /**
     * Bank reference number
     *
     * @var string
     */
    protected $bankReference;

    /**
     * MAC (Message Authentication Code)
     *
     * 32 character long uppercase hexadecimal string.
     *
     * $mac is calculated by Fipn based on different values, which vary from
     * payment gateway to payment gateway, but the values always include
     * {@see $secretMerchantId}.
     *
     * By using a MAC, the recipient can be sure that the message is from the
     * intended source and that the message is not tampered during the
     * transmission.
     *
     * $mac is sent to payment gateway by Sender.
     * $mac is compared to $responseMac by Receiver.
     * @see Aivomatic_Fipn_Receiver::$responseMac
     * @var string
     */
    protected $mac;

    /**
     * Multibyte string functions class
     * @var object
     */
    protected $MbStr;

    /**
     * Constructor
     * @param  void
     * @return void
     */
    protected function __construct()
    {
        $this->MbStr = Aivomatic_Fipn_Factory::getMbString();
    }

    /**
     * Return secret merchant Id
     * @param void
     * @uses $secretMerchantId
     * @return string secretMerchantId
     */
    protected function getSecretMerchantId()
    {
        return $this->secretMerchantId;
    }

    /**
     * Returns MAC key (merchant's secret ID) version
     *
     * @param   void
     * @return  string $keyVersion
     */
    public function getKeyVersion()
    {
        return $this->keyVersion;
    }

    /**
     * Get myMac
     * @param void
     * @return string
     * @uses $mac
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * Get public merchant Id
     * @uses $publicMerchantId
     * @param void
     * @return string
     */
    public function getPublicMerchantId()
    {
        return $this->publicMerchantId;
    }

    /**
     * Get orderId
     * @uses $orderId
     * @param void
     * @return string
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Get orderTotal
     * @param   void
     * @return  string
     * @uses $orderTotal
     */
    public function getOrderTotal()
    {
        return $this->orderTotal;
    }

    /**
     * Format orderTotal
     * @param   float $orderTotal float
     * @return  string Dot as decimal point, precision two digits
     * @see sprintf()
     */
    public static function formatOrderTotal($orderTotal)
    {
        return sprintf("%01.2F", $orderTotal);
    }

    /**
     * Formats order total from Euros to Euro cents (from float to strval(int))
     * @param $amount float
     * @return string Amount in cents as a string
     */
    public static function formatEurosToCents($amount)
    {
        return strval(intval($amount*100));
    }

    /**
     * Get currency code
     * @uses $currency
     * @param   void
     * @return  string currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Returns bank reference number
     * @uses $bankReference
     * @param   void
     * @return  string Bank reference number
     */
    public function getBankReference()
    {
        return $this->bankReference;
    }

    /**
     * Calculates a MAC (message authentication code)
     *
     * MAC is a MD5 hash in 32-character hexadecimal number uppercase format.
     * Different payment gateway use different data in its calculation, but
     * merchant's secret security ID is always a part of it.
     * @see strtoupper()
     * @see md5()
     * @param sting $string String for which the MAC is calculated for
     * @return string MAC
     */
    protected static function calculateMac($string)
    {
        return strtoupper(md5($string));
    }

    /**
     * Calculates check sum for finnish bank reference number
     *
     * Check sum is calculated according to Finnish Banker's Assosiation
     * standard.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 3-19 characters.
     * @throws Aivomatic_Fipn_Exception If base is not valid.
     * @return string Single digit check sum
     */
    public static function calculateFbaBankRefCheckSum($base)
    {
        $base = Aivomatic_Fipn_Validator::validateFbaBankRefBase($base);
        $baseNums = array_reverse(str_split($base));
        $weight = 0;
        $sum = 0;
        foreach ($baseNums as $num) {
            switch ($weight) {
            case 0:
                $sum += 7 * $num;
                $weight = 1;
                break;
            case 1:
                $sum += 3 * $num;
                $weight = 2;
                break;
            case 2:
                $sum += 1 * $num;
                $weight = 0;
                break;
            }
        }
        $rounded = round($sum, -1);

        //Tulot lasketaan yhteen ja summa vähennetään *seuraavasta* täydestä
        // kymmenestä.
        if ($rounded < $sum) {
            $rounded += 10;
        }
        return (string)($rounded-$sum);
    }

    /**
     * Formats finnish bank reference number
     *
     * Formating is performed according to Finnish Banker's Assosiation
     * standard. The reference number is split into chunks of five characters
     * beginning from end. Zero padding is removed from the start.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 4-20 characters.
     * @throws Aivomatic_Fipn_Exception If $ref is not valid.
     * @return string Formated reference number
     */
    public static function formatFbaBankRef($ref)
    {
        $ref = Aivomatic_Fipn_Validator::validateUnformattedFbaBankRef($ref);
        return trim(strrev(chunk_split(strrev(ltrim($ref, '0')), 5, ' ')));
    }

    /**
     * Returns formated finnish bank reference number including its check sum
     *
     * Check sum and formating according to Finnish Banker's Assosiation
     * standard.
     * @param string|int Numeric base part for the bank reference. All digits.
     *                   Length 3-19 characters.
     * @throws Aivomatic_Fipn_Exception If base is not valid.
     * @return string Formated standard compliant bank reference number.
     */
    public static function makeFbaBankRef($base)
    {
        return self::formatFbaBankRef(
            $base.self::calculateFbaBankRefCheckSum($base));
    }

    /**
     * Textual presentation of object's properties
     *
     * Gathers and formats class properties to a string to be used by eg. echo
     * and print PHP-functions. Nice for debuging etc.
     * @see $secretMerchantId, $publicMerchantId, $orderId, $orderTotal,
     *      $currency, $mac
     * @param   void
     * @return  string Class properties as formated string
     */
    public function __toString()
    {
        $str =
        __CLASS__.' (v. '.self::AIVOMATIC_FIPN_VERSION.')::'."\n".
        'len(secretMerchantId): '.strlen($this->getSecretMerchantId())."\n".
        'keyVersion:            '.$this->getKeyVersion()."\n".
        'publicMerchantId:      '.$this->getPublicMerchantId()."\n".
        'orderId:               '.$this->getOrderId()."\n".
        'orderTotal:            '.$this->getOrderTotal()."\n".
        'bankReference:         '.$this->getBankReference()."\n".
        'currency:              '.$this->getCurrency()."\n".
        'mac:                   '.$this->getMac()."\n".
        'MbStr extension name   '.$this->MbStr->getExtensionName()."\n";
        if (extension_loaded('mbstring')) {
            $str .= 'mbstring-extension     Loaded';
        } else {
            $str .= 'mbstring-extension     Not loaded';
        }
        if (extension_loaded('iconv')) {
            $str .= 'iconv-extension        Loaded';
        } else {
            $str .= 'iconv-extension        Not loaded';
        }
        return $str;
    }
}
?>