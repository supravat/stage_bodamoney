<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: StandardController.php 1728 2013-02-01 06:04:49Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/controllers/StandardController.php $
 * @copyright  Aivomatic Ltd 2013 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_StandardController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get singleton with svmmg standard
     *
     * @return object Aivomatic_Svmmg_Model_Standard
     */
    public function getStandard()
    {
        return Mage::getSingleton('svmmg/standard');
    }

    /**
     * Get Config model
     *
     * @return object Aivomatic_Svmmg_Model_Config
     */
    public function getConfig()
    {
        return $this->getStandard()->getConfig();
    }

    /**
     *  Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return $this->getStandard()->getDebug();
    }

    /**
     * When a customer chooses Svmmg on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setSvmmgStandardQuoteId($session->getQuoteId());
        
        $order = Mage::getModel('sales/order');
      
        $order->loadByIncrementId($session->getLastRealOrderId());
        $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('svmmg')->__('Customer was redirected to PGW'),
            true
        );
        $order->save();
        
        $this->getResponse()
            ->setBody($this->getLayout()
                ->createBlock('svmmg/standard_redirect')
                ->setOrder($order)
                ->toHtml());
        $session->unsQuoteId();
    }

    /**
     * Result response request from Suomen Verkkomaksut
     *
     */
    public function fipnResponseAction()
    {
        //Debug - $_GET
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n".
                '$_GET: '.print_r($_GET, true));
        }

        //Require Finnish Instant Payment Notification Factory Class
        require_once BP.'/app/code/local/Aivomatic/Fipn/Factory.php';

        //Try to get a Receiver from Fipn_Factory
        try {
            $Receiver = Aivomatic_Fipn_Factory::getReceiver(
                $this->getConfig()->getAuthCode(),
                $_GET,
                'suomenverkkomaksut');
        } catch (Aivomatic_Fipn_Exception $e) {
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                //Log Exception
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                          .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
            }
            //Rethrow Exception
            throw $e;
        }

        if ($Receiver->getResponseStatus() == 'notify') {
            sleep(5);
        }

        //Debug - Aivomatic_Fipn_Receiver_PaymentGatewayName
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".
                $Receiver->__toString());
        }

        //Set Receiver for Standard
        $this->getStandard()->setReceiver($Receiver);

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($this->getStandard()->getReceiver()->getOrderId());

        //Debug - Mage_Sales_Model_Order
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".
                'order state:'.$order->getState()."\n".
                'order id:'.$order->getId()."\n".
                'order status label:'.$order->getStatusLabel()."\n".
                'can cancel:'.print_r($order->canCancel(), true)."\n");
        }

        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getSvmmgStandardQuoteId(true));

        $msg = '';

        //Debug - Session
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".
                '$session:'.print_r($session, true));
        }

        if (!$order->getId()) {
            //Can't find matching order
            $session->setErrorMessage(
                Mage::helper('svmmg')->__('Unknown order Id').
                ' #'.$this->getStandard()->getReceiver()->getOrderId());
            $redirectTo = 'svmmg/standard/failure';
        } elseif ($order->getState() !== Mage_Sales_Model_Order::STATE_NEW AND
                  $Receiver->getResponseStatus() == 'notify') {
            //Order is already updated by returning customer or by admin. SVM/Notify is not needed.
            $msg = Mage::helper('svmmg')->__('Notify message received');
            $order->addStatusToHistory($order->getStatus(), $msg, true);
            $order->save();
            $redirectTo = 'checkout/onepage/success';
        } elseif ($order->getState() !== Mage_Sales_Model_Order::STATE_PENDING_PAYMENT AND
                  $Receiver->getResponseStatus() != 'notify') {
            //Order status has already been changed from new to other statues.
            //Customer reloaded the page or SVM/Notify has already been received?
            $msg = Mage::helper('svmmg')->__('Payment status already set').' ('.$order->getState().')';
            $order->addStatusToHistory($order->getStatus(), $msg, true);
            $order->save();
            $session->setErrorMessage($msg);
            $redirectTo = 'svmmg/standard/failure';
        } elseif ($this->getStandard()->getReceiver()->getIsSuccess()) {
            //Successful payment
            $order->getPayment()->setTransactionId(
                $this->getStandard()->getReceiver()->getTransactionId());
            if ($this->saveInvoice($order)) {
                $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING,
                                 Mage_Sales_Model_Order::STATE_PROCESSING);
            }
            if ($Receiver->getResponseStatus() == 'notify') {
                $msg = Mage::helper('svmmg')->__('Notify message received');
            } else {
                $msg = Mage::helper('svmmg')->__('Customer successfully returned from PGW');
            }
            $msg .= Mage::helper('svmmg')->__('Transaction Id is').
                    $this->getStandard()->getReceiver()->getTransactionId().'. '.
                    Mage::helper('svmmg')->__('Payment method was').
                    $this->getStandard()->getReceiver()->getMethodName().
                    ' ('.$this->getStandard()->getReceiver()->getMethodId().')';
            $order->addStatusToHistory($order->getStatus(), $msg, true);
            $order->setEmailSent(true);
            $order->save();
            $order->sendNewOrderEmail();
            //Testi $order->sendOrderUpdateEmail(true, 'Päivitys');
            Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
            $redirectTo = 'checkout/onepage/success';
        } elseif ($this->getStandard()->getReceiver()->getIsDelay()) {
            //Delayed payment
            $order->setState(
                Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                'payment_delay',
                Mage::helper('svmmg')->__('Suomen Verkkomaksut payment pending'),
                $notified = false
            );
            $msg .= Mage::helper('svmmg')->__('Transaction Id is').
                    $this->getStandard()->getReceiver()->getTransactionId().'. '.
                    Mage::helper('svmmg')->__('Payment method was').
                    $this->getStandard()->getReceiver()->getMethodName().
                    ' ('.$this->getStandard()->getReceiver()->getMethodId().')';
            $order->addStatusToHistory($order->getStatus(), $msg, true);
            $order->setEmailSent(true);
            $order->save();
            $order->sendNewOrderEmail();
            Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
            $redirectTo = 'checkout/onepage/success';
        } elseif ($this->getStandard()->getReceiver()->getIsCancel()) {
            //Customer has canceled the order during payment
            $msg = Mage::helper('svmmg')->__('Order was canceled by customer');
            $redirectTo = 'checkout/cart';
            $this->repopulateCart($order);
            $order->cancel();
            $order->addStatusToHistory($order->getStatus(), $msg);
            $order->save();
        } else {
            //An error has occured
            $msg = Mage::helper('svmmg')->__('Error in payment');
            if ($this->getStandard()->getReceiver()->getIsError() AND
                $this->getStandard()->getReceiver()->getIsAuthenticResponse()) {
                //Fipn reports an error
                $msg .= ' '.Mage::helper('svmmg')->__('Technical failure');
            }
            if (!$this->getStandard()->getReceiver()->getIsAuthenticResponse()) {
                //Responce couldn't be authenticated
                $msg .= ' '.Mage::helper('svmmg')->__('Authentication failure');
            }
            $session->setErrorMessage($msg);
            $redirectTo = 'svmmg/standard/failure';
            $order->cancel();
            $order->addStatusToHistory($order->getStatus(), $msg, true);
            $order->save();
        }
        $this->_redirect($redirectTo);
    }

    /**
     * Puts order items back to cart
     *
     * Magento empties the shopping cart when customer places his order. If the
     * customer then cancels the payment to return to store, his order is fucked
     * up by the empty cart. Thus we must repopulate the cart on behalf of the
     * customer to keep him a happy camper.
     * This wasn't a problem before Magento CE 1.4.1.0
     * @param Mage_Sales_Model_Order $order
     * @return   boolean True if OK
     * @see http://www.magentocommerce.com/boards/.../viewthread/199053/P15/#t280453
     */
    protected function repopulateCart($order)
    {
        $session = Mage::getSingleton('checkout/session');
        $cart = Mage::getSingleton('checkout/cart');

        $items = $order->getItemsCollection();
        foreach ($items as $item) {
            try {
                $cart->addOrderItem($item,$item->getQty());
            }
            catch (Mage_Core_Exception $e){
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice($e->getMessage());
                }
                else {
                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
                }
            }
            catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addException($e,
                    Mage::helper('checkout')->__('Cannot add the item to shopping cart.')
                );
            }
        }
        $cart->save();
        return true;
    }

    /**
     *  Save invoice for order
     *
     *  @param    Mage_Sales_Model_Order $order
     *  @return	  boolean Can save invoice or not
     */
    protected function saveInvoice (Mage_Sales_Model_Order $order)
    {
        if ($order->canInvoice()) {
            $convertor = Mage::getModel('sales/convert_order');
            $invoice = $convertor->toInvoice($order);
            foreach ($order->getAllItems() as $orderItem) {
               if (!$orderItem->getQtyToInvoice()) {
                   continue;
               }
               $item = $convertor->itemToInvoiceItem($orderItem);
               $item->setQty($orderItem->getQtyToInvoice());
               $invoice->addItem($item);
            }
            $invoice->collectTotals();
            $invoice->register()->capture();
            Mage::getModel('core/resource_transaction')
               ->addObject($invoice)
               ->addObject($invoice->getOrder())
               ->save();
            //Turha IPN:llä $invoice->sendEmail();
            return true;
        }

        return false;
    }

    /**
     *  Failure Action
     *
     *  @return	  void
     */
    public function failureAction ()
    {
        $session = Mage::getSingleton('checkout/session');

        if (!$session->getErrorMessage()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('svmmg/session');
        $this->renderLayout();
    }
}