<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: Config.php 1731 2013-02-01 13:25:10Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Model/Config.php $
 * @copyright  Aivomatic Ltd 2013
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Model_Config extends Varien_Object
{
    const CULTURE_TYPE_FI = 'fi_FI';
    const CULTURE_TYPE_US = 'en_US';
    const CULTURE_TYPE_SV = 'sv_SE';

    const S1   = 'S1';
    const E1   = 'E1';

    //const CURRENCY_CODE     = 'EUR';
    const CURRENCY_CODE      ='UGX';
    const TRANSACTION_TYPE  = '3';

    /**
     *  Return config var
     *
     *  @param    string Var key
     *  @param    string Default value for non-existing key
     *  @return	  mixed
     */
    public function getConfigData($key, $default=false)
    {
        if (!$this->hasData($key)) {
             $value = Mage::getStoreConfig('payment/svmmg_standard/'.$key);
             if (is_null($value) || false===$value) {
                 $value = $default;
             }
            $this->setData($key, $value);
        }
        return $this->getData($key);
    }

    /**
     *  Return merchant Id
     *
     *  @return   string Merchant Id
     */
    public function getMerchantId ()
    {
        return $this->getConfigData('merchant_id');
    }

    /**
     *  Return authentication code
     *
     *  @return   string Auth Code
     */
    public function getAuthCode ()
    {
        return $this->getConfigData('auth_code');
    }

    /**
     *  Return description row 1
     *
     *  @return   string Desc Row 1
     */
    public function getDescRow1 ()
    {
        return $this->getConfigData('desc_row1');
    }

    /**
     *  Return description row 2
     *
     *  @return   string Desc Row 2
     */
    public function getDescRow2 ()
    {
        return $this->getConfigData('desc_row2');
    }

    /**
     *  Return Culture
     *
     *  @return   string Culture
     */
    public function getCulture ()
    {
        return $this->getConfigData('culture');
    }

    /**
     *  Return Embed Form setting
     *
     *  @return   bool
     */
    public function getEmbedForm()
    {
        if ($this->getConfigData('embed_form') == '1') {
            return true;
        } else {
            return false;
        }
    }

    /**
     *  Return Embed Form options
     *
     *  @return   string
     */
    public function getEmbedOptions()
    {
        return $this->getConfigData('embed_options');
    }

    /**
     *  Returns WAPI version
     *
     *  @return   string Version
     */
    public function getWapiVersion ()
    {
        return $this->getConfigData('wapi_version');
    }

    /**
     *  Return new order status
     *
     *  @return   string New order status
     */
    /*public function getNewOrderStatus ()
    {
        return $this->getConfigData('order_status_new');
    }*/

    /**
     *  Return payment method title
     *
     *  @return   string Title
     */
    public function getTitle()
    {
        return $this->getConfigData('title');
    }

    /**
     * Return button grid text setting
     *
     * @return bool true if yes, false if no
     */
    public function getGridText()
    {
        if ($this->getConfigData('button_grid_text') === '1') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Do prices include VAT?
     *
     * @return bool Always true
     */
    public function getPricesIncludeVat()
    {
        return true;
        //return false;
    }

    /**
     * Returns payment method preselection
     *
     * Empty if no preselection, digit if selected.
     * @return string At the moment always empty
     */
    public function getPreselectedMethod()
    {
        return '';
        /*$method = $this->getConfigData('preselected_method');
        if (empty($method)) {
            return '';
        } else {
            return $method;
        }*/
    }

    /**
     * Returns gateway mode
     *
     * '1' = normal, '2' = bypass. For future use.
     * @return string At the moment always '1'
     */
    public function getGatewayMode()
    {
        return '1';
        //return $this->getConfigData('gateway_mode');
    }

    /**
     *  Return debug flag
     *
     *  @return   boolean Debug flag (0/1)
     */
    public function getDebug ()
    {
        return $this->getConfigData('debug_flag');
    }

    /**
     *  Return sandbox flag
     *
     *  @return   boolean True if '1', else false
     */
    public function getSandbox ()
    {
        if ($this->getConfigData('sandbox') == '1') {
            return true;
        } else {
            return false;
        }
    }
}