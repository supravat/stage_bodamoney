<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: Standard.php 1731 2013-02-01 13:25:10Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Model/Standard.php $
 * @copyright  Aivomatic Ltd 2013 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'svmmg_standard';
    protected $_formBlockType = 'svmmg/standard_form';

    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_isInitializeNeeded      = true; // PayPal

    protected $_order = null;

    protected $receiver = false;
    protected $sender   = false;

    /**
     * HTTP request parameters
     * @var array
     */
    protected $requestParameters           = array();


    /**
     * Get Config model
     *
     * @return object Aivomatic_Svmmg_Model_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('svmmg/config');
    }

    /**
     * Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return $this->getConfig()->getDebug();
    }

    /**
     *  Returns target URL for HTML form
     *
     *  @return	  string Target URL
     */
    public function getSvmmgUrl ()
    {
        if ($this->getConfig()->getSandbox()) {
            return 'http://sandbox.verkkomaksut.fi/interface/payment';
        } else {
            return 'https://payment.verkkomaksut.fi/';
        }
    }

    /**
     * Return Total Amount of the order as a formated string
     *
     * @see    http://www.php.net/manual/en/function.sprintf.php
     * @return string Total amount, two desimals, dot as desimal sign, non-locale aware
     */
    protected function getTotalAmount()
    {
        return sprintf('%.2f', $this->getOrder()->getBaseGrandTotal());
    }

    /**
     *  Return URL for Svmmg success response
     *
     *  @return	  string URL
     */
    protected function getSuccessURL()
    {
        return Mage::getUrl('svmmg/standard/fipnresponse').'?';
    }

    /**
     *  Return URL for Svmmg failure response
     *
     *  @return	  string URL
     */
    protected function getFailureURL()
    {
        return Mage::getUrl('svmmg/standard/fipnresponse').'?';
    }

    /**
     *  Return URL for Svmmg notify response
     *
     *  @return   string URL
     */
    protected function getNotifyURL()
    {
        return Mage::getUrl('svmmg/standard/fipnresponse').'?notify=notify&';
    }

    /**
     *  Return URL for Svmmg delay response
     *
     *  @return   string URL
     */
    protected function getDelayURL()
    {
        return Mage::getUrl('svmmg/standard/fipnresponse').'?';
    }

    /**
     * Transaction unique ID sent to SVM and sent back by SVM for order restore
     * Using created order ID
     *
     *  @return	  string Transaction unique number
     */
    protected function getVendorTxCode ()
    {
        return $this->getOrder()->getRealOrderId();
    }

    /**
     * Return Order description
     *
     *  @return   string Order description
     */
    protected function getOrderDescription()
    {
        $desc = '';
        if ($this->getConfig()->getDescRow1() != '') {
            $desc .= $this->getConfig()->getDescRow1()."\n";
        }
        if ($this->getConfig()->getDescRow2() != '') {
            $desc .= $this->getConfig()->getDescRow2()."\n";
        }
        return $desc;
    }

    /**
     * Check that currency code is EUR and return it or throw exception
     *
     * @uses    Aivomatic_Svmmg_Model_Config::CURRENCY_CODE
     * @return  string Order currency code ('EUR')
     */
    protected function getCurrencyCode()
    {
        if ($this->getOrder()->getBaseCurrencyCode() != Aivomatic_Svmmg_Model_Config::CURRENCY_CODE) {
            throw new Exception('Currency code ('.$this->getOrder()->getBaseCurrencyCode().') is not '.Aivomatic_Svmmg_Model_Config::CURRENCY_CODE);
        }
        return $this->getOrder()->getBaseCurrencyCode();
    }

    /**
     * Return transaction type
     *
     * According to SVM transaction type (payment form field 'TYPE') must be '3'
     *
     * @uses    Aivomatic_Svmmg_Model_Config::TRANSACTION_TYPE
     * @return  string Transaction type
     */
    protected function getTransactionType()
    {
        return Aivomatic_Svmmg_Model_Config::TRANSACTION_TYPE;
    }

    /**
     * Return culture code
     *
     * @return  string Culture code
     */
    protected function getCultureCode()
    {
        return $this->getConfig()->getCulture();
    }

    public function getEmbedForm()
    {
        return $this->getConfig()->getEmbedForm();
    }

    public function getEmbedOptions()
    {
        return $this->getConfig()->getEmbedOptions();
    }

    /**
     *  Form block description
     *
     *  @return	 object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('svmmg/form_standard', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());
        return $block;
    }

    /**
     *  Return Order Place Redirect URL
     *
     *  @return	  string Order Redirect URL
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('svmmg/standard/redirect');
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        $checkout = Mage::getSingleton('checkout/session');
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
            .print_r($checkout, true));
        }
        return $checkout;
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        $quote = $this->getCheckout()->getQuote();
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
            .print_r($quote, true));
        }
        return $quote;
    }

    public function getButtonGrid()
    {
        require_once BP.'/app/code/local/Aivomatic/Fipn/Util/SuomenVerkkomaksut/ButtonGrid.php';
        $grid = '';
        try {
            $grid = new Aivomatic_Fipn_Util_SuomenVerkkomaksut_ButtonGrid(
                        $this->getConfig()->getAuthCode(),
                        $this->getConfig()->getMerchantId(),
                        $this->getConfig()->getConfigData('button_grid_type'),
                        $this->getConfig()->getConfigData('button_grid_columns'),
                        $this->getConfig()->getGridText());
        }  catch (Aivomatic_Fipn_Exception $e) {
            if (Mage::getStoreConfig('dev/log/active')) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                          .'Grid: Aivomatic_Fipn_Exception: '.$e->getRawMessage());
            }
        }
        return $grid;

    }

    /**
     *  Return Standard Checkout Form Fields for request to SVM
     *
     *  @return   array Array of hidden form fields
     */
    public function getStandardCheckoutFormFields ()
    {
        require_once BP.'/app/code/local/Aivomatic/Fipn/Factory.php';
        $discounts = array();
        try {
            $this->sender = Aivomatic_Fipn_Factory::getSender('suomenverkkomaksut');

            $this->sender->setOrderData(
                $this->getConfig()->getAuthCode(),
                $this->getConfig()->getMerchantId(),
                $this->getVendorTxCode(),
                floatval($this->getTotalAmount()),
                $this->getCurrencyCode(),
                $this->getSuccessURL(),
                $this->getFailureURL(),
                $this->getNotifyURL(),
                $this->getDelayURL(),
                $this->getCultureCode(),
                $this->getOrderDescription(),
                $this->getConfig()->getPricesIncludeVat(),
                $this->getConfig()->getPreselectedMethod(),
                $this->getConfig()->getGatewayMode());
           
        } catch (Aivomatic_Fipn_Exception $e) {
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                          .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
            }
            throw $e;
        }
        if ($this->getConfig()->getWapiVersion() == Aivomatic_Svmmg_Model_Config::E1) {
            $order = $this->getOrder();
            if (!($order instanceof Mage_Sales_Model_Order)) {
                Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
            }
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                    .'Tilaus:'."\n".print_r($order->getData(), true)."\n");
            }
            $addr = $order->getBillingAddress();
           
            try {
                $this->sender->setCustomerData(
                    $addr->getFirstname(),
                    $addr->getLastname(),
                    $addr->getStreet(1),
                    $addr->getPostcode(),
                    $addr->getCity(),
                    strtoupper($addr->getCountry()),
                    $order->getCustomerEmail(),
                    $addr->getTelephone(),
                    false,
                    $addr->getCompany()
                );
            } catch (Aivomatic_Fipn_Exception $e) {
                
                if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                    Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                        .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
                    Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                        .'BillingAddress: '.print_r($addr, true));
                }
                throw $e;
            }
          
            //Items
            $items = $order->getAllItems();
            if ($items) {
              
                foreach($items as $item) {
                    if ($item->getParentItem()) {
                        continue;
                    }
                    
                    $name = $item->getName();
                    $qty  = floatval($item->getQtyOrdered());
                    //$basePrice = floatval($item->getBasePrice()); //ALViton 'Catalog prices include tax -asetuksesta riippumatta
                    //$priceInclTax = floatval($item->getBasePriceInclTax()); //ng 1.3
                    $priceInclTax = floatval($item->getBasePrice())+(floatval($item->getBaseTaxAmount()/$qty));
                    $taxClass = floatval($item->getTaxPercent());
                    $sku = $item->getSku();
                     
                    if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                        Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                            .'Tuote:'."\n".print_r($item->getData(), true)."\n"
                            .'item: '."\n".
                            ' name:'.$name."\n".
                            ' qty:'.$qty."\n".
                            ' priceInclTax:'.$priceInclTax."\n".
                            ' taxClass'.$taxClass."\n".
                            ' sku:'.$sku."\n".
                            ' xPrice:'.floatval($item->getPrice()-$item->getDiscountAmount()));
                    }
                    try {
                        $this->sender->addItem(
                            $name, $qty, $priceInclTax, $taxClass, $sku, 1, false);
                     
                    } catch (Aivomatic_Fipn_Exception $e) {
                        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                                    .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
                            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                            .'$item: '.print_r($item, true));
                        }
                        throw $e;
                    }
                     
                    $discounts =
                        self::addToDiscountDetails(
                            $discounts,
                            $taxClass,
                            floatval($item->getBaseDiscountAmount()));
                }
            }
            
            //Shipping
            //$shipping = $order->getBaseShippingAmount(); //ALViton 'Shipping prices include tax -asetuksesta riippumatta
            //$shipping = $order->getBaseShippingInclTax(); ng 1.3
            $shipping = floatval($order->getBaseShippingAmount())+floatval($order->getBaseShippingTaxAmount());
             
            if ($shipping > 0) {
                $name = $order->getData('shipping_description');
                $qty  = 1.0;
                $total = floatval($shipping);
                $sku = $order->getData('shipping_method');

                $taxId = Mage::helper('tax')->getShippingTaxClass(Mage::app()->getStore()->getId());

                $request = Mage::getSingleton('tax/calculation')->getRateRequest();
                $request->setCustomerClassId($this->_getCustomerTaxClass())
                        ->setProductClassId($taxId);
                $shipTaxRate = floatval(Mage::getSingleton('tax/calculation')->getRate($request));

                if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                    Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                        .'Shipping: '."\n".
                        ' name:'.$name."\n".
                        ' qty:'.$qty."\n".
                        ' total:'.$total."\n".
                        ' shippingTaxRate:'.$shipTaxRate."\n".
                        ' sku:'.$sku."\n".
                        ' xShipping:'.$order->getShippingAmount()."\n".
                        ' shippingTax:'.$order->getBaseShippingTaxAmount()."\n".
                        ' taxId:'.$taxId."\n".
                        ' shippingIncludesTax:'.print_r(Mage::helper('tax')->shippingPriceIncludesTax(Mage::app()->getStore()->getId()), true)."\n".
                        ' itemPriceIncludesTax:'.print_r(Mage::helper('tax')->priceIncludesTax(Mage::app()->getStore()->getId()), true)."\n");
                }
                try {
                    $this->sender->addItem(
                        $name, $qty, $total, $shipTaxRate, $sku, 1, false);
                } catch (Aivomatic_Fipn_Exception $e) {
                    if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                        Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                                .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
                    }
                    throw $e;
                }
                $discounts =
                        self::addToDiscountDetails(
                            $discounts,
                            $shipTaxRate,
                            floatval($order->getBaseShippingDiscountAmount()));
            }

            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                .'$discounts: '.print_r($discounts, true));
            }
          
            foreach($discounts as $key => $value) {
                if (floatval($value) > 0) {
                    $name = trim(Mage::helper('sales')->__('Discount')).' ('.
                                Mage::helper('svmmg')->__('VAT').' '.
                                $key.'%)';
                    $qty  = 1.0;
                    $total = floatval($value)*-1;
                    $sku = $name;
                    $discountTaxRate = floatval($key);
                    try {
                        $this->sender->addItem(
                            $name, $qty, $total, $discountTaxRate, $sku, 1, false);
                    } catch (Aivomatic_Fipn_Exception $e) {
                        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                                    .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
                        }
                        throw $e;
                    }
                }
            }
           
            //Rounding difference of row sums and total sum
            //$diff = $this->sender->getRoundingDiff();
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                    .'Rounding difference original: '."\n".
                    ' orderTotal:'.$this->sender->getOrderTotal()."\n".
                    ' rowSum    :'.$this->sender->getRowSum()."\n".
                    ' rndDiff   :'.$this->sender->getRoundingDiff()."\n");
            }

            $diff = $this->sender->getOrderTotal()-round($this->sender->getRowSum(), 2);
            //die($diff);
           // if (abs($diff) >= 0.04) {
            //    throw new Exception('Too big rounding difference ('.$diff.')'."\n".$this->sender->__toString());
            //}
            if (abs($diff) >= 0.01) {
                $name = trim(Mage::helper('svmmg')->__('Rounding difference'));
                $qty  = 1.0;
                $total = round($diff, 2);
                $sku = trim(Mage::helper('svmmg')->__('Rounding difference'));

                $diffTaxRate = 0.0;

                if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                    Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                        .'Rounding difference row: '."\n".
                        ' name:'.$name."\n".
                        ' qty:'.$qty."\n".
                        ' total:'.$total."\n".
                        ' diffTaxRate:'.$diffTaxRate."\n".
                        ' sku:'.$sku."\n");
                }
                try {
                    $this->sender->addItem(
                        $name, $qty, $total, $diffTaxRate, $sku,1, false);
                } catch (Aivomatic_Fipn_Exception $e) {
                    if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                        Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                                .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
                    }
                    throw $e;
                }
                if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                    Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                        .'Rounding difference fixed: '."\n".
                        ' orderTotal:'.$this->sender->getOrderTotal()."\n".
                        ' rowSum    :'.$this->sender->getRowSum()."\n".
                        ' rndDiff   :'.$this->sender->getRoundingDiff()."\n");
                }
            }
        }
        try {
            
            $formFields = $this->sender->getFormFields();
        } catch (Aivomatic_Fipn_Exception $e) {
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                        .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
            }
            throw $e;
        }
        return $formFields;
    }

    private static function addToDiscountDetails($array, $vatLevel, $discount) {
        if ($discount > 0) {
            $vatClass = number_format($vatLevel, 2, '.', '');
            if (!isset($array[$vatClass])) {
                $array[$vatClass] = $discount;
            } else {
                $array[$vatClass] += $discount;
            }
        }
        return $array;
    }

    /**
     * Instantiate state and set it to state object
     * @param string $paymentAction
     * @param Varien_Object
     */
    public function initialize($paymentAction, $stateObject)   // PayPal
    {
        $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT;
        $stateObject->setState($state);
        $stateObject->setStatus('pending_payment');
        $stateObject->setIsNotified(false);
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                    .'$stateObject: '.print_r($stateObject, true));
        }
    }

    protected function _getCustomerTaxClass()
    {
        $customerGroup = $this->getQuote()->getCustomerGroupId();
        if (!$customerGroup) {
            $customerGroup = Mage::getStoreConfig('customer/create_account/default_group', $this->getQuote()->getStoreId());
        }
        return Mage::getModel('customer/group')->load($customerGroup)->getTaxClassId();
    }

    public function setReceiver(
        Aivomatic_Fipn_Receiver_SuomenVerkkomaksut $receiver)
    {
        $this->receiver = $receiver;
    }

    public function getReceiver()
    {
        return $this->receiver;
    }

    public function getSender()
    {
        return $this->sender;
    }
}