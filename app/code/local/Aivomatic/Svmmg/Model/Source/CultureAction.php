<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: CultureAction.php 1196 2010-05-31 09:28:57Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Model/Source/CultureAction.php $
 * @copyright  Aivomatic Ltd 2013
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Model_Source_CultureAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Aivomatic_Svmmg_Model_Config::CULTURE_TYPE_FI, 'label' => Mage::helper('svmmg')->__('fi_FI')),
            array('value' => Aivomatic_Svmmg_Model_Config::CULTURE_TYPE_US, 'label' => Mage::helper('svmmg')->__('en_US')),
            array('value' => Aivomatic_Svmmg_Model_Config::CULTURE_TYPE_SV, 'label' => Mage::helper('svmmg')->__('sv_SE'))
        );
    }
}