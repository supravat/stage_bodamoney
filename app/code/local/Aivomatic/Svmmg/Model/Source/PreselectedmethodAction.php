<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: ButtongridtypeAction.php 779 2009-05-15 09:57:52Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Model/Source/ButtongridtypeAction.php $
 * @copyright  Aivomatic Ltd 2013
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * 1 = Nordea
 * 2 = Osuuspankki
 * 3 = Sampo Pankki
 * 4 = Tapiola
 * 5 = Ålandsbanken
 * 6 = Handelsbanken
 * 7 = Säästöpankit, paikallisosuuspankit, Aktia, Nooa
 * 8 = Luottokunta
 * 9 = Paypal
 * 10 = S-Pankki
 * 11 = Klarna, Laskulla
 * 12 = Klarna, Osamaksulla
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Model_Source_PreselectedmethodAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => '', 'label' => Mage::helper('svmmg')->__('No preselection')),
            array('value' => '1', 'label' => Mage::helper('svmmg')->__('Nordea Pankki')),
            array('value' => '2', 'label' => Mage::helper('svmmg')->__('Osuuspankki')),
            array('value' => '3', 'label' => Mage::helper('svmmg')->__('Sampo Pankki')),
            array('value' => '4', 'label' => Mage::helper('svmmg')->__('Tapiola Pankki')),
            array('value' => '5', 'label' => Mage::helper('svmmg')->__('Ålandsbanken')),
            array('value' => '6', 'label' => Mage::helper('svmmg')->__('Handelsbanken')),
            array('value' => '7', 'label' => Mage::helper('svmmg')->__('Aktia/SP/POP/Nooa')),
            array('value' => '8', 'label' => Mage::helper('svmmg')->__('Luottokunta')),
            array('value' => '9', 'label' => Mage::helper('svmmg')->__('PayPal')),
            array('value' => '10', 'label' => Mage::helper('svmmg')->__('S-Pankki')),
            array('value' => '11', 'label' => Mage::helper('svmmg')->__('Klarna, Laskulla')),
            array('value' => '12', 'label' => Mage::helper('svmmg')->__('Klarna, Osamaksulla'))
        );
    }
}