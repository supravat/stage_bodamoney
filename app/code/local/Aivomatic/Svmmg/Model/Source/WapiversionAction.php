<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: WapiversionAction.php 1196 2010-05-31 09:28:57Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Model/Source/WapiversionAction.php $
 * @copyright  Aivomatic Ltd 2013
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Model_Source_WapiversionAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Aivomatic_Svmmg_Model_Config::S1, 'label' => Mage::helper('svmmg')->__('wapiS1')),
            array('value' => Aivomatic_Svmmg_Model_Config::E1, 'label' => Mage::helper('svmmg')->__('wapiE1'))
        );
    }
}