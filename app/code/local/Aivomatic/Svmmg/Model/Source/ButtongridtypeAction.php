<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: ButtongridtypeAction.php 779 2009-05-15 09:57:52Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Model/Source/ButtongridtypeAction.php $
 * @copyright  Aivomatic Ltd 2009 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Model_Source_ButtongridtypeAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'vertical', 'label' => Mage::helper('svmmg')->__('vertical')),
            array('value' => 'horizontal', 'label' => Mage::helper('svmmg')->__('horizontal'))
        );
    }
}