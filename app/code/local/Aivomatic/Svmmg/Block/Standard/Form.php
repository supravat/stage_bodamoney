<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: Form.php 53 2008-06-13 08:06:29Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Block/Standard/Form.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Block_Standard_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('svmmg/standard/form.phtml');
        parent::_construct();
    }
}