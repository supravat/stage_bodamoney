<?php
/**
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @version    ver. 1.5.2 - $Id: Redirect.php 1529 2011-05-24 10:36:20Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/svmmg/trunk/base/app/code/local/Aivomatic/Svmmg/Block/Standard/Redirect.php $
 * @copyright  Aivomatic Ltd 2013 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic Svmmg
 *
 * @category   Aivomatic
 * @package    Aivomatic_Svmmg
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Svmmg_Block_Standard_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        
        $standard = Mage::getModel('svmmg/standard');
     
        $form = new Varien_Data_Form();
        $form->setAction($standard->getSvmmgUrl())
            ->setId('svmmg_standard_checkout')
            ->setName('svmmg_standard_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
       
        foreach ($standard->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
       
        $html = '<html><body>';
        $html.= $form->toHtml();
        if ($standard->getEmbedForm()) {
            $options = 'locale:"'.$standard->getSender()->getLocale().'"';
            if (!$standard->getEmbedOptions() == '') {
                $options .= ','.$standard->getEmbedOptions();
            }
            $html.= '<script type="text/javascript" src="https://payment.verkkomaksut.fi/js/sv-widget.min.js"></script>'."\n".
            $html.= '<script type="text/javascript">SV.widget.initWithForm("svmmg_standard_checkout", {'.$options.'});</script>';
        } else {
            $html.= $this->__('You will be redirected to PGW in a few seconds.');
            $html.= '<script type="text/javascript">document.getElementById("svmmg_standard_checkout").submit();</script>';
        }
        $html.= '</body></html>';
       
        if (Mage::getStoreConfig('dev/log/active') AND $standard->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".$standard->getSender()->__toString());
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n".print_r($html, true));
        }
        return $html;
    }
}