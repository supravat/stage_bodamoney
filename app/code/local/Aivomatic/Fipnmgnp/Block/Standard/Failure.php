<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: Failure.php 291 2008-08-19 08:24:55Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/Block/Standard/Failure.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_Block_Standard_Failure extends Mage_Core_Block_Template
{
    /**
     *  Return StatusDetail field value from Response
     *
     *  @return	  string
     */
    public function getErrorMessage ()
    {
        $error = Mage::getSingleton('checkout/session')->getErrorMessage();
        Mage::getSingleton('checkout/session')->unsErrorMessage();
        return $error;
    }

    /**
     * Get continue shopping url
     */
    public function getContinueShoppingUrl()
    {
        return Mage::getUrl('checkout/cart');
    }
}