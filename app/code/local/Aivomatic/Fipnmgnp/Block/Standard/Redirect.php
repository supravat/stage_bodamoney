<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: Redirect.php 298 2008-08-20 05:55:57Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/Block/Standard/Redirect.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_Block_Standard_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        $standard = Mage::getModel('fipnmgnp/standard');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getFipnmgnpUrl())
            ->setId('fipnmgnp_standard_checkout')
            ->setName('fipnmgnp_standard_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($standard->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
        $html = '<html><body>';
        $html.= $this->__('You will be redirected to PGW in a few seconds.');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("fipnmgnp_standard_checkout").submit();</script>';
        $html.= '</body></html>';

        if (Mage::getStoreConfig('dev/log/active') AND $standard->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".$standard->getSender()->__toString());
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n".print_r($html, true));
        }
        return $html;
    }
}