<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: Form.php 291 2008-08-19 08:24:55Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/Block/Standard/Form.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_Block_Standard_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('fipnmgnp/standard/form.phtml');
        parent::_construct();
    }
}