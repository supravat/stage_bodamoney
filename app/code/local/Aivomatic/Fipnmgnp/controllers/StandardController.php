<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: StandardController.php 298 2008-08-20 05:55:57Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/controllers/StandardController.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_StandardController extends Mage_Core_Controller_Front_Action
{
    /**
     * Get singleton with Fipnmgnp standard
     *
     * @return object Aivomatic_Fipnmgnp_Model_Standard
     */
    public function getStandard()
    {
        return Mage::getSingleton('fipnmgnp/standard');
    }

    /**
     * Get Config model
     *
     * @return object Aivomatic_Fipnmgnp_Model_Config
     */
    public function getConfig()
    {
        return $this->getStandard()->getConfig();
    }

    /**
     *  Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return $this->getStandard()->getDebug();
    }

    /**
     * When a customer chooses Fipnmgnp on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setFipnmgnpStandardQuoteId($session->getQuoteId());

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($session->getLastRealOrderId());
        $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('fipnmgnp')->__('Customer was redirected to PGW'),
            true
        );
        $order->save();

        $this->getResponse()
            ->setBody($this->getLayout()
                ->createBlock('fipnmgnp/standard_redirect')
                ->setOrder($order)
                ->toHtml());

        $session->unsQuoteId();
    }

    /**
     * Result response request from Nordea
     *
     */
    public function fipnResponseAction()
    {
        //Debug - $_GET
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n".
                '$_GET: '.print_r($_GET, true));
        }

        //Require Finnish Instant Payment Notification Factory Class
        require_once 'Aivomatic/Fipn/Factory.php';

        //Try to get a Receiver from Fipn_Factory
        try {
            $Receiver = Aivomatic_Fipn_Factory::getReceiver(
                $this->getConfig()->getAuthCode(),
                $_GET,
                'nordeapankki');
        } catch (Aivomatic_Fipn_Exception $e) {
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                //Log Exception
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                          .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
            }
            //Rethrow Exception
            throw $e;
        }

        //Debug - Aivomatic_Fipn_Receiver_PaymentGatewayName
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".
                $Receiver->__toString());
        }

        //Set Receiver for Standard
        $this->getStandard()->setReceiver($Receiver);

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId($this->getStandard()->getReceiver()->getOrderId());

        //Debug - Mage_Sales_Model_Order
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\n".
                'order state:'.$order->getState()."\n".
                'order id:'.$order->getId()."\n".
                'order status label:'.$order->getStatusLabel()."\n".
                'can cancel:'.print_r($order->canCancel(), true)."\n");
        }

        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getFipnmgnpStandardQuoteId(true));

        if (!$order->getId()) {
            //Can't find matching order
            $session->setErrorMessage(
                Mage::helper('fipnmgnp')->__('Unknown order Id').
                ' #'.$this->getStandard()->getReceiver()->getOrderId());
            $redirectTo = 'fipnmgnp/standard/failure';
        } elseif ($order->getState() !== Mage_Sales_Model_Order::STATE_NEW) {
            //Order status has already been changed from new to other statues.
            //Customer propably reloaded the page?
            $msg = Mage::helper('fipnmgnp')->__('Payment status already set');
            $order->addStatusToHistory($order->getStatus(), $msg, true);
            $session->setErrorMessage($msg);
            $redirectTo = 'fipnmgnp/standard/failure';
        } elseif ($this->getStandard()->getReceiver()->getIsSuccess()) {
            //Successful payment
            if ($this->saveInvoice($order)) {
                $order->setState($this->getConfig()->getNewOrderStatus(),
                                 $this->getConfig()->getNewOrderStatus());
            } else {
                $newOrderStatus = $this->getConfig()->getNewOrderStatus() ?
                    $this->getConfig()->getNewOrderStatus() : Mage_Sales_Model_Order::STATE_NEW;
            }
            $order->addStatusToHistory(
                $order->getStatus(),
                Mage::helper('fipnmgnp')->__('Customer successfully returned from PGW'),
                true);
            $order->setEmailSent(true);
            $order->save();
            $order->sendNewOrderEmail();
            Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save();
            $redirectTo = 'checkout/onepage/success';
        } elseif ($this->getStandard()->getReceiver()->getIsCancel()) {
            //Customer has canceled the order during payment
            $msg = Mage::helper('fipnmgnp')->__('Order was canceled by customer');
            $redirectTo = 'checkout/cart';
            $order->cancel();
            $order->addStatusToHistory($order->getStatus(), $msg);
            $order->save();
        } else {
            //An error has happend
            $publicMsg = Mage::helper('fipnmgnp')->__('Error in payment');
            if ($this->getStandard()->getReceiver()->getIsError() AND
                $this->getStandard()->getReceiver()->getIsAuthenticResponse()) {
                //Fipn reports an error
                $publicMsg .= ' '.Mage::helper('fipnmgnp')->__('Technical failure');
            }
            if (!$this->getStandard()->getReceiver()->getIsAuthenticResponse()) {
                //Responce couldn't be authenticated
                $publicMsg .= ' '.Mage::helper('fipnmgnp')->__('Authentication failure');
            }
            $session->setErrorMessage($publicMsg);
            $redirectTo = 'fipnmgnp/standard/failure';
            $order->cancel();
            $order->addStatusToHistory($order->getStatus(), $publicMsg, true);
            $order->save();
        }
        $this->_redirect($redirectTo);
    }

    /**
     *  Save invoice for order
     *
     *  @param    Mage_Sales_Model_Order $order
     *  @return	  boolean Can save invoice or not
     */
    protected function saveInvoice (Mage_Sales_Model_Order $order)
    {
        if ($order->canInvoice()) {
            $convertor = Mage::getModel('sales/convert_order');
            $invoice = $convertor->toInvoice($order);
            foreach ($order->getAllItems() as $orderItem) {
               if (!$orderItem->getQtyToInvoice()) {
                   continue;
               }
               $item = $convertor->itemToInvoiceItem($orderItem);
               $item->setQty($orderItem->getQtyToInvoice());
               $invoice->addItem($item);
            }
            $invoice->collectTotals();
            $invoice->register()->capture();
            Mage::getModel('core/resource_transaction')
               ->addObject($invoice)
               ->addObject($invoice->getOrder())
               ->save();
            //Turha IPN:llä $invoice->sendEmail();
            return true;
        }

        return false;
    }

    /**
     *  Failure Action
     *
     *  @return	  void
     */
    public function failureAction ()
    {
        $session = Mage::getSingleton('checkout/session');

        if (!$session->getErrorMessage()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $this->loadLayout();
        $this->_initLayoutMessages('fipnmgnp/session');
        $this->renderLayout();
    }
}