<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: Config.php 296 2008-08-19 10:39:20Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/Model/Config.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_Model_Config extends Varien_Object
{
    const CURRENCY_CODE     = 'EUR';

    const LANGUAGE_FI = '1';
    const LANGUAGE_EN = '3';
    const LANGUAGE_SE = '2';

    /**
     *  Return config var
     *
     *  @param    string Var key
     *  @param    string Default value for non-existing key
     *  @return	  mixed
     */
    public function getConfigData($key, $default=false)
    {
        if (!$this->hasData($key)) {
             $value = Mage::getStoreConfig('payment/fipnmgnp_standard/'.$key);
             if (is_null($value) || false===$value) {
                 $value = $default;
             }
            $this->setData($key, $value);
        }
        $val = $this->getData($key);
        //Debug
        if (Mage::getStoreConfig('dev/log/active')) {
            Mage::log("\n".__FILE__." (".__LINE__.")\nConfigData::$key:".var_export($val,1).' type:'.gettype($val)."\n");
        }
        return $val;
        //return $this->getData($key);
    }

    /**
     *  Return merchant number
     *
     *  @return   string secret_merchant_id
     */
    public function getMerchantNumber ()
    {
        return $this->getConfigData('public_merchant_id');
    }

    /**
     *  Return authentication code
     *
     *  @return   string secret_merchant_id
     */
    public function getAuthCode ()
    {
        return $this->getConfigData('secret_merchant_id');
    }

    /**
     *  Return description row 1
     *
     *  @return   string Desc Row 1
     */
    public function getDescRow1 ()
    {
        return $this->getConfigData('desc_row1');
    }

    /**
     *  Return description row 2
     *
     *  @return   string Desc Row 2
     */
    public function getDescRow2 ()
    {
        return $this->getConfigData('desc_row2');
    }

    /**
     *  Return language (FI=1 , SV=2, EN=3)
     *
     *  @return   string Language code
     */
    public function getLanguage ()
    {
        return $this->getConfigData('language');
    }

    /**
     *  Return new order status
     *
     *  @return   string New order status
     */
    public function getNewOrderStatus ()
    {
        return $this->getConfigData('order_status_new');
    }

    /**
     *  Return debug flag
     *
     *  @return   boolean Debug flag (0/1)
     */
    public function getDebug ()
    {
        return $this->getConfigData('debug_flag');
    }
}