<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: Standard.php 298 2008-08-20 05:55:57Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/Model/Standard.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @copyright  Copyright (c) 2004-2007 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'fipnmgnp_standard';
    protected $_formBlockType = 'fipnmgnp/standard_form';

    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_order = null;

    protected $receiver = false;
    protected $sender   = false;

    /**
     * HTTP request parameters
     * @var array
     */
    protected $requestParameters           = array();


    /**
     * Get Config model
     *
     * @return object Aivomatic_Fipnmgnp_Model_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('fipnmgnp/config');
    }

    /**
     * Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return $this->getConfig()->getDebug();
    }

    /**
     *  Returns Target URL
     *
     *  @return	  string Target URL
     */
    public function getFipnmgnpUrl ()
    {
        return $url = 'https://solo3.nordea.fi/cgi-bin/SOLOPM01';
    }

    /**
     * Return Total Amount of the order as a formated string
     *
     * @see    http://www.php.net/manual/en/function.sprintf.php
     * @return string Total amount in cents (no desimals)
     */
    protected function getTotalAmount()
    {
        return floatval(sprintf('%.2f', $this->getOrder()->getBaseGrandTotal()));
    }

    /**
     *  Return URL for Fipnmgnp success response
     *
     *  @return	  string URL
     */
    public function getSuccessURL ()
    {
        return Mage::getUrl('fipnmgnp/standard/fipnresponse').'?';
    }

    /**
     *  Return URL for Fipnmgnp cancel response
     *
     *  @return	  string URL
     */
    public function getCancelURL ()
    {
        return Mage::getUrl('fipnmgnp/standard/fipnresponse').'?';
    }

    /**
     *  Return URL for Fipnmgnp error response
     *
     *  @return   string URL
     */
    public function getErrorURL ()
    {
        return Mage::getUrl('fipnmgnp/standard/fipnresponse').'?';
    }

    /**
     * Transaction unique ID sent to payment gateway and sent back by payment
     * gateway for order restore Using created order ID
     *
     *  @return	  string Transaction unique number
     */
    protected function getVendorTxCode()
    {
        return $this->getOrder()->getRealOrderId();
    }

    /**
     * Return Id of the customer of this order
     *
     *  @return   string Customer Id
     */
    protected function getCustomerId()
    {
        return $this->getOrder()->getCustomerId();
    }

    /**
     * Return Order description
     *
     *  @return   string Order description
     */
    protected function getOrderDescription()
    {
        $desc = '';
        if ($this->getConfig()->getDescRow1() != '') {
            $desc .= $this->getConfig()->getDescRow1()."\n";
        }
        if ($this->getConfig()->getDescRow2() != '') {
            $desc .= $this->getConfig()->getDescRow2()."\n";
        }
        //Debug - Mage_Sales_Model_Order
        if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
            Mage::log("\n".__FILE__." (".__LINE__.")\nDesc:".$desc."\n");
        }
        return $desc;
    }

    /**
     * Check that currency code is EUR and return it or throw exception
     *
     * @uses    Aivomatic_Fipnmglk_Model_Config::CURRENCY_CODE
     * @return  string Order currency code ('EUR')
     */
    protected function getCurrencyCode()
    {
        if ($this->getOrder()->getBaseCurrencyCode() != Aivomatic_Fipnmgnp_Model_Config::CURRENCY_CODE) {
            throw new Exception('Currency code ('.$this->getOrder()->getBaseCurrencyCode().') is not '.Aivomatic_Fipnmgnp_Model_Config::CURRENCY_CODE);
        }
        return $this->getOrder()->getBaseCurrencyCode();
    }

    /**
     *  Form block description
     *
     *  @return	 object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('fipnmgnp/form_standard', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());
        return $block;
    }

    /**
     *  Return Order Place Redirect URL
     *
     *  @return	  string Order Redirect URL
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('fipnmgnp/standard/redirect');
    }

    /**
     *  Return Standard Checkout Form Fields for request to Luottokunta
     *
     *  @return   array Array of hidden form fields
     */
    public function getStandardCheckoutFormFields ()
    {
        require_once 'Aivomatic/Fipn/Factory.php';
        try {
            $this->sender = Aivomatic_Fipn_Factory::getSender('nordeapankki');
            $this->getSender()->setData($this->getConfig()->getAuthCode(),
                                        $this->getConfig()->getMerchantNumber(),
                                        $this->getVendorTxCode(),
                                        $this->getTotalAmount(),
                                        $this->getCurrencyCode(),
                                        $this->getSuccessURL(),
                                        $this->getCancelURL(),
                                        $this->getErrorURL(),
                                        $this->getConfig()->getConfigData('secret_id_key_version'),
                                        $this->getConfig()->getLanguage(),
                                        $this->getOrderDescription(),
                                        $this->getConfig()->getConfigData('alt_account'),
                                        $this->getConfig()->getConfigData('alt_name'));
            return $this->getSender()->getFormFields();
        } catch (Aivomatic_Fipn_Exception $e) {
            if (Mage::getStoreConfig('dev/log/active') AND $this->getDebug()) {
                Mage::log("\n".__FILE__." (".__LINE__.")\n".__METHOD__."\n"
                          .'Aivomatic_Fipn_Exception: '.$e->getRawMessage());
            }
            throw $e;
        }
    }

    public function setReceiver(
        Aivomatic_Fipn_Receiver_NordeaPankki $receiver)
    {
        $this->receiver = $receiver;
    }

    public function getReceiver()
    {
        return $this->receiver;
    }

    public function getSender()
    {
        return $this->sender;
    }
}