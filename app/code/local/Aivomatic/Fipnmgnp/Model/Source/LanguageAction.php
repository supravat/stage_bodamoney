<?php
/**
 * @category   Aivomatic
 * @package    FipnMgNp
 * @version    ver. 0.1.1 - $Id: LanguageAction.php 298 2008-08-20 05:55:57Z teemu $
 * @file       $URL: file:///home/teemu/svn/aivomatic/fipnmgnp/trunk/base/app/code/local/Aivomatic/Fipnmgnp/Model/Source/LanguageAction.php $
 * @since      0.0.0+
 * @copyright  Aivomatic Ltd 2008 (http://www.aivomatic.com)
 * @link       http://www.aivomatic.com/
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Aivomatic FipnMgNp
 *
 * @category   Aivomatic
 * @package    FipnMgNp
 * @author     Teemu Mäntynen <teemu.mantynen@aivomatic.com>
 */
class Aivomatic_Fipnmgnp_Model_Source_LanguageAction
{
    public function toOptionArray()
    {
        return array(
            array('value' => Aivomatic_Fipnmgnp_Model_Config::LANGUAGE_FI, 'label' => Mage::helper('fipnmgnp')->__('finnish')),
            array('value' => Aivomatic_Fipnmgnp_Model_Config::LANGUAGE_EN, 'label' => Mage::helper('fipnmgnp')->__('english')),
            array('value' => Aivomatic_Fipnmgnp_Model_Config::LANGUAGE_SE, 'label' => Mage::helper('fipnmgnp')->__('swedish'))
        );
    }
}