<?php

class Creationsoul_Bodaid_Model_Mysql4_Bodaid extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the bodaid_id refers to the key field in your database table.
        $this->_init('bodaid/bodaid', 'bodaid_id');
    }
}