<?php

class Creationsoul_Bodaid_Model_Mysql4_Bodaid_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('bodaid/bodaid');
    }
}