<?php

class Creationsoul_Bodaid_Block_Adminhtml_Bodaid_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('bodaid_form', array('legend'=>Mage::helper('bodaid')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('bodaid')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('bodaid')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('bodaid')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('bodaid')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('bodaid')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('bodaid')->__('Content'),
          'title'     => Mage::helper('bodaid')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getBodaidData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBodaidData());
          Mage::getSingleton('adminhtml/session')->setBodaidData(null);
      } elseif ( Mage::registry('bodaid_data') ) {
          $form->setValues(Mage::registry('bodaid_data')->getData());
      }
      return parent::_prepareForm();
  }
}