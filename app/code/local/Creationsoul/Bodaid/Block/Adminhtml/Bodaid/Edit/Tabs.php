<?php

class Creationsoul_Bodaid_Block_Adminhtml_Bodaid_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('bodaid_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('bodaid')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('bodaid')->__('Item Information'),
          'title'     => Mage::helper('bodaid')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('bodaid/adminhtml_bodaid_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}