<?php

class Creationsoul_Bodaid_Block_Adminhtml_Bodaid_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'bodaid';
        $this->_controller = 'adminhtml_bodaid';
        
        $this->_updateButton('save', 'label', Mage::helper('bodaid')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('bodaid')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('bodaid_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'bodaid_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'bodaid_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('bodaid_data') && Mage::registry('bodaid_data')->getId() ) {
            return Mage::helper('bodaid')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('bodaid_data')->getTitle()));
        } else {
            return Mage::helper('bodaid')->__('Add Item');
        }
    }
}