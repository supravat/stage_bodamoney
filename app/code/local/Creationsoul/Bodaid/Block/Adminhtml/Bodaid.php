<?php
class Creationsoul_Bodaid_Block_Adminhtml_Bodaid extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_bodaid';
    $this->_blockGroup = 'bodaid';
    $this->_headerText = Mage::helper('bodaid')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('bodaid')->__('Add Item');
    parent::__construct();
  }
}