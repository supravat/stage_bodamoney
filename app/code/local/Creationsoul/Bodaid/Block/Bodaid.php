<?php
class Creationsoul_Bodaid_Block_Bodaid extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getBodaid()     
     { 
        if (!$this->hasData('bodaid')) {
            $this->setData('bodaid', Mage::registry('bodaid'));
        }
        return $this->getData('bodaid');
        
    }
}