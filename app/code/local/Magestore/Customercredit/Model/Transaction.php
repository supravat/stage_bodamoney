<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Customercredit Model
 *
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @author      Magestore Developer
 */
class Magestore_Customercredit_Model_Transaction extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('customercredit/transaction');
    }

    protected function _beforeSave() {
        $this->setTransactionTime(now());
        if (!$this->getStatus()) {
            $this->setStatus('Completed');
        }
        return parent::_beforeSave();
    }

    public function addTransactionHistory($customer_id, $transaction_type_id, $transaction_detail, $order_id, $amount_credit,$refer=null,$duedate=null,$customer_name=null,$parent_id=0,$comment="",$email="") {
        
        $customer = Mage::getModel('customer/customer')->load($customer_id);
        $customer_group_id = (float) $customer->getGroupId();
        $spent_credit = 0;
        $received_credit = 0;
        if ($transaction_type_id == Magestore_Customercredit_Model_TransactionType::TYPE_CHECK_OUT_BY_CREDIT) {
            $spent_credit = ($amount_credit > 0) ? $amount_credit : -$amount_credit;
        } elseif ($transaction_type_id == Magestore_Customercredit_Model_TransactionType::TYPE_REFUND_ORDER_INTO_CREDIT) {
            $received_credit = ($amount_credit > 0) ? $amount_credit : -$amount_credit;
        }

        if ($transaction_type_id == Magestore_Customercredit_Model_TransactionType::TYPE_BUY_CREDIT) {
            $received_credit = ($amount_credit > 0) ? $amount_credit : -$amount_credit;
        }
        
        $begin_balance = $customer->getCreditValue();
        $end_balance = $begin_balance + $amount_credit;
        if ($end_balance < 0) {
            $end_balance = 0;
        }
        $transaction = Mage::getModel('customercredit/transaction');
        
            $transaction->setCustomerId($customer_id)
                    ->setTypeTransactionId($transaction_type_id)
                    ->setDetailTransaction($transaction_detail)
                    ->setOrderIncrementId($order_id)
                    ->setAmountCredit($amount_credit)
                    ->setBeginBalance($begin_balance)
                    ->setEndBalance($end_balance)
                    ->setCutomerGroupIds($customer_group_id)
                    ->setSpentCredit($spent_credit)
                    ->setReceivedCredit($received_credit);
               
        try {
            $id=$transaction->save()->getId(); 
          
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            
            $write->update(
                "credit_transaction",
                array("reference" =>$refer,
                      "due_time"=>$duedate,
                      "customer_name"=>$customer_name,
                      "parent_id"=>0,
                      "comment"=>$comment,
                      "customer_email_receive"=>$email
                ),
                "transaction_id=$id"
                );
          
          return $id;
        } catch (Exception $ex) {
            ///echo $ex->getMessage();
        }
    }
    public function addTransactionSubAccountHistory($customer_id, $transaction_type_id, $transaction_detail, $order_id, $amount_credit,$refer=null,$duedate=null,$customer_name=null,$parent_id=NULL,$comment="",$email="") {
        
        $customer = Mage::getModel('sublogin/sublogin')->load($customer_id);
        $customer_group_id = 0;
        $spent_credit = 0;
        $received_credit = 0;
        if ($transaction_type_id == Magestore_Customercredit_Model_TransactionType::TYPE_CHECK_OUT_BY_CREDIT) {
            $spent_credit = ($amount_credit > 0) ? $amount_credit : -$amount_credit;
        } elseif ($transaction_type_id == Magestore_Customercredit_Model_TransactionType::TYPE_REFUND_ORDER_INTO_CREDIT) {
            $received_credit = ($amount_credit > 0) ? $amount_credit : -$amount_credit;
        }

        if ($transaction_type_id == Magestore_Customercredit_Model_TransactionType::TYPE_BUY_CREDIT) {
            $received_credit = ($amount_credit > 0) ? $amount_credit : -$amount_credit;
        }
        
        $begin_balance = $customer->getData("credit_value");
        $end_balance = $begin_balance + $amount_credit;
        if ($end_balance < 0) {
            $end_balance = 0;
        }
        $transaction = Mage::getModel('customercredit/transaction');
        //$data = func_get_args(); 
         //   print_r($data);
        //echo $customer_id.":".$transaction_type_id.":".$transaction_detail.":".$order_id.":".$amount_credit
        //.":".$begin_balance.":".$end_balance.":".$customer_group_id.":".$spent_credit.":".$received_credit;
        //die;
            $transaction->setCustomerId($customer_id)
                    ->setTypeTransactionId($transaction_type_id)
                    ->setDetailTransaction($transaction_detail)
                    ->setOrderIncrementId($order_id)
                    ->setParentId($parent_id)
                    ->setAmountCredit($amount_credit)
                    ->setBeginBalance($begin_balance)
                    ->setEndBalance($end_balance)
                    ->setCutomerGroupIds($customer_group_id)
                    ->setSpentCredit($spent_credit)
                    ->setReceivedCredit($received_credit);
               
        try {
            //$data = func_get_args(); 
           // print_r($data);
           $id=$transaction->save()->getId(); 
            //die($id);
            
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            
            $write->update(
                "credit_transaction",
                array("reference" =>$refer,
                      "due_time"=>$duedate,
                      "customer_name"=>$customer_name,
                      "parent_id"=>$parent_id,
                      "comment"=>$comment,
                  
                      "customer_email_receive"=>$email
                ),
                "transaction_id=$id"
                );
          
          return $id;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            //die;
        }
    }
    public function getTransactionByOrderId($order_id) {
        $transactions = Mage::getModel('customercredit/transaction')->getCollection()
                ->addFieldToFilter('order_increment_id', $order_id);
        return $transactions;
    }

    public function getTransactionCreditMemo($order_id, $type_id) {
        $transactions = Mage::getModel('customercredit/transaction')->getCollection()
                ->addFieldToFilter('order_increment_id', $order_id)
                ->addFieldToFilter('type_transaction_id', $type_id)
                ->addFieldToFilter('amount_credit', array('gt' => 0));
        return $transactions->getSize();
    }
    public function getTransactionPerDay($customer_id,$parent_id){
        $total =0;                
        if(!$parent_id)
        {
            $transactions = Mage::getModel('customercredit/transaction')->getCollection()
                ->addFieldToFilter('customer_id', $customer_id)
                ->addFieldToFilter('type_transaction_id',array("in",array(Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS,Magestore_Customercredit_Model_TransactionType::TYPE_TRANSFER_TOFAMILY,Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER)))
                ->addFieldToFilter('amount_credit', array('neq' => 0))
                ->addFieldToFilter('DATE(transaction_time)',array('eq'=>date("Y-m-d")));
                //->addFieldToFilter('transaction_time', array(
               // 'from'     => strtotime('-1 day', time()),
                //'to'       => time(),
               // 'datetime' => true
                // )); 
                
         }                
        else
        {
            $transactions = Mage::getModel('customercredit/transaction')->getCollection()
                ->addFieldToFilter('customer_id',$customer_id)
                 ->addFieldToFilter('parent_id',$parent_id)
                 ->addFieldToFilter('type_transaction_id',array("in",array(Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS,Magestore_Customercredit_Model_TransactionType::TYPE_TRANSFER_TOFAMILY,Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER)))
                ->addFieldToFilter('amount_credit', array('neq' => 0))              
                 ->addFieldToFilter('DATE(transaction_time)',array('eq'=>date("Y-m-d")));
        }
        if($transactions)
        {              
            foreach($transactions as $_trans){
                
               $total+=abs($_trans->getAmountCredit()) ;
            }
            return $total;
        }
        return 0;
    }
}