<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Customercredit Model
 * 
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @author      Magestore Developer
 */

class Magestore_Customercredit_Model_Customercredit extends Mage_Core_Model_Abstract
{
    public function sendvertifycode($sender,$buyer,$merchange,$orderid,$amount,$code){
         
         if (strpos($sender,'bodamoney.com') !== false) { 
          
            $this->sendvertifybyphone($sender,$buyer,$merchange,$orderid,$amount,$code);
        }else{
          
            $this->sendvertifybyemail($sender,$buyer,$merchange,$orderid,$amount,$code);
        }
    }
    public function sendvertifybyphone($sender,$buyer,$merchange,$orderid,$amount,$code){
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($sender);
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
        $phone=str_replace("@bodamoney.com","",$sender);
       
        $phone=str_replace("+","",$phone);
        $target=$phone;

        $message="This is a security verification step.\n Confirm Payment $amount to $email by using this Confirmation code:\t $code\n";
        $message.="Before you can proceed to send money for  $buyer using your Boda Money account $phone.\n";
              
        shell_exec ("php sms/index.php $username  $password  $target  '$message'");
        return $this;
    }

    public function sendVerifyEmail($email,$value,$message,$keycode)
    {
        $customer_id = Mage::getSingleton('customer/session')->getCustomerId();
        $customerData = Mage::getModel('customer/customer')->load($customer_id);
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $store = Mage::app()->getStore($this->getStoreId());
        $share_url = Mage::getSingleton('core/url')->getUrl('customercredit/index/validateCustomer');
        $veriryurl = Mage::getSingleton('core/url')->getUrl('customercredit/index/sharepost?keycode='.$keycode);
        $veriryurl = substr($veriryurl,0,strlen($veriryurl)-1);
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area'	=> 'frontend',
                'store'	=> $store->getStoreId()
            ));
            $mailTemplate->sendTransactional(
            Mage::helper('customercredit')->getEmailConfig('verify',$store->getStoreId()),
            Mage::helper('customercredit')->getEmailConfig('sender',$store->getStoreId()),
            $customerData->getEmail(),
            $customerData->getName(),
                array(
                'store'		=> $store,
                'recipient_email'     =>$email,
                'value'     =>$this->getLabel($value),
                'send_name' => $send_name,
                'message'   =>$message,
                'emailcode' =>$keycode,
                'verifyurl' =>$veriryurl,
                'share_url' =>$share_url,
            )
        );
        $translate->setTranslateInline(true);
        return $this;
    }
    public function sendVerifySms($phone,$value,$message,$keycode){
            
            $username = Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit'); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
            $password = Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit');
            $w = new WhatsProt($username, 0, "WhatsApp Messaging", true); //Name your application by replacing "WhatsApp Messaging"
            $phone=str_replace("@bodamoney.com","",$email);
            $phone=str_replace("+","",$phone);
            $target=$phone;
            
            $message = "$message.Key code is $creditcode with amount $amount";
            $message=str_replace(" ","aenn",$message);          
            shell_exec ("php sms/index.php $username  $password  $target  $message");
            return $this;
    }
     public function sendmessageSms($phone,$message){
            
            $username = Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit'); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
            $password = Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit');
            $w = new WhatsProt($username, 0, "WhatsApp Messaging", true); //Name your application by replacing "WhatsApp Messaging"
            $phone=str_replace("@bodamoney.com","",$email);
            $phone=str_replace("+","",$phone);
            $target=$phone;
            $message=str_replace(" ","aenn",$message);          
            shell_exec ("php sms/index.php $username  $password  $target  $message");
            return $this;
    }
    public function changeCustomerCredit($credit_amount,$customer_id=null)
    {
        if($customer_id==null)
        	$customer = Mage::helper('customercredit')->getCustomer();
	else
		$customer = Mage::getModel('customer/customer')->load($customer_id);
        $customer->setCreditValue($customer->getCreditValue()+$credit_amount)->save();
    }
     public function changeSubCustomerCredit($credit_amount,$customer_id=null)
    {
        if($customer_id){
           
		      $customer = Mage::getModel('sublogin/sublogin')->load($customer_id);
             
              $customer->setData("credit_value",$customer->getData("credit_value")+$credit_amount)->save();
              // print_r($customer->getData());
              //echo $credit_amount;
              //die;
        }
    }
    public function addCreditToFriend($credit_amount,$customer_id)
    {	
		$friend_account = Mage::getModel('customer/customer')->load($customer_id);
		if(isset($friend_account)){
			$friend_credit_balance = $friend_account->getCreditValue() + $credit_amount;
			$friend_account->setCreditValue($friend_credit_balance);
			try{
				$friend_account->save();
			}
			catch(Exception $e){
				echo $e->getMessage();
			}
		}
    }
    public function addCreditToSubAccountFriend($credit_amount,$customer_id)
    {	
		$friend_account = Mage::getModel('sublogin/sublogin')->load($customer_id);
		if(isset($friend_account)){
			$friend_credit_balance = $friend_account->getCreditValue() + $credit_amount;
			$friend_account->setCreditValue($friend_credit_balance);
			try{
				$friend_account->save();
			}
			catch(Exception $e){
				//echo $e->getMessage();
			}
		}
    }
    public function sendCreditToFriendByEmail($credit_amount,$friend_email,$message,$receiver_id=null)
    {
		$credit_code = Mage::getModel('customercredit/creditcode');
		$code = $credit_code->addCreditCode($friend_email, $credit_amount, Magestore_Customercredit_Model_Status::STATUS_UNUSED);
        
        if (strpos($friend_email,'bodamoney.com') === false) { 
            $this->sendCodeToFriendEmail($friend_email,$credit_amount,$message,$code);
        }else{
            if(!$receiver_id)
            $this->sendCodeToFriendSms($friend_email,$credit_amount,$message,$code);
            else
            {
                 $sub_account=Mage::getModel('sublogin/sublogin')->load($receiver_id);
                 $contact=$sub_account->getData("contact_confirm_ccount");
                 if(!$contact){
                     $this->sendCodeToFriendSms($friend_email,$credit_amount,$message,$code);
                 }else{
                      if($contact=="contact_email")
                      {
                         $friend_email=$sub_account->getData("contact_email");
                         $this->sendCodeToFriendEmail($friend_email,$credit_amount,$message,$code);
                      }elseif($contact=="contact_whatapp"){
                            $phone=$sub_account->getData("contact_whatapp");
                            $phone =str_replace("-","",$phone);
                            $pos   =explode("|",$phone);
                            $friend_email =$pos[0];
                            $this->sendCodeToFriendSms($friend_email,$credit_amount,$message,$code);
                      }else{
                            $phone=$sub_account->getData("contact_telegram");
                            $phone =str_replace("-","",$phone);
                            $pos   =explode("|",$phone);
                            $friend_email =$pos[0];
                            $this->sendCodeToFriendSms($friend_email,$credit_amount,$message,$code);
                      }
                 }
            }
        }
      
    }
    public function sendConfirmToFriendByEmail($credit_amount,$friend_email,$message,$receiver_id=null)
    {
		//$credit_code = Mage::getModel('customercredit/creditcode');
		//$code = $credit_code->addCreditCode($friend_email, $credit_amount, Magestore_Customercredit_Model_Status::STATUS_UNUSED);
        
        if (strpos($friend_email,'bodamoney.com') === false) { 
            $this->sendConfirmToFriendEmail($friend_email,$credit_amount,$message);
        }else{
          
            if(!$receiver_id)
            $this->sendConfirmReceviveToFriendSms($friend_email,$credit_amount);
            else
            {
                 $sub_account=Mage::getModel('sublogin/sublogin')->load($receiver_id);
                 $contact=$sub_account->getData("contact_confirm_ccount");
                 if(!$contact){
                     $this->sendConfirmReceviveToFriendSms($friend_email,$credit_amount);
                 }else{
                      if($contact=="contact_email")
                      {
                         $friend_email=$sub_account->getData("contact_email");
                         $this->sendConfirmToFriendEmail($friend_email,$credit_amount);
                      }elseif($contact=="contact_whatapp"){
                            $phone=$sub_account->getData("contact_whatapp");
                            $phone =str_replace("-","",$phone);
                            $pos   =explode("|",$phone);
                            $friend_email =$pos[0];
                            $this->sendConfirmReceviveToFriendSms($friend_email,$credit_amount);
                      }else{
                            $phone=$sub_account->getData("contact_telegram");
                            $phone =str_replace("-","",$phone);
                            $pos   =explode("|",$phone);
                            $friend_email =$pos[0];
                            $this->sendConfirmReceviveToFriendSms($friend_email,$credit_amount);
                      }
                 }
            }
            //$this->sendConfirmSendingToFriendSms($friend_email,$credit_amount);
        }
      
    }
    public function failConfirmToFriendByEmail($credit_amount,$friend_email,$message)
    {
		//$credit_code = Mage::getModel('customercredit/creditcode');
		///$code = $credit_code->addCreditCode($friend_email, $credit_amount, Magestore_Customercredit_Model_Status::STATUS_UNUSED);        
        if (strpos($friend_email,'bodamoney.com') === false) { 
            $this->failConfirmToFriendEmail($friend_email,$credit_amount,$message);
        }else{          
            
            $this->failConfirmReceviveToFriendSms($friend_email,$credit_amount);
        }
      
    }
    /*****
    **recevie confirm 
    /*****/
    public function receiveConfirmToFriendByEmail($credit_amount,$friend_email,$message,$receiver_id=null)
    {   
       
        if (strpos($friend_email,'bodamoney.com') === false) { 
            $this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount,$message);
        }else{
          
            if(!$receiver_id)
            $this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount);
            else
            {
                 $sub_account=Mage::getModel('sublogin/sublogin')->load($receiver_id);
               
                 $contact=$sub_account->getData("contact_confirm_ccount");
               
                 if(!$contact){
                     $this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount);
                 }else{
                      if($contact=="contact_email")
                      {
                         $friend_email=$sub_account->getData("contact_email");
                         $this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount);
                      }elseif($contact=="contact_whatapp"){
                            $phone=$sub_account->getData("contact_whatapp");
                            $phone =str_replace("-","",$phone);
                            $pos   =explode("|",$phone);
                            $friend_email =$pos[0];
                            
                            $this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount);
                           
                      }else{
                            $phone=$sub_account->getData("contact_telegram");
                            $phone =str_replace("-","",$phone);
                            $pos   =explode("|",$phone);
                            $friend_email =$pos[0];
                            $this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount);
                      }
                 }
            }
            //$this->receiveConfirmSendingToFriendSms($friend_email,$credit_amount);
        }
      
    }
    
	public function sendCreditToFriendByEmailAfterVerify($credit_code_id,$credit_amount,$friend_email,$message){
		$credit_code = Mage::getModel('customercredit/creditcode')->load($credit_code_id);
		if(isset($credit_code) && isset($friend_email) && isset($credit_amount)){
			 $this->sendCodeToFriendEmail($friend_email,$credit_amount,$message,$credit_code->getCreditCode());
		}
	}
    public function sendCodeToFriendEmail($email,$amount,$message,$creditcode)
    {
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $store = Mage::app()->getStore($this->getStoreId());
        $login_url = Mage::getSingleton('core/url')->getUrl('customer/account/login');
        $redeem_page_url = Mage::getSingleton('core/url')->getUrl('customercredit/index/redeem');
        $redeemurl = Mage::getSingleton('core/url')->getUrl('customercredit/index/redeem?code='.$creditcode);
       // $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1).$creditcode;
        $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1);
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area'	=> 'frontend',
                'store'	=> $store->getStoreId()
            ));
         
        $mailTemplate->sendTransactional(
            Mage::helper('customercredit')->getEmailConfig('creditcode',$store->getStoreId()),
            Mage::helper('customercredit')->getEmailConfig('sender',$store->getStoreId()),
            $email,
            $send_name,
            array(
                'store'		=> $store,
                'send_name'     =>$send_name,
		       'receiver_name'=>$receiver_name,
                'value'     =>$amount,
                'login_url'     =>$login_url,
                'redeem_page_url'     =>$redeem_page_url,
                'message'   =>$message,
                'creditcode' =>$creditcode,
                'redeemurl' =>$redeemurl,
            )
        );
        $translate->setTranslateInline(true);
        return $this;
    }
    public function sendConfirmToFriendEmail($email,$amount,$message)
    {
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $store = Mage::app()->getStore($this->getStoreId());
        $login_url = Mage::getSingleton('core/url')->getUrl('customer/account/login');
        $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1);
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area'	=> 'frontend',
                'store'	=> $store->getStoreId()
            ));
  
       
         
        $mailTemplate->sendTransactional(
            Mage::helper('customercredit')->getEmailConfig('verifyvalue',$store->getStoreId()),
            Mage::helper('customercredit')->getEmailConfig('sender',$store->getStoreId()),
            $email,
            $send_name,
            array(
                'store'		=> $store,
                'send_name'     =>$send_name,
		        'receiver_name'=>$receiver_name,
                'value'     =>$amount,
                'login_url'     =>$login_url,
                'name'     =>"Recipient Name",
                'message'   =>$message,
                'balance' =>"Customer Credit Balance",
                'status' =>"Status",
                'expiredat' =>"Expired At",
            )
        );
        $translate->setTranslateInline(true);
        return $this;
    }
    public function failConfirmToFriendEmail($email,$amount,$message)
    {
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $store = Mage::app()->getStore($this->getStoreId());
        $login_url = Mage::getSingleton('core/url')->getUrl('customer/account/login');
        $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1);
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area'	=> 'frontend',
                'store'	=> $store->getStoreId()
            ));
  
       
         
        $mailTemplate->sendTransactional(
            Mage::helper('customercredit')->getEmailConfig('failnotify',$store->getStoreId()),
            Mage::helper('customercredit')->getEmailConfig('sender',$store->getStoreId()),
            $email,
            $send_name,
            array(
                'store'		=> $store,
                'send_name'     =>$send_name,
		        'receiver_name'=>$receiver_name,
                'value'     =>$amount,
                'login_url'     =>$login_url,
                'name'     =>"Recipient Name",
                'message'   =>$message,
                'balance' =>"Customer Credit Balance",
                'status' =>"Status",
                'expiredat' =>"Expired At",
            )
        );
        $translate->setTranslateInline(true);
        return $this;
    }
    /***
    **receive
    **/
    public function receiveConfirmToFriendEmail($email,$amount,$message)
    {
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $store = Mage::app()->getStore($this->getStoreId());
        $login_url = Mage::getSingleton('core/url')->getUrl('customer/account/login');
        $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1);
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);
        $mailTemplate = Mage::getModel('core/email_template')
            ->setDesignConfig(array(
                'area'	=> 'frontend',
                'store'	=> $store->getStoreId()
            ));         
        $mailTemplate->sendTransactional(
            Mage::helper('customercredit')->getEmailConfig('receiveconfirm',$store->getStoreId()),
            Mage::helper('customercredit')->getEmailConfig('sender',$store->getStoreId()),
            $email,
            $send_name,
            array(
                'store'		=> $store,
                'send_name'     =>$send_name,
		        'receiver_name'=>$receiver_name,
                'value'     =>$amount,
                'login_url'     =>$login_url,
                'name'     =>"Recipient Name",
                'message'   =>$message,
                'balance' =>"Customer Credit Balance",
                'status' =>"Status",
                'expiredat' =>"Expired At",
            )
        );
        $translate->setTranslateInline(true);
        return $this;
    }
    public function sendCodeToFriendSms($email,$amount,$message,$creditcode)
    {
       
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $store = Mage::app()->getStore($this->getStoreId());
        $login_url = Mage::getSingleton('core/url')->getUrl('customer/account/login');
        $redeem_page_url = Mage::getSingleton('core/url')->getUrl('customercredit/index/redeem');
        $redeemurl = Mage::getSingleton('core/url')->getUrl('customercredit/index/redeem?code='.$creditcode);
       // $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1).$creditcode;
        $redeemurl = substr($redeemurl,0,strlen($redeemurl)-1);
        $storename =Mage::app()->getStore()->getName();
        //$translate = Mage::getSingleton('core/translate');
        //$translate->setTranslateInline(false);
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
      
        $phone=str_replace("@bodamoney.com","",$email);
        $phone=str_replace("+","",$phone);
        $target=$phone;
        $mail = Mage::getModel('core/email_template');
        $mail->load(10);
      
        $messages  =$mail->getData("template_text");
        $messages  =str_replace('{{$send_name}}',$send_name,$messages);
        $messages  =str_replace('{{$value}}',$amount,$messages);
        $messages  =str_replace('{{$store}}',$storename,$messages);
        $messages =str_replace('{{$redeemurl}}',$redeemurl,$messages);
        $messages  =str_replace('{{$creditcode}}',$creditcode,$messages);
        $messages  =str_replace('{{$receiver_name}}',$phone,$messages);
        if($message)
         $messages  =str_replace('{{$message}}',$message,$messages);
       
        //$message=str_replace(" ","aenn",$message);
        
        shell_exec ('php sms/index.php '.$username.'  '.$password.'  '.$target.'  "'.$messages.'"');
      
        return $this;
    }
    public function sendConfirmReceviveToFriendSms($email,$amount)
    {
       
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
        $phone=str_replace("@bodamoney.com","",$email);
        $storename="Boda Money Store";
        $phone=str_replace("+","",$phone);
        $target=$phone;
        $mail = Mage::getModel('core/email_template');
        $mail->load(8);
        $messages  =$mail->getData("template_text");
        $messages  =str_replace('{{$send_name}}',$send_name,$message);
        $messages  =str_replace('{{$value}}',$amount,$message);
        $messages  =str_replace('{{$store}}',$storename,$message);
        $messages =str_replace('{{$receiver_name}}',$receiver_name,$message);
       
        
        shell_exec ('php sms/index.php '.$username.'  '.$password.'  '.$target.'  "'.$messages.'"');
        //$this->sendConfirmSendingToFriendSms($email,-$amount);
        return $this;
    }
     public function failConfirmReceviveToFriendSms($email,$amount)
    {
       
        $amount        =abs($amount);
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
        $phone=str_replace("@bodamoney.com","",$email);

        $phone=str_replace("+","",$phone);
        $target=$phone;
        $amount = Mage::helper('core')->currency( $amount, true, false);
        
        $message=" We are glad to inform you that  $send_name payment failed due to insufficient funds (not enough money) $amount";
        $message=str_replace(" ","aenn",$message);
       
        shell_exec ('php sms/index.php '.$username.'  '.$password.'  '.$target.'  "'.$message.'"');
        return $this;
    }
     
    public function sendConfirmSendingToFriendSms($email,$amount)
    {
        $amount        =abs($amount);
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
        $phone=str_replace("@bodamoney.com","",$email);

        $phone=str_replace("+","",$phone);
        $target=$phone;
        $amount = Mage::helper('core')->currency( $amount, true, false);
        $message=" We are glad to inform you has just sent   $amount";
        $message=str_replace(" ","aenn",$message);
       
        shell_exec ("php sms/index.php $username  $password  $target  '$message'");
        return $this;
    }
    public function receiveConfirmSendingToFriendSms($email,$amount)
    {
        $amount        =abs($amount);
        $receiver_name = Mage::helper('customercredit')->getNameCustomerByEmail($email);
        $customerData = Mage::helper('customercredit')->getCustomer();
        $send_name = $customerData->getFirstname()." ".$customerData->getLastname();
        $password =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphone_credit')); //Mobile Phone prefixed with country code so for india it will be 91xxxxxxxx
        $username =trim(Mage::helper('customercredit')->getGeneralConfig('whatapphonepassword_credit'));
        $phone=str_replace("@bodamoney.com","",$email);
        $storename=Mage::app()->getStore()->getName();
        $phone=str_replace("+","",$phone);
        $target=$phone;
        $amount = Mage::helper('core')->currency( $amount, true, false);
        $mail = Mage::getModel('core/email_template');
        $mail->load(9);
        $messages  =$mail->getData("template_text");
       
        $messages  =str_replace('{{$send_name}}',$send_name,$messages);
        $messages  =str_replace('{{$value}}',$amount,$messages);
        $messages  =str_replace('{{$strore}}',"Boda Store",$messages);
        $messages =str_replace('{{$receiver_name}}',$receiver_name,$messages);
        shell_exec ('php sms/index.php '.$username.'  '.$password.'  '.$target.'  "'.$messages.'"');      
        return $this;
    }
    public function getBaseLabel($amount)
    {
        $base_currency = Mage::app()->getStore()->getBaseCurrency();
        return $base_currency->format($amount);
    }
    public function getLabel($amount)
    {
        $current_currency = Mage::app()->getStore()->getCurrentCurrency();
        return $current_currency->format($amount);
    }
    public function getBaseCustomerCredit()
    {
        return Mage::helper('customercredit')->getCustomer()->getCreditValue();
    }

    public function getBaseCustomerCreditLabel()
    {
        $customer_credit = $this->getBaseCustomerCredit();
        return $this->getBaseLabel($customer_credit);
    }
    public function getCustomerCredit()
    {
        $store = Mage::app()->getStore();
        $customer_credit = $this->getBaseCustomerCredit();
        return round($store->convertPrice($customer_credit),3);
    }
    public function getCustomerCreditLabel()
    {
        return $this->getLabel($this->getCustomerCredit());
    }
    public function getSubAccountCreditLabel($creditvalue){
        return $this->getLabel($creditvalue);
    }
    public function getConvertedFromBaseCustomerCredit($credit_amount)
    {
        $store = Mage::app()->getStore();
        return $store->convertPrice($credit_amount);
    }
    public function getConvertedToBaseCustomerCredit($credit_amount)
    {
        $rate = Mage::app()->getStore()->convertPrice(1);
        if($rate)
            return $credit_amount/$rate;
        else
        return $credit_amount;
    }
    public function createCreditProduct(){
        $product1 = new Mage_Catalog_Model_Product();
        $product1->setStoreId(0)
                ->setId(null)
                ->setAttributeSetId(9)
                ->setTypeId("customercredit")
                ->setName("Credit product 1")
                ->setSku("creditproduct")
                ->setStatus("1")
                ->setTaxClassId("0")
                ->setVisibility("4")
                ->setEnableGooglecheckout("1")
                ->setCreditAmount("100")
                ->setDescription("Credit product")
                ->setShortDescription("credit")
                ->save();
        $product2 = new Mage_Catalog_Model_Product();
        $product2->setStoreId(0)
                ->setId(null)
                ->setAttributeSetId(9)
                ->setTypeId("customercredit")
                ->setName("Credit product 2")
                ->setSku("creditproduct")
                ->setStatus("1")
                ->setTaxClassId("0")
                ->setVisibility("4")
                ->setEnableGooglecheckout("1")
                ->setCreditAmount("1-10000")
                ->setDescription("Credit product")
                ->setShortDescription("credit")
                ->save();
    }
    
}
