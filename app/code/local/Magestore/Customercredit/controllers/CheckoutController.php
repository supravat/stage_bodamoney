<?php

/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Customercredit Controller
 * 
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @author      Magestore Developer
 */
class Magestore_Customercredit_CheckoutController extends Mage_Core_Controller_Front_Action {

    /**
     * change use customer credit to spend
     */
    public function setAmountPostAction() {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $session = Mage::getSingleton('checkout/session');
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customer_id = $customerData->getId();
            $subtotal = (float) (string) Mage::getSingleton('checkout/session')->getQuote()->getBaseSubtotal();
            $tax = (float) (string) Mage::helper('checkout')->getQuote()->getShippingAddress()->getData('base_tax_amount');
            $shipping = (float) (string) Mage::helper('checkout')->getQuote()->getShippingAddress()->getData('base_shipping_amount');
            $shipping_tax = (float) (string) Mage::helper('checkout')->getQuote()->getShippingAddress()->getData('base_shipping_tax_amount');
            $maxcredit = Mage::helper('customercredit')->getMaxCreditCanUse($customer_id, $subtotal, $tax, $shipping, $shipping_tax);

            if ($session->getData('onestepcheckout_giftwrap')) {
                $maxcredit += $session->getData('onestepcheckout_giftwrap_amount');
            }

            if (is_numeric($request->getParam('customer_credit')) && Mage::helper('customercredit')->getGeneralConfig('enable')) {
                $credit_amount = $request->getParam('customer_credit');
                $base_credit_amount = Mage::getModel('customercredit/customercredit')
                        ->getConvertedToBaseCustomerCredit($credit_amount);
                $base_customer_credit = Mage::getModel('customercredit/customercredit')->getBaseCustomerCredit();

                $base_credit_amount = ($base_credit_amount > $base_customer_credit) ? $base_customer_credit : $base_credit_amount;


                if ($base_credit_amount > $maxcredit) {
                    $session->setBaseCustomerCreditAmount($maxcredit);
                } else {
                    $session->setBaseCustomerCreditAmount($base_credit_amount);
                }
                $session->setUseCustomerCredit(true);


                $this->_redirect('checkout/cart');
            }


            if (is_numeric($request->getParam('credit_amount'))) {
                $amount = $request->getParam('credit_amount');
                $base_amount = Mage::getModel('customercredit/customercredit')
                        ->getConvertedToBaseCustomerCredit($amount);
                $base_customer_credit = Mage::getModel('customercredit/customercredit')->getBaseCustomerCredit();
                $base_credit_amount = ($base_amount > $base_customer_credit) ? $base_customer_credit : $base_amount;
                if ($base_credit_amount > $maxcredit) {
                    $session->setBaseCustomerCreditAmount($maxcredit);
                } else {
                    $session->setBaseCustomerCreditAmount($base_credit_amount);
                }
                $session->setUseCustomerCredit(true);
                $result = array();
                $result['success'] = 1;
                $result['amount'] = Mage::getModel('customercredit/customercredit')
                        ->getConvertedFromBaseCustomerCredit($session->getBaseCustomerCreditAmount());
                $result['price0'] = 0;
                if ($session->getBaseCustomerCreditAmount() == Mage::getSingleton('checkout/session')->getQuote()->getBaseGrandTotal())
                    $result['price0'] = 1;
                
                //Tich hop One step checkout - Marko
                $moduleOnestepActive = Mage::getConfig()->getModuleConfig('Magestore_Onestepcheckout')->is('active', 'true');
                if ($moduleOnestepActive && Mage::getStoreConfig('onestepcheckout/general/active') == '1') {
                    $result['isonestep'] = Mage::getUrl('onestepcheckout/index/save_shipping', array('_secure' => true));
                } else {
                    //update lai payment khi khong co one step
                    Mage::getSingleton('checkout/type_onepage')->getQuote()->collectTotals()->save();
                    $html = $this->_getPaymentMethodsHtml();
                     // $html = $this->getLayout()->createBlock('checkout/onepage_payment')->setTemplate('amasty/amscheckout/onepage/payment.phtml')->toHtml();
                    $result['payment_html'] = $html;
                    $result['review_html'] = $this->_getReviewHtml(); //$this->getLayout()->createBlock('checkout/onepage_review_info')->setTemplate('amasty/amscheckout/onepage/review/info.phtml')->toHtml();
                    }
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }
        }
    }

    protected function _getPaymentMethodsHtml() {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_paymentmethod');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;
    }

    protected function _getReviewHtml(){
        //clear cache aftr change collection - if no magento can't find product in review block
        Mage::app()->getCacheInstance()->cleanType('layout');
        
    
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('checkout_onepage_review');
        $layout->generateXml();
        $layout->generateBlocks();
        $review = $layout->getBlock('root');
        $review->setTemplate('/amasty/amscheckout/onepage/review/info.phtml');
        
        return $review->toHtml();
    }

}
