<?php

/**
 * Magestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Customercredit Index Controller
 *
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @author      Magestore Developer
 */
class Magestore_Customercredit_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
     */
    public function checkemailAction()
    {
        $result = array();
        $email = $this->getRequest()->getParam('email');

        if($pos=strpos($email,"bodamoney.com")==true)
        $email="+".$email;
        $email=str_replace(" ","",$email);
        $email=trim($email);       
        $existed = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email', $email)->getFirstItem()->getId();
        if (!$existed) {     
            $existed = Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email', $email)->getFirstItem()->getId();
        }
        if ($existed)
            $result['existed'] = 1;
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function sendemailAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        Mage::getSingleton('core/session')->setData("sentemail", 'yes');
        Mage::getSingleton('core/session')->setData("is_credit_code", 'yes');
        $email = $this->getRequest()->getParam('email');
        $value = $this->getRequest()->getParam('value');
        $message = $this->getRequest()->getParam('message');
        $ran_num = rand(1, 1000000);
        $keycode = md5(md5(md5($ran_num)));
        Mage::getSingleton('core/session')->setData("emailcode", $keycode);
        Mage::getModel('customercredit/customercredit')->sendVerifyEmail($email, $value,
            $message, $keycode);
        $result = array();
        $result['success'] = 1;
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function indexAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }

    public function shareAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function paymentsdueAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function familyAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function bodacardsAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function loanAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function sharevoucherAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function redeemAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }

    public function redeempostAction()
    {        
        $credit_code = $this->getRequest()->getParam('redeem_credit_code');
        $account_id  =$this->getRequest()->getParam('account_email_input');
        $account_type =$this->getRequest()->getParam('account');
        $sub      =Mage::helper('sublogin')->getCurrentSublogin();
        $parent_id =0;
        $is_sub  =false;
        if($sub){
            $customer_id =$sub->getId();
            $parent_id =$sub->getData("entity_id");
        }else{
            ///
            if($account_type=="main"){
                $customer_id = Mage::getSingleton('customer/session')->getCustomerId();
            }else{
                 $is_sub=true;
                 $customer = Mage::getModel("sublogin/sublogin")->load($account_id);
                 if($customer_id)
                 $customer_id =$customer->getId();         
            }                    
        }
        $credit = Mage::getModel('customercredit/creditcode')->getCollection()->addFieldToFilter('credit_code', $credit_code);
        if ($credit->getSize() == 0) {
            Mage::getSingleton('core/session')->addError('Code is invalid. Please check again!');
            $this->_redirect('customercredit/index/redeem');
        } elseif ($credit->getFirstItem()->getStatus() != 1) {
            Mage::getSingleton('core/session')->addError('Code was used. Please check again!');
            $this->_redirect('customercredit/index/redeem');
        } else {
            Mage::getModel('customercredit/creditcode')->changeCodeStatus($credit->getFirstItem()->getId(), Magestore_Customercredit_Model_Status::STATUS_USED);
            $credit_amount = $credit->getFirstItem()->getAmountCredit();
            if(!$is_sub)
            Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id, Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                "redeem credit by code '" . $credit_code . "'", "", $credit_amount,null,null,null, $parent_id, null, null);
            else
            Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($customer_id, Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,
                "redeem credit by code '" . $credit_code . "'", "", $credit_amount,null,null,null, $parent_id, null, null);
            if(!$sub)
            {  
                if(!$is_sub)
                Mage::getModel('customercredit/customercredit')->changeCustomerCredit($credit_amount);
                else{
                 if($customer_id)
                 Mage::getModel('customercredit/customercredit')->changeSubCustomerCredit($credit_amount,$customer_id);
                }
            }
            else
             Mage::getModel('customercredit/customercredit')->changeSubCustomerCredit($credit_amount,$customer_id);
            Mage::getSingleton('core/session')->addSuccess('Redeem success!');
            $this->_redirect('customercredit/index/index');
        }
    }

    public function listAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        //        if (!Mage::helper('customercredit/account')->isLoggedIn())
        //            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function statementsAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $this->loadLayout();
        $this->renderLayout();
    }

    public function cancelAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $credit_code_id = $this->getRequest()->getParam('id');
        $customer_id = Mage::getSingleton('customer/session')->getCustomerId();
        $add_balance = Mage::getModel('customercredit/creditcode')->load($credit_code_id)->
            getAmountCredit();
        Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,
            Magestore_Customercredit_Model_TransactionType::TYPE_CANCEL_SHARE_CREDIT,
            "cancel share credit ", "", $add_balance);
        Mage::getModel('customercredit/customercredit')->changeCustomerCredit($add_balance);
        Mage::getModel('customercredit/creditcode')->changeCodeStatus($credit_code_id,
            Magestore_Customercredit_Model_Status::STATUS_CANCELLED);
        return $this->_redirect('*/index/share');
    }
    /**
     **delete action
     **/
    public function deleteAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $tran_id = $this->getRequest()->getParam('id');

        $transactions = Mage::getModel('customercredit/transaction')->load($tran_id);
        $transactions->setData("type_transaction_id",Magestore_Customercredit_Model_TransactionType::TYPE_CANCEL_SHARE_CREDIT)->save();
        $transactions->setData("status",Magestore_Customercredit_Model_Status::STATUS_CANCELLED)->save();
      
        return $this->_redirect('*/index/paymentsdue/');
    }

    public function sharepostAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        $parent_id = 0;
        $parent_recevie_id = 0;
        $account = $this->getRequest()->getParam('account');
        $trans  =Mage::getModel('customercredit/transaction');
        $totaltranfer = 0;
        $from_email = "";
        $sub = Mage::helper('sublogin')->getCurrentSublogin();
        $from_is_sub=0;
        $to_is_sub =0;
        /*if($sub){
            $customer=$sub;
            $customer_id=$sub->getId();
            $tranf_limit=$customer->getData("transfer_limit");
            $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
            $customer_email = $customer->getEmail();
            $parent_id      = $customer->getData("entity_id");
            $parent=Mage::getModel('customer/customer');
            $totaltranfer = $trans->getTransactionPerDay($customer_id,$parent_id);
            $from_is_sub=1;
            
        }else{
            $parent = Mage::helper('customercredit')->getCustomer();
            //$parent_id=$parent->getId();
        }*/
        if ($account == "main") {
            $customer = Mage::helper('customercredit')->getCustomer();
            $customer_credit = Mage::getModel('customercredit/customercredit')->getCustomerCredit();
            if ($customer_credit <= 0) {
                Mage::getSingleton('core/session')->addError('Your credit amount not enough to share!');
                return $this->_redirect("customercredit/index/share");
            }
            $tranf_limit=$customer->getData("transfer_limit");
            if(!$tranf_limit){
                $groupId = $customer->getCustomerGroupId();
                $group = Mage::getModel('customer/group')->load($groupId);
                $tranf_limit = $group->getData("transfer_limit");
            }    
            $customer_id = $customer->getId();
            $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
            $customer_email = $customer->getEmail();
            $totaltranfer = $trans->getTransactionPerDay($customer_id,$parent_id);
           
           
        } elseif ($account == "sub") {
            if($sub)
            $customer_id=$sub->getId();
            else
            $customer_id = $this->getRequest()->getParam('account_email_input');
            $from_is_sub=1;
            $customer = Mage::getModel('sublogin/sublogin')->load($customer_id);
            $customer_credit = $customer->getData('credit_value');
            $parent = Mage::helper('customercredit')->getCustomer();
            if ($customer_credit <= 0) {
                Mage::getSingleton('core/session')->addError('Your credit amount not enough to share!');
                return $this->_redirect("customercredit/index/share");
            }
            $tranf_limit = $customer->getTransferLimit();
            $parent_id = $parent->getId();
            $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
            $customer_email = $customer->getEmail();
            $totaltranfer = $trans->getTransactionPerDay($customer_id,$parent_id);
        }

        $credit_code_id = $this->getRequest()->getParam('credit_code_id_hide');
        $due_time = $this->getRequest()->getParam('customercredit_duedate_input');
        $_intduetime = strtotime($due_time);
        $_currenttime = date("d.m.Y");
        $_intcurrenttime = strtotime($_currenttime);
        $message = $this->getRequest()->getParam('customer-credit-share-message');
        ///update field

        $name_receive = $this->getRequest()->getParam('customercredit_fullname_input');
        $refer = $this->getRequest()->getParam('customercredit_reference_input');
        $login  =$this->getRequest()->getParam('login');
        
        ////////////////

        if (Mage::helper('customercredit')->getGeneralConfig('validate')) {

            if (Mage::getSingleton('core/session')->getData("sentemail") != 'yes') {
                return $this->_redirect("customercredit/index/share");
            }
            $keycode = $this->getRequest()->getParam('customercreditcode');
            $email = $this->getRequest()->getParam('email_hide');
            $amount = $this->getRequest()->getParam('amount_hide');
            $amount = Mage::getModel('customercredit/customercredit')->
                getConvertedToBaseCustomerCredit($amount);
            $message = $this->getRequest()->getParam('message_hide');

            if ($email == $customer_email) {
                Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                return $this->_redirect("customercredit/index/share");
            }
            if ($amount < 0 || $amount > $customer_credit) {
                Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                return $this->_redirect("customercredit/index/share");
            }
            $totaltranfer += $amount;
            if (isset($tranf_limit) && $totaltranfer > $tranf_limit) {
                Mage::getSingleton('core/session')->addError('You get tranfer limit!');
                return $this->_redirect("customercredit/index/share");
            }

            $friend_account_id = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email', $email)->getFirstItem()->getId();
            
            
            if (trim($keycode) == trim(Mage::getSingleton('core/session')->getData("emailcode"))) {
                if ($_intduetime <= $_intcurrenttime) {
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $customer_email .
                        " sent " . $amount . " credit to " . $email, "", -$amount, $refer, $customer_name,
                        $parent_id, $comment, $customer_email);
                    Mage::getModel('customercredit/customercredit')->changeCustomerCredit(-$amount);
                } else {
                    $trans_id = Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $customer_email .
                        " sent " . $amount . " credit to " . $email, "", -$amount, $refer, $_intduetime,
                        $customer_name, $parent_id, $comment, $customer_email);
                    Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status",
                        "pending")->save();
                }
                if (isset($friend_account_id)) {
                    if ($_intduetime <= $_intcurrenttime) {
                        Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,
                            Magestore_Customercredit_Model_TransactionType::
                            TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $email . " received " . $amount .
                            " credit from " . $name_receive, "", $amount, $refer, "", $customer_name, $parent_id,
                            $comment);
                        Mage::getModel('customercredit/customercredit')->addCreditToFriend($amount, $friend_account_id);
                    } else {
                        $trans_id = Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,
                            Magestore_Customercredit_Model_TransactionType::
                            TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $email . " received " . $amount .
                            " credit from " . $customer_name, "", $amount, $refer, $_intduetime, $name_receive,
                            $parent_id, $comment);
                        Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status",
                            "pending")->save();

                    }
                } else {
                    if (isset($credit_code_id)) {
                        Mage::getModel('customercredit/creditcode')->changeCodeStatus($credit_code_id,
                            Magestore_Customercredit_Model_Status::STATUS_UNUSED);
                        Mage::getModel('customercredit/customercredit')->
                            sendCreditToFriendByEmailAfterVerify($credit_code_id, $amount, $email, $message);
                    } else {
                        Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,
                            $email, $message);
                    }
                }
                Mage::getSingleton('core/session')->setData("sentemail", 'no');
                Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .
                    ' Success');
                return $this->_redirect("customercredit/index/share");
            } else {
                Mage::getSingleton('core/session')->addError('Invalid verify code. Please check again!');
                return $this->_redirect("customercredit/index/share");
            }
        } else 
        {
            $amount = $this->getRequest()->getParam('customercredit_value_input');        
            $amount = Mage::getModel('customercredit/customercredit')->getConvertedToBaseCustomerCredit($amount);
          
            $message = $this->getRequest()->getParam('customer-credit-share-message');
            $comment =$message;
            $is_sub = false;
            $toemail=$this->getRequest()->getParam('customercredit_email_input');
            if(!$toemail||$toemail=="")
            {   
                $login=$this->getRequest()->getParam('login');
                $toemail=trim($login['country_code'].$login['phone_number']."@bodamoney.com");
            }
            $friend_account_id = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email', $toemail)->getFirstItem()->getId();
            if (!$friend_account_id) {     
                $subaccount= Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email', $toemail)->getFirstItem();
                $friend_account_id=$subaccount->getId();
                if($friend_account_id){
                    $is_sub = true;
                    $parent_recevie_id=$subaccount->getData("entity_id");
                }
            }
            if ($toemail == $customer_email) {
                Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                return $this->_redirect("customercredit/index/share");
            }
            if ($amount < 0 || $amount > $customer_credit) {
                Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                return $this->_redirect("customercredit/index/share");
            }
            ////check resend customer                          
            if ($toemail) {
                $transactions = Mage::getModel('customercredit/transaction')->getCollection();                
                $transactions->addFieldToFilter('customer_email_receive', $toemail)
                             ->addFieldToFilter('customer_id', $customer_id)   
                             ->addFieldToFilter('amount_credit',-$amount)   
                             ->addFieldToFilter('type_transaction_id', array(array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS),
                                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE),
                                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER)))
                             ->setOrder('transaction_time', 'DESC')
                             ->getFirstItem();
                //echo $transactions->getSelect();
                //die;
                $data = $transactions->getData();
                if(isset($data[0])&&$data[0]['transaction_time'])
                $transaction_time = $data[0]['transaction_time'];
                if ($transaction_time) {
                    //echo $transaction_time."--";
                    $to_time = strtotime(date("Y-m-d h:i:s a"));
                    $from_time = strtotime($transaction_time);
                    //echo date("Y-m-d h:i:s a")."--";
                    $mini = round(abs($to_time - $from_time) / 60, 2);
                    //die($mini);
                    if ($mini <= 15) {
                        Mage::getSingleton('core/session')->addError('A payment to the same account has been done less than 15 mins ago. Wait until after 15 mins have elapsed!');
                        return $this->_redirect("customercredit/index/share");
                    }
                }
            }
            $totaltranfer+=$amount;

            
            if (isset($tranf_limit) && $totaltranfer > $tranf_limit) {
                Mage::getSingleton('core/session')->addError('You get tranfer limit!');
                return $this->_redirect("customercredit/index/share");
            }
            
            if ($_intduetime <= $_intcurrenttime) {
                if ($account == "main") {
                    Mage::getModel('customercredit/customercredit')->changeCustomerCredit(-$amount);
                }else{
                    Mage::getModel('customercredit/customercredit')->changeSubCustomerCredit(-$amount,$customer_id);
                }
                if(!$from_is_sub)
                $trans=Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $customer_email .
                                            " sent " . $amount . " credit to " . $toemail, "", -$amount, $refer, "", $customer_name,$parent_id, $comment, $toemail);
                else
                 $trans=Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($customer_id,Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $customer_email .
                                            " sent " . $amount . " credit to " . $toemail, "", -$amount, $refer, "", $customer_name,$parent_id, $comment, $toemail);               
            } else {
                if(!$from_is_sub)
                {
                    $trans_id=Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,
                            Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $customer_email .
                            " sent " . $amount . " credit to " . $toemail, "", -$amount, $refer, $_intduetime,$customer_name, $parent_id, $comment, $toemail);
                            Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status","pending")->save();
                }
                else{
                    $trans_id=Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($customer_id,
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS, $customer_email .
                        " sent " . $amount . " credit to " . $toemail, "", -$amount, $refer, $_intduetime,$customer_name, $parent_id, $comment, $toemail);
                        Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status","pending")->save();
                }
            }            
            if (isset($friend_account_id)) {
                if ($_intduetime <= $_intcurrenttime) {
                   if(!$is_sub)
                   {
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS,$toemail . " received " . $amount ." credit from " .$customer_name, "", $amount, "", "", $name_receive,0,$comment,$customer_email);
                    Mage::getModel('customercredit/customercredit')->addCreditToFriend($amount, $friend_account_id);
                    $receiver  =Mage::getModel('customer/customer')->load($friend_account_id);                  
                    $receiver->setData("credit_value",($amount+$receiver->getData("credit_value")))->save();            
                   } 
                    else
                   {     
                        Mage::getModel('customercredit/customercredit')->addCreditToSubAccountFriend($amount, $friend_account_id);
                        Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS,$toemail . " received " . $amount ." credit from " .$customer_name, "", $amount, "", "", $name_receive,$parent_recevie_id,$comment,$customer_email);
                        $receiver  =Mage::getModel('sublogin/sublogin')->load($friend_account_id);                  
                        //$receiver->setData("credit_value",($amount+$receiver->getData("credit_value")))->save(); 
                    }    ///Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,$toemail, $message);                   
                } else {
                   //if(!$is_sub){
                   //     $trans_id = Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $toemail . " received " . $amount ." credit from " . $name_receive, "", $amount, "", $_intduetime, $name_receive,0, $comment, $customer_email);
                    //}else{
                   //     $trans_id = Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $toemail . " received " . $amount ." credit from " . $name_receive, "", $amount, "", $_intduetime, $name_receive,$parent_recevie_id, $comment, $customer_email);
                   // }
                    //Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status","pending")->save();              
                }
            } 
           else{
                if ($_intduetime <= $_intcurrenttime) 
                        Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,$toemail, $message);              
            }          
            ////
            Mage::getSingleton('core/session')->setData("sentemail", 'no');
             if (strpos($email,'@bodamoney.com') === false)
            Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .' Success');
            else
            Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .'  Success');
            //$this->_redirect("sales/order/history/");
            $this->_redirect("customercredit/");
        }
    }
    /**
     **sharr voucher
     **/
    public function voucherpostAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        $parent_id = 0;
        $parent_recevie_id = 0;
        $account = $this->getRequest()->getParam('account');
        $trans  =Mage::getModel('customercredit/transaction');
        $totaltranfer = 0;
        $from_email = "";
        $sub = Mage::helper('sublogin')->getCurrentSublogin();
        $from_is_sub=0;
        $to_is_sub =0;
        /*if($sub){
            $customer=$sub;
            $customer_id=$sub->getId();
            $tranf_limit=$customer->getData("transfer_limit");
            $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
            $customer_email = $customer->getEmail();
            $parent_id      = $customer->getData();
            $parent=Mage::getModel('customer/customer');
            $totaltranfer = $trans->getTransactionPerDay($customer_id,$parent_id);
            $from_is_sub=1;            
        }else{
            $parent = Mage::helper('customercredit')->getCustomer();
            //$parent_id=$parent->getId();
        }*/
        if ($account == "main") {
            $customer = Mage::helper('customercredit')->getCustomer();
            $customer_credit = Mage::getModel('customercredit/customercredit')->getCustomerCredit();
            if ($customer_credit <= 0) {
                Mage::getSingleton('core/session')->addError('Your credit amount not enough to share!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            $tranf_limit=$customer->getData("transfer_limit");
            if(!$tranf_limit){
                $groupId = $customer->getCustomerGroupId();
                $group = Mage::getModel('customer/group')->load($groupId);
                $tranf_limit = $group->getData("transfer_limit");
            }    
            $customer_id = $customer->getId();
            $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
            $customer_email = $customer->getEmail();
            $totaltranfer = $trans->getTransactionPerDay($customer_id,$parent_id);
           
           
        } elseif ($account == "sub") {
            $sub      =Mage::helper('sublogin')->getCurrentSublogin();
            if($sub)
            $customer_id=$sub->getId();
            else
            $customer_id = $this->getRequest()->getParam('account_email_input');
            $from_is_sub=1;
            $customer = Mage::getModel('sublogin/sublogin')->load($customer_id);
            $customer_credit = $customer->getData('credit_value');
            $parent = Mage::helper('customercredit')->getCustomer();
            if ($customer_credit <= 0) {
                Mage::getSingleton('core/session')->addError('Your credit amount not enough to share!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            $tranf_limit = $customer->getTransferLimit();
            $parent_id = $parent->getId();
            $customer_id = $customer->getId();
            $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
            $customer_email = $customer->getEmail();
            $totaltranfer = $trans->getTransactionPerDay($customer_id, $parent_id);
        }
        else{
            Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
            return $this->_redirect("customercredit/index/sharevoucher");
        }
        $credit_code_id = $this->getRequest()->getParam('credit_code_id_hide');

        $message = "";
        ///update field

        $name_receive = $this->getRequest()->getParam('customercredit_fullname_input');
        $refer = $this->getRequest()->getParam('customercredit_reference_input');
        $login  =$this->getRequest()->getParam('login');
        
        ////////////////

        if (Mage::helper('customercredit')->getGeneralConfig('validate')) {

            if (Mage::getSingleton('core/session')->getData("sentemail") != 'yes') {
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            $keycode = $this->getRequest()->getParam('customercreditcode');
            $email = $this->getRequest()->getParam('email_hide');
            $amount = $this->getRequest()->getParam('amount_hide');
            $amount = Mage::getModel('customercredit/customercredit')->
                getConvertedToBaseCustomerCredit($amount);
            $message = $this->getRequest()->getParam('message_hide');

            if ($email == $customer_email) {
                Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            if ($amount < 0 || $amount > $customer_credit) {
                Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            $totaltranfer += $amount;
            if (isset($tranf_limit) && $totaltranfer > $tranf_limit) {
                Mage::getSingleton('core/session')->addError('You get tranfer limit!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }

            $friend_account_id = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email', $email)->getFirstItem()->getId();
            
            
            if (trim($keycode) == trim(Mage::getSingleton('core/session')->getData("emailcode"))) {
                if ($_intduetime <= $_intcurrenttime) {
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER, $customer_email .
                        " sent " . $amount . " credit to " . $email, "", -$amount, $refer, $customer_name,
                        $parent_id, $comment, $customer_email);
                    Mage::getModel('customercredit/customercredit')->changeCustomerCredit(-$amount);
                } else {
                    $trans_id = Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,
                        Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER, $customer_email .
                        " sent " . $amount . " credit to " . $email, "", -$amount, $refer, $_intduetime,
                        $customer_name, $parent_id, $comment, $customer_email);
                    Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status",
                        "pending")->save();
                }
                if (isset($friend_account_id)) {
                    if ($_intduetime <= $_intcurrenttime) {
                        Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,
                            Magestore_Customercredit_Model_TransactionType::
                            TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $email . " received " . $amount .
                            " credit from " . $name_receive, "", $amount, $refer, "", $customer_name, $parent_id,
                            $comment);
                        Mage::getModel('customercredit/customercredit')->addCreditToFriend($amount, $friend_account_id);
                    } else {
                        $trans_id = Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,
                            Magestore_Customercredit_Model_TransactionType::
                            TYPE_RECEIVE_CREDIT_FROM_FRIENDS, $email . " received " . $amount .
                            " credit from " . $customer_name, "", $amount, $refer, $_intduetime, $name_receive,
                            $parent_id, $comment);
                        Mage::getModel('customercredit/transaction')->load($trans_id)->setData("status",
                            "pending")->save();

                    }
                } else {
                    if (isset($credit_code_id)) {
                        Mage::getModel('customercredit/creditcode')->changeCodeStatus($credit_code_id,
                            Magestore_Customercredit_Model_Status::STATUS_UNUSED);
                        Mage::getModel('customercredit/customercredit')->
                            sendCreditToFriendByEmailAfterVerify($credit_code_id, $amount, $email, $message);
                    } else {
                        Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,
                            $email, $message);
                    }
                }
                Mage::getSingleton('core/session')->setData("sentemail", 'no');
                Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .
                    ' Success');
                return $this->_redirect("customercredit/index/sharevoucher");
            } else {
                Mage::getSingleton('core/session')->addError('Invalid verify code. Please check again!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
        } else {
            $amount = $this->getRequest()->getParam('customercredit_value_input');        
            $amount = Mage::getModel('customercredit/customercredit')->getConvertedToBaseCustomerCredit($amount);
          
            $message = $this->getRequest()->getParam('customer-credit-share-message');
            $is_sub = false;
            $toemail=$this->getRequest()->getParam('customercredit_email_input');
            if(!$toemail)
            {   
                $login=$this->getRequest()->getParam('login');
                $toemail=trim($login['country_code'].$login['phone_number']."@bodamoney.com");
            }
            $friend_account_id = Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email', $toemail)->getFirstItem()->getId();
            if (!$friend_account_id) {     
                $subaccount= Mage::getModel('sublogin/sublogin')->getCollection()->addFieldToFilter('email', $toemail)->getFirstItem();
                $friend_account_id=$subaccount->getId();
                if($friend_account_id){
                    $is_sub = true;
                    $parent_recevie_id=$subaccount->getData("entity_id");
                }
            }
            if ($toemail == $customer_email) {
                Mage::getSingleton('core/session')->addError('Invalid email. Please check again!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            if ($amount < 0 || $amount > $customer_credit) {
                Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }
            ////check resend customer                          
            if ($toemail){
                $transactions = Mage::getModel('customercredit/transaction')->getCollection();                
                $transactions->addFieldToFilter('customer_email_receive', $toemail)
                             ->addFieldToFilter('customer_id', $customer_id)   
                             ->addFieldToFilter('amount_credit',-$amount) 
                ->addFieldToFilter('type_transaction_id', array(array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS),
                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE),
                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER)))
                ->setOrder('transaction_time', 'DESC')
                ->getFirstItem();
                $data = $transactions->getData();
                if(isset($data[0])&&$data[0]['transaction_time'])
                $transaction_time = $data[0]['transaction_time'];
                if ($transaction_time) {
                    //echo $transaction_time."--";
                    $to_time = strtotime(date("Y-m-d h:i:s a"));
                    $from_time = strtotime($transaction_time);
                    //echo date("Y-m-d h:i:s a")."--";
                    $mini = round(abs($to_time - $from_time) / 60, 2);
                    //die($mini);
                    if ($mini <= 15) {
                        Mage::getSingleton('core/session')->addError('A payment to the same account has been done less than 15 mins ago. Wait until after 15 mins have elapsed!');
                        return $this->_redirect("customercredit/index/sharevoucher");
                    }
                }
            }
            $totaltranfer+=$amount;
            if (isset($tranf_limit) && $totaltranfer > $tranf_limit) {
                Mage::getSingleton('core/session')->addError('You get tranfer limit!');
                return $this->_redirect("customercredit/index/sharevoucher");
            }   
            if(!$from_is_sub)
                $trans=Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER, $customer_email .
                                            " sent " . $amount . " credit to " . $toemail, "", -$amount, $refer, "", $customer_name,$parent_id, $comment, $toemail);
                else
                 $trans=Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($customer_id,Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER, $customer_email .
                                            " sent " . $amount . " credit to " . $toemail, "", -$amount, $refer, "", $customer_name,$parent_id,"", $toemail);
                if ($account == "main") {
                    Mage::getModel('customercredit/customercredit')->changeCustomerCredit(-$amount);
                }else{
                    Mage::getModel('customercredit/customercredit')->changeSubCustomerCredit(-$amount,$customer_id);
            }                     
            if (isset($friend_account_id)) {
                   if(!$is_sub)
                   {
                    Mage::getModel('customercredit/transaction')->addTransactionHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS,$toemail . " received " . $amount ." credit from " .$customer_name, "", $amount, "", "", $customer_name, $parent_recevie_id,"",$email);
                    Mage::getModel('customercredit/customercredit')->addCreditToFriend($amount, $friend_account_id);
                        
                   } 
                    else
                   {     
                        Mage::getModel('customercredit/customercredit')->addCreditToSubAccountFriend($amount, $friend_account_id);
                        Mage::getModel('customercredit/transaction')->addTransactionSubAccountHistory($friend_account_id,Magestore_Customercredit_Model_TransactionType::TYPE_RECEIVE_CREDIT_FROM_FRIENDS,$toemail . " received " . $amount ." credit from " .$customer_name, "", $amount, "", "", $customer_name, $parent_recevie_id,"",$email);
                    }    ///Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,$toemail, $message);     
                    Mage::getModel('customercredit/customercredit')->sendConfirmToFriendByEmail($amount,$customer_email,$comment);
                    Mage::getModel('customercredit/customercredit')->receiveConfirmToFriendByEmail($amount,$toemail,$comment);
            } 
           else {              
                Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,$toemail, $message);
            }
            ////
            Mage::getSingleton('core/session')->setData("sentemail", 'no');
             if (strpos($email,'@bodamoney.com') === false)
            Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .' Success');
            else
            Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .'  Success');
            //$this->_redirect("sales/order/history/");
            $this->_redirect("customercredit/");
        }
    }
    /**
     **tranfer money
     ***/
    public function tranferpostAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        $from = $this->getRequest()->getParam('from_main_account');
        $to = $this->getRequest()->getParam('to_main_account');
        $fromparent_id = 0;
        $toparent_id = 0;
        $parent_id =0;
        $sender_id =0;
        $customer_receive_id=0;
        $sub = Mage::helper('sublogin')->getCurrentSublogin();
        if($sub){
            $sender_id=$sub->getId();    
            $customer_credit = $sub->getData("credit_value");
         
            if ($customer_credit <= 0) {
                Mage::getSingleton('core/session')->addError('Your credit amount not enough to share!');
                return $this->_redirect("customercredit/index/family");
            }
            $parent_id =$sub->getData("entity_id");           
            $parent =  Mage::getModel("customer/customer")->load($parent_id);
            $customer=$sub;
        }else{    
            $parent = Mage::helper('customercredit')->getCustomer();
            $parent_id=$parent->getId();
            
            if ($from == "main") {
                $customer = $parent;
                $customer_credit = Mage::getModel('customercredit/customercredit')->getCustomerCredit();              
                $sender_id=$parent->getId();
                //$tranf_limit=$customer->getData("transfer_limit");
                $parent_id=0;
               /// if(!$tranf_limit){
               //     $groupId = $customer->getCustomerGroupId();
                //    $group = Mage::getModel('customer/group')->load($groupId);
                //    $tranf_limit = $group->getData("transfer_limit");
               // }   
            }elseif($from=="sub"){
                $sender_id=$this->getRequest()->getParam('from');
                $customer = Mage::getModel('sublogin/sublogin')->load($sender_id);            
                $customer_credit = $customer->getData("credit_value");
               // $tranf_limit=$customer->getData("transfer_limit");
                
            }
            if ($customer_credit <= 0) {
                Mage::getSingleton('core/session')->addError('Your credit amount not enough to share!');
                return $this->_redirect("customercredit/index/family");
            }
        }
        
        $fromparent_id = $parent_id;
        $customer_id = $customer->getId();
        $customer_name = $customer->getFirstname() . " " . $customer->getLastname();
        $customer_email = $customer->getEmail();
        ///to action
        $toaccount = $this->getRequest()->getParam('to');
        
        if ($to == "main") {
            $email = $toaccount;
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($email); //load customer by email id
            $customer_receive_id=$customer->getId();
        } elseif ($to == "sub") {
            $to_customer = Mage::getModel('sublogin/sublogin')->load($toaccount);           
            $email = $to_customer->getData("email");
            $toparent_id = $to_customer->getData("entity_id");
            $customer_receive_id=$to_customer->getId();
        }
         if ($email) {
                $transactions = Mage::getModel('customercredit/transaction')->getCollection();
                $transactions
                    ->addFieldToSelect('transaction_time')    
                    ->addFieldToFilter('customer_id',$sender_id)
                    ->addFieldToFilter('customer_email_receive',$email)
                    ->addFieldToFilter('type_transaction_id', array(
                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_CREDIT_TO_FRIENDS),
                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_PAYMENT_DUE),
                    array('eq' => Magestore_Customercredit_Model_TransactionType::TYPE_SHARE_VOUCHER)))->setOrder('transaction_time', 'DESC')->getFirstItem();
                $data = $transactions->getData();
                if($data[0]&&isset($data[0]['transaction_time']))
                $transaction_time = $data[0]['transaction_time'];

                if ($transaction_time) {
                    $to_time = strtotime(date("Y-m-d h:i:s a"));
                    $from_time = strtotime($transaction_time);
                    $mini = round(abs($to_time - $from_time) / 60, 2);

                    if ($mini <= 15) {
                        Mage::getSingleton('core/session')->addError('A payment to the same account has been done less than 15 mins ago. Wait until after 15 mins have elapsed!');
                        return $this->_redirect("customercredit/index/sharevoucher");
                    }
                }
        }
        ////
        ////
        $amount = $this->getRequest()->getParam('customercredit_value_input');
        $amount = Mage::getModel('customercredit/customercredit')->getConvertedToBaseCustomerCredit($amount);
        $message = $this->getRequest()->getParam('customer-credit-share-message');

        if ($email == $customer_email) {
            Mage::getSingleton('core/session')->addError('Your credit cannot share to  same account!');
            return $this->_redirect("customercredit/index/family");
        }
        if ($amount < 0 || $amount > $customer_credit) {
            Mage::getSingleton('core/session')->addError('Invalid amount. Please check again!');
            return $this->_redirect("customercredit/index/family");
        }
      
        if ($from == "main") {
            Mage::getModel('customercredit/customercredit')->changeCustomerCredit(-$amount);
        } elseif ($from == "sub") {         
            Mage::getModel('customercredit/customercredit')->changeSubCustomerCredit(-$amount,$customer_id);
        }
        Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_id,Magestore_Customercredit_Model_TransactionType::TYPE_TRANSFER_TOFAMILY, $customer_email ." sent " . $amount . " credit to " . $email, "", -$amount, null, null, $customer_name,$fromparent_id, $message, $email);
        if ($to == "main") {
            Mage::getModel('customercredit/customercredit')->changeCustomerCredit($amount);
        } elseif ($to == "sub") {         
            Mage::getModel('customercredit/customercredit')->changeSubCustomerCredit($amount,$customer_receive_id);            
        }
        Mage::getModel('customercredit/transaction')->addTransactionHistory($customer_receive_id,Magestore_Customercredit_Model_TransactionType::TYPE_REDEEM_CREDIT,"redeem credit by '" . $customer_email . "'", "", $amount, null, null, $customer_name,$toparent_id, $message, $email);
        //Mage::getModel('customercredit/customercredit')->sendCreditToFriendByEmail($amount,$email, $message);

        Mage::getSingleton('core/session')->setData("sentemail", 'no');
        if (strpos($email,'@bodamoney.com') === false)
            Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .
                ' Success');
        else
          {
            $email=str_repeat("@bodamoney.com","",$email);
            Mage::getSingleton('core/session')->addSuccess('Share Credit to ' . $email .
                '  Success');
          }
        //$this->_redirect("sales/order/history/");

        $this->_redirect("customercredit/");
        //}
    }
    public function validateCustomerAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        if ($validate_config = Mage::helper('customercredit')->getGeneralConfig('validate', null) ==
            0) {
            $this->_redirect("customercredit/index/share");
        }
        $sender_id = Mage::getSingleton('customer/session')->getCustomerId();
        $customer = Mage::getModel('customer/customer');
        $sender_email = $customer->load($sender_id)->getEmail();
        $recipient_email = $this->getRequest()->getPost('customercredit_email_input');
        $credit_amount = $this->getRequest()->getPost('customercredit_value_input');
        $is_send_email = Mage::getSingleton('core/session')->getData('is_credit_code');
        $customer_id = $customer->getCollection()->addFieldToFilter('email', $recipient_email)->
            getFirstItem()->getId();
        if ($recipient_email && $credit_amount && !isset($customer_id) && ($is_send_email ==
            'yes')) {
            $credit_code = Mage::getModel('customercredit/creditcode')->addCreditCode($recipient_email,
                $credit_amount, Magestore_Customercredit_Model_Status::
                STATUS_AWAITING_VERIFICATION);
            $credit_code_id = Mage::getModel('customercredit/creditcode')->getCollection()->
                addFieldToFilter('credit_code', $credit_code)->getFirstItem()->getId();
            if (isset($credit_code_id)) {
                $this->getRequest()->setParam('id', $credit_code_id);
            }
        }
        Mage::getSingleton('core/session')->setData("is_credit_code", 'no');
        Mage::getSingleton('core/session')->addSuccess('A verification code has been sent to <a href="mailto:' .
            $sender_email . '"><b>your email</b></a>. Now, please check your email and verify your credit sending!');
        $this->loadLayout();
        $this->renderLayout();
    }

    public function checkCreditAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        $session = Mage::getModel('checkout/session');
        $unchecked = $this->getRequest()->getParam('check_credit');
        if (($unchecked) && ($unchecked == 'unchecked')) {
            $session->setBaseCustomerCreditAmount(0.0);
        }
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array )$modules;
        if (isset($modulesArray['Magestore_Onestepcheckout']) && Mage::getStoreConfig('onestepcheckout/general/active') ==
            '1') {
            $result['isonestep'] = Mage::getUrl('onestepcheckout/index/save_shipping', array
                ('_secure' => true));
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function verifySenderAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }
        if ($validate_config = Mage::helper('customercredit')->getGeneralConfig('validate', null) ==
            0) {
            $this->_redirect("customercredit/index/share");
        }
        $sender_id = Mage::getSingleton('customer/session')->getCustomerId();
        $customer = Mage::getModel('customer/customer');
        $sender_email = $customer->load($sender_id)->getEmail();
        $id = $this->getRequest()->getParam('id');
        $email = $this->getRequest()->getParam('customercredit_email_input');
        $value = $this->getRequest()->getParam('customercredit_value_input');
        if (isset($id) && ($email) && isset($value)) {
            Mage::getSingleton('core/session')->setData("sentemail", 'yes');
            $ran_num = rand(1, 1000000);
            $keycode = md5(md5(md5($ran_num)));
            Mage::getSingleton('core/session')->setData("emailcode", $keycode);
            Mage::getModel('customercredit/customercredit')->sendVerifyEmail($email, $value, null,
                $keycode);
        }
        Mage::getSingleton('core/session')->addSuccess('A verification code has been sent to <a href="mailto:' .
            $sender_email . '"><b>your email</b></a>. Now, please check your email and verify your credit sending!');
        $this->loadLayout();
        $this->renderLayout();
    }
    public function getBalanceLabelAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn()) {
            return $this->_redirect('customer/account/login');
        }

        if ($_POST) {
            $id = $this->getRequest()->getParam('id');
            $account = $this->getRequest()->getParam('main_account');
            if ($account == "main") {
                $val= Mage::getModel('customercredit/customercredit')->getCustomerCreditLabel();
                $customer = Mage::helper('customercredit')->getCustomer();
                $creditValue = $customer->getCreditValue();   
                $data=array("creditvalue"=>$creditValue,"current_currency"=>"$val");
                echo json_encode($data);
            } elseif ($account == "sub") {
                $subaccount = Mage::getModel('sublogin/sublogin')->load($id);
                $creditValue = $subaccount->getCreditValue();                
                $current_currency = Mage::app()->getStore()->getCurrentCurrency();
                $val=$current_currency->format($creditValue);
                $data=array("creditvalue"=>$creditValue,"current_currency"=>"$val");
                echo json_encode($data);

            }
        }
        die;
    }
    public function transactionAction()
    {
        if (!Mage::helper('customercredit/account')->isLoggedIn())
            return $this->_redirect('customer/account/login');
        $tran_id = $this->getRequest()->getParam('id', false);

        if ($tran_id) {
            $transaction = Mage::getModel('customercredit/transaction')->load($tran_id);
            Mage::register("transaction", $transaction);
        }
        
        $this->loadLayout();
        $this->renderLayout();
    }
}
