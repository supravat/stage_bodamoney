<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * Customercredit Block
 * 
 * @category    Magestore
 * @package     Magestore_Customercredit
 * @author      Magestore Developer
 */
class Magestore_Customercredit_Block_Customercredit extends Mage_Core_Block_Template
{
    /**
     * prepare block's layout
     *
     * @return Magestore_Customercredit_Block_Customercredit
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    public function addTopLinkStores() {
        $toplinkBlock = $this->getParentBlock();
        if ($toplinkBlock)
            $toplinkBlock->addLink($this->__('Buy Boda money'), 'customercredit/index/index', $this->__('Buy Boda money'), true, array(), 10);
    }
    public function countPaymentDue(){
        $sub      =Mage::helper('sublogin')->getCurrentSublogin();
        if(!$sub){
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customer_id  =  $customerData->getId();
            $collection =  Mage::getModel('customercredit/transaction')->getCollection()
                          ->addFieldToFilter( 
                                        array('customer_id','parent_id'),
                                        array(
                                            array('eq' =>$customer_id), 
                                            array('eq' => $customer_id) 
                                        )
                              )
                          ->addFieldToFilter('status',
                            array(
                                array('eq' =>'pending')
                            )
                        );
          }else{
             $customer_id=$sub->getId();
             $collection =  Mage::getModel('customercredit/transaction')->getCollection()
                          //->addAttributeToSelect("*")
                          ->addFieldToFilter('customer_id',$customer_id)
                           ->addFieldToFilter('parent_id',array('neq' => 0)
                          )
                          ->addFieldToFilter('status',
                            array(
                                array('eq' =>'pending')
                            )
                        ); 
            
          }                      
        //$collection->setOrder('transaction_time', 'DESC');
        //$customer_id = Mage::getSingleton('customer/session')->getCustomer()->getId();
       /// $collection =  Mage::getModel('customercredit/transaction')->getCollection()
                      //->addAttributeToSelect("*")
        //              ->addFieldToFilter('customer_id',$customer_id)
        //              ->addFieldToFilter('type_transaction_id',
         //               array(
         //                   array('status' =>'pending')
         //               )
         //           );                               
        return $collection->getSize();
    }


}