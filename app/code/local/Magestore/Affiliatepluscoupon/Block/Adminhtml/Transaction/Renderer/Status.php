<?php

class Magestore_Affiliatepluscoupon_Block_Adminhtml_Transaction_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row){
		if ($row->getOrderId()){
		  $array=array(
				1 => Mage::helper('affiliateplus')->__('Completed'),
				2 => Mage::helper('affiliateplus')->__('Pending'),
				3 => Mage::helper('affiliateplus')->__('Canceled'),
                4 => Mage::helper('affiliateplus')->__('On Hold'),
			);
		  $id=$row->getOrderId();
     
	      $order=Mage::getModel("sales/order")->load($id);
          return ($order->getStatus());
        }
        return "";
	}
}