<?php
/**
 * @copyright   Copyright (c) 2009-2012 Amasty (http://www.amasty.com)
 */ 
class Amasty_Payrestriction_Helper_Payment_Data extends Mage_Payment_Helper_Data
{
    protected $_allRules = null;
    
    public function getStoreMethods($store = null, $quote = null)
    {
        
        $methods = parent::getStoreMethods($store, $quote);
        if (!$quote){
            return $methods;
        }
        
        $address = $quote->getShippingAddress();
        foreach ($methods as $k => $method){
            foreach ($this->getRules($address) as $rule){
               if ($rule->restrict($method)){
                   if ($rule->validate($address)){
                       unset($methods[$k]);
                   }//if validate
               }//if restrict
            }//rules        
        }//methods
        
        return $methods;
    }
    
    public function getRules($address)
    {
        if (is_null($this->_allRules)){
            $this->_allRules = Mage::getModel('ampayrestriction/rule')
                ->getCollection()
                ->addAddressFilter($address)
             ;
             if ($this->_isAdmin()){
                 $this->_allRules->addFieldToFilter('for_admin', 1);
             }
             
            $this->_allRules->load();
            foreach ($this->_allRules as $rule){
                $rule->afterLoad(); 
            }
        }
        
        return  $this->_allRules;       
    }
    
    protected function _isAdmin()
    {
        if (Mage::app()->getStore()->isAdmin())
            return true;
        // for some reason isAdmin does not work here
        if (Mage::app()->getRequest()->getControllerName() == 'sales_order_create')
            return true;
            
        return false;
    }    
    
}