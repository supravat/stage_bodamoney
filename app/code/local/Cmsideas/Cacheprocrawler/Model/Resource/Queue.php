<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Model_Resource_Queue extends Mage_Core_Model_Resource_Db_Abstract
{
    public function addToQueue($bind)
    {
        $res     = Mage::getSingleton('core/resource');
        $query   = "" . 'INSERT INTO `' . $res->getTableName('glacecacheprocrawler/queue') . "` (`url`, `rate`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `rate` = ?;";
        $data    = $res->getConnection('core_write');
        $results = $data->query($query, $bind);

        return $results;
    }

    public function cleanQueue()
    {
        $limit = Mage::getStoreConfig('glacecacheprocrawler/queue/queue_limit');
        $res   = Mage::getSingleton('core/resource');
        $data  = $res->getConnection('core_write');
        // get number of rows total
        $query    = 'SELECT COUNT(*) AS count FROM ' . $res->getTableName('glacecacheprocrawler/queue');
        $count    = $data->fetchOne($query);
        $deleting = $count - $limit;

        // empty last rows
        if ($deleting > 0) {
            $query = "" . 'DELETE FROM `' . $res->getTableName('glacecacheprocrawler/queue') . '` WHERE `queue_id`>0 ORDER BY `rate` LIMIT ' . (int)$deleting;
            $data->query($query);
        }
    }

    public function flushQueue()
    {
        $this->_getWriteAdapter()->query('TRUNCATE TABLE ' . $this->getMainTable());

        return $this;
    }

    protected function _construct()
    {
        $this->_init('glacecacheprocrawler/queue', 'queue_id');
    }

}
