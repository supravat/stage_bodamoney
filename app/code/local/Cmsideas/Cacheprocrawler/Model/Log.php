<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Model_Log extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('glacecacheprocrawler/log');
    }
}
