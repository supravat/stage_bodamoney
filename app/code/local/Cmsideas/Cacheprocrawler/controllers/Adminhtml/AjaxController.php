<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Adminhtml_AjaxController extends Mage_Adminhtml_Controller_Action
{

    public function generateAction()
    {
        /** @var Cmsideas_Cacheprocrawler_Model_Observer $observer */
        $observer = Mage::getModel('glacecacheprocrawler/observer');
        $observer->generateQueue();


        $msg = Mage::helper('glacecacheprocrawler')->__('Queue was generated.');
        Mage::getSingleton('adminhtml/session')->addSuccess($msg);

        $this->_redirect('adminhtml/system_config/edit/section/glacecacheprocrawler');

        return true;
    }

    public function processAction()
    {
        /** @var Cmsideas_Cacheprocrawler_Model_Observer $observer */
        $observer = Mage::getModel('glacecacheprocrawler/observer');
        $observer->processQueue();

        $msg = Mage::helper('glacecacheprocrawler')->__('Queue was processed.');
        Mage::getSingleton('adminhtml/session')->addSuccess($msg);

        $this->_redirect('adminhtml/system_config/edit/section/glacecacheprocrawler');

        return true;
    }

}
