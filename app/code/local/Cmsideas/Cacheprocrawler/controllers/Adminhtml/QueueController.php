<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Adminhtml_QueueController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('report/glacecacheprocrawler');
        $this->_addContent($this->getLayout()->createBlock('glacecacheprocrawler/adminhtml_queue'));
        $this->renderLayout();
    }

    protected function _setActiveMenu($menuPath)
    {
        $this->getLayout()->getBlock('menu')->setActive($menuPath);
        $this->_title($this->__('Reports'))->_title($this->__('CACHEPRO Crawler Queue'));

        return $this;
    }

    protected function _title($text = null, $resetIfExists = true)
    {
        if (Mage::helper('glacecacheprocrawler')->isVersionLessThan(1, 4)) {
            return $this;
        }

        return parent::_title($text, $resetIfExists);
    }

    public function flushAction()
    {
        Mage::getResourceModel('glacecacheprocrawler/queue')->flushQueue();
        $this->_redirect('*/*/index');

        return true;
    }
}
