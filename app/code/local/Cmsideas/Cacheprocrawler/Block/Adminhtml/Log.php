<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_log';
        $this->_blockGroup = 'glacecacheprocrawler';
        $this->_headerText = Mage::helper('glacecacheprocrawler')->__('Log');
        $this->_removeButton('add');
    }
}