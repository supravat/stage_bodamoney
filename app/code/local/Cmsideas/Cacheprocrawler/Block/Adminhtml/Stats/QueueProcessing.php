<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Stats_QueueProcessing extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        $this->setHtmlId('processing');
        $this->setTemplate('cmsideas/glacecacheprocrawler/charts/queueProcessing.phtml');

        parent::__construct();
    }

    protected function _toHtml()
    {
        $processing = Mage::getResourceModel('glacecacheprocrawler/log')->getQueueProcessingTime();
        $this->setProcessing($processing);

        return parent::_toHtml();
    }
}
