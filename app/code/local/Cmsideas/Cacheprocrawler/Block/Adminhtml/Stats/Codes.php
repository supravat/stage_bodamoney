<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Stats_Codes extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        $this->setHtmlId('codes');
        $this->setTemplate('cmsideas/glacecacheprocrawler/charts/codes.phtml');

        parent::__construct();
    }

    protected function _toHtml()
    {
        $period = 30;
        $codes  = Mage::getResourceModel('glacecacheprocrawler/log')->getStatusCodes($period);
        $this->setCodes($codes);

        return parent::_toHtml();
    }
}
