<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Stats_Time extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        $this->setHtmlId('time');
        $this->setTemplate('cmsideas/glacecacheprocrawler/charts/time.phtml');

        parent::__construct();
    }

    protected function _toHtml()
    {
        $period = 30;
        $load   = Mage::getResourceModel('glacecacheprocrawler/log')->getPageLoadTime($period);
        $this->setLoad($load);

        return parent::_toHtml();
    }
}
