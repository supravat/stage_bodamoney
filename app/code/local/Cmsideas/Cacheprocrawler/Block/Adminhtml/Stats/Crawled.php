<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Stats_Crawled extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        $this->setHtmlId('crawled');
        $this->setTemplate('cmsideas/glacecacheprocrawler/charts/crawled.phtml');

        parent::__construct();
    }

    protected function _toHtml()
    {
        $period  = 30;
        $crawled = Mage::getResourceModel('glacecacheprocrawler/log')->getCrawledPages($period);
        $this->setCrawled($crawled);

        return parent::_toHtml();
    }
}
