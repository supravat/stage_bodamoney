<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Queue extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_controller = 'adminhtml_queue';
        $this->_blockGroup = 'glacecacheprocrawler';
        $this->_headerText = Mage::helper('glacecacheprocrawler')->__('Queue');
        $this->_removeButton('add');
        $this->_addButton('flush', array(
                'label'   => 'Flush Queue',
                'onclick' => 'setLocation(\'' . Mage::helper("adminhtml")->getUrl("glacecacheprocrawler/adminhtml_queue/flush") . '\')',
            )
        );
    }
}