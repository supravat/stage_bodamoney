<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Queue_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('queueGrid');
        $this->setDefaultSort('rate');
        $this->setDefaultDir('desc');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('glacecacheprocrawler/queue')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $hlp = Mage::helper('glacecacheprocrawler');

        $this->addColumn('url', array(
            'header' => $hlp->__('URL'),
            'align'  => 'left',
            'index'  => 'url',
        )
        );
        $this->addColumn('rate', array(
            'header' => $hlp->__('Rate'),
            'align'  => 'left',
            'index'  => 'rate',
            'width'  => '20px',
        )
        );

        return parent::_prepareColumns();
    }

}