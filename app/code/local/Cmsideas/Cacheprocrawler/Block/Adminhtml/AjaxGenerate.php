<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_AjaxGenerate extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     *  Set template
     */
    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxCheckUrl()
    {
        return Mage::helper('adminhtml')->getUrl('cmsideas/glacecacheprocrawler/adminhtml_ajax/generate');
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
                       ->setData(array(
                           'id'      => 'glacecacheprocrawler_button_generate',
                           'label'   => $this->helper('adminhtml')->__('Generate'),
                           'onclick' => 'queueGenerate();'
                       )
                       );

        return $button->toHtml();
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('cmsideas/glacecacheprocrawler/system/config/buttonGenerate.phtml');
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     *
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

}