<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/
class Cmsideas_Cacheprocrawler_Block_Adminhtml_Log_Filter_Store extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Store
{
    public function getCondition()
    {
        $value = $this->getValue();
        if (is_null($value) || $value == 0) {
            return null;
        }

        return array('or' => array('eq' => $value));
    }
}