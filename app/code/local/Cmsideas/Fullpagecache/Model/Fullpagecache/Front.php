<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Model_Fullpagecache_Front
{
    protected $_debug = null;
    protected $_debugInfo = null;
    protected $_dynamicBlocks = null;

    protected static $_currentCacheKey = null;

    protected static $_storeCode = null;
    protected static $_isMobile = null;

    protected $_cache = null;
    protected $_sessionName = 'frontend';
    protected $_sessionStarted = false;

    protected $_formKey = null;

    protected $_coreInitTime = 0.015; // Used to measure time on PHP<5.4

    const PAGE_LOAD_HIT = 1;
    const PAGE_LOAD_MISS = 2;
    const PAGE_LOAD_NEVER_CACHE = 3;
    const PAGE_LOAD_IGNORE = 4;
    const PAGE_LOAD_HIT_UPDATE = 5;
    const PAGE_LOAD_HIT_SESSION = 6;
    const PAGE_LOAD_IGNORE_PARAM = 7;

    public function __construct()
    {
        if (isset($_SESSION))
            $this->_sessionName = session_name();
    }

    public function getCache()
    {
        if (!$this->_cache)
            $this->_cache = new Cmsideas_Fullpagecache_Model_Fullpagecache();

        return $this->_cache;
    }

    public static function getStoreCode()
    {
        if (self::$_storeCode === null)
        {
            $store = isset($_COOKIE['store']) ? $_COOKIE['store'] : false;

            if (isset($_GET['___store']))
            {
                $code = $_GET['___store'];

                $resource = Mage::getSingleton('core/resource');
                $adapter = $resource->getConnection('core_read');

                $select = $adapter->select()
                    ->from(array('store' => $resource->getTableName('core/store')))
                    ->join(
                        array('group' => $resource->getTableName('core/store_group')),
                        'group.group_id = store.group_id AND group.default_store_id = store.store_id',
                        array()
                    )
                    ->where('store.code = ?', $code)
                ;

                $isDefault = (bool)$adapter->fetchOne($select);

                if ($isDefault)
                    $store = false;
                else
                    $store = $code;
            }

            self::$_storeCode = $store;
        }

        return self::$_storeCode;
    }

    public static function getCustomerGroupId()
    {
        if (isset($_SESSION['customer_base']['customer_group_id']))
            return $_SESSION['customer_base']['customer_group_id'];
        else
            return Mage_Customer_Model_Group::NOT_LOGGED_IN_ID;
    }

    public static function removeDisregardedParams($getParams)
    {
        if ($paramsString = self::_getDbConfig('cmsideasfpc/pages/disregard_params'))
        {
            $params = preg_split('/[,\s]+/', $paramsString, -1, PREG_SPLIT_NO_EMPTY);

            $disregarded = array_intersect_key(array_flip($params), $getParams);

            foreach ($disregarded as $name => $value)
                unset($getParams[$name]);
        }

        ksort($getParams);

        return $getParams;
    }

    public static function getSecureKey()
    {
        return (int)(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off');
    }

    public static function getCacheKey()
    {
        if (self::$_currentCacheKey === null)
        {
            $mobile = self::isMobile() ? 'm' : false;
            $currency = isset($_COOKIE['currency']) ? $_COOKIE['currency'] : false;
            $store = self::getStoreCode();
            $customerGroup = self::getCustomerGroupId();

            $url = strtok($_SERVER['REQUEST_URI'], '?');
            $params = self::removeDisregardedParams($_GET);

            $secure = self::getSecureKey();

            $queryString = '?' . http_build_query($params);

            $key = 'cmsideasfpc_' . $mobile . $currency . $store . $customerGroup . $secure . $_SERVER['HTTP_HOST'] . $url . $queryString;

            self::$_currentCacheKey = sha1($key);
        }

        return self::$_currentCacheKey;
    }

    protected function getSessionSaveMethod()
    {
        return (string)Mage::app()->getConfig()->getNode('global/session_save');
    }

    protected function getSessionSavePath()
    {
        if ($sessionSavePath = Mage::app()->getConfig()->getNode('global/session_save_path'))
        {
            return $sessionSavePath;
        }

        return Mage::getBaseDir('session');
    }

    protected function isAdmin()
    {
        if (preg_match('|/key/\w{32}/|', $_SERVER['REQUEST_URI']))
            return true;

        $config = Mage::app()->getConfig();

        $adminKey = (string)$config->getNode('admin/routers/adminhtml/args/frontName');

        if (preg_match('|^(/\w+\.php)?(/admin)?/'.$adminKey.'(/.*)?$|', $_SERVER['REQUEST_URI']))
            return true;
        if (preg_match('|/adminhtml_|', $_SERVER['REQUEST_URI']))
            return true;
    }

    protected function startSession()
    {
        if (isset($_SESSION))
            return true;

        $moduleName = $this->getSessionSaveMethod();
        switch ($moduleName) {
            case 'db':
                $moduleName = 'user';
                $sessionResource = new Cmsideas_Fullpagecache_Model_Resource_Session();
                $sessionResource->setSaveHandler();
                break;
            case 'user':
                call_user_func($this->getSessionSavePath());
                break;
            case 'files':
                if (!is_writable($this->getSessionSavePath())) {
                    break;
                }
            default:
                session_save_path($this->getSessionSavePath());
                break;
        }
        session_module_name($moduleName);

        session_name($this->_sessionName);

        session_start();

        if ($moduleName == 'files')
            $this->_sessionStarted = true;

        return true;
    }

    protected function closeSession()
    {
        if ($this->_sessionStarted)
        {
            $_SESSION = null;
            session_write_close();
        }
    }

    protected function hasMessages()
    {
        if (isset($_SESSION))
        {
            foreach ($_SESSION as $section)
            {
                if (isset($section['messages']) && $section['messages'] instanceof Mage_Core_Model_Message_Collection)
                {
                    if ($section['messages']->count() > 0)
                        return true;
                }
            }
        }
        return false;
    }

    protected function checkSession()
    {
        $result = (isset($_SESSION['core']['visitor_data']['customer_id'])
            ||
            isset($_SESSION['customer_base']['id'])
            ||
            isset($_SESSION['core']['visitor_data']['quote_id'])
            ||
            isset($_SESSION['checkout']['last_added_product_id'])
            ||
            isset($_SESSION['checkout']['cart_was_updated'])
            ||
            isset($_SESSION['checkout']['checkout_state'])
            ||
            $this->hasMessages()
        );

        return $result;
    }

    protected function containsIgnoredParams()
    {
        if ($paramsString = $this->_getDbConfig('cmsideasfpc/pages/ignored_params'))
        {
            $params = preg_split('/[,\s]+/', $paramsString, -1, PREG_SPLIT_NO_EMPTY);

            if (!empty($params))
            {
                foreach ($params as $param)
                {
                    if (isset($_GET[$param]))
                    {
                        Mage::register('cmsideasfpc_ignored', self::PAGE_LOAD_IGNORE_PARAM, true);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected function ignore()
    {
        if ($_SERVER['REQUEST_METHOD'] !== 'GET')
            return true;

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            return true;

        if ($this->isAdmin())
            return true;

        if ($this->containsIgnoredParams())
            return true;

        if (!$this->isDynamicBlocksEnabled())
        {
            if (isset($_COOKIE[Mage_Persistent_Model_Session::COOKIE_NAME]))
                return true;
        }

        if (!isset($_COOKIE[$this->_sessionName]))
            return false;

        $this->startSession();

        $this->_formKey = isset($_SESSION['core']['_form_key']) ? $_SESSION['core']['_form_key'] : false;

        if (!$this->isDynamicBlocksEnabled())
        {
            if ($this->checkSession())
                return true;
        }

        return false;
    }

    public function isDynamicBlocksEnabled()
    {
        if ($this->_dynamicBlocks === null)
            $this->_dynamicBlocks = (bool)$this->_getDbConfig('cmsideasfpc/general/dynamic_blocks');

        return $this->_dynamicBlocks;
    }

    public function getBlockSessionKey($name)
    {
        $session = new Cmsideas_Fullpagecache_Model_Session();

        if ($session->isBlockUpdated($name))
            $result = isset($_COOKIE[$this->_sessionName]) ? $_COOKIE[$this->_sessionName] : '';
        else
        {
            $persistent = (string)Mage::app()->getConfig()->getNode('global/cmsideasfpc/blocks/'.$name.'/persistent');

            if ($persistent && isset($_COOKIE['persistent_shopping_cart']))
                $result = $_COOKIE['persistent_shopping_cart'];
            else
                $result = self::getCustomerGroupId();
        }

        return $result;
    }

    public function getBlockCacheId($name)
    {
        $blockNode = Mage::app()->getConfig()->getNode('global/cmsideasfpc/blocks/'.$name);
        $scope = $blockNode ? (string) $blockNode['scope'] : '';

        $isUrlScope = ($scope == 'url');

        $session = $this->getBlockSessionKey($name);

        $url = $isUrlScope ? $_SERVER['REQUEST_URI'] : '';

        $mobile = self::isMobile() ? 'm' : false;
        $secure = self::getSecureKey();

        // TODO invalidate blocks cache on currency switch
        $currency = isset($_COOKIE['currency']) ? $_COOKIE['currency'] : false;
        $store = self::getStoreCode();

        $key = $secure.$_SERVER['HTTP_HOST'].$url.$name.$session.$mobile.$currency.$store;

        return sha1($key);
    }

    public function getBlockCacheTag($name)
    {
        $session = $this->getBlockSessionKey($name);

        return 'cmsideasfpc_block_' . sha1($name.$session);
    }

    protected function fetch()
    {
        if ($page = $this->getCache()->load($this->getCacheKey()))
        {
            $sessionRequired = !$this->initFormKey();

            if (!$sessionRequired)
                $page = str_replace('AMFPC_FORM_KEY', $this->_formKey, $page);

            if ($this->isDynamicBlocksEnabled())
            {
                $requiredBlocks = $this->getRequiredBlocks($page);
                $ajaxBlocks = $this->getRequiredAjaxBlocks($page);

                $validBlocks = array();
                $invalidBlocks = array();
                foreach ($requiredBlocks as $name)
                {
                    $id = $this->getBlockCacheId($name);
                    $content = $this->getCache()->load($id);
                    if ($content !== false)
                        $validBlocks[$name] = $content;
                    else
                        $invalidBlocks[] = $name;
                }

                if (!empty($validBlocks))
                {
                    $blockPositions = $this->getTopLevelBlocks($page, array_keys($validBlocks));
                    $page = $this->replaceBlocks($page, $validBlocks, $blockPositions);
                }

                $invalidBlocksPositions = array();
                if (!empty($invalidBlocks))
                {
                    $invalidBlocksPositions = $this->getTopLevelBlocks($page, $invalidBlocks);
                }

                if (!empty($invalidBlocksPositions))
                {
                    $ajaxPositions = $this->getTopLevelBlocks($page, $ajaxBlocks);
                    $blockPositions = array_merge($ajaxPositions, $invalidBlocksPositions);

                    $info = array(
                        'page' => $page,
                        'positions' => $blockPositions,
                    );

                    Mage::register('cmsideasfpc_blocks', $info, true);
                    return false;
                }
                else
                {
                    if (!empty($ajaxBlocks))
                        $page = $this->addAjaxLoad($page, $ajaxBlocks);
                }
            }

            if ($sessionRequired)
            {
                Mage::register('cmsideasfpc_page', $page, true);
                return false;
            }

            if (isset($_GET['___store']))
                $_COOKIE['store'] = self::getStoreCode();

            $this->debug($page, 'Early page load');
            $this->addLoadTimeInfo($page);

            return $page;
        }
        else
        {
            Mage::register('cmsideasfpc_preserve', true, true);
            return false;
        }
    }

    protected static function _getDbConfig($path)
    {
        $resource = Mage::getSingleton('core/resource');
        $adapter = $resource->getConnection('core_read');

        $select = $adapter->select()
            ->from($resource->getTableName('core/config_data'), 'value')
            ->where('path=?', $path)
        ;

        return $adapter->fetchOne($select);
    }

    public function allowedDebugInfo()
    {
        if ($this->_debugInfo === null)
        {
            $ips = $this->_getDbConfig('cmsideasfpc/debug/ip');
            $ips = preg_split('/[,\s]+/', $ips, -1, PREG_SPLIT_NO_EMPTY);

            $this->_debugInfo = empty($ips) || in_array($_SERVER['REMOTE_ADDR'], $ips);
        }

        return $this->_debugInfo;
    }

    public function isDebugEnabled()
    {
        if ($this->_debug === null)
            $this->_debug = $this->_getDbConfig('cmsideasfpc/debug/hints') && $this->allowedDebugInfo();

        return $this->_debug;
    }

    public function addLoadTimeInfo(&$html, $type = self::PAGE_LOAD_HIT)
    {
        global $cmsideasfpc_start_time;

        $displayPopup = $this->allowedDebugInfo() && $this->_getDbConfig('cmsideasfpc/debug/load_time');
        $displayHidden = $this->_getDbConfig('cmsideasfpc/debug/hidden_stats');

        if (!$displayHidden && !$displayPopup)
            return;

        if ($_SERVER['REQUEST_METHOD'] !== 'GET')
            return;

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            return;

        if (isset($_SERVER['REQUEST_TIME_FLOAT']))
        {
            $time = round(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 3);
        }
        else if ($cmsideasfpc_start_time)
        {
            $time = round(microtime(true) - $cmsideasfpc_start_time + $this->_coreInitTime, 3);
        }
        else
            return;

        switch ($type)
        {
            case self::PAGE_LOAD_MISS:
                $typeTitle = "Cache Miss";
                break;
            case self::PAGE_LOAD_NEVER_CACHE:
                $typeTitle = "Never cache";
                break;
            case self::PAGE_LOAD_IGNORE:
                $typeTitle = "In ignore list";
                break;
            case self::PAGE_LOAD_HIT_UPDATE:
                $typeTitle = "Cache Hit<br/>(with block updates)";
                break;
            case self::PAGE_LOAD_HIT_SESSION:
                $typeTitle = "Cache Hit<br/>(with session initialization)";
                break;
            case self::PAGE_LOAD_IGNORE_PARAM:
                $typeTitle = "Contains ignored params";
                break;
            default:
                $typeTitle = "Cache Hit";
                break;
        }

        $popup = <<<POPUP
<style type="text/css">
.cmsideasfpc-info
{
    background: none repeat scroll 0 0 #fff;
    border: 2px solid #ed6502;
    bottom: 10px;
    color: #000;
    font-size: 32px;
    min-height: 95px;
    position: fixed;
    right: 10px;
    width: 350px;
    z-index: 99999;
}

.cmsideasfpc-info h1
{
    background: #ed6502;
    color: #fff;
    padding:5px;
    font-size: 14px;
    font-weight: bold;
}

.cmsideasfpc-info h2
{
    margin-top: 12px;
    text-align: center;
    font-size: 30px;
    font-weight: 400;
    color: rgba(255, 255, 255, 0.7);
}

.help-box .content
{
    padding: 2px 5px 10px 5px;
}

.cmsideasfpc-info .content
{
    line-height: 48px;
    padding:10px;
}

</style>
<div class="cmsideasfpc-info">
    <h1>Full Page Cache</h1>
    <div class="content">
        <div>$typeTitle</div>
        <strong>Load Time: </strong>{$time}s
    </div>
</div>
POPUP;

        $hiddenHtml = '<!-- AMFPC|' . strip_tags($typeTitle) . '|' . $time . 's -->';

        $resultHtml = '</body>';

        if ($displayHidden)
            $resultHtml = $hiddenHtml . $resultHtml;

        if ($displayPopup)
            $resultHtml = $popup . $resultHtml;

        $html = str_replace('</body>', $resultHtml, $html);
    }

    public function debug(&$html, $message)
    {
        if (!$this->isDebugEnabled())
            return false;

        $html = <<<HTML
<div style="position:relative; border:1px dotted red; margin:6px 2px; padding:18px 2px 2px 2px; zoom:1;">
<div style="position:absolute; left:0; top:0; padding:2px 5px; background:red; color:white; font:normal 11px Arial;
text-align:left !important; z-index:998;" onmouseover="this.style.zIndex='999'"
onmouseout="this.style.zIndex='998'" >FPC: {$message}</div>$html</div>
HTML;
    }

    protected function addAjaxLoad($page, $names)
    {
        $names = implode(',', $names);
        $js = <<<AJAX
<script type="text/javascript">
new Ajax.Request('{$_SERVER['REQUEST_URI']}', {
    parameters: {cmsideasfpc_ajax_blocks: '$names'},
    onSuccess: function(response) {
        var blocks = response.responseText.evalJSON();
        for (var name in blocks)
        {
            $$('cmsideasfpc[name='+name+']').each(function(element){
                element.replace(blocks[name]);
            });
        }
    }
});
</script>
AJAX;

        $page = str_replace("</head>", "\n$js\n</head>", $page);

        return $page;
    }

    protected function getRequiredAjaxBlocks($page)
    {
        $fullpagecacheConfig = new Cmsideas_Fullpagecache_Model_Config();
        $config = $fullpagecacheConfig->getConfig();

        $ajaxBlocks = $config['ajax_blocks'];
        if (!$this->hasMessages())
        {
            if (isset($ajaxBlocks['global_messages']))
                unset($ajaxBlocks['global_messages']);

            if (isset($ajaxBlocks['messages']))
                unset($ajaxBlocks['messages']);
        }

        $ajaxBlocks = array_keys($ajaxBlocks);

        $ajaxBlockPositions = $this->getTopLevelBlocks($page, $ajaxBlocks);
        $ajaxBlocks = array_intersect($ajaxBlocks, array_keys($ajaxBlockPositions));

        return $ajaxBlocks;
    }

    protected function getRequiredBlocks($page)
    {
        $fullpagecacheConfig = new Cmsideas_Fullpagecache_Model_Config();
        $config = $fullpagecacheConfig->getConfig();

        $blocks = array_keys($config['blocks']);

        $blockPositions = $this->getTopLevelBlocks($page, $blocks);
        $blocks = array_intersect($blocks, array_keys($blockPositions));

        return $blocks;
    }

    public function replaceBlocks($page, $blocks, $positions)
    {
        foreach ($positions as $name => $position)
        {
            if (isset($blocks[$name]))
            {
                $content = $blocks[$name];
                $this->debug($content, $name);

                $page = substr_replace($page, $content, $position['begin'], $position['end'] - $position['begin']);
            }
        }

        return $page;
    }

    protected function getTopLevelBlocks($page, $names)
    {
        $blocks = array();

        foreach ($names as $name)
        {
            $begin = strpos($page, "<div><!-- AMFPC_BEGIN[$name] -->");

            $endToken = "<!-- AMFPC_END[$name] --></div>";
            $end = strpos($page, $endToken);

            if ($begin !== false && $end !== false)
                $blocks[] = array('begin' => $begin, 'end' => $end + strlen($endToken), 'name' => $name);
        }

        usort($blocks, array($this, '_beginComparator'));

        $result = array();
        $prevEnd = false;
        foreach ($blocks as $block)
        {
            if (!$prevEnd || $prevEnd <= $block['begin'])
            {
                $result[$block['name']] = $block;
                $prevEnd = $block['end'];
            }
        }

        // replace blocks from bottom to up to preserve positions
        uasort($result, array($this, '_reversedBeginComparator'));

        return $result;
    }

    protected function initFormKey()
    {
        $version = Mage::getVersionInfo();

        if ($version['minor'] >= 8 && !isset($_COOKIE[$this->_sessionName]))
        {
            if ($this->_getDbConfig('cmsideasfpc/robots/boost_robots'))
            {
                if (!isset($_SERVER['HTTP_USER_AGENT']))
                    return false;

                $agents = $this->getRobotAgents();
                if (!$agents)
                    return false;

                return in_array($_SERVER['HTTP_USER_AGENT'], $agents);
            }
            else
                return false;
        }

        return true;
    }

    public function _beginComparator($a, $b)
    {
        return $a['begin'] < $b['begin'] ? -1 : 1;
    }

    public function _reversedBeginComparator($a, $b)
    {
        return $a['begin'] > $b['begin'] ? -1 : 1;
    }

    protected function getRobotAgents()
    {
        return array(
            'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
            'Googlebot/2.1 (+http://www.googlebot.com/bot.html)',
            'Googlebot/2.1 (+http://www.google.com/bot.html)',
            'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)',
            'Mozilla/5.0 (compatible; bingbot/2.0 +http://www.bing.com/bingbot.htm)',
            'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)',
        );
    }

    public static function isMobile()
    {
        if (self::$_isMobile === null)
        {
            self::$_isMobile = false;

            if (isset($_SERVER['HTTP_USER_AGENT']) && self::_getDbConfig('cmsideasfpc/mobile/enabled'))
            {
                $regexp = self::_getDbConfig('cmsideasfpc/mobile/agents');

                if (@preg_match('@' . $regexp . '@', $_SERVER['HTTP_USER_AGENT']))
                {
                    self::$_isMobile = true;
                }
            }
        }

        return self::$_isMobile;
    }

    public function extractContent()
    {
        if (!Mage::app()->useCache('cmsideasfpc'))
            return false;

        global $cmsideasfpc_start_time;
        $cmsideasfpc_start_time = microtime(true);

        if (!$this->ignore())
        {
            $content = $this->fetch();

            if ($content)
            {
                Mage::app()->getResponse()->appendBody($content)->sendResponse();
                exit;
            }
        }

        $this->closeSession();

        return false;
    }
}
