<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Model_Stats extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('cmsideasfpc/stats');
    }
}
