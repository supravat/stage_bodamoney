<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Model_Observer
{
    protected $_showBlockNames = false;

    public function __construct()
    {
        if (Mage::getSingleton('cmsideasfpc/fullpagecache_front')->allowedDebugInfo())
        {
            if (Mage::getStoreConfig('cmsideasfpc/debug/block_info'))
            {
                $this->_showBlockNames = true;
                Mage::app()->getCacheInstance()->banUse('block_html');
            }
        }
    }

    public function actionPredispatch($observer)
    {
        if (Mage::app()->getStore()->isAdmin())
            return;

        if ($page = Mage::registry('cmsideasfpc_page'))
        {
            $key = Mage::getSingleton('core/session')->getFormKey();
            $page = str_replace('AMFPC_FORM_KEY', $key, $page);

            /** @var Cmsideas_Fullpagecache_Model_Fullpagecache_Front $front */
            $front = Mage::getSingleton('cmsideasfpc/fullpagecache_front');
            $front->debug($page, 'Session Initialized');
            $front->addLoadTimeInfo($page, Cmsideas_Fullpagecache_Model_Fullpagecache_Front::PAGE_LOAD_HIT_SESSION);

            echo $page;
            exit;
        }

        $request = $observer->getData('controller_action')->getRequest();
        Mage::getSingleton('cmsideasfpc/fullpagecache')->validateBlocks($request);
    }

    public function afterToHtml($observer)
    {
        if (!Mage::app()->useCache('cmsideasfpc'))
            return;

        if (Mage::app()->getRequest()->isAjax())
            return;

        $block = $observer->getBlock();

        if ($this->_showBlockNames == false)
        {
            if (Mage::getStoreConfig('cmsideasfpc/general/dynamic_blocks'))
            {
                /** @var Cmsideas_Fullpagecache_Model_Config $config */
                $config = Mage::getSingleton('cmsideasfpc/config');

                if ($config->blockIsDynamic($block, $isAjax, $tags))
                {
                    $name = $block->getNameInLayout();

                    if (in_array($name, array('global_messages', 'messages')) && $block->getData('cmsideasfpc_wrapped'))
                        return;

                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    Mage::getSingleton('cmsideasfpc/fullpagecache')->saveBlockCache($name, $html, $tags);

                    $html = "<div><!-- AMFPC_BEGIN[$name] -->$html<!-- AMFPC_END[$name] --></div>";

                    if ($isAjax)
                        $html = "<cmsideasfpc name=\"$name\">$html</cmsideasfpc>";

                    $block->setData('cmsideasfpc_wrapped', true);

                    $transport->setHtml($html);
                }
            }
        }
        else
        {
            if ($block instanceof Mage_Core_Block_Template)
            {
                $transport = $observer->getTransport();

                $html = $transport->getHtml();

                $border = <<<BORDER
            <div class="cmsideasfpc-block-info" style="position:relative; border:1px dotted #008080; margin:6px 2px; padding:18px 2px 2px 2px; zoom:1;" onmouseover="this.style['border-color']='orange'"
onmouseout="this.style['border-color']='#008080'">
<div style="position:absolute; left:0; top:-17px; padding:2px 5px; background:#008080; color:white; font:normal 11px Arial;
text-align:left !important; z-index:998;">{$block->getNameInLayout()}</div>
BORDER;

                $transport->setHtml($border.$html.'</div>');
            }
        }
    }

    public function layoutRenderBefore()
    {
        $request = Mage::app()->getRequest();

        if ($dynamicBlocks = Mage::registry('cmsideasfpc_blocks'))
        {
            $layout = Mage::app()->getLayout();
            $page = $dynamicBlocks['page'];

            /** @var Cmsideas_Fullpagecache_Model_Fullpagecache_Front $front */
            $front = Mage::getSingleton('cmsideasfpc/fullpagecache_front');

            foreach ($dynamicBlocks['positions'] as $name =>$position)
            {
                $block = $layout->getBlock($name);
                if ($block)
                {
                    $html = $block->toHtml();
                    $front->debug($html, $name . ' (refresh)');
                    $page = substr_replace($page, $html, $position['begin'], $position['end'] - $position['begin']);
                }
            }

            $front->debug($page, 'Late page load');
            $front->addLoadTimeInfo($page, Cmsideas_Fullpagecache_Model_Fullpagecache_Front::PAGE_LOAD_HIT_UPDATE);

            Mage::app()->getResponse()->setBody($page)->sendResponse();
            exit;
        }
        else if ($request->isAjax())
        {
            $blocks = $request->getParam('cmsideasfpc_ajax_blocks');
            if ($blocks)
            {
                Mage::app()->setUseSessionVar(false);

                $blocks = explode(',', $blocks);

                $result = array();
                $layout = Mage::app()->getLayout();
                foreach ($blocks as $name)
                {
                    $block = $layout->getBlock($name);
                    if ($block)
                    {
                        $content = Mage::getSingleton('core/url')->sessionUrlVar($block->toHtml());

                        $result[$name] = $content;
                    }
                }

                $blocksJson = Mage::helper('core')->jsonEncode($result);

                Mage::app()->getResponse()->setBody($blocksJson)->sendResponse();
                exit;
            }
        }
    }

    protected function _canPreserve()
    {
        if (!Mage::registry('cmsideasfpc_preserve'))
            return false;

        if (Mage::app()->getResponse()->getHttpResponseCode() != 200)
            return false;

        if ($this->_showBlockNames)
            return false;

        if ($layout = Mage::app()->getLayout())
        {
            if ($block = $layout->getBlock('messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
            if ($block = $layout->getBlock('global_messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
        }
        else
            return false;

        return true;
    }

    public function setResponse($response, $html, $status)
    {
        /** @var Cmsideas_Fullpagecache_Model_Fullpagecache_Front $front */
        $front = Mage::getSingleton('cmsideasfpc/fullpagecache_front');
        
        $html = preg_replace(
            '|<div><!-- AMFPC_BEGIN\[([^\]]+)\] -->(.*?)<!-- AMFPC_END\[\1\] --></div>|s',
            '\2',
            $html
        );

        $front->addLoadTimeInfo($html, $status);
        
        $response->setBody($html);
    }
    
    public function onFrontSendResponseBefore($observer)
    {
        if (Mage::app()->getRequest()->getModuleName() == 'api')
            return;

        if (!Mage::app()->useCache('cmsideasfpc'))
            return;

        if (Mage::app()->getStore()->isAdmin())
            return;

        if (Mage::app()->getRequest()->isAjax())
            return;

        // No modifications in response till here

        Mage::getSingleton('core/session')->getFormKey(); // Init form key

        $page = $observer->getFront()->getResponse()->getBody();

        if ($ignoreStatus = Mage::registry('cmsideasfpc_ignored'))
        {
            $this->setResponse($observer->getFront()->getResponse(), $page, Cmsideas_Fullpagecache_Model_Fullpagecache_Front::PAGE_LOAD_IGNORE_PARAM);

            return;
        }

        $tags = Mage::getSingleton('cmsideasfpc/config')->matchRoute($observer->getFront()->getAction()->getRequest());

        if (!$tags && !Mage::getStoreConfig('cmsideasfpc/pages/all'))
        {
            $this->setResponse($observer->getFront()->getResponse(), $page, Cmsideas_Fullpagecache_Model_Fullpagecache_Front::PAGE_LOAD_NEVER_CACHE);

            return;
        }

        if (!$tags && Mage::helper('cmsideasfpc')->inIgnoreList())
        {
            $this->setResponse($observer->getFront()->getResponse(), $page, Cmsideas_Fullpagecache_Model_Fullpagecache_Front::PAGE_LOAD_IGNORE);
            return;
        }


        if ($this->_canPreserve())
        {
            $tags[] = Cmsideas_Fullpagecache_Model_Fullpagecache::CACHE_TAG;
            $tags[] = Mage_Core_Block_Abstract::CACHE_GROUP;

            $lifetime = +Mage::getStoreConfig('cmsideasfpc/general/page_lifetime');
            $lifetime *= 3600;

            Mage::getSingleton('cmsideasfpc/fullpagecache')->saveCache($page, $tags, $lifetime);
        }

        $this->setResponse($observer->getFront()->getResponse(), $page, Cmsideas_Fullpagecache_Model_Fullpagecache_Front::PAGE_LOAD_MISS);
    }

    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        Mage::getSingleton('cmsideasfpc/fullpagecache')->getFrontend()->clean(Zend_Cache::CLEANING_MODE_OLD);
    }

    public function flushOutOfStockCache($observer)
    {
        $item = $observer->getItem();

        if ($item->getStockStatusChangedAutomatically())
        {
            $tags = array('catalog_product_' . $item->getProductId(), 'catalog_product');
            //Mage::getSingleton('cmsideasfpc/fullpagecache')->clean($tags);
            Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
        }

        return $item;
    }

    public function onApplicationCleanCache($observer)
    {
        $tags = $observer->getTags();

        /**
         * @var Cmsideas_Fullpagecache_Model_Fullpagecache $fullpagecache
         */
        $fullpagecache = Mage::getSingleton('cmsideasfpc/fullpagecache');

        if (!empty($tags))
        {
            if (!is_array($tags))
            {
                $tags = array($tags);
            }

            $productIds = array();
            foreach ($tags as $tag)
            {
                if (preg_match('/^catalog_product_(?P<id>\d+)$/i', $tag, $matches))
                {
                    $productIds[] = +$matches['id'];
                }
            }

            $additionalTags = $fullpagecache->getProductsAdditionalTags($productIds);

            if (!empty($additionalTags))
            {
                $tags = array_merge($tags, $additionalTags);
            }
        }

        $fullpagecache->clean($tags);
    }

    public function onModelSaveBefore($observer)
    {
        $object = $observer->getObject();

        if (class_exists('Mirasvit_AsyncCache_Model_Asynccache', false)
            && $object instanceof Mirasvit_AsyncCache_Model_Asynccache)
        {
            if ($object->getData('status') == Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS &&
                $object->getOrigData('status') != Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS)
            {
                Mage::getSingleton('cmsideasfpc/fullpagecache')->getFrontend()->clean($object->getMode(), $object->getTagArray(), true);
            }
        }
    }

    public function onCustomerLogin($observer)
    {
        $customer = $observer->getCustomer();

        Mage::getSingleton('customer/session')
            ->setCustomerGroupId($customer->getGroupId());
    }

    public function onReviewSaveAfter($observer)
    {
        $review = $observer->getObject();

        if ($review->getEntityId() == 1) // Product
            Mage::getSingleton('cmsideasfpc/fullpagecache')->clean('catalog_product_' . $review->getEntityPkValue());
    }

    public function onQuoteSaveAfter($observer)
    {
        Mage::helper('cmsideasfpc')->invalidateBlocksWithAttribute('cart');
    }

    public function onCustomerLoginLogout($observer)
    {
        Mage::helper('cmsideasfpc')->invalidateBlocksWithAttribute('customer');
    }
}
