<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Model_Resource_Session extends Mage_Core_Model_Resource_Session
{
    public function getLifeTime()
    {
        $stores = Mage::app()->getStores();
        if (empty($stores))
            return self::SEESION_MAX_COOKIE_LIFETIME;
        else
            return parent::getLifeTime();
    }
}
