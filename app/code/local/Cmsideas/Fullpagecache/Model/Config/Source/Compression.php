<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Model_Config_Source_Compression
{
    const COMPRESSION_NONE = 0;
    const COMPRESSION_PAGES = 1;
    const COMPRESSION_ALL = 2;

    public function toOptionArray()
    {
        $hlp = Mage::helper('cmsideasfpc');
        $vals = array(
            self::COMPRESSION_NONE   => $hlp->__('None'),
            self::COMPRESSION_PAGES  => $hlp->__('Compress pages'),
            self::COMPRESSION_ALL    => $hlp->__('Compress pages and blocks'),
        );

        $options = array();
        foreach ($vals as $k => $v)
            $options[] = array(
                'value' => $k,
                'label' => $v
            );

        return $options;
    }
}
