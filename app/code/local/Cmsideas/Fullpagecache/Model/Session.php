<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Model_Session
{
    public function __construct()
    {
        if (isset($_SESSION))
        {
            if (!isset($_SESSION['cmsideasfpc']))
                $_SESSION['cmsideasfpc'] = array('updated_blocks' => array());
        }
    }

    public function getUpdatedBlocks()
    {
        if (isset($_SESSION))
            return $_SESSION['cmsideasfpc']['updated_blocks'];
        else
            return array();
    }

    public function updateBlock($name)
    {
        if (!in_array($name, $_SESSION['cmsideasfpc']['updated_blocks']))
            $_SESSION['cmsideasfpc']['updated_blocks'][] = $name;
    }

    public function isBlockUpdated($name)
    {
        if (isset($_SESSION) && isset($_SESSION['cmsideasfpc']['updated_blocks']))
            return in_array($name, $_SESSION['cmsideasfpc']['updated_blocks']);
        else
            return false;
    }
}