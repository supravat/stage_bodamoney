<?php
/*
 * Developer: Rene Voorberg
* Team site: http://cmsideas.net/
* Support: http://support.cmsideas.net/
*
*
*/

class Cmsideas_Fullpagecache_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function inIgnoreList()
    {
        $ignore = Mage::getStoreConfig('cmsideasfpc/pages/ignore_list');
        $ignoreList = preg_split('|[\r\n]+|', $ignore, -1, PREG_SPLIT_NO_EMPTY);

        $path = Mage::app()->getRequest()->getOriginalPathInfo();

        foreach ($ignoreList as $pattern)
        {
            if (preg_match("|$pattern|", $path))
                return true;
        }

        return false;
    }

    public function isPageCompressionEnabled()
    {
        $entities = Mage::getStoreConfig('cmsideasfpc/compression/entities');

        return in_array($entities, array(
            Cmsideas_Fullpagecache_Model_Config_Source_Compression::COMPRESSION_ALL,
            Cmsideas_Fullpagecache_Model_Config_Source_Compression::COMPRESSION_PAGES
        ));
    }

    public function isBlockCompressionEnabled()
    {
        $entities = Mage::getStoreConfig('cmsideasfpc/compression/entities');

        $c = Mage::getModel('cmsideasfpc/config_source_compression');
        return $entities == Cmsideas_Fullpagecache_Model_Config_Source_Compression::COMPRESSION_ALL;
    }

    public function invalidateBlocksWithAttribute($attribute)
    {
        $config = Mage::getSingleton('cmsideasfpc/config')->getConfig();

        foreach ($config['blocks'] as $name => $block)
        {
            if (isset($block['@']) && isset($block['@'][$attribute]))
            {
                Mage::getSingleton('cmsideasfpc/fullpagecache')->removeBlockCache($name);
                Mage::getSingleton('cmsideasfpc/session')->updateBlock($name);
            }
        }
    }
}
