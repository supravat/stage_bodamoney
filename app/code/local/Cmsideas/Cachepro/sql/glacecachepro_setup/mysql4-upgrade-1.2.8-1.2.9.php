<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/
$this->startSetup();

$this->run("
CREATE TABLE `{$this->getTable('glacecachepro/url')}` (
  `url_id`       int(10) unsigned NOT NULL auto_increment,
  `url`          varchar(255) NOT NULL,
  `rate`         int(11) unsigned NOT NULL,

  PRIMARY KEY (`url_id`),
  UNIQUE KEY (`url`)
) ENGINE=InnoDB;
");

$this->endSetup();
