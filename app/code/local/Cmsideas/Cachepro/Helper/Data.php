<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/

class Cmsideas_Cachepro_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function inIgnoreList()
    {
        $ignore = Mage::getStoreConfig('glacecachepro/pages/ignore_list');
        $ignoreList = preg_split('|[\r\n]+|', $ignore, -1, PREG_SPLIT_NO_EMPTY);

        $path = Mage::app()->getRequest()->getOriginalPathInfo();

        foreach ($ignoreList as $pattern)
        {
            if (preg_match("|$pattern|", $path))
                return true;
        }

        return false;
    }

    public function isPageCompressionEnabled()
    {
        $entities = Mage::getStoreConfig('glacecachepro/compression/entities');

        return in_array($entities, array(
            Cmsideas_Cachepro_Model_Config_Source_Compression::COMPRESSION_ALL,
            Cmsideas_Cachepro_Model_Config_Source_Compression::COMPRESSION_PAGES
        ));
    }

    public function isBlockCompressionEnabled()
    {
        $entities = Mage::getStoreConfig('glacecachepro/compression/entities');

        return $entities == Cmsideas_Cachepro_Model_Config_Source_Compression::COMPRESSION_ALL;
    }

    public function invalidateBlocksWithAttribute($attributes)
    {
        $config = Mage::getSingleton('glacecachepro/config')->getConfig();

        if (!is_array($attributes))
            $attributes = array($attributes);

        foreach ($config['blocks'] as $name => $block)
        {
            foreach ($attributes as $attribute)
            {
                if (isset($block['@']) && isset($block['@'][$attribute]))
                {
                    Mage::getSingleton('glacecachepro/session')->updateBlock($name);
                    Mage::getSingleton('glacecachepro/cachepro')->removeBlockCache($name);

                    break;
                }
            }
        }
    }

    public function cutHoles(&$html)
    {
        $stack = array();

        if (preg_match_all('#(<(?P<tag>glacecachepro(?:_ajax)?)[\s\n]+(?P<attributes>[^>]*?)>|</glacecachepro(?:_ajax)?>)#s',
            $html,
            $matches,
            PREG_OFFSET_CAPTURE | PREG_PATTERN_ORDER))
        {
            for ($i = sizeof($matches[1]) - 1; $i >= 0; $i--)
            {
                if (!is_array($matches['tag'][$i]) || $matches['tag'][$i][1] < 0) // Closing tag
                {
                    array_push($stack, $matches[1][$i][1] + strlen($matches[1][$i][0]));
                }
                else // Opening tag
                {
                    $startPosition = $matches[1][$i][1];
                    $endPosition = array_pop($stack);

                    if (sizeof($stack) == 0) // Top level of nesting
                    {
                        $attributes = $matches['attributes'][$i][0];
                        $tag = $matches['tag'][$i][0];
                        $html = substr_replace($html, "<{$tag} {$attributes} />", $startPosition, $endPosition - $startPosition);
                    }
                }
            }
        }
    }

    public function replaceFormKey($html)
    {
        $key = Mage::getSingleton('core/session')->getFormKey();
        $html = str_replace('CMSIDEAS_FORM_KEY', $key, $html);

        return $html;
    }
}
