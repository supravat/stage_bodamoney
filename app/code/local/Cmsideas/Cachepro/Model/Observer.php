<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/

class Cmsideas_Cachepro_Model_Observer
{
    protected $_showBlockNames = false;
    protected $_showBlockTemplates = false;
    protected $_blockHtmlCache = array();
    protected $_ajaxBlocks = null;

    public function __construct()
    {
        try
        {
            if (
                Mage::app()->useCache('glacecachepro')
                &&
                Mage::getStoreConfig('glacecachepro/debug/block_info')
                &&
                Mage::getSingleton('glacecachepro/cachepro_front')->allowedDebugInfo()
                &&
                !Mage::app()->getStore()->isAdmin()
            )
            {
                $this->_showBlockNames = true;
                Mage::app()->getCacheInstance()->banUse('block_html');
            }

            $this->_showBlockTemplates = Mage::getStoreConfig('glacecachepro/debug/block_templates');
        }
        catch (Mage_Core_Model_Store_Exception $e) // Stores aren't initialized
        {
            $this->_showBlockNames = $this->_showBlockTemplates = false;
        }
    }

    public function actionPredispatch($observer)
    {
        if (Mage::app()->getStore()->isAdmin())
            return;

        if ($page = Mage::registry('glacecachepro_page'))
        {
            $page = Mage::helper('glacecachepro')->replaceFormKey($page);

            /** @var Cmsideas_Cachepro_Model_Cachepro_Front $front */
            $front = Mage::getSingleton('glacecachepro/cachepro_front');
            $front->debug($page, 'Session Initialized');

            $response = Mage::app()->getResponse();

            $this->setResponse($response, $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_HIT_SESSION);

            $response->sendHeaders();
            $response->outputBody();

            exit;
        }

        $request = $observer->getData('controller_action')->getRequest();
        Mage::getSingleton('glacecachepro/cachepro')->validateBlocks($request);
    }

    public function afterToHtml($observer)
    {
        if (!Mage::app()->useCache('glacecachepro'))
            return;

        if (Mage::app()->getRequest()->isAjax() && $this->_ajaxBlocks === null)
            return;

        $block = $observer->getBlock();

        if ($this->_showBlockNames == false)
        {
            if (Mage::getStoreConfig('glacecachepro/general/dynamic_blocks'))
            {
                /** @var Cmsideas_Cachepro_Model_Config $config */
                $config = Mage::getSingleton('glacecachepro/config');

                if ($config->blockIsDynamic($block, $isAjax, $tags, $children))
                {
                    $name = $block->getNameInLayout();

                    if (in_array($name, array('global_messages', 'messages')) && $block->getData('glacecachepro_wrapped'))
                        return;

                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    Mage::getSingleton('glacecachepro/cachepro')->saveBlockCache($name, $html, $tags);

                    if (($this->_ajaxBlocks !== null) && array_key_exists($name, $this->_ajaxBlocks))
                    {
                        $this->_ajaxBlocks[$name] = $html;
                    }

                    $tag = ($isAjax ? 'glacecachepro_ajax' : 'glacecachepro');
                    $html = "<$tag name=\"$name\">$html</$tag>";

                    $block->setData('glacecachepro_wrapped', true);

                    $transport->setHtml($html);
                }

                else if (!empty($children))
                {
                    $transport = $observer->getTransport();

                    $html = $transport->getHtml();

                    foreach ($children as $childName => $tags)
                    {
                        if(preg_match(
                            '#<glacecachepro\s*name="' . preg_quote($childName) . '"\s*>(.*?)</glacecachepro>#s',
                            $html,
                            $matches
                        ))
                        {
                            Mage::getSingleton('glacecachepro/cachepro')->saveBlockCache($childName, $matches[1], $tags);
                        }
                    }
                }
            }
        }
        else
        {
            if ($block instanceof Mage_Core_Block_Template || $block instanceof Mage_Cms_Block_Block)
            {
                $transport = $observer->getTransport();

                $html = $transport->getHtml();

                if ($this->_showBlockTemplates) {
                    if ($block instanceof Mage_Core_Block_Template){
                        $template = $block->getTemplateFile();
                    }
                    else {
                        $template = get_class($block);
                    }
                    $templateHint
                        = "<div class=\"glacecachepro-template-info\">$template</div>";
                }
                else
                    $templateHint = '';

                $html = <<<HTML
<div class="glacecachepro-block-info">
    <div class="glacecachepro-block-handle"
        onmouseover="$(this).parentNode.addClassName('active')"
        onmouseout="$(this).parentNode.removeClassName('active')"
    >{$block->getNameInLayout()}</div>
    $templateHint
    $html
</div>
HTML;

                $transport->setHtml($html);
            }
        }
    }

    public function layoutRenderBefore()
    {
        $request = Mage::app()->getRequest();

        if ($dynamicBlocks = Mage::registry('glacecachepro_blocks'))
        {
            $layout = Mage::app()->getLayout();
            $page = $dynamicBlocks['page'];

            Mage::app()->setUseSessionVar(false);

            /** @var Cmsideas_Cachepro_Model_Cachepro_Front $front */
            $front = Mage::getSingleton('glacecachepro/cachepro_front');

            foreach ($dynamicBlocks['blocks'] as $name)
            {
                $blockConfig = Mage::app()->getConfig()->getNode('global/glacecachepro/blocks/'.$name);
                $parent = (string)$blockConfig['parent'];

                $realName = $parent ? $parent : $name;

                if (!isset($this->_blockHtmlCache[$realName]))
                {
                    $block = $layout->getBlock($realName);
                    if ($block)
                    {
                        $this->_blockHtmlCache[$realName] = $block->toHtml();

                        if ($parent)
                        {
                            if (preg_match(
                                '#<glacecachepro\s*name="' . preg_quote($name) . '"\s*>(.*?)</glacecachepro>#s',
                                $this->_blockHtmlCache[$realName],
                                $matches
                            ))
                            {
                                $this->_blockHtmlCache[$name] = $matches[1];
                            }
                        }
                    }
                }

                if (isset($this->_blockHtmlCache[$name]))
                {
                    $blockHtml = $this->_blockHtmlCache[$name];
                    $blockHtml = preg_replace('/<glacecachepro[^>]*>/', '', $blockHtml);
                    $blockHtml = str_replace('</glacecachepro>', '', $blockHtml);
                    $blockHtml = str_replace('</glacecachepro_ajax>', '', $blockHtml);

                    $front->debug($blockHtml, $name . ($parent ? "[$parent]" : '') . ' (refresh)');

                    if (preg_match(
                        '#<glacecachepro(_ajax)? name="'.preg_quote($name).'" />#',
                        $page,
                        $matches,
                        PREG_OFFSET_CAPTURE))
                    {
                        $page = substr_replace($page, $blockHtml, $matches[0][1], strlen($matches[0][0]));
                    }
                }
            }

            $front->debug($page, 'Late page load');

            if (Mage::registry('glacecachepro_new_session'))
            {
                $page = Mage::helper('glacecachepro')->replaceFormKey($page);
            }

            $response = Mage::app()->getResponse();

            $this->setResponse($response, $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_HIT_UPDATE);

            $response->sendHeaders();
            $response->outputBody();

            exit;
        }
        else if ($request->isAjax())
        {
            $blocks = $request->getParam('glacecachepro_ajax_blocks');
            if ($blocks)
            {
                $blocks = explode(',', $blocks);

                $cmsAjaxBlocks = (bool)(string)Mage::app()->getConfig()->getNode('global/glacecachepro/cms_ajax_blocks');

                if ($cmsAjaxBlocks)
                {
                    $this->_ajaxBlocks = array_fill_keys($blocks, null);
                }
                else {

                    /** @var Cmsideas_Cachepro_Model_Cachepro_Front $front */
                    $front = Mage::getSingleton('glacecachepro/cachepro_front');

                    Mage::app()->setUseSessionVar(false);

                    $result = array();
                    $layout = Mage::app()->getLayout();
                    foreach ($blocks as $name) {
                        $block = $layout->getBlock($name);
                        if ($block) {
                            $content = Mage::getSingleton('core/url')->sessionUrlVar($block->toHtml());

                            $front->debug($content, $name . ' (ajax)');
                            $result[$name] = $content;
                        }
                    }

                    $blocksJson = Mage::helper('core')->jsonEncode($result);

                    Mage::app()->getResponse()->setBody($blocksJson)->sendResponse();
                    exit;
                }
            }
        }
    }

    protected function _canPreserve()
    {
        if (!Mage::registry('glacecachepro_preserve'))
            return false;

        if (Mage::app()->getResponse()->getHttpResponseCode() != 200)
            return false;

        if ($this->_showBlockNames)
            return false;

        if ($layout = Mage::app()->getLayout())
        {
            if ($block = $layout->getBlock('messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
            if ($block = $layout->getBlock('global_messages'))
                if ($block->getMessageCollection()->count() > 0)
                    return false;
        }
        else
            return false;

        return true;
    }

    public function setResponse($response, $html, $status)
    {
        /** @var Cmsideas_Cachepro_Model_Cachepro_Front $front */
        $front = Mage::getSingleton('glacecachepro/cachepro_front');

        $html = preg_replace(
            '#(<glacecachepro[^>]*?>|</glacecachepro(_ajax)?>)#s',
            '',
            $html
        );

        $front->addLoadTimeInfo($html, $status);

        $response->setBody($html);
    }

    public function onHttpResponseSendBefore($observer)
    {
        if (Mage::app()->getRequest()->getModuleName() == 'api')
            return;

        if (!Mage::app()->useCache('glacecachepro'))
            return;

        if (Mage::app()->getStore()->isAdmin())
            return;

        if (Mage::app()->getRequest()->isAjax() && $this->_ajaxBlocks === null)
            return;

        // No modifications in response till here

        Mage::getSingleton('core/session')->getFormKey(); // Init form key

        $page = $observer->getResponse()->getBody();

        if ($ignoreStatus = Mage::registry('glacecachepro_ignored'))
        {
            $this->setResponse($observer->getResponse(), $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_IGNORE_PARAM);

            return;
        }

        $tags = Mage::getSingleton('glacecachepro/config')->matchRoute(Mage::app()->getRequest());

        if (!$tags && !Mage::getStoreConfig('glacecachepro/pages/all'))
        {
            $this->setResponse($observer->getResponse(), $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_NEVER_CACHE);

            return;
        }

        if (Mage::helper('glacecachepro')->inIgnoreList() || Mage::registry('glacecachepro_ignorelist'))
        {
            $this->setResponse($observer->getResponse(), $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_IGNORE);
            return;
        }


        if ($this->_canPreserve())
        {
            $tags[] = Cmsideas_Cachepro_Model_Cachepro::CACHE_TAG;
            $tags[] = Mage_Core_Block_Abstract::CACHE_GROUP;

            $lifetime = +Mage::getStoreConfig('glacecachepro/general/page_lifetime');
            $lifetime *= 3600;

            Mage::getSingleton('glacecachepro/cachepro')->saveCache($page, $tags, $lifetime);
        }

        if (Mage::registry('glacecachepro_cms_blocks'))
        {
            $this->setResponse($observer->getResponse(), $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_CMS_UPDATE);
            return;
        }

        if (is_array($this->_ajaxBlocks))
        {
            $result = array();
            $front = Mage::getSingleton('glacecachepro/cachepro_front');
            foreach ($this->_ajaxBlocks as $name => $content) {
                $front->debug($content, $name . ' (ajax)');
                $result[$name] = $content;
            }

            $blocksJson = Mage::helper('core')->jsonEncode($result);

            Mage::app()->getResponse()->setBody($blocksJson);
            return;
        }

        $this->setResponse($observer->getResponse(), $page, Cmsideas_Cachepro_Model_Cachepro_Front::PAGE_LOAD_MISS);
    }

    public function cleanCache(Mage_Cron_Model_Schedule $schedule)
    {
        Mage::getSingleton('glacecachepro/cachepro')->getFrontend()->clean(Zend_Cache::CLEANING_MODE_OLD);
    }

    public function flushOutOfStockCache($observer)
    {
        $item = $observer->getItem();

        if ($item->getStockStatusChangedAutomatically())
        {
            $tags = array('catalog_product_' . $item->getProductId(), 'catalog_product');
            Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
        }

        return $item;
    }

    public function onQuoteSubmitSuccess($observer)
    {
        if (Mage::getStoreConfig('glacecachepro/product/flush_on_purchase'))
        {
            $tags = array();

            $quote = $observer->getEvent()->getQuote();
            foreach ($quote->getAllItems() as $item) {
                $tags []= 'catalog_product_' . $item->getProductId();
                $children   = $item->getChildrenItems();
                if ($children) {
                    foreach ($children as $childItem) {
                        $tags []= 'catalog_product_' . $childItem->getProductId();
                    }
                }
            }

            if (!empty($tags))
                Mage::dispatchEvent('application_clean_cache', array('tags' => $tags));
        }
    }

    public function onApplicationCleanCache($observer)
    {
        $tags = $observer->getTags();

        /**
         * @var Cmsideas_Cachepro_Model_Cachepro $cachepro
         */
        $cachepro = Mage::getSingleton('glacecachepro/cachepro');

        if (!empty($tags))
        {
            if (!is_array($tags))
            {
                $tags = array($tags);
            }

            $productIds = array();
            foreach ($tags as $tag)
            {
                if (preg_match('/^catalog_product_(?P<id>\d+)$/i', $tag, $matches))
                {
                    $productIds[] = +$matches['id'];
                }
            }

            $additionalTags = $cachepro->getProductsAdditionalTags($productIds);

            if (!empty($additionalTags))
            {
                $tags = array_merge($tags, $additionalTags);
            }
        }

        $cachepro->clean($tags);
    }

    public function onModelSaveBefore($observer)
    {
        $object = $observer->getObject();

        if (class_exists('Mirasvit_AsyncCache_Model_Asynccache', false)
            && $object instanceof Mirasvit_AsyncCache_Model_Asynccache)
        {
            if ($object->getData('status') == Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS &&
                $object->getOrigData('status') != Mirasvit_AsyncCache_Model_Asynccache::STATUS_SUCCESS)
            {
                Mage::getSingleton('glacecachepro/cachepro')->getFrontend()->clean($object->getMode(), $object->getTagArray(), true);
            }
        }
    }

    public function onCustomerLogin($observer)
    {
        $customer = $observer->getCustomer();

        Mage::getSingleton('customer/session')
            ->setCustomerGroupId($customer->getGroupId());
    }

    public function onReviewSaveAfter($observer)
    {
        $review = $observer->getObject();

        if ($review->getEntityId() == 1) // Product
            Mage::getSingleton('glacecachepro/cachepro')->clean('catalog_product_' . $review->getEntityPkValue());
    }

    public function onQuoteSaveAfter($observer)
    {
        Mage::helper('glacecachepro')->invalidateBlocksWithAttribute('cart');
    }

    public function onCustomerLoginLogout($observer)
    {
        Mage::helper('glacecachepro')->invalidateBlocksWithAttribute(array('customer', 'cart'));
    }

    public function onCategorySaveAfter($observer)
    {
        if (Mage::getStoreConfig('glacecachepro/category/flush_all'))
            Mage::getSingleton('glacecachepro/cachepro')->flush();
    }

    public function onAdminhtmlInitSystemConfig($observer)
    {
        $backend = Mage::getSingleton('glacecachepro/cachepro')->getBackendType();

        if ($backend != 'database') {
            $observer->getConfig()->setNode(
                'sections/glacecachepro/groups/compression/fields/max_size', false, true
            );
        }
    }

    public function onCrawlerProcessLink($observer)
    {
        /**
         * @var Cmsideas_Cachepro_Model_Cachepro $cachepro
         */
        $cachepro = Mage::getSingleton('glacecachepro/cachepro');
        $data = $observer->getData('data');
        $key = $cachepro->getCacheKey($data);
        $exists = $cachepro->getFrontend()->test($key);

        if ($exists) {
            $data->setData('hasCache', true);
        }
    }
}
