<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/

class Cmsideas_Cachepro_Model_Resource_Redis_Session extends Cm_RedisSession_Model_Session
{
    const SEESION_MAX_COOKIE_LIFETIME = 3155692600;

    public function getLifeTime()
    {
        $stores = Mage::app()->getStores();
        if (empty($stores))
            return self::SEESION_MAX_COOKIE_LIFETIME;
        else
            return parent::getLifeTime();
    }
}
