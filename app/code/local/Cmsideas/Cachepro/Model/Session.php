<?php
/*
 * Developer: Rene Voorberg
 * Team site: http://cmsideas.net/
 * Support: http://support.cmsideas.net/
 * 
 *
*/

class Cmsideas_Cachepro_Model_Session
{
    public function __construct()
    {
        if (isset($_SESSION))
        {
            if (!isset($_SESSION['glacecachepro']))
            {
                $_SESSION['glacecachepro'] = array('updated_blocks' => array());
            }

            if (Cmsideas_Cachepro_Model_Cachepro_Front::getDbConfig('web/cookie/cookie_restriction')) {
                $cookieBlock = Cmsideas_Cachepro_Model_Config::getCookieNoticeBlockName();
                $_SESSION['glacecachepro']['updated_blocks']['cookie_notice_block'] = $cookieBlock;
            }
        }
    }

    public function getUpdatedBlocks()
    {
        if (isset($_SESSION))
            return $_SESSION['glacecachepro']['updated_blocks'];
        else
            return array();
    }

    public function updateBlock($name)
    {
        if (!in_array($name, $_SESSION['glacecachepro']['updated_blocks']))
            $_SESSION['glacecachepro']['updated_blocks'][] = $name;
    }

    public function isBlockUpdated($name)
    {
        if (isset($_SESSION) && isset($_SESSION['glacecachepro']['updated_blocks']))
            return in_array($name, $_SESSION['glacecachepro']['updated_blocks']);
        else
            return false;
    }
}