<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Return all binds that are actual for date
     * @param  $y
     * @param  $m
     * @param  $d
     * @param  $product
     * @param bool $includeCanceled
     * @return array
     */
	public function getBindDetails($y, $m, $d, $product, $includeCanceled = false){
		// Returns details on date for products
		$out = array();
		$m = str_pad($m, 2, "0", STR_PAD_LEFT);
		$d = str_pad($d, 2, "0", STR_PAD_LEFT);
		$date = "$y-$m-$d";
		$from = $date." 23:59:59";
		$to = $date." 00:00:00";

		// Returns binded days as array of ()
		$collection = Mage::getModel('booking/order')
            ->getCollection()
            ->addProductIdFilter($product->getId())
            ->addBindInrangeFilter($from, $to)
            ->groupByOrderId()
        ;
		if ($includeCanceled) {
			$collection->dropCanceledFilter();
		}
		$collection->load();
		foreach ($collection as $order) {
			$realOrder = Mage::getModel('sales/order')->loadByIncrementId($order->getOrderId());
			if ($realOrder->getCustomerIsGuest()) {
				$customerName = $realOrder->getBillingAddress()->getName();
				$isGuest = true;
				$customerId = -1;
				$customerHref = '';
			} else {
				$isGuest = false;
				$customerId = $realOrder->getCustomerId();
				$customerName = $realOrder->getCustomerName();
				$customerHref = Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/customer/edit', array('id'=>$customerId));
			}
			$ORD = Mage::getModel('sales/order')->loadByIncrementId($order->getOrderId());
			$orderHref = '<a href="'.Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/sales_order/view', array('order_id'=>$ORD->getId())).'">'.$order->getOrderId().'</a>';
			$out[] = array(
				'createdTime'	=> Mage::helper('core')->formatDate($order->getCreatedTime(), 'short'),
				'bindStart'		=> Mage::helper('core')->formatDate(new Zend_Date($order->getBindStart()), 'short', $order->getProduct()->getAwBookingRangeType() != 'date_fromto'),
				'bindEnd'		=> Mage::helper('core')->formatDate(new Zend_Date($order->getBindEnd()), 'short', $order->getProduct()->getAwBookingRangeType() != 'date_fromto'),
				'orderId'		=> $order->getOrderId(),
				'orderHref'		=> $orderHref,
				'customerName'	=> $customerName,
				'customerHref'	=> $customerHref,
                'customerEmail' => $realOrder->getCustomerEmail(),
                'customerPhone' => $realOrder->getBillingAddress()->getTelephone(),
				'customerId'	=> $customerId,
				'isGuest'		=> $isGuest,
				'totalItems'	=> $order->getTotalItems(),
				'isCanceled'	=> $order->getIsCanceled()
			);
		}
		return $out;
	}

    public static function strToDate($d, $format = null)
    {
        if (null === $format) {
            $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        }
        $date = new Zend_Date($d, $format);
        return $date;
    }

    public static function toTimestamp($d = null, $format = null)
    {
        if (null === $format) {
            $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        }

        if (empty($d)) {
            return 0;
        }
        $date = self::strToDate($d, $format);
        return $date->getTimestamp();
    }

    public static function getDayOfWeek(Zend_Date $d)
    {
        $date = $d->toArray();
        return $date['weekday'];
    }

    //TODO AW_ALL
    public function checkVersion($version)
    {
        return version_compare(Mage::getVersion(), $version, '>=');
    }
}