<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_AjaxController extends Mage_Core_Controller_Front_Action
{
    /**
     * Echoes rendered time table for date
     *
     */
    public function getDateAction()
    {
        $date = $this->getRequest()->getParam('date');
        $zDate = Mage::app()->getLocale()->date(Mage::getSingleton('core/date')->gmtTimestamp(), null, null);
        $zDate->setDate($date, AW_Core_Model_Abstract::DB_DATE_FORMAT, null);
        $this
            ->getResponse()
            ->setBody($this->getLayout()->createBlock('booking/catalog_product_view_timetable')->getFromToHtml($zDate))
        ;
    }

    public function getPriceAction()
    {
        if (!$this->getRequest()->getParam('product_id')) {
            return;
        }
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product_id'));
        if ($from = urldecode($this->getRequest()->getParam('from'))) {
            $zDateFrom = new Zend_Date($from, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
            if ($to = urldecode($this->getRequest()->getParam('to'))) {
                $zDateTo = new Zend_Date($to, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
                // From and To are now ready. Get price for period
                $price = $product->getPriceModel()->getBookingPrice(
                    $product, $zDateFrom, $zDateTo, null, AW_Core_Model_Abstract::RETURN_ARRAY
                );
            }
        }
        if ($date = urldecode($this->getRequest()->getParam('date'))) {
            $zDate = new Zend_Date($date, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
            $price = $product->getPriceModel()->getBookingPrice($product, $zDate);
        }
        $this
            ->getResponse()
            ->setBody(Zend_Json::encode($price))
        ;
    }

    public function getBindedDatesAction()
    {
        if (!$this->getRequest()->getParam('product_id')) {
            return;
        }
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product_id'));
        if ($to = urldecode($this->getRequest()->getParam('to'))) {
            $zDateFrom = new Zend_Date($to, AW_Core_Model_Abstract::DB_DATE_FORMAT);
            $zDateTo = clone $zDateFrom;
            $zDateTo->addMonth(Mage::getStoreConfig(AW_Booking_Helper_Config::XML_PATH_APPEARANCE_CALENDAR_PAGES));
            $dates = Mage::getModel('booking/checker_bind')->getUnavailDays($product, $zDateFrom, $zDateTo);
        }
        $this
            ->getResponse()
            ->setBody(Zend_Json::encode($dates))
        ;
    }
}
