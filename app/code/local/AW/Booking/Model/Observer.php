<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Observer
{
    protected $_session;

    public function __construct()
    {
        $this->_session = Mage::getSingleton('customer/session');
    }

    public function getSession()
    {
        return $this->_session;
    }

    /**
     * Update product's name at booking/order table
     *
     * @param object $observer
     */
    public function updateBookingOrdersProductName($observer)
    {
        $product = $observer->getEvent()->getProduct();
        if (!$storeId = $product->getStore()->getId()) {
            $storeId = null;
        }
        Mage::getResourceModel('booking/order')->setProductNameById($product->getId(), $product->getName(), $storeId);
    }

    /**
     * Set binds for products added to cart
     *
     * @param $observer
     * @throws Mage_Core_Exception
     */
    public function bindCartItems($observer)
    {
        if (Mage::registry("booking_order_created")) {
            // Already converted to order
            return;
        }

        $quoteItem = $observer->getItem();
        if ($quoteItem->getProductType() != AW_Booking_Helper_Config::PRODUCT_TYPE_CODE) {
            // Not bookable product type
            return;
        }

        /** If item is deleted, don't process later */
        if ($quoteItem->isDeleted()) {
            return;
        }

        /** Test if quote item is already converted to order item */
        if (Mage::getModel('sales/order_item')->load($quoteItem->getId(), 'quote_item_id')->getId()) {
            return;
        }

        // Get From and To values
        $product = $quoteItem->getProduct();
        if (!is_object($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME))) {
            $source = unserialize($product->getCustomOption('info_buyRequest')->getValue());
            $fromDate = $source['aw_booking_from'];
            $toDate = $source['aw_booking_to'];
        } else {
            $fromDate = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME)->getValue()
            ;
            $toDate = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_DATE_OPTION_NAME)->getValue()
            ;
            $fromTime = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_TIME_OPTION_NAME)->getValue()
            ;
            $toTime = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_TIME_OPTION_NAME)->getValue()
            ;
            $fromDate .= " {$fromTime}";
            $toDate .= " {$toTime}";
        }

        $zDateFrom = new Zend_Date($fromDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        $zDateTo = new Zend_Date($toDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);

        if ($product->getAwBookingRangeType() !== 'date_fromto' && $zDateFrom->compare($zDateTo) >= 0) {
            throw new Mage_Core_Exception(Mage::helper("checkout")->__("Incorrect period selected"));
        }

        // Check if specified quantity is available
        $originalQty = Mage::getModel('booking/order')->getCollection()
            ->addQuoteItemIdFilter($quoteItem->getId())
            ->count()
        ;
        $qtyToAdd = $quoteItem->getQty() - $originalQty;

        if (
            $qtyToAdd > 0
            && !Mage::getModel('booking/checker_bind')->isQtyAvailableForPeriodCheck(
                $product, $zDateFrom, $zDateTo, $quoteItem->getQty()
            )
        ) {
            if ($quoteItem->getQuote()->getItemById($quoteItem->getId())) {
                $quoteItem->getQuote()
                    ->removeItem($quoteItem->getId())
                    ->save()
                ;
            }
            throw new Mage_Core_Exception(
                Mage::helper("checkout")->__(
                    "Some of the products you requested are not available in the desired quantity"
                )
            );
        }

        if ($qtyToAdd == 0) {
            return;
        }
        Mage::getResourceModel('booking/order')->deleteByQuoteItem($quoteItem);

        /* Create as many records as quoteItem quantity is */
        $i = 1;
        $qty = $quoteItem->getQty();
        while ($i <= $qty) {
            $bookingQuoteItem = Mage::getModel('booking/order');
            $bookingQuoteItem
                ->setProductId($quoteItem->getProductId())
                ->setProductName($quoteItem->getName())
                ->setSku($quoteItem->getSku())
                ->setBindStart($zDateFrom->get(AW_Core_Model_Abstract::DB_DATETIME_FORMAT))
                ->setBindEnd($zDateTo->get(AW_Core_Model_Abstract::DB_DATETIME_FORMAT))
                ->setBindType(AW_Booking_Model_Order::BIND_TYPE_CART)
                ->setOrderId($quoteItem->getId())
                ->setQuoteId($quoteItem->getQuote()->getId())
                ->setCreatedTime(now())
                ->save()
            ;
            $i += 1;
        }
    }

    /**
     * Remove all cart binds for specified quote item
     *
     * @param Varien_Object $observer
     * @todo emplement
     */
    public function removeCartBinds($observer)
    {
        $quoteItem = $observer->getQuoteItem();
        if (!$quoteItem) {
            $quoteItem = $observer->getItem();
        }
        if ($quoteItem->getProductType() != AW_Booking_Helper_Config::PRODUCT_TYPE_CODE) {
            // Not bookable product type
            return;
        }
        Mage::getResourceModel('booking/order')->deleteByQuoteItem($quoteItem);
    }

    /**
     * Clear old quote info
     *
     * @param Varien_Object $observer
     */
    public function quoteMergeAfter($observer)
    {
        $source = $observer->getSource();
        Mage::getResourceModel('booking/order')->deleteByQuoteId($source->getId());
    }

    /**
     * Binds product as ordered
     *
     * @param Varien_Object $observer
     *
     * @return bool
     */
    public function bindOrderItems($observer)
    {
        $items = $observer->getInvoice()->getOrder()->getItemsCollection();
        foreach ($items as $item) {
            // Fetch for products
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            // Affect only bookable broducts
            if ($product->getTypeId() != AW_Booking_Helper_Config::PRODUCT_TYPE_CODE) {
                continue;
            }
            $data = $item->getProductOptionByCode('info_buyRequest');
            $_dateFrom = $data['aw_booking_from'];
            $_dateTo = @$data['aw_booking_to'] ? $data['aw_booking_to'] : $data['aw_booking_from'];
            $_timeFrom = @$data['aw_booking_time_from'];
            $_timeTo = @$data['aw_booking_time_to'];

            // Parse date to set to db
            $zDateFrom = new Zend_Date(
                $_dateFrom, Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );
            $zDateTo = new Zend_Date(
                $_dateTo, Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );

            $from = $zDateFrom->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT);
            $to = $zDateTo->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT);
            // If time is set, we should also remember it
            /**
             * @todo Test with time options
             */
            if ($_timeFrom) {
                $_timeFrom = AW_Booking_Model_Product_Type_Bookable::convertTime(
                    $_timeFrom, AW_Core_Model_Abstract::RETURN_STRING
                );
                $from .= " " . $_timeFrom;
            }
            if ($_timeTo) {
                $_timeTo = AW_Booking_Model_Product_Type_Bookable::convertTime(
                    $_timeTo, AW_Core_Model_Abstract::RETURN_STRING
                );
                $to .= " " . $_timeTo;
            }
            // If interval is free, check if interval is available

            $qty = $item->getQtyInvoiced();
            $isQtyAvailableForPeriod =  Mage::getModel('booking/checker_bind')->isQtyAvailableForPeriod(
                $product,
                new Zend_Date($from, AW_Core_Model_Abstract::DB_DATETIME_FORMAT),
                new Zend_Date($to, AW_Core_Model_Abstract::DB_DATETIME_FORMAT),
                $qty,
                false
            );
            if ($isQtyAvailableForPeriod) {
                $model = Mage::getModel('booking/order')
                    ->setProductId($item->getProductId())
                    ->setProductName($product->getName())
                    ->setSku($product->getSku())
                    ->setBindStart($from)
                    ->setBindEnd($to)
                    ->setOrderId($observer->getInvoice()->getOrder()->getIncrementId())
                    ->setOrderItemId($item->getId())
                    ->setCreatedTime(now())
                ;
                while ($qty--) {
                    $model->setId(null)->save();
                }
                Mage::getResourceModel('booking/order')->deleteByQuoteItemId($item->getQuoteItemId());
            } else {
                Mage::getSingleton('customer/session')->addError('Sorry, the period you specified is inaccessible');
                throw new Mage_Core_Exception(Mage::helper('booking')->__('Sorry, the period you specified is inaccessible'));
                return false;
            }
        }
        // Write flag to session to skip checking quantity for bookable products set
        Mage::register('booking_order_created', 1);
        return true;
    }

    public function attachExcludeEditor($observer)
    {
        $form = $observer->getForm();
        if ($excludedDays = $form->getElement('aw_booking_exclude_days')) {
            $excludedDays->setRenderer(
                Mage::getSingleton('core/layout')->createBlock(
                    'booking/adminhtml_catalog_product_edit_tab_booking_excludeddays'
                )
            );
        }
    }

    public function attachPricesEditor($observer)
    {
        $form = $observer->getForm();
        if ($excludedDays = $form->getElement('aw_booking_prices')) {
            $excludedDays->setRenderer(
                Mage::getSingleton('core/layout')->createBlock(
                    'booking/adminhtml_catalog_product_edit_tab_booking_prices'
                )
            );
        }
    }

    /**
     * Cancels booking "Order" record
     * TODO: Check if this function needed or cancelOrderItem function can replace it
     *
     * @param  $observer
     * @return void
     */
    public function cancelOrder($observer)
    {
        $order = $observer->getOrder();
        if (
            $order->getState() == Mage_Sales_Model_Order::STATE_CANCELED
            || $order->getState() == Mage_Sales_Model_Order::STATE_CLOSED
        ) {
            Mage::getResourceSingleton('booking/order')->cancelByOrderId($order->getIncrementId());
        }
    }

    /**
     * Cancels booking "Order" record by order item
     *
     * @param  $observer
     * @return void
     */
    public function cancelOrderItem($observer)
    {
        $orderItem = $observer->getEvent()->getDataObject();
        if (!($orderItem instanceof Mage_Sales_Model_Order_Item)) {
            return;
        }
        if ($orderItem->getStatusId() == Mage_Sales_Model_Order_Item::STATUS_REFUNDED) {
            Mage::getResourceSingleton('booking/order')->cancelByOrderId($orderItem->getOrder()->getIncrementId());
        }
    }

    public function checkQtyOrder($observer)
    {
        $order = $observer->getEvent()->getOrder();
        if ($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE) {
            foreach ($order->getAllItems() as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                $placed = Mage::getModel('booking/order')->load($order->getIncrementId(), 'order_id');
                if (
                    $item->getProductType() == AW_Booking_Helper_Config::PRODUCT_TYPE_CODE
                    && $item->getQtyInvoiced() == $item->getQtyOrdered()
                    && !$placed->getId()
                ) {
                    $options = $item->getProductOptions();
                    $fromTimeOptionName = AW_Booking_Model_Product_Type_Bookable::FROM_TIME_OPTION_NAME;
                    $fromDateOptionName = AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME;

                    $from = new Zend_Date(
                        $options['info_buyRequest'][$fromDateOptionName], Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                    );
                    if (isset($options['info_buyRequest'][$fromTimeOptionName])) {
                        $from->setMinute($options['info_buyRequest'][$fromTimeOptionName]['minutes']);
                        $from = Mage::helper('booking/dates')->setHoursByDayPart($from,
                            $options['info_buyRequest'][$fromTimeOptionName]['hours'],
                            $options['info_buyRequest'][$fromTimeOptionName]['daypart']
                        );
                    }

                    $toTimeOptionName = AW_Booking_Model_Product_Type_Bookable::TO_TIME_OPTION_NAME;
                    $toDateOptionName = AW_Booking_Model_Product_Type_Bookable::TO_DATE_OPTION_NAME;
                    $to = new Zend_Date(
                        $options['info_buyRequest'][$toDateOptionName], Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                    );
                    if (isset($options['info_buyRequest'][$toTimeOptionName])) {
                        $to->setMinute($options['info_buyRequest'][$toTimeOptionName]['minutes']);
                        $to = Mage::helper('booking/dates')->setHoursByDayPart($to,
                            $options['info_buyRequest'][$toTimeOptionName]['hours'],
                            $options['info_buyRequest'][$toTimeOptionName]['daypart']
                        );
                    }
                    $isQtyAvailableForPeriod = Mage::getModel('booking/checker_bind')->isQtyAvailableForPeriod(
                        $product, $from, $to, $item->getQtyOrdered(), false
                    );
                    if (!$isQtyAvailableForPeriod) {
                        $orderedQty = Mage::getModel('booking/order')->getOrderedQty($product, $from, $to);
                        $available = $product->getAwBookingQuantity() - $orderedQty;
                        Mage::getModel('booking/notification')->sendQtyNotAvailableAlert($order, $item, $available);
                    }
                }
            }
        }
    }
}