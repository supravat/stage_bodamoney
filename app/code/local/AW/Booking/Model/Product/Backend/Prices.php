<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Product_Backend_Prices extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * Website currency codes and rates
     *
     * @var array
     */
    protected $_rates;

    /**
     * Retrieve resource model
     *
     * @return AW_Booking_Model_Mysql4_Booking_Price
     */
    protected function _getResource()
    {
        return Mage::getResourceSingleton('booking/booking_price');
    }

    protected function _getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * Validate data
     *
     * @param   Mage_Catalog_Model_Product $object
     *
     * @return bool
     */
    public function validate($object)
    {
        $this->_validateQuantity($object);
        $this->_validateDates($object);
        $periods = $object->getData($this->getAttribute()->getName());
        $processedPeriodList = array();
        foreach ($periods as $k => $period) {
            if (!$this->_validatePeriod($k, $period)) {
                continue;
            }

            if (!empty($processedPeriodList)) {
                foreach ($processedPeriodList as $processedPeriod) {
                    if ($this->_intersects($processedPeriod, $period)) {
                        Mage::throwException(
                            Mage::helper('booking')->__("Please enter valid price intervals")
                        );
                    }
                }
            }
            $processedPeriodList[] = $period;
        }
        return parent::validate($object);
    }

    public function _intersects($periodA, $periodB)
    {
        $fromA = AW_Booking_Helper_Data::toTimestamp($periodA['date_from']);
        $toA = AW_Booking_Helper_Data::toTimestamp($periodA['date_to']);
        $fromB = AW_Booking_Helper_Data::toTimestamp($periodB['date_from']);
        $toB = AW_Booking_Helper_Data::toTimestamp($periodB['date_to']);
        if (
            ($fromB >= $fromA && $toB <= $toA)
            || ($fromB <= $fromA && $toB >= $toA)
            || ($toB > $fromA && $toB < $toA)
            || ($fromB > $fromA && $fromB < $toA)
            || ($toA == $fromB)
        ) {
            return true;
        }
        return false;
    }

    /**
     * After Save Attribute manipulation
     *
     * @param Mage_Catalog_Model_Product $object
     * @return Mage_Catalog_Model_Product_Attribute_Backend_Tierprice
     */
    public function afterSave($object)
    {
        $generalStoreId = $object->getStoreId();
        $periods = $object->getData($this->getAttribute()->getName());
        if (!is_array($periods)) {
            return $this;
        }
        Mage::getResourceSingleton('booking/prices')->deleteByEntityId($object->getId(), $generalStoreId);
        foreach ($periods as $k => $period) {
            if (!$this->_validatePeriod($k, $period)) {
                continue;
            }
            $period['date_from'] = date('Y-m-d H:i:s', AW_Booking_Helper_Data::toTimestamp($period['date_from']));
            $period['date_to'] = date('Y-m-d H:i:s', AW_Booking_Helper_Data::toTimestamp($period['date_to']));
            $storeId = @$period['use_default_value'] ? 0 : $object->getStoreId();

            Mage::getModel('booking/prices')
                ->setEntityId($this->_getProduct()->getId())
                ->setStoreId($storeId)
                ->setDateFrom($period['date_from'])
                ->setDateTo($period['date_to'])
                ->setPriceFrom($period['price_from'])
                ->setPriceTo($period['price_to'])
                ->setIsProgressive(intval($period['is_progressive']))
                ->save()
            ;
        }
        return $this;
    }

    protected function _validatePeriod($periodIndex, $period)
    {
        if (!is_numeric($periodIndex)) {
            return false;
        }
        if (array_key_exists('use_default_value', $period) && !empty($period['use_default_value'])) {
            return false;
        }
        if (
            !empty($period['delete'])
            || empty($period['price_from'])
            || empty($period['date_from'])
            || (
                intval($period['is_progressive'])
                && empty($period['date_to'])
            )
            || (
                intval($period['is_progressive'])
                && (
                    AW_Booking_Helper_Data::toTimestamp($period['date_from'])
                    >= AW_Booking_Helper_Data::toTimestamp($period['date_to'])
                )
            )
        ) {
            return false;
        }
        return true;
    }

    protected function _validateDates($object)
    {
        if (
            $object->getData('aw_booking_date_to') == null
            || AW_Booking_Helper_Data::toTimestamp($object->getData('aw_booking_date_from'), Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT))
            < AW_Booking_Helper_Data::toTimestamp($object->getData('aw_booking_date_to'), Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT))
        ) {
            return true;
        }
        throw new Mage_Core_Exception(Mage::helper('booking')->__("'Date to' shouldn't be early than 'Date from'"));
    }

    protected function _validateQuantity($object)
    {
        if ($object->getData('aw_booking_quantity') > 0) {
            return true;
        }
        throw new Mage_Core_Exception(Mage::helper('booking')->__("Please enter a positive number for the 'Quantity' field."));
    }
}
