<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Product_Backend_Excludedays extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    /**
     * Retrieve resource model
     *
     * @return AW_Booking_Model_Mysql4_Booking_Price
     */
    protected function _getResource()
    {
        return Mage::getResourceSingleton('booking/booking_price');
    }

    /**
     * Returns current product from registry
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _getProduct()
    {
        return Mage::registry('product');
    }

    /**
     * After Save Attribute manipulation
     *
     * @param Mage_Catalog_Model_Product $object
     * @return AW_Booking_Model_Product_Backend_Excludedays
     */
    public function afterSave($object)
    {
        $periods = $object->getData($this->getAttribute()->getName());
        if (!is_array($periods)) {
            return $this;
        }
        Mage::getResourceSingleton('booking/excludeddays')->deleteByEntityId($object->getId(), $object->getStoreId());
        foreach ($periods as $k => $period) {
            if (!is_numeric($k)) {
                continue;
            }
            if (array_key_exists('use_default_value', $period) && !empty($period['use_default_value'])) {
                continue;
            }

            if ($period['period_type'] == 'recurrent_day') {
                $dayOfWeek = $period['recurrent_day'];
                $date = new Zend_Date();
                Zend_Date::setOptions(array('extend_month' => true)); // Fix Zend_Date::addMonth unexpected result
                while (AW_Booking_Helper_Data::getDayOfWeek($date) != $dayOfWeek) {
                    $date->addDayOfYear(1);
                }
                $period['period_from'] = $date->toString(
                    Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                );
            }

            if (
                !empty($period['delete'])
                || empty($period['period_type'])
                || empty($period['period_from'])
                || !AW_Booking_Helper_Data::toTimestamp($period['period_from'])
                || (
                    $period['period_type'] == 'period'
                    && empty($period['period_to'])
                )
                || (
                    $period['period_type'] == 'period'
                    && (
                        AW_Booking_Helper_Data::toTimestamp($period['period_from'])
                        >= AW_Booking_Helper_Data::toTimestamp($period['period_to'])
                    )
                )
            ) {
                continue;
            }

            $period['period_from'] = date('Y-m-d H:i:s', AW_Booking_Helper_Data::toTimestamp($period['period_from']));
            $period['period_to'] = date('Y-m-d H:i:s', AW_Booking_Helper_Data::toTimestamp($period['period_to']));

            Mage::getModel('booking/excludeddays')
                ->setEntityId($this->_getProduct()->getId())
                ->setStoreId($object->getStoreId())
                ->setPeriodType($period['period_type'])
                ->setPeriodRecurrenceType($period['period_recurrence_type'])
                ->setPeriodFrom($period['period_from'])
                ->setPeriodTo($period['period_to'])
                ->save()
            ;
        }
        return $this;
    }
}