<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Product_Price extends Mage_Catalog_Model_Product_Type_Price
{
    /**
     * Returns complex price (booking price rules + common price * multiplier)
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Zend_Date                  $from
     * @param mixed                      $to
     * @param                            $basePrice base price
     * @param null                       $ret
     *
     * @return string
     */
    public function getBookingPrice($product, Zend_Date $from, $to = null, $basePrice = null, $ret = null)
    {
        if (is_null($basePrice)) {
            $basePrice = $product->getData('price');
        }
        if ($to instanceof Zend_Date) {
            // Period is used
            $price = Mage::getModel('booking/checker_price')->getPriceForPeriod(
                $product, $from, $to, $basePrice, Mage::app()->getStore()->getId(), null, $ret
            );
        } else {
            // Price for single day
            $price = Mage::getModel('booking/checker_price')->getPriceForDate(
                $product, $from, Mage::app()->getStore()->getId()
            );
            if ($price == AW_Booking_Model_Checker::NO_PRICE) {
                $price = $basePrice;
            }
        }
        return $price;
    }

    /**
     * Applies "Special" price. This is built-in magento.
     * By the moment this is just stub, please set special price via provided with booking price rules selector
     * @param Mage_Catalog_Model_Product $product
     * @param float $finalPrice
     * @return float
     */
    protected function _applySpecialPrice($product, $finalPrice)
    {
        return $finalPrice;
    }

    public function getStoredFirstAvailableDate($product)
    {
        $cProduct_id = Mage::registry('booking-time-product-id');
        $product_id = $product->getId();
        if ($product_id != Mage::registry('booking-time-product-id')) {
            Mage::unregister('booking-time-product-id');
            Mage::register('booking-time-product-id', $product_id);
        }
        if ($cProduct_id != Mage::registry('booking-time-product-id')) {
            Mage::unregister('booking-time-firstAvailableDate');
            Mage::register('booking-time-firstAvailableDate', $product->getTypeInstance()->getFirstAvailableDate());
        }
        return Mage::registry('booking-time-firstAvailableDate');
    }

    /**
     * Default action to get price of product
     *
     * @param $product
     * @return decimal|string
     */
    public function getPrice($product)
    {
        $date = $this->getStoredFirstAvailableDate($product)
            ? $this->getStoredFirstAvailableDate($product) : new Zend_Date()
        ;
        $fromDate = $date->toString(AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        $date->addHour(1);
        $toDate = $date->toString(AW_Core_Model_Abstract::DB_DATETIME_FORMAT);

        if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME)) {
            $fromDate = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME)
                ->getValue()
            ;
        }
        if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_DATE_OPTION_NAME)) {
            $toDate = $product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_DATE_OPTION_NAME)->getValue(
            );
        }
        if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_TIME_OPTION_NAME)) {
            $from_time = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_TIME_OPTION_NAME)
                ->getValue()
            ;
            $fromDate .= " $from_time";
        }
        if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_TIME_OPTION_NAME)) {
            $to_time = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_TIME_OPTION_NAME)
                ->getValue()
            ;
            $toDate .= " $to_time";
        }

        $from = new Zend_Date($fromDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        if ($toDate) {
            $to = new Zend_Date($toDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        } else {
            $to = clone $from;
        }
        $price = $this->getBookingPrice($product, $from, $to, $product->getData('price'));
        $product->setData('final_price', $price);
        return $price;
    }

    /**
     * Return final price
     * @param int  $qty
     * @param  Mage_Catalog_Model_Product $product
     * @todo Optimize that with previous method to be more spartan
     * @return int|mixed
     */
    public function getFinalPrice($qty = null, $product)
    {
        // Calculate how many reservations used
        $Date = $this->getStoredFirstAvailableDate($product)
            ? $this->getStoredFirstAvailableDate($product) : new Zend_Date()
        ;

        $fromDate = $Date->toString(AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        $Date->addHour(1);
        $toDate = $Date->toString(AW_Core_Model_Abstract::DB_DATETIME_FORMAT);

        if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME)) {
            $fromDate = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_DATE_OPTION_NAME)
                ->getValue()
            ;
        }
        if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_DATE_OPTION_NAME)) {
            $toDate = $product
                ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_DATE_OPTION_NAME)
                ->getValue()
            ;
        }

        //check product Billable period and if it equal days don't use time in price calculating
        if (
            $product->getAwBookingQratioMultiplier()
            != AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::DAYS
        ) {
            if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_TIME_OPTION_NAME)) {
                $fromTime = $product
                    ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::FROM_TIME_OPTION_NAME)
                    ->getValue()
                ;
                $fromDate .= " $fromTime";
            }
            if ($product->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_TIME_OPTION_NAME)) {
                $toTime = $product
                    ->getCustomOption(AW_Booking_Model_Product_Type_Bookable::TO_TIME_OPTION_NAME)
                    ->getValue()
                ;
                $toDate .= " $toTime";
            }
        }

        $from = new Zend_Date($fromDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        if ($toDate) {
            $to = new Zend_Date($toDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        } else {
            $to = clone $from;
        }

        $price = $product->getData('price');
        $finalPrice = $this->_applyTierPrice($product, $qty, $price);
        $finalPrice = $this->_applySpecialPrice($product, $finalPrice);

        list($finalPrice, $occurs) = $this->getBookingPrice(
            $product, $from, $to, $finalPrice, AW_Core_Model_Abstract::RETURN_ARRAY
        );
        return $finalPrice;
    }

    public function getOptionsPrice($product, $price)
    {
        $optprice = 0;
        if ($optionIds = $product->getCustomOption('option_ids')) {
            $basePrice = $price;
            foreach (explode(',', $optionIds->getValue()) as $optionId) {
                if ($option = $product->getOptionById($optionId)) {
                    $quoteItemOption = $product->getCustomOption('option_' . $option->getId());
                    $group = $option->groupFactory($option->getType())
                        ->setOption($option)
                        ->setQuoteItemOption($quoteItemOption)
                    ;
                    $optprice += $group->getOptionPrice($quoteItemOption->getValue(), $basePrice);
                }
            }
        }
        return $optprice;
    }
}