<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Product_Type_Bookable extends Mage_Catalog_Model_Product_Type_Abstract
{
    /** Flag constant indicationg no period from is set for product */
    const HAS_NO_PERIOD_FROM = 2; // Indicates
    /** Flag constant indicationg no period to is set for product */
    const HAS_NO_PERIOD_TO = 4;
    /** "From" date field name */
    const FROM_DATE_OPTION_NAME = 'aw_booking_from';
    /** "To" date field name */
    const TO_DATE_OPTION_NAME = 'aw_booking_to';
    /** "From" time field name */
    const FROM_TIME_OPTION_NAME = 'aw_booking_time_from';
    /** "To" time field name */
    const TO_TIME_OPTION_NAME = 'aw_booking_time_to';

    protected $_isDuplicable = false;
    protected $_product;

    /**
     * Retrieve product instance in any cost
     *
     * @param null $product
     *
     * @return Mage_Catalog_Model_Product|mixed|null
     * @throws AW_Core_Exception
     */
    public function getProduct($product = null)
    {
        if (!$product) {
            if ($this->_product) {
                return $this->_product;
            }
            $product = $this->_product;
            if (!$product) {
                $product = Mage::registry('product');
                if (!$product) {
                    //TODO remove new AW_Core_Exception
                    throw new AW_Core_Exception("Can't get product instance");
                }
                $this->_product = Mage::getModel('catalog/product')->load($product->getId());
            }
        }
        $this->setProduct($product);
        return $product;
    }

    public function prepareForCartAdvanced(Varien_Object $buyRequest, $product = null, $processMode = null)
    {
        if (!$product) {
            $product = $this->getProduct();
        }

        /* We should add custom options that doesnt exist */
        if (!$product) {
            $product = $this->getProduct();
        }

        if ($buyRequest->getAwBookingFrom()) {
            // Set "from" equal to "To" if no "to" specified
            if (!$buyRequest->getAwBookingTo()) {
                $buyRequest->setAwBookingTo($buyRequest->getAwBookingFrom());
            }

            $fromDate = new Zend_Date(
                $buyRequest->getAwBookingFrom(),
                Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );
            $toDate = new Zend_Date(
                $buyRequest->getAwBookingTo(),
                Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );

            // Add time and date
            list($fH, $fM) = self::_convertTimeTo24hArray($buyRequest->getAwBookingTimeFrom());
            list($tH, $tM) = self::_convertTimeTo24hArray($buyRequest->getAwBookingTimeTo());

            $fromDate->setHour((int)$fH)->setMinute((int)$fM);
            $toDate->setHour((int)$tH)->setMinute((int)$tM);

            $product->addCustomOption(self::FROM_DATE_OPTION_NAME, $fromDate->toString('Y-m-d', 'php'), $product);
            $product->addCustomOption(self::TO_DATE_OPTION_NAME, $toDate->toString('Y-m-d', 'php'), $product);
            $product->addCustomOption(self::FROM_TIME_OPTION_NAME, $fromDate->toString('H:i', 'php'), $product);
            $product->addCustomOption(self::TO_TIME_OPTION_NAME, $toDate->toString('H:i', 'php'), $product);

            $now = Zend_Date::now();
            if ($product->getAwBookingRangeType() == AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE) {
                $now->setHour(0)->setMinute(0)->setSecond(0);
            }
            if (
                $now->compare($fromDate) > 0
                || $now->compare($toDate) > 0
            ) {
                Mage::throwException(
                    Mage::helper('booking')->__("Chosen date/time is in the past")
                );
                return false;
            }

            /* Check if product can be added to cart */
            $isAvail = $this->isAvailable($product, $buyRequest->getQty(), true);
            if (!$isAvail) {
                Mage::throwException(
                    Mage::helper('booking')->__("Chosen quantity is not available. Only reservations left.")
                );
            }
            return parent::prepareForCartAdvanced($buyRequest, $product);
        }
        return Mage::helper('booking')->__('Please specify reservation information');
    }

    public function prepareForCart(Varien_Object $buyRequest, $product = null)
    {
        if (!$product) {
            $product = $this->getProduct();
        }

        /* We should add custom options that doesnt exist */
        if (!$product) {
            $product = $this->getProduct();
        }

        if ($buyRequest->getAwBookingFrom()) {
            // Set "from" equal to "To" if no "to" specified
            if (!$buyRequest->getAwBookingTo()) {
                $buyRequest->setAwBookingTo($buyRequest->getAwBookingFrom());
            }

            $fromDate = new Zend_Date(
                $buyRequest->getAwBookingFrom(),
                Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );
            $toDate = new Zend_Date(
                $buyRequest->getAwBookingTo(),
                Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );

            // Add time and date
            list($fH, $fM) = self::_convertTimeTo24hArray($buyRequest->getAwBookingTimeFrom());
            list($tH, $tM) = self::_convertTimeTo24hArray($buyRequest->getAwBookingTimeTo());

            $fromDate->setHour((int)$fH)->setMinute((int)$fM);
            $toDate->setHour((int)$tH)->setMinute((int)$tM);

            $product->addCustomOption(self::FROM_DATE_OPTION_NAME, $fromDate->toString('Y-m-d', 'php'), $product);
            $product->addCustomOption(self::TO_DATE_OPTION_NAME, $toDate->toString('Y-m-d', 'php'), $product);
            $product->addCustomOption(self::FROM_TIME_OPTION_NAME, $fromDate->toString('H:i', 'php'), $product);
            $product->addCustomOption(self::TO_TIME_OPTION_NAME, $toDate->toString('H:i', 'php'), $product);

            /* Check if product can be added to cart */
            $isAvail = $this->isAvailable($product, $buyRequest->getQty(), true);
            if (!$isAvail) {
                Mage::throwException(
                //Mage::helper('booking')->__("Chosen quantity is not available. Only %s reservations left.", $itemsLeft)
                    Mage::helper('booking')->__("Chosen quantity is not available. Only reservations left.")
                );
            }
            return parent::prepareForCart($buyRequest, $product);
        }
        return Mage::helper('booking')->__('Please specify reservation information');
    }

    /**
     * Detects if specified date is first salable day
     *
     * @param Zend_Date $zDate
     *
     * @return bool
     */
    public function isFirstDay(Zend_Date $zDate)
    {
        $product = $this->_product ? $this->_product : $this->getProduct();
        $fromDate = new Zend_Date($product->getAwBookingDateFrom(), AW_Core_Model_Abstract::DB_DATE_FORMAT);
        return $zDate->compare($fromDate, Zend_Date::DATE_SHORT) == 0;
    }

    /**
     * Detects if specified date is last salable day
     *
     * @param Zend_Date $zDate
     *
*@return bool
     */
    public function isLastDay(Zend_Date $zDate)
    {
        $product = $this->_product ? $this->_product : $this->getProduct();
        $toDate = new Zend_Date($product->getAwBookingDateTo(), AW_Core_Model_Abstract::DB_DATE_FORMAT);
        return $zDate->compare($toDate, Zend_Date::DATE_SHORT) == 0;
    }

    /**
     * Check if product can be really bought
     *
     * @param null $product
     * @param int  $qty
     * @param bool $includeAvail
     *
     * @return bool
     */
    public function isAvailable($product = null, $qty = 1, $includeAvail = false)
    {
        if (!$qty) {
            $qty = 1;
        }
        if (is_null($product)) {
            $product = ($this->_product) ? $this->_product : $this->getProduct();
        }

        $fromDate = $product->getCustomOption(self::FROM_DATE_OPTION_NAME)->getValue();
        $toDate = $product->getCustomOption(self::TO_DATE_OPTION_NAME)->getValue();

        $from_time = $product->getCustomOption(self::FROM_TIME_OPTION_NAME)->getValue();
        $to_time = $product->getCustomOption(self::TO_TIME_OPTION_NAME)->getValue();

        $fromDate .= " $from_time";
        $toDate .= " $to_time";

        /**
         * @todo Attach time to "from" and "to"
         */
        $from = new Zend_Date($fromDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        if ($toDate) {
            $to = new Zend_Date($toDate, AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        } else {
            $to = clone $from;
        }

        if ($from->compare(time()) < 0) {
            Mage::throwException(
                Mage::helper('booking')->__("Chosen from date/time is in the past")
            );
            return false;
        }

        $now = Mage::app()->getLocale()->date(Mage::getSingleton('core/date')->gmtTimestamp(), null, null);
        if ($product->getAwBookingRangeType() == AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE) {
            $now->setHour(0)->setMinute(0)->setSecond(0);
        }
        if (
            $now->compare($from) > 0
            || $now->compare($to) > 0
        ) {
            Mage::throwException(
                Mage::helper('booking')->__("Chosen date/time is in the past")
            );
            return false;
        }

        if (!Mage::getModel('booking/checker')->getDateChecker()->isPeriodAvail($product, $from, $to)) {
            // Time/date out of bounds
            return false;
        }

        if (!Mage::getModel('booking/checker_bind')->isQtyAvailableForPeriod($product, $from, $to)) {
            // Check already booked
            return false;
        }
        return true;
    }

    /**
     * Returns first available day for product
     * This method checks for ranges, binded days and excluded days
     *
     * @return Zend_Date
     */
    public function getFirstAvailableDate()
    {
        /* @var $date Zend_Date */
        $today = Mage::app()->getLocale()->date(Mage::getSingleton('core/date')->gmtTimestamp(), null, null);
        if (($from = $this->getDateFrom()) !== self::HAS_NO_PERIOD_FROM) {
            if ($from->compare($today) > 0) {
                $minDate = $from;
            } else {
                $minDate = $today;
            }
        } else {
            $minDate = $today;
        }
        $minDate->setHour(0)->setMinute(0)->setSecond(0)->setMilliSecond(0); // Reset date

        if (
            $this->getProduct()->getAwBookingQratioMultiplier()
            == AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS
        ) {
            // Hours multiplier
            $method = 'addHour';
        } else {
            // Days multiplier
            $method = 'addDayOfYear';
            // To switch month forward
            Zend_Date::setOptions(array('extend_month' => true));
        }

        if (($maxDate = $this->getDateTo()) === self::HAS_NO_PERIOD_TO) {
            // Assert that $MaxDate is year later than $MinDate
            $maxDate = clone $minDate;
            $maxDate = $maxDate->addYear(1);
        }

        $checkerBind = Mage::getModel('booking/checker_bind')->setProduct($this->getProduct());
        $checker = Mage::getModel('booking/checker')->getDateChecker();
        if($minDate->compare($maxDate) != 0) {
            while ($minDate->compare($maxDate) <= 0) {
                // Check if day is available as for excluded days
                if ($checker->isDateAvail($this->getProduct(), $minDate, Mage::app()->getStore()->getId())) {
                    // iterate
                    if ($checkerBind->isQtyAvailableForDate(null, $minDate)) {
                        return $minDate;
                    }
                }
                // Iterate
                $minDate = call_user_func(array($minDate, $method), 1);
            }
        }
        return $minDate;
    }

    /**
     * Returns bookable period start as Zend_Date object
     *
     * @return Zend_Date | int
     */
    public function getDateFrom()
    {
        /**
         * @var $product Mage_Catalog_Model_Product
         */
        $productModel = $this->getProduct();
        $dateFrom = $productModel->getAwBookingDateFrom();
        if (is_null($dateFrom)) {
            // Not set.
            return self::HAS_NO_PERIOD_FROM;
        }
        $from = new Zend_Date($dateFrom, AW_Core_Model_Abstract::DB_DATE_FORMAT);
        return $from;
    }

    /**
     * Returns bookable period end as Zend_Date object
     *
     * @return Zend_Date
     */
    public function getDateTo()
    {
        /**
         * @var $product Mage_Catalog_Model_Product
         */
        $productModel = $this->getProduct();
        $dateFrom = $productModel->getAwBookingDateTo();
        if (is_null($dateFrom)) {
            // Not set. Return
            return self::HAS_NO_PERIOD_TO;
        }
        $from = new Zend_Date($dateFrom, AW_Core_Model_Abstract::DB_DATE_FORMAT);
        return $from;
    }

    /**
     * Converts array to other array convtaining 24h data
     * @param array $time
     *
     * @return array
     */
    protected static function _convertTimeTo24hArray($time)
    {
        if (!is_array($time)) {
            $time = array(0, 0);
        }
        list($hours, $minutes) = array(
            @$time[AW_Booking_Block_Catalog_Product_Options_Date::TYPE_HOURS],
            @$time[AW_Booking_Block_Catalog_Product_Options_Date::TYPE_MINUTES],
        );
        if (isset($time[AW_Booking_Block_Catalog_Product_Options_Date::TYPE_DAYPART])) {
            // Am/Pm
            if ($hours == 12) {
                $hours = 0;
            }
            if ($time[AW_Booking_Block_Catalog_Product_Options_Date::TYPE_DAYPART] == 'pm') {
                $hours += 12;
            }
        }
        return array($hours, $minutes);
    }

    /**
     * Converts array to time string
     *
     * @param array $time
     * @return string
     */
    protected function _convertTimeToStringFromArray($time)
    {
        list($hours, $minutes) = self::_convertTimeTo24hArray($time);
        return $hours . ":" . $minutes . ":00";
    }

    /**
     * Return time formatted in specified type
     *
     * @param      $time
     * @param bool $format
     *
     * @return array|string
     */
    public static function convertTime($time, $format = false)
    {
        list($hours, $minutes) = self::_convertTimeTo24hArray($time);
        if ($format == AW_Core_Model_Abstract::RETURN_STRING) {
            return $hours . ":" . $minutes . ":00";
        } else {
            return array(sprintf("%02s", $hours), sprintf("%02s", $minutes));
        }
    }

    /**
     * Stub to render price checker
     * This can be potential source of troubles
     *
     * @todo check if it intersects with magento logic
     *
     * @param Mage_Core_Model_Abstract $product
     *
     * @return boolean
     */
    public function hasOptions($product = null)
    {
        return true;
    }

    public function hasRequiredOptions($product = null)
    {
        return true;
    }

    protected function _toDate($date, $time = null)
    {
        $str = $date . ($time ? " $time" : "");
        $ts = strtotime($str);
        // Add offset

        list($Y, $m, $d, $H, $i) = explode("-", date('Y-m-d-H-i', $ts));
        $date = Mage::app()
            ->getLocale()
            ->storeDate(Mage::app()->getStore()->getId())
            ->setYear($Y)
            ->setMonth($m)
            ->setDay($d)
            ->setHour($H)
            ->setMinute($i)
        ;
        return $date;
    }

    protected function _glueTime($time)
    {
        if ($time) {
            return $time[0] . ":" . $time[1] . (isset($time[2]) ? " {$time[2]}" : '');
        }
        return "00:00";
    }

    public function isSalable($product = null)
    {
        /* New 'instant' way to update manage stock */
        if (is_null($product)) {
            $product = Mage::registry('product');
        }
        if (!$product->getAwBookingQuantity() && !is_null($product->getAwBookingQuantity())) {
            return false;
        }
        if ($product->getStockItem() && $product->getStockItem()->getManageStock()) {
            $product
                ->getStockItem()
                ->setManageStock(0)
                ->setUseConfigManageStock(0)
                ->setQty(1)
                ->save()
            ;
        }
        $today = new Zend_Date();
        $salable = true;
        if (!is_null($product->getAwBookingDateTo())) {
            $To = new Zend_Date($product->getAwBookingDateTo() . " " . str_replace(
                ",", ":", $product->getAwBookingTimeTo()
            ), AW_Core_Model_Abstract::DB_DATE_FORMAT);
            if ($today->compare($To) > 0) {
                $salable = false;
            }
        }
        if (!$salable) {
            return $salable;
        }
        return parent::isSalable($product);
    }

    /**
     * Returns if product is "virtual", e.g. requires no shipping
     *
     * @param object $product [optional]
     * @return bool
     */
    public function isVirtual($product = null)
    {
        if (is_null($product)) {
            $product = Mage::registry('product');
        }
        $product->load($product->getId());
        return !$product->getAwBookingShippingEnabled();
    }

    public function processBuyRequest($product, $buyRequest)
    {
        $options = parent::processBuyRequest($product, $buyRequest);
        foreach ($buyRequest->getData() as $key => $value) {
            if (strpos($key, 'booking')) {
                $options[$key] = $value;
            }
        }
        return $options;
    }
}
