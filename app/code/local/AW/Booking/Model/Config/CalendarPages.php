<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


/**
 * Class AW_Booking_Model_Config_CalendarPages
 *
 * Used to output the options array for "Number of calendar pages to show" field on the backend
 * and validating the input values for good measure.
 */
class AW_Booking_Model_Config_CalendarPages extends Mage_Core_Model_Config_Data
{
    /**
     * Produce options array for the field
     *
     * @return array
     */
    public function toOptionArray() {
        return array(
            array(
                'value' => '1',
                'label' => Mage::helper('booking')->__('1')
            ),
            array(
                'value' => '2',
                'label' => Mage::helper('booking')->__('2')
            ),
            array(
                'value' => '3',
                'label' => Mage::helper('booking')->__('3')
            ),
            array(
                'value' => '4',
                'label' => Mage::helper('booking')->__('4')
            ),
            array(
                'value' => '5',
                'label' => Mage::helper('booking')->__('5')
            ),
            array(
                'value' => '6',
                'label' => Mage::helper('booking')->__('6')
            ),
        );
    }

    /**
     * Check if the value is positive integer
     *
     * @return AW_Booking_Model_Config_CalendarPages
     */
    protected function _beforeSave()
    {
        $value = $this->getValue();
        if (intval($value) > 0) {
            $this->setValue(intval($value));
        } else {
            $this->unsValue();
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('booking')->__("Please enter a positive integer for 'Number of calendar pages to show' field. Previous value restored."));
        }
        return $this;
    }
}
