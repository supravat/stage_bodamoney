<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Mysql4_Prices_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('booking/prices');
    }

    /**
     * Return only records matching for date
     *
     * @param Zend_Date $date
     * @return AW_Booking_Model_Mysql4_Prices_Collection
     */
    public function addDateFilter(Zend_Date $date)
    {
        $dateString = $date->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT);
        $this->getSelect()->where("date_from <= '{$dateString}' AND date_to >= '{$dateString}'");
        return $this;
    }

    /**
     * Adds filter by entity_id (product_id)
     *
     * @param int $id entity id
     * @return AW_Booking_Model_Mysql4_Prices_Collection
     */
    public function addEntityIdFilter($id)
    {
        $this->getSelect()->where('entity_id = ?', $id);
        return $this;
    }

    /**
     * Adds filter by store_id
     *
     * @param int|null $storeId
     * @return AW_Booking_Model_Mysql4_Prices_Collection
     */
    public function addStoreIdFilter($storeId = null)
    {
        if ($storeId === null) {
            $storeId = Mage::app()->getStore()->getId();
        }
        $this->getSelect()->where('store_id = ? OR store_id = 0', $storeId);
        return $this;
    }
}