<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Mysql4_Order extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('booking/order', 'id');
    }

    /**
     * Cancel bind by id
     *
     * @param int $id
     * @return AW_Booking_Model_Mysql4_Order
     */
    public function cancelByOrderId($id)
    {
        $this->_getWriteAdapter()->update($this->getMainTable(), array('is_canceled' => 1), 'order_id = ' . ($id));
        return $this;
    }

    /**
     * Cancel bind by order item id
     *
     * @param int $itemId order item id
     * @param int $qty number of items to cancel
     * @return AW_Booking_Model_Mysql4_Order
     */
    public function cancelByOrderItemId($itemId, $qty = 0)
    {
        $adapter = $this->_getWriteAdapter();
        $sql = "UPDATE "
            . $adapter->quoteIdentifier($this->getMainTable(), true)
            . ' SET is_canceled = 1'
            . 'WHERE `order_item_id` = ' . $itemId
            . ($qty ? "LIMIT $qty" : "")
        ;
        $adapter->query($sql);
        return $this;
    }

    /**
     * Delete by quote item
     *
     * @param Mage_Sales_Model_Quote_Item $quoteItem
     * @return AW_Booking_Model_Mysql4_Order
     */
    public function deleteByQuoteItem(Mage_Sales_Model_Quote_Item $quoteItem)
    {
        $itemId = intval($quoteItem->getId());
        $where = "bind_type='" . AW_Booking_Model_Order::BIND_TYPE_CART . "' AND order_id=" . $itemId;
        $this->_getWriteAdapter()->delete($this->getMainTable(), $where);
        return $this;
    }

    /**
     * Delete by quote item id
     *
     * @param int $id
     * @return AW_Booking_Model_Mysql4_Order
     */
    public function deleteByQuoteItemId($id)
    {
        $condition = "bind_type='" . AW_Booking_Model_Order::BIND_TYPE_CART . "' AND order_id=" . intval($id);
        $this->_getWriteAdapter()->delete($this->getMainTable(), $condition);
        return $this;
    }

    /**
     * Delete by quote id
     *
     * @param int $id
     * @return AW_Booking_Model_Mysql4_Order
     */
    public function deleteByQuoteId($id)
    {
        $condition = "bind_type='" . AW_Booking_Model_Order::BIND_TYPE_CART . "' AND quote_id=" . intval($id);
        $this->_getWriteAdapter()->delete($this->getMainTable(), $condition);
        return $this;
    }

    /**
     * Updates product title for product id
     *
     * @param int $productId
     * @param string $title
     * @param int $storeId
     */
    public function setProductNameById($productId, $title, $storeId = null)
    {
        $adapter = $this->_getWriteAdapter();
        $prop = array(
            'product_name' => $title
        );
        if (!is_null($storeId)) {
            $adapter->update($this->getMainTable(), $prop, $adapter->quoteInto('product_id = ?', $productId));
        } else {
            $adapter->update($this->getMainTable(), $prop, $adapter->quoteInto('product_id = ?', $productId));
        }
    }
}