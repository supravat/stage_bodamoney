<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Excludeddays extends Mage_Core_Model_Abstract
{
    /**
     * Single day
     **/
    const TYPE_SINGLE           = 'single';
    /**
     * Recurrent day of week, 0-7
     **/
    const TYPE_RECURRENT_DAY    = 'recurrent_day';
    /**
     * Recurring date of month, e.g. 31
     **/
    const TYPE_RECURRENT_DATE   = 'recurrent_date';
    /**
     * Single period
     **/
    const TYPE_PERIOD           = 'period';
    /**
     * Recurrent period
     **/
    const TYPE_RECURRENT_PERIOD = 'recurrent_period';

    protected function _construct()
    {
        $this->_init('booking/excludeddays');
    }

    /**
     * get Exclude Day details
     *
     * @return array|int|string
     */
    public function getExcludeDayDetails()
    {
        $zDateFrom = new Zend_Date($this->getPeriodFrom(), AW_Core_Model_Abstract::DB_DATE_FORMAT);
        $zDateTo = new Zend_Date($this->getPeriodTo(), AW_Core_Model_Abstract::DB_DATE_FORMAT);
        switch ($this->getType()) {
            case self::TYPE_SINGLE:
                if ($this->getOutputFormat()) {
                    $stringDateFrom = $zDateFrom->toString($this->getOutputFormat());
                }
                $result = $stringDateFrom;
                break;
            case self::TYPE_RECURRENT_DAY:
                $arrayDate = $zDateFrom->toArray();
                $weekday = $arrayDate['weekday'] == 7 ? 0 : (int)$arrayDate['weekday'];
                $result = $weekday;
                break;
            case self::TYPE_RECURRENT_DATE:
                $arrayDate = $zDateFrom->toArray();
                $day = $arrayDate['day'];
                $result = (int)$day;
                break;
            case self::TYPE_PERIOD:
                if ($this->getOutputFormat()) {
                    $stringDateFrom = $zDateFrom->toString($this->getOutputFormat());
                    $stringDateTo = $zDateTo->toString($this->getOutputFormat());
                }
                $result = array('from' => $stringDateFrom, 'to' => $stringDateTo);
                break;
            case self::TYPE_RECURRENT_PERIOD:
                if ($this->getOutputFormat()) {
                    $stringDateFrom = $zDateFrom->toString($this->getOutputFormat());
                    $stringDateTo = $zDateTo->toString($this->getOutputFormat());
                }
                $result = array(
                    'from'   => $stringDateFrom,
                    'to'     => $stringDateTo,
                    'period' => $this->getPeriodRecurrenceType(),
                );
                break;
        }
        return $result;
    }

    /**
     * Checks if date is available
     *
     * @param Zend_Date $date
     *
     * @return boolean
     * * @throws AW_Core_Exception
     */
    public function isDateAvail(Zend_Date $date)
    {
        $from = new Zend_Date($this->getPeriodFrom(), AW_Core_Model_Abstract::DB_DATE_FORMAT);
        $to = new Zend_Date($this->getPeriodTo(), AW_Core_Model_Abstract::DB_DATE_FORMAT);

        if ($this->getType() == self::TYPE_SINGLE) {
            return $date->compare($from, Zend_Date::DATE_SHORT) != 0;
        }
        if ($this->getType() == self::TYPE_RECURRENT_DAY) {
            return $date->compare($from, Zend_Date::WEEKDAY) != 0;
        }
        if ($this->getType() == self::TYPE_RECURRENT_DATE) {
            return $date->compare($from, Zend_Date::DAY) != 0;
        }
        if ($this->getType() == self::TYPE_PERIOD) {
            return !(
                $date->compare($from, Zend_Date::DATE_SHORT) >= 0
                && $date->compare($to, Zend_Date::DATE_SHORT) <= 0
            );
        }
        if ($this->getType() == self::TYPE_RECURRENT_PERIOD) {
            $dateFrom = clone $date;
            $dateTo = clone $date;

            switch ($this->getPeriodRecurrenceType()) {
                case 'monthly':
                    $dateFrom->setDay($from->getDay());
                    $dateTo->setDay($to->getDay());
                    return !(
                        $date->compare($dateFrom, Zend_Date::DATE_SHORT) >= 0
                        && $date->compare($dateTo, Zend_Date::DATE_SHORT) <= 0
                    );
                    break;
                case 'yearly':
                    $dateFrom->setMonth($from->getMonth())->setDay($from->getDay());
                    $dateTo->setMonth($to->getMonth())->setDay($to->getDay());
                    return !(
                        $date->compare($dateFrom, Zend_Date::DATE_SHORT) >= 0
                        && $date->compare($dateTo, Zend_Date::DATE_SHORT) <= 0
                    );
                    break;
            }
            throw new AW_Core_Exception("Unsupported recurrence interval '{$this->getPeriodRecurrenceType()}'");
        }
    }

    /**
     * Wrapper for getPeriodType()
     *
     * @return string
     */
    public function getType()
    {
        return $this->getPeriodType();
    }

    /**
     * Sets output format. If no format specified, Zend_Date objects are returned
     *
     * @return string
     */
    public function getOutputFormat()
    {
        if (!$this->getData('output_format')) {
            return false;
        }
        return $this->getData('output_format');
    }
}