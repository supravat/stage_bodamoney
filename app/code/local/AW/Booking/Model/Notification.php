<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Notification
{
    public function sendQtyNotAvailableAlert($order, $product, $qtyAvailable)
    {
        $productLink = Mage::getModel('adminhtml/url')->getUrl(
            'adminhtml/catalog_product/edit', array('id' => $product->getProductId())
        );
        $orderLink = Mage::getModel('adminhtml/url')->getUrl(
            'adminhtml/sales_order/view', array('order_id' => $order->getId())
        );
        $orderDate = Mage::app()->getLocale()->date($order->getCreatedAt(), Varien_Date::DATETIME_INTERNAL_FORMAT)
            ->toString(Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM))
        ;
        if ($order->getCustomerId()) {
            $customerLink = Mage::getModel('adminhtml/url')->getUrl(
                'adminhtml/customer/edit', array('id' => $order->getCustomerId())
            );
        } else {
            $customerLink = null;
        }

        $variables = array(
            'product_sku'            => $product->getSku(),
            'product_name'           => $product->getName(),
            'product_link'           => $productLink,
            'current_reservations'   => (int)$product->getQtyOrdered(),
            'reservations_available' => max(0, $qtyAvailable),
            'order_date'             => $orderDate,
            'order_id'               => $order->getIncrementId(),
            'order_link'             => $orderLink,
            'customer_name'          => $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname(),
            'customer_link'          => $customerLink,
        );

        $subject = Mage::helper('booking')->__('Notification about unsuccessful ')
            . $product->getName()
            . Mage::helper('booking')->__(' reservation')
        ;
        $sender = Mage::helper('booking/notification')->getNotifSender();
        $emailTemplate = Mage::helper('booking/notification')->getTemplate();
        $this->_sendNotify(
            $sender['mail'], $sender['name'], $sender['mail'], $sender['name'], $subject, $emailTemplate, $variables
        );
    }

    protected function _sendNotify($toEmail, $toName, $fromEmail, $fromName, $subject, $emailTemplate, $variables)
    {
        $emailTemplate->getProcessedTemplate($variables);
        $emailTemplate
            ->setSenderName($fromName)
            ->setSenderEmail($fromEmail)
            ->setTemplateSubject($subject)
            ->send($toEmail, $toName, $variables)
        ;
    }
}