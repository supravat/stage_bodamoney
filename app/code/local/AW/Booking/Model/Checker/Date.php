<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Checker_Date extends Varien_Object
{
    /**
     * Return unavailable day
     *
     * @param     $product_id
     * @param int $store_id
     * @return mixed
     */
    public function getUnavailDays($product_id, $store_id = 0)
    {
        if (!$this->getData('unavail_days')) {
            $model = Mage::getSingleton('catalog/product');
            if ($product_id instanceof $model) {
                $product_id = $product_id->getId();
            }

            $rules = $this->_getCollection($product_id, $store_id);
            $data = array(
                AW_Booking_Model_Excludeddays::TYPE_SINGLE           => array(),
                AW_Booking_Model_Excludeddays::TYPE_RECURRENT_DAY    => array(),
                AW_Booking_Model_Excludeddays::TYPE_RECURRENT_DATE   => array(),
                AW_Booking_Model_Excludeddays::TYPE_PERIOD           => array(),
                AW_Booking_Model_Excludeddays::TYPE_RECURRENT_PERIOD => array(),
            );
            foreach ($rules as $rule) {
                if ($format = $this->getOutputFormat()) {
                    $rule->setOutputFormat($format);
                }
                $excludeDayDetails = $rule->getExcludeDayDetails();
                $type = $rule->getType();

                $data[$type][] = $excludeDayDetails;
            }
            $this->setData('unavail_days', $data);
        }
        return $this->getData('unavail_days');
    }

    /**
     * Detectes if product is salable for date
     *
*@param object $productId
     * @param Zend_Date $date
     * @param int $storeId
     *
*@return bool
     */
    public function isDateAvail($productId, Zend_Date $date, $storeId = 0)
    {
        $model = Mage::getSingleton('catalog/product');
        if ($productId instanceof $model) {
            $productId = $productId->getId();
        }
        $collection = $this->getTimeCollection($productId, $storeId);
        foreach ($collection as $rule) {
            if (!$rule->isDateAvail($date)) {
                return false;
            }
        }
        return true;
    }

    public function getTimeCollection($productId, $storeId = 0)
    {
        $cProductId = Mage::registry('booking-time-product-id') ? Mage::registry('booking-time-product-id') : null;
        $cStoreId = Mage::registry('booking-time-store-id') ? Mage::registry('booking-time-store-id') : null;
        if ($productId != Mage::registry('booking-time-product-id')) {
            Mage::unregister('booking-time-product-id');
            Mage::register('booking-time-product-id', $productId);
        }
        if ($storeId != Mage::registry('booking-time-store-id')) {
            Mage::unregister('booking-time-store-id');
            Mage::register('booking-time-store-id', $storeId);
        }
        if (
            $cProductId != Mage::registry('booking-time-product-id')
            || $cStoreId != Mage::registry('booking-time-store-id')
        ) {
            Mage::unregister('booking-time-collection');
            Mage::register('booking-time-collection', $this->_getCollection($productId, $storeId));
        }
        return Mage::registry('booking-time-collection');
    }

    /**
     * Check if period is available
     *
     * @param           $productId
     * @param Zend_Date $from
     * @param Zend_Date $to
     * @param int       $storeId
     * @param int       $failOnNoMultiplier
     *
     * @return bool
     * @throws AW_Core_Exception
     */
    public function isPeriodAvail($productId, Zend_Date $from, Zend_Date $to, $storeId = 0, $failOnNoMultiplier = 0)
    {
        $model = Mage::getSingleton('catalog/product');
        if (!($productId instanceof $model)) {
            $product = $model->load($productId);
        } else {
            $product = $productId;
            if (!$product->getAwBookingQratioMultiplier()) {
                if (!$failOnNoMultiplier) {
                    // Reload product if no multiplier is load
                    return $this->isPeriodAvail($product->getId(), $from, $to, $storeId, true);
                } else {
                    $exceptionMessage = "Can't load product #{$product->getId()} as bookable. Is it really bookable?";
                    throw new AW_Core_Exception($exceptionMessage);
                }
            }
        }
        $_datesCache = array();
        if ($from->compare($to) > 0) {
            $fromDate = clone $to;
            $toDate = clone $from;
        } else {
            $fromDate = clone $from;
            $toDate = clone $to;
        }
        if (
            $product->getAwBookingQratioMultiplier()
            != AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS
        ) {
            $fromDate->setHour(0)->setMinute(0)->setSecond(0);
            $toDate->setHour(0)->setMinute(0)->setSecond(0);
        }
        $zDate = $fromDate;
        // Check if to is not out of bounds
        if ($product->getTypeInstance()->getDateTo() !== AW_Booking_Model_Product_Type_Bookable::HAS_NO_PERIOD_TO) {
            if ($toDate->compare($product->getTypeInstance()->getDateTo()) > 0) {
                return false;
            }
        }

        while ($toDate->compare($zDate, Zend_Date::DATE_MEDIUM) >= 0) {
            if (!isset($_datesCache[$zDate->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT)])) {
                foreach ($this->_getCollection($productId, $storeId) as $rule) {
                    if (!$rule->isDateAvail($zDate)) {
                        return false;
                    }
                }
                $_datesCache[$zDate->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT)] = true;
            }

            if (
                $product->getAwBookingQratioMultiplier()
                == AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS
            ) {
                $zDate = $zDate->addHour(1);
            } else {
                $zDate = $zDate->addDayOfYear(1);
            }
        }
        return true;
    }

    /**
     * Get collection of excluded days rules
     * @param     $product_id
     * @param int $store_id
     *
     * @return mixed
     */
    protected function _getCollection($product_id, $store_id = 0)
    {
        if (($product_id instanceof Mage_Catalog_Model_Product)) {
            $product_id = $product_id->getId();
        }
        if ($this->getData('product_id') != $product_id) {
            $this
                ->setData('product_id', $product_id)
                ->setData('collection', null)
            ;
        }
        if (!$this->getData('collection')) {
            $collection = array();
            $coll = Mage::getModel('booking/excludeddays')
                ->getCollection()
                ->addEntityIdFilter($product_id)
                ->addStoreIdFilter($store_id)
            ;
            foreach ($coll as $rule) {
                $collection[] = $rule;
            }
            $this->setData('collection', $collection);
        }
        $arr = $this->getData('collection');
        reset($arr);
        return $this->getData('collection');
    }

    /**
     * Sets output format. If no format specified, Zend_Date objects are returned
     *
     * @return string
     */
    public function getOutputFormat()
    {
        if (!$this->getData('output_format')) {
            return false;
        }
        return $this->getData('output_format');
    }
}