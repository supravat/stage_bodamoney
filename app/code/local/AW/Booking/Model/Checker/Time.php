<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Checker_Time
{
    private $_dayFrom;
    private $_dayTo;
    private $_from;
    private $_to;
    private $_midnightCrossing;

    public function init($product, Zend_Date $day)
    {
        $model = Mage::getModel('catalog/product');
        if ($product instanceof $model) {
            $productModel = $product;
        } else {
            $productModel = $model->load($product);
        }
        $day->setSecond(0);
        $this->_dayFrom = clone $day;
        $this->_dayTo = clone $day;
        $this->_dayFrom->setHour(0)->setMinute(0);
        $this->_dayTo->setHour(23)->setMinute(0);

        $f = clone $day;
        $t = clone $day;
        list($fh, $fm) = explode(",", $productModel->getAwBookingTimeFrom());
        list($th, $tm) = explode(",", $productModel->getAwBookingTimeTo());
        $f->setHour($fh)->setMinute($fm);
        $t->setHour($th)->setMinute($tm);

        $now = Mage::app()->getLocale()->date(Mage::getSingleton('core/date')->gmtTimestamp(), null, null);
        $now->setMinute(0)->setSecond(0);

        /**
         * This is questionable behaviour, inherited from the old model code:
         * when you set the DATETIME range type,
         * this operation makes the time interval to be applied for the first and last day only.
         * Other days will always have the full range. While this may be worthwhile behaviour,
         * it contradicts with the time selector control.
         */
        if ($productModel->getAwBookingRangeType() == AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATETIME) {
            $this->_from = clone $this->_dayFrom;
            $this->_to = clone $this->_dayTo;
            if ($productModel->getTypeInstance()->isFirstDay($day)) {
                $this->_from = $f;
            }
            if ($productModel->getTypeInstance()->isLastDay($day)) {
                $this->_to = $t;
            }
        } else {
            if ($f->compare($t) > 0) {
                $this->_from = $t;
                $this->_to = $f;
                $this->_midnightCrossing = true;
            } else {
                $this->_from = $f;
                $this->_to = $t;
                $this->_midnightCrossing = false;
            }
        }
        if ($this->_dayFrom->compare($now) < 0) {
            $this->_dayFrom = $now;
        }
        if ($this->_from->compare($now) < 0) {
            $this->_from = $now;
        }
        return $this;
    }

    /**
     * @return Zend_Date
     */
    public function getFirstHour()
    {
        if ($this->_midnightCrossing) {
            return clone $this->_dayFrom;
        } else {
            return clone $this->_from;
        }
    }

    public function isFirstHour(Zend_Date $date)
    {
        if ($this->_midnightCrossing) {
            $l = $this->_dayFrom;
        } else {
            $l = $this->_from;
        }
        if ($l->compare($date, Zend_Date::HOUR_SHORT) == 0) {
            return true;
        }
        return false;
    }

    public function isLastHour(Zend_Date $date)
    {
        if ($this->_midnightCrossing) {
            $l = $this->_dayTo;
        } else {
            $l = $this->_to;
        }
        if ($l->compare($date, Zend_Date::HOUR_SHORT) == 0) {
            return true;
        }
        return false;
    }

    public function isPastLastHour(Zend_Date $date)
    {
        if ($this->_midnightCrossing) {
            $l = clone $this->_dayTo;
        } else {
            $l = clone $this->_to;
        }
        $l->addHour(1);
        if ($l->compare($date) <= 0) {
            return true;
        }
        return false;
    }

    public function isAvailable(Zend_Date $date) {
        if ($this->_midnightCrossing) {
            if (
                $this->_from->compare($date) >= 0
                || $this->_to->compare($date) < 0
            ) {
                return true;
            }
        } else {
            if (
                $this->_from->compare($date) < 0
                && $this->_to->compare($date) >= 0
            ) {
                return true;
            }
        }
        return false;
    }

}
