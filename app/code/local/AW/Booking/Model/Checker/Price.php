<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Checker_Price
{
    /**
     * Get price for date
     *
     * @param           $product_id
     * @param Zend_Date $zDate
     * @param int       $store_id
     *
     * @return int
     */
    public function getPriceForDate($product_id, Zend_Date $zDate, $store_id = 0)
    {
        $model = Mage::getSingleton('catalog/product');
        if ($product_id instanceof $model) {
            $product_id = $product_id->getEntityId();
        }
        $coll = Mage::getModel('booking/prices')
            ->getCollection()
            ->addEntityIdFilter($product_id)
            ->addStoreIdFilter($store_id)
            ->addDateFilter($zDate)
        ;

        // Get the first matching price
        $item = false;
        foreach ($coll as $item) {
            break;
        }

        if (!$item) {
            return AW_Booking_Model_Checker::NO_PRICE; // No price rules found
        }

        // Not progressive price
        if (!$item->getIsProgressive()) {
            return $item->getPriceFrom();
        }

        // Progressive price
        $from = new Zend_Date($item->getDateFrom(), AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        $to = new Zend_Date($item->getDateTo(), AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
        $toDate = clone($to);

        $delta = $toDate->sub($from);
        /* Zend > 1.9 check */
        if ($delta instanceof Zend_Date) {
            $delta = $delta->toValue();
        }

        $daysDelta = $delta / AW_Booking_Model_Checker::ONE_DAY;
        $pricesDelta = $item->getPriceTo() - $item->getPriceFrom();
        $dayPrice = $pricesDelta / $daysDelta;

        // Calculate how much days spent till date
        $_Date = clone $zDate;
        $_Date->setHour(0)->setMinute(0)->setSecond(0);
        // Days spent
        $dateSub = $_Date->sub($from);
        /* Zend > 1.9 check */
        if ($dateSub instanceof Zend_Date) {
            $dateSub = $dateSub->toValue();
        }

        $daysSpent = $dateSub / AW_Booking_Model_Checker::ONE_DAY;
        return $item->getPriceFrom() + $daysSpent * $dayPrice;
    }

    /**
     * Get price for period
     *
     * @param            $entityId
     * @param Zend_Date  $from
     * @param Zend_Date  $to
     * @param float|bool $basePrice
     * @param int        $storeId
     * @param int        $failOnNoMultiplier
     * @param null       $return
     *
     * @return array|bool|float|int|mixed
     * @throws AW_Core_Exception
     */
    public function getPriceForPeriod(
        $entityId, Zend_Date $from, Zend_Date $to,
        $basePrice = false, $storeId = 0, $failOnNoMultiplier = 0, $return = null
    )
    {
        Zend_Date::setOptions(array('extend_month' => true));

        $model = Mage::getModel('catalog/product');
        if (!($entityId instanceof $model)) {
            $product = $model->load($entityId);
        } else {
            $product = $entityId;
            if (!$product->getAwBookingQratioMultiplier()) {
                if (!$failOnNoMultiplier) {
                    // Reload product if no multiplier is load
                    return $this->getPriceForPeriod($product->getId(), $from, $to, $basePrice, $storeId, true, $return);
                } else {
                    $exceptionMessage = "Can't load product #{$product->getId()} as bookable. Is it really bookable?";
                    throw new AW_Core_Exception($exceptionMessage);
                }
            }
        }

        // Try to retrieve base price if not specified
        if ($basePrice === false) {
            $basePrice = $product->getData('price');
        }

        if ($from->compare($to) > 0) {
            $_From = clone $to;
            $_To = clone $from;
        } else {
            $_From = clone $from;
            $_To = clone $to;
        }

        if ($product->getAwBookingQratioMultiplier() != AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS) {
            $_From->setHour(0)->setMinute(0)->setSecond(0);
            $_To->setHour(0)->setMinute(0)->setSecond(0);
        }

        $zDate = $_From;
        $price = 0;

        $isTypeDate = $product->getAwBookingRangeType() == AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE;
        $comparision = $isTypeDate ? Zend_Date::DATE_MEDIUM : null;

        if ($_To->compare($zDate, $comparision) == 0) {
            if ($product->getAwBookingRangeType() != AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE) {
                $_To = $to->addHour(1);
            } else {
                $_To = $to->addDayOfYear(1);
            }
        } else {
            if (
                ($product->getAwBookingQratioMultiplier()
                    != AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS)
                || ($product->getAwBookingRangeType() == AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE)
            ) {
                $_To = $to->addDayOfYear(1);
            }
        }

        $occurencies = 0;
        while ($_To->compare($zDate, $comparision) > 0) {
            $occurencies += 1;
            if (isset($_price_cache[$zDate->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT)])) {
                $_price = $_price_cache[$zDate->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT)];
            } else {
                $_price = $this->getPriceForDate($entityId, $zDate, $storeId);
                $_price_cache[$zDate->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT)] = $_price;
            }
            if ($_price != AW_Booking_Model_Checker::NO_PRICE) {
                $delta = $_price;
            } else {
                $delta = $basePrice;
            }

            $price += $delta;
            if ($product->hasCustomOptions() && $product->getAwBookingMultiplyOptions()) {
                $optionsPrice = $product->getPriceModel()->getOptionsPrice($product, $delta);
                $price += floatval($optionsPrice);
            }

            if ($product->getAwBookingQratioMultiplier()
                == AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS
            ) {
                $zDate = $zDate->addHour(1);
            } else {
                $zDate = $zDate->addDayOfYear(1);
            }
        }

        if ($product->hasCustomOptions() && !$product->getAwBookingMultiplyOptions()) {
            $optionsPrice = $product->getPriceModel()->getOptionsPrice($product, $price);
            $price += floatval($optionsPrice);
        }

        if ($return == AW_Core_Model_Abstract::RETURN_ARRAY) {
            return array($price, $occurencies);
        }
        return $price;
    }
}