<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Model_Checker_Bind extends Varien_Object
{
    protected $skipTimeCheck = false;

    public function _construct()
    {
        Zend_Date::setOptions(array('extend_month' => true));
        return parent::_construct();
    }

    public function isQtyAvailable($product_id, Zend_Date $Date, $qty = 1, $includeCart = true)
    {
        return $this->isQtyAvailableForDate($product_id, $Date, $qty, $includeCart);
    }

    public function isQtyAvailableForDate($productId, Zend_Date $zDate, $qty = 1, $includeCart = true)
    {
        $_date = clone $zDate;
        $this->skipTimeCheck = true;
        if ($this->getProduct()) {
            $product = $this->getProduct();
        } else {
            $model = Mage::getModel('catalog/product');
            if ($productId instanceof $model) {
                $product = $productId;
            } else {
                $product = $model->load($productId);
            }
        }

        $qRatioMultiplierHoursType = AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS;
        if (
            !$this->skipTimeCheck
            && $product->getAwBookingQratioMultiplier() == $qRatioMultiplierHoursType
            && $zDate->Compare($product->getData('aw_booking_time_to'), Zend_Date::TIME_SHORT) > -1
        ) {
            return false;
        }

        $Quote = Mage::helper('checkout')->getQuote();
        $quoteId = 0;
        if ($includeCart && $Quote && Mage::app()->getRequest()->getActionName() != 'configure') {
            $quoteId = $Quote->getId();
        }

        $Orders = Mage::getModel('booking/order')
            ->getCollection()
            ->addQuoteIdFilter($quoteId)
            ->addProductIdFilter($product->getId())
        ;

        if ($product->getAwBookingQratioMultiplier() == $qRatioMultiplierHoursType) {
            $Orders->addBindDateTimeFilter($_date);
        } else {
            $Orders->addBindDateFilter($_date);
        }

        $total_binds = $Orders->count();
        if (!($bookingQty = $product->getAwBookingQuantity())) {
            $product = $product->load($product->getId());
            $bookingQty = $product->getAwBookingQuantity();
        }
        return $total_binds <= ($bookingQty - $qty);
    }

    /**
     * Check if period if specified quantity is available for period
     *
     * @param object     $productId
     * @param Zend_Date  $from
     * @param Zend_Date  $to
     * @param int|object $qty [optional]
     * @param bool       $includeCart
     *
     * @internal param \Zend_Date $From
     * @internal param \Zend_Date $To
     * @return bool
     */
    public function isQtyAvailableForPeriod($productId, Zend_Date $from, Zend_Date $to, $qty = 1, $includeCart = true)
    {
        if (
            Mage::app()->getRequest()->getControllerName() == 'cart'
            && Mage::app()->getRequest()->getActionName() == 'updateItemOptions'
        ) {
            $includeCart = false;
        }

        $fromDate = clone $from;
        $toDate = clone $to;
        $model = Mage::getModel('catalog/product');
        if ($productId instanceof $model) {
            $productModel = $productId;
        } else {
            $productModel = $model->load($productId);
        }

        $qRatioMultiplierHoursType = AW_Booking_Model_Entity_Attribute_Source_Qratiomultipliertype::HOURS;
        if ($productModel->getAwBookingQratioMultiplier() == $qRatioMultiplierHoursType) {
            $method = 'addHour';
        } else {
            $method = 'addDayOfYear';
        }

        while ($this->compareDateOrTime($productModel->getAwBookingRangeType(), $fromDate, $toDate)) {
            if (!$this->isQtyAvailable($productModel, $fromDate, $qty, $includeCart)) {
                return false;
            }
            $fromDate = call_user_func(array($fromDate, $method), 1);
        }
        return true;
    }

    public function isQtyAvailableForPeriodCheck($productId, Zend_Date $from, Zend_Date $to, $qty = 1)
    {
        $model = Mage::getModel('catalog/product');
        if ($productId instanceof $model) {
            $productModel = $productId;
        } else {
            $productModel = $model->load($productId);
        }

        list($h, $m, $s) = explode(',', $productModel->getAwBookingTimeFrom());
        $productFrom = ($productModel->getAwBookingDateFrom()) ? new Zend_Date($productModel->getAwBookingDateFrom())
            : new Zend_Date();
        $productFrom->setTime('00:00:00');
        if ($productModel->getAwBookingRangeType() != 'date_fromto') {
            $productFrom->addHour((int)$h);
            $productFrom->addMinute((int)$m);
            $productFrom->addSecond((int)$s);
        }

        list($h, $m, $s) = explode(',', $productModel->getAwBookingTimeTo());
        if ($productModel->getAwBookingDateTo()) {
            $productTo = new Zend_Date($productModel->getAwBookingDateTo());
        } else {
            $productTo = new Zend_Date();
            $productTo->addYear(10);
        }

        $productTo->setTime('23:59:59');
        if ($productModel->getAwBookingRangeType() != 'date_fromto') {
            $productTo->addHour((int)$h);
            $productTo->addMinute((int)$m);
            $productTo->addSecond((int)$s);
        }
        $orderedQty = Mage::getModel('booking/order')->getOrderedQty($productModel, $from, $to);

        if (
            $from >= $productFrom
            && $from < $productTo
            && $to >= $productFrom
            && $to <= $productTo
            && $qty <= ($productModel->getAwBookingQuantity() - $orderedQty)
        ) {
            return true;
        }
        return false;
    }

    /**
     * Compares if $compareType is date, then use <= else (datetime and time) use <
     *
     * @param string    $compareType
     * @param Zend_Date $From
     * @param Zend_Date $To
     * @return bool
     */
    private function compareDateOrTime($compareType, $From, $To)
    {
        $compareResult = $From->compare($To) < 0;
        if ($compareType == AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE) {
            $compareResult = $From->compare($To) <= 0;
        }
        return $compareResult;
    }

    /**
     * Return unavailable dates as array
     *
     * @param           $product
     * @param Zend_Date $from
     * @param Zend_Date $to
     * @param int       $qty
     * @param bool      $includeCart
     *
*@return array
     */
    public function getUnavailDays($product, Zend_Date $from, Zend_Date $to, $qty = 1, $includeCart = true)
    {
        $dates = array();
        // Clone from and to to not affect original values
        $fromDate = clone $from;
        $toDate = clone $to;
        $this->setProduct($product->load($product->getId()));
        while ($fromDate->compare($toDate) <= 0) {
            $this->skipTimeCheck = true;
            $dates[$fromDate->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT)] = $this->isQtyAvailable(
                null, $fromDate, $qty, $includeCart
            );
            $fromDate = $fromDate->addDayOfYear(1);
        }
        return $dates;
    }
}
