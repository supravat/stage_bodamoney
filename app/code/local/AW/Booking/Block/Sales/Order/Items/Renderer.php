<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Sales_Order_Items_Renderer extends Mage_Sales_Block_Order_Item_Renderer_Default
{
    public function getItemOptions()
    {
        $result = array();
        if ($options = $this->getOrderItem()->getProductOptions()) {
            if (isset($options['info_buyRequest'])) {
                $reservationFrom = @$options['info_buyRequest']['aw_booking_from'];
                $reservationTo = @$options['info_buyRequest']['aw_booking_to'];
                $reservationTimeFrom = @$options['info_buyRequest']['aw_booking_time_from'];
                $reservationTimeTo = @$options['info_buyRequest']['aw_booking_time_to'];
                if ($reservationFrom) {
                    $from = new Zend_Date(
                        $reservationFrom,
                        Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                    );
                    $to = new Zend_Date(
                        $reservationTo,
                        Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                    );
                    $product = Mage::getModel('catalog/product')->load($this->getOrderItem()->getProductId());
                    $displayTime
                        = $product->getAwBookingRangeType() != AW_Booking_Model_Entity_Attribute_Source_Rangetype::DATE;
                    if ($displayTime) {
                        $timeFrom = AW_Booking_Model_Product_Type_Bookable::convertTime(
                            $reservationTimeFrom, AW_Core_Model_Abstract::RETURN_ARRAY
                        );
                        $timeTo = AW_Booking_Model_Product_Type_Bookable::convertTime(
                            $reservationTimeTo, AW_Core_Model_Abstract::RETURN_ARRAY
                        );
                        $from->setHour(@$timeFrom[0]);
                        $from->setMinute(@$timeFrom[1]);
                        $to->setHour(@$timeTo[0]);
                        $to->setMinute(@$timeTo[1]);
                    }
                    $result[] = array(
                        'label' => $this->__('Reservation from:'),
                        'value' => $this->formatDate($from, 'short', $displayTime),
                    );
                    $result[] = array(
                        'label' => $this->__('Reservation to:'),
                        'value' => $this->formatDate($to, 'short', $displayTime),
                    );
                }
            }
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }
        return $result;
    }
}