<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Js extends Mage_Adminhtml_Block_Template
{
    /* Customer's block of JS helpers */
    public function getProduct()
    {
        return Mage::getModel('catalog/product')->load(Mage::registry('current_product')->getId());
    }

    public function getCalendarQtyAvailable($product)
    {
        $dates = array();
        $collection = Mage::getModel('booking/order')->getCollection();
        $collection->addProductIdFilter($product->getId());
        $collection->groupByOrderId();
        $collection->load();
        foreach ($collection as $item) {
            $from = new Zend_Date($item->getBindStart(), AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
            $to = new Zend_Date($item->getBindEnd(), AW_Core_Model_Abstract::DB_DATETIME_FORMAT);
            while ($from->compare($to) <= 0) {
                $this->skipTimeCheck = true;
                $key = date(
                    AW_Booking_Helper_Yui::DAY_FORMAT,
                    strtotime($from->toString(AW_Core_Model_Abstract::DB_DATE_FORMAT))
                );
                $dates[$key] = Mage::getModel('booking/checker_bind')->isQtyAvailableForDate(
                    $product->getId(), $from, $item->getTotalItems(), false
                );
                $from = $from->addDayOfYear(1);
            }
            unset($from, $to);
        }
        return Zend_Json::encode($dates);
    }
}