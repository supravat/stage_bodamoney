<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Adminhtml_Sales_Order_Info
    extends Mage_Adminhtml_Block_Sales_Order_Abstract
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    public function getTabLabel()
    {
        return $this->__('Booking');
    }

    public function getTabTitle()
    {
        return $this->__('Booking Information');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        // Show only if there are reserved items in order
        return !$this->getItemsCollection()->getSize();
    }

    public function getItemsCollection()
    {
        $order = $this->getOrder();
        $collection = Mage::getModel('booking/order')->getCollection()
            ->addOrderIdFilter($order->getIncrementId())
            ->load()
        ;
        return $collection;
    }

    public function getCollection()
    {
        return $this->getItemsCollection();
    }
}