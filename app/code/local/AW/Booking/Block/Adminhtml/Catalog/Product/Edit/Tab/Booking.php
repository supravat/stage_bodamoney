<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Adminhtml_Catalog_Product_Edit_Tab_Booking
    extends Mage_Adminhtml_Block_Widget
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Reference to product objects that is being edited
     *
     * @var Mage_Catalog_Model_Product
     */
    protected $_product = null;

    protected $_config = null;

    /**
     * Class constructor
     *
     */
    public function _construct()
    {
        $this->setTemplate('booking/product/edit/information.phtml');
    }

    /**
     * Get tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Booking Information');
    }

    public function getTabTitle()
    {
        return $this->__('Booking Information');
    }

    /**
     * Detect if tab can be shown
     * @return bool
     */
    public function canShowTab()
    {
        return $this->getProduct()->getTypeId() == AW_Booking_Helper_Config::PRODUCT_TYPE_CODE;
    }

    /**
     * Check if tab is hidden
     * @return boolean
     */
    public function isHidden(){
        return !$this->canShowTab();
    }

    protected function _toHtml()
    {
        $accordion = $this->getLayout()
            ->createBlock('adminhtml/widget_accordion')
            ->setId('bookingInfo2')
        ;
        $calendar = $this->getLayout()->createBlock('booking/adminhtml_catalog_product_edit_tab_information_calendar');
        $accordion->addItem(
            'samples2',
            array(
                'title'   => $this->__('Booked Dates'),
                'content' => $calendar->toHtml(),
                'open'    => true,
            )
        );
        $this->setChild('accordion', $accordion);
        return parent::_toHtml();
    }

    /**
     * Return current product
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (!$this->getData('product')) {
            $this->setData('product', Mage::registry('product'));
        }
        return $this->getData('product');
    }
}