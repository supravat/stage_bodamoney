<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Adminhtml_Products_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productsGrid');
        $this->setDefaultSort('created_time');
        $this->setDefaultDir('DESC');
    }

    protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('booking/order_collection')
            ->addProductIdFilter($this->getRequest()->getParam('id'))
        ;
        $this->setCollection($collection);

        // Calculate offset in hours for admin
        $offset = Mage::app()->getLocale()->storeDate()->get(Zend_Date::TIMEZONE_SECS);
        $this->getCollection()->getSelect()->columns(
            "FROM_UNIXTIME(UNIX_TIMESTAMP(bind_start) - " . $offset . ") as bind_start_ftd"
        );
        $this->getCollection()->getSelect()->columns(
            "FROM_UNIXTIME(UNIX_TIMESTAMP(bind_end) - " . $offset . ") as bind_end_ftd"
        );
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'order_id',
            array(
                'header' => $this->__('Order'),
                'align'  => 'right',
                'width'  => '100px',
                'index'  => 'order_id',
            )
        );
        $this->addColumn(
            'created_time',
            array(
                'type'   => 'datetime',
                'header' => $this->__('Order date'),
                'align'  => 'left',
                'index'  => 'created_time',
                'width'  => '200px',
            )
        );
        $this->addColumn(
            'bind_start',
            array(
                'type'   => 'datetime',
                'header' => $this->__('Reservation from'),
                'align'  => 'left',
                'index'  => 'bind_start_ftd',
                'width'  => '200px',
            )
        );
        $this->addColumn(
            'bind_end',
            array(
                'type'   => 'datetime',
                'header' => $this->__('Reservation to'),
                'align'  => 'left',
                'index'  => 'bind_end_ftd',
                'width'  => '200px',
            )
        );
        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportXml', $this->__('XML'));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        $orderId = Mage::getModel('sales/order')->loadByIncrementId($row->getOrderId())->getId();
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $orderId));
    }
}