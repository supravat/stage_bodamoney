<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Catalog_Product_View_Yuical extends Mage_Catalog_Block_Product_Abstract
{
    const READ_DATE_FORMAT      = "M/d/yyyy"; // This is format accepted by YUI by default
    const READ_DATE_PAGE_FORMAT = "M/yyyy";   // This is format accepted by YUI by default

    public function getProduct($product_id = 0)
    {
        if (!$this->getData('product')) {
            $model = Mage::getSingleton('booking/booking');
            if ($product_id instanceof $model) {
                $model = $product_id;
            } elseif ($product_id || ($product_id = $this->getRequest()->getParam('id'))) {
                $model->load($product_id);
            } elseif ($product = Mage::registry('product')) {
                $model = $product;
            }
            $this->setData('product', $model);
        }
        $this->setData('product', Mage::registry('product'));
        return $this->getData('product');
    }

    /**
     * Returns encoded JSON content
     * @return string
     */
    public function getUnavailDaysJSON()
    {
        return Zend_Json::encode(
            Mage::getModel('booking/checker')
                ->getDateChecker()
                ->setOutputFormat(self::READ_DATE_FORMAT)
                ->getUnavailDays($this->getProduct(), (int)Mage::app()->getStore()->getId())
        );
    }

    /**
     * Returns encoded JSON content
     * @return string
     */
    public function getBindedDaysJSON()
    {
        $from = $this->getFirstAvailableDate();
        $to = clone $from;
        $to->addMonth(Mage::getStoreConfig(AW_Booking_Helper_Config::XML_PATH_APPEARANCE_CALENDAR_PAGES));
        $dates = Mage::getModel('booking/checker_bind')->getUnavailDays($this->getProduct(), $from, $to);
        return Zend_Json::encode($dates);
    }

    /**
     * Returns first day in current month
     * @return Zend_Date
     */
    public function getFirstAvailableDate()
    {
        $zDate = $this->getProduct()->getTypeInstance()->getFirstAvailableDate();
        return $zDate;
    }
}