<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Catalog_Product_View_Timetable extends Mage_Core_Block_Template
{
    protected $_product = null;
    protected $_date = false;
    protected $_binds;

    /**
     * Retrieves current product
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        if (null === $this->_product) {
            $this->_product = Mage::registry('current_product');
            if (null === $this->_product) {
                $this->_product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
            }
        }
        return $this->_product;
    }

    /**
     * Sets date
     * @param Zend_Date $date
     * @return AW_Booking_Block_Order_Hours
     */
    public function setDate(Zend_Date $date)
    {
        $this->_date = $date;
        return $this;
    }

    /**
     * Returns date
     * @return Zend_Date
     */
    public function getDate()
    {
        return $this->_date;
    }

    /**
     * Returns HTML with dates
     * @param Zend_Date $date
     * @return string
     */
    public function getFromToHtml(Zend_Date $date)
    {
        /**
         * @var $timeChecker AW_Booking_Model_Checker_Time
         */
        $timeChecker = Mage::getModel('booking/checker_time')->init($this->getProduct(), $date);
        $hour = $timeChecker->getFirstHour();
        $from = clone $hour;
        $to = clone $from;
        $to->addMinute(30);

        $out = '<table class="aw_booking_timetable">';
        $out .= '<thead><tr>';
        do {
            $out .= '<td colspan="2">' . $this->_getHourTitle($hour) . '</td>';
            $hour->addHour(1);
        } while (!$timeChecker->isPastLastHour($hour));
        $out .= '</tr></thead>';

        $out .= '<tbody><tr>';
        $primary = true;
        do {
            $out .= '<td class="';

            if ($timeChecker->isFirstHour($from)) {
                $out .= 'first';
            } elseif ($timeChecker->isLastHour($from)) {
                $out .= 'last';
            } else {
                $out .= 'common';
            }
            $out .= '-';

            if ($primary) {
                $out .= 'prim';
            } else {
                $out .= 'sec';
            }
            $out .= ' ';

            if ($timeChecker->isAvailable($from)) {
                if (!Mage::getModel('booking/checker_bind')->isQtyAvailableForPeriod($this->getProduct(), $from, $to)) {
                    $out .= 'busy';
                } else {
                    $out .= 'free';
                }
            } else {
                $out .= 'out';
            }
            $out .= '"></td>';

            $from->addMinute(30);
            $to->addMinute(30);
            $primary = !$primary;
        } while (!$timeChecker->isPastLastHour($from));
        $out .= '</tr></tbody></table>';
        return $out;
    }

    private function _getHourTitle(Zend_Date $date)
    {
        if (Mage::getSingleton('catalog/product_option_type_date')->is24hTimeFormat()) {
            // 24H format used 00-23
            return $date->get(Zend_Date::HOUR_SHORT);
        } else {
            // 12H format used
            return
                $date->get(Zend_Date::HOUR_SHORT_AM)
                .' '
                .strtoupper($date->get(Zend_Date::MERIDIEM));
        }
    }
}
