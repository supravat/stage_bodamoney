<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Booking
 * @version    1.4.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Booking_Block_Catalog_Product_Options_Date extends Mage_Core_Block_Template
{
    const ID_PREFIX = 'id-';

    /** Type identity hours */
    const TYPE_HOURS = 'hours';
    /** Type identity minutes */
    const TYPE_MINUTES = 'minutes';
    /** Type identity daypart */
    const TYPE_DAYPART = 'daypart';

    /** Type identity AM hours */
    const TYPE_HOURS_AM = 'hours-am';
    /** Type identity PM hours */
    const TYPE_HOURS_PM = 'hours-pm';

    public $hourStart = 0;
    public $hourEnd = 23;
    public $_hours;
    public $_minutes;
    public $_daypart = null;

    /**
     * Return whole element HTML
     *
     * @return string
     */
    public function getElementHtml()
    {
        $this->setClass('select');
        $value_min = 0;
        if (!$this->is24hTimeFormat() && $this->_isAccessibleInAM() && $this->_isAccessibleInPM()) {
            // AM && PM
            $blockAm = clone $this;
            $blockPm = clone $this;
            $html = $blockAm->getHoursHtml($this->hourStart, $this->hourEnd, $this->getHours(), self::TYPE_HOURS_AM, false, 'am');
            $html .= $blockPm->getHoursHtml($this->hourStart, $this->hourEnd, $this->getHours(), self::TYPE_HOURS_PM, false, 'pm');
            if ($this->getDayPart() != 'pm') {
                $html .= $blockAm->getHoursHtml($this->hourStart, $this->hourEnd, $this->getHours(), '', true, 'am');
            } else {
                $html .= $blockPm->getHoursHtml($this->hourStart, $this->hourEnd, $this->getHours(), '', true, 'pm');
            }
        } else {
            $html = $this->getHoursHtml($this->hourStart, $this->hourEnd, $this->getHours());
        }

        $html .= '&nbsp;<b>:</b>&nbsp;<select id="' . $this->getId('minutes')
            . '" name="'. $this->getName(self::TYPE_MINUTES)
            . '" '.$this->serialize($this->getHtmlAttributes())
            .">\n"
        ;
        for ($i = 0; $i < 60; $i++) {
            $hour = str_pad($i, 2, '0', STR_PAD_LEFT);
            $html.= '<option value="' . $hour . '" ' . (($value_min == $i) ? 'selected="selected"' : '' ) . '>'
                . $hour
                . '</option>'
            ;
        }
        $html .= '</select>' . "\n";
        $html .= $this->getDayPartHtml($this->getDayPart());
        $html = '<div class="time-selector">' . $html . '</div>';
        $html .= $this->getAfterElementHtml();
        $html .='<script type="text/javascript">' . $this->getHelperJs() . "</script>";
        return $html;
    }

    /**
     * Return HTML part with hours selector
     * @param int $start
     * @param int $end
     * @param int $value_hrs
     * @param string $id
     * @param bool $visible
     * @return string
     * @todo: After fixing a problem with time range $start > $end, all calls to this function passes the same $start, $end, $value_hrs parameters. Consider removing them from the parameters list and updating the function code.
     */
    public function getHoursHtml($start, $end, $value_hrs = 0, $id = '', $visible = true, $dayPart = null)
    {
        $html = '<select id="' . $this->getId($id ? $id : self::TYPE_HOURS)
            . '" name="' . $this->getName($id ? $id : self::TYPE_HOURS)
            . '" ' . $this->serialize($this->getHtmlAttributes())
            . ' style="' . ($visible ? "" : "display:none;") . '">' . "\n"
        ;
        if ($end > 0 && $start >= $end) {
            $end += 24;
        }
        for ($i = (int)$start; $i <= $end; $i++) {
            $z = $i;
            if ($z >= 24) {
                $z -= 24;
            }
            if (
                (
                    $dayPart == 'am'
                    && $z >= 12
                ) || (
                    $dayPart == 'pm'
                    && $z < 12
                )
            ) {
                continue;
            }
            $hour = $z;
            if (!$this->is24hTimeFormat()) {
                if ($z == 0) {
                    $hour = 12;
                } elseif ($z > 12) {
                    $hour -= 12;
                }
            }
            $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
            $html .= '<option value="' . $hour . '" ' . (($value_hrs == $z) ? 'selected="selected"' : '')
                . '>' . $hour . '</option>'
            ;
        }
        $html .= '</select>' . "\n";
        $html.= "<script>
            Event.observe('". $this->getId($id ? $id : self::TYPE_HOURS) ."',  'change', function(){
                var name = '" . $this->getName() . "';
                if( name == 'aw_booking_time_to' ) {
                    checkTimeSelector" . $this->getName() . "(this.value, true);
                } else checkTimeSelector" . $this->getName() . "(this.value, false);

            });

            Event.observe(window, 'load', function(){
                var name = '" . $this->getName() . "';
                if( name == 'aw_booking_time_to' ) {
                    checkTimeSelector" . $this->getName() . "(" . $this->getHours() . ", true);
                } else checkTimeSelector" . $this->getName() . "(" . $this->getHours() . ", false);
            });

            checkTimeSelector" . $this->getName() . " = function(value, recursive) {
                var minutesSelector = '" . $this->getId('minutes') . "';
                var hoursSelector = '" . $this->getId($id ? $id : self::TYPE_HOURS) . "';
                var Hour = '" . $this->getHours() . "';
                var Minutes = '" . $this->getMinutes() . "';
                if( Hour == value ) {
                    var options = $$('select#' + minutesSelector + ' option');
                    var len = options.length;
                    for (var i = 0; i < len; i++) {
                        if(recursive) {
                            if( options[i].value > parseInt(Minutes) ) {
                                options[i].hide();
                            }
                            if( options[i].value == Minutes ){
                                options[i].selected = true;
                            }
                        }else{

                            if( options[i].value < parseInt(Minutes) ) {
                                options[i].hide();
                            }
                            if( options[i].value == Minutes ){
                                options[i].selected = true;
                            }
                        }
                    }

                }else{
                    var options = $$('select#' + minutesSelector + ' option');
                    var len = options.length;
                    for (var i = 0; i < len; i++) {
                        options[i].show();
                    }
                }
            }
            </script>"
        ;
        return $html;
    }

    /**
     * Return HTML part with date part selector (AM/PM)
     * @param string $value_daypart
     * @return string
     */
    public function getDayPartHtml($value_daypart = 'am')
    {
        $html = '';
        if(!$this->is24hTimeFormat()){
            $html = '<span class="daypart">' . ($value_daypart == 'am' ? $this->__('AM') : $this->__('PM'))
                . '</span><input type="hidden" id="' . $this->getId('daypart')
                . '" name="' . $this->getName(self::TYPE_DAYPART) . '" value="'
                . $value_daypart .'" />'
            ;
            if(!$this->_isOnlyAMOrPmAccessible()){
                $html = '&nbsp; &nbsp;<select id="' . $this->getId('daypart')
                    . '" name="'. $this->getName(self::TYPE_DAYPART)
                    . '" ' .$this->serialize($this->getHtmlAttributes())
                    . ' onchange="TimeInput'
                    . $this->getId() . '.dateSwitchHours(this)">' . "\n"
                    . '<option value="am" ' . (($value_daypart == 'am') ? 'selected="selected"' : '')
                    . '>' . $this->__('AM') . '</option>'
                    . '<option value="pm" ' . (($value_daypart == 'pm') ? 'selected="selected"' : '') . '>'
                    . $this->__('PM') . '</option></select>' . "\n"
                ;
            }
        }
        return $html;
    }

    /**
     * Set time received via array [h,m] or string "h,m"
     * @param mixed $time
     * @return object
     */
    public function setTime($time)
    {
        if (is_string($time)) {
            list($h, $m) = explode(",", $time);
        } elseif (is_array($time)) {
            $h = $time[0];
            $m = $time[1];
        }
        if (isset($h)) {
            $this->setHours($h)->setMinutes($m);
            if (!$this->is24hTimeFormat()) {
                $this->setDayPart('am');
                if ((int)$h > 11) {
                    $this->setDayPart('pm');
                }
            }
        }
        return $this;
    }

    public function setHours($h)
    {
        // Sets hours value
        $this->setData('hours', $h >= 24 ? 23 : ($h < 0 ? 0 : $h));
        return $this;
    }

    public function setMinutes($h)
    {
        // Sets minutes value
        $this->setData('minutes', $h >= 60 ? 59 : ($h < 0 ? 0 : $h));
        return $this;
    }

    public function setHourStart($t)
    {
        $this->hourStart = (int)$t;
        return $this;
    }

    public function setHourEnd($t)
    {
        $this->hourEnd = (int)$t;
        return $this;
    }

    public function getId($postfix = '')
    {
        $id = $this->getData('id');
        if ($postfix) {
            $id .= '-' . $postfix;
        }
        return $id;
    }

    /**
     * If AM part is accessible
     * @return bool
     */
    protected function _isAccessibleInAM()
    {
        return (
            $this->hourStart < 12
            || $this->hourEnd < 12
            || $this->hourStart > $this->hourEnd
        );
    }

    /**
     * If PM part is accessible
     * @return bool
     */
    protected function _isAccessibleInPM()
    {
        return (
            $this->hourStart > 12
            || $this->hourEnd > 12
            || $this->hourStart > $this->hourEnd
        );
    }

    /**
     * Indicate if only AM or only PM is accessible
     * @return bool
     */
    protected function _isOnlyAMOrPmAccessible()
    {
        return !($this->_isAccessibleInAM() && $this->_isAccessibleInPM());
    }

    /**
     * Detect if 24 hour format used for output
     * @return bool
     */
    public function is24hTimeFormat()
    {
        if (null === $this->getIs24hTimeFormat()) {
            return Mage::getSingleton('catalog/product_option_type_date')->is24hTimeFormat();
        }
        return (bool)$this->getIs24hTimeFormat();
    }

    /**
     * Return helping JS for switching between AM/PM and also get values methods
     * @return string
     */
    public function getHelperJs()
    {
        return "
            {$this->getJsObjectName()} = {
                dateSwitchHours : function(el){
                    var isPm = el.selectedIndex;
                    var toShow = isPm ? '{$this->getId(self::TYPE_HOURS_PM)}' : '{$this->getId(self::TYPE_HOURS_AM)}';
                    $('{$this->getId(self::TYPE_HOURS)}').options.length = 0;
                    \$A($(toShow).options).each(function(op){
                        var newOp = new Option(op.text, op.value);
                        $('{$this->getId(self::TYPE_HOURS)}')[$('{$this->getId(self::TYPE_HOURS)}').options.length] = (newOp);
                    })
                },
                getMinutes : function(){
                    var hours = {$this->getJsObjectName()}.getAbsoluteHours();
                    return parseInt(hours*60 + \$F('{$this->getId(self::TYPE_MINUTES)}'));
                },
                getAbsoluteHours : function(){
                   var hours = 1*\$F($('{$this->getId(self::TYPE_HOURS)}'))
                    if($('{$this->getId(self::TYPE_DAYPART)}')){
                        if(hours == 12){
                            hours = 0;
                        }
                        if(\$F($('{$this->getId(self::TYPE_DAYPART)}')) == 'pm'){
                            hours += 12;
                        }
                    }
                    return hours;
                },
                getValueString : function(){
                    return '' + (100+this.getAbsoluteHours()+'').substr(1) + ':' + \$F('{$this->getId(self::TYPE_MINUTES)}') + ':00';
                }
            }
        ";
    }

    /**
     * Return name for JS Helper object
     * @return string
     */
    public function getJsObjectName()
    {
        return "TimeInput{$this->getId()}";
    }

    /**
     * Return complete name
     * @param null $part
     *
     * @return string
     */
    public function getName($part = null)
    {
        switch ($part) {
            case self::TYPE_HOURS:
            case self::TYPE_MINUTES:
            case self::TYPE_DAYPART:
            case self::TYPE_HOURS_AM:
            case self::TYPE_HOURS_PM:
                return $this->getData('name') . "[" . $part . "]";
            default:
                return $this->getData('name');
        }
    }
}
