<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Helper_Mailer
{
    /**
     * @param array $orderItemIds
     * @param string $subject
     * @param string $body
     * @return AW_Eventbooking_Helper_Mailer
     */
    public function sendMessageToAttendees($orderItemIds, $subject, $body)
    {
        $orderHistoryCollection = Mage::getModel('aw_eventbooking/order_history')->getCollection();
        $orderHistoryCollection->addFilterOnOrderItemIds($orderItemIds);
        $orderItemCollection = $orderHistoryCollection->getRelatedOrderItemCollection();
        $emailDataList = array();
        foreach ($orderItemCollection as $item) {
            $order = $item->getOrder();
            if (is_null($order)) {
                continue;
            }
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name', $item->getStoreId());
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email', $item->getStoreId());
            $emailDataList[$item->getId()] = array(
                'customer_name' => Mage::helper('aw_eventbooking/order')->getCustomerNameFromOrder($order),
                'customer_email' => $order->getCustomerEmail(),
                'sender_name' => $senderName,
                'sender_email' => $senderEmail,
            );
        }

        //removing duplicate customer_email
        $customerEmailList = array();
        foreach ($emailDataList as $key => $item) {
            $customerEmailList[$key] = $item['customer_email'];
        }
        $customerEmailList = array_unique($customerEmailList);
        $emailDataList = array_intersect_key($emailDataList, $customerEmailList);

        foreach ($emailDataList as $emailData) {
            $mail = new Zend_Mail('utf-8');
            $mail
                ->setFrom($emailData['sender_email'], $emailData['sender_name'])
                ->addTo($emailData['customer_email'], $emailData['customer_name'])
                ->setSubject($subject)
                ->setBodyHtml($body);
            try {
                $mail->send();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @return array
     */
    protected function _getPDFAttachments(Mage_Sales_Model_Order_Item $orderItem)
    {
        /** @var AW_Eventbooking_Model_Resource_Ticket_Collection $ticketsCollection */
        $ticketsCollection = Mage::getModel('aw_eventbooking/ticket')->getCollection()
            ->addFieldToFilter('order_item_id', array('eq' => $orderItem->getId()))
            ->addPaymentStatusFilter();

        /** @var AW_Eventbooking_Helper_Ticket $ticketHelper */
        $ticketHelper = Mage::helper('aw_eventbooking/ticket');
        $attachments = array();
        foreach ($ticketsCollection as $ticket) {
            $name = strtoupper(str_replace(' ', '_', $ticket->getProduct()->getName()));
            $name .= '_';
            $name .= substr(md5($ticket->getData('code')), 0, 8);
            $attachments[$name] = $ticketHelper->renderTicketPdf($ticket);
        }
        return $attachments;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @throws Exception
     * @return AW_Eventbooking_Helper_Mailer
     */
    public function sendConfirmationEmailForOrderItem(Mage_Sales_Model_Order_Item $orderItem)
    {
        $eventTicket = Mage::helper('aw_eventbooking/order')->getEventTicketFromOrderItem($orderItem);
        if (is_null($eventTicket)) {
            throw new Exception('Ticket does not exist');
        }

        $templateId = $eventTicket->getConfirmationTemplateId();
        if (intval($templateId) === AW_Eventbooking_Model_Source_Email_Template::PLEASE_CHOOSE_CODE) {
            return $this;
        }

        $storeId = $orderItem->getStoreId();
        $sender = Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_IDENTITY, $storeId);
        $order = $orderItem->getOrder();
        $event = $eventTicket->getEventModel();
        $eventDate = Mage::helper('core')->formatDate(
            $event->getData('event_start_date'), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true
        );
        $eventEndDate = $event->getData('event_end_date')
            ? Mage::helper('core')->formatDate($event->getData('event_end_date'), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true)
            : null;
        /** @var AW_Eventbooking_Helper_Order $orderHelper */
        $orderHelper = Mage::helper('aw_eventbooking/order');
        $customerName = $orderHelper->getCustomerNameFromOrder($order);
        $customerFirstName = $orderHelper->getCustomerFirstNameFromOrder($order);

        /** @var AW_Rma_Model_Email_Template $emailTemplate */
        $emailTemplate = Mage::getModel('aw_eventbooking/email_template');
        if ($event->getData('generate_pdf_tickets')) {
            foreach ($this->_getPDFAttachments($orderItem) as $code => $PDFContent) {
                $emailTemplate->addAttachment($code . '.pdf', $PDFContent);
            }
            unset($code, $PDFContent);
        }
        $orderItem->setQtyOrdered((int)$orderItem->getQtyInvoiced());
        $emailTemplate
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
            ->sendTransactional(
                $templateId,
                $sender,
                $order->getCustomerEmail(),
                $customerName,
                array(
                    'order' => $order,
                    'order_item' => $orderItem,
                    'event_ticket' => $eventTicket,
                    'event' => $event,
                    'event_date' => $eventDate, // Backward compatibility
                    'event_start_date' => $eventDate,
                    'event_end_date' => $eventEndDate,
                    'customer_name' => $customerFirstName,
                )
            );
        return $this;
    }

    /**
     * @param AW_Eventbooking_Model_Event $event
     * @return AW_Eventbooking_Helper_Mailer
     */
    public function sendReminderEmailForEvent(AW_Eventbooking_Model_Event $event)
    {
        $ticketCollection = $event->getTicketCollection();
        /**@var AW_Eventbooking_Model_Resource_Order_History_Collection $orderItemCollection */
        $orderItemCollection = Mage::getModel('aw_eventbooking/order_history')->getCollection()
            ->addFilterOnEventTicketCollection($ticketCollection)
            ->getRelatedOrderItemCollection()
            ->addFieldToFilter('qty_invoiced', array('gt' => new Zend_Db_Expr("qty_refunded")));

        $emailDataList = array();
        foreach ($orderItemCollection as $orderItem) {
            $order = $orderItem->getOrder();
            if (is_null($order)) {
                continue;
            }
            $eventModel = Mage::getModel('aw_eventbooking/event')
                ->setStoreId($orderItem->getStoreId())
                ->load($event->getId());
            $eventTicket = Mage::helper('aw_eventbooking/order')->getEventTicketFromOrderItem($orderItem);
            $eventDate = Mage::helper('core')->formatDate(
                $event->getData('event_start_date'), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true
            );
            $eventEndDate = $event->getData('event_end_date')
                ? Mage::helper('core')->formatDate($event->getData('event_end_date'), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true)
                : null;
            /** @var AW_Eventbooking_Helper_Order $orderHelper */
            $orderHelper = Mage::helper('aw_eventbooking/order');
            $customerName = $orderHelper->getCustomerNameFromOrder($order);
            $customerFirstName = $orderHelper->getCustomerFirstNameFromOrder($order);

            $templateId = $eventModel->getReminderTemplateId();

            if (intval($templateId) === AW_Eventbooking_Model_Source_Email_Template::PLEASE_CHOOSE_CODE) {
                continue;
            }
            $emailDataList[$orderItem->getId()] = array(
                'store_id' => $orderItem->getStoreId(),
                'customer_name' => $customerName,
                'customer_email' => $order->getCustomerEmail(),
                'sender' => 'general',
                'template_id' => $templateId,
                'vars' => array(
                    'order' => $order,
                    'order_item' => $orderItem,
                    'event_ticket' => $eventTicket,
                    'event' => $eventModel,
                    'event_date' => $eventDate, // Backward compatibility
                    'event_start_date' => $eventDate,
                    'event_end_date' => $eventEndDate,
                    'customer_name' => $customerFirstName,
                )
            );
        }

        //send emails
        foreach ($emailDataList as $emailData) {
            $emailTemplateModel = Mage::getModel('core/email_template')
                ->setDesignConfig(array('area' => 'frontend', 'store' => $emailData['store_id']));
            try {
                $emailTemplateModel
                    ->sendTransactional(
                        $emailData['template_id'],
                        $emailData['sender'],
                        $emailData['customer_email'],
                        $emailData['customer_name'],
                        $emailData['vars']
                    );
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @throws Exception
     * @return AW_Eventbooking_Helper_Mailer
     */
    public function sendReminderEmailForOrderItem(Mage_Sales_Model_Order_Item $orderItem)
    {
        $eventTicket = Mage::helper('aw_eventbooking/order')->getEventTicketFromOrderItem($orderItem);
        if (is_null($eventTicket)) {
            throw new Exception('Ticket does not exist');
        }

        $event = $eventTicket->getEventModel();
        $templateId = $event->getReminderTemplateId();
        if (intval($templateId) === AW_Eventbooking_Model_Source_Email_Template::PLEASE_CHOOSE_CODE) {
            return $this;
        }

        $storeId = $orderItem->getStoreId();
        $sender = 'general';
        $order = $orderItem->getOrder();
        $eventDate = Mage::helper('core')->formatDate(
            $event->getData('event_start_date'), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true
        );
        $eventEndDate = $event->getData('event_end_date')
            ? Mage::helper('core')->formatDate($event->getData('event_end_date'), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true)
            : null;
        /** @var AW_Eventbooking_Helper_Order $orderHelper */
        $orderHelper = Mage::helper('aw_eventbooking/order');
        $customerName = $orderHelper->getCustomerNameFromOrder($order);
        $customerFirstName = $orderHelper->getCustomerFirstNameFromOrder($order);

        Mage::getModel('core/email_template')
            ->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
            ->sendTransactional(
                $templateId,
                $sender,
                $order->getCustomerEmail(),
                $customerName,
                array(
                    'order' => $order,
                    'order_item' => $orderItem,
                    'event_ticket' => $eventTicket,
                    'event' => $event,
                    'event_date' => $eventDate, // Backward compatibility
                    'event_start_date' => $eventDate,
                    'event_end_date' => $eventEndDate,
                    'customer_name' => $customerFirstName,
                )
            );
        return $this;
    }
}
