<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Helper_Quote
{
    /**
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return array
     */
    public function getAllEventbookingItemsFromQuote(Mage_Sales_Model_Quote $quote)
    {
        $eventItems = array();
        if (is_null($quote)) {
            return $eventItems;
        }
        foreach ($quote->getItemsCollection() as $item) {
            if ($this->isQuoteItemIsEventProduct($item)) {
                $eventItems[] = $item;
            }
        }
        return $eventItems;
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     *
     * @return bool
     */
    public function isQuoteItemIsEventProduct(Mage_Sales_Model_Quote_Item $item)
    {
        $event = Mage::getModel('aw_eventbooking/event')
            ->loadByProductId($item->getProduct()->getId())
        ;
        return !is_null($event->getId());
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     *
     * @return AW_Eventbooking_Model_Event_Ticket|null
     */
    public function getEventTicketFromQuoteItem(Mage_Sales_Model_Quote_Item $item)
    {
        $optionList = $item->getOptionsByCode();
        $eventOptionKey = 'option_' . AW_Eventbooking_Model_Event::PRODUCT_OPTION_ID;
        if (array_key_exists($eventOptionKey, $optionList)) {
            $eventTicketId = intval($optionList[$eventOptionKey]->getValue());
            $eventTicketModel = Mage::getModel('aw_eventbooking/event_ticket')->load($eventTicketId);
            if (is_null($eventTicketModel->getId())) {
                return null;
            }
            return $eventTicketModel;
        }
        return null;
    }
}