<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Helper_Order
{
    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function getAllEventbookingItemsFromOrder(Mage_Sales_Model_Order $order)
    {
        $eventItems = array();
        if (is_null($order)) {
            return $eventItems;
        }
        foreach ($order->getAllItems() as $item) {
            if ($this->isOrderItemIsEventProduct($item)) {
                $eventItems[] = $item;
            }
        }
        return $eventItems;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     * @return bool
     */
    public function isOrderItemIsEventProduct(Mage_Sales_Model_Order_Item $item)
    {
        $eventTicketId = $this->getEventTicketIdFromOrderItem($item);
        if (is_null($eventTicketId)) {
            return false;
        }
        return true;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     *
     * @return int|null
     */
    public function getEventTicketIdFromOrderItem(Mage_Sales_Model_Order_Item $item)
    {
        $options = $item->getProductOptionByCode('options');
        if (!is_array($options)) {
            return null;
        }
        foreach ($options as $option) {
            if ($option['option_id'] === AW_Eventbooking_Model_Event::PRODUCT_OPTION_ID) {
                return intval($option['option_value']);
            }
        }
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     *
     * @return AW_Eventbooking_Model_Event_Ticket|null
     */
    public function getEventTicketFromOrderItem(Mage_Sales_Model_Order_Item $item)
    {
        $eventTicketId = $this->getEventTicketIdFromOrderItem($item);
        $eventTicketModel = Mage::getModel('aw_eventbooking/event_ticket')
            ->setStoreId($item->getStoreId())
            ->load($eventTicketId);
        if (is_null($eventTicketModel->getId())) {
            return null;
        }
        return $eventTicketModel;
    }


    /**
     * @param Mage_Sales_Model_Mysql4_Order_Item_Collection $collection
     * @return Mage_Sales_Model_Mysql4_Order_Item_Collection
     */
    public function addOrderInfoToOrderItemCollection($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                array('order_table' => $collection->getTable('sales/order')),
                "main_table.order_id = order_table.entity_id",
                array(
                    'order_increment_id' => "order_table.increment_id",
                    'order_customer_id' => "order_table.customer_id",
                    'order_customer_email' => "order_table.customer_email",
                    'order_customer_name' => "CONCAT(order_table.customer_firstname, ' ', order_table.customer_lastname)",
                )
            );
        return $collection;
    }

    /**
     * @param Mage_Sales_Model_Mysql4_Order_Item_Collection $collection
     * @return Mage_Sales_Model_Mysql4_Order_Item_Collection
     */
    public function addEventTicketInfoToOrderItemCollection($collection)
    {
        $collection->getSelect()
            ->joinLeft(
                array('event_order_history_table' => $collection->getTable('aw_eventbooking/order_history')),
                "main_table.item_id = event_order_history_table.order_item_id",
                array()
            )
            ->joinLeft(
                array('event_ticket_table' => $collection->getTable('aw_eventbooking/event_ticket')),
                "event_order_history_table.event_ticket_id = event_ticket_table.entity_id",
                array()
            );

        //add event ticket data
        $storeId = Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
        foreach (Mage::helper('aw_eventbooking/attribute')->getTicketAttributes() as $attributeCode) {
            $defaultStoreAlias = 'event_ticket_attribute_' . $attributeCode . '_ds';
            $currentStoreAlias = 'event_ticket_attribute_' . $attributeCode . '_cs';
            $collection->getSelect()
                ->joinLeft(
                    array($defaultStoreAlias => $collection->getTable('aw_eventbooking/event_ticket_attribute')),
                    "event_ticket_table.entity_id = {$defaultStoreAlias}.ticket_id AND
                    {$defaultStoreAlias}.attribute_code = '{$attributeCode}' AND
                    {$defaultStoreAlias}.store_id = " . (int)Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID,
                    array()
                )
                ->joinLeft(
                    array($currentStoreAlias => $collection->getTable('aw_eventbooking/event_ticket_attribute')),
                    "event_ticket_table.entity_id = {$currentStoreAlias}.ticket_id
                    AND {$currentStoreAlias}.attribute_code = '{$attributeCode}'
                    AND {$currentStoreAlias}.attribute_code = {$defaultStoreAlias}.attribute_code
                    AND {$currentStoreAlias}.store_id = {$storeId}",
                    array('event_ticket_' . $attributeCode => "IFNULL({$currentStoreAlias}.value, {$defaultStoreAlias}.value)")
                );
        }
        return $collection;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    public function getCustomerNameFromOrder(Mage_Sales_Model_Order $order)
    {
        if (!$order->getId()) {
            return '';
        }
        if ($order->getData('customer_is_guest')) {
            return $order->getBillingAddress()->getName();
        } else {
            return $order->getCustomerName();
        }
    }

    public function getCustomerFirstNameFromOrder(Mage_Sales_Model_Order $order)
    {
        if ($order->getCustomerIsGuest()) {
            return $order->getBillingAddress()->getFirstname();
        } else {
            return $order->getCustomerFirstname();
        }
    }
}
