<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Helper_Config
{
    const EXTENSION_KEY = 'aw_eventbooking';

    const QR_WIDTH = 'qr/width';
    const TEMPLATES_CONFIRMATION = 'templates/confirmation';
    const TEMPLATES_REMINDER = 'templates/reminder';

    public function getConfig($key, $store = null)
    {
        return Mage::getStoreConfig(self::EXTENSION_KEY . '/' . $key, $store);
    }

    public function getQRWidth($store = null)
    {
        return $this->getConfig(self::QR_WIDTH, $store);
    }

    public function getTemplatesConfirmation($store = null)
    {
        return $this->getConfig(self::TEMPLATES_CONFIRMATION, $store);
    }

    public function getTemplatesReminder($store = null)
    {
        return $this->getConfig(self::TEMPLATES_REMINDER, $store);
    }

    public function setConfig($key, $value)
    {
        /** @var Mage_Core_Model_Config $configModel */
        $configModel = Mage::getModel('core/config');
        $configModel->saveConfig(self::EXTENSION_KEY . '/' . $key, $value);
        return $this;
    }
}
