<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Helper_Data extends Mage_Core_Helper_Data
{
    public function isCustomSMTPInstalled()
    {
        return $this->isModuleOutputEnabled('AW_Customsmtp') && @class_exists('AW_Customsmtp_Model_Email_Template');
    }

    public function isEnabled()
    {
        return $this->isModuleOutputEnabled();
    }

    /**
     * @return array
     */
    public function getCurrentAdminRoleIds()
    {
        /** @var Mage_Admin_Model_Session $adminSession */
        $adminSession = Mage::getSingleton('admin/session');
        return $adminSession->isLoggedIn()
            ? $adminSession->getUser()->getRoles()
            : array();
    }

    /**
     * @return Mage_Core_Model_Cache|null
     */
    public function getFPCInstance()
    {
        if ($this->isModuleOutputEnabled('Enterprise_PageCache')) {
            return Enterprise_PageCache_Model_Cache::getCacheInstance();
        }
    }
}
