<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Adminhtml_AbstractController extends Mage_Adminhtml_Controller_Action
{
    protected function _setTitle($title)
    {
        foreach ($title as $titlePart) {
            $this->_title($titlePart);
        }
        return $this;
    }

    protected function _initAction($title = null)
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/aw_eventbooking');
        if ($title) {
            $this->_setTitle(is_array($title) ? $title : array($title));
        }

        return $this;
    }
}
