<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_ExtController extends Mage_Core_Controller_Front_Action
{

    public function viewAction()
    {
        if ($ticket = Mage::helper('aw_eventbooking/ticket')->getTicketByExternalRequest()) {
            $session = Mage::getSingleton('customer/session');

            if (Mage::app()->getRequest()->getParam('p')) {
                $session->addError($this->__('Sorry, you are not allowed to redeem tickets for this event.'));
            }

            if ($session->isLoggedIn() && $session->getCustomerId() == $ticket->getData('customer_id')) {
                return $this->_redirect('aw_eventbooking/ticket/view', array('_secure' => true, 'id' => $ticket->getId()));
            }

            $customer = Mage::getSingleton('customer/customer')->load($ticket->getData('customer_id'));

            $this->loadLayout();
            $layout = $this->getLayout();
            $block = $layout->getBlock('ticket.view');
            $messages = $session->getMessages(true);
            $layout->getMessagesBlock()->setMessages($messages);

            $block->setData('ticket', $ticket);
            $block->setData('customer', $customer);
            $block->setData('FullInfo', false);
            $this->renderLayout();


        } else {
            /* Bad ticket info */
            $this->_redirect('');
        }
    }

}
