<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Block_Ticket_List extends Mage_Core_Block_Template
{

    public function getCollection()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        /** @var AW_Eventbooking_Model_Resource_Ticket_Collection $collection */
        $collection = Mage::getModel('aw_eventbooking/ticket')->getCollection();
        $collection
            ->joinEventData()
            ->joinOrderData();
        $collection->addFieldToFilter('customer_id', $customer->getId());
        return $collection;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $pager = $this->getLayout()->createBlock('page/html_pager', 'aw_eventbooking.ticket.view.pager')
            ->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        return $this;
    }


    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getProductUrl($id)
    {
        /** @var Mage_Catalog_Model_Product $productModel */
        $productModel = Mage::getModel('catalog/product')->load($id);
        return $productModel->getProductUrl();
    }

    public function getViewUrl($id)
    {
        return Mage::getUrl('aw_eventbooking/ticket/view', array('_secure'=>true,'id' => $id));
    }

    protected function _processDateTime($dateTime)
    {
        // Convert date from UTC to Store Locale
        return Mage::app()->getLocale()
            ->date($dateTime, Varien_Date::DATETIME_INTERNAL_FORMAT);
    }

    public function isToday($item)
    {
        $startDate = $this->_processDateTime($item->getData('event_start_date'));
        $now = Mage::app()->getLocale()->storeDate(null, null, true);
        if ($itemEndDate = $item->getData('event_end_date')) {
            $endDate = $this->_processDateTime($itemEndDate);
            if ($now->isLater($startDate) && $now->isEarlier($endDate)) {
                /* Event happening right now */
                return true;
            }
        }

        $startDate->setTime(0);
        $now->setTime(0);
        return $now->equals($startDate);
    }

    protected function _getFormattedTicketPrice(AW_Eventbooking_Model_Ticket $ticket)
    {
        $orderItem = $ticket->getOrderItem();
        $price = $orderItem->getRowTotal() / $orderItem->getQtyOrdered();
        return $ticket->getStore()->formatPrice($price);
    }
}
