<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Block_Ticket_View extends Mage_Core_Block_Template
{

    public function getOrderUrl($id){
        return Mage::getUrl('sales/order/view',array('_secure'=>true,'order_id'=>$id));
    }
    public function getBackUrl()
    {
        return Mage::getUrl('aw_eventbooking/ticket/index',array('_secure'=>true));
    }
    public function getConfirmationUrl($id)
    {
        return Mage::getUrl('aw_eventbooking/ticket/resendConfirmation',array('_secure'=>true,'id'=>$id));
    }
    public function getProductOptions($ticket)
    {
        $orderItem = $ticket->getOrderItem();
        /** @var AW_Eventbooking_Helper_Ticket $ticketHelper */
        $ticketHelper = Mage::helper('aw_eventbooking/ticket');
        return $ticketHelper->getOrderItemOptions($orderItem);
    }

}