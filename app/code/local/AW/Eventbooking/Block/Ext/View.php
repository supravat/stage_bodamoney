<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Block_Ext_View extends Mage_Adminhtml_Block_Template
{

    public function getOrderUrl($id)
    {
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $id));
    }

    public function getUndoUrl($code, $hash)
    {
        return $this->getUrl('*/*/undoredeem', array('code' => $code, 'hash' => $hash));
    }

    public function getActionUrl()
    {
        return $this->getUrl('*/*/view');
    }
}