<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Block_Adminhtml_Catalog_Product_Edit_Tab_Eventbooking_Attendees_Column_Renderer_Customer
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $customerId = $row->getData('order_customer_id');
        if (!$customerId) {
            $value = $this->_getValue($row);
            if (strlen(trim($value)) < 1) {
                $value = Mage::helper('sales')->__('Guest');
            }
            return $value;
        }
        $url = $this->getUrl('*/customer/edit', array('id' => $row->getData('order_customer_id')));
        return sprintf('<a href="%s">%s</a>', $url, $this->_getValue($row));
    }

    public function renderExport(Varien_Object $row)
    {
        return $this->_getValue($row);
    }
}