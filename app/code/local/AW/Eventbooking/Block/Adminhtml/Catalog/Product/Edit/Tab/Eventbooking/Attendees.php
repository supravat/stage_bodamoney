<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Eventbooking_Block_Adminhtml_Catalog_Product_Edit_Tab_Eventbooking_Attendees
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('aw_eventbooking_attendees_grid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection()
    {
        $product = Mage::registry('current_product');
        /* @var $event AW_Eventbooking_Model_Event */
        $event = Mage::getModel('aw_eventbooking/event')
            ->loadByProductId($product->getId())
        ;
        $ticketCollection = Mage::getResourceModel('aw_eventbooking/event_ticket_collection')
            ->addFilterOnEventId($event->getId())
        ;
        $orderHistoryCollection = Mage::getModel('aw_eventbooking/order_history')->getCollection()
            ->addFilterOnEventTicketCollection($ticketCollection)
        ;
        /* @var Mage_Sales_Model_Mysql4_Order_Item_Collection */
        $collection = $orderHistoryCollection->getRelatedOrderItemCollection();
        $collection = Mage::helper('aw_eventbooking/order')->addOrderInfoToOrderItemCollection($collection);
        $collection = Mage::helper('aw_eventbooking/order')->addEventTicketInfoToOrderItemCollection($collection);
        $collection->addExpressionFieldToSelect(
            'purchased_qty',
            '({{i}}-{{r}})',
            array(
                 'i' => 'main_table.qty_invoiced',
                 'r' => 'main_table.qty_refunded'
            )
        );
        //get only invoiced
        $collection->addFieldToFilter(
            'qty_invoiced',
            array('gt' => new Zend_Db_Expr("qty_refunded"))
        );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('customer_name', array(
             'index'                     => 'order_customer_name',
             'header'                    => $this->__('Customer'),
             'width'                     => '200',
             'filter_condition_callback' => array($this, '_filterCustomerName'),
             'renderer'                  => 'aw_eventbooking/adminhtml_catalog_product_edit_tab_eventbooking_attendees_column_renderer_customer'
        ));
        $this->addColumn('increment_id', array(
             'index'        => 'order_increment_id',
             'filter_index' => 'order_table.increment_id',
             'header'       => $this->__('Order #'),
             'width'        => '200',
             'renderer'     => 'aw_eventbooking/adminhtml_catalog_product_edit_tab_eventbooking_attendees_column_renderer_order'
        ));
        $this->addColumn('ticket_type', array(
            'index'         => 'event_ticket_title',
            'header'        => $this->__('Ticket Type'),
            'filter_index'  => 'event_ticket_attribute_title_ds.value',
            'width'         => '200',
        ));
        $this->addColumn('purchased_qty', array(
            'type'                      => 'number',
            'index'                     => 'purchased_qty',
            'header'                    => $this->__('Qty invoiced'),
            'filter_condition_callback' => array($this, '_filterPurchasedQty'),
            'width'                     => '50',
        ));
        $this->addExportType('aw_eventbooking_admin/adminhtml_attendees/exportCsv', $this->__('CSV'));
        $this->addExportType('aw_eventbooking_admin/adminhtml_attendees/exportXml', $this->__('XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('attendees');

        $this->getMassactionBlock()->addItem('send_message', array(
            'label'    => $this->__('Send Message'),
            'url'      => $this->getUrl('aw_eventbooking_admin/adminhtml_attendees/massSendMessage'),
            'selected' => true,
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        return null;
    }

    public function getGridUrl()
    {
        return $this->getUrl('aw_eventbooking_admin/adminhtml_attendees/grid', array('_current' => true));
    }

    protected function _filterCustomerName($collection, $column)
    {
        $condition = $column->getFilter()->getCondition();
        $condition = array_pop($condition);
        $collection->getSelect()
            ->where(
                "order_table.customer_firstname LIKE ? OR order_table.customer_lastname LIKE ?",
                $condition,
                $condition
            )
        ;
        return $this;
    }

    protected function _filterPurchasedQty($collection, $column)
    {
        $condition = $column->getFilter()->getCondition();
        $fieldExpression = "(main_table.qty_invoiced - main_table.qty_refunded)";
        if (array_key_exists('from', $condition)) {
            $collection->getSelect()
                ->where($fieldExpression . " >= ?", $condition['from'])
            ;
        }
        if (array_key_exists('to', $condition)) {
            $collection->getSelect()
                ->where($fieldExpression . " <= ?", $condition['to'])
            ;
        }
        return $this;
    }
}