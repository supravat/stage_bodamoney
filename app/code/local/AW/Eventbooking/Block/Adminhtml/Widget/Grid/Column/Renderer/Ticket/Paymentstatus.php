<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Block_Adminhtml_Widget_Grid_Column_Renderer_Ticket_Paymentstatus
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{
    /**
     * @return AW_Eventbooking_Helper_Data
     */
    protected function _getHelper()
    {
        return Mage::helper('aw_eventbooking');
    }

    protected function _renderPaid($ticketId, $parentRenderedRow)
    {
        return sprintf(
            "%s [ <a href='#' onclick='AWEventbookingTicketsGrid.refundTicket(%d);return false;'>%s</a> ]",
            $parentRenderedRow,
            $ticketId,
            $this->_getHelper()->__('cancel')
        );
    }

    protected function _renderRefunded($ticketId, $parentRenderedRow)
    {
        return sprintf(
            "%s [ <a href='#' onclick='AWEventbookingTicketsGrid.undoRefundTicket(%d);return false;'>%s</a> ]",
            $parentRenderedRow,
            $ticketId,
            $this->_getHelper()->__('undo')
        );
    }

    public function render(Varien_Object $row)
    {
        $ticketId = $row->getData('id');
        $parentRenderedRow = parent::render($row);
        switch ($row->getData('payment_status')) {
            case AW_Eventbooking_Model_Ticket::PAYMENT_STATUS_PAID:
                return $this->_renderPaid($ticketId, $parentRenderedRow);
                break;
            case AW_Eventbooking_Model_Ticket::PAYMENT_STATUS_REFUNDED:
                return $this->_renderRefunded($ticketId, $parentRenderedRow);
                break;
        }
        return $parentRenderedRow;
    }
}
