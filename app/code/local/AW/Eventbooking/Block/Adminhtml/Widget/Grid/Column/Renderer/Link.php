<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Block_Adminhtml_Widget_Grid_Column_Renderer_Link
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        /** @var Mage_Adminhtml_Block_Widget_Grid_Column $column */
        $column = $this->getColumn();
        $linkAction = $column->getData('link_action');
        $linkParams = $column->getData('link_params');
        if (!$linkParams) {
            $linkParams = array();
        }
        foreach ($linkParams as $k => $v) {
            if ($row->getData($v)) {
                $linkParams[$k] = $row->getData($v);
            }
        }
        return sprintf(
            "<a href='%s'>%s</a>",
            Mage::helper('adminhtml')->getUrl($linkAction, $linkParams),
            $this->_getValue($row)
        );
    }
}
