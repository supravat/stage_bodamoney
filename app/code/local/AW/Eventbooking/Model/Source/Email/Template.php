<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Eventbooking_Model_Source_Email_Template
{
    const PLEASE_CHOOSE_CODE  = 0;
    const PLEASE_CHOOSE_LABEL = '--Please Choose--';

    public function toOptionArray($isAddEmptyOption = false)
    {
        $optionArray = array();
        if ($isAddEmptyOption) {
            $optionArray[] = array(
                'value' => self::PLEASE_CHOOSE_CODE,
                'label' => Mage::helper('aw_eventbooking')->__(self::PLEASE_CHOOSE_LABEL),
            );
        }
        foreach (Mage::getResourceModel('core/email_template_collection') as $key => $item) {
            $optionArray[] = array(
                'value' => $key,
                'label' => $item->getTemplateCode(),
            );
        }
        return $optionArray;
    }

    public function toArray($isAddEmptyOption = false)
    {
        $array = array();
        if ($isAddEmptyOption) {
            $array[self::PLEASE_CHOOSE_CODE] = Mage::helper('aw_eventbooking')->__(self::PLEASE_CHOOSE_LABEL);
        }
        foreach (Mage::getResourceModel('core/email_template_collection') as $key => $item) {
            $array[$key] = $item->getTemplateCode();
        }
        return $array;
    }
}