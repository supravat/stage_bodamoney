<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Source_System_Email_Template extends AW_Eventbooking_Model_Source_Abstract
{
    /**
     * @return Mage_Core_Model_Resource_Email_Template_Collection
     */
    protected function _getCollection()
    {
        if (!$collection = Mage::registry('config_system_email_template')) {
            $collection = Mage::getResourceModel('core/email_template_collection')->load();
            Mage::register('config_system_email_template', $collection);
        }
        return $collection;
    }

    protected function _toOptionArray()
    {
        /**
         * The same as Mage_Adminhtml_Model_System_Config_Source_Email_Template
         * but not including the 'Default template from Locale' item
         */
        return $this->_getCollection()->toOptionArray();
    }
}
