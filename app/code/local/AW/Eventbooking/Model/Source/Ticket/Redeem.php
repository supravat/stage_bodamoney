<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Source_Ticket_Redeem extends AW_Eventbooking_Model_Source_Abstract
{
    const REDEEMED_LABEL = 'Redeemed';
    const NOT_REDEEMED_LABEL = 'Not Redeemed';

    protected function _toOptionArray()
    {
        $_helper = $this->_getHelper();
        return array(
            array('value' => AW_Eventbooking_Model_Ticket::REDEEMED, 'label' => $_helper->__(self::REDEEMED_LABEL)),
            array('value' => AW_Eventbooking_Model_Ticket::NOT_REDEEMED, 'label' => $_helper->__(self::NOT_REDEEMED_LABEL)),
        );
    }
}
