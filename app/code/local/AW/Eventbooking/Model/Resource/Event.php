<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Resource_Event extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('aw_eventbooking/event', 'entity_id');
    }

    /**
     * After Load event push attribute values for store to model
     *
     * @param Mage_Core_Model_Abstract $event
     * @return AW_Eventbooking_Model_Resource_Event
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $event)
    {
        $redeemRoles = $event->getData('redeem_roles');
        $event->setData('redeem_roles', (is_string($redeemRoles) && $redeemRoles)
            ? explode(',', $redeemRoles)
            : array());

        foreach ($event->getAttributeCollection() as $attribute) {
            $event->setData($attribute->getAttributeCode(), $attribute->getValue());
        }
        return parent::_afterLoad($event);
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $event)
    {
        $redeemRoles = $event->getData('redeem_roles');
        $event->setData('redeem_roles', (is_array($redeemRoles) && $redeemRoles)
            ? implode(',', $redeemRoles)
            : null);
        return parent::_beforeSave($event);
    }
}
