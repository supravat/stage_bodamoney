<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Resource_Event_Ticket extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('aw_eventbooking/event_ticket', 'entity_id');
    }

    /**
     * Attach event ticket attribute values to model
     *
     * @param Mage_Core_Model_Abstract $ticket
     * @return AW_Eventbooking_Model_Resource_Event_Ticket
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $ticket)
    {
        foreach ($ticket->getAttributeCollection() as $attribute) {
            $ticket->setData($attribute->getAttributeCode(), $attribute->getValue());
        }
        return $this;
    }
}