<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Resource_Event_Ticket_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('aw_eventbooking/event_ticket');
    }

    /**
     * @param int $eventId
     *
     * @return $this
     */
    public function addFilterOnEventId($eventId)
    {
        $this->addFieldToFilter('main_table.event_id', intval($eventId));
        return $this;
    }

    /**
     * Join attribute values to collection
     *
     * @param int $storeId
     * @return AW_Eventbooking_Model_Resource_Event_Ticket_Collection
     */
    public function addTicketAttributes($storeId = Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID)
    {
        foreach (Mage::helper('aw_eventbooking/attribute')->getTicketAttributes() as $attributeCode) {
            $defaultStoreAlias = $attributeCode . '_ds';
            $currentStoreAlias = $attributeCode . '_cs';
            $this->getSelect()
                ->joinLeft(
                    array($defaultStoreAlias => $this->getTable('aw_eventbooking/event_ticket_attribute')),
                    "main_table.entity_id = {$defaultStoreAlias}.ticket_id AND
                    {$defaultStoreAlias}.attribute_code = '{$attributeCode}' AND
                    {$defaultStoreAlias}.store_id = " . (int)Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID,
                    array()
                )
                ->joinLeft(
                    array($currentStoreAlias => $this->getTable('aw_eventbooking/event_ticket_attribute')),
                    "main_table.entity_id = {$currentStoreAlias}.ticket_id
                    AND {$currentStoreAlias}.attribute_code = '{$attributeCode}'
                    AND {$currentStoreAlias}.attribute_code = {$defaultStoreAlias}.attribute_code
                    AND {$currentStoreAlias}.store_id = {$storeId}",
                    array($attributeCode => "IFNULL({$currentStoreAlias}.value, {$defaultStoreAlias}.value)")
                )
            ;
        }
        return $this;
    }

    /**
     *
     * @return AW_Eventbooking_Model_Resource_Event_Ticket_Collection
     */
    public function addOrderHistoryTotals()
    {
        $purchasedQtyExpression = "SUM(order_item_table.qty_invoiced - order_item_table.qty_refunded)";
        $currentRevenueExpression = "SUM(order_item_table.base_price_incl_tax * (order_item_table.qty_invoiced - order_item_table.qty_refunded))";
        $this->getSelect()
            ->joinLeft(
                array('event_order_history_table' => $this->getTable('aw_eventbooking/order_history')),
                "main_table.entity_id = event_order_history_table.event_ticket_id",
                array()
            )
            ->joinLeft(
                array('order_item_table' => $this->getTable('sales/order_item')),
                "event_order_history_table.order_item_id = order_item_table.item_id",
                array(
                    "purchased_qty"         => "IFNULL({$purchasedQtyExpression}, 0)",
                    "available_qty"         => "IFNULL((main_table.qty - IFNULL({$purchasedQtyExpression}, 0)), 0)",
                    "current_revenue"       => "IFNULL({$currentRevenueExpression}, 0)",
                )
            )
            ->group("main_table.entity_id")
        ;
        return $this;
    }

}