<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Resource_Order_History_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('aw_eventbooking/order_history');
    }

    /**
     * @param array $orderItemIds
     *
     * @return $this
     */
    public function addFilterOnOrderItemIds($orderItemIds)
    {
        $this->addFieldToFilter('order_item_id', array('in' => $orderItemIds));
        return $this;
    }

    /**
     * @param AW_Eventbooking_Model_Event_Ticket $eventTicket
     *
     * @return $this
     */
    public function addFilterOnEventTicket(AW_Eventbooking_Model_Event_Ticket $eventTicket)
    {
        $this->addFieldToFilter('event_ticket_id', $eventTicket->getId());
        return $this;
    }

    /**
     * @param $collection
     *
     * @return AW_Eventbooking_Model_Resource_Order_History_Collection
     */
    public function addFilterOnEventTicketCollection($collection)
    {
        $eventTicketIdsSelect = clone $collection->getSelect();
        $eventTicketIdsSelect->reset(Zend_Db_Select::COLUMNS);
        $eventTicketIdsSelect->columns(
            'main_table.entity_id'
        );
        $this->addFieldToFilter(
            'event_ticket_id',
            array('in' => $eventTicketIdsSelect)
        );
        return $this;
    }

    /**
     *
     * @return Mage_Sales_Model_Mysql4_Order_Item_Collection
     */
    public function getRelatedOrderItemCollection()
    {
        $orderItemCollection = Mage::getModel('sales/order_item')->getCollection();
        $orderItemIdsSelect = clone $this->getSelect();
        $orderItemIdsSelect->reset(Zend_Db_Select::COLUMNS);
        $orderItemIdsSelect->columns(
            'main_table.order_item_id'
        );
        $orderItemCollection->addFieldToFilter(
            'item_id',
            array('in' => $orderItemIdsSelect)
        );
        return $orderItemCollection;
    }
}