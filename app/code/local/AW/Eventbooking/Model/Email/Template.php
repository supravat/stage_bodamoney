<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


if (Mage::helper('aw_eventbooking')->isCustomSMTPInstalled()) {
    class AW_Eventbooking_Model_Email_TemplateCommon extends AW_Customsmtp_Model_Email_Template
    {
    }
} else {
    class AW_Eventbooking_Model_Email_TemplateCommon extends Mage_Core_Model_Email_Template
    {
    }
}

class AW_Eventbooking_Model_Email_Template extends AW_Eventbooking_Model_Email_TemplateCommon
{
    public function addAttachment($name, $content)
    {
        /** @var Zend_Mime_Part $attach */
        $attach = $this->getMail()->createAttachment($content);
        $attach->filename = $name;
        return $this;
    }
}
