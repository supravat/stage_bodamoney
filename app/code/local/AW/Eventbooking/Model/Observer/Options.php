<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Observer_Options
{
    /**
     * Observer for add our options to product entity after load
     * @param Varien_Object $observer
     *
     * @return $this
     */
    public function catalogProductLoadAfter($observer)
    {
        /* @var Mage_Catalog_Model_Product */
        $product = $observer->getEvent()->getProduct();
        /**
         * check of load product in admin/catalog/product/edit
         * @see Mage_Adminhtml_Catalog_ProductController->_initProduct
         */
        if ($product->getData('_edit_mode')) {
            return $this;
        }
        $this->_addOptionsToProduct($product);
        return $this;
    }

    /**
     * Observer for add our options to product items after load collection
     * in Mage_Sales_Model_Resource_Quote_Item_Collection->_assignProducts
     * @param $observer
     *
     * @return $this
     */
    public function salesQuoteItemCollectionProductsAfterLoad($observer)
    {
        $productCollection = $observer->getEvent()->getProductCollection();
        foreach ($productCollection as $item) {
            $this->_addOptionsToProduct($item);
        }
        return $this;
    }

    /**
     * Observer for add our options to product items after load collection
     * in Mage_Wishlist_Model_Resource_Item_Collection->_assignProducts
     * @param $observer
     *
     * @return $this
     */
    public function wishlistItemCollectionProductsAfterLoad($observer)
    {
        $productCollection = $observer->getEvent()->getProductCollection();
        foreach ($productCollection as $item) {
            $this->_addOptionsToProduct($item);
        }
        return $this;
    }


    protected function _addOptionsToProduct($product)
    {
        /**@var AW_Eventbooking_Model_Event $event */
        $event = Mage::getModel('aw_eventbooking/event')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->loadByProductId($product->getId())
        ;
        if (is_null($event->getId()) || !$event->getData('is_enabled')) {
            return;
        }
        $productHelper = Mage::helper('aw_eventbooking/product');
        $option = $productHelper->createProductOption(
            $product,
            Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO,
            AW_Eventbooking_Model_Event::PRODUCT_OPTION_ID,
            $event->getData('ticket_section_title')
        );

        $ticketCollection = $event->getTicketCollection();
        $ticketCollection->setOrder('CAST(sort_order AS SIGNED)', Varien_Data_Collection::SORT_ORDER_ASC);//sort as INT

        foreach ($ticketCollection as $ticket) {
            $productHelper->addProductOptionValue(
                $option,
                $ticket->getId(),
                array(
                     'title'        => $ticket->getData('title'),
                     'sku'          => $ticket->getData('sku'),
                     'sort_order'   => $ticket->getData('sort_order'),
                     'price_type'   => $ticket->getData('price_type'),
                     'price_value'  => $ticket->getData('price')
                )
            );
        }
        $product->addOption($option);
        $product->setHasOptions(true);
        $product->setRequiredOptions(true);
    }
}