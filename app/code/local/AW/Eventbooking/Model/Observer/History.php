<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Observer_History
{
    protected function _createTickets($orderItem, $eventTicket)
    {
        return Mage::getModel('aw_eventbooking/ticket')->createTickets($orderItem, $eventTicket);
    }

    /**
     * Observer for storing data about tickets when has been
     * @param Mage_Sales_Model_Order $order
     * @return $this
     */
    public function addOrderToHistory(Mage_Sales_Model_Order $order)
    {
        $eventBookingItems = Mage::helper('aw_eventbooking/order')->getAllEventbookingItemsFromOrder($order);
        foreach($eventBookingItems as $item) {
            $orderHistoryItems = Mage::getModel('aw_eventbooking/order_history')
                ->getCollection()
                ->addFieldToFilter('order_item_id', array('eq' => $item->getId()));

            if($orderHistoryItems->getSize()) {
                continue;
            }
            $eventTicketModel = Mage::helper('aw_eventbooking/order')->getEventTicketFromOrderItem($item);
            $eventOrderHistory = Mage::getModel('aw_eventbooking/order_history');
            $eventOrderHistory->setData(
                array(
                    'event_ticket_id'   => $eventTicketModel->getId(),
                    'order_item_id'     => $item->getId()
                )
            );
            try {
                $eventOrderHistory->save();
                $this->_createTickets($item, $eventTicketModel);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        return $this;
    }
}
