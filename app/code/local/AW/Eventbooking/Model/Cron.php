<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Cron
{

    /**
     * Call every 0 * * * *
     * Uses for send reminder email to customer
     */
    public function eventReminder()
    {
        $now = new Zend_Date();
        /**@var AW_Eventbooking_Model_Resource_Event_Collection $eventCollection*/
        $eventCollection = Mage::getModel('aw_eventbooking/event')->getCollection()
            ->addPendingReminderEmailFilter()
            ->addEventAttributes()
        ;
        foreach ($eventCollection as $event) {
            $daysBefore = (int)$event->getData('day_count_before_send_reminder_letter');
            $eventDatetime = new Zend_Date($event->getData('event_start_date'), Varien_Date::DATETIME_INTERNAL_FORMAT);
            $eventDatetime->subDay($daysBefore);
            if ($now->compare($eventDatetime) === -1) {
                continue;
            }
            try {
                Mage::helper('aw_eventbooking/mailer')->sendReminderEmailForEvent($event);
            } catch (Exception $e) {
                Mage::logException($e);
            }
            $event->setData('is_reminder_send', 1);
            $event->save();
        }
    }
}
