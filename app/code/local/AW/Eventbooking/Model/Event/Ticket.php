<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Event_Ticket extends Mage_Core_Model_Abstract
{
    protected $_storeId = Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    protected $_event;

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('aw_eventbooking/event_ticket');
    }

    /**
     * Get assigned event model
     *
     * @return AW_Eventbooking_Model_Event
     */
    public function getEventModel()
    {
        $eventId = $this->getData('event_id');
        if ($eventId && is_null($this->_event)) {
            /** @var AW_Eventbooking_Model_Event $event */
            $event = Mage::getModel('aw_eventbooking/event')
                ->setStoreId($this->getStoreId())
                ->load($eventId);
            if ($event->getId()) {
                $this->_event = $event;
            }
        }
        return $this->_event;
    }

    /**
     * @return AW_Eventbooking_Model_Resource_Order_History_Collection
     */
    public function getOrderHistoryCollection()
    {
        /* @var $collection AW_Eventbooking_Model_Resource_Order_History_Collection */
        $collection = Mage::getModel('aw_eventbooking/order_history')->getCollection();
        $collection->addFilterOnEventTicket($this);
        return $collection;
    }

    /**
     * @param Varien_Object $buyRequest
     * @param Mage_Sales_Model_Quote $quote
     * @throws Exception
     * @return AW_Eventbooking_Model_Event_Ticket
     */
    public function validateOnAddToCart($buyRequest, $quote)
    {
        $this->getEventModel()->validateOnAddToCart();

        //check on available stock
        $requestQty = intval($buyRequest->getData('qty'));
        if ($requestQty < 1) {
            $requestQty = 1;
        }
        $availableQty = $this->getQty() - $this->getPurchasedTicketQty();
        $usedItemQty = 0; //get item qty in quote
        $quoteItemList = Mage::helper('aw_eventbooking/quote')->getAllEventbookingItemsFromQuote($quote);
        foreach ($quoteItemList as $item) {
            $eventTicket = Mage::helper('aw_eventbooking/quote')->getEventTicketFromQuoteItem($item);
            if (!is_null($eventTicket) && $eventTicket->getId() === $this->getId()) {
                $usedItemQty += $item->getQty();
                break;
            }
        }
        if ($availableQty < ($requestQty + $usedItemQty)) {
            $productName = "";
            $product = $this->getEventModel()->getProductModel();
            if (!is_null($product)) {
                $productName = $product->getName();
            }
            $exceptionMessage = Mage::helper('aw_eventbooking')->__(
                "Only %s tickets are available for %s of type %s",
                $availableQty, $productName, $this->getTitle()
            );
            throw new Exception($exceptionMessage);
        }
        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $item
     * @throws Exception
     * @return AW_Eventbooking_Model_Event_Ticket
     */
    public function validateOnPlaceOrder(Mage_Sales_Model_Order_Item $item)
    {
        $this->getEventModel()->validateOnPlaceOrder();
        //check on available stock
        $availableQty = $this->getQty() - $this->getPurchasedTicketQty();
        if ($availableQty < $item->getQtyOrdered()) {
            $productName = "";
            $product = $this->getEventModel()->getProductModel();
            if (!is_null($product)) {
                $productName = $product->getName();
            }
            $exceptionMessage = Mage::helper('aw_eventbooking')->__(
                "Only %s tickets are available for %s of type %s",
                $availableQty, $productName, $this->getTitle()
            );
            throw new Exception($exceptionMessage);
        }
        return $this;
    }

    /**
     * @return AW_Eventbooking_Model_Resource_Event_Ticket_Attribute_Collection
     */
    public function getAttributeCollection()
    {
        return Mage::getResourceModel('aw_eventbooking/event_ticket_attribute_collection')
            ->getAttributeCollectionByStoreId($this->getId(), $this->getStoreId())
        ;
    }

    public function getAttributeByCode($attributeCode)
    {
        return Mage::getModel('aw_eventbooking/event_ticket_attribute')
            ->getTicketAttributeByCode($this->getId(), $attributeCode, $this->getStoreId())
        ;
    }

    public function setStoreId($storeId)
    {
        $this->_storeId = (int)$storeId;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_storeId;
    }

    /**
     * @see Mage_Sales_Model_Order_Item -> getStatusId
     * @return float
     */
    public function getPurchasedTicketQty()
    {
        $orderItemCollection = $this->getOrderHistoryCollection()
            ->getRelatedOrderItemCollection()
        ;
        $orderItemCollection->addExpressionFieldToSelect('sum_qty_ordered', 'SUM({{qty_ordered}})', 'qty_ordered');
        $orderItemCollection->addExpressionFieldToSelect('sum_qty_canceled', 'SUM({{qty_canceled}})', 'qty_canceled');
        $orderItemCollection->addExpressionFieldToSelect('sum_qty_refunded', 'SUM({{qty_refunded}})', 'qty_refunded');

        $ordered = 0;
        $canceled = 0;
        $refunded = 0;
        $item = $orderItemCollection->getFirstItem();
        if (!is_null($item->getId())) {
            $ordered  = $item->getData('sum_qty_ordered');
            $canceled = $item->getData('sum_qty_canceled');
            $refunded = $item->getData('sum_qty_refunded');
        }
        return $ordered - $canceled - $refunded;
    }
}
