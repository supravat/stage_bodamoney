<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Event_Ticket_Attribute extends Mage_Core_Model_Abstract
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('aw_eventbooking/event_ticket_attribute');
    }

    /**
     * @param int $ticketId
     * @param string $attributeCode
     * @param int $storeId
     *
     * @return AW_Eventbooking_Model_Event_Ticket_Attribute
     */
    public function getTicketAttributeByCode($ticketId, $attributeCode, $storeId)
    {
        $collection = $this->getCollection()
            ->addFieldToFilter('ticket_id', $ticketId)
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('attribute_code', $attributeCode)
        ;
        $collection->getSelect()->limit(1);
        return $collection->getFirstItem();
    }
}