<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Eventbooking
 * @version    1.1.1
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Eventbooking_Model_Observer
{
    /**
     * Call before "catalog/product/edit" action dispatching in adminhtml
     * @event controller_action_predispatch_adminhtml_catalog_product_edit
     * @see Mage_Adminhtml_Catalog_ProductController
     * @see Mage_Adminhtml_Controller_Action->preDispatch
     * @see Mage_Core_Controller_Varien_Action->preDispatch
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function catalogProductEditPredispatch($observer)
    {
        Mage::getModel('aw_eventbooking/observer_event')->catalogProductEditPredispatch($observer);
        return $this;
    }

    /**
     * Call before product save
     * @event catalog_product_save_before
     * @see Mage_Catalog_Model_Product->_beforeSave
     * @see Mage_Core_Model_Abstract->_beforeSave
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function catalogProductSaveBefore($observer)
    {
        Mage::getModel('aw_eventbooking/observer_event')->saveEvent($observer);
        return $this;
    }

    /**
     * Call after product save
     * @event catalog_product_save_after
     * @see Mage_Catalog_Model_Product->_afterSave
     * @see Mage_Core_Model_Abstract->_afterSave
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function catalogProductSaveAfter($observer)
    {
        Mage::getModel('aw_eventbooking/observer_event')->duplicateProduct($observer);
        return $this;
    }

    /**
     * Call after product load
     * @event catalog_product_load_after
     * @see Mage_Catalog_Model_Product->_afterLoad
     * @see Mage_Core_Model_Abstract->_afterLoad
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function catalogProductLoadAfter($observer)
    {
        Mage::getModel('aw_eventbooking/observer_options')->catalogProductLoadAfter($observer);
        return $this;
    }

    /**
     * Call after load product collection for quote items
     * @event sales_quote_item_collection_products_after_load
     * @see Mage_Sales_Model_Mysql4_Quote_Item_Collection->_assignProducts
     * @see Mage_Sales_Model_Resource_Quote_Item_Collection->_assignProducts
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function salesQuoteItemCollectionProductsAfterLoad($observer)
    {
        Mage::getModel('aw_eventbooking/observer_options')->salesQuoteItemCollectionProductsAfterLoad($observer);
        return $this;
    }

    /**
     * Call before add product to cart
     * @event catalog_product_type_prepare_cart_options
     * @see Mage_Catalog_Model_Product_Type_Abstract->_prepareOptionsForCart
     *
     * @event catalog_product_type_prepare_full_options
     * @see Mage_Catalog_Model_Product_Type_Abstract->_prepareOptions
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function catalogProductTypePrepareFullOptions($observer)
    {
        Mage::getModel('aw_eventbooking/observer_event')->validateOnAddToCart($observer);
        return $this;
    }

    /**
     * Call before update items in checkout/cart
     * @event checkout_cart_update_items_before
     * @see Mage_Checkout_Model_Cart->updateItems
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function checkoutCartUpdateItemsBefore($observer)
    {
        Mage::getModel('aw_eventbooking/observer_event')->validateOnUpdateItemsInCart($observer);
        return $this;
    }

    /**
     * Call before place order
     * @event sales_model_service_quote_submit_before
     * @see Mage_Sales_Model_Service_Quote->submitOrder
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function salesModelServiceQuoteSubmitBefore($observer)
    {
        Mage::getModel('aw_eventbooking/observer_event')->validateOnPlaceOrder($observer);
        return $this;
    }

    /**
     * Calls after multishipping checkout submits all created orders
     * @event checkout_submit_all_after
     * @see Mage_Checkout_Model_Type_Multishipping->createOrders
     *
     * @param $observer
     * @return $this
     */
    public function checkoutSubmitMultipleOrders($observer)
    {
        $orders = $observer->getOrders();
        if (is_array($orders) || ($orders instanceof Traversable)) {
            foreach ($orders as $order) {
                Mage::getModel('aw_eventbooking/observer_history')->addOrderToHistory($order);
            }
        }
        return $this;
    }

    /**
     * Call after place order
     * @event sales_model_service_quote_submit_after
     * @see Mage_Sales_Model_Service_Quote->submitOrder
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function salesModelServiceQuoteSubmitAfter($observer)
    {
        Mage::getModel('aw_eventbooking/observer_history')->addOrderToHistory($observer->getEvent()->getOrder());
        return $this;
    }

    /**
     * Call before block render (toHtml)
     * @event core_block_abstract_to_html_before
     * @see Mage_Core_Block_Abstract->toHtml
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function coreBlockAbstractToHtmlBefore($observer)
    {
        Mage::getModel('aw_eventbooking/observer_agreement')->addEventAgreementsToAgreementsBlock($observer);
        return $this;
    }

    /**
     * Call before "checkout/onepage/saveOrder" action dispatching in frontend
     * @event controller_action_predispatch_checkout_onepage_saveOrder
     * @see Mage_Checkout_OnepageController
     * @see Mage_Core_Controller_Front_Action->preDispatch
     * @see Mage_Core_Controller_Varien_Action->preDispatch
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function checkoutOnepageSaveOrderPredispatch($observer)
    {
        Mage::getModel('aw_eventbooking/observer_agreement')->checkoutOnepageSaveOrderPredispatch($observer);
        return $this;
    }

    /**
     * Call before "checkout/multishipping/overviewPost" action dispatching in frontend
     * @event controller_action_predispatch_checkout_multishipping_overviewPost
     * @see Mage_Checkout_MultishippingController
     * @see Mage_Core_Controller_Front_Action->preDispatch
     * @see Mage_Core_Controller_Varien_Action->preDispatch
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function checkoutMultishippingOverviewPostPredispatch($observer)
    {
        Mage::getModel('aw_eventbooking/observer_agreement')->checkoutMultishippingOverviewPostPredispatch($observer);
        return $this;
    }

    /**
     * Call before "onestepcheckout/ajax/placeOrder" action dispatching in frontend
     * @event controller_action_predispatch_onestepcheckout_ajax_placeOrder
     * @see AW_Onestepcheckout_AjaxController
     * @see Mage_Core_Controller_Front_Action->preDispatch
     * @see Mage_Core_Controller_Varien_Action->preDispatch
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function onestepcheckoutAjaxPlaceOrderPredispatch($observer)
    {
        Mage::getModel('aw_eventbooking/observer_agreement')->onestepcheckoutAjaxPlaceOrderPredispatch($observer);
        return $this;
    }

    /**
     * Call after load product collection for wishlist items
     * @event wishlist_item_collection_products_after_load
     * @see Mage_Wishlist_Model_Resource_Item_Collection->_assignProducts
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function wishlistItemCollectionProductsAfterLoad($observer)
    {
        Mage::getModel('aw_eventbooking/observer_options')->wishlistItemCollectionProductsAfterLoad($observer);
        return $this;
    }

    /**
     * Call after invoice register
     * @event sales_order_invoice_register
     * @see Mage_Sales_Model_Order_Invoice->register
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function salesOrderInvoiceRegister($observer)
    {
        Mage::getModel('aw_eventbooking/observer_history')->addOrderToHistory($observer->getEvent()->getOrder());
        Mage::getModel('aw_eventbooking/ticket')->markTicketsAsPaid($observer->getInvoice());
        Mage::getModel('aw_eventbooking/observer_event')->sendConfirmationEmail($observer);
        Mage::getModel('aw_eventbooking/observer_event')->sendReminderAfterInvoice($observer);
        return $this;
    }

    /**
     * Replace back url in catalog\product\edit
     * @event controller_action_layout_render_before_adminhtml_catalog_product_edit
     *
     * @param Varien_Object $observer
     * @return $this
     */
    public function controllerActionLayoutRenderBeforeAdminhtmlCatalogProductEdit($observer)
    {

        $request = Mage::app()->getRequest();
        if (!$backUrl = $request->getParam('awBackUrl')) {
            return $this;
        }

        $layout = Mage::getSingleton('core/layout');
        if (!$block = $layout->getBlock('product_edit')) {
            return $this;
        }
        if ($backButton = $block->getChild('back_button')) {
            $backButton->setData('onclick', 'setLocation(\'' . $backButton->getUrl(base64_decode($backUrl)) . '\')');
        }

        return $this;
    }
}
