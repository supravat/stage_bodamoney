<?php
require_once 'Mage/Customer/controllers/AccountController.php';
class Webkul_Mpgroupproduct_IndexController extends Mage_Customer_AccountController
{
    public function indexAction()
    {	
		if($this->getRequest()->isPost()){
			list($data, $errors) = $this->validatePost();
			$wholedata=$this->getRequest()->getParams();
			if(empty($errors)){
				Mage::getModel('mpgroupproduct/mpgroupproduct')->saveGroupNewProduct($wholedata);
				$vendorId = Mage::getSingleton('customer/session')->getCustomer()->getId();
				$customer = Mage::getModel('customer/customer')->load($vendorId);
				$cfname=$customer->getFirstname()." ".$customer->getLastname();
				$cmail=$customer->getEmail();
				$catagory_model = Mage::getModel('catalog/category');
				$categoryname="";				 
				foreach($wholedata['category'] as $key)
                {
				   $categoriesy = $catagory_model->load($key);
				   $categoryname=$categoryname ."  ". $categoriesy->getName().", ";
                }
				$emailTemp = Mage::getModel('core/email_template')->loadDefault('approveproduct');
				$emailTempVariables = array();
				$userModel = Mage::getModel('admin/user');
				 $userCollection = $userModel->getCollection()->load(); 
				foreach ($userCollection as  $value) {
					$emailTempVariables['myvar1'] = $wholedata['name'];
					$emailTempVariables['myvar2'] =$categoryname;
					$emailTempVariables['myvar3'] = $value->getName();
					$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
					$emailTemp->setSenderName($cfname);
					$emailTemp->setSenderEmail($cmail);
					$emailTemp->send( $value->getEmail(),$value->getName(),$emailTempVariables);
				}
			}else{
				foreach ($errors as $message) {Mage::getSingleton('core/session')->addError($message);}
				$_SESSION['new_products_errors'] = $data;
			}
			if (empty($errors))
				Mage::getSingleton('core/session')->addSuccess(Mage::helper('marketplace')->__('Your product was successfully Saved'));
				$this->_redirect('marketplace/marketplaceaccount/new/');
		}else{
			$this->_redirect('marketplace/marketplaceaccount/new/');
		}
    }
    
    private function validatePost(){
		$errors = array();
		$data = array();
		foreach( $this->getRequest()->getParams() as $code => $value){
			switch ($code) :
				case 'name':
					if(trim($value) == '' ){$errors[] = Mage::helper('marketplace')->__('Name has to be completed');} 
					else{$data[$code] = $value;}
					break;
				case 'description':
					if(trim($value) == '' ){$errors[] = Mage::helper('marketplace')->__('Description has to be completed');} 
					else{$data[$code] = $value;}
					break;
				case 'short_description':
					if(trim($value) == ''){$errors[] = Mage::helper('marketplace')->__('Short description has to be completed');} 
					else{$data[$code] = $value;}
					break;

				case 'sku_type':
					if(trim($value) == '' ){$errors[] = Mage::helper('marketplace')->__('Sku Type has to be selected');} 
					else{$data[$code] = $value;}
					break;	
			endswitch;
		}
		return array($data, $errors);
	}
	
	public function editapprovedgroupedAction()
	{
		 if($this->getRequest()->isPost()){
			list($data, $errors) = $this->validatePost();
			$id= $this->getRequest()->getParam('productid');
			if(empty($errors)){	
			  //  Mage::getSingleton('core/session')->setEditProductId($id);
				Mage::getModel('mpgroupproduct/mpgroupproduct')->editProduct($id,$this->getRequest()->getParams());
				Mage::getSingleton('core/session')->addSuccess(Mage::helper('marketplace')->__('Your Product Is Been Sucessfully Updated'));
				$this->_redirect('marketplace/marketplaceaccount/myproductslist/');
			}else{
				foreach ($errors as $message) {Mage::getSingleton('core/session')->addError($message);}
				$_SESSION['new_products_errors'] = $data;
				$this->_redirect('mpgroupproduct/index/editapprovedgrouped',array('id'=>$id));
			}
			
		}
		else{
			$this->loadLayout( array('default','mpgroup_account_groupedproductedit'));
			$this->_initLayoutMessages('customer/session');
			$this->_initLayoutMessages('catalog/session');
			$this->getLayout()->getBlock('head')->setTitle( Mage::helper('marketplace')->__('MarketPlace: Edit Grouped Magento Product'));
			$this->renderLayout();
		  }
	}
}
