<?php
Class Webkul_Mpgroupproduct_Model_Observer
{
	public function addGroupedProduct($observer){
		$layout=$observer->getLayout();
		$type=$observer->getType();
		switch($type){
			case "grouped":
				$layout->loadLayout( array('default','mpgroupproduct_index_index'));
				$layout->getLayout()->getBlock('head')->setTitle( Mage::helper('marketplace')->__('MarketPlace Product Type: Grouped Product'));
				break;
		}
	}			
}
