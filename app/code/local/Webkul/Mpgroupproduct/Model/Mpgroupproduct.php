<?php

class Webkul_Mpgroupproduct_Model_Mpgroupproduct extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('mpgroupproduct/mpgroupproduct');
    }
    
    public function saveGroupNewProduct($wholedata){
		$cats=array();
		$quan=array();
		$pos=array();
		foreach($wholedata['category'] as $keycat){
			array_push($cats,$keycat);
		}
		
		foreach($wholedata['ord'] as $key){
		   array_push($pos,$key); 
		}
		
		foreach($wholedata['quant'] as $ky){
			array_push($quan,$ky); 
		}
		$assignp=array();
		foreach($wholedata['ids'] as $key){
			array_push($assignp,$key);	
		}
		
		if($wholedata['status']==1 && isset($wholedata['wstoreids']) ){
			$status=1; 
			$stores=$wholedata['wstoreids'];
		}
		else{		
			$status=Mage::getStoreConfig('marketplace/marketplace_options/product_approval')? 2:1;
			$stores=Mage::app()->getStore()->getStoreId();
		}
		$wholedata['status']=$status;
		$magentoProductModel = Mage::getModel('catalog/product');
		$magentoProductModel->setData($wholedata);
		$saved=$magentoProductModel->save();
		$lid=$saved->getId();
        
		$magentoProductModel = Mage::getModel('catalog/product')->load($saved->getId());
		$magentoProductModel->setStoresIds(array($stores));
		$storeId = Mage::app()->getStore()->getId();
		$magentoProductModel->setWebsiteIds(array(Mage::getModel('core/store')->load( $storeId )->getWebsiteId()));
		$magentoProductModel->setCategoryIds($cats);
		$magentoProductModel->setStatus($status);
		$saved=$magentoProductModel->save();
		$lastId = $saved->getId();
		
		foreach ($assignp as $key=>$val) {	
			$products_links = Mage::getModel('catalog/product_link_api');
	        $products_links->assign ("grouped",$lastId,$assignp[$key],array('position'=>$pos[$key],'qty'=>$quan[$key]));           
	    }
		//$this->_saveStock($lastId,$wholedata['stock'],$wholedata['is_in_stock']); 
		$vendorId = Mage::getSingleton('customer/session')->getCustomer()->getId();
		$collection1=Mage::getModel('marketplace/product');
		$collection1->setMageproductid($lastId);
		$collection1->setUserid($vendorId);
		$collection1->setStatus($status);
		$collection1->save();
		if(!is_dir(Mage::getBaseDir().'/media/marketplace/')){
			mkdir(Mage::getBaseDir().'/media/marketplace/', 0755);
		}
		if(!is_dir(Mage::getBaseDir().'/media/marketplace/'.$lastId.'/')){
			mkdir(Mage::getBaseDir().'/media/marketplace/'.$lastId.'/', 0755);
		}
		$target =Mage::getBaseDir().'/media/marketplace/'.$lastId.'/';

		if(isset($_FILES) && count($_FILES) > 0){
			if($wholedata['type_id']=='simple'){
				foreach($_FILES as $image ){
					if($image['tmp_name'] != ''){
						$target1 = $target.$image['name']; 
						move_uploaded_file($image['tmp_name'],$target1);
					}
				}
			}				
		}
		$this->_addImages($lastId,$wholedata['defaultimage']);
		$qtyStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($lastId);
		$qtyStock->setProductId($lastId)->setStockId(1);
		$qtyStock->setData('is_in_stock', $wholedata['is_in_stock']); 
		$savedStock = $qtyStock->save();
		return $lastId;		
	}
	
	private function _addImages($objProduct,$defaultimage){
		$mediDir = Mage::getBaseDir('media');
		$imagesdir = $mediDir . '/marketplace/' . $objProduct . '/';
		if(!file_exists($imagesdir)){return false;}
		foreach (new DirectoryIterator($imagesdir) as $fileInfo){
    		if($fileInfo->isDot() || $fileInfo->isDir()) continue;
    		if($fileInfo->isFile()){
				$fileinfo=explode('@',$fileInfo->getPathname());
				$objprod=Mage::getModel('catalog/product')->load($objProduct);
				$objprod->addImageToMediaGallery($fileInfo->getPathname(), array ('image','small_image','thumbnail'), true, false);
				Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
				$objprod->save();					
				
    		}
		}
		$_product = Mage::getModel('catalog/product')->load($objProduct)->getMediaGalleryImages();
		if (strpos($defaultimage, '.') !== FALSE){
			$defimage =  explode('.',$defaultimage);
			$defimage[0] = str_replace('-', '_', $defimage[0]);
			foreach ($_product as $value) {
				$image = explode($defimage[0],$value->getFile());
				if (strpos($value->getFile(), $defimage[0]) !== FALSE){
					$newimage = $value->getFile();
				}
			}
		}else{
			foreach ($_product as $value) {
				if($value->getValueId()==$defaultimage){
					$newimage = $value->getFile();
				}
			}
		}
		$objprod=Mage::getModel('catalog/product')->load($objProduct);
		$objprod->setSmallImage($newimage);
		$objprod->setImage($newimage);
		$objprod->setThumbnail($newimage);
		$objprod->save();	
	}
	
	private function _saveStock($lastId,$stock,$isstock){
		$stockItem = Mage::getModel('cataloginventory/stock_item');
		$stockItem->loadByProduct($lastId);
		if(!$stockItem->getId()){$stockItem->setProductId($lastId)->setStockId(1);}
		$stockItem->setProductId($lastId)->setStockId(1);
		$stockItem->setData('is_in_stock', $isstock); 
		$savedStock = $stockItem->save();
		$stockItem->load($savedStock->getId())->setQty($stock)->save();
		$stockItem->setData('is_in_stock', $isstock); 
		$savedStock = $stockItem->save();
	}
	
	public function editproduct($id,$wholedata){
		$cats=array();
		$assignp=array(); 
		$rmid=array(); 
		$quan=array();
		$pos=array();
		foreach($wholedata['ids'] as $key){
			array_push($assignp,trim($key));	
		}
		$loadp =Mage::getModel('catalog/product')->load($id);
		foreach($loadp->getTypeInstance(true)->getAssociatedProducts($loadp) as $key){
			 $pid=trim($key->getId());
		     if(!in_array($pid,$assignp)){
				 array_push($rmid,$pid);  
			 }
		}
		
		foreach($wholedata['ord'] as $key){
		   array_push($pos,$key); 
		}
		
		foreach($wholedata['quant'] as $ky){
			array_push($quan,$ky); 
		}
		
		
		foreach ($rmid as $key=>$val) {	
			$products_links = Mage::getModel('catalog/product_link_api');
			$products_links->remove('grouped',$id,$rmid[$key]);           
	    }
		
		foreach ($assignp as $key=>$val) {	
		$products_links = Mage::getModel('catalog/product_link_api');
        $products_links->update('grouped',$id,$assignp[$key],array('position'=>$pos[$key],'qty'=>$quan[$key]));           
	    }
	    
		
		foreach($wholedata['category'] as $keycat){
			array_push($cats,$keycat);
		}
		$productcity = Mage::getModel('catalog/product');
		$loadpro = $productcity->load($id);
		foreach($wholedata as $key=>$val)
		{
				$loadpro->setData($key,$val);
		}
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$saved=$loadpro->save();
		$loadpro = Mage::getModel('catalog/product')->load($saved->getId());	
		$loadpro->setCategoryIds($cats);
		$loadpro->save();
		$qtyStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($id);
		$saved=$loadpro->save();
		if(!is_dir(Mage::getBaseDir().'/media/marketplace/')){
			mkdir(Mage::getBaseDir().'/media/marketplace/', 0755);
		}
		if(!is_dir(Mage::getBaseDir().'/media/marketplace/'.$id.'/')){
			mkdir(Mage::getBaseDir().'/media/marketplace/'.$id.'/', 0755);
		}
		$target =Mage::getBaseDir().'/media/marketplace/'.$id.'/';
		if(isset($_FILES) && count($_FILES) > 0){
			foreach($_FILES as $image ){
				if($image['tmp_name'] != ''){
					$target1 = $target.$image['name']; 
					move_uploaded_file($image['tmp_name'],$target1);
				}
			}				
		}
		if($wholedata['status']==2){
			$collection1=Mage::getModel('marketplace/product')->getCollection()->addFieldToFilter('mageproductid',array('eq'=>$saved->getId()));
			foreach ($collection1 as $coll) {
				$coll->setStatus(2);
				$coll->save();
			}
			$allStores = Mage::app()->getStores();
			foreach ($allStores as $_eachStoreId => $val)
			{
				Mage::getModel('catalog/product_status')->updateProductStatus($saved->getId(),Mage::app()->getStore($_eachStoreId)->getId(), Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
			}
		}
		$this->_addImages($id,$wholedata['defaultimage']);
		$qtyStock->setProductId($id)->setStockId(1);
		$qtyStock->setData('is_in_stock', $wholedata['is_in_stock']); 
		$savedStock = $qtyStock->save();
		return 0;
				
    }
}
