<?php
class Webkul_Mpgroupproduct_Block_Mpgroupproduct extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getMpgroupproduct()     
     { 
        if (!$this->hasData('mpgroupproduct')) {
            $this->setData('mpgroupproduct', Mage::registry('mpgroupproduct'));
        }
        return $this->getData('mpgroupproduct');
        
    }
}