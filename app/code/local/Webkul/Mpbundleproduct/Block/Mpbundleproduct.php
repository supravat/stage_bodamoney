<?php
class Webkul_Mpbundleproduct_Block_Mpbundleproduct extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	public function __construct(){		

		parent::__construct();	

    	$userId=Mage::getSingleton('customer/session')->getCustomer()->getId();

		$collection = Mage::getModel('marketplace/product')->getCollection()->addFieldToFilter('userid',array('eq'=>$userId))->addFieldToFilter('status',array('eq'=>1));
		$products=array();
		foreach($collection as $data){
			array_push($products,$data->getMageproductid());
		}
		$collection = Mage::getModel('catalog/product')
					->getCollection()->addAttributeToSelect('*')
					->addFieldToFilter('entity_id',array('in'=>$products))
					->addFieldToFilter('type_id',array('neq'=>'bundle'));

		$this->setCollection($collection);

	}
     public function getMpbundleproduct()     
     { 
        if (!$this->hasData('mpbundleproduct')) {
            $this->setData('mpbundleproduct', Mage::registry('mpbundleproduct'));
        }
        return $this->getData('mpbundleproduct');
        
    }
	public function getProduct() {

		$id = $this->getRequest()->getParam('id');

		$products = Mage::getModel('catalog/product')->load($id);

		return $products;

	}
}
