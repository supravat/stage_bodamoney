<?php
	require_once 'Mage/Customer/controllers/AccountController.php';
	class Webkul_Mpbundleproduct_IndexController extends Mage_Customer_AccountController{
		public function indexAction(){
			if($this->getRequest()->isPost()){
			$wholedata=$this->getRequest()->getParams();
			$productid=Mage::getModel('marketplace/product')->saveSimpleNewProduct($wholedata);
			$productCheck=Mage::getModel('catalog/product')->load($productid);
			Mage::register('product', $productCheck);
			foreach($wholedata['bundle_options'] as $ky=>$vl){
				$optionRawData[$ky] = array(
										'required' => $vl['required'],
										'option_id' => '',
										'position' =>$vl['position'],
										'type' => $vl['type'],
										'title' => $vl['default_title'],
										'default_title' => $vl['default_title'],
										'delete' => '',
									  );
			} 
			$selectionRawData=array();
			foreach($wholedata['product_id'] as $key=>$value){
				foreach($value as $ki){ 
					$selectionRawData[$key][] = array(
													'product_id' =>$ki,
													'selection_id' => '',
													'option_id' => '',
													'delete' => ''
												);
				} 
			}
			foreach($wholedata['selection_can_change_qty'] as $key=>$value){
				$i= 0;
				foreach($value as $ki){ 	   
					$selectionRawData[$key][$i++]['selection_can_change_qty']=$ki;
				}        
			}
			foreach($wholedata['default_position'] as $key=>$value){
				$i= 0;
				foreach($value as $ki) { 	   
					$selectionRawData[$key][$i++]['position']=$ki;
				}        
			}
			foreach($wholedata['default'] as $key=>$value){
				$i= 0;
				foreach($value as $ki) { 	   
					$selectionRawData[$key][$i++]['is_default']= $ki;
				}        
			}
			foreach($wholedata['default_price'] as $key=>$value){
				$i= 0;
				foreach($value as $ki) { 	   
					$selectionRawData[$key][$i++]['selection_price_value']= $ki;
				}        
			}
			foreach($wholedata['default_qty'] as $key=>$value){
				$i= 0;
				foreach($value as $ki) { 	   
					$selectionRawData[$key][$i++]['selection_qty']= $ki;
				}        
			}	
			foreach($wholedata['default_price_type'] as $key=>$value){
				$i= 0;
				foreach($value as $ki) { 	   
					$selectionRawData[$key][$i++]['selection_price_type']= $ki;
				}        
			}

			$productCheck->setBundleOptionsData($optionRawData);
			$productCheck->setBundleSelectionsData($selectionRawData);
			$productCheck->save();
			Mage::unregister('product');
			Mage::getSingleton('core/session')->addSuccess(Mage::helper('mpbundleproduct')->__('Your Bundle Product was successfully Saved'));
			return $this->_redirect('marketplace/marketplaceaccount/new/');
			}
			else{
				return $this->_redirect('marketplace/marketplaceaccount/new/');
			}			
		}	
		
		
		public function editbundleAction(){
			if($this->getRequest()->isPost()){
				$wholedata=$this->getRequest()->getParams(); 
				$id= $this->getRequest()->getParam('productid');
				$customerid=Mage::getSingleton('customer/session')->getCustomerId();
				$collection_product = Mage::getModel('marketplace/product')->getCollection()->addFieldToFilter('mageproductid',array('eq'=>$id))->addFieldToFilter('userid',array('eq'=>$customerid));
				if(count($collection_product)){
					Mage::getSingleton('core/session')->setEditProductId($id);
					Mage::getModel('marketplace/product')->editProduct($id,$this->getRequest()->getParams());
					$productCheck=Mage::getModel('catalog/product')->load($id);
					Mage::register('product', $productCheck);
					$bundled = Mage::getModel('catalog/product')->load($id);
					$selectionCollection = $bundled->getTypeInstance(true)
												   ->getSelectionsCollection($bundled->getTypeInstance(true)->getOptionsIds($bundled), $bundled);
					
					foreach($wholedata['bundle_options'] as $ky=>$vl){
						$optionRawData[$ky] = array(
												'required' => $vl['required'],
												'option_id' =>  $vl['optid'],
												'position' =>$vl['position'],
												'type' => $vl['type'],
												'title' => $vl['default_title'],
												'default_title' => $vl['default_title'],
												'delete' => '',
											);
					} 
					$narrays=array();
					$selectionRawData=array();

					foreach($wholedata['product_id'] as $key=>$value){ 
						foreach($value as $ki) {
							$selectionRawData[$key][] = array(
														'product_id' =>$ki,
														// 'selection_id' => '',
														//'option_id' => '',
														'is_default'=>$wholedata['default'][$key][$ki],
														'delete' => ''
														);
						} 
					}
					
					foreach($wholedata['selection_can_change_qty'] as $key=>$value){
						$i= 0;
						foreach($value as $ki) { 	   
							$selectionRawData[$key][$i++]['selection_can_change_qty']= $ki;
						}        
					}
					foreach($wholedata['default_position'] as $key=>$value){
						$i= 0;
						foreach($value as $ki) { 	   
							$selectionRawData[$key][$i++]['position']= $ki;
						}        
					}
					foreach($wholedata['default_price'] as $key=>$value){
						$i= 0;
						foreach($value as $ki) { 	   
							$selectionRawData[$key][$i++]['selection_price_value']= $ki;
						}        
					}
					foreach($wholedata['default_qty'] as $key=>$value){
						$i= 0;
						foreach($value as $ki) { 	   
							$selectionRawData[$key][$i++]['selection_qty']= $ki;
						}        
					}	
					foreach($wholedata['default_price_type'] as $key=>$value){
						$i= 0;
						foreach($value as $ki) { 	   
							$selectionRawData[$key][$i++]['selection_price_type']= $ki;
						}        
					}
					
					$productCheck->setBundleOptionsData($optionRawData);
					$productCheck->setBundleSelectionsData($selectionRawData);
					$productCheck->save();
					
					Mage::getSingleton('core/session')->addSuccess(Mage::helper('marketplace')->__('Your Product Is Been Sucessfully Updated'));
					$this->_redirect('marketplace/marketplaceaccount/myproductslist/');
		      }
		      else
		      {
				  $this->_redirect('mpbundleproduct/index/editbundle',array('id'=>$id));
			  }
		   
		   }
			else{
				$this->loadLayout(array('default','marketplace_marketplaceaccount_editapprovedbundle'));
				$this->_initLayoutMessages('customer/session');
				$this->_initLayoutMessages('catalog/session');
				$this->getLayout()->getBlock('head')->setTitle( Mage::helper('marketplace')->__('MarketPlace: Edit Bundle Product'));
				$this->renderLayout();
			}
		 
		}
		
		
		public function deleteoptionAction(){
			$data = $this->getRequest()->getParams();
			$optionModel = Mage::getModel('bundle/option');
			$optionModel->setId($data['opid']);
			$optionModel->delete();
		}	
		
	} 
