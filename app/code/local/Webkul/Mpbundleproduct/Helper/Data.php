<?php

class Webkul_Mpbundleproduct_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function saveBundleproduct($productid,$wholedata){
		$productCheck=Mage::getModel('catalog/product')->load($productid);	
		foreach($productCheck->getTypeInstance(true)->getOptionsCollection($productCheck) as $options){	
			$optionid=$options->delete();
		}				
		Mage::register('product',$productCheck);	
		$selectionRawData=array();			
		$optionRawData[0] = array(			
			'required' => $wholedata['bundle_options']['required'],	
			'option_id' => '',						
			'position' =>$wholedata['bundle_options']['position'],	
			'type' => $wholedata['bundle_options']['type'],		
			'title' => $wholedata['bundle_options']['default_title'],				'default_title' => $wholedata['bundle_options']['default_title'],	
			'delete' => '',				
			);	
		foreach($wholedata['product_id'] as $key=>$value){	
			$val=$wholedata['selection_can_change_qty'][$key];		
			if($val!=''){			
				$selectionRawData[0][] = array(		
					'product_id' => $value,				
					'selection_qty' => $wholedata['default_qty'][$key],
					'selection_can_change_qty'=>$wholedata['selection_can_change_qty'][$key],							
					'position' => $wholedata['default_position'][$key],				
					'is_default' => $wholedata['default'][$key],
					'selection_price_value'=>$wholedata['default_price'][$key],
					'selection_price_type'=>$wholedata['default_price_type'][$key],
					'selection_id' => '',
					'option_id' => '',
					'delete' => ''	
					);	
				}	
		}			
		$productCheck->setBundleOptionsData($optionRawData);	
		$productCheck->setBundleSelectionsData($selectionRawData);				$productCheck->save();		
		Mage::unregister('product');	
	}
}