<?php
Class Webkul_Mpbundleproduct_Model_Observer
{
	public function addBundleProduct($observer){
		$layout=$observer->getLayout();
		$type=$observer->getType();
		switch($type){
			case "bundle":
				$layout->loadLayout( array('default','mpbundleproduct_index_index'));
				$layout->getLayout()->getBlock('head')->setTitle( Mage::helper('marketplace')->__('MarketPlace Product Type: Bundle Product'));
				break;
		}
	}			
}
