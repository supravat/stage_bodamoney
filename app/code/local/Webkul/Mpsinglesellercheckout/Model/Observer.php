<?php
Class Webkul_Mpsinglesellercheckout_Model_Observer
{
	public function addItem($observer){
		if(Mage::getStoreConfig('marketplace/marketplace_options/allow_single_seller')==1){
			$productid=$observer->getQuoteItem()->getProductId();
			$sellerid=$this->getSellerId($productid);
			$items =  Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
			$count=0;
			foreach($items as $item){
					$tempsellerid=$this->getSellerId($item->getProductId());
					if($sellerid!=$tempsellerid){
						$item->delete();
						$count++;
					}
			}
			if($count>0){
				Mage::getSingleton('checkout/session')->addNotice('At a time you can add any one seller products...');
			}
		}
	}
	
	private function getSellerId($productid){
		$sellers=Mage::getModel('marketplace/product')->getCollection()
								->addFieldToFilter('mageproductid',array('eq'=>$productid));
		$sellerid=0;
		foreach($sellers as $seller){
				$sellerid=$seller->getUserid();
		}
		return $sellerid;
	}
}
