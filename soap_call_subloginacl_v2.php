<?php
ini_set('display_errors', 1);

$client = new SoapClient('http://127.0.0.1/patrick_mage_sublogin/api/v2_soap?wsdl'); // v2
$session = $client->login('sublogin', 'sublogin123'); // login(username, apiKey)

// how to create new entry
$data = array(
	'name'			=>		"Allow Checkout",
	'identifier'	=>		"allow_checkout",
);
$newId = $client->subloginAclCreate($session, $data);
echo "########## CREATE ##########<br>";
echo "<pre>";
print_r($newId);
echo "</pre>";

// how to get list
$params = array('filter' => array(
    array('key' => 'acl_id', 'value' => $newId),
));
$result = $client->subloginAclList($session, $params);

echo "########## LIST ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to get single row: info
$result = $client->subloginAclInfo(
	$session, 
	$newId // OR can specify id manually for example: 1
); 
echo "########## INFO ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to update existing entry
$data = array(
	'name'			=>		"Allow Checkout2",
	'identifier'	=>		"allow_checkout2",
);
$result = $client->subloginAclUpdate(
	$session, 
	$newId, // OR can specify id manually for example: 1
	$data
);
echo "########## UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";


$result = $client->subloginAclInfo(
	$session, 
	$newId // OR can specify id manually for example: 1
);
echo "########## INFO AFTER UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to delete
//$result = $client->subloginAclDelete(
	//$session, 
	//$newId // OR can specify id manually for example: 1
//); 
echo "########## DELETE ##########<br>";
//echo "<pre>";
//print_r($result);
//echo "</pre>";
$client->endSession($session);
