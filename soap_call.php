<?php
ini_set('display_errors', 1);

$client = new SoapClient('http://127.0.0.1/patrick_mage_sublogin/api/v2_soap?wsdl'); // v2
$session = $client->login('sublogin', 'sublogin123'); // login(username, apiKey)

// how to create new entry
$data = array(
	'entity_id'			=>		"127", // customer id
	'email'				=>		time()."@mageb2b.com",
	'password'			=>		"123456",
	'firstname'			=>		"Firstname",
	'lastname'			=>		"Lastname",
	'expire_date'		=>		"2015-12-30", // Y-m-d
	'active'			=>		"1",
	'send_backendmails'	=>		"0",
	'create_sublogins'	=>		"0",
	'is_subscribed'		=>		"0",
	'store_id'			=>		"1",
	'address_ids'		=>		"",
	'acl'				=>		"allow_checkout,allow_orderview",
);
$subloginId = $client->subloginCreate($session, $data);
echo "########## CREATE ##########<br>";
echo "<pre>";
print_r($subloginId);
echo "</pre>";

// how to get list
$params = array('filter' => array(
    array('key' => 'id', 'value' => '1'),
));
$result = $client->subloginList($session, $params);

echo "########## LIST ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to get single row: info
$result = $client->subloginInfo(
	$session, 
	$subloginId // OR can specify id manually for example: 1
); 
echo "########## INFO ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to update existing entry
$data = array(
	'entity_id'			=>		"127", // customer id
	'email'				=>		time()."@mageb2b.com",
	'password'			=>		"123456",
	'firstname'			=>		"Firstname updated",
	'lastname'			=>		"Lastname updated",
	'expire_date'		=>		"2016-12-30", // Y-m-d
	'active'			=>		"1",
	'send_backendmails'	=>		"0",
	'create_sublogins'	=>		"1",
	'is_subscribed'		=>		"0",
	'store_id'			=>		"1",
	'address_ids'		=>		"",
	'acl'				=>		"allow_checkout,allow_orderview",
);
$result = $client->subloginUpdate(
	$session, 
	$subloginId, // OR can specify id manually for example: 1
	$data
);
echo "########## UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";


$result = $client->subloginInfo(
	$session, 
	$subloginId // OR can specify id manually for example: 1
);
echo "########## INFO AFTER UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to delete
//$result = $client->subloginDelete(
	//$session, 
	//$subloginId // OR can specify id manually for example: 1
//); 
echo "########## DELETE ##########<br>";
//echo "<pre>";
//print_r($result);
//echo "</pre>";
$client->endSession($session);
