<?php
ini_set('display_errors', 1);

$mageFilename = 'app/Mage.php';
if (!file_exists($mageFilename)) {
    if (is_dir('downloader')) {
        header("Location: downloader");
    } else {
        echo $mageFilename." was not found";
    }
    exit;
}
require_once $mageFilename;

// xmlrpc
$client = new Zend_XmlRpc_Client('http://127.0.0.1/patrick_mage_sublogin/api/xmlrpc/');

$session = $client->call('login', array('sublogin', 'sublogin123')); // pass username and apiKey in array

// how to create new entry
$data = array(
	'entity_id'			=>		"127", // customer id
	'email'				=>		time()."@mageb2b.com",
	'password'			=>		"123456",
	'firstname'			=>		"Firstname",
	'lastname'			=>		"Lastname",
	'expire_date'		=>		"2014-12-30", // Y-m-d
	'active'			=>		"1",
	'send_backendmails'	=>		"0",
	'create_sublogins'	=>		"0",
	'is_subscribed'		=>		"0",
	'store_id'			=>		"1",
	'address_ids'		=>		"",
);
$subloginId = $client->call('call', array($session, 'mageb2b_sublogin.create', array($data)));

echo "########## CREATE ##########<br>";
echo "<pre>";
print_r($subloginId);
echo "</pre>";

// how to get list
$params = array(
    'id' => $subloginId,
);

$result = $client->call('call', array($session, 'mageb2b_sublogin.list', array($params)));

echo "########## LIST ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to get single row: info
Mage::log('get single row start');
$result = $client->call('call', array($session, 'mageb2b_sublogin.info', array($subloginId)));
echo "########## INFO ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";
Mage::log('get single row end');

// how to update existing entry
$data = array(
	'entity_id'			=>		"127", // customer id
	'email'				=>		time()."@mageb2b.com",
	'password'			=>		"123456",
	'firstname'			=>		"Firstname updated",
	'lastname'			=>		"Lastname updated",
	'expire_date'		=>		"2014-12-30", // Y-m-d
	'active'			=>		"1",
	'send_backendmails'	=>		"0",
	'create_sublogins'	=>		"1",
	'is_subscribed'		=>		"0",
	'store_id'			=>		"1",
	'address_ids'		=>		"",
);

$result = $client->call('call', array($session, 'mageb2b_sublogin.update', array($subloginId, $data)));
echo "########## UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// getting info after update: info
$result = $client->call('call', array($session, 'mageb2b_sublogin.info', array($subloginId)));
echo "########## INFO AFTER UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to delete
$result = $client->call('call', array($session, 'mageb2b_sublogin.delete', array($subloginId)));
echo "########## DELETE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

$client->call('endSession', array($session));


