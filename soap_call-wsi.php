<?php
ini_set('display_errors', 1);
## MAIN DIFFERENCE IN WSI IS THAT PARAMETERS ARE GOING TO BE PASSED AS AN ARRAY WHILE IN WSDL DIRECT PARAMETER CAN BE PASSED
## SO IN WSDL: $client->subloginList($session, $filters);
## WHILE SAME CAN USE USED IN THIS IN WSI COMPLIANT MODE: 
	#$client->subloginList(array(
	#	'sessionId'=>$sessionId, 
	#	'filters'=>$filters
	#));
## AND SESSION ID NEEDS BE GET IN THIS WAY: $sessionId = $session->result;

$client = new SoapClient('http://127.0.0.1/patrick_mage_sublogin/api/v2_soap?wsdl', array('trace' => 1, 'connection_timeout' => 120)); // v2
$session = $client->login(array(
	'username' => "sublogin",
	'apiKey' => "sublogin123"
));
$sessionId = $session->result;

// how to create new entry
$data = array(
	'entity_id'			=>		"127", // customer id
	'email'				=>		time()."@mageb2b.com",
	'password'			=>		"123456",
	'firstname'			=>		"Firstname",
	'lastname'			=>		"Lastname",
	'expire_date'		=>		"2015-12-30", // Y-m-d
	'active'			=>		"1",
	'send_backendmails'	=>		"0",
	'create_sublogins'	=>		"0",
	'is_subscribed'		=>		"0",
	'store_id'			=>		"1",
	'address_ids'		=>		"",
	'acl'				=>		"allow_checkout,allow_orderview",
);
$result = $client->subloginCreate(array(
	'sessionId'=>$sessionId, 
	'data'=>$data
));
$subloginId = $result->result;
echo "########## CREATE ##########<br>";
echo "<pre>";
print_r($subloginId);
echo "</pre>";

// how to get list
$filters = array(
	'complex_filter' => array(
		array(
			'key' => 'id',
			'value' => array(
					'key' => 'eq',
					'value' => $subloginId
			),
		),
	),
);

$result = $client->subloginList(array(
	"sessionId" => $sessionId, 
	"filters" => $filters
));
echo "########## LIST ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to get single row: info
$result = $client->subloginInfo(array(
	'sessionId'=>$sessionId,
	'id'=>$subloginId, // OR can specify id manually for example: 1
));

echo "########## INFO ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

// how to update existing entry
$data = array(
	'entity_id'			=>		"127", // customer id
	'email'				=>		time()."@mageb2b.com",
	'password'			=>		"123456",
	'firstname'			=>		"Firstname updated",
	'lastname'			=>		"Lastname updated",
	'expire_date'		=>		"2016-12-30", // Y-m-d
	'active'			=>		"1",
	'send_backendmails'	=>		"0",
	'create_sublogins'	=>		"1",
	'is_subscribed'		=>		"0",
	'store_id'			=>		"1",
	'address_ids'		=>		"",
	'acl'				=>		"allow_checkout,allow_orderview",
);
$result = $client->subloginUpdate(array(
	'sessionId'=>$sessionId, 
	'id'=>$subloginId, // OR can specify id manually for example: 1
	'data'=>$data
));
echo "########## UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

$result = $client->subloginInfo(array(
	'sessionId'=>$sessionId,
	'id'=>$subloginId, // OR can specify id manually for example: 1
));
echo "########## INFO AFTER UPDATE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";


// how to delete
$result = $client->subloginDelete(array(
	'sessionId'=>$sessionId, 
	'id'=>$subloginId, // OR can specify id manually for example: 1
)); 
echo "########## DELETE ##########<br>";
echo "<pre>";
print_r($result);
echo "</pre>";

$client->endSession(array(
	'sessionId'=>$sessionId
));
