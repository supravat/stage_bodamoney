<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="content-type" content="text/html"/>
<meta name="author" content="GallerySoft.info"/>
<title>Untitled 2</title>
</head>
<link href="/css/checkout.css" rel="stylesheet"/>
<link href="/css/core/global.css" rel="stylesheet"/>
<body>
<div id="stdpage" class="">
	<div id="header">
		<h1 class="confidential">Garcon Model</h1>
	</div>
	<hr>
	<div id="content">
		<div id="headline">
			<h1 class="accessAid">Choose a way to pay</h1>
		</div>
		<div id="messageBox">
		</div>
		<div id="main">
			<div class="layout1">
				<input type="hidden" value="" name="yui-history-field" id="yui-history-field">
				<form action="https://www.paypal.com/uk/cgi-bin/merchantpaymentweb" class="noAnimation" method="get" id="historyForm">
					<input type="hidden" value="_flow" name="cmd"/>
					<input type="hidden" value="50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b08198ecd47ed44bac94cd6fd721232afa4155" name="dispatch" id="historyDispatch"><input type="hidden" name="SESSION" id="historySession"><input type="submit" class="hide parentSubmit noAnimation" value="" name="historySubmit" id="historySubmit"><input type="hidden" value="A-PHmpEouhOvmDfMgCSW1k56XnOqaz0aF8aRihIDf-dlZh58zyTZqCclhj5jLdmIh" name="auth"><input type="hidden" value="UTF-8" name="form_charset"><input type="hidden" name="flow_name" value="xpt/Checkout/wps/Login"/>
					<input type="hidden" name="flow_name" value="xpt/Checkout/wps/Login">
				</form>
				<div id="swapPanel" class="accessAid">
				</div>
				<div id="babySlider" class="accessAid">
				</div>
				<form class="" action="https://www.paypal.com/uk/cgi-bin/merchantpaymentweb?dispatch=50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b08198ecd47ed44bac94cd6fd721232afa4155" name="login_form" id="parentForm" method="post">
					<div id="loginToChange" class="hide">
						<div class="messageBox error">
							<h2 class="accessAid">Error Message</h2>
							<p>
								Log in to make changes.
							</p>
						</div>
					</div>
					<input type="hidden" value="_flow" name="cmd">
					<input type="hidden" value="" name="myAllTextSubmitID" id="myAllTextSubmitID"/>
					<input type="hidden" value="" name="miniPager" id="miniPager">
					<div class="layout2f">
						<div class="col first">
							<div class="rounded defaultStyles">
								<div class="top outer">
								</div>
								<div class="body outer clearfix">
									<div class="rounded child">
										<div class="top inner">
										</div>
										<div id="miniCartContent" class="body clearfix" style="height: 540px;">
											<div class="" id="miniCart">
												<h3>Your order summary</h3>
												<div class="small head wrap">
													Descriptions<span class="amount">Amount</span>
												</div>
												<ol class="small wrap items limit-a">
													<li class="seller1">
													<ul>
														<li class="itmdet" id="multiitem1">
														<ul class="item1">
															<li class="dark">
															<span class="name"><a id="showname0" title="" href="#name0" class="autoTooltip">Green Boxer<span class="accessAid">Green Boxer</span></a></span><span class="amount">�20.00</span></li>
															<li class="secondary">Item number: GM12-BX01-GRE-S</li>
															<li class="secondary"><span>Item price: �20.00</span></li>
															<li class="secondary">Quantity: 1</li>
														</ul>
														<ul>
															<li class="dark">
															<span class="name"><a id="showname1" title="" href="#name1" class="autoTooltip">Shipping<span class="accessAid">Shipping</span></a></span><span class="amount">�5.00</span></li>
															<li class="secondary">Item number: <a id="shownum1" title="" href="#num1" class="autoTooltip">Standard Shipping �<span class="accessAid">Standard Shipping - International Standard (5-10 Business Days)</span></a></li>
															<li class="secondary"><span>Item price: �5.00</span></li>
															<li class="secondary">Quantity: 1</li>
														</ul>
														<ul>
														</ul>
														</li>
													</ul>
													</li>
												</ol>
												<div class="wrap items totals item1">
													<ul>
														<li class="small heavy">Item total<span class="amount">�25.00</span></li>
													</ul>
												</div>
												<div class="small wrap items totals item1">
													<ul>
														<li class="heavy highlight finalTotal">
														<span class="grandTotal amount highlight">Total �25.00 GBP</span></li>
													</ul>
												</div>
												<div>
													<div>
													</div>
													<div>
													</div>
												</div>
											</div>
										</div>
										<div class="bottom inner">
										</div>
									</div>
								</div>
								<div class="bottom outer">
								</div>
							</div>
						</div>
						<div class="col last">
							<div id="fullPageMask" class="hide">
								<div id="pageMask">
									&nbsp;<input type="hidden" value="1" name="reviewPgReturn" id="reviewPgReturn">
								</div>
								<div id="pgMaskContent" class="pgMaskContent">
									<div class="pplogo">
									</div>
									<div id="pgMaskMsg" class="pgMaskMessage">
										Don't see the secure browser? We'll help you re-launch the window to complete your purchase.<a name="pageMaskContinue" class="pgMaskCont" id="pgMaskContinue">Continue</a>
									</div>
								</div>
							</div>
							<div id="panelMask" class="hide">
								<div class="top">
									<div>
										&nbsp;
									</div>
								</div>
								<div class="body clearfix">
									<div>
										<div id="progressMeter">
											<img border="0" alt="Loading.." src="https://www.paypalobjects.com/en_US/i/icon/icon_animated_prog_dkgy_42wx42h.gif">
											<span id="defaultMsg" class="panelMaskMsg">Loading...</span>
											<span id="updateCtry" class="hide panelMaskMsg">Updating country</span>
											<span id="loginMsg" class="panelMaskMsg hide">Authenticating your information</span>
											<span id="loginUpdateMsg" class="panelMaskMsg hide">Logging you in</span>
											<span id="authMsg" class="hide panelMaskMsg">Authenticating your information.</span>
											<span id="authUpdateMsg" class="hide panelMaskMsg">Securing your information.</span>
											<span id="reviewMsg" class="hide panelMaskMsg">Updating your information...</span>
											<span id="shippingMsg" class="hide panelMaskMsg">Updating your information...</span>
											<span id="cartMsg" class="hide panelMaskMsg">Updating order total...</span>
										</div>
									</div>
								</div>
								<div class="bottom">
									<div>
										&nbsp;
									</div>
								</div>
							</div>
							<div id="sliderWrapper">
								<div id="parentSlider" style="margin-left: 0px; z-index: 10;" class="" tabindex="-1">
									<div id="sliders">
										<input type="hidden" value="font_normal" name="font_option" id="font_option">
										<input type="hidden" value="T22j8kCEQeBGA7-3Xnh_KIFiCfaL8VIVD3BWPjJBojG2QUx-S1w00aapKUW" name="currentSession" id="currentSession"><input type="hidden" value="login" name="pageState" id="pageState"><input type="hidden" value="50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b08198ecd47ed44bac94cd6fd721232afa4155" name="currentDispatch" id="currentDispatch">
										<noscript>
										&lt;input type="hidden" id="flag_non_js" name="flag_non_js" value="true"&gt;
										</noscript>
										<div id="loginModule">
											<div class="subhdr" id="hdrContainer">
												<h2 id="loginPageTitle">Choose a way to pay</h2>
											</div>
											<div id="method-paypal" class="panel active">
												<div class="top">
												</div>
												<div class="body clearfix">
													<div class="lockLogo" id="secureCheckout">
														<span title="PayPal" class="spriteLogo paypallock"></span>
													</div>
													<span class="downarrow"></span>
													<div id="loginTitle" class="subhead">
														Pay with my PayPal account <span class="help">Log in to your account to complete the purchase</span>
													</div>
													<input type="hidden" value="false" name="email_recovery">
													<input type="hidden" value="false" name="password_recovery">
													<div id="loginBox">
														<p class="group">
															<label for="login_email">
															<span class="labelText">Email</span></label>
															<span class="field"><input type="text" value="info@3tiermarketing.com" name="login_email" class="confidential large" id="login_email"></span>
														</p>
														<p class="group">
															<label for="login_password"><span class="labelText">PayPal password</span></label><span class="field"><input type="password" class="restricted large" value="" name="login_password" id="login_password" autocomplete="off"></span>
														</p>
														<p id="privateDevice">
															<input type="hidden" value="on" name="private_device_checkbox_flag">
															<input type="checkbox" value="true" name="private_device" class="checkbox" id="privateDeviceCheckbox">
															<label for="privateDeviceCheckbox">This is a private computer.</label>
															<span title="" id="privateDeviceTooltip" class="autoTooltip" tabindex="0">What's this?<span class="accessAid">We remember your login details so you can check out more quickly. Please don't choose this option if you are using a public or shared computer.</span></span>
														</p>
														<p class="buttons">
															<input type="submit" class="button primary default parentSubmit" value="Log In" name="login.x" id="submitLogin">
														</p>
														<p>
															<a onclick="PAYPAL.core.openWindow(event,{width: 385, height:530, left:420, top:250});" href="https://www.paypal.com/uk/webapps/accountrecovery/passwordrecovery" target="_blank">Forgotten your email address or password?</a>
														</p>
														<div id="account-recovery-help" class="hide clearfix">
															<div class="col1">
																<ul>
																	<li>Step 1. Enter or recover your email address.</li>
																	<li>Step 2. Reset your password.</li>
																	<li>Step 3. Return and complete your payment.</li>
																</ul>
															</div>
															<div class="col2">
																<a href="https://www.paypal.com/uk/cgi-bin/merchantpaymentweb?cmd=_account-recovery&amp;from=PayPal" target="_blank">Start now</a>
															</div>
														</div>
														<p>
														</p>
													</div>
													<input type="hidden" value="dKmDG6xsR_DkDY_V18lUVAgGv_0O0sPq6kbABRnZZQSqCQw8t3jGTswj0P0" name="SESSION" id="pageSession"><input type="hidden" value="50a222a57771920b6a3d7b606239e4d529b525e0b7e69bf0224adecfb0124e9b61f737ba21b08198ecd47ed44bac94cd6fd721232afa4155" name="dispatch" id="pageDispatch"><input type="hidden" value="X3-7SZn2ExXucINxlliZ_05NdFsrIIpaV9TcRYNLL_GiOwm9XgEZzWKQeV0" name="CONTEXT" id="CONTEXT_CGI_VAR">
												</div>
												<div class="bottom">
												</div>
											</div>
											<div class="panel scTrack:main:wps:ux:3pcart:start:ClickBilling" id="method-cc" style="cursor: pointer;">
												<span class="rightarrow"></span>
												<div class="subhead">
													<span class="buttonAsLink"><input type="submit" class="parentSubmit noMask noAnimation" name="new_user_button.x" id="minipageSubmitBtn" value="Pay with a debit or credit card"></span>
													<p class="small secondary">
														(Optional) Sign up to PayPal to make your next checkout faster
													</p>
												</div>
												<input type="hidden" value="_flow" name="cmd"><input type="hidden" value="" name="id"><input type="hidden" value="false" name="close_external_flow"><input type="hidden" value="payment_flow" name="external_close_account_payment_flow">
											</div>
										</div>
									</div>
									<div class="layout1">
										<p class="small secondary return">
											<span class="buttonAsLink"><input type="submit" class="action" name="cancel_return" value="Cancel and return to Garcon Model."></span>
										</p>
									</div>
									<img width="1" height="1" border="0" alt="" src="https://www.paypalobjects.com/en_US/i/scr/btn_tracking_pixel.gif?fltk=e18fb77b8054d&amp;ip=14%2e176%2e141%2e59&amp;vid=6124911754768022502&amp;calc=48734a737555f&amp;calf=752d7a14d9afd&amp;mrid=21C748851E4454028&amp;WWW_AKA_MVT_ID=null&amp;WWW_ AKA_MVT_BUTTONS=null&amp;page=main%3awps%3aux%3a3pcart%3astart%3amember%3a%3a&amp;fpti=0e6b73bf14c0a494d2056535fe74c053&amp;xe=935%2c538&amp;xt=2099%2c1067&amp;teal=OAroEZk47Y%252bNuX7gGziLV0fdk1Nt0aXAtMPZYa6WxPFwOsb2MWQz7s12E%252fNL7%252b2trNF9%252fG5U%252byo%253d%5f14ca9c2b31a&amp;ru=https%3a%2f%2fsecure%2egarconmodel%2ecom%2fpaypal%2fstandard%2fredirect%2f">
								</div>
								<script type="text/javascript">
																	YUD.setStyle('parentSlider', 'margin-left', '-560px');
																</script>
							</div>
						</div>
					</div>
					<input type="hidden" value="A-PHmpEouhOvmDfMgCSW1k56XnOqaz0aF8aRihIDf-dlZh58zyTZqCclhj5jLdmIh" name="auth"><input type="hidden" value="UTF-8" name="form_charset"><input type="hidden" name="flow_name" value="xpt/Checkout/wps/Login"><input type="hidden" name="flow_name" value="xpt/Checkout/wps/Login"><input type="hidden" name="external_remember_me_read_cookie_ids" value="">
				</form>
				<script type="text/javascript">YAHOO.util.Event.addListener(window, "load", function(){var meplexwp = new Image(); meplexwp.src='https://altfarm.mediaplex.com/ad/bk/3484-16283-2054-83?WPSTXNStart=1'}); </script>
			</div>
		</div>
	</div>
	<input type="hidden" value="GB" name="" id="XoCountryCode">
	<div id="prefooter">
	</div>
	<div id="footer">
		<div id="opinionlab">
			<a href="javascript:PP_O_LC();">Site Feedback</a>&nbsp;<img border="0" alt="Site Feedback" src="https://www.paypalobjects.com/en_US/i/scr/sm_333_oo.gif">
		</div>
		<!-- OnlineOpinionF3cS v3.0-->
		<script type="text/javascript">
			var feedback_link='Site Feedback'; var _ht=escape(document.location.href);
                                                                var _hr=document.referrer;custom_var=escape('OAroEZk47Y\x252bNuX7gGziLV0fdk1Nt0aXAtMPZYa6WxPFwOsb2MWQz7s12E\x252fNL7\x252b2trNF9\x252fG5U\x252byo\x253d\x5f14ca9c2b31a')+'|';custom_var+='Unknown|main\x3awps\x3aux\x3a3pcart\x3astart|GB|en_GB|Unknown|21C748851E4454028';// Paypal Custom URL class
			var PayPalURL = function(pagename) {
				this.language_country_code = "en_GB";
				this.dd = "GB";
				this.nn = "00";this.pagename = escape(pagename.replace(/\s|\//g, "_"));
				this.to_str = function() {
				return "https://"+this.dd+".paypal.com/"+this.language_country_code+"/"+this.nn+"/"+this.pagename+".page"
				}
				}
				var paypal_url = new PayPalURL("main\x3awps\x3aux\x3a3pcart\x3astart");
			// Below gives you the ability to change the other properties after the object is created
			//paypal_url.dd="ES";
			//paypal_url.nn="01";
		_ht = paypal_url.to_str();
		var _ht_temp=_ht;
		var _hr_temp=_hr;
		var baseurl="https://www\x2epaypal\x2ecom\x2fuk/cgi-bin/webscr?cmd=_handle-sf-redirect";var custom_var_temp=custom_var;</script>
		<p>
			PayPal. Safer. Simpler. Smarter.
		</p>
		<p>
			 For more information, see our <a title="Opens in new window" href="https://cms.paypal.com/uk/cgi-bin/marketingweb?cmd=_render-content&amp;content_ID=ua/Privacy_popup&amp;locale.x=en_GB" id="privacy_policy_link" target="_blank">Privacy Policy</a>, <a title="Opens in new window" href="https://cms.paypal.com/uk/cgi-bin/marketingweb?cmd=_render-content&amp;content_ID=ua/UserAgreement_popup&amp;locale.x=en_GB" id="ua_link" target="_blank">User Agreement</a> and <a title="Opens in new window" href="https://cms.paypal.com/uk/cgi-bin/marketingweb?cmd=_render-content&amp;content_ID=ua/ServiceDescription_popup&amp;locale.x=en_GB" target="_blank">Key Payment and Service Information</a>.
		</p>
		<script type="text/javascript">document.getElementById("privacy_policy_link").onclick=function() {openWindow640('https://cms\x2epaypal\x2ecom/uk/cgi-bin/marketingweb?cmd=_render-content&amp;content_ID=ua/Privacy_popup&amp;locale.x=en_GB'); return false;};document.getElementById("ua_link").onclick=function() {openWindow640('https://cms\x2epaypal\x2ecom/uk/cgi-bin/marketingweb?cmd=_render-content&amp;content_ID=ua/UserAgreement_popup&amp;locale.x=en_GB'); return false;};</script>
		<div role="contentinfo" id="footer">
			<p id="legal">
				Copyright &copy; 1999-2015 PayPal. All rights reserved.
			</p>
		</div>
	</div>
</div>
</body>
</html>